package com.calclipse.math.parser;

import java.util.Collection;

import com.calclipse.math.expression.Token;
import com.calclipse.math.parser.internal.LongestMatchLookupTable;

/**
 * A symbol table.
 * Used in particular during scanning to
 * {@link #lookUp(char) look up}
 * non-literal symbols.
 * 
 * @author Tone Sommerland
 */
public interface SymbolTable
{
    /**
     * Returns a longest-match lookup table.
     * This symbol table is based on a longest-match approach
     * to symbol recognition.
     * As an example, say we have the symbols 's', 'int' and 'sin'.
     * Then the symbol 'sin',  rather than then symbols 's' and 'int',
     * will be extracted from the expression 'sint'
     * (the last 't' will be undefined).
     * This symbol table does not allow tokens with duplicate names.
     */
    public static SymbolTable longestMatchLookup()
    {
        return new LongestMatchLookupTable();
    }
    
    /**
     * Adds a symbol to the table.
     * @return {@code false} if the symbol was not added,
     * for instance due to a name conflict
     */
    public abstract boolean add(Token symbol);
    
    /**
     * Removes a symbol from the table.
     * @param name the name of the symbol
     * @return {@code false} if the symbol could not be removed,
     * for instance because it wasn't found
     */
    public abstract boolean remove(String name);
    
    /**
     * Returns the symbol with the given name.
     * @return {@code null} if not found
     */
    public abstract Token get(String name);
    
    /**
     * Returns all symbols stored in the table.
     * @return an empty collection if none
     */
    public abstract Collection<Token> all();
    
    /**
     * Returns all symbols with names starting with the given char.
     * This is the primary method used for recognizing symbols during a scan.
     * Therefore, the faster this method, the better parser performance.
     * A scanner will typically try to match symbols
     * starting with the first symbol in the returned collection,
     * so the order of the returned tokens is significant.
     * @return an empty collection if none
     */
    public abstract Collection<Token> lookUp(char firstChar);
    
    /**
     * Clears the table.
     */
    public abstract void clear();

}
