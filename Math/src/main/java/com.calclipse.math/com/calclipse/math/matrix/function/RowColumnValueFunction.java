package com.calclipse.math.matrix.function;

/**
 * A function that accepts a row index, a column index
 * and the current value,
 * and returns a new value.
 * 
 * @see com.calclipse.math.matrix.Matrix
 * #applyRowColumnValueFunction(RowColumnValueFunction)
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface RowColumnValueFunction
{
    /**
     * Provides the value for the element at the given row and column.
     * @param row the row index
     * @param column the column index
     * @param value the current value of the element
     */
    public abstract double apply(int row, int column, double value);

}
