package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.LuFactorization;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Creates an LU decomposition of a matrix.
 * Accepts the matrix to be factorized,
 * a boolean flag specifying whether to use partial pivoting,
 * and an augmentation matrix.
 * The two last arguments are optional.
 * Returns an array containing L, U, P,
 * and the augmentation after row operations (if supplied).
 * LU = PA.
 * 
 * @author Tone Sommerland
 */
public final class Lu implements VariableArityOperation
{
    public static final String NAME = "lu";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Lu(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Lu(typeContext)).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final var params = new Gauss.ReductionParameters(typeContext, args);
        final LuFactorization lu;
        if (params.augmentation() == null)
        {
            lu = LuFactorization.factorize(
                    params.coefficients(),
                    params.isPivoting());
        }
        else
        {
            try
            {
                lu = LuFactorization.factorizeAugmented(
                        params.coefficients(),
                        params.augmentation(),
                        params.isPivoting());
            }
            catch (final SizeViolation ex)
            {
                throw errorMessage(EvalMessages.mismatchingDimensions())
                    .withCause(ex);
            }
        }
        final Matrix p = params.coefficients().create(0, 0);
        lu.p().copyEntriesTo(p);
        final var arr = typeContext.arrayType().arrayOf(
                lu.l().copy(), lu.u().copy(), p);
        lu.augmentation().map(Matrix::copy).ifPresent(arr::add);
        return Value.constantOf(arr);
    }

}
