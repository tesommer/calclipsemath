package com.calclipse.math.expression;

import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;

/**
 * Values with special significance.
 * 
 * @author Tone Sommerland
 */
public final class Values
{
    /**
     * A constant representing an undefined value.
     * This instance may be used to represent values
     * such as {@code null} or {@code void}.
     * The value of this constant is an object with the string representation
     * {@code "undef"}.
     */
    public static final Value UNDEF = Value.constantOf(
            new Object()
            {
                @Override
                public String toString()
                {
                    return "undef";
                }
            });
    
    /**
     * This constant may be returned from an overriding evaluation
     * to let the overridden operation handle the evaluation instead.
     * See for instance
     * {@link
     * com.calclipse.math.expression.operation.BinaryOperation
     * #override(BinaryOperation)}.
     * The value of this constant is an object with the string representation
     * {@code "yield"}.
     */
    public static final Value YIELD = Value.constantOf(
            new Object()
            {
                @Override
                public String toString()
                {
                    return "yield";
                }
            });

    private Values()
    {
    }

}
