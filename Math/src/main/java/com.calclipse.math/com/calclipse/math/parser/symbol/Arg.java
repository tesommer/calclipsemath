package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.MathUtil;

/**
 * The argument of a complex.
 * Supported types:
 * <ul>
 *  <li>complex</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Arg implements UnaryOperation
{
    public static final String NAME = "arg";
    
    private static final Id ID = Id.unique();
    
    private static final UnaryOperation INSTANCE = new Arg();

    private Arg()
    {
    }
    
    public static UnaryOperator operator()
    {
        return UnaryOperator.prefixed(NAME, OperatorPriorities.MEDIUM, INSTANCE)
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Complex)
        {
            final var c = (Complex)o;
            return Value.constantOf(MathUtil.arg(c));
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
