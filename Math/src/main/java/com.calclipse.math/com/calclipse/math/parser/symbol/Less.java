package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Tests if the first argument is less than the second.
 * Results in a true value if it is.
 * 
 * @author Tone Sommerland
 */
public final class Less implements BinaryOperation
{
    public static final String NAME = "<";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Less(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.TEST, new Less(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        return Value.constantOf(typeContext.booleanType()
                .valueOf(typeContext.comparer().compare(o1, o2) < 0));
    }

}
