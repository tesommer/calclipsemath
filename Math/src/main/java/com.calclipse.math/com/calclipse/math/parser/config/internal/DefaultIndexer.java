package com.calclipse.math.parser.config.internal;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.Arrays;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.Indexer;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The default indexer.
 * Handles indices to matrices, arrays, strings and other indexers.
 * 
 * @author Tone Sommerland
 */
public final class DefaultIndexer implements Indexer
{
    private final TypeContext typeContext;
    
    public DefaultIndexer(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }

    @Override
    public Value get(final Value value, final int... indices)
            throws ErrorMessage
    {
        final Object o = value.get();
        
        if (o instanceof Matrix)
        {
            return getForMatrix(typeContext, value, indices);
            
        }
        else if (o instanceof Array)
        {
            return getForArray(typeContext, value, indices);
            
        }
        else if (o instanceof String)
        {
            return getForString(value, indices);
            
        }
        else if (o instanceof Indexer)
        {
            return ((Indexer)o).get(value, indices);
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }
    
    private static Value getForMatrix(
            final TypeContext typeContext,
            final Value value,
            final int[] indices) throws ErrorMessage
    {
        if (indices.length == 2)
        {
            return getMatrixElement(
                    typeContext, value, indices[0], indices[1]);
        }
        else if (indices.length == 4)
        {
            return getMatrixPartition(
                    value, indices[0], indices[1], indices[2], indices[3]);
        }
        else
        {
            throw errorMessage(EvalMessages.invalidIndexCount());
        }
    }
    
    private static Value getForArray(
            final TypeContext typeContext,
            final Value value,
            final int[] indices) throws ErrorMessage
    {
        if (indices.length == 1)
        {
            return getArrayElement(value, indices[0]);
        }
        else if (indices.length == 2)
        {
            return getSubArray(typeContext, value, indices[0], indices[1]);
        }
        else
        {
            throw errorMessage(EvalMessages.invalidIndexCount());
        }
    }
    
    private static Value getForString(
            final Value value, final int[] indices) throws ErrorMessage
    {
        if (indices.length == 1)
        {
            return getStringChar(value, indices[0]);
        }
        else if (indices.length == 2)
        {
            return getSubstring(value, indices[0], indices[1]);
        }
        else
        {
            throw errorMessage(EvalMessages.invalidIndexCount());
        }
    }
    
    private static Value getMatrixElement(
            final TypeContext typeContext,
            final Value value,
            final int index1,
            final int index2) throws ErrorMessage
    {
        final int row = index1 - 1;
        final int col = index2 - 1;
        final var mx = (Matrix)value.get();
        if (row < 0 || row >= mx.rows() || col < 0 || col >= mx.columns())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        if (value.isConstant())
        {
            return Value.constantOf(typeContext.numberType()
                    .valueOf(mx.get(row, col)));
        }
        else
        {
            return new MatrixElementValue(typeContext, mx, row, col);
        }
    }
    
    private static Value getMatrixPartition(
            final Value value,
            final int index1,
            final int index2,
            final int index3,
            final int index4) throws ErrorMessage
    {
        final int fromRow = index1 - 1;
        final int fromCol = index2 - 1;
        final int toRow = index3 - 1;
        final int toCol = index4 - 1;
        
        final var mx = (Matrix)value.get();
        
        checkMatrixPartitionArgs(mx, fromRow, fromCol, toRow, toCol);
        
        if (value.isConstant())
        {
            return Value.constantOf(
                    mx.partition(fromRow, fromCol, toRow, toCol));
        }
        else
        {
            return new MatrixPartitionValue(
                    mx, fromRow, fromCol, toRow, toCol);
        }
    }

    private static void checkMatrixPartitionArgs(
            final Matrix mx,
            final int fromRow,
            final int fromCol,
            final int toRow,
            final int toCol) throws ErrorMessage
    {
        if (fromRow < 0 || fromRow >= mx.rows())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        else if (fromCol < 0 || fromCol >= mx.columns())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        else if (toRow < fromRow || toRow > mx.rows())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        else if (toCol < fromCol || toCol > mx.columns())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
    }
    
    private static Value getArrayElement(
            final Value value, final int index1) throws ErrorMessage
    {
        final int index = index1 - 1;
        final var arr = (Array)value.get();
        if (index < 0 || index >= arr.size())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        if (value.isConstant())
        {
            return Value.constantOf(arr.get(index));
        }
        else
        {
            return new ArrayElementValue(arr, index);
        }
    }
    
    private static Value getSubArray(
            final TypeContext typeContext,
            final Value value,
            final int index1,
            final int index2) throws ErrorMessage
    {
        final int fromIndex = index1 - 1;
        final int toIndex = index2 - 1;
        
        final var arr = (Array)value.get();
        
        if (fromIndex < 0 || fromIndex >= arr.size()
                || toIndex < fromIndex || toIndex > arr.size())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        
        if (value.isConstant())
        {
            return Value.constantOf(typeContext.arrayType().createArray(
                    arr.subList(fromIndex, toIndex)));
        }
        else
        {
            return new SubArrayValue(typeContext, arr, fromIndex, toIndex);
        }
    }
    
    private static Value getStringChar(
            final Value value, final int index1) throws ErrorMessage
    {
        final int index = index1 - 1;
        final var str = (String)value.get();
        if (index < 0 || index >= str.length())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        return Value.constantOf(String.valueOf(str.charAt(index)));
    }
    
    private static Value getSubstring(
            final Value value,
            final int index1,
            final int index2) throws ErrorMessage
    {
        final int fromIndex = index1 - 1;
        final int toIndex = index2 - 1;
        final var str = (String)value.get();
        if (fromIndex < 0 || fromIndex >= str.length()
                || toIndex < fromIndex || toIndex > str.length())
        {
            throw errorMessage(EvalMessages.invalidIndex());
        }
        return Value.constantOf(str.substring(fromIndex, toIndex));
    }
    
    /**
     * Superclass of a value backed by a matrix or an array.
     */
    private static abstract class IndexableValue implements Value
    {
        private final int[] dimensions;

        private IndexableValue(final int... dimensions)
        {
            this.dimensions = dimensions.clone();
        }
        
        IndexableValue(final Matrix matrix)
        {
            this(matrix.rows(), matrix.columns());
        }
        
        IndexableValue(final Array array)
        {
            this(array.size());
        }
        
        private boolean hasSizeChanged(final int... currentDimensions)
        {
            return !Arrays.equals(dimensions, currentDimensions);
        }
        
        /**
         * Throws IllegalStateException if the size of the underlying matrix
         * has changed since this value was created.
         * @param matrix the underlying matrix
         */
        void assureSizeUnchanged(final Matrix matrix)
        {
            if (hasSizeChanged(matrix.rows(), matrix.columns()))
            {
                throw new IllegalStateException("Matrix size has changed.");
            }
        }
        
        /**
         * Throws IllegalStateException if the size of the underlying array
         * has changed since this value was created.
         * @param array the underlying array
         */
        void assureSizeUnchanged(final Array array)
        {
            if (hasSizeChanged(array.size()))
            {
                throw new IllegalStateException("Array size has changed.");
            }
        }

        @Override
        public final boolean isConstant()
        {
            return false;
        }
    }
    
    private static final class MatrixElementValue extends IndexableValue
    {
        private final TypeContext typeContext;
        private final Matrix matrix;
        private final int row;
        private final int column;
        
        MatrixElementValue(
                final TypeContext typeContext,
                final Matrix matrix,
                final int row,
                final int column)
        {
            super(matrix);
            assert typeContext != null;
            assert row         >= 0 && row    < matrix.rows();
            assert column      >= 0 && column < matrix.columns();
            this.typeContext = typeContext;
            this.matrix = matrix;
            this.row = row;
            this.column = column;
        }
        
        @Override
        public Object get()
        {
            assureSizeUnchanged(matrix);
            return typeContext.numberType().valueOf(matrix.get(row, column));
        }
        
        @Override
        public void set(final Object value) throws ErrorMessage
        {
            assureSizeUnchanged(matrix);
            matrix.set(row, column, TypeUtil.toDouble(value));
        }
    }
    
    private static final class MatrixPartitionValue extends IndexableValue
    {
        private final Matrix matrix;
        private final int fromRow;
        private final int fromColumn;
        private final int toRow;
        private final int toColumn;
        
        MatrixPartitionValue(
                final Matrix matrix,
                final int fromRow,
                final int fromColumn,
                final int toRow,
                final int toColumn)
        {
            super(matrix);
            assert fromRow    >= 0          && fromRow    <  matrix.rows();
            assert fromColumn >= 0          && fromColumn <  matrix.columns();
            assert toRow      >= fromRow    && toRow      <= matrix.rows();
            assert toColumn   >= fromColumn && toColumn   <= matrix.columns();
            this.matrix = matrix;
            this.fromRow = fromRow;
            this.fromColumn = fromColumn;
            this.toRow = toRow;
            this.toColumn = toColumn;
        }
        
        @Override
        public Object get()
        {
            assureSizeUnchanged(matrix);
            return matrix.partition(fromRow, fromColumn, toRow, toColumn);
        }
        
        @Override
        public void set(final Object value) throws ErrorMessage
        {
            assureSizeUnchanged(matrix);
            if (value instanceof Matrix)
            {
                final var partition = (Matrix)value;
                if (partition.rows() != toRow - fromRow
                        || partition.columns() != toColumn - fromColumn)
                {
                    throw errorMessage(EvalMessages.invalidSize());
                }
                matrix.setPartition(fromRow, fromColumn, partition);
            }
            else
            {
                throw errorMessage(TypeMessages.invalidType(value));
            }
        }
    }
    
    private static final class ArrayElementValue extends IndexableValue
    {
        private final Array array;
        private final int index;
        
        ArrayElementValue(final Array array, final int index)
        {
            super(array);
            assert index >= 0 && index < array.size();
            this.array = array;
            this.index = index;
        }
        
        @Override
        public Object get()
        {
            assureSizeUnchanged(array);
            return array.get(index);
        }
        
        @Override
        public void set(final Object value) throws ErrorMessage
        {
            assureSizeUnchanged(array);
            array.set(index, value);
        }
    }
    
    private static final class SubArrayValue extends IndexableValue
    {
        private final TypeContext typeContext;
        private final Array array;
        private final int fromIndex;
        private final int toIndex;
        
        SubArrayValue(
                final TypeContext typeContext,
                final Array array,
                final int fromIndex,
                final int toIndex)
        {
            super(array);
            assert typeContext != null;
            assert fromIndex   >= 0         && fromIndex <  array.size();
            assert toIndex     >= fromIndex && toIndex   <= array.size();
            this.typeContext = typeContext;
            this.array = array;
            this.fromIndex = fromIndex;
            this.toIndex = toIndex;
        }
        
        @Override
        public Object get()
        {
            assureSizeUnchanged(array);
            return typeContext.arrayType().createArray(
                    array.subList(fromIndex, toIndex));
        }
        
        @Override
        public void set(final Object value) throws ErrorMessage
        {
            assureSizeUnchanged(array);
            if (value instanceof Array)
            {
                final var subArray = (Array)value;
                if (toIndex - fromIndex != subArray.size())
                {
                    throw errorMessage(EvalMessages.invalidSize());
                }
                for (int i = 0; i < subArray.size(); i++)
                {
                    array.set(fromIndex + i, subArray.get(i));
                }
            }
            else
            {
                throw errorMessage(TypeMessages.invalidType(value));
            }
        }
    }

}
