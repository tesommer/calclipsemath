package com.calclipse.math.parser.config.internal;

import java.math.BigInteger;

import com.calclipse.math.Fraction;
import com.calclipse.math.parser.misc.MathUtil;

/**
 * The default matrix element format.
 * 
 * @author Tone Sommerland
 */
public final class EntryStringification
{
    private EntryStringification()
    {
    }

    /**
     * Makes a string representation of a matrix element.
     */
    public static String stringify(final double element)
    {
        final int intElement = (int)element;
        if (intElement == element)
        {
            return String.valueOf(intElement);
        }
        try
        {
            final Fraction frac = MathUtil.toFraction(element, 23, 1E-5)
                    .orElse(null);
            if (frac == null)
            {
                return String.valueOf(element);
            }
            if (frac.denominator().equals(BigInteger.ONE))
            {
                return frac.numerator().toString();
            }
            return frac.toString();
        }
        catch (final ArithmeticException ex)
        {
            return String.valueOf(element);
        }
    }

}
