package com.calclipse.math.parser.misc;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.math.BigInteger;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.parser.type.TypeContext;

/**
 * This class contains static utility methods for converting
 * {@link com.calclipse.math.expression.operation.Value#get() operand values}
 * into primitive types.
 * 
 * @author Tone Sommerland
 */
public final class TypeUtil
{
    private TypeUtil()
    {
    }
    
    /**
     * Returns an operand value as a {@code double}.
     * @param argNumber an argument number to include in the error message
     * if an exception is thrown
     * @throws ErrorMessage if the given value is unsuitable
     * @throws IllegalArgumentException if {@code argNumber} is less than one
     */
    public static double argToDouble(final int argNumber, final Object value)
            throws ErrorMessage
    {
        return intoDouble(requireOneBasedArgNumber(argNumber), value);
    }
    
    /**
     * Returns an operand value as a {@code double}.
     * @throws ErrorMessage if the given value is unsuitable
     */
    public static double toDouble(final Object value) throws ErrorMessage
    {
        return intoDouble(-1, value);
    }
    
    private static double intoDouble(final int argNumber, final Object value)
            throws ErrorMessage
    {
        requireNonNull(value, "value");
        if (value instanceof Number)
        {
            return ((Number)value).doubleValue();
        }
        throw error(argNumber, TypeMessages.invalidType(value));   
    }
    
    /**
     * Returns an operand value as a {@code long}.
     * @param argNumber an argument number to include in the error message
     * if an exception is thrown
     * @throws ErrorMessage if the given value is unsuitable
     * @throws IllegalArgumentException if {@code argNumber} is less than one
     */
    public static long argToLong(final int argNumber, final Object value)
            throws ErrorMessage
    {
        return intoLong(requireOneBasedArgNumber(argNumber), value);
    }
    
    /**
     * Returns an operand value as a {@code long}.
     * @throws ErrorMessage if the given value is unsuitable
     */
    public static long toLong(final Object value) throws ErrorMessage
    {
        return intoLong(-1, value);
    }
    
    private static long intoLong(final int argNumber, final Object value)
            throws ErrorMessage
    {
        requireNonNull(value, "value");
        if ((value instanceof Long) || (value instanceof BigInteger))
        {
            return ((Number)value).longValue();
        }
        else if (value instanceof Number)
        {
            final double d = ((Number)value).doubleValue();
            final long l = (long)d;
            if (l == d)
            {
                return l;
            }
            throw error(argNumber, EvalMessages.expectedInteger());
        }
        throw error(argNumber, TypeMessages.invalidType(value));
    }
    
    /**
     * Returns {@code true}
     * if the given operand value can be represented as an {@code int}.
     * If this method returns {@code false},
     * then calling
     * {@link #toInt(Object)} or
     * {@link #argToInt(int, Object)}
     * with the same argument
     * would cause an exception.
     */
    public static boolean isInt(final Object value)
    {
        requireNonNull(value, "value");
        if (value instanceof Number)
        {
            final double d = ((Number)value).doubleValue();
            final int i = (int)d;
            return i == d;
        }
        return false;
    }
    
    /**
     * Returns an operand value as an {@code int}.
     * @param argNumber an argument number to include in the error message
     * if an exception is thrown
     * @throws ErrorMessage if the given value is unsuitable
     * @throws IllegalArgumentException if {@code argNumber} is less than one
     */
    public static int argToInt(final int argNumber, final Object value)
            throws ErrorMessage
    {
        return intoInt(requireOneBasedArgNumber(argNumber), value);
    }
    
    /**
     * Returns an operand value as an {@code int}.
     * @throws ErrorMessage if the given value is unsuitable
     */
    public static int toInt(final Object value) throws ErrorMessage
    {
        return intoInt(-1, value);
    }
    
    private static int intoInt(final int argNumber, final Object value)
            throws ErrorMessage
    {
        requireNonNull(value, "value");
        if (value instanceof Number)
        {
            final double d = ((Number)value).doubleValue();
            final int i = (int)d;
            if (i == d)
            {
                return i;
            }
            throw error(argNumber, EvalMessages.expectedInteger());
        }
        throw error(argNumber, TypeMessages.invalidType(value));
    }
    
    /**
     * Returns an operand value as a {@code boolean}.
     * @param argNumber an argument number to include in the error message
     * if an exception is thrown
     * @throws ErrorMessage if the given value is unsuitable
     * @throws IllegalArgumentException if {@code argNumber} is less than one
     */
    public static boolean argToBoolean(
            final int argNumber,
            final Object value,
            final TypeContext typeContext) throws ErrorMessage
    {
        requireNonNull(value, "value");
        requireOneBasedArgNumber(argNumber);
        try
        {
            return typeContext.booleanType().isTrue(value);
        }
        catch (final ErrorMessage ex)
        {
            throw error(argNumber, ex.getMessage(), ex);
        }
    }
    
    private static ErrorMessage error(
            final int argNumber, final String message, final Throwable cause)
    {
        if (argNumber < 1)
        {
            return errorMessage(message).withCause(cause);
        }
        final String messageForArg = ArgMessages.forArgument(
                argNumber, message);
        return errorMessage(messageForArg).withCause(cause);
    }
    
    private static ErrorMessage error(final int argNumber, final String message)
    {
        return error(argNumber, message, null);
    }
    
    private static int requireOneBasedArgNumber(final int argNumber)
    {
        if (argNumber < 1)
        {
            throw new IllegalArgumentException("argNumber < 1: " + argNumber);
        }
        return argNumber;
    }

}
