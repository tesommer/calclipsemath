package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Rounding to a certain number of decimals.
 * The second argument, the number of decimals, defaults to zero.
 * Supported types:
 * <ul>
 *  <li>complex[, integer]</li>
 *  <li>real[, integer]</li>
 *  <li>matrix[, integer]: rounds the elements</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Round implements VariableArityOperation
{
    public static final String NAME = "round";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Round(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Round(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(1, 2, args.length);
        
        final Object o1 = args[0].get();
        final int i2;
        
        if (args.length == 2)
        {
            i2 = TypeUtil.argToInt(2, args[1].get());
            if (i2 < 0)
            {
                throw ArgUtil.forArgument(2, EvalMessages.domainError());
            }
        }
        else
        {
            i2 = 0;
        }
        
        if (o1 instanceof Complex)
        {
            final var c1 = (Complex)o1;
            final double re = MathUtil.round(c1.real(), i2);
            final double im = MathUtil.round(c1.imaginary(), i2);
            return Value.constantOf(Complex.valueOf(re, im));
            
        }
        else if (o1 instanceof Number)
        {
            final var n1 = (Number)o1;
            return Value.constantOf(typeContext.numberType()
                    .valueOf(MathUtil.round(n1.doubleValue(), i2)));
            
        }
        else if (o1 instanceof Matrix)
        {
            final Matrix mx1 = ((Matrix)o1).copy();
            mx1.applyRowColumnValueFunction((row, column, value)
                    -> MathUtil.round(value, i2));
            return Value.constantOf(mx1);
        }
        
        throw ArgUtil.forArgument(1, TypeMessages.invalidType(o1));
    }

}
