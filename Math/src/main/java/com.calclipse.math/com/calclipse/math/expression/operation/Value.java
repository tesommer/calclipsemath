package com.calclipse.math.expression.operation;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.internal.Constant;
import com.calclipse.math.expression.operation.internal.ValueAsConstant;

/**
 * A mathematical value.
 * The value represented by this interface is not allowed to be {@code null}.
 * 
 * @see com.calclipse.math.expression.Operand
 * 
 * @author Tone Sommerland
 */
public interface Value
{
    /**
     * Returns a constant containing the given value.
     * The returned instance will return {@code true} from
     * {@link #isConstant()},
     * and its mutator will reject any value.
     * @param value the value of the constant
     * @return a constant value
     */
    public static Value constantOf(final Object value)
    {
        return new Constant(value);
    }
    
    /**
     * Rejects the given value by
     * throwing a {@link com.calclipse.math.expression.ErrorMessage}
     * with a message indicating that the assignment is illegal.
     */
    public static void reject(final Object value) throws ErrorMessage
    {
        requireNonNull(value, "value");
        throw errorMessage(EvalMessages.illegalAssignment());
    }
    
    /**
     * The accessor of this value.
     */
    public abstract Object get();
    
    /**
     * The mutator of this value.
     * @param value the object to assign to this value
     * @throws ErrorMessage if this value does not support the assignment
     */
    public abstract void set(Object value) throws ErrorMessage;
    
    /**
     * Whether or not this value is a constant.
     */
    public abstract boolean isConstant();
    
    /**
     * Returns a constant view of this value.
     * The returned value will return {@code true} from
     * {@link #isConstant()},
     * and its mutator will reject any value.
     */
    public default Value asConstant()
    {
        return new ValueAsConstant(this);
    }

}
