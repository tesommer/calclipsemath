package com.calclipse.math.expression.operation;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;

/**
 * A mathematical operation on a variable number of arguments.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface VariableArityOperation
{
    /**
     * Performs the evaluation.
     * @param args the arguments
     * @return the result of the evaluation
     * @throws ErrorMessage if the evaluation resulted in an error
     */
    public abstract Value evaluate(Value... args) throws ErrorMessage;
    
    /**
     * Overrides the given operation with this operation.
     * @param operation the operation to override
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default VariableArityOperation override(
            final VariableArityOperation operation)
    {
        requireNonNull(operation, "operation");
        return args -> operation.overriddenEvaluate(args, this);
    }
    
    /**
     * Allows this operation to be overridden by the given operation.
     * @param override the operation that overrides this one
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default VariableArityOperation acceptOverride(
            final VariableArityOperation override)
    {
        requireNonNull(override, "override");
        return args -> overriddenEvaluate(args, override);
    }
    
    private Value overriddenEvaluate(
            final Value[] args,
            final VariableArityOperation override) throws ErrorMessage
    {
        final Value result = override.evaluate(args);
        if (result == Values.YIELD)
        {
            return evaluate(args);
        }
        return result;
    }

}
