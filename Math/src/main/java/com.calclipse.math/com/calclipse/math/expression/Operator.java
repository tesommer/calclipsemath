package com.calclipse.math.expression;

/**
 * A binary or unary operator.
 * 
 * @author Tone Sommerland
 */
public class Operator extends Token
{
    final int priority;
    
    Operator(
            final TokenType type,
            final Id id,
            final String name,
            final int priority)
    {
        super(type, id, name);
        assert type.isOperator();
        this.priority = priority;
    }
    
    /**
     * Returns the priority of this operator.
     * The relative priorities of operators
     * determine the order in which they are evaluated.
     */
    public final int priority()
    {
        return priority;
    }
    
}
