package com.calclipse.math;

/**
 * A complex number.
 * 
 * @author Tone Sommerland
 */
public final class Complex
{
    /**
     * The square root of -1.
     */
    public static final Complex I = new Complex(0, 1);
    
    private final double re;
    private final double im;
    
    private Complex(final double real, final double imaginary)
    {
        this.re = real;
        this.im = imaginary;
    }
    
    /**
     * Returns a complex number instance.
     * @param real the real part
     * @param imaginary the imaginary part
     */
    public static Complex valueOf(final double real, final double imaginary)
    {
        return new Complex(real, imaginary);
    }
    
    /**
     * The real part of this complex number.
     */
    public double real()
    {
        return re;
    }
    
    /**
     * The imaginary part of this complex number.
     */
    public double imaginary()
    {
        return im;
    }
    
    /**
     * The absolute value of this complex number.
     */
    public double abs()
    {
        return Math.sqrt(re * re + im * im);
    }
    
    /**
     * The complex conjugate of this complex number.
     */
    public Complex conjugate()
    {
        return new Complex(re, -im);
    }
    
    /**
     * The negative of this complex number.
     */
    public Complex negated()
    {
        return new Complex(-re, -im);
    }
    
    /**
     * Returns {@code this + d}.
     */
    public Complex plus(final double d)
    {
        return new Complex(re + d, im);
    }
    
    /**
     * Returns {@code this + c}.
     */
    public Complex plus(final Complex c)
    {
        return new Complex(re + c.re, im + c.im);
    }
    
    /**
     * Returns {@code this - d}.
     */
    public Complex minus(final double d)
    {
        return new Complex(re - d, im);
    }
    
    /**
     * Returns {@code this - c}.
     */
    public Complex minus(final Complex c)
    {
        return new Complex(re - c.re, im - c.im);
    }
    
    /**
     * Returns {@code this * d}.
     */
    public Complex times(final double d)
    {
        return new Complex(re * d, im * d);
    }
    
    /**
     * Returns {@code this * c}.
     */
    public Complex times(final Complex c)
    {
        return new Complex(re * c.re - im * c.im, re * c.im + im * c.re);
    }
    
    /**
     * Returns {@code this / d}.
     */
    public Complex dividedBy(final double d)
    {
        return new Complex(re / d, im / d);
    }
    
    /**
     * Returns {@code this / c}.
     */
    public Complex dividedBy(final Complex c)
    {
        return times(c.conjugate()).dividedBy(c.re * c.re + c.im * c.im);
    }
    
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Complex)
        {
            final var complex = (Complex)obj;
            return
                       Double.compare(re, complex.re) == 0
                    && Double.compare(im, complex.im) == 0;
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return 31 * Double.hashCode(re) + Double.hashCode(im);
    }
    
    @Override
    public String toString()
    {
        return "(" + re + ", " + im + ')';
    }

}
