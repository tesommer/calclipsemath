package com.calclipse.math.parser.config.internal;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.parser.type.Association;

/**
 * A standard, immutable association-implementation.
 * 
 * @author Tone Sommerland
 */
public final class StandardAssociation implements Association
{
    private final Object key;
    private final Object value;
    
    public StandardAssociation(final Object key, final Object value)
    {
        this.key = requireNonNull(key, "key");
        this.value = requireNonNull(value, "value");
    }

    @Override
    public Object getKey()
    {
        return key;
    }

    @Override
    public Object getValue()
    {
        return value;
    }

    /**
     * Unsupported operation.
     * @throws UnsupportedOperationException when called
     */
    @Override
    public Object setValue(final Object value)
    {
        throw new UnsupportedOperationException("setValue(" + value + ')');
    }

    @Override
    public boolean equals(final Object obj)
    {
        return Association.equals(this, obj);
    }

    @Override
    public int hashCode()
    {
        return Association.hashCode(this);
    }

    @Override
    public String toString()
    {
        return Stringification.stringify(this);
    }

}
