package com.calclipse.math.expression.operation.internal;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.Value;

public final class ValueAsConstant implements Value
{
    private final Value value;

    public ValueAsConstant(final Value value)
    {
        this.value = requireNonNull(value, "value");
    }

    @Override
    public Object get()
    {
        return value.get();
    }

    @Override
    public void set(final Object value) throws ErrorMessage
    {
        Value.reject(value);
    }

    @Override
    public boolean isConstant()
    {
        return true;
    }

    @Override
    public Value asConstant()
    {
        return this;
    }

    @Override
    public String toString()
    {
        return ValueAsConstant.class.getSimpleName() + ": " + value;
    }

}
