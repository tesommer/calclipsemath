package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;

/**
 * An operator with arity 2.
 * This class delegates
 * {@link #evaluate(Value, Value)} to its internal
 * {@link #operation() operation}.
 * 
 * @author Tone Sommerland
 */
public final class BinaryOperator extends Operator implements BinaryOperation
{
    private final BinaryOperation operation;

    private BinaryOperator(
            final Id id,
            final String name,
            final int priority,
            final BinaryOperation operation)
    {
        super(TokenType.BOPERATOR, id, name, priority);
        this.operation = requireNonNull(operation, "operation");
    }
    
    /**
     * Returns an instance with the given name, priority and operation.
     * @param name the operator name
     * @param priority the operator precedence
     * @param operation the calculation logic
     * @throws IllegalArgumentException if the name is empty
     */
    public static BinaryOperator of(
            final String name,
            final int priority,
            final BinaryOperation operation)
    {
        return new BinaryOperator(Id.unique(), name, priority, operation);
    }

    /**
     * The operation of this operator.
     */
    public BinaryOperation operation()
    {
        return operation;
    }
    
    @Override
    public BinaryOperator withId(final Id newId)
    {
        return new BinaryOperator(newId, name, priority, operation);
    }

    @Override
    public BinaryOperator withName(final String newName)
    {
        return new BinaryOperator(id, newName, priority, operation);
    }
    
    /**
     * Returns an operator similar to this, but with the given priority.
     * @param newPriority the new priority
     */
    public BinaryOperator withPriority(final int newPriority)
    {
        return new BinaryOperator(id, name, newPriority, operation);
    }

    /**
     * Returns an operator similar to this, but with the given operation.
     * @param newOperation the new operation
     */
    public BinaryOperator withOperation(final BinaryOperation newOperation)
    {
        return new BinaryOperator(id, name, priority, newOperation);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        return operation.evaluate(left, right);
    }

}
