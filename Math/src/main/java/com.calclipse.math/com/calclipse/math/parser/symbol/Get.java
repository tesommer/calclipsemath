package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.AssociativeArray;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Multi-index accessor.
 * Supported types:
 * <ul>
 *  <li>indexable, integer[, ..., integer]</li>
 *  <li>associative array, ?</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Get implements VariableArityOperation
{
    public static final String NAME = "get";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Get(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Get(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(2, args.length);
        if (args[0].get() instanceof AssociativeArray)
        {
            return evaluateAssociativeArray(args);
        }
        final var indices = new int[args.length - 1];
        for (int i = 0; i < indices.length; i++)
        {
            indices[i] = TypeUtil.argToInt(i + 2, args[i + 1].get());
        }
        return typeContext.indexer().get(args[0], indices);
    }
    
    private static Value evaluateAssociativeArray(final Value[] args)
            throws ErrorMessage
    {
        ArgUtil.requireArgCount(2, args.length);
        return evaluateAssociativeArray(args[0], args[1]);
    }
    
    /**
     * Assumes that the first argument is an instance of
     * {@link com.calclipse.math.parser.type.AssociativeArray}.
     */
    static Value evaluateAssociativeArray(
            final Value first, final Value second)
    {
        final var aarr = (AssociativeArray)first.get();
        final Object key = second.get();
        if (first.isConstant())
        {
            final Object value = aarr.get(key);
            return value == null ? Values.UNDEF : Value.constantOf(value);
        }
        return new AssociationValue(aarr, key);
    }
    
    private static final class AssociationValue implements Value
    {
        private final AssociativeArray aarr;
        private final Object key;

        private AssociationValue(
                final AssociativeArray aarr, final Object key)
        {
            assert aarr != null;
            assert key != null;
            this.aarr = aarr;
            this.key = key;
        }

        @Override
        public Object get()
        {
            final Object value = aarr.get(key);
            return value == null ? Values.UNDEF.get() : value;
        }

        @Override
        public void set(final Object value) throws ErrorMessage
        {
            if (value.equals(Values.UNDEF.get()))
            {
                aarr.remove(key);
            }
            else
            {
                aarr.put(key, value);
            }
        }

        @Override
        public boolean isConstant()
        {
            return false;
        }
        
    }

}
