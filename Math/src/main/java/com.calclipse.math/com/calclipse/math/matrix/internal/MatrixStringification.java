package com.calclipse.math.matrix.internal;

import java.util.function.DoubleFunction;

import com.calclipse.math.matrix.Matrix;

/**
 * The default matrix string format.
 * 
 * @author Tone Sommerland
 */
public final class MatrixStringification
{
    private static final String EOL = "\n";
    private static final String RBRAC = "]";
    
    private MatrixStringification()
    {
    }
    
    /**
     * Makes a string representation of a matrix.
     * @param entryStringifier the string representation of an element
     */
    public static String stringify(
            final Matrix matrix,
            final DoubleFunction<String> entryStringifier)
    {
        if (matrix.rows() <= 0 || matrix.columns() <= 0)
        {
            return "[[]]";
        }
        
        final String[][] strElems
            = stringifyAndPadElements(matrix, entryStringifier);
        
        final var buffer = new StringBuilder();
        for (int i = 0; i < matrix.rows(); i++)
        {
            buffer.append(" [");
            for (int j = 0; j < matrix.columns(); j++)
            {
                buffer.append(strElems[i][j]);                
            }
            buffer.append(RBRAC).append(EOL);
        }
        
        return '[' + buffer.substring(1, buffer.length() - 1) + RBRAC;
    }
    
    private static String[][] stringifyAndPadElements(
            final Matrix matrix,
            final DoubleFunction<String> entryStringifier)
    {
        final var strElems = new String[matrix.rows()][matrix.columns()];
        
        for (int i = 0; i < matrix.columns(); i++)
        {
            int maxLength = 0;
            
            for (int j = 0; j < matrix.rows(); j++)
            {
                strElems[j][i] = entryStringifier.apply(matrix.get(j, i));
                maxLength = Math.max(maxLength, strElems[j][i].length());
            }
            
            if (i < matrix.columns() - 1)
            {
                maxLength++;
            }
            
            padColumn(strElems, i, maxLength);
        }
        return strElems;
    }

    private static void padColumn(
            final String[][] strElems,
            final int col,
            final int maxLength)
    {
        final String format = "%-" + maxLength + 's';
        for (int i = 0; i < strElems.length; i++)
        {
            strElems[i][col] = String.format(format, strElems[i][col]);
        }
    }

}
