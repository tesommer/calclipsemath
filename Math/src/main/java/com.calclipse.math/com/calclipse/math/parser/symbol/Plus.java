package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.AssociativeArray;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Plus (addition).
 * Supported types:
 * <ul>
 *  <li>fraction, fraction</li>
 *  <li>complex|real, complex|real</li>
 *  <li>matrix, matrix</li>
 *  <li>array, array: concatenation</li>
 *  <li>associative array, associative array: concatenation</li>
 *  <li>string|?, string|?:
 *  concatenation (if at least one arg is a string)</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Plus implements BinaryOperation
{
    public static final String NAME = "+";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private Plus(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.SUMMATION, new Plus(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        
        if (o1 instanceof Fraction && o2 instanceof Fraction)
        {
            return evalFractionFraction(o1, o2);
            
        }
        else if (o1 instanceof Complex && o2 instanceof Complex)
        {
            return evalComplexComplex(o1, o2);
            
        }
        else if (o1 instanceof Complex && o2 instanceof Number)
        {
            return evalComplexNumber(o1, o2);
            
        }
        else if (o1 instanceof Number && o2 instanceof Complex)
        {
            return evalNumberComplex(o1, o2);
            
        }
        else if (o1 instanceof Number && o2 instanceof Number)
        {
            return evalNumberNumber(typeContext, o1, o2);
            
        }
        else if (o1 instanceof Matrix && o2 instanceof Matrix)
        {
            return evalMatrixMatrix(o1, o2);
            
        }
        else if (o1 instanceof Array && o2 instanceof Array)
        {
            return evalArrayArray(typeContext, o1, o2);
            
        }
        else if (o1 instanceof AssociativeArray
                && o2 instanceof AssociativeArray)
        {
            return evalAssociativeArrayAssociativeArray(typeContext, o1, o2);
            
        }
        else if (o1 instanceof String || o2 instanceof String)
        {
            return Value.constantOf(o1.toString() + o2.toString());
        }
        
        throw errorMessage(TypeMessages.incompatibleTypes(o1, o2));
    }

    private static Value evalFractionFraction(
            final Object o1, final Object o2)
    {
        final var f1 = (Fraction)o1;
        final var f2 = (Fraction)o2;
        return Value.constantOf(f1.plus(f2));
    }

    private static Value evalNumberNumber(
            final TypeContext typeContext, final Object o1, final Object o2)
    {
        final var n1 = (Number)o1;
        final var n2 = (Number)o2;
        return Value.constantOf(typeContext.numberType().valueOf(
                n1.doubleValue() + n2.doubleValue()));
    }

    private static Value evalComplexComplex(final Object o1, final Object o2)
    {
        final var c1 = (Complex)o1;
        final var c2 = (Complex)o2;
        return Value.constantOf(c1.plus(c2));
    }

    private static Value evalComplexNumber(final Object o1, final Object o2)
    {
        final var c1 = (Complex)o1;
        final var n2 = (Number)o2;
        return Value.constantOf(c1.plus(n2.doubleValue()));
    }

    private static Value evalNumberComplex(final Object o1, final Object o2)
    {
        final var n1 = (Number)o1;
        final var c2 = (Complex)o2;
        return Value.constantOf(c2.plus(n1.doubleValue()));
    }

    private static Value evalMatrixMatrix(final Object o1, final Object o2)
            throws ErrorMessage
    {
        final var m1 = (Matrix)o1;
        final var m2 = (Matrix)o2;
        try
        {
            return Value.constantOf(m1.plus(m2));
        }
        catch (final SizeViolation ex)
        {
            throw errorMessage(EvalMessages.mismatchingDimensions())
                .withCause(ex);
        }
    }

    private static Value evalArrayArray(
            final TypeContext typeContext, final Object o1, final Object o2)
    {
        final var a1 = (Array)o1;
        final var a2 = (Array)o2;
        final var both = typeContext.arrayType().createArray(a1);
        both.addAll(a2);
        return Value.constantOf(both);
    }
    
    private static Value evalAssociativeArrayAssociativeArray(
            final TypeContext typeContext, final Object o1, final Object o2)
    {
        final var aa1 = (AssociativeArray)o1;
        final var aa2 = (AssociativeArray)o2;
        final var both = typeContext.associativeArrayType()
                .createAssociativeArray(aa1);
        aa2.entrySet().forEach(
                entry -> both.put(entry.getKey(), entry.getValue()));
        return Value.constantOf(both);
    }

}
