package com.calclipse.math.matrix.function;

/**
 * A function that accepts a row index and a column index,
 * and returns a matrix element.
 * 
 * @see com.calclipse.math.matrix.Matrix
 * #applyRowColumnFunction(RowColumnFunction)
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface RowColumnFunction
{
    /**
     * Provides the value for the element at the given row and column.
     * @param row the row index
     * @param column the column index
     */
    public abstract double apply(int row, int column);

}
