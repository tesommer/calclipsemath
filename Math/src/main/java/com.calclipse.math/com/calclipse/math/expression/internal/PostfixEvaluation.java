package com.calclipse.math.expression.internal;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

import com.calclipse.math.expression.BadExpression;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Return;
import com.calclipse.math.expression.TokenType;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;

/**
 * This class contains a static method that evaluates a
 * {@link com.calclipse.math.expression.Expression parsed expression}
 * on RPN form.
 * 
 * @author Tone Sommerland
 */
public final class PostfixEvaluation
{
    private PostfixEvaluation()
    {
    }

    /**
     * The default
     * {@link com.calclipse.math.expression.Expression#evaluate(boolean)
     * expression evaluation method}.
     * @return {@link com.calclipse.math.expression.Values#UNDEF}
     * if passed an empty expression
     */
    public static Value evaluate(
            final Expression expression,
            final boolean interceptReturn) throws ErrorMessage
    {
        if (interceptReturn)
        {
            try
            {
                return evaluate(expression, false);
            }
            catch (final Return ex)
            {
                return ex.value();
            }
        }
        if (expression.isEmpty())
        {
            return Values.UNDEF;
        }
        return evaluateNonEmptyExpression(expression);
    }

    private static Value evaluateNonEmptyExpression(
            final Expression expression) throws ErrorMessage
    {
        final var stack = new ArrayDeque<Value>();
        for (final Fragment frag : expression)
        {
            evaluate(frag, stack);
        }
        if (stack.size() != 1)
        {
            throw badExpression(expression);
        }
        return pop(stack);
    }

    private static void evaluate(
            final Fragment frag,
            final Deque<Value> stack) throws ErrorMessage
    {
        final TokenType type = frag.token().type();
        if (type.isOperand())
        {
            stack.add((Value)frag.token());
        }
        else if (type.isFunction())
        {
            evaluateFunction(frag, stack);
        }
        else if (type.isBinary())
        {
            evaluateBinaryOperator(frag, stack);
        }
        else if (type.isUnary())
        {
            evaluateUnaryOperator(frag, stack);
        }
        else
        {
            throw badExpression(frag);
        }
    }
    
    private static void evaluateFunction(
            final Fragment frag,
            final Deque<? super Value> stack) throws ErrorMessage
    {
        final var func = (Function)frag.token();
        if (func.isConditional())
        {
            evaluateConditional(func, stack, frag);
        }
        else
        {
            evaluateVariableArity(func, stack, frag);
        }
    }

    private static void evaluateConditional(
            final Function func,
            final Deque<? super Value> stack,
            final Fragment detail) throws ErrorMessage
    {
        try
        {
            stack.add(func.conditional().evaluate(func.arguments()));
        }
        catch (final ErrorMessage ex)
        {
            throw attachIfAbsent(detail, ex);
        }
    }

    private static void evaluateVariableArity(
            final Function func,
            final Deque<? super Value> stack,
            final Fragment detail) throws ErrorMessage
    {
        final List<Expression> arguments = func.arguments();
        final var args = new Value[arguments.size()];
        for (int i = 0; i < args.length; i++)
        {
            args[i] = evaluate(arguments.get(i), false);
        }
        try
        {
            stack.add(func.operation().evaluate(args));
        }
        catch (final ErrorMessage ex)
        {
            throw ex.attach(detail);
        }
    }
    
    private static void evaluateBinaryOperator(
            final Fragment frag,
            final Deque<Value> stack) throws ErrorMessage
    {
        final var op = (BinaryOperation)frag.token();
        final Value right = pop(stack);
        final Value left = pop(stack);
        try
        {
            stack.add(op.evaluate(left, right));
        }
        catch (final ErrorMessage ex)
        {
            throw ex.attach(frag);
        }
    }
    
    private static void evaluateUnaryOperator(
            final Fragment frag,
            final Deque<Value> stack) throws ErrorMessage
    {
        final var op = (UnaryOperation)frag.token();
        final Value arg = pop(stack);
        try
        {
            stack.add(op.evaluate(arg));
        }
        catch (final ErrorMessage ex)
        {
            throw ex.attach(frag);
        }
    }
    
    private static ErrorMessage attachIfAbsent(
            final Fragment detail, final ErrorMessage ex)
    {
        return ex.detail()
            .map(existing -> ex)
            .orElse(ex.attach(detail));
    }
    
    private static Value pop(final Deque<? extends Value> stack)
    {
        if (stack.isEmpty())
        {
            throw badExpression("Empty stack.");
        }
        return stack.removeLast();
    }
    
    private static BadExpression badExpression(final Object message)
    {
        return new BadExpression(message.toString());
    }

}
