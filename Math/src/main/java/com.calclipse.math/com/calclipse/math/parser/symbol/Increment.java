package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * Increment and assign (similar to +=).
 * Adds the right operand to the left
 * and stores the result in the left operand.
 * Returns the left operand.
 * The addition is performed by the operation
 * given to the constructor of this class.
 * 
 * @author Tone Sommerland
 */
public final class Increment implements BinaryOperation
{
    public static final String NAME = "inc";
    
    private static final Id ID = Id.unique();
    
    private final BinaryOperation addition;
    
    private Increment(final BinaryOperation addition)
    {
        this.addition = requireNonNull(addition, "addition");
    }
    
    public static BinaryOperator operator(final BinaryOperation addition)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.ASSIGNMENT, new Increment(addition))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Value result = addition.evaluate(left, right);
        left.set(result.get());
        return left;
    }

}
