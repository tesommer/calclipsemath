package com.calclipse.math.parser.scanner;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Queue;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Scanner;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;

/**
 * An undefined-symbol defragmenter.
 * Undefined symbols may be broken up by smaller recognized symbols.
 * This scanner acts as a filter that fuses some of these fragments together
 * to form longer, nicer looking undefined symbols.
 * 
 * @author Tone Sommerland
 */
public final class Defragmenter implements Scanner
{
    private final Queue<Fragment> buffer = new ArrayDeque<>();
    private final Scanner source;

    /**
     * Creates a new defragmenter.
     * @param source the source of the fragments
     */
    public Defragmenter(final Scanner source)
    {
        this.source = requireNonNull(source, "source");
    }

    @Override
    public void reset(final String expression) throws ErrorMessage
    {
        source.reset(expression);
    }

    @Override
    public boolean hasNext() throws ErrorMessage
    {
        return buffer.isEmpty() ? updateBuffer() : true;
    }

    @Override
    public Fragment next() throws ErrorMessage
    {
        return buffer.remove();
    }
    
    private boolean updateBuffer() throws ErrorMessage
    {
        if (!source.hasNext())
        {
            return false;
        }
        final var quarantine = new ArrayDeque<Fragment>();
        boolean awaitingNext;
        do
        {
            final Fragment next = source.next();
            if (next.token().type().isUndefined())
            {
                awaitingNext = processNextUndefined(quarantine, next);
            }
            else
            {
                awaitingNext = processNextDefined(quarantine, next);
            }
        }
        while (awaitingNext && source.hasNext());
        if (awaitingNext)
        {
            flush(quarantine);
        }
        return true;
    }

    private boolean processNextUndefined(
            final Deque<Fragment> quarantine, final Fragment next)
    {
        final boolean endsWithWS = endsWithWhitespace(next);
        final List<Fragment> chopped = chopWhitespace(next);
        if (endsWithWhitespace(chopped.get(0)))
        {
            // next consists of only whitespace.
            flush(quarantine, next);
            return false;
        }
        else if (areAdjacent(quarantine, chopped.get(0)))
        {
            confine(quarantine, chopped.remove(0));
            if (chopped.isEmpty() && !endsWithWS)
            {
                return true;
            }
        }
        if (endsWithWS)
        {
            flush(quarantine, chopped);
            return false;
        }
        flush(quarantine, chopped.subList(0, chopped.size() - 1));
        quarantine.add(chopped.get(chopped.size() - 1));
        return true;
    }
    
    private boolean processNextDefined(
            final Deque<Fragment> quarantine, final Fragment next)
    {
        if (containsNonAlphanumeric(next))
        {
            flush(quarantine, next);
            return false;
        }
        else if (areAdjacent(quarantine, next))
        {
            confine(quarantine, next);
        }
        else
        {
            flush(quarantine);
            quarantine.add(next);
        }
        return true;
    }
    
    private void flush(final Collection<Fragment> quarantine)
    {
        buffer.addAll(quarantine);
        quarantine.clear();
    }
    
    private void flush(
            final Collection<Fragment> quarantine, final Fragment next)
    {
        flush(quarantine);
        buffer.add(next);
    }
    
    private void flush(
            final Collection<Fragment> quarantine,
            final Collection<Fragment> next)
    {
        flush(quarantine);
        buffer.addAll(next);
    }
    
    private static void confine(
            final Queue<Fragment> quarantine, final Fragment frag)
    {
        final boolean infected
            = frag.token().type().isUndefined()
            ||    (quarantine.size() == 1
                && quarantine.peek().token().type().isUndefined());
        quarantine.add(frag);
        if (infected)
        {
            defragment(quarantine);
        }
    }

    private static void defragment(final Queue<Fragment> quarantine)
    {
        final String unifiedName = quarantine.stream()
                .map(Fragment::token)
                .map(Token::name)
                .collect(joining());
        final int unifiedPosition = quarantine.peek().position();
        quarantine.clear();
        quarantine.add(Fragment.of(
                Tokens.undefined(unifiedName), unifiedPosition));
    }
    
    /**
     * Chops a fragment containing whitespace characters
     * into sub-fragments without whitespace characters.
     * The returned list will have at least one element, possibly the original.
     * If the given fragment consists of only white space,
     * it will be left as it is and returned.
     */
    private static List<Fragment> chopWhitespace(final Fragment frag)
    {
        final var chopped = new ArrayList<Fragment>();
        final String name = frag.token().name();
        int i = nextNonWSOrLength(name, 0);
        if (i == name.length())
        {
            return List.of(frag);
        }
        int j = i + 1;
        for (; j < name.length(); j++)
        {
            if (Character.isWhitespace(name.charAt(j)))
            {
                addUndefined(
                        chopped, name.substring(i, j), frag.position() + i);
                j = nextNonWSOrLength(name, j + 1);
                i = j;
            }
        }
        if (i < name.length())
        {
            final String lastSub = name.substring(i, j);
            addUndefined(chopped, lastSub, frag.position() + i);
        }
        return chopped;
    }
    
    /**
     * fromIndex must be >= 0 and <= name.length().
     */
    private static int nextNonWSOrLength(
            final String name, final int fromIndex)
    {
        int index = fromIndex;
        while (index < name.length()
                && Character.isWhitespace(name.charAt(index)))
        {
            index++;
        }
        return index;
    }
    
    private static void addUndefined(
            final List<? super Fragment> addTo,
            final String name,
            final int position)
    {
        addTo.add(Fragment.of(Tokens.undefined(name), position));
    }
    
    private static boolean endsWithWhitespace(final Fragment frag)
    {
        final String name = frag.token().name();
        return Character.isWhitespace(name.charAt(name.length() - 1));
    }
    
    private static boolean containsNonAlphanumeric(final Fragment frag)
    {
        final String name = frag.token().name();
        for (int i = 0; i < name.length(); i++)
        {
            if (!Character.isLetterOrDigit(name.charAt(i)))
            {
                return true;
            }
        }
        return false;
    }
    
    private static boolean areAdjacent(
            final Deque<Fragment> before, final Fragment after)
    {
        if (before.isEmpty())
        {
            return true;
        }
        return areAdjacent(before.peekLast(), after);
    }
    
    private static boolean areAdjacent(
            final Fragment before, final Fragment after)
    {
        return before.position() + before.token().name().length()
                == after.position();
    }

}
