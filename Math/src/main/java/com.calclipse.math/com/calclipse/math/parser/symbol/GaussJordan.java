package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.misc.MatrixUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Reduces a matrix to reduced echelon form.
 * Accepts the matrix to reduce,
 * a boolean flag specifying whether to use partial pivoting,
 * and an augmentation matrix.
 * The two last arguments are optional.
 * Results in an array containing
 * the reduced echelon matrix,
 * and the augmentation after row operations (if supplied).
 * 
 * @author Tone Sommerland
 */
public final class GaussJordan implements VariableArityOperation
{
    public static final String NAME = "gaussjordan";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private GaussJordan(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new GaussJordan(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final var params = new Gauss.ReductionParameters(typeContext, args);
        final Matrix coeffCopy = params.coefficients().copy();
        final Matrix aug = params.augmentation();
        final Matrix augCopy = aug == null ? null : aug.copy();
        if (augCopy == null)
        {
            MatrixUtil.gaussJordan(coeffCopy, params.isPivoting());
        }
        else
        {
            try
            {
                MatrixUtil.gaussJordan(
                        coeffCopy, augCopy, params.isPivoting());
            }
            catch (final SizeViolation ex)
            {
                throw errorMessage(EvalMessages.mismatchingDimensions())
                    .withCause(ex);
            }
        }
        final var arr = typeContext.arrayType().arrayOf(coeffCopy);
        if (augCopy != null)
        {
            arr.add(augCopy);
        }
        return Value.constantOf(arr);
    }

}
