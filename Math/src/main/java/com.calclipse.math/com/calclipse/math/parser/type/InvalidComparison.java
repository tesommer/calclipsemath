package com.calclipse.math.parser.type;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;

/**
 * Thrown by the comparator returned from
 * {@link Comparer#asComparator()}
 * if a comparison caused an
 * {@link com.calclipse.math.expression.ErrorMessage}.
 * The {@link com.calclipse.math.expression.ErrorMessage}
 * is accessible as the cause of this exception.
 * 
 * @author Tone Sommerland
 */
public class InvalidComparison extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * Creates an invalid comparison with the given cause.
     * The cause may not be {@code null}.
     */
    public InvalidComparison(final ErrorMessage cause)
    {
        super(requireNonNull(cause, "cause"));
    }

    @Override
    public ErrorMessage getCause()
    {
        return (ErrorMessage)super.getCause();
    }

}
