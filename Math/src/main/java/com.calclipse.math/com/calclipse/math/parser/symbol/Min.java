package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.util.Collection;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.type.InvalidComparison;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Returns the minimum of its arguments.
 * Arguments containing arrays are treated
 * as collections of objects to compare.
 * Returns undef if there are no objects to compare.
 * Supports:
 * <ul>
 * <li>[array|*[, ..., array|*]]</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Min implements VariableArityOperation
{
    public static final String NAME = "min";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Min(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Min(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final Collection<Object> objects = Max.collect(args);
        if (objects.isEmpty())
        {
            return Values.UNDEF;
        }
        try
        {
            final Object result = objects.stream()
                    .min(typeContext.comparer().asComparator()).get();
            return Value.constantOf(result);
        }
        catch (final InvalidComparison ex)
        {
            throw ex.getCause();
        }
    }

}
