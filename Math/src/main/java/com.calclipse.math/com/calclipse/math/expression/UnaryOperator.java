package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;

/**
 * An operator with arity 1.
 * This class delegates
 * {@link #evaluate(Value)} to its internal
 * {@link #operation() operation}.
 * 
 * @author Tone Sommerland
 */
public final class UnaryOperator extends Operator implements UnaryOperation
{
    private final UnaryOperation operation;

    private UnaryOperator(
            final TokenType type,
            final Id id,
            final String name,
            final int priority,
            final UnaryOperation operation)
    {
        super(type, id, name, priority);
        this.operation = requireNonNull(operation, "operation");
    }
    
    /**
     * Returns a left unary (prefixed) operator
     * with the given name, priority and operation.
     * @param name the operator name
     * @param priority the operator precedence
     * @param operation the calculation logic
     * @throws IllegalArgumentException if the name is empty
     */
    public static UnaryOperator prefixed(
            final String name,
            final int priority,
            final UnaryOperation operation)
    {
        return new UnaryOperator(
                TokenType.LUOPERATOR,
                Id.unique(),
                name,
                priority,
                operation);
    }
    
    /**
     * Returns a right unary (postfixed) operator
     * with the given name, priority and operation.
     * @param name the operator name
     * @param priority the operator precedence
     * @param operation the calculation logic
     * @throws IllegalArgumentException if the name is empty
     */
    public static UnaryOperator postfixed(
            final String name,
            final int priority,
            final UnaryOperation operation)
    {
        return new UnaryOperator(
                TokenType.RUOPERATOR,
                Id.unique(),
                name,
                priority,
                operation);
    }

    /**
     * The operation of this operator.
     */
    public UnaryOperation operation()
    {
        return operation;
    }
    
    @Override
    public UnaryOperator withId(final Id newId)
    {
        return new UnaryOperator(type, newId, name, priority, operation);
    }
    
    @Override
    public UnaryOperator withName(final String newName)
    {
        return new UnaryOperator(type, id, newName, priority, operation);
    }
    
    /**
     * Returns an operator similar to this, but with the given priority.
     * @param newPriority the new priority
     */
    public UnaryOperator withPriority(final int newPriority)
    {
        return new UnaryOperator(type, id, name, newPriority, operation);
    }
    
    /**
     * Returns an operator similar to this, but with the given operation.
     * @param newOperation the new operation
     */
    public UnaryOperator withOperation(final UnaryOperation newOperation)
    {
        return new UnaryOperator(type, id, name, priority, newOperation);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        return operation.evaluate(arg);
    }

}
