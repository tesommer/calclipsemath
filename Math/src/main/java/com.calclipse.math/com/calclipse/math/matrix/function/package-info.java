/**
 * Functional interfaces representing methods
 * for populating or processing matrix elements.
 */
package com.calclipse.math.matrix.function;
