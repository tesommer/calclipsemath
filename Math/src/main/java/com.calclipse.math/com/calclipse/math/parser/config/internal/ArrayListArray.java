package com.calclipse.math.parser.config.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.calclipse.math.parser.type.Array;

/**
 * An array implementation based on {@code ArrayList<Object>}.
 * 
 * @author Tone Sommerland
 */
public final class ArrayListArray
    extends ArrayList<Object> implements Array
{
    private static final long serialVersionUID = 1L;

    public ArrayListArray(final Collection<?> c)
    {
        super(c);
    }

    @Override
    public String toString()
    {
        return Stringification.stringify(this);
    }

}
