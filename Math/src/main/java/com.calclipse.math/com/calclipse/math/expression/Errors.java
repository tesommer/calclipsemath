package com.calclipse.math.expression;

/**
 * A utility class for making error messages.
 * 
 * @author Tone Sommerland
 */
public final class Errors
{

    private Errors()
    {
    }

    /**
     * Returns an error message with the given message.
     */
    public static ErrorMessage errorMessage(final String message)
    {
        return ErrorMessage.errorMessageOf(message);
    }

}
