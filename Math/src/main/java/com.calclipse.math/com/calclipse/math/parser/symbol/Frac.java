package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import java.math.BigInteger;

import com.calclipse.math.Fraction;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.TypeUtil;

/**
 * Fraction.
 * Supported types:
 * <ul>
 * <li>real: tries to convert the real number to a fraction</li>
 * <li>integer, integer:
 * the numerator and denominator of the fraction to create</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Frac implements VariableArityOperation
{
    public static final String NAME = "frac";
    
    private static final Id ID = Id.unique();
    
    private static final  VariableArityOperation INSTANCE = new Frac();

    private Frac()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(1, 2, args.length);
        
        if (args.length == 1)
        {
            final double d = TypeUtil.toDouble(args[0].get());
            try
            {
                final Fraction frac = MathUtil.toFraction(d, 30, .00001)
                        .orElseThrow(() -> errorMessage(
                                EvalMessages.irrationalNumber()));
                return Value.constantOf(frac);
            }
            catch (final ArithmeticException ex)
            {
                throw errorMessage(
                        EvalMessages.divisionByZero()).withCause(ex);
            }
            
        }
        else
        {
            final long i1 = TypeUtil.argToLong(1, args[0].get());
            final long i2 = TypeUtil.argToLong(2, args[1].get());
            final BigInteger num = BigInteger.valueOf(i1);
            final BigInteger den = BigInteger.valueOf(i2);
            try
            {
                return Value.constantOf(Fraction.valueOf(num, den));
            }
            catch (final ArithmeticException ex)
            {
                throw errorMessage(EvalMessages.divisionByZero())
                    .withCause(ex);
            }
            
        }
    }

}
