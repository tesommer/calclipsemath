package com.calclipse.math.parser.type;

import java.util.List;

/**
 * An object-array data-type.
 * 
 * @author Tone Sommerland
 */
public interface Array extends List<Object>
{
}
