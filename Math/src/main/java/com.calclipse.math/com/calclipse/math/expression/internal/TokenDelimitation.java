package com.calclipse.math.expression.internal;

import static java.util.Objects.requireNonNull;

import java.util.function.BiPredicate;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.Delimiters;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Token;

/**
 * A stateless and immutable delimitation that uses tokens as delimiters.
 * 
 * @author Tone Sommerland
 */
public final class TokenDelimitation extends Delimitation
{
    private final Token opener;
    private final Token closer;
    private final Token separator;
    private final BiPredicate<? super Token, ? super Token> reader;
    
    private TokenDelimitation(
            final Token opener,
            final Token closer,
            final Token separator,
            final BiPredicate<? super Token, ? super Token> reader)
    {
        this.opener = opener;
        this.closer = requireNonNull(closer, "closer");
        this.separator = requireNonNull(separator, "separator");
        this.reader = requireNonNull(reader, "reader");
    }
    
    public static Delimitation withOpener(
            final Token opener,
            final Token closer,
            final Token separator,
            final BiPredicate<? super Token, ? super Token> reader)
    {
        return new TokenDelimitation(
                requireNonNull(opener, "opener"),
                closer,
                separator,
                reader);
    }
    
    public static Delimitation withoutOpener(
            final Token closer,
            final Token separator,
            final BiPredicate<? super Token, ? super Token> reader)
    {
        return new TokenDelimitation(null, closer, separator, reader);
    }

    @Override
    protected boolean hasOpener()
    {
        return opener != null;
    }

    @Override
    protected boolean readsCloser(final Token token)
    {
        return reader.test(closer, token);
    }

    @Override
    protected boolean readsOpener(final Token token)
    {
        return opener == null ? false : reader.test(opener, token);
    }

    @Override
    protected boolean readsSeparator(final Token token)
    {
        return reader.test(separator, token);
    }

    @Override
    protected Delimiters delimiters()
    {
        if (opener == null)
        {
            return Delimiters.withoutOpener(
                    closer.name(), separator.name());
        }
        return Delimiters.withOpener(
                opener.name(), closer.name(), separator.name());
    }

    @Override
    protected Delimitation spawn() {
        return this;
    }

    @Override
    protected Function postSpawn(
            final Function func, final Delimitation spawnedDelim) {
        
        return func;
    }

}
