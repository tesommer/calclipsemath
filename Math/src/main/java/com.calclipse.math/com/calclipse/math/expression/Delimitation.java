package com.calclipse.math.expression;

import java.util.function.BiPredicate;

import com.calclipse.math.expression.internal.TokenDelimitation;

/**
 * Establishes the boundaries of
 * {@link com.calclipse.math.expression.Function}
 * parameters that are extracted during parsing.
 * A parameter list may have zero or one opener,
 * at least one separator,
 * and one closer.
 * The parameter extraction may alter the state
 * of this object depending on the implementation.
 * 
 * @author Tone Sommerland
 */
public abstract class Delimitation
{
    protected Delimitation()
    {
    }
    
    /**
     * Returns a stateless, immutable delimitation by tokens
     * for parameter lists with an opening delimiter.
     * @param opener the parameter list opener
     * @param closer the parameter list closer
     * @param separator the parameter separator
     * @param reader the method by which delimiters are recognized
     */
    public static Delimitation byTokensWithOpener(
            final Token opener,
            final Token closer,
            final Token separator,
            final BiPredicate<? super Token, ? super Token> reader)
    {
        return TokenDelimitation.withOpener(
                opener,
                closer,
                separator,
                reader);
    }
    
    /**
     * Returns a stateless, immutable delimitation by tokens
     * for parameter lists without an opening delimiter.
     * @param closer the parameter list closer
     * @param separator the parameter separator
     * @param reader the method by which delimiters are recognized
     */
    public static Delimitation byTokensWithoutOpener(
            final Token closer,
            final Token separator,
            final BiPredicate<? super Token, ? super Token> reader)
    {
        return TokenDelimitation.withoutOpener(
                closer,
                separator,
                reader);
    }
    
    /**
     * Whether or not this delimitation has an opener.
     */
    protected abstract boolean hasOpener();
    
    /**
     * Called during parameter processing.
     * @return {@code true} if the given token opens the parameter list
     */
    protected abstract boolean readsOpener(Token token);

    /**
     * Called during parameter processing.
     * @return {@code true} if the given token closes the parameter list
     */
    protected abstract boolean readsCloser(Token token);

    /**
     * Called during parameter processing.
     * @return {@code true} if the given token separates two parameters
     */
    protected abstract boolean readsSeparator(Token token);
    
    /**
     * The delimiters of this delimitation.
     */
    protected abstract Delimiters delimiters();
    
    /**
     * Called during parsing before parameter processing.
     * If parameter processing alters the state of this object,
     * this method should return a new instance with a fresh state.
     */
    protected abstract Delimitation spawn();
    
    /**
     * Called after {@link #spawn()}
     * with the spawned delimitation
     * and the function instance that received it.
     * This method is given a chance to do any final preparation
     * of either object before they are sent on their way.
     */
    protected abstract Function postSpawn(
            Function func, Delimitation spawnedDelim);

}
