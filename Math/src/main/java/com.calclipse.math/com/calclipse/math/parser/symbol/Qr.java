package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.QrFactorization;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * QR decomposition of a matrix.
 * Accepts the matrix to be factorized.
 * Returns an array containing Q and R.
 * 
 * @author Tone Sommerland
 */
public final class Qr implements VariableArityOperation
{
    public static final String NAME = "qr";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private Qr(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Qr(typeContext)).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireArgCount(1, args.length);
        
        final Object o = args[0].get();
        
        if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            final QrFactorization qr = QrFactorization.factorize(mx);
            final var arr = typeContext.arrayType().arrayOf(
                    qr.computeQ(), qr.r().copy());
            return Value.constantOf(arr);
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
