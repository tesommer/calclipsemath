package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.InconsistentSystem;
import com.calclipse.math.parser.misc.MatrixUtil;

/**
 * Solves a system of linear equations.
 * Expects the coefficient matrix and the answer matrix (augmentation).
 * Returns a matrix containing the solution on parametric vector form.
 * Supported types:
 * <ul>
 *  <li>matrix, matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Solve implements VariableArityOperation
{
    public static final String NAME = "solve";
    
    private static final Id ID = Id.unique();
    
    private static final VariableArityOperation INSTANCE = new Solve();

    private Solve()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireArgCount(2, args.length);
        
        final Object o1 = args[0].get();
        final Object o2 = args[1].get();
        
        if ((o1 instanceof Matrix) && (o2 instanceof Matrix))
        {
            final var mx1 = (Matrix)o1;
            final var mx2 = (Matrix)o2;
            
            try
            {
                final Matrix result = MatrixUtil.solve(mx1, mx2, true);
                return Value.constantOf(result);
                
            }
            catch (final SizeViolation ex)
            {
                throw errorMessage(EvalMessages.mismatchingDimensions())
                    .withCause(ex);
                
            }
            catch (final InconsistentSystem ex)
            {
                throw errorMessage(EvalMessages.inconsistentSystem())
                    .withCause(ex);
            }
        }
        
        throw errorMessage(TypeMessages.incompatibleTypes(o1, o2));
    }

}
