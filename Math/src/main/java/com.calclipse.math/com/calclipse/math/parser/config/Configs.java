package com.calclipse.math.parser.config;

import com.calclipse.math.parser.MathParser;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Provides math parsers with various configurations.
 * 
 * @author Tone Sommerland
 */
public final class Configs
{
    private Configs()
    {
    }
    
    /**
     * Returns a math parser with the
     * {@link StandardConfig standard configuration}.
     * A parser returned from this method
     * is configured in such a way that
     * it is safe to access two different parsers
     * (including the expressions they produce) from each own thread.
     */
    public static MathParser standardParser()
    {
        return StandardConfig.parser();
    }
    
    /**
     * Returns a math parser with the
     * {@link ArbitraryPrecisionConfig arbitrary-precision configuration}.
     * A parser returned from this method
     * is configured in such a way that
     * it is safe to access two different parsers
     * (including the expressions they produce) from each own thread.
     */
    public static MathParser arbitraryPrecisionParser()
    {
        return ArbitraryPrecisionConfig.parser();
    }

    /**
     * Returns the standard type context.
     */
    public static TypeContext standardTypeContext()
    {
        return StandardTypeContext.INSTANCE;
    }

    /**
     * Returns the arbitrary-precision type-context.
     */
    public static TypeContext arbitraryPrecisionTypeContext()
    {
        return ArbitraryPrecisionTypeContext.INSTANCE;
    }

}
