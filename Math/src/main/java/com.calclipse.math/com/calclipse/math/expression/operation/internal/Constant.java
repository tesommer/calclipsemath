package com.calclipse.math.expression.operation.internal;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.Value;

public final class Constant implements Value
{
    private final Object value;

    public Constant(final Object value)
    {
        this.value = requireNonNull(value, "value");
    }

    @Override
    public Object get()
    {
        return value;
    }

    @Override
    public void set(final Object value) throws ErrorMessage
    {
        Value.reject(value);
    }

    @Override
    public boolean isConstant()
    {
        return true;
    }

    @Override
    public Value asConstant()
    {
        return this;
    }

    @Override
    public String toString()
    {
        return Constant.class.getSimpleName() + ": " + value;
    }

}
