package com.calclipse.math.expression.operation;

import static java.util.Objects.requireNonNull;

import java.util.function.Supplier;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;

/**
 * A mathematical operation on two values.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface BinaryOperation
{
    /**
     * Performs the evaluation.
     * @param left the left argument
     * @param right the right argument
     * @return the result of the evaluation
     * @throws ErrorMessage if the evaluation resulted in an error
     */
    public abstract Value evaluate(Value left, Value right)
            throws ErrorMessage;
    
    /**
     * Returns this operation as variable arity.
     * The returned operation expects two arguments.
     * @param wrongNumberOfArguments
     * to throw when the returned operation receives
     * the wrong number of arguments.
     */
    public default VariableArityOperation asVariableArity(
            final Supplier<? extends ErrorMessage> wrongNumberOfArguments)
    {
        return args ->
        {
            if (args.length != 2)
            {
                throw wrongNumberOfArguments.get();
            }
            return BinaryOperation.this.evaluate(args[0], args[1]);
        };
    }
    
    /**
     * Overrides the given operation with this operation.
     * @param operation the operation to override
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default BinaryOperation override(final BinaryOperation operation)
    {
        requireNonNull(operation, "operation");
        return (left, right) -> operation.overriddenEvaluate(left, right, this);
    }
    
    /**
     * Allows this operation to be overridden by the given operation.
     * @param override the operation that overrides this one
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default BinaryOperation acceptOverride(
            final BinaryOperation override)
    {
        requireNonNull(override, "override");
        return (left, right) -> overriddenEvaluate(left, right, override);
    }
    
    private Value overriddenEvaluate(
            final Value left,
            final Value right,
            final BinaryOperation override) throws ErrorMessage
    {
        final Value result = override.evaluate(left, right);
        if (result == Values.YIELD)
        {
            return evaluate(left, right);
        }
        return result;
    }

}
