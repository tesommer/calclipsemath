package com.calclipse.math.expression.message;

import static java.util.Objects.requireNonNull;

import java.text.MessageFormat;

/**
 * Parse-related user-level error messages.
 * Each message has a static accessor and a corresponding mutator in
 * {@link Builder}.
 * Any parameters accepted by a message's accessor are used as format elements.
 * This means that the number of format elements of each message
 * can be determined by looking at the message's accessor.
 * (See {@code java.text.MessageFormat} for information on the format pattern.)
 * 
 * This class has the following messages:
 * 
 * <ul>
 * 
 * <li>{@code unexpectedEnd}:
 * The expression ended prematurely.</li>
 * 
 * <li>{@code unexpectedRParen}:
 * Unexpected right parenthesis.</li>
 * 
 * <li>{@code expectedRParen}:
 * A right parenthesis is missing.</li>
 * 
 * <li>{@code undelimitedComment}:
 * Unclosed comment.</li>
 * 
 * <li>{@code undelimitedString}:
 * Unclosed string.</li>
 * 
 * <li>{@code invalidNumber}:
 * Malformed number.</li>
 * 
 * <li>{@code undefinedSymbol}:
 * Unrecognized symbol.</li>
 * 
 * <li>{@code unexpected}:
 * Something unexpected was encountered.</li>
 * 
 * <li>{@code expected}:
 * Something is missing.</li>
 * 
 * </ul>
 * 
 * This class is thread safe.
 * 
 * @author Tone Sommerland
 */
public final class ParseMessages
{
    private static final class Internals
    {
        private String
        unexpectedEnd = "Unexpected end of expression.";

        private String
        unexpectedRParen = "Unexpected closing parenthesis.";
        
        private String
        expectedRParen = "Closing parenthesis expected.";
        
        private String
        undelimitedComment = "Undelimited comment.";
        
        private String
        undelimitedString = "Undelimited string.";
        
        private String
        invalidNumber = "Invalid number: {0}";
        
        private String
        undefinedSymbol = "Undefined symbol: {0}";
        
        private String
        unexpected = "Unexpected: {0}";
        
        private String
        expected = "''{0}'' expected.";

        private Internals()
        {
        }
    }
    
    /**
     * Builds instances of {@link ParseMessages}.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final Internals internals = new Internals();

        /**
         * Creates a new builder
         * with a default set of messages as a starting point.
         */
        public Builder()
        {
        }

        /**
         * Sets the {@code unexpectedEnd} message.
         * @param unexpectedEnd a message with zero format arguments
         * @return {@code this}
         */
        public Builder setUnexpectedEnd(final String unexpectedEnd)
        {
            internals.unexpectedEnd = requireNonNull(
                    unexpectedEnd, "unexpectedEnd");
            return this;
        }

        /**
         * Sets the {@code unexpectedRParen} message.
         * @param unexpectedRParen a message with zero format arguments
         * @return {@code this}
         */
        public Builder setUnexpectedRParen(final String unexpectedRParen)
        {
            internals.unexpectedRParen = requireNonNull(
                    unexpectedRParen, "unexpectedRParen");
            return this;
        }

        /**
         * Sets the {@code expectedRParen} message.
         * @param expectedRParen a message with zero format arguments
         * @return {@code this}
         */
        public Builder setExpectedRParen(final String expectedRParen)
        {
            internals.expectedRParen = requireNonNull(
                    expectedRParen, "expectedRParen");
            return this;
        }

        /**
         * Sets the {@code undelimitedComment} message.
         * @param undelimitedComment a message with zero format arguments
         * @return {@code this}
         */
        public Builder setUndelimitedComment(final String undelimitedComment)
        {
            internals.undelimitedComment = requireNonNull(
                    undelimitedComment, "undelimitedComment");
            return this;
        }

        /**
         * Sets the {@code undelimitedString} message.
         * @param undelimitedString a message with zero format arguments
         * @return {@code this}
         */
        public Builder setUndelimitedString(final String undelimitedString)
        {
            internals.undelimitedString = requireNonNull(
                    undelimitedString, "undelimitedString");
            return this;
        }

        /**
         * Sets the {@code invalidNumber} message.
         * @param invalidNumber a message with one format argument
         * @return {@code this}
         */
        public Builder setInvalidNumber(final String invalidNumber)
        {
            internals.invalidNumber = requireNonNull(
                    invalidNumber, "invalidNumber");
            return this;
        }

        /**
         * Sets the {@code undefinedSymbol} message.
         * @param undefinedSymbol a message with one format argument
         * @return {@code this}
         */
        public Builder setUndefinedSymbol(final String undefinedSymbol)
        {
            internals.undefinedSymbol = requireNonNull(
                    undefinedSymbol, "undefinedSymbol");
            return this;
        }

        /**
         * Sets the {@code unexpected} message.
         * @param unexpected a message with one format argument
         * @return {@code this}
         */
        public Builder setUnexpected(final String unexpected)
        {
            internals.unexpected = requireNonNull(unexpected, "unexpected");
            return this;
        }

        /**
         * Sets the {@code expected} message.
         * @param expected a message with one format argument
         * @return {@code this}
         */
        public Builder setExpected(final String expected)
        {
            internals.expected = requireNonNull(expected, "expected");
            return this;
        }
        
        /**
         * Builds the parse messages.
         */
        public ParseMessages build()
        {
            return new ParseMessages(internals);
        }
    }
    
    private static volatile ParseMessages
    instance = new ParseMessages(new Internals());
    
    private final String unexpectedEnd;
    private final String unexpectedRParen;
    private final String expectedRParen;
    private final String undelimitedComment;
    private final String undelimitedString;
    private final String invalidNumber;
    private final String undefinedSymbol;
    private final String unexpected;
    private final String expected;

    private ParseMessages(final Internals internals)
    {
        assert internals.unexpectedEnd      != null;
        assert internals.unexpectedRParen   != null;
        assert internals.expectedRParen     != null;
        assert internals.undelimitedComment != null;
        assert internals.undelimitedString  != null;
        assert internals.invalidNumber      != null;
        assert internals.undefinedSymbol    != null;
        assert internals.unexpected         != null;
        assert internals.expected           != null;
        this.unexpectedEnd      = internals.unexpectedEnd;
        this.unexpectedRParen   = internals.unexpectedRParen;
        this.expectedRParen     = internals.expectedRParen;
        this.undelimitedComment = internals.undelimitedComment;
        this.undelimitedString  = internals.undelimitedString;
        this.invalidNumber      = internals.invalidNumber;
        this.undefinedSymbol    = internals.undefinedSymbol;
        this.unexpected         = internals.unexpected;
        this.expected           = internals.expected;
    }
    
    /**
     * Sets the main instance.
     */
    public static void set(final ParseMessages instance)
    {
        ParseMessages.instance = requireNonNull(instance, "instance");
    }

    /**
     * Returns the {@code unexpectedEnd} message.
     */
    public static String unexpectedEnd()
    {
        return instance.unexpectedEnd;
    }

    /**
     * Returns the {@code unexpectedRParen} message.
     */
    public static String unexpectedRParen()
    {
        return instance.unexpectedRParen;
    }

    /**
     * Returns the {@code expectedRParen} message.
     */
    public static String expectedRParen()
    {
        return instance.expectedRParen;
    }

    /**
     * Returns the {@code undelimitedComment} message.
     */
    public static String undelimitedComment()
    {
        return instance.undelimitedComment;
    }

    /**
     * Returns the {@code undelimitedString} message.
     */
    public static String undelimitedString()
    {
        return instance.undelimitedString;
    }

    /**
     * Returns a formatted {@code invalidNumber} message.
     * @param number the invalid number
     */
    public static String invalidNumber(final Object number)
    {
        return MessageFormat.format(
                instance.invalidNumber, requireNonNull(number, "number"));
    }

    /**
     * Returns a formatted {@code undefinedSymbol} message.
     * @param symbol the undefined symbol
     */
    public static String undefinedSymbol(final Object symbol)
    {
        return MessageFormat.format(
                instance.undefinedSymbol, requireNonNull(symbol, "symbol"));
    }

    /**
     * Returns a formatted {@code unexpected} message.
     * @param what that which was unexpected
     */
    public static String unexpected(final Object what)
    {
        return MessageFormat.format(
                instance.unexpected, requireNonNull(what, "what"));
    }

    /**
     * Returns a formatted {@code expected} message.
     * @param what that which was expected
     */
    public static String expected(final Object what)
    {
        return MessageFormat.format(
                instance.expected, requireNonNull(what, "what"));
    }

    @Override
    public String toString()
    {
        return ParseMessages.class.getSimpleName() + ": " + unexpectedEnd;
    }

}
