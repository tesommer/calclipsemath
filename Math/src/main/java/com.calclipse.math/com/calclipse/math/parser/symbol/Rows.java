package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Number of rows in a matrix.
 * The returned value is assignable unless the input is a constant.
 * Supported types:
 * <ul>
 *  <li>matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Rows implements UnaryOperation
{
    public static final String NAME = "rows";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Rows(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Rows(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            if (arg.isConstant())
            {
                return Value.constantOf(typeContext.numberType()
                        .valueOf(mx.rows()));
            }
            else
            {
                return new RowsValue(typeContext, mx);
            }
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }
    
    private static final class RowsValue implements Value
    {
        private final TypeContext typeContext;
        private final Matrix matrix;
        
        private RowsValue(final TypeContext typeContext, final Matrix matrix)
        {
            assert typeContext != null;
            assert matrix != null;
            this.typeContext = typeContext;
            this.matrix = matrix;
        }

        @Override
        public Object get()
        {
            return typeContext.numberType().valueOf(matrix.rows());
        }

        @Override
        public void set(final Object value) throws ErrorMessage
        {
            final int newRows = TypeUtil.toInt(value);
            if (newRows < 1)
            {
                throw errorMessage(EvalMessages.invalidSize());
            }
            matrix.setSize(newRows, matrix.columns(), true);
        }

        @Override
        public boolean isConstant()
        {
            return false;
        }
        
    }

}
