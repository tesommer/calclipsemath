package com.calclipse.math.parser.misc;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Optional;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.Real;

/**
 * This class contains static methods for
 * performing various mathematical operations.
 * 
 * @author Tone Sommerland
 */
public final class MathUtil
{
    private MathUtil()
    {
    }
    
      //////////////
     // Discrete //
    //////////////
    
    /**
     * The factorial of an integer:
     * {@code n! = 1 * 2 * 3 * � * n}.
     * Throws ArithmeticException if the argument is less than zero
     */
    public static BigInteger factorial(final long n)
    {
        if (n < 0)
        {
            throw new ArithmeticException("n!: n < 0: " + n);
        }
        if (n == 0)
        {
            return BigInteger.ONE;
        }
        BigInteger result = BigInteger.ONE;
        for (long factor = 2; factor <= n; factor++)
        {
            result = result.multiply(BigInteger.valueOf(factor));
        }
        return result;
    }
    
    /**
     * The binomial coefficient:
     * {@code nCr = n! / (n! * (n - r)!)}.
     * @throws ArithmeticException if n is less than zero
     */
    public static BigInteger choose(final long n, final long r)
    {
        if (n < 0)
        {
            throw new ArithmeticException("choose(n, r): n < 0: " + n);
        }
        if (r < 0 || r > n)
        {
            return BigInteger.ZERO;
        }
        final long nMinusR = n - r;
        if (nMinusR < r)
        {
            return permutations(n, nMinusR).divide(factorial(nMinusR));
        }
        return permutations(n, r).divide(factorial(r));
    }
    
    /**
     * The number of ordered selections of r objects
     * from a collections of n objects:
     * {@code nPr = n! / (n - r)!}.
     * @throws ArithmeticException if n is less than zero
     */
    public static BigInteger permutations(final long n, final long r)
    {
        if (n < 0)
        {
            throw new ArithmeticException("permutations(n, r): n < 0: " + n);
        }
        if (r < 0 || r > n)
        {
            return BigInteger.ZERO;
        }
        BigInteger result = BigInteger.ONE;
        final long nMinusR = n - r;
        for (long i = 1; i <= r; i++)
        {
            result = result.multiply(BigInteger.valueOf(nMinusR + i));
        }
        return result;
    }
    
    /**
     * The least common multiple of two integers.
     */
    public static BigInteger lcm(final BigInteger a, final BigInteger b)
    {
        if (a.equals(BigInteger.ZERO) || b.equals(BigInteger.ZERO))
        {
            return BigInteger.ZERO;
        }
        return a.divide(a.gcd(b)).multiply(b);
    }
    
      //////////
     // real //
    //////////
    
    /**
     * Rounds a floating number to a specified number of decimals.
     * @throws ArithmeticException if decimals is negative
     */
    public static double round(final double d, final int decimals)
    {
        if (decimals < 0)
        {
            throw new ArithmeticException("decimals < 0: " + decimals);
        }
        final double tenPowDec;
        if (d >= 0)
        {
            tenPowDec = Math.pow(10, decimals);
        }
        else
        {
            tenPowDec = -Math.pow(10, decimals);
        }
        final double rounded = (d + (.5 / tenPowDec)) * tenPowDec;
        // rounded will be positive in floor.
        return Math.floor(rounded) / tenPowDec;
    }
    
    /**
     * Inverse hyperbolic cosine.
     */
    public static double acosh(final double d)
    {
        return 2 * Math.log(Math.sqrt((d + 1) * .5) + Math.sqrt((d - 1) * .5));
    }
    
    /**
     * Inverse hyperbolic sine.
     */
    public static double asinh(final double d)
    {
        return Math.log(d + Math.sqrt(1 + d * d));
    }
    
    /**
     * Inverse hyperbolic tangent.
     */
    public static double atanh(final double d)
    {
        return (Math.log(1 + d) - Math.log(1 - d)) * .5;
    }

    //////////////
    // Rational //
    //////////////

    /**
     * Converts a double to a fraction.
     * @param rational the number to convert
     * @param iterations the maximum number of iterations (e.g. 20)
     * @param delta tolerance (a positive number close to zero, e.g. 1E-5)
     * @return empty if the argument is irrational
     * @throws ArithmeticException if the number to convert is NaN or infinite.
     */
    public static Optional<Fraction> toFraction(
            final double rational,
            final int iterations,
            final double delta)
    {
        if (Double.isNaN(rational) || Double.isInfinite(rational))
        {
            throw new ArithmeticException("NaN or infinite: " + rational);
        }

        double d = rational;
        long iPart = (long)d;
        final Fraction initial = Fraction.valueOf(
                BigInteger.valueOf(iPart), BigInteger.ONE);

        d -= iPart;
        if (Math.abs(d) <= delta)
        {
            return Optional.of(initial);
        }

        final var summands = new ArrayDeque<BigInteger>();
        boolean irrational = true;

        for (int i = 0; i < iterations && irrational; i++)
        {
            d = 1 / d;
            iPart = (long)d;
            summands.add(BigInteger.valueOf(iPart));
            d -= iPart;
            irrational = Math.abs(d) > delta;
        }

        if (irrational)
        {
            return Optional.empty();
        }

        Fraction summand = Fraction.valueOf(
                summands.removeLast(), BigInteger.ONE);
        while (!summands.isEmpty())
        {
            final Fraction next = Fraction.valueOf(
                    summands.removeLast(), BigInteger.ONE);
            summand = next.plus(summand.inverse());
        }

        return Optional.of(initial.plus(summand.inverse()));
    }
    
      //////////////////////////////
     // Arbitrary-precision real //
    //////////////////////////////
    
    /**
     * Converts a real into a big integer, checking for lost information.
     * @return empty if the real has a nonzero fraction part
     */
    public static Optional<BigInteger> toBigIntegerExact(final Real real)
    {
        return real.isFractured()
                ? toBigIntegerExact(real.fraction())
                : toBigIntegerExact(real.bigDecimal());
    }
    
    private static Optional<BigInteger> toBigIntegerExact(
            final Fraction fraction)
    {
        if (fraction.denominator().equals(BigInteger.ONE))
        {
            return Optional.of(fraction.numerator());
        }
        return Optional.empty();
    }
    
    private static Optional<BigInteger> toBigIntegerExact(
            final BigDecimal bigDecimal)
    {
        try
        {
            return Optional.of(bigDecimal.toBigIntegerExact());
        }
        catch (final ArithmeticException ex)
        {
            return Optional.empty();
        }
    }
    
    /**
     * The integer part of a real.
     * The returned real is fractured if the argument is fractured,
     * and unfractured if the argument is unfractured.
     */
    public static Real integerPart(final Real real)
    {
        if (real.isFractured())
        {
            final Fraction frac = real.fraction();
            final Fraction iPart = Fraction.valueOf(
                    frac.numerator().divide(frac.denominator()),
                    BigInteger.ONE);
            return Real.fractured(
                    iPart, real.mathContext(), real.fracturability());
        }
        final var iPart = new BigDecimal(real.bigDecimal().toBigInteger());
        return Real.unfractured(
                iPart, real.mathContext(), real.fracturability());
    }
    
    /**
     * The fraction part of a real.
     * The returned real is fractured if the argument is fractured,
     * and unfractured if the argument is unfractured.
     */
    public static Real fractionPart(final Real real)
    {
        if (real.isFractured())
        {
            final Fraction frac = real.fraction();
            final Fraction fPart = Fraction.valueOf(
                    frac.numerator().remainder(frac.denominator()),
                    frac.denominator());
            return Real.fractured(
                    fPart, real.mathContext(), real.fracturability());
        }
        final BigDecimal bigDec = real.bigDecimal();
        final BigDecimal fPart
            = bigDec.subtract(new BigDecimal(bigDec.toBigInteger()));
        return Real.unfractured(
                fPart, real.mathContext(), real.fracturability());
    }
    
    /**
     * Rounds a real to a specified number of decimals
     * using the rounding mode of the real's math context.
     * The given real is first
     * {@link com.calclipse.math.Real#unfracture() unfractured},
     * and the returned real is thus unfractured.
     * @throws ArithmeticException if
     * the specified number of decimals is negative,
     * or possibly by the rounding operation
     * if the real's math context requires exact results
     */
    public static Real round(final Real real, final int decimals)
    {
        if (decimals < 0)
        {
            throw new ArithmeticException("decimals < 0: " +  decimals);
        }
        final BigDecimal bigDec = real.unfracture().bigDecimal();
        final BigDecimal rounded
            = bigDec.setScale(decimals, real.mathContext().getRoundingMode());
        return Real.unfractured(
                rounded, real.mathContext(), real.fracturability());
    }
    
      /////////////
     // Complex //
    /////////////
    
    /**
     * The square root of a complex number.
     */
    public static Complex sqrt(final Complex c)
    {
        return pow(c, .5);
    }
    
    /**
     * The natural logarithm of a complex number.
     */
    public static Complex ln(final Complex c)
    {
        return Complex.valueOf(Math.log(c.abs()), arg(c));
    }
    
    /**
     * {@code e^c}.
     */
    public static Complex exp(final Complex c)
    {
        return pow(Complex.valueOf(Math.E, 0), c);
    }
    
    /**
     * Cosine of a complex number.
     */
    public static Complex cos(final Complex c)
    {
        return Complex.valueOf(Math.cos(c.real()) * Math.cosh(c.imaginary()),
                -Math.sin(c.real()) * Math.sinh(c.imaginary()));
    }
    
    /**
     * Sine of a complex number.
     */
    public static Complex sin(final Complex c)
    {
        return Complex.valueOf(Math.sin(c.real()) * Math.cosh(c.imaginary()),
                Math.cos(c.real()) * Math.sinh(c.imaginary()));
    }
    
    /**
     * Tangent of a complex number.
     */
    public static Complex tan(final Complex c)
    {
        return sin(c).dividedBy(cos(c));
    }
    
    /**
     * Inverse cosine of a complex number.
     */
    public static Complex acos(final Complex c)
    {
        return Complex.valueOf(0, -1).times(ln(c.plus(sqrt(Complex.valueOf(1, 0)
                .minus(pow(c, 2))).times(Complex.I))));
    }
    
    /**
     * Inverse sine of a complex number.
     */
    public static Complex asin(final Complex c)
    {
        return Complex.valueOf(0, -1).times(ln(c.times(Complex.I)
                .plus(sqrt(Complex.valueOf(1, 0).minus(pow(c, 2))))));
    }
    
    /**
     * Inverse tangent of a complex number.
     */
    public static Complex atan(final Complex c)
    {
        return Complex.I.dividedBy(2).times(
                ln(Complex.I.plus(c).dividedBy(Complex.I.minus(c))));
    }
    
    /**
     * Hyperbolic cosine of a complex number.
     */
    public static Complex cosh(final Complex c)
    {
        return Complex.valueOf(Math.cosh(c.real()) * Math.cos(c.imaginary()),
                Math.sinh(c.real()) * Math.sin(c.imaginary()));
    }
    
    /**
     * Hyperbolic sine of a complex number.
     */
    public static Complex sinh(final Complex c)
    {
        return Complex.valueOf(Math.sinh(c.real()) * Math.cos(c.imaginary()),
                Math.cosh(c.real()) * Math.sin(c.imaginary()));
    }
    
    /**
     * Hyperbolic tangent of a complex number.
     */
    public static Complex tanh(final Complex c)
    {
        return exp(c).minus(exp(c.times(-1))).dividedBy(exp(c)
                .plus(exp(c.times(-1))));
    }
    
    /**
     * Inverse hyperbolic cosine of a complex number.
     */
    public static Complex acosh(final Complex c)
    {
        return ln(c.plus(sqrt(pow(c, 2).minus(1))));
    }
    
    /**
     * Inverse hyperbolic sine of a complex number.
     */
    public static Complex asinh(final Complex c)
    {
        return ln(c.plus(sqrt(pow(c, 2).plus(1))));
    }
    
    /**
     * Inverse hyperbolic tangent of a complex number.
     */
    public static Complex atanh(final Complex c)
    {
        return ln(c.plus(1)).minus(ln(Complex.valueOf(1, 0).minus(c)))
                .dividedBy(2);
    }
    
    /**
     * The argument of a complex number,
     * i.e. the angle between the vector (real, imaginary) and the x-axis.
     */
    public static double arg(final Complex c)
    {
        return Math.atan2(c.imaginary(), c.real());
    }
    
    /**
     * Raises a complex number to a power.
     * @param c the base
     * @param exp the exponent
     */
    public static Complex pow(final Complex c, final Complex exp)
    {
        final double r = c.abs();
        final double lnR = r == 0 ? 0 : Math.log(r);
        final double argC = arg(c);
        final double d = exp.imaginary() * lnR + exp.real() * argC;
        final double d2
            = Math.pow(r, exp.real()) * Math.exp(-exp.imaginary() * argC);
        return Complex.valueOf(Math.cos(d), Math.sin(d)).times(d2);
    }
    
    /**
     * Raises a complex number to a power.
     * @param c the base
     * @param exp the exponent
     */
    public static Complex pow(final Complex c, final double exp)
    {
        final double r = c.abs();
        final double d = arg(c) * exp;
        return Complex.valueOf(
                Math.cos(d), Math.sin(d)).times(Math.pow(r, exp));
    }

}
