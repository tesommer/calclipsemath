/**
 * Interfaces for implementing mathematical operations.
 */
package com.calclipse.math.expression.operation;
