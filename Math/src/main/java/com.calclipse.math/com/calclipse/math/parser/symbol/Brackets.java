package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Left bracket is a function for creating matrices.
 * It supports reals for creating a vector
 * and matrices (vectors) for creating a matrix.
 * 
 * @author Tone Sommerland
 */
public final class Brackets implements VariableArityOperation
{
    public static final String NAME = "[";
    
    public static final Token CLOSER = Tokens.plain("]");
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Brackets(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(final TypeContext typeContext)
    {
        return Function.of(
                NAME,
                Delimitation.byTokensWithoutOpener(
                        CLOSER, ParensAndComma.COMMA, Token::identifiesAs),
                new Brackets(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(1, args.length);
        
        final Object firstVal = args[0].get();
        
        if (firstVal instanceof Number)
        {
            return createVector(args, (Number)firstVal);
        }
        else if (firstVal instanceof Matrix)
        {
            return createMatrix(args, (Matrix)firstVal);
        }
        
        throw ArgUtil.forArgument(1, TypeMessages.invalidType(firstVal));
    }

    private Value createVector(
            final Value[] args, final Number firstVal) throws ErrorMessage
    {
        final Matrix vec
            = typeContext.matrixType().createMatrix(args.length, 1);
        vec.set(0, 0, firstVal.doubleValue());
        for (int i = 1; i < args.length; i++)
        {
            final Object value = args[i].get();
            vec.set(i, 0, TypeUtil.argToDouble(i + 1, value));
        }
        return Value.constantOf(vec);
    }

    private static Value createMatrix(
            final Value[] args, final Matrix firstVal) throws ErrorMessage
    {
        if (firstVal.columns() != 1)
        {
            throw ArgUtil.forArgument(1, EvalMessages.invalidSize());
        }
        final Matrix mx = firstVal.create(firstVal.rows(), args.length);
        mx.setColumn(0, firstVal);
        
        for (int i = 1; i < args.length; i++)
        {
            final Object value = args[i].get();
            if (!(value instanceof Matrix))
            {
                throw ArgUtil.forArgument(
                        i + 1, TypeMessages.invalidType(value));
            }
            final var vec = (Matrix)value;
            if (vec.rows() != mx.rows() || vec.columns() != 1)
            {
                throw ArgUtil.forArgument(i + 1, EvalMessages.invalidSize());
            }
            mx.setColumn(i, vec);
        }
        
        return Value.constantOf(mx);
    }

}
