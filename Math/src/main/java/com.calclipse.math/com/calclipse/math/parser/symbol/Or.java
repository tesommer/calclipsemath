package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.EvalUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Logical or.
 * Results in true if one or both arguments evaluates to true.
 * Supported types:
 * <ul>
 *  <li>boolean, boolean</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Or implements BinaryOperation, ConditionalOperation
{
    public static final String NAME = "or";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Or(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.LOGICAL, new Or(typeContext))
                    .withId(ID);
    }
    
    /**
     * Returns a control flow version of {@code or}
     * that only evaluates as many arguments as necessary.
     * The returned function accepts an arbitrary number of arguments
     * (minimum one).
     */
    public static Function shortCircuited(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.ofConditional(
                NAME, delimitation, new Or(typeContext)).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final boolean b1 = TypeUtil.argToBoolean(1, left.get(), typeContext);
        final boolean b2 = TypeUtil.argToBoolean(2, right.get(), typeContext);
        return Value.constantOf(typeContext.booleanType().valueOf(b1 || b2));
    }
    
    @Override
    public Value evaluate(final List<Expression> args) throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(1, args.size());
        for (int i = 0; i < args.size(); i++)
        {
            try
            {
                if (EvalUtil.evaluateBoolean(args.get(i), false, typeContext))
                {
                    return Value.constantOf(
                            typeContext.booleanType().valueOf(true));
                }
            }
            catch (final ErrorMessage ex)
            {
                throw ArgUtil.forArgument(i + 1, ex);
            }
        }
        return Value.constantOf(typeContext.booleanType().valueOf(false));
    }

}
