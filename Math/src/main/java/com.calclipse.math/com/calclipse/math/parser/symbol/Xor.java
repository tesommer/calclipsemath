package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Logical exclusive or.
 * Results in true if exactly one argument evaluates to true.
 * Supported types:
 * <ul>
 *  <li>boolean, boolean</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Xor implements BinaryOperation
{
    public static final String NAME = "xor";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Xor(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.LOGICAL, new Xor(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final boolean b1 = TypeUtil.argToBoolean(1, left.get(), typeContext);
        final boolean b2 = TypeUtil.argToBoolean(2, right.get(), typeContext);
        return Value.constantOf(typeContext.booleanType().valueOf(b1 ^ b2));
    }

}
