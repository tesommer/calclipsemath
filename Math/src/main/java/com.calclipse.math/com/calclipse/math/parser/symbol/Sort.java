package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.type.InvalidComparison;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Sorts its arguments.
 * Arguments containing arrays are treated
 * as collections of objects to compare.
 * Supports:
 * <ul>
 * <li>[array|*[, ..., array|*]]</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Sort implements VariableArityOperation
{
    public static final String NAME = "sort";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Sort(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Sort(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final List<Object> objects = Max.collect(args);
        try
        {
            objects.sort(typeContext.comparer().asComparator());
        }
        catch (final InvalidComparison ex)
        {
            throw ex.getCause();
        }
        return Value.constantOf(
                typeContext.arrayType().createArray(objects));
    }

}
