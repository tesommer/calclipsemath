package com.calclipse.math.parser.type;

import static java.util.stream.Collectors.toMap;

import java.util.Map;
import java.util.stream.Stream;

/**
 * Represents the math parser's associative-array type.
 * 
 * @author Tone Sommerland
 */
public interface AssociativeArrayType
{
    /**
     * Creates an associative array containing the given associations.
     */
    public abstract AssociativeArray createAssociativeArray(
            Map<?, ?> associations);
    
    /**
     * Returns an association between the given key and value.
     */
    public abstract Association associate(Object key, Object value);
    
    /**
     * Returns an associative array of the given associations.
     * In case of duplicate keys,
     * existing associations are overwritten by new ones.
     * @param associations the key-value pairs
     * @implSpec
     * Calls {@link #createAssociativeArray(Map)}.
     */
    public default AssociativeArray associativeArrayOf(
            final Map.Entry<?, ?>... associations)
    {
        return createAssociativeArray(
                Stream.of(associations)
                    .collect(toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (before, after) -> after)));
    }

}
