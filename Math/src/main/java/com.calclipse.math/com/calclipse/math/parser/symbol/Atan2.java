package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Two-argument inverse tangent.
 * Expects two arguments, both real.
 * 
 * @author Tone Sommerland
 */
public final class Atan2 implements VariableArityOperation
{
    public static final String NAME = "atan2";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Atan2(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Atan2(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireArgCount(2, args.length);
        final double d1 = TypeUtil.argToDouble(1, args[0].get());
        final double d2 = TypeUtil.argToDouble(2, args[1].get());
        return Value.constantOf(typeContext.numberType()
                .valueOf(Math.atan2(d1, d2)));
    }

}
