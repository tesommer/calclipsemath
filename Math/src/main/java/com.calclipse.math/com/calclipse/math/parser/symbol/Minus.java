package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Minus (subtraction).
 * Supported types:
 * <ul>
 *  <li>fraction, fraction</li>
 *  <li>complex|real, complex|real</li>
 *  <li>matrix, matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Minus implements BinaryOperation
{
    public static final String NAME = "-";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Minus(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.SUMMATION, new Minus(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        
        if (o1 instanceof Fraction && o2 instanceof Fraction)
        {
            return evalFractionFraction(o1, o2);
            
        }
        else if (o1 instanceof Complex && o2 instanceof Complex)
        {
            return evalComplexComplex(o1, o2);
            
        }
        else if (o1 instanceof Complex && o2 instanceof Number)
        {
            return evalComplexNumber(o1, o2);
            
        }
        else if (o1 instanceof Number && o2 instanceof Complex)
        {
            return evalNumberComplex(o1, o2);
            
        }
        else if (o1 instanceof Number && o2 instanceof Number)
        {
            return evalNumberNumber(typeContext, o1, o2);
            
        }
        else if (o1 instanceof Matrix && o2 instanceof Matrix)
        {
            return evalMatrixMatrix(o1, o2);
        }
        
        throw errorMessage(TypeMessages.incompatibleTypes(o1, o2));
    }

    private static Value evalFractionFraction(
            final Object o1, final Object o2)
    {
        final var f1 = (Fraction)o1;
        final var f2 = (Fraction)o2;
        return Value.constantOf(f1.minus(f2));
    }

    private static Value evalNumberNumber(
            final TypeContext typeContext, final Object o1, final Object o2)
    {
        final var n1 = (Number)o1;
        final var n2 = (Number)o2;
        return Value.constantOf(typeContext.numberType()
                .valueOf(n1.doubleValue() - n2.doubleValue()));
    }

    private static Value evalComplexComplex(final Object o1, final Object o2)
    {
        final var c1 = (Complex)o1;
        final var c2 = (Complex)o2;
        return Value.constantOf(c1.minus(c2));
    }

    private static Value evalComplexNumber(final Object o1, final Object o2)
    {
        final var c1 = (Complex)o1;
        final var n2 = (Number)o2;
        return Value.constantOf(c1.minus(n2.doubleValue()));
    }

    private static Value evalNumberComplex(final Object o1, final Object o2)
    {
        final var n1 = (Number)o1;
        final var c2 = (Complex)o2;
        return Value.constantOf(Complex.valueOf(n1.doubleValue(), 0).minus(c2));
    }

    private static Value evalMatrixMatrix(final Object o1, final Object o2)
            throws ErrorMessage
    {
        final var mx1 = (Matrix)o1;
        final var mx2 = (Matrix)o2;
        try
        {
            return Value.constantOf(mx1.minus(mx2));
        }
        catch (final SizeViolation ex)
        {
            throw errorMessage(EvalMessages.mismatchingDimensions())
                .withCause(ex);
        }
    }

}
