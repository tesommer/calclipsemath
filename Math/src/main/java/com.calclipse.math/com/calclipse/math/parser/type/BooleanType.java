package com.calclipse.math.parser.type;

import com.calclipse.math.expression.ErrorMessage;

/**
 * Represents the math parser's boolean type.
 * 
 * @author Tone Sommerland
 */
public interface BooleanType
{
    /**
     * Turns a primitive boolean into a value object.
     */
    public abstract Object valueOf(boolean b);
    
    /**
     * Turns a value object into a primitive boolean.
     * @throws ErrorMessage if the given value is not a suitable boolean
     */
    public abstract boolean isTrue(Object boolValue) throws ErrorMessage;

}
