package com.calclipse.math.parser.type;

import java.util.Comparator;

import com.calclipse.math.expression.ErrorMessage;

/**
 * Compares objects of various data types.
 * Objects implementing this interface may be used by certain operations
 * for comparison and ordering purposes.
 * Objects being compared are not allowed to be {@code null}.
 * 
 * @author Tone Sommerland
 */
public interface Comparer
{
    /**
     * Imposes an ordering of data types that are deemed comparable.
     * @param value1 the first to be compared
     * @param value2 the second to be compared
     * @return a negative integer if the first is less than the second,
     * 0 if they are equal,
     * and a positive integer if the first is the greater of the two
     * @throws ErrorMessage in particular when asked to compare something
     * that doesn't make sense, such as two matrices
     */
    public abstract int compare(Object value1, Object value2)
            throws ErrorMessage;
    
    /**
     * Compares two objects for equality.
     * For this method to be consistent with
     * {@link #compare(Object, Object)},
     * it must return {@code true} if {@code compare} returns {@code 0}.
     * @return {@code true} if they are equal, {@code false} otherwise
     */
    public abstract boolean areEqual(Object value1, Object value2);
    
    /**
     * Returns this comparer as a comparator of objects.
     * Any {@link com.calclipse.math.expression.ErrorMessage}
     * thrown during comparison is wrapped in an
     * {@link InvalidComparison}, which then must be caught.
     */
    public default Comparator<Object> asComparator()
    {
        return (o1, o2) ->
        {
            try
            {
                return compare(o1, o2);
            }
            catch (final ErrorMessage ex)
            {
                throw new InvalidComparison(ex);
            }
        };
    }

}
