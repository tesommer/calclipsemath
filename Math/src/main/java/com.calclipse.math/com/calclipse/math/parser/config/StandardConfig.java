package com.calclipse.math.parser.config;

import static com.calclipse.math.parser.ParensAndComma.COMMA;
import static com.calclipse.math.parser.ParensAndComma.LPAREN;
import static com.calclipse.math.parser.ParensAndComma.RPAREN;
import static com.calclipse.math.parser.SymbolDependencies.binaryReference;
import static com.calclipse.math.parser.SymbolDependencies.selection;

import java.util.ArrayList;
import java.util.Optional;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Evaluator;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Scanner;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.MathParser;
import com.calclipse.math.parser.MathParserConfig;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.config.internal.AlternativeNotations;
import com.calclipse.math.parser.scanner.Defragmenter;
import com.calclipse.math.parser.scanner.SymbolAndLiteralScanner;
import com.calclipse.math.parser.step.Arguments;
import com.calclipse.math.parser.step.Check;
import com.calclipse.math.parser.step.ImplicitMultiplication;
import com.calclipse.math.parser.step.Indices;
import com.calclipse.math.parser.step.OperatorToFunction;
import com.calclipse.math.parser.step.Postfix;
import com.calclipse.math.parser.step.Sign;
import com.calclipse.math.parser.symbol.Abs;
import com.calclipse.math.parser.symbol.Acos;
import com.calclipse.math.parser.symbol.Acosh;
import com.calclipse.math.parser.symbol.And;
import com.calclipse.math.parser.symbol.Arg;
import com.calclipse.math.parser.symbol.Asin;
import com.calclipse.math.parser.symbol.Asinh;
import com.calclipse.math.parser.symbol.Assign;
import com.calclipse.math.parser.symbol.Associate;
import com.calclipse.math.parser.symbol.At;
import com.calclipse.math.parser.symbol.Atan;
import com.calclipse.math.parser.symbol.Atan2;
import com.calclipse.math.parser.symbol.Atanh;
import com.calclipse.math.parser.symbol.Braces;
import com.calclipse.math.parser.symbol.BracketAngles;
import com.calclipse.math.parser.symbol.Brackets;
import com.calclipse.math.parser.symbol.Cbrt;
import com.calclipse.math.parser.symbol.Ceil;
import com.calclipse.math.parser.symbol.Choose;
import com.calclipse.math.parser.symbol.Columns;
import com.calclipse.math.parser.symbol.Cos;
import com.calclipse.math.parser.symbol.Cosh;
import com.calclipse.math.parser.symbol.Cross;
import com.calclipse.math.parser.symbol.Decrement;
import com.calclipse.math.parser.symbol.Degrees;
import com.calclipse.math.parser.symbol.Denominator;
import com.calclipse.math.parser.symbol.Determinant;
import com.calclipse.math.parser.symbol.DividedBy;
import com.calclipse.math.parser.symbol.Equals;
import com.calclipse.math.parser.symbol.Exp;
import com.calclipse.math.parser.symbol.Factorial;
import com.calclipse.math.parser.symbol.Floor;
import com.calclipse.math.parser.symbol.Frac;
import com.calclipse.math.parser.symbol.FractionPart;
import com.calclipse.math.parser.symbol.Gauss;
import com.calclipse.math.parser.symbol.GaussJordan;
import com.calclipse.math.parser.symbol.Gcd;
import com.calclipse.math.parser.symbol.Get;
import com.calclipse.math.parser.symbol.GramSchmidt;
import com.calclipse.math.parser.symbol.Greater;
import com.calclipse.math.parser.symbol.GreaterEquals;
import com.calclipse.math.parser.symbol.Identity;
import com.calclipse.math.parser.symbol.ImaginaryPart;
import com.calclipse.math.parser.symbol.Increment;
import com.calclipse.math.parser.symbol.IntegerPart;
import com.calclipse.math.parser.symbol.Lcm;
import com.calclipse.math.parser.symbol.Less;
import com.calclipse.math.parser.symbol.LessEquals;
import com.calclipse.math.parser.symbol.Ln;
import com.calclipse.math.parser.symbol.Lu;
import com.calclipse.math.parser.symbol.Max;
import com.calclipse.math.parser.symbol.Min;
import com.calclipse.math.parser.symbol.Minus;
import com.calclipse.math.parser.symbol.Modulo;
import com.calclipse.math.parser.symbol.Negative;
import com.calclipse.math.parser.symbol.Not;
import com.calclipse.math.parser.symbol.Numerator;
import com.calclipse.math.parser.symbol.Or;
import com.calclipse.math.parser.symbol.Percent;
import com.calclipse.math.parser.symbol.Permutations;
import com.calclipse.math.parser.symbol.Plus;
import com.calclipse.math.parser.symbol.Qr;
import com.calclipse.math.parser.symbol.Radians;
import com.calclipse.math.parser.symbol.Rand;
import com.calclipse.math.parser.symbol.RealPart;
import com.calclipse.math.parser.symbol.Reduce;
import com.calclipse.math.parser.symbol.Round;
import com.calclipse.math.parser.symbol.Rows;
import com.calclipse.math.parser.symbol.Sin;
import com.calclipse.math.parser.symbol.Sinh;
import com.calclipse.math.parser.symbol.Size;
import com.calclipse.math.parser.symbol.Solve;
import com.calclipse.math.parser.symbol.Sort;
import com.calclipse.math.parser.symbol.Sqrt;
import com.calclipse.math.parser.symbol.Tan;
import com.calclipse.math.parser.symbol.Tanh;
import com.calclipse.math.parser.symbol.Times;
import com.calclipse.math.parser.symbol.ToThePowerOf;
import com.calclipse.math.parser.symbol.Transposed;
import com.calclipse.math.parser.symbol.Unequals;
import com.calclipse.math.parser.symbol.Xor;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The standard math parser configuration.
 * 
 * @author Tone Sommerland
 */
public class StandardConfig implements MathParserConfig
{
    private TypeContext typeContext;
    private SymbolTable symTab;
    
    private UnaryOperator negative;
    private BinaryOperator times;
    
    private ParseStep arguments;
    private ParseStep indices;
    private ParseStep implicitMultiplication;
    private ParseStep sign;
    private ParseStep check;
    private ParseStep postfix;
    
    private OperatorToFunction.Replacer[] replacers;
    
    protected StandardConfig()
    {
    }
    
    static MathParser parser()
    {
        return new MathParser(new StandardConfig());
    }
    
    /**
     * The type context of this configuration.
     */
    protected final TypeContext theTypeContext()
    {
        if (typeContext == null)
        {
            typeContext = typeContext();
        }
        return typeContext;
    }
    
    /**
     * The symbol table of this configuration.
     */
    protected final SymbolTable theSymbolTable()
    {
        if (symTab == null)
        {
            symTab = SymbolTable.longestMatchLookup();
            populateSymbolTable(symTab);
        }
        return symTab;
    }
    
    /**
     * The negated operator instance.
     */
    private UnaryOperator theNegativeOperator()
    {
        if (negative == null)
        {
            negative = Negative.operator(theTypeContext());
        }
        return negative;
    }
    
    /**
     * The times operator instance.
     */
    private BinaryOperator theTimesOperator()
    {
        if (times == null)
        {
            times = Times.operator(theTypeContext());
        }
        return times;
    }
    
    /**
     * 1st parse step.
     */
    protected final ParseStep theArgumentsStep()
    {
        if (arguments == null)
        {
            arguments = Arguments.INSTANCE;
        }
        return arguments;
    }
    
    /**
     * 2nd parse step.
     */
    protected final ParseStep theIndicesStep()
    {
        if (indices == null)
        {
            indices = Indices
                    .openIndexListWith(LPAREN::identifiesAs)
                    .closeIndexListWith(RPAREN::identifiesAs)
                    .separateIndicesWith(COMMA::identifiesAs)
                    .delegateIndexingTo(theTypeContext().indexer())
                    .build();
        }
        return indices;
    }
    
    /**
     * 3rd parse step.
     */
    protected final ParseStep theImplicitMultiplicationStep()
    {
        if (implicitMultiplication == null)
        {
            implicitMultiplication = new ImplicitMultiplication(
                    selection(
                            BinaryOperator.class,
                            theTimesOperator(),
                            theSymbolTable()::all,
                            Times::identifiesAs));
        }
        return implicitMultiplication;
    }
    
    /**
     * 4th parse step.
     */
    protected final ParseStep theSignStep()
    {
        if (sign == null)
        {
            sign = new Sign(
                    selection(
                            Token.class,
                            theNegativeOperator(),
                            theSymbolTable()::all,
                            Negative::identifiesAs),
                    Plus::identifiesAs,
                    Minus::identifiesAs);
        }
        return sign;
    }
    
    /**
     * 5th parse step.
     */
    protected final ParseStep theCheckStep()
    {
        if (check == null)
        {
            check = Check.INSTANCE;
        }
        return check;
    }
    
    /**
     * 6th parse step.
     */
    protected final ParseStep thePostfixStep()
    {
        if (postfix == null)
        {
            postfix = Postfix.INSTANCE;
        }
        return postfix;
    }
    
    /**
     * The 7th parse step, if present.
     */
    protected final Optional<ParseStep> theOperatorToFunctionStep()
    {
        if (replacers == null)
        {
            replacers = operatorToFunctionReplacers();
        }
        if (replacers.length == 0)
        {
            return Optional.empty();
        }
        return Optional.of(new OperatorToFunction(replacers));
    }
    
    /**
     * Returns the type context implementation to use.
     * This method returns {@link StandardTypeContext#INSTANCE} by default.
     */
    protected TypeContext typeContext()
    {
        return Configs.standardTypeContext();
    }
    
    /**
     * Returns an array of operator-to-function replacers.
     * If the returned array is nonempty,
     * a step that replaces the target operators
     * will be added as a 7th parse step.
     * The default implementation returns an array containing one element:
     * a replacer for the logical {@code and} and {@code or} operators,
     * making them sequence points.
     */
    protected OperatorToFunction.Replacer[] operatorToFunctionReplacers()
    {
        final var replacer = new OperatorToFunction.Replacer()
        {
            @Override
            public boolean isTarget(final Operator operator)
            {
                return And.identifiesAs(operator) || Or.identifiesAs(operator);
            }
            
            @Override
            public Function replace(final Operator operator)
            {
                if (And.identifiesAs(operator))
                {
                    return And.shortCircuited(
                            ParensAndComma.DELIMITATION, theTypeContext());
                }
                return Or.shortCircuited(
                        ParensAndComma.DELIMITATION, theTypeContext());
            }
        };
        return new OperatorToFunction.Replacer[] {replacer};
    }
    
    /**
     * Populates the symbol table of this configuration.
     * @param symbolTable the symbol table to add symbols to
     */
    protected void populateSymbolTable(final SymbolTable symbolTable)
    {
        final BinaryOperator plus = Plus.operator(theTypeContext());
        final BinaryOperator minus = Minus.operator(theTypeContext());
        
        final Operand piConstant = Operand.of(
                "pi",
                Value.constantOf(
                        theTypeContext().numberType().valueOf(Math.PI)));
        final Operand eConstant = Operand.of(
                "e",
                Value.constantOf(
                        theTypeContext().numberType().valueOf(Math.E)));
        final Operand falseConstant = Operand.of(
                "false",
                Value.constantOf(
                        theTypeContext().booleanType().valueOf(false)));
        final Operand trueConstant = Operand.of(
                "true",
                Value.constantOf(
                        theTypeContext().booleanType().valueOf(true)));
        final Operand iConstant = Operand.of("i", Value.constantOf(Complex.I));
        final Operand undefConstant = Operand.of("undef", Values.UNDEF);
        
        symbolTable.add(ParensAndComma.LPAREN);
        symbolTable.add(ParensAndComma.RPAREN);
        symbolTable.add(ParensAndComma.COMMA);
        
        symbolTable.add(piConstant);
        symbolTable.add(eConstant);
        symbolTable.add(falseConstant);
        symbolTable.add(trueConstant);
        symbolTable.add(iConstant);
        symbolTable.add(undefConstant);
        
        symbolTable.add(Braces.function(theTypeContext()));
        symbolTable.add(Braces.CLOSER);
        symbolTable.add(Brackets.function(theTypeContext()));
        symbolTable.add(Brackets.CLOSER);
        symbolTable.add(BracketAngles.function(theTypeContext()));
        symbolTable.add(BracketAngles.CLOSER);
        symbolTable.add(Associate.operator(theTypeContext()));

        symbolTable.add(plus);
        symbolTable.add(minus);
        symbolTable.add(theNegativeOperator());
        symbolTable.add(theTimesOperator());
        symbolTable.add(DividedBy.operator(theTypeContext()));
        symbolTable.add(ToThePowerOf.operator(theTypeContext()));
        
        symbolTable.add(Abs.operator(theTypeContext()));
        symbolTable.add(Sqrt.operator(theTypeContext()));
        symbolTable.add(Cbrt.operator(theTypeContext()));
        symbolTable.add(Ln.operator(theTypeContext()));
        symbolTable.add(Exp.operator(theTypeContext()));
        
        symbolTable.add(Cos.operator(theTypeContext()));
        symbolTable.add(Sin.operator(theTypeContext()));
        symbolTable.add(Tan.operator(theTypeContext()));
        symbolTable.add(Acos.operator(theTypeContext()));
        symbolTable.add(Asin.operator(theTypeContext()));
        symbolTable.add(Atan.operator(theTypeContext()));
        symbolTable.add(Atan2.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        
        symbolTable.add(Cosh.operator(theTypeContext()));
        symbolTable.add(Sinh.operator(theTypeContext()));
        symbolTable.add(Tanh.operator(theTypeContext()));
        symbolTable.add(Acosh.operator(theTypeContext()));
        symbolTable.add(Asinh.operator(theTypeContext()));
        symbolTable.add(Atanh.operator(theTypeContext()));
        
        symbolTable.add(Arg.operator());
        symbolTable.add(RealPart.operator(theTypeContext()));
        symbolTable.add(ImaginaryPart.operator(theTypeContext()));
        symbolTable.add(Denominator.operator(theTypeContext()));
        symbolTable.add(Numerator.operator(theTypeContext()));
        symbolTable.add(Reduce.operator());
        symbolTable.add(Frac.function(ParensAndComma.DELIMITATION));
        
        symbolTable.add(Equals.operator(theTypeContext()));
        symbolTable.add(Unequals.operator(theTypeContext()));
        symbolTable.add(Less.operator(theTypeContext()));
        symbolTable.add(LessEquals.operator(theTypeContext()));
        symbolTable.add(Greater.operator(theTypeContext()));
        symbolTable.add(GreaterEquals.operator(theTypeContext()));
        
        symbolTable.add(And.operator(theTypeContext()));
        symbolTable.add(Or.operator(theTypeContext()));
        symbolTable.add(Xor.operator(theTypeContext()));
        symbolTable.add(Not.operator(theTypeContext()));
        
        symbolTable.add(Max.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Min.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Sort.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        
        symbolTable.add(Ceil.operator(theTypeContext()));
        symbolTable.add(Floor.operator(theTypeContext()));
        symbolTable.add(IntegerPart.operator(theTypeContext()));
        symbolTable.add(FractionPart.operator(theTypeContext()));
        symbolTable.add(Round.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        
        symbolTable.add(Choose.operator(theTypeContext()));
        symbolTable.add(Permutations.operator(theTypeContext()));
        symbolTable.add(Factorial.operator(theTypeContext()));
        symbolTable.add(Gcd.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Lcm.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Modulo.operator(theTypeContext()));
        
        symbolTable.add(Assign.operator());
        symbolTable.add(Get.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(At.operator(theTypeContext()));
        symbolTable.add(Size.operator(theTypeContext()));
        symbolTable.add(Columns.operator(theTypeContext()));
        symbolTable.add(Rows.operator(theTypeContext()));
        symbolTable.add(Increment.operator(
                binaryReference(plus, symbolTable::get)));
        symbolTable.add(Decrement.operator(
                binaryReference(minus, symbolTable::get)));
        
        symbolTable.add(Determinant.operator(theTypeContext()));
        symbolTable.add(Identity.operator(theTypeContext()));
        symbolTable.add(Transposed.operator());
        symbolTable.add(Cross.operator());
        
        symbolTable.add(Gauss.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(GaussJordan.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(GramSchmidt.function(
                ParensAndComma.DELIMITATION));
        symbolTable.add(Lu.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Qr.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Solve.function(
                ParensAndComma.DELIMITATION));
        
        symbolTable.add(Degrees.operator(theTypeContext()));
        symbolTable.add(Radians.operator(theTypeContext()));
        symbolTable.add(Percent.operator(theTypeContext()));
        
        symbolTable.add(Rand.operand(theTypeContext()));
        
        AlternativeNotations.add(symbolTable, theTypeContext());
    }

    @Override
    public Scanner scanner()
    {
        return new Defragmenter(
                new SymbolAndLiteralScanner(
                        theSymbolTable(), theTypeContext()));
    }

    @Override
    public ParseStep[] steps()
    {
        final var steps = new ArrayList<ParseStep>(7);
        steps.add(theArgumentsStep());
        steps.add(theIndicesStep());
        steps.add(theImplicitMultiplicationStep());
        steps.add(theSignStep());
        steps.add(theCheckStep());
        steps.add(thePostfixStep());
        theOperatorToFunctionStep().ifPresent(steps::add);
        return steps.stream().toArray(ParseStep[]::new);
    }

    @Override
    public SymbolTable symbolTable()
    {
        return theSymbolTable();
    }

    @Override
    public Evaluator evaluator()
    {
        return Evaluator.forPostfix();
    }

}
