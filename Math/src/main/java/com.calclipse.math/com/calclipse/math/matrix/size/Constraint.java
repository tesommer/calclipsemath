package com.calclipse.math.matrix.size;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.util.function.Predicate;
import java.util.stream.Stream;

import com.calclipse.math.matrix.Matrix;

/**
 * A constraint with respect to matrix size.
 * A constraint is check on creation,
 * and the resulting instance is either violated or not.
 * 
 * @see {@link com.calclipse.math.matrix.size.Constraints}
 * 
 * @author Tone Sommerland
 */
public final class Constraint
{
    private final Size[] violations;
    
    private Constraint(final Size[] violations)
    {
        this.violations = violations.clone();
    }
    
    /**
     * Returns a constraint that is violated
     * if the given condition is {@code false} for any of the given matrices.
     * @param condition the condition to check
     * @param matrices the matrices to check
     */
    public static Constraint check(
            final Predicate<? super Size> condition, final Matrix... matrices)
    {
        requireNonNull(condition, "condition");
        requireNonNull(matrices, "matrices");
        final Size[] violations = Stream.of(matrices)
                .map(Size::of)
                .filter(condition.negate())
                .toArray(Size[]::new);
        return new Constraint(violations);
    }
    
    /**
     * Whether or not this constraint is violated.
     */
    public boolean isViolated()
    {
        return violations.length > 0;
    }
    
    /**
     * Enforces this constraint.
     * @param message exception message if violated
     * @throws SizeViolation if this constraint is violated
     */
    public void enforce(final String message)
    {
        if (violations.length > 0)
        {
            throw new SizeViolation(
                    enrichMessage(message, violations), violations);
        }
    }
    
    private static String enrichMessage(
            final String message, final Size[] violations)
    {
        return message
                + Stream.of(violations)
                    .map(Size::toString)
                    .collect(joining(", ", ": ", ""));
    }

    @Override
    public String toString()
    {
        final var builder = new StringBuilder(Constraint.class.getSimpleName());
        builder.append(": violations=[");
        builder.append(
                Stream.of(violations)
                    .map(Size::toString)
                    .collect(joining(", ")));
        builder.append(']');
        return builder.toString();
    }

}
