package com.calclipse.math.matrix.function;

/**
 * A consumer that accepts a row index, a column index and the value.
 * 
 * @see com.calclipse.math.matrix.Matrix
 * #acceptRowColumnValueConsumer(RowColumnValueConsumer)
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface RowColumnValueConsumer
{
    /**
     * Performs this operation.
     * @param row the row index
     * @param column the column index
     * @param value the current value of the element
     */
    public abstract void accept(int row, int column, double value);

}
