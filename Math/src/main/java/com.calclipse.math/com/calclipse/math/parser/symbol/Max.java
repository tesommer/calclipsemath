package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.InvalidComparison;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Returns the maximum of its arguments.
 * Arguments containing arrays are treated
 * as collections of objects to compare.
 * Returns undef if there are no objects to compare.
 * Supports:
 * <ul>
 * <li>[array|*[, ..., array|*]]</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Max implements VariableArityOperation
{
    public static final String NAME = "max";
    
    private static final  Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Max(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Max(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final Collection<Object> objects = collect(args);
        if (objects.isEmpty())
        {
            return Values.UNDEF;
        }
        try
        {
            final Object result = objects.stream()
                    .max(typeContext.comparer().asComparator()).get();
            return Value.constantOf(result);
        }
        catch (final InvalidComparison ex)
        {
            throw ex.getCause();
        }
    }
    
    /**
     * Collects all the values to be compared in a single list.
     * Returns an empty list if there are none.
     */
    static List<Object> collect(final Value[] args)
    {
        final var objects = new ArrayList<Object>();
        Stream.of(args).forEach(arg -> collect(arg, objects));
        return objects;
    }
    
    private static void collect(
            final Value arg, final Collection<Object> objects)
    {
        final Object value = arg.get();
        if (value instanceof Array)
        {
            objects.addAll((Array)value);
        }
        else
        {
            objects.add(value);
        }
    }

}
