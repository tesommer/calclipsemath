package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.AssociativeArray;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Single-index accessor.
 * Expects the index on the left.
 * 
 * @author Tone Sommerland
 */
public final class At implements BinaryOperation
{
    public static final String NAME = "@";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private At(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.VERY_HIGH, new At(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        if (right.get() instanceof AssociativeArray)
        {
            return Get.evaluateAssociativeArray(right, left);
        }
        final int index = TypeUtil.argToInt(1, left.get());
        return typeContext.indexer().get(right, index);
    }

}
