package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import java.util.Optional;

/**
 * Thrown when an exception occurs during
 * parsing or evaluation of a mathematical expression.
 * Exceptions of this type will typically contain messages
 * intended for the end user.
 * An instance may also contain a
 * {@link #detail() detail fragment}
 * specifying the token and position in the expression
 * that resulted in this exception.
 * This exception type has suppression disabled and not writable stack trace.
 * 
 * @author Tone Sommerland
 */
public class ErrorMessage extends Exception
{
    private static final long serialVersionUID = 1L;
    
    /**
     * Builds {@link ErrorMessage} instances.
     * 
     * @author Tone Sommerland
     */
    protected static class Builder
    {
        private String message;
        private Throwable cause;
        private Fragment detail;
        
        /**
         * Creates a new {@link ErrorMessage} builder.
         * @param message a user-level detail message
         */
        protected Builder(final String message)
        {
            this.message = requireNonNull(message, "message");
        }
        
        /**
         * Creates a new builder initialized with
         * the message, cause and detail of the given exception.
         * @param ex starting point
         */
        protected Builder(final ErrorMessage ex)
        {
            this(ex.getMessage());
            this.cause = ex.getCause();
            this.detail = ex.detail;
        }

        /**
         * Sets the detail message.
         * @param message the message
         * @return {@code this}
         */
        public final Builder setMessage(final String message)
        {
            this.message = requireNonNull(message, "message");
            return this;
        }

        /**
         * Sets the cause.
         * @param cause the cause (nullable)
         * @return {@code this}
         */
        public final Builder setCause(final Throwable cause)
        {
            this.cause = cause;
            return this;
        }

        /**
         * Sets the detail.
         * @param detail the detail (nullable)
         * @return {@code this}
         */
        public final Builder setDetail(final Fragment detail)
        {
            this.detail = detail;
            return this;
        }

        /**
         * Builds the exception.
         * Subclasses should override this method
         * to return an instance of the appropriate class.
         */
        protected ErrorMessage build()
        {
            return new ErrorMessage(this);
        }
    }
    
    private final transient Fragment detail;
    
    /**
     * Creates an instance
     * using the message, cause and detail of the given builder.
     */
    protected ErrorMessage(final Builder builder)
    {
        super(builder.message, builder.cause, false, false);
        assert builder.message != null;
        this.detail = builder.detail;
    }
    
    /**
     * Returns an error message with the given message.
     */
    static ErrorMessage errorMessageOf(final String message)
    {
        return new Builder(message).build();
    }
    
    /**
     * Returns a builder
     * initialized with the message, cause and detail of this instance.
     * Subclasses should override this method
     * to return an instance of the appropriate class.
     */
    protected Builder builder()
    {
        return new Builder(this);
    }
    
    /**
     * Returns an instance derived from this instance,
     * but with the given cause.
     * @param cause the cause (nullable)
     * @implSpec Self-uses {@link #builder()}.
     */
    public final ErrorMessage withCause(final Throwable cause)
    {
        return builder().setCause(cause).build();
    }
    
    /**
     * Returns an instance derived from this instance,
     * but with the given detail attached.
     * This method does not accept {@code null}.
     * @implSpec Self-uses {@link #builder()}.
     */
    public final ErrorMessage attach(final Fragment detail)
    {
        return builder().setDetail(requireNonNull(detail, "detail")).build();
    }

    /**
     * A fragment containing the token that resulted in this exception,
     * in addition to the token's position in the original expression.
     */
    public final Optional<Fragment> detail()
    {
        return Optional.ofNullable(detail);
    }

}
