package com.calclipse.math.parser.step;

import java.util.List;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.TokenType;

/**
 * Infix-to-postfix transformation.
 * 
 * @author Tone Sommerland
 */
public enum Postfix implements ParseStep
{
    INSTANCE;

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        final TokenType type = stream.input().token().type();
        if (type.isLeftParenthesis() || type.isLeftUnary())
        {
            push(stream);
        }
        else if (type.isRightParenthesis())
        {
            readRParen(stream);
        }
        else if (type.isBinary() || type.isRightUnary())
        {
            readBOperatorOrRuOperator(stream, type);
        }
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
        for (Fragment frag = pop(stream); frag != null; frag = pop(stream))
        {
            stream.write(frag);
        }
    }
    
    private static Fragment pop(final TokenStream stream)
    {
        final List<Fragment> buffer = stream.buffer();
        return buffer.isEmpty() ? null : buffer.remove(buffer.size() - 1);
    }
    
    private static void push(final TokenStream stream)
    {
        stream.buffer().add(stream.input());
        stream.skip();
    }
    
    private static boolean supersedes(final Fragment frag, final int priority)
    {
        if (frag.token().type().isLeftParenthesis())
        {
            return false;
        }
        else
        {
            final int priority2 = ((Operator)frag.token()).priority();
            return priority2 >= priority;
        }
    }

    private static void readBOperatorOrRuOperator(
            final TokenStream stream,
            final TokenType type) throws ErrorMessage
    {
        final int priority
            = ((Operator)stream.input().token()).priority();
        for (Fragment frag = pop(stream); frag != null;)
        {
            if (supersedes(frag, priority))
            {
                stream.write(frag);
                frag = pop(stream);
            }
            else
            {
                stream.buffer().add(frag);
                frag = null;
            }
        }
        if (type.isBinary())
        {
            push(stream);
        }
    }

    private static void readRParen(final TokenStream stream)
            throws ErrorMessage
    {
        for (Fragment frag = pop(stream);
            !frag.token().type().isLeftParenthesis();
            frag = pop(stream))
        {
            stream.write(frag);
        }
        stream.skip();
    }

}
