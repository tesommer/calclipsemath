package com.calclipse.math.expression.message;

import static java.util.Objects.requireNonNull;

import java.text.MessageFormat;

/**
 * Evaluation-related user-level error messages.
 * Each message has a static accessor and a corresponding mutator in
 * {@link Builder}.
 * Any parameters accepted by a message's accessor are used as format elements.
 * This means that the number of format elements of each message
 * can be determined by looking at the message's accessor.
 * (See {@code java.text.MessageFormat} for information on the format pattern.)
 * 
 * This class has the following messages:
 * 
 * <ul>
 * 
 * <li>{@code mismatchingDimensions}:
 * An operation was invoked on objects with mismatching dimensions.
 * Example: Adding a 2x3 matrix to a 2x3000000000000000000000000000 matrix.</li>
 * 
 * <li>{@code invalidDimensionCount}:
 * An invalid number of dimensions was specified.</li>
 * 
 * <li>{@code invalidIndex}:
 * An invalid index was specified.</li>
 * 
 * <li>{@code invalidIndexCount}:
 * An invalid number of indices was specified.</li>
 * 
 * <li>{@code invalidSize}:
 * Indicates that the size of an object is invalid for an operation,
 * or that an invalid size has been specified.</li>
 * 
 * <li>{@code singularMatrix}:
 * Singular matrix.</li>
 * 
 * <li>{@code inconsistentSystem}:
 * Inconsistent system of equations.</li>
 * 
 * <li>{@code divisionByZero}:
 * Division by zero.</li>
 * 
 * <li>{@code irrationalNumber}:
 * Attempt to turn an irrational number into a fraction.</li>
 * 
 * <li>{@code domainError}:
 * An argument to an operation was outside the domain of the operation.</li>
 * 
 * <li>{@code expectedInteger}:
 * A decimal number was specified were an integer was expected.</li>
 * 
 * <li>{@code illegalAssignment}:
 * Attempt to assign an unassignable value.</li>
 * 
 * <li>{@code syntaxError}:
 * This one is classic.</li>
 * 
 * <li>{@code indefiniteReqursion}:
 * Unrestricted recursion.</li>
 * 
 * <li>{@code interruption}:
 * The thread was interrupted.</li>
 * 
 * <li>{@code forImplicitMultiplication}:
 * Used for formatting an error message
 * to say that it was caused by implicit multiplication.</li>
 * 
 * <li>{@code messageFor}:
 * Used for formatting an error message
 * to specify in further detail what the message applies to.</li>
 * 
 * </ul>
 * 
 * This class is thread safe.
 * 
 * @author Tone Sommerland
 */
public final class EvalMessages
{
    private static final class Internals
    {
        private String
        mismatchingDimensions = "Mismatching dimensions.";
        
        private String
        invalidDimensionCount = "Wrong number of dimensions.";
        
        private String
        invalidIndex = "Invalid index.";
        
        private String
        invalidIndexCount = "Wrong number of indices.";
        
        private String
        invalidSize = "Invalid size.";

        private String
        singularMatrix = "Singular matrix.";
        
        private String
        inconsistentSystem = "Inconsistent system.";
        
        private String
        divisionByZero = "Division by zero.";
        
        private String
        irrationalNumber = "Irrational number.";
        
        private String
        domainError = "Domain error.";
        
        private String
        expectedInteger = "Integer required.";
        
        private String
        illegalAssignment = "Illegal assignment.";

        private String
        syntaxError = "Syntax error.";
        
        private String
        indefiniteRecursion = "Indefinite recursion.";
        
        private String
        interruption = "An operation was cancelled.";
        
        private String
        forImplicitMultiplication = "{0} (Implicit multiplication)";
        
        private String
        messageFor = "{0} ({1})";

        private Internals()
        {
        }
    }
    
    /**
     * Builds instances of {@link EvalMessages}.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final Internals internals = new Internals();

        /**
         * Creates a new builder
         * with a default set of messages as a starting point.
         */
        public Builder()
        {
        }

        /**
         * Sets the {@code mismatchingDimensions} message.
         * @param mismatchingDimensions a message with zero format arguments
         * @return {@code this}
         */
        public Builder setMismatchingDimensions(
                final String mismatchingDimensions)
        {
            internals.mismatchingDimensions = requireNonNull(
                    mismatchingDimensions, "mismatchingDimensions");
            return this;
        }

        /**
         * Sets the {@code invalidDimensionCount} message.
         * @param invalidDimensionCount a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInvalidDimensionCount(
                final String invalidDimensionCount)
        {
            internals.invalidDimensionCount = requireNonNull(
                    invalidDimensionCount, "invalidDimensionCount");
            return this;
        }

        /**
         * Sets the {@code invalidIndex} message.
         * @param invalidIndex a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInvalidIndex(final String invalidIndex)
        {
            internals.invalidIndex = requireNonNull(
                    invalidIndex, "invalidIndex");
            return this;
        }

        /**
         * Sets the {@code invalidIndexCount} message.
         * @param invalidIndexCount a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInvalidIndexCount(final String invalidIndexCount)
        {
            internals.invalidIndexCount = requireNonNull(
                    invalidIndexCount, "invalidIndexCount");
            return this;
        }

        /**
         * Sets the {@code invalidSize} message.
         * @param invalidSize a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInvalidSize(final String invalidSize)
        {
            internals.invalidSize = requireNonNull(invalidSize, "invalidSize");
            return this;
        }

        /**
         * Sets the {@code singularMatrix} message.
         * @param singularMatrix a message with zero format arguments
         * @return {@code this}
         */
        public Builder setSingularMatrix(final String singularMatrix)
        {
            internals.singularMatrix = requireNonNull(
                    singularMatrix, "singularMatrix");
            return this;
        }

        /**
         * Sets the {@code inconsistentSystem} message.
         * @param inconsistentSystem a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInconsistentSystem(final String inconsistentSystem)
        {
            internals.inconsistentSystem = requireNonNull(
                    inconsistentSystem, "inconsistentSystem");
            return this;
        }

        /**
         * Sets the {@code divisionByZero} message.
         * @param divisionByZero a message with zero format arguments
         * @return {@code this}
         */
        public Builder setDivisionByZero(final String divisionByZero)
        {
            internals.divisionByZero = requireNonNull(
                    divisionByZero, "divisionByZero");
            return this;
        }

        /**
         * Sets the {@code irrationalNumber} message.
         * @param irrationalNumber a message with zero format arguments
         * @return {@code this}
         */
        public Builder setIrrationalNumber(final String irrationalNumber)
        {
            internals.irrationalNumber = requireNonNull(
                    irrationalNumber, "irrationalNumber");
            return this;
        }

        /**
         * Sets the {@code domainError} message.
         * @param domainError a message with zero format arguments
         * @return {@code this}
         */
        public Builder setDomainError(final String domainError)
        {
            internals.domainError = requireNonNull(domainError, "domainError");
            return this;
        }

        /**
         * Sets the {@code expectedInteger} message.
         * @param expectedInteger a message with zero format arguments
         * @return {@code this}
         */
        public Builder setExpectedInteger(final String expectedInteger)
        {
            internals.expectedInteger = requireNonNull(
                    expectedInteger, "expectedInteger");
            return this;
        }

        /**
         * Sets the {@code illegalAssignment} message.
         * @param illegalAssignment a message with zero format arguments
         * @return {@code this}
         */
        public Builder setIllegalAssignment(final String illegalAssignment)
        {
            internals.illegalAssignment = requireNonNull(
                    illegalAssignment, "illegalAssignment");
            return this;
        }

        /**
         * Sets the {@code syntaxError} message.
         * @param syntaxError a message with zero format arguments
         * @return {@code this}
         */
        public Builder setSyntaxError(final String syntaxError)
        {
            internals.syntaxError = requireNonNull(syntaxError, "syntaxError");
            return this;
        }

        /**
         * Sets the {@code indefiniteRecursion} message.
         * @param indefiniteRecursion a message with zero format arguments
         * @return {@code this}
         */
        public Builder setIndefiniteRecursion(
                final String indefiniteRecursion)
        {
            internals.indefiniteRecursion = requireNonNull(
                    indefiniteRecursion, "indefiniteRecursion");
            return this;
        }

        /**
         * Sets the {@code interruption} message.
         * @param interruption a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInterruption(final String interruption)
        {
            internals.interruption = requireNonNull(
                    interruption, "interruption");
            return this;
        }

        /**
         * Sets the {@code forImplicitMultiplication} message.
         * @param forImplicitMultiplication a message with one format argument
         * @return {@code this}
         */
        public Builder setForImplicitMultiplication(
                final String forImplicitMultiplication)
        {
            internals.forImplicitMultiplication = requireNonNull(
                    forImplicitMultiplication, "forImplicitMultiplication");
            return this;
        }

        /**
         * Sets the {@code messageFor} message.
         * @param messageFor a message with two format arguments
         * @return {@code this}
         */
        public Builder setMessageFor(final String messageFor)
        {
            internals.messageFor = requireNonNull(messageFor, "messageFor");
            return this;
        }
        
        /**
         * Builds the evaluation messages.
         */
        public EvalMessages build()
        {
            return new EvalMessages(internals);
        }
    }
    
    private static volatile EvalMessages
    instance = new EvalMessages(new Internals());
    
    private final String mismatchingDimensions;
    private final String invalidDimensionCount;
    private final String invalidIndex;
    private final String invalidIndexCount;
    private final String invalidSize;
    private final String singularMatrix;
    private final String inconsistentSystem;
    private final String divisionByZero;
    private final String irrationalNumber;
    private final String domainError;
    private final String expectedInteger;
    private final String illegalAssignment;
    private final String syntaxError;
    private final String indefiniteRecursion;
    private final String interruption;
    private final String forImplicitMultiplication;
    private final String messageFor;

    private EvalMessages(final Internals internals)
    {
        assert internals.mismatchingDimensions     != null;
        assert internals.invalidDimensionCount     != null;
        assert internals.invalidIndex              != null;
        assert internals.invalidIndexCount         != null;
        assert internals.invalidSize               != null;
        assert internals.singularMatrix            != null;
        assert internals.inconsistentSystem        != null;
        assert internals.divisionByZero            != null;
        assert internals.irrationalNumber          != null;
        assert internals.domainError               != null;
        assert internals.expectedInteger           != null;
        assert internals.illegalAssignment         != null;
        assert internals.syntaxError               != null;
        assert internals.indefiniteRecursion       != null;
        assert internals.interruption              != null;
        assert internals.forImplicitMultiplication != null;
        assert internals.messageFor                != null;
        this.mismatchingDimensions     = internals.mismatchingDimensions;
        this.invalidDimensionCount     = internals.invalidDimensionCount;
        this.invalidIndex              = internals.invalidIndex;
        this.invalidIndexCount         = internals.invalidIndexCount;
        this.invalidSize               = internals.invalidSize;
        this.singularMatrix            = internals.singularMatrix;
        this.inconsistentSystem        = internals.inconsistentSystem;
        this.divisionByZero            = internals.divisionByZero;
        this.irrationalNumber          = internals.irrationalNumber;
        this.domainError               = internals.domainError;
        this.expectedInteger           = internals.expectedInteger;
        this.illegalAssignment         = internals.illegalAssignment;
        this.syntaxError               = internals.syntaxError;
        this.indefiniteRecursion       = internals.indefiniteRecursion;
        this.interruption              = internals.interruption;
        this.forImplicitMultiplication = internals.forImplicitMultiplication;
        this.messageFor                = internals.messageFor;
    }
    
    /**
     * Sets the main instance.
     */
    public static void set(final EvalMessages instance)
    {
        EvalMessages.instance = requireNonNull(instance, "instance");
    }

    /**
     * Returns the {@code mismatchingDimensions} message.
     */
    public static String mismatchingDimensions()
    {
        return instance.mismatchingDimensions;
    }

    /**
     * Returns the {@code invalidDimensionCount} message.
     */
    public static String invalidDimensionCount()
    {
        return instance.invalidDimensionCount;
    }

    /**
     * Returns the {@code invalidIndex} message.
     */
    public static String invalidIndex()
    {
        return instance.invalidIndex;
    }

    /**
     * Returns the {@code invalidIndexCount} message.
     */
    public static String invalidIndexCount()
    {
        return instance.invalidIndexCount;
    }

    /**
     * Returns the {@code invalidSize} message.
     */
    public static String invalidSize()
    {
        return instance.invalidSize;
    }

    /**
     * Returns the {@code singularMatrix} message.
     */
    public static String singularMatrix()
    {
        return instance.singularMatrix;
    }

    /**
     * Returns the {@code inconsistentSystem} message.
     */
    public static String inconsistentSystem()
    {
        return instance.inconsistentSystem;
    }

    /**
     * Returns the {@code divisionByZero} message.
     */
    public static String divisionByZero()
    {
        return instance.divisionByZero;
    }

    /**
     * Returns the {@code irrationalNumber} message.
     */
    public static String irrationalNumber()
    {
        return instance.irrationalNumber;
    }

    /**
     * Returns the {@code domainError} message.
     */
    public static String domainError()
    {
        return instance.domainError;
    }

    /**
     * Returns the {@code expectedInteger} message.
     */
    public static String expectedInteger()
    {
        return instance.expectedInteger;
    }

    /**
     * Returns the {@code illegalAssignment} message.
     */
    public static String illegalAssignment()
    {
        return instance.illegalAssignment;
    }

    /**
     * Returns the {@code syntaxError} message.
     */
    public static String syntaxError()
    {
        return instance.syntaxError;
    }

    /**
     * Returns the {@code indefiniteRecursion} message.
     */
    public static String indefiniteRecursion()
    {
        return instance.indefiniteRecursion;
    }

    /**
     * Returns the {@code interruption} message.
     */
    public static String interruption()
    {
        return instance.interruption;
    }

    /**
     * Returns a formatted {@code forImplicitMultiplication} message.
     * @param message the main message
     */
    public static String forImplicitMultiplication(final Object message)
    {
        return MessageFormat.format(
                instance.forImplicitMultiplication,
                requireNonNull(message, "message"));
    }

    /**
     * Returns a formatted {@code messageFor} message.
     * @param message the main message
     * @param forWhat what the main message is for
     */
    public static String messageFor(
            final Object message, final Object forWhat)
    {
        return MessageFormat.format(
                instance.messageFor,
                requireNonNull(message, "message"),
                requireNonNull(forWhat, "forWhat"));
    }

    @Override
    public String toString()
    {
        return EvalMessages.class.getSimpleName() + ": "
                + mismatchingDimensions;
    }

}
