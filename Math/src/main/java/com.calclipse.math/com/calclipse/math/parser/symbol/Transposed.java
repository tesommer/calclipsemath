package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * The transpose of a matrix.
 * Supported types:
 * <ul>
 *  <li>matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Transposed implements UnaryOperation
{
    public static final String NAME = "^T";
    
    private static final Id ID = Id.unique();
    
    private static final UnaryOperation INSTANCE = new Transposed();

    private Transposed()
    {
    }
    
    public static UnaryOperator operator()
    {
        return UnaryOperator.postfixed(
                NAME, OperatorPriorities.MEDIUM, INSTANCE).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            return Value.constantOf(mx.transposed());
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
