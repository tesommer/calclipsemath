package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The integer part of a real number.
 * Supported types:
 * <ul>
 *  <li>real</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class IntegerPart implements UnaryOperation
{
    public static final String NAME = "ipart";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private IntegerPart(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new IntegerPart(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final double d = TypeUtil.toDouble(arg.get());
        return Value.constantOf(typeContext.numberType().valueOf((long)d));
    }

}
