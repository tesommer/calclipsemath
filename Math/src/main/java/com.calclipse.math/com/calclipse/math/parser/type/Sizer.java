package com.calclipse.math.parser.type;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.Value;

/**
 * Defines how to access the size of data types with one or more dimensions.
 * 
 * @author Tone Sommerland
 */
public interface Sizer
{
    /**
     * Accesses the size of the given value.
     * Re-sizing might be permitted through the returned value's mutator.
     */
    public abstract Value sizeOf(Value value) throws ErrorMessage;

}
