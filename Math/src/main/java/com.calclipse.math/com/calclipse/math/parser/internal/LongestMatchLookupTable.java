package com.calclipse.math.parser.internal;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.calclipse.math.expression.Token;
import com.calclipse.math.parser.SymbolTable;

/**
 * Longest-match lookup-table.
 * 
 * @author Tone Sommerland
 */
public final class LongestMatchLookupTable implements SymbolTable
{
    /*
     * Values are lists of tokens with names starting with the same char.
     * Keys are the char that the names in the associated list begin with.
     * Tokens are ordered descendingly by the length of their names.
     */
    private final Map<String, List<Token>> symbols = new HashMap<>();

    public LongestMatchLookupTable()
    {
    }

    @Override
    public boolean add(final Token token)
    {
        final String key = keyOf(token.name());
        List<Token> tokens = symbols.get(key);
        if (tokens == null)
        {
            tokens = new ArrayList<>();
            tokens.add(token);
            symbols.put(key, tokens);
            return true;
        }
        return add(tokens, token);
    }

    @Override
    public boolean remove(final String name)
    {
        if (name.isEmpty())
        {
            return false;
        }
        final String key = keyOf(name);
        final Collection<Token> tokens = symbols.get(key);
        if (tokens == null)
        {
            return false;
        }
        final Token token = remove(tokens, name);
        if (tokens.isEmpty())
        {
            symbols.remove(key);
        }
        return token != null;
    }

    @Override
    public Token get(final String name)
    {
        if (name.isEmpty())
        {
            return null;
        }
        final Collection<Token> tokens = symbols.get(keyOf(name));
        return tokens == null ? null : get(tokens, name);
    }

    @Override
    public Collection<Token> all()
    {
        return symbols.values().stream()
                .flatMap(List::stream)
                .collect(toList());
    }

    @Override
    public Collection<Token> lookUp(final char firstChar)
    {
        final Collection<Token> tokens = symbols.get(keyOf(firstChar));
        if (tokens == null)
        {
            return List.of();
        }
        else
        {
            return Collections.unmodifiableCollection(tokens);
        }
    }

    @Override
    public void clear()
    {
        symbols.clear();
    }
    
    private static String keyOf(final String name)
    {
        return keyOf(name.charAt(0));
    }
    
    private static String keyOf(final char firstChar)
    {
        return String.valueOf(firstChar);
    }

    private static boolean add(
            final List<Token> tokens, final Token token)
    {
        for (int i = 0; i < tokens.size(); i++)
        {
            final Token token2 = tokens.get(i);
            if (token2.name().length() < token.name().length())
            {
                tokens.add(i, token);
                return true;
            }
            else if (token.matches(token2))
            {
                return false;
            }
        }
        tokens.add(token);
        return true;
    }
    
    private static <T extends Token> T remove(
            final Collection<T> tokens, final String name)
    {
        for (final Iterator<T> it = tokens.iterator(); it.hasNext();)
        {
            final T token = it.next();
            if (token.name().length() < name.length())
            {
                return null;
            }
            else if (token.matches(name))
            {
                it.remove();
                return token;
            }
        }
        return null;
    }
    
    private static <T extends Token> T get(
            final Collection<T> tokens, final String name)
    {
        for (final T token : tokens)
        {
            if (token.name().length() < name.length())
            {
                return null;
            }
            else if (token.matches(name))
            {
                return token;
            }
        }
        return null;        
    }

}
