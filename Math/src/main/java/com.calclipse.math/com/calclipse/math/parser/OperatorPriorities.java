package com.calclipse.math.parser;

/**
 * Standard operator priorities.
 * <ul>
 * <li>{@code VERY_LOW}: -200</li>
 * <li>{@code ASSIGNMENT}: -100</li>
 * <li>{@code LOGICAL}: 0</li>
 * <li>{@code TEST}: 100</li>
 * <li>{@code LOW}: 200</li>
 * <li>{@code SUMMATION}: 300</li>
 * <li>{@code PRODUCT}: 400</li>
 * <li>{@code MEDIUM}: 500</li>
 * <li>{@code NEGATION}: 600</li>
 * <li>{@code IMPLICIT_MULTIPLICATION}: 700</li>
 * <li>{@code EXPONENTIATION}: 800</li>
 * <li>{@code HIGH}: 900</li>
 * <li>{@code VERY_HIGH}: 1000</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class OperatorPriorities
{
    public static final int VERY_LOW = -200;
    
    public static final int ASSIGNMENT = -100;
    
    /**
     * E.g.: {@code and}, {@code or}, {@code xor}.
     */
    public static final int LOGICAL = 0;
    
    /**
     * E.g.:
     * {@code <}, {@code <=},
     * {@code >}, {@code >=},
     * {@code =}, {@code <>}.
     */
    public static final int TEST = 100;
    
    public static final int LOW = 200;
    
    /**
     * E.g.: {@code +}, {@code -}.
     */
    public static final int SUMMATION = 300;
    
    /**
     * E.g.: {@code *}, {@code /}.
     */
    public static final int PRODUCT = 400;
    
    public static final int MEDIUM = 500;
    
    /**
     * E.g.: {@code -}, {@code not}.
     */
    public static final int NEGATION = 600;
    
    public static final int IMPLICIT_MULTIPLICATION = 700;
    
    /**
     * E.g.: {@code ^}.
     */
    public static final int EXPONENTIATION = 800;
    
    public static final int HIGH = 900;
    
    public static final int VERY_HIGH = 1000;
    
    private OperatorPriorities()
    {
    }

}
