package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.stream.Stream;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.type.Association;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Left brace, used for creating arrays and associative arrays.
 * 
 * @author Tone Sommerland
 */
public final class Braces implements VariableArityOperation
{
    public static final String NAME = "{";
    
    public static final Token CLOSER = Tokens.plain("}");
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private Braces(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(final TypeContext typeContext)
    {
        return Function.of(
                NAME,
                Delimitation.byTokensWithoutOpener(
                        CLOSER, ParensAndComma.COMMA, Token::identifiesAs),
                new Braces(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        if (args.length > 0)
        {
            final Object first = args[0].get();
            if (first instanceof Association)
            {
                return evaluateAssociations((Association)first, args);
            }
        }
        final var arr = typeContext.arrayType().arrayOf(
                Stream.of(args).map(Value::get).toArray());
        return Value.constantOf(arr);
    }
    
    private Value evaluateAssociations(
            final Association first, final Value[] args) throws ErrorMessage
    {
        final var associations = new Association[args.length];
        associations[0] = first;
        for (int i = 1; i < args.length; i++)
        {
            final Object next = args[i].get();
            if (!(next instanceof Association))
            {
                throw errorMessage(TypeMessages.invalidType(next));
            }
            associations[i] = (Association)next;
        }
        return Value.constantOf(typeContext.associativeArrayType()
                .associativeArrayOf(associations));
    }

}
