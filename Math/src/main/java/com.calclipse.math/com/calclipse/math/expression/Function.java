package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;

/**
 * A function in a mathematical expression.
 * A function in an
 * {@link Expression}
 * represents a node in a tree structure.
 * The leaves are the
 * {@link #arguments() function arguments},
 * which are themselves expressions.
 * Functions are automatically replicated during parsing.
 * Each replica given its own argument list,
 * which may grow as the parsing continues.
 * Once the parsing has stopped,
 * the functions in the resulting expression will remain unchanged.
 * A function instance will contain either an instance of
 * {@link com.calclipse.math.expression.operation.VariableArityOperation}
 * or
 * {@link com.calclipse.math.expression.operation.ConditionalOperation}.
 * 
 * @author Tone Sommerland
 */
public final class Function extends Token
{
    private static final String
    EMPTY_OPERATION_MESSAGE = "This function is conditional.";
    
    private static final String
    EMPTY_CONDITIONAL_MESSAGE = "This function is unconditional.";
    
    private static final VariableArityOperation
    EMPTY_OPERATION = Function::emptyOperation;
    
    private static final ConditionalOperation
    EMPTY_CONDITIONAL = Function::emptyConditional;
    
    private final List<Expression> arguments;
    private final Delimitation delimitation;
    private final VariableArityOperation operation;
    private final ConditionalOperation conditional;
    
    private Function(
            final Id id,
            final String name,
            final List<Expression> arguments,
            final Delimitation delimitation,
            final VariableArityOperation operation,
            final ConditionalOperation conditional)
    {
        super(TokenType.FUNCTION, id, name);
        this.arguments = requireNonNull(arguments, "arguments");
        this.delimitation = requireNonNull(delimitation, "delimitation");
        this.operation = requireNonNull(operation, "operation");
        this.conditional = requireNonNull(conditional, "conditional");
    }
    
    /**
     * Returns an unconditional function,
     * one that contains an (ordinary) variable arity operation.
     * @param name the name of the function
     * @param delimitation the argument delimitation
     * @param operation the operation of this function
     * @throws IllegalArgumentException if the name is empty
     */
    public static Function of(
            final String name,
            final Delimitation delimitation,
            final VariableArityOperation operation)
    {
        return new Function(
                Id.unique(),
                name,
                freshArguments(),
                delimitation,
                operation,
                EMPTY_CONDITIONAL);
    }
    
    /**
     * Returns a conditional function,
     * one that contains a conditional operation.
     * @param name the name of the function
     * @param delimitation the argument delimitation
     * @param conditional the conditional operation of this function
     * @throws IllegalArgumentException if the name is empty
     */
    public static Function ofConditional(
            final String name,
            final Delimitation delimitation,
            final ConditionalOperation conditional)
    {
        return new Function(
                Id.unique(),
                name,
                freshArguments(),
                delimitation,
                EMPTY_OPERATION,
                conditional);
    }

    /**
     * The argument list.
     */
    public List<Expression> arguments()
    {
        return Collections.unmodifiableList(arguments);
    }
    
    /**
     * The argument delimiters.
     */
    public Delimiters delimiters()
    {
        return delimitation.delimiters();
    }
    
    /**
     * Whether or not this function contains
     * an ordinary operation or a conditional operation.
     */
    public boolean isConditional()
    {
        return conditional != EMPTY_CONDITIONAL;
    }

    /**
     * Returns the operation of this function.
     * @throws NoSuchElementException if this function is
     * {@link #isConditional() conditional}
     */
    public VariableArityOperation operation()
    {
        if (operation == EMPTY_OPERATION)
        {
            throw new NoSuchElementException(EMPTY_OPERATION_MESSAGE);
        }
        return operation;
    }

    /**
     * Returns the conditional operation of this function.
     * @throws NoSuchElementException if this function is not
     * {@link #isConditional() conditional}
     */
    public ConditionalOperation conditional()
    {
        if (conditional == EMPTY_CONDITIONAL)
        {
            throw new NoSuchElementException(EMPTY_CONDITIONAL_MESSAGE);
        }
        return conditional;
    }
    
    @Override
    public Function withId(final Id newId)
    {
        return new Function(
                newId, name, arguments, delimitation, operation, conditional);
    }
    
    @Override
    public Function withName(final String newName)
    {
        return new Function(
                id, newName, arguments, delimitation, operation, conditional);
    }
    
    /**
     * Returns a function similar to this, but with the given argument list.
     * @param newArguments the new arguments
     */
    public Function withArguments(final Collection<Expression> newArguments)
    {
        return new Function(
                id,
                name,
                new ArrayList<>(newArguments),
                delimitation,
                operation,
                conditional);
    }
    
    /**
     * Returns a function similar to this,
     * but with the given operation.
     * The returned function will have no conditional operation.
     * @param newOperation the new operation
     */
    public Function withOperation(final VariableArityOperation newOperation)
    {
        return new Function(
                id,
                name,
                arguments,
                delimitation,
                newOperation,
                EMPTY_CONDITIONAL);
    }
    
    /**
     * Returns a function similar to this,
     * but with the given conditional operation.
     * The returned function will have no unconditional operation.
     * @param newConditional the new conditional operation
     */
    public Function withConditional(final ConditionalOperation newConditional)
    {
        return new Function(
                id,
                name,
                arguments,
                delimitation,
                EMPTY_OPERATION,
                newConditional);
    }

    Delimitation delimitation()
    {
        return delimitation;
    }

    Function spawn()
    {
        final Delimitation spawnedDelim = delimitation.spawn();
        final var offspring = new Function(
                id,
                name,
                freshArguments(),
                spawnedDelim,
                operation,
                conditional);
        return spawnedDelim.postSpawn(offspring, spawnedDelim);
    }
    
    void addArgument(final Expression argument)
    {
        arguments.add(argument);
    }
    
    private static List<Expression> freshArguments()
    {
        return new ArrayList<>();
    }
    
    private static Value emptyOperation(final Value[] args)
    {
        throw new AssertionError(EMPTY_OPERATION_MESSAGE);
    }
    
    private static Value emptyConditional(final List<Expression> args)
    {
        throw new AssertionError(EMPTY_CONDITIONAL_MESSAGE);
    }
    
}
