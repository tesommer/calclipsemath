package com.calclipse.math.parser.config.internal;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.Association;
import com.calclipse.math.parser.type.AssociativeArray;

public final class Stringification
{
    private static final String EOL = "\n";
    
    /**
     * Delimiters of a string representation of multiple elements.
     */
    private static final class Delims
    {
        private final String before;
        private final String between;
        private final String after;
        
        private Delims(
                final String before,
                final String between,
                final String after)
        {
            this.before = requireNonNull(before, "before");
            this.between = requireNonNull(between, "between");
            this.after = requireNonNull(after, "after");
        }
    }
    
    private static final Delims
    FIRST_ARRAY_DELIMS = new Delims("{", ", ", "}");
    
    private static final Delims
    REMAINING_ARRAY_DELIMS = new Delims(" ", "  ", "");
    
    private static final Delims
    FIRST_ASSOCIATION_DELIMS = new Delims("", " => ", "");
    
    private static final Delims
    REMAINING_ASSOCIATION_DELIMS = new Delims("", "    ", "");
    
    private static final Delims
    FIRST_ASSOCIATIVE_ARRAY_DELIMS = new Delims("{", ", ", "}");
    
    private static final Delims
    REMAINING_ASSOCIATIVE_ARRAY_DELIMS = new Delims(" ", "  ", "");

    private Stringification()
    {
    }

    static String stringify(final Array array)
    {
        return stringify(
                FIRST_ARRAY_DELIMS,
                REMAINING_ARRAY_DELIMS,
                array.toArray());
    }
    
    static String stringify(final Association association)
    {
        return stringify(
                FIRST_ASSOCIATION_DELIMS,
                REMAINING_ASSOCIATION_DELIMS,
                association.getKey(),
                association.getValue());
    }
    
    static String stringify(final AssociativeArray array)
    {
        return stringify(
                FIRST_ASSOCIATIVE_ARRAY_DELIMS,
                REMAINING_ASSOCIATIVE_ARRAY_DELIMS,
                array.entrySet().stream()
                    .map(entry -> stringify(
                            FIRST_ASSOCIATION_DELIMS,
                            REMAINING_ASSOCIATION_DELIMS,
                            entry.getKey(),
                            entry.getValue()))
                    .toArray());
    }
    
    /**
     * Stringifies an object array.
     * @param firstDelims for the first line
     * @param remainingDelims for any remaining lines
     * @param array the array to stringify
     * @return a, possibly multiline, string representation of the array
     */
    private static String stringify(
            final Delims firstDelims,
            final Delims remainingDelims,
            final Object... array)
    {
        if (array.length == 0)
        {
            return firstDelims.before + firstDelims.after;
        }
        final List<List<String>> paddedLines = paddedLines(array);
        final var buffer = new StringBuilder();
        appendFirstLineToBuffer(firstDelims, paddedLines, buffer);
        appendRemainingLinesToBuffer(remainingDelims, paddedLines, buffer);
        return buffer.toString();
    }

    private static void appendFirstLineToBuffer(
            final Delims delims,
            final Collection<? extends List<String>> paddedLines,
            final StringBuilder buffer)
    {
        final String firstLine = paddedLines.stream()
                .map(column -> column.get(0))
                .collect(joining(delims.between, delims.before, delims.after));
        buffer.append(firstLine);
    }

    private static void appendRemainingLinesToBuffer(
            final Delims delims,
            final List<? extends List<String>> paddedLines,
            final StringBuilder buffer)
    {
        final int maxLines = paddedLines.get(0).size();
        for (int line = 1; line < maxLines; line++)
        {
            buffer.append(EOL).append(delims.before);
            for (int column = 0; column < paddedLines.size(); column++)
            {
                if (line >= paddedLines.get(column).size())
                {
                    break;
                }
                if (column > 0)
                {
                    buffer.append(delims.between);
                }
                buffer.append(paddedLines.get(column).get(line));
            }
            buffer.append(delims.after);
        }
    }
    
    private static List<List<String>> paddedLines(final Object[] array)
    {
        final var paddedLines = new ArrayList<List<String>>();
        int lineCount = 0;
        for (int i = array.length - 1; i >= 0; i--)
        {
            final List<String> lines = linesIn(array[i].toString());
            final int maxLength
                = lines.stream().mapToInt(String::length).max().orElse(0);
            padLines(lines, lineCount, maxLength);
            lineCount = Math.max(lineCount, lines.size());
            paddedLines.add(0, lines);
        }
        return paddedLines;
    }
    
    private static List<String> linesIn(final String str)
    {
        return new ArrayList<>(List.of(str.split("(\r\n)|\r|\n")));
    }
    
    /**
     * Pads lines with space until their lengths reach maxLength.
     * If lineCount > lines.size(), lines containing only space
     * will be appended to the list.
     * Doesn't do anything if lineCount is zero.
     */
    private static void padLines(
            final List<? super String> lines,
            final int lineCount,
            final int maxLength)
    {
        final String format;
        if (maxLength > 0)
        {
            format = "%-" + maxLength + 's';
        }
        else
        {
            format = "%s";
        }
        for (int i = 0; i < lineCount; i++)
        {
            if (i < lines.size())
            {
                lines.set(i, String.format(format, lines.get(i)));
            }
            else
            {
                lines.add(i, String.format(format, ""));
            }
        }
    }

}
