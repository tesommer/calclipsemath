package com.calclipse.math.expression;

/**
 * Identification.
 * This class provides a means to
 * {@link com.calclipse.math.expression.Token#identifiesAs(Id) identify}
 * a
 * {@link com.calclipse.math.expression.Token}
 * by its origin rather than by its
 * {@link com.calclipse.math.expression.Token#matches(String) name}.
 * Identification is issued to each token at birth.
 * The ID then follows any derived tokens except those returned by
 * {@link com.calclipse.math.expression.Token#withId(Id)}.
 * 
 * @author Tone Sommerland
 */
public final class Id
{
    private Id()
    {
    }

    /**
     * Returns a unique instance of this class.
     */
    public static Id unique()
    {
        return new Id();
    }

    @Override
    public String toString()
    {
        return "ID: " + System.identityHashCode(this);
    }

}
