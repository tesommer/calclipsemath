package com.calclipse.math.parser.type;

import java.math.MathContext;
import java.math.RoundingMode;

import com.calclipse.math.expression.ErrorMessage;

/**
 * Represents the math parser's number type.
 * 
 * @author Tone Sommerland
 */
public interface NumberType
{
    /**
     * The default math context: 34 digits, rounding mode {@code HALF_UP}.
     * @see com.calclipse.math.Real
     */
    public static final MathContext
    DEFAULT_MATH_CONTEXT = new MathContext(34, RoundingMode.HALF_UP);
    
    /**
     * The default fracturability: 7.
     * @see com.calclipse.math.Real
     */
    public static final int DEFAULT_FRACTURABILITY = 7;

    /**
     * Parses a number literal.
     */
    public abstract Number parse(String literal) throws ErrorMessage;
    
    /**
     * Turns a {@code double} into a {@code Number}.
     */
    public abstract Number valueOf(double d);
    
    /**
     * Turns a {@code long} into a {@code Number}.
     */
    public abstract Number valueOf(long l);
    
    /**
     * Turns an {@code int} into a {@code Number}.
     * @implSpec The default implementation calls {@link #valueOf(long)}.
     */
    public default Number valueOf(final int i)
    {
        return valueOf((long)i);
    }
    
    /**
     * Turns a {@code Number} into a {@code Number}.
     * @implSpec The default implementation just returns its argument.
     */
    public default Number valueOf(final Number n)
    {
        return n;
    }
    
    /**
     * Returns the math context of this number type.
     * @implSpec
     * Returns {@link #DEFAULT_MATH_CONTEXT}.
     */
    public default MathContext mathContext()
    {
        return DEFAULT_MATH_CONTEXT;
    }
    
    /**
     * Returns the fracturability of this number type.
     * @implSpec
     * Returns {@link #DEFAULT_FRACTURABILITY}.
     */
    public default int fracturability()
    {
        return DEFAULT_FRACTURABILITY;
    }

}
