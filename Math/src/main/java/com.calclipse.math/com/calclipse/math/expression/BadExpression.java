package com.calclipse.math.expression;

/**
 * Indicates a malformed or improperly parsed
 * {@link com.calclipse.math.expression.Expression}.
 * 
 * @author Tone Sommerland
 */
public class BadExpression extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public BadExpression(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public BadExpression(final String message)
    {
        super(message);
    }

    public BadExpression(final Throwable cause)
    {
        super(cause);
    }

}
