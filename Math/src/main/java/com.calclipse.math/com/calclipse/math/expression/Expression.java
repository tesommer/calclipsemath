package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Stream;

import com.calclipse.math.expression.operation.Value;

/**
 * A parsed expression.
 * A parsed expression is essentially a sequence of
 * {@link com.calclipse.math.expression.Fragment}s
 * representing the expression on, say, RPN (postfix) form.
 * Instances of this class are primarily produced by a
 * {@link com.calclipse.math.expression.Parser},
 * but an expression may be created or manipulated by other means as well.
 * 
 * @author Tone Sommerland
 */
public final class Expression implements Iterable<Fragment>
{
    /**
     * Builds {@link Expression} instances.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final List<Fragment> fragments;
        private Evaluator evaluator;
        
        private Builder(
                final Collection<Fragment> fragments,
                final Evaluator evaluator)
        {
            this.fragments = new ArrayList<>(fragments);
            this.evaluator = requireNonNull(evaluator, "evaluator");
        }

        public Builder(final Expression expression)
        {
            this(expression.fragments, expression.evaluator);
        }
        
        /**
         * Creates a new expression builder
         * with an empty expression as a starting point.
         * @param evaluator the expression evaluator
         */
        public Builder(final Evaluator evaluator)
        {
            this(List.of(), evaluator);
        }

        /**
         * Sets the expression evaluator.
         * @param evaluator the evaluator
         * @return {@code this}
         */
        public Builder setEvaluator(final Evaluator evaluator)
        {
            this.evaluator = requireNonNull(evaluator, "evaluator");
            return this;
        }

        /**
         * Adds a fragment at the end.
         * @param fragment the fragment to add
         * @return {@code this}
         */
        public Builder add(final Fragment fragment)
        {
            fragments.add(requireNonNull(fragment, "fragment"));
            return this;
        }
        
        /**
         * Inserts a fragment at the specified index.
         * @param index the index to insert the fragment
         * @param fragment the fragment to insert
         * @return {@code this}
         * @throws IndexOutOfBoundsException if the index is invalid
         */
        public Builder add(final int index, final Fragment fragment)
        {
            fragments.add(index, requireNonNull(fragment, "fragment"));
            return this;
        }
        
        /**
         * Overwrites the fragment at the specified index
         * @param index the index to overwrite
         * @param fragment the replacement
         * @return {@code this}
         * @throws IndexOutOfBoundsException if the index is invalid
         */
        public Builder set(final int index, final Fragment fragment)
        {
            fragments.set(index, requireNonNull(fragment, "fragment"));
            return this;
        }
        
        /**
         * Removes the fragment at the specified index.
         * @param index the index of the fragment to remove
         * @return {@code this}
         * @throws IndexOutOfBoundsException if the index is invalid
         */
        public Builder remove(final int index)
        {
            fragments.remove(index);
            return this;
        }
        
        /**
         * Removes the fragment at the specified index.
         * @param index the index of the fragment to remove
         * @return the removed fragment
         * @throws IndexOutOfBoundsException if the index is invalid
         */
        public Fragment removeAndGet(final int index)
        {
            return fragments.remove(index);
        }

        /**
         * Removes all fragments.
         * @return {@code this}
         */
        public Builder clear()
        {
            fragments.clear();
            return this;
        }
        
        /**
         * Returns this builder's evaluator.
         */
        public Evaluator evaluator()
        {
            return evaluator;
        }

        /**
         * Returns the fragment at the specified index.
         * @throws IndexOutOfBoundsException if the index is invalid
         */
        public Fragment get(final int index)
        {
            return fragments.get(index);
        }

        /**
         * Returns the current number of fragments.
         */
        public int size()
        {
            return fragments.size();
        }
        
        /**
         * Whether or not the expression being built is empty.
         */
        public boolean isEmpty()
        {
            return fragments.isEmpty();
        }
        
        /**
         * Builds the expression.
         */
        public Expression build()
        {
            return new Expression(fragments, evaluator);
        }
    }
    
    private final List<Fragment> fragments;
    private final Evaluator evaluator;
    
    private Expression(
            final Collection<Fragment> fragments, final Evaluator evaluator)
    {
        assert evaluator != null;
        this.fragments = List.copyOf(fragments);
        this.evaluator = evaluator;
    }

    /**
     * Evaluates this expression.
     * @param interceptReturn whether or not to catch
     * {@link com.calclipse.math.expression.Return}
     * or let it pass
     * @return the result of the evaluation
     * @throws ErrorMessage
     * containing a user-level message if an error occurred during evaluation
     * @throws com.calclipse.math.expression.BadExpression
     * if the expression is invalid or not properly parsed
     */
    public Value evaluate(final boolean interceptReturn)
            throws ErrorMessage
    {
        return evaluator.evaluate(this, interceptReturn);
    }

    /**
     * Returns the evaluator of this expression.
     */
    public Evaluator evaluator()
    {
        return evaluator;
    }
    
    /**
     * Returns the fragment at the specified index.
     * @throws IndexOutOfBoundsException if the index is invalid
     */
    public Fragment get(final int index)
    {
        return fragments.get(index);
    }
    
    /**
     * The number of fragments of this expression.
     */
    public int size()
    {
        return fragments.size();
    }
    
    /**
     * Whether or not this expression is empty.
     */
    public boolean isEmpty()
    {
        return fragments.isEmpty();
    }
    
    public Stream<Fragment> stream()
    {
        return fragments.stream();
    }
    
    public Stream<Fragment> parallelStream()
    {
        return fragments.parallelStream();
    }

    /**
     * The returned iterator does not support removal.
     */
    @Override
    public Iterator<Fragment> iterator()
    {
        return fragments.iterator();
    }

    @Override
    public Spliterator<Fragment> spliterator()
    {
        return fragments.spliterator();
    }

    @Override
    public String toString()
    {
        return fragments.stream()
                .map(Fragment::token)
                .map(Token::name)
                .collect(joining(" "));
    }
    
}
