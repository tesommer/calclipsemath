package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MatrixUtil;

/**
 * Performs the Gram-Schmidt process on a matrix
 * and returns the result.
 * Does not modify the argument.
 * Supported types:
 * <ul>
 *  <li>matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class GramSchmidt implements VariableArityOperation
{
    public static final String NAME = "gramschmidt";
    
    private static final Id ID = Id.unique();
    
    private static final VariableArityOperation INSTANCE = new GramSchmidt();

    private GramSchmidt()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireArgCount(1, args.length);
        final Object o = args[0].get();
        if (o instanceof Matrix)
        {
            final Matrix mx = ((Matrix)o).copy();
            MatrixUtil.gramSchmidt(mx);
            return Value.constantOf(mx);
        }
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
