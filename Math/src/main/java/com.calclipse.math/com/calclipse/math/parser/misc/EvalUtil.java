package com.calclipse.math.parser.misc;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Static utility methods related to
 * evaluation of mathematical expressions.
 * 
 * @author Tone Sommerland
 */
public final class EvalUtil
{
    private EvalUtil()
    {
    }
    
    /**
     * Attempts to evaluate an expression and return the result as a boolean.
     */
    public static boolean evaluateBoolean(
            final Expression expression,
            final boolean interceptReturn,
            final TypeContext typeContext) throws ErrorMessage
    {
        final Value result = expression.evaluate(interceptReturn);
        return typeContext.booleanType().isTrue(result.get());
    }
    
    /**
     * Checks the interrupted status of the current thread.
     * @param resetStatus whether or not to clear the status
     * @throws ErrorMessage if the thread is interrupted.
     */
    public static void checkInterrupted(final boolean resetStatus)
        throws ErrorMessage
    {
        if ((resetStatus && Thread.interrupted())
                || Thread.currentThread().isInterrupted())
        {
            throw errorMessage(EvalMessages.interruption());
        }
    }
    
    /**
     * Interrupts the thread and returns an exception that can be thrown
     * to signal interruption.
     * @param cause used as the cause of the returned exception (nullable)
     */
    public static ErrorMessage interrupt(final InterruptedException cause)
    {
        Thread.currentThread().interrupt();
        return errorMessage(EvalMessages.interruption()).withCause(cause);
    }

}
