package com.calclipse.math.parser;

import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.VariableArityOperation;

/**
 * A utility class for dealing with dependencies on symbols.
 * A symbol typically resides in a
 * {@link com.calclipse.math.parser.SymbolTable},
 * but may be replaced at any time, for instance when overridden.
 * This class has methods that return references to symbols
 * that reflect such changes.
 * This can be used to make symbol aliases,
 * and to make composite operations
 * that rely on other operations for its evaluation.
 * 
 * @author Tone Sommerland
 */
public final class SymbolDependencies
{
    private SymbolDependencies()
    {
    }
    
    /**
     * Returns a reference to a unary operator.
     * The returned operation evaluates
     * the symbol from the lookup if both present and suitable.
     * Otherwise it evaluates the symbol given to this method.
     * The lookup may result in {@code null}.
     * @param symbol the symbol of which to reference
     * @param lookup performs the symbol lookup
     */
    public static UnaryOperation unaryReference(
            final UnaryOperator symbol,
            final java.util.function.Function<? super String, ? extends Token>
                lookup)
    {
        requireNonNull(symbol, "symbol");
        requireNonNull(lookup, "lookup");
        return arg -> upToDate(UnaryOperator.class, symbol, lookup)
                .evaluate(arg);
    }
    
    /**
     * Returns a reference to a binary operator.
     * The returned operation evaluates
     * the symbol from the lookup if both present and suitable.
     * Otherwise it evaluates the symbol given to this method.
     * The lookup may result in {@code null}.
     * @param symbol the symbol of which to reference
     * @param lookup performs the symbol lookup
     */
    public static BinaryOperation binaryReference(
            final BinaryOperator symbol,
            final java.util.function.Function<? super String, ? extends Token>
                lookup)
    {
        requireNonNull(symbol, "symbol");
        requireNonNull(lookup, "lookup");
        return (left, right) -> upToDate(BinaryOperator.class, symbol, lookup)
                .evaluate(left, right);
    }
    
    /**
     * Returns a reference to an unconditional function.
     * The returned operation evaluates
     * the symbol from the lookup if both present and suitable.
     * Otherwise it evaluates the symbol given to this method.
     * The lookup may result in {@code null}.
     * @param symbol the symbol of which to reference
     * @param lookup performs the symbol lookup
     * @throws IllegalArgumentException if the given symbol is
     * {@link com.calclipse.math.expression.Function#isConditional()
     * conditional}
     */
    public static VariableArityOperation variableArityReference(
            final com.calclipse.math.expression.Function symbol,
            final java.util.function.Function<? super String, ? extends Token>
                lookup)
    {
        requireNonNull(symbol, "symbol");
        requireNonNull(lookup, "lookup");
        if (symbol.isConditional())
        {
            throw new IllegalArgumentException(symbol + " is conditional.");
        }
        return args -> upToDate(symbol, false, lookup).operation()
                .evaluate(args);
    }
    
    /**
     * Returns a reference to a conditional function.
     * The returned operation evaluates
     * the symbol from the lookup if both present and suitable.
     * Otherwise it evaluates the symbol given to this method.
     * The lookup may result in {@code null}.
     * @param symbol the symbol of which to reference
     * @param lookup performs the symbol lookup
     * @throws IllegalArgumentException if the given symbol is not
     * {@link com.calclipse.math.expression.Function#isConditional()
     * conditional}
     */
    public static ConditionalOperation conditionalReference(
            final com.calclipse.math.expression.Function symbol,
            final java.util.function.Function<? super String, ? extends Token>
                lookup)
    {
        requireNonNull(symbol, "symbol");
        requireNonNull(lookup, "lookup");
        if (!symbol.isConditional())
        {
            throw new IllegalArgumentException(symbol + " is not conditional.");
        }
        return args -> upToDate(symbol, true, lookup).conditional()
                .evaluate(args);
    }
    
    /**
     * Returns a token supplier that selects a symbol
     * based on the given selector.
     * @param type a type token
     * @param fallback
     * returned by the selection if no symbol matches the selector
     * @param source the symbols to select from
     * @param selector symbol selector
     */
    public static <T extends Token> Supplier<T> selection(
            final Class<T> type,
            final T fallback,
            final Supplier<? extends Collection<? extends Token>> source,
            final Predicate<? super Token> selector)
    {
        requireNonNull(type,     "type");
        requireNonNull(fallback, "fallback");
        requireNonNull(selector, "selector");
        requireNonNull(source,   "source");
        return () ->
        {
            return source.get().stream()
                    .filter(selector)
                    .filter(type::isInstance)
                    .map(type::cast)
                    .findAny()
                    .orElse(fallback);
        };
    }
    
    private static <T extends Token> T upToDate(
            final Class<T> type,
            final T symbol,
            final java.util.function.Function<? super String, ? extends Token>
                lookup)
    {
        final Token current = lookup.apply(symbol.name());
        if (type.isInstance(current))
        {
            return type.cast(current);
        }
        return symbol;
    }
    
    private static com.calclipse.math.expression.Function upToDate(
            final com.calclipse.math.expression.Function symbol,
            final boolean conditional,
            final java.util.function.Function<? super String, ? extends Token>
                lookup)
    {
        final com.calclipse.math.expression.Function current = upToDate(
                com.calclipse.math.expression.Function.class, symbol, lookup);
        if (current.isConditional() == conditional)
        {
            return current;
        }
        return symbol;
    }

}
