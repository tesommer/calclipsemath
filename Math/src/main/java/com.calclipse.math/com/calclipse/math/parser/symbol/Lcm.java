package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.math.BigInteger;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Least common multiple.
 * Supported types:
 * <ul>
 *  <li>integer[, ..., integer]</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Lcm implements VariableArityOperation
{
    public static final String NAME = "lcm";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Lcm(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Lcm(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(2, args.length);
        
        final long l = TypeUtil.argToLong(1, args[0].get());
        BigInteger result = BigInteger.valueOf(l);
        
        for (int i = 1; i < args.length; i++)
        {
            final long l2 = TypeUtil.argToLong(i + 1, args[i].get());
            final BigInteger next = BigInteger.valueOf(l2);
            result = MathUtil.lcm(result, next);
        }
        
        return Value.constantOf(typeContext.numberType().valueOf(result));
    }

}
