package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

/**
 * A fragment of an arithmetic expression.
 * This class wraps a token along with its position in the parsed string.
 * The position is the zero-based index of the occurrence of the token's name.
 * If the position is -1,
 * it means that the fragment has been artificially created
 * (in other words, that it is not the result of parsing a string).
 * 
 * @see com.calclipse.math.expression.Expression
 * 
 * @author Tone Sommerland
 */
public final class Fragment
{
    private final Token token;
    private final int position;
    
    private Fragment(final Token token, final int position)
    {
        this.token = requireNonNull(token, "token");
        this.position = position;
    }
    
    /**
     * Returns a fragment with the given token and position.
     * @param token the token
     * @param position the (zero based) position of the token
     * @throws IllegalArgumentException if {@code position < 0}
     */
    public static Fragment of(final Token token, final int position)
    {
        return new Fragment(token, requireZeroPlus(position, "position"));
    }

    private static int requireZeroPlus(final int arg, final String argName)
    {
        if (arg < 0)
        {
            throw new IllegalArgumentException(argName + " < 0: " + arg);
        }
        return arg;
    }
    
    /**
     * Creates an artificial fragment that is not the result of parsing.
     * Its position will be to -1.
     */
    public static Fragment fabricate(final Token token)
    {
        return new Fragment(token, -1);
    }

    /**
     * The token of this fragment.
     */
    public Token token()
    {
        return token;
    }

    /**
     * The position of this fragment's token in the parsed expression.
     * @return -1 if this is an artificial fragment
     */
    public int position()
    {
        return position;
    }

    @Override
    public String toString()
    {
        return Fragment.class.getSimpleName()
                + '(' + token.name() + ", " + position + ')';
    }
    
}
