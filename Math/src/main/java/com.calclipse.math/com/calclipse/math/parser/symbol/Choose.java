package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Binomial coefficient.
 * Supported types:
 * <ul>
 *  <li>integer</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Choose implements BinaryOperation
{
    public static final String NAME = "nCr";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Choose(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.LOW, new Choose(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final long i1 = TypeUtil.argToLong(1, left.get());
        final long i2 = TypeUtil.argToLong(2, right.get());
        
        try
        {
            return Value.constantOf(typeContext.numberType()
                    .valueOf(MathUtil.choose(i1, i2)));
            
        }
        catch (final ArithmeticException ex)
        {
            throw errorMessage(EvalMessages.domainError()).withCause(ex);
        }
    }

}
