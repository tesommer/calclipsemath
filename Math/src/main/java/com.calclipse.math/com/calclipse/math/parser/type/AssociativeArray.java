package com.calclipse.math.parser.type;

import java.util.Map;

/**
 * An associative-array data-type.
 * 
 * @author Tone Sommerland
 */
public interface AssociativeArray extends Map<Object, Object>
{
}
