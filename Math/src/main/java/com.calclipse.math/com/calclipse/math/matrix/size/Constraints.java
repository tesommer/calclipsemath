package com.calclipse.math.matrix.size;

import java.util.stream.Stream;

import com.calclipse.math.matrix.Matrix;

/**
 * Static methods dealing with matrix size constraints.
 * 
 * @author Tone Sommerland
 */
public final class Constraints
{
    private Constraints()
    {
    }
    
    /**
     * Enforces the given constraints.
     * @param message exception message thrown if violated
     * @param constraints the constraints to check
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if any of the constraints is violated
     */
    public static void require(
            final String message, final Constraint... constraints)
    {
        Stream.of(constraints)
            .filter(Constraint::isViolated)
            .findFirst()
            .ifPresent(constraint -> constraint.enforce(message));
    }
    
    /**
     * Returns a constraint that is violated
     * if the dimensions of the given matrices mismatch.
     */
    public static Constraint matchingDimensions(
            final Matrix matrix1, final Matrix matrix2)
    {
        return Constraint.check(
                matrix
                    ->     matrix1.rows() == matrix2.rows()
                        && matrix1.columns() == matrix2.columns(),
                matrix1,
                matrix2);
    }
    
    /**
     * Returns a constraint that is violated
     * if the given matrices have unequal number of rows.
     */
    public static Constraint matchingRows(
            final Matrix matrix1, final Matrix matrix2)
    {
        return Constraint.check(
                matrix -> matrix1.rows() == matrix2.rows(),
                matrix1,
                matrix2);
    }
    
    /**
     * Returns a constraint that is violated
     * if the given matrices have unequal number of columns.
     */
    public static Constraint matchingColumns(
            final Matrix matrix1, final Matrix matrix2)
    {
        return Constraint.check(
                matrix -> matrix1.columns() == matrix2.columns(),
                matrix1,
                matrix2);
    }
    
    /**
     * Returns a constraint that is violated
     * if the any of the given matrices does not have the specified dimensions.
     */
    public static Constraint rowsAndColumns(
            final int rows, final int columns, final Matrix... matrices)
    {
        return Constraint.check(
                matrix -> matrix.rows() == rows && matrix.columns() == columns,
                matrices);
    }
    
    /**
     * Returns a constraint that is violated
     * if the any of the given matrices
     * does not have the specified number of rows.
     */
    public static Constraint numberOfRows(
            final int rows, final Matrix... matrices)
    {
        return Constraint.check(
                matrix -> matrix.rows() == rows,
                matrices);
    }
    
    /**
     * Returns a constraint that is violated
     * if the any of the given matrices
     * does not have the specified number of columns.
     */
    public static Constraint numberOfColumns(
            final int columns, final Matrix... matrices)
    {
        return Constraint.check(
                matrix -> matrix.columns() == columns,
                matrices);
    }
    
    /**
     * Returns a constraint that is violated if any of the matrices are empty
     * (i.e. have zero rows and/or columns).
     */
    public static Constraint nonEmpty(final Matrix... matrices)
    {
        return Constraint.check(
                matrix -> matrix.rows() > 0 && matrix.columns() > 0,
                matrices);
    }
    
    /**
     * Returns a constraint that is violated if any of the matrices
     * are not square.
     */
    public static Constraint square(final Matrix... matrices)
    {
        return Constraint.check(
                matrix -> matrix.rows() == matrix.columns(),
                matrices);
    }

}
