package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * The mechanism by which tokens are handed to
 * {@link ParseStep}s.
 * This class provides parse-control methods,
 * such as methods for creating a function tree,
 * as well as methods for saving state information between token deliveries.
 * 
 * @author Tone Sommerland
 */
public final class TokenStream
{
    /**
     * -1.
     */
    public static final int DEFAULT_STATE = -1;
    
    private final ParseStep[] steps;
    private final Evaluator evaluator;
    private Fragment previousInput;
    private Context context;
    
    TokenStream(final ParseStep[] steps, final Evaluator evaluator)
    {
        assert evaluator != null;
        this.steps = steps.clone();
        this.evaluator = evaluator;
        this.context = new Context(null, evaluator);
    }
    
    /**
     * Called by the parser to start a processing cycle.
     */
    void read(final Fragment input) throws ErrorMessage
    {
        final Fragment frag = spawnIfFunction(input);
        write(0, frag);
        context.input = null;
        previousInput = frag;
    }
    
    private static Fragment spawnIfFunction(final Fragment frag)
    {
        if (frag.token().type().isFunction())
        {
            final var func = (Function)frag.token();
            return Fragment.of(func.spawn(), frag.position());
        }
        return frag;
    }
    
    /**
     * Sends a fragment down stream.
     */
    public void write(final Fragment frag) throws ErrorMessage
    {
        final Fragment oldInput = context.input;
        final int oldStepIndex = context.stepIndex;
        write(oldStepIndex + 1, requireNonNull(frag, "frag"));
        context.input = oldInput;
        context.stepIndex = oldStepIndex;
    }
    
    private void write(final int firstStepIndex, final Fragment frag)
            throws ErrorMessage
    {
        context.input = frag;
        for (
                context.stepIndex = firstStepIndex;
                context.stepIndex < steps.length;
                context.stepIndex++)
        {
            steps[context.stepIndex].read(this);
            if (context.input == null)
            {
                break;
            }
        }
        if (context.input != null)
        {
            context.output.add(context.input);
        }
    }
    
    /**
     * Skips the current input and
     * informs all steps that the end of the stream
     * is reached for the current expression.
     */
    public void end() throws ErrorMessage
    {
        context.input = null;
        final int oldStepIndex = context.stepIndex;
        for (
                context.stepIndex = 0;
                context.stepIndex < steps.length;
                context.stepIndex++)
        {
            steps[context.stepIndex].end(this);
        }
        context.stepIndex = oldStepIndex;
    }
    
    /**
     * Starts a new function argument.
     * Should only be called if the current input is a function.
     * After calling this method,
     * {@link #node()}
     * will return the argument-receiving function until
     * {@link #endArgument()}
     * is called.
     * Calling this method will temporarily reset the information
     * stored by the current step,
     * such as the {@link #setState(int) state}.
     * The information will be restored after the argument is
     * {@link #endArgument() finished}.
     * @throws IllegalStateException if the current input is not a function
     */
    public void startArgument()
    {
        if (context.input == null || !context.input.token().type().isFunction())
        {
            throw new IllegalStateException(
                    "Current input is not a function: " + context.input);
        }
        context = new Context(context, evaluator);
    }
    
    /**
     * Ends the current function argument.
     * @throws IllegalStateException
     * if an argument is not {@link #startArgument() started}
     */
    public void endArgument()
    {
        final Function node = currentNode();
        if (!context.output.isEmpty())
        {
            node.addArgument(context.output.build());
        }
        context = context.parent;
    }
    
    /**
     * The node is the function which will receive the argument when
     * {@link #endArgument()}
     * is called.
     * @throws IllegalStateException
     * if an argument is not {@link #startArgument() started}
     */
    public Function node()
    {
        return currentNode();
    }
    
    /**
     * Whether or not the
     * {@link #node() current node}
     * has an argument list opener.
     * @throws IllegalStateException
     * if an argument is not {@link #startArgument() started}
     */
    public boolean hasArgumentListOpener()
    {
        return currentNode().delimitation().hasOpener();
    }
    
    /**
     * Whether or not the given token opens the
     * {@link #node() current node}'s
     * argument list.
     * @throws IllegalStateException
     * if an argument is not {@link #startArgument() started}
     */
    public boolean readsArgumentListOpener(final Token token)
    {
        return currentNode().delimitation().readsOpener(token);
    }
    
    /**
     * Whether or not the given token closes the
     * {@link #node() current node}'s
     * argument list.
     * @throws IllegalStateException
     * if an argument is not {@link #startArgument() started}
     */
    public boolean readsArgumentListCloser(final Token token)
    {
        return currentNode().delimitation().readsCloser(token);
    }
    
    /**
     * Whether or not the given token separates
     * the current argument from the next.
     * @throws IllegalStateException
     * if an argument is not {@link #startArgument() started}
     */
    public boolean readsArgumentSeparator(final Token token)
    {
        return currentNode().delimitation().readsSeparator(token);
    }
    
    private Function currentNode()
    {
        if (context.parent == null)
        {
            throw new IllegalStateException("Argument not started.");
        }
        return (Function)context.parent.input.token();
    }
    
    /**
     * The previous fragment that was received from the scanner.
     * This is not necessarily the last input read by any given step,
     * except the first step.
     * @throws java.util.NoSuchElementException if no previous input exist
     */
    public Fragment previousInput()
    {
        if (previousInput == null)
        {
            throw new NoSuchElementException("previousInput");
        }
        return previousInput;
    }
    
    /**
     * The current input to process.
     * This method should only be called from
     * {@link ParseStep#read(TokenStream)}.
     * @throws java.util.NoSuchElementException if there is no current input
     */
    public Fragment input()
    {
        if (context.input == null)
        {
            throw new NoSuchElementException("input");
        }
        return context.input;
    }
    
    /**
     * Replaces the current input with another.
     * @param replacement the new input
     * @throws java.util.NoSuchElementException if there is no input to replace
     */
    public void replaceInput(final Fragment replacement)
    {
        if (context.input == null)
        {
            throw new IllegalStateException("No input to replace.");
        }
        context.input = requireNonNull(replacement, "replacement");
    }

    /**
     * Skips the current input.
     */
    public void skip()
    {
        context.input = null;
    }
    
    Expression output()
    {
        return context.output.build();
    }
    
    /**
     * The evaluator that the parsed expression will receive.
     */
    public Evaluator evaluator()
    {
        return evaluator;
    }

    /**
     * Returns either the previous state set by the current step,
     * or the default state.
     * @see #setState(int)
     */
    public int state()
    {
        return stepData().state;
    }
    
    /**
     * The state set by this method will only be visible
     * to the current step.
     * @see #state()
     */
    public void setState(final int state)
    {
        stepData().state = state;
    }
    
    /**
     * Each step has its own buffer.
     */
    public List<Fragment> buffer()
    {
        return stepData().buffer;
    }
    
    /**
     * Each step has its own general-purpose map.
     */
    public Map<String, Object> map()
    {
        return stepData().map;
    }
    
    private Data stepData()
    {
        return context.data.computeIfAbsent(
                context.stepIndex, key -> new Data());
    }
    
    /**
     * Tracks a single leaf in a function tree.
     */
    private static final class Context
    {
        final Context parent;
        final Expression.Builder output;
        final Map<Integer, Data> data = new HashMap<>();
        int stepIndex;
        Fragment input;
        
        Context(final Context p, final Evaluator evaluator)
        {
            parent = p;
            output = new Expression.Builder(evaluator);
            if (p != null)
            {
                stepIndex = p.stepIndex;
            }
        }
    }
    
    /**
     * Step data stored in a context.
     */
    private static final class Data
    {
        final List<Fragment> buffer = new ArrayList<>();
        final Map<String, Object> map = new HashMap<>();
        int state = DEFAULT_STATE;
        
        Data()
        {
        }
    }

    @Override
    public String toString()
    {
        return TokenStream.class.getSimpleName()
                + ": [input=" + context.input
                + "] [previousInput=" + previousInput + ']';
    }

}
