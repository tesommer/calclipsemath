package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * Decrement and assign (similar to -=).
 * Subtracts the right operand from the left
 * and stores the result in the left operand.
 * Returns the left operand.
 * The subtraction is performed by the operation
 * given to the operator.
 * 
 * @author Tone Sommerland
 */
public final class Decrement implements BinaryOperation
{
    public static final String NAME = "dec";
    
    private static final Id ID = Id.unique();
    
    private final BinaryOperation subtraction;
    
    private Decrement(final BinaryOperation subtraction)
    {
        this.subtraction = requireNonNull(subtraction, "subtraction");
    }
    
    public static BinaryOperator operator(
            final BinaryOperation subtraction)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.ASSIGNMENT, new Decrement(subtraction))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Value result = subtraction.evaluate(left, right);
        left.set(result.get());
        return left;
    }

}
