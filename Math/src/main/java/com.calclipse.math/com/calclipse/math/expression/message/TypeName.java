package com.calclipse.math.expression.message;

import static java.util.Objects.requireNonNull;

/**
 * The name of a data type.
 * 
 * @author Tone Sommerland
 */
public final class TypeName
{
    private final Class<?> type;
    private final String name;
    
    private TypeName(final Class<?> type, final String name)
    {
        this.type = requireNonNull(type, "type");
        this.name = requireNonNull(name, "name");
    }
    
    /**
     * Returns a type name.
     * @param type the type
     * @param name the name
     */
    public static TypeName of(final Class<?> type, final String name)
    {
        return new TypeName(type, name);
    }
    
    boolean isTypeOf(final Object value)
    {
        return type.isInstance(value);
    }
    
    String name()
    {
        return name;
    }
    
    @Override
    public String toString()
    {
        return TypeName.class.getSimpleName() + '(' + type + '=' + name + ')';
    }

}
