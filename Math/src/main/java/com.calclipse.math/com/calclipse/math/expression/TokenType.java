package com.calclipse.math.expression;

/**
 * Types of {@link com.calclipse.math.expression.Token}s.
 * 
 * @author Tone Sommerland
 */
public enum TokenType
{
    /**
     * Left parenthesis.
     */
    LPAREN,
    
    /**
     * Right parenthesis.
     */
    RPAREN,
    
    /**
     * A plain token.
     */
    PLAIN,
    
    /**
     * An undefined (unrecognized) symbol.
     */
    UNDEFINED,
    
    /**
     * Operand.
     * A token with this type is an instance of
     * {@link com.calclipse.math.expression.Operand}.
     */
    OPERAND,
    
    /**
     * Function.
     * A token with this type is an instance of
     * {@link com.calclipse.math.expression.Function}.
     */
    FUNCTION,
    
    /**
     * Binary operator.
     * A token with this type is an instance of
     * {@link com.calclipse.math.expression.BinaryOperator}.
     */
    BOPERATOR,

    /**
     * Left unary operator (such as square root: &#8730;).
     * A token with this type is an instance of
     * {@link com.calclipse.math.expression.UnaryOperator}.
     */
    LUOPERATOR,
    
    /**
     * Right unary operator (such as factorial: !).
     * A token with this type is an instance of
     * {@link com.calclipse.math.expression.UnaryOperator}.
     */
    RUOPERATOR;
    
    /**
     * Is it an opening parenthesis?
     * @return {@code true} if this type is {@link #LPAREN}
     */
    public boolean isLeftParenthesis()
    {
        return this == LPAREN;
    }
    
    /**
     * Is it a closing parenthesis?
     * @return {@code true} if this type is {@link #RPAREN}
     */
    public boolean isRightParenthesis()
    {
        return this == RPAREN;
    }
    
    /**
     * Is it a plain token?
     * @return {@code true} if this type is {@link #PLAIN}
     */
    public boolean isPlain()
    {
        return this == PLAIN;
    }
    
    /**
     * Is it undefined?
     * @return {@code true} if this type is {@link #UNDEFINED}
     */
    public boolean isUndefined()
    {
        return this == UNDEFINED;
    }
    
    /**
     * Is it an operand?
     * @return {@code true} if this type is {@link #OPERAND}
     */
    public boolean isOperand()
    {
        return this == OPERAND;
    }
    
    /**
     * Is it a function?
     * @return {@code true} if this type is {@link #FUNCTION}
     */
    public boolean isFunction()
    {
        return this == FUNCTION;
    }
    
    /**
     * Is it an operator?
     * @return {@code true} if this type is
     * {@link #BOPERATOR}, {@link #LUOPERATOR} or {@link #RUOPERATOR}
     */
    public boolean isOperator()
    {
        return this == BOPERATOR || this == LUOPERATOR || this == RUOPERATOR;
    }
    
    /**
     * Is it a binary operator?
     * @return {@code true} if this type is {@link #BOPERATOR}
     */
    public boolean isBinary()
    {
        return this == BOPERATOR;
    }
    
    /**
     * Is it a unary operator?
     * @return {@code true} if this type is either
     * {@link #LUOPERATOR} or {@link #RUOPERATOR}
     */
    public boolean isUnary()
    {
        return this == LUOPERATOR || this == RUOPERATOR;
    }
    
    /**
     * Is it a prefixed unary operator?
     * @return {@code true} if this type is {@link #LUOPERATOR}
     */
    public boolean isLeftUnary()
    {
        return this == LUOPERATOR;
    }
    
    /**
     * Is it a postfixed unary operator?
     * @return {@code true} if this type is {@link #RUOPERATOR}
     */
    public boolean isRightUnary()
    {
        return this == RUOPERATOR;
    }
}
