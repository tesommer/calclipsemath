package com.calclipse.math.matrix;

import static com.calclipse.math.matrix.size.Constraints.numberOfColumns;
import static com.calclipse.math.matrix.size.Constraints.numberOfRows;
import static com.calclipse.math.matrix.size.Constraints.require;
import static java.util.Objects.checkIndex;

import java.util.Arrays;

/**
 * Represents a permutation,
 * which is an identity matrix where some of the rows and columns
 * may have been interchanged.
 * 
 * @author Tone Sommerland
 */
public final class Permutation
{
    /**
     * Builds {@link Permutation} instances.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final int[] entries;

        /**
         * Creates a permutation builder
         * with the identity matrix as a starting point.
         * @throws IllegalArgumentException if {@code rows} is negative
         */
        public Builder(final int rows)
        {
            if (rows < 0)
            {
                throw new IllegalArgumentException("rows < 0: " + rows);
            }
            this.entries = new int[rows];
            for (int i = 1; i < rows; i++)
            {
                this.entries[i] = i;
            }
        }
        
        /**
         * Creates a permutation builder
         * with the given permutation instance as a starting point.
         */
        public Builder(final Permutation permutation)
        {
            this.entries = permutation.entries.clone();
        }
        
        /**
         * Swapping rows a and b is the same as
         * swapping columns a and b, and vice versa.
         * @param a the first row/column
         * @param b the second row/column
         * @return {@code this}
         * @throws IndexOutOfBoundsException if either index is invalid
         */
        public Builder swap(final int a, final int b)
        {
            checkIndices(a, b, entries.length);
            final int h = entries[a];
            entries[a] = entries[b];
            entries[b] = h;
            return this;
        }
        
        /**
         * Builds the permutation.
         */
        public Permutation build()
        {
            return new Permutation(entries);
        }
    }
    
    private final int[] entries;
    
    private Permutation(final int[] entries)
    {
        this.entries = entries.clone();
    }
    
    /**
     * The number of rows of this permutation
     * (which is also the number of columns).
     */
    public int rows()
    {
        return entries.length;
    }
    
    /**
     * Returns the element at the specified row and column
     * (which is either zero or one).
     * @param row zero-based row index
     * @param column zero-based column index
     * @throws IndexOutOfBoundsException if the row and/or column are invalid
     */
    public int get(final int row, final int column)
    {
        checkIndices(row, column, entries.length);
        return entries[column] == row ? 1 : 0;
    }
    
    /**
     * Copies this permutation into a matrix.
     */
    public void copyEntriesTo(final Matrix destination)
    {
        destination.setSize(entries.length, entries.length, false);
        for (int i = 0; i < entries.length; i++)
        {
            destination.set(i, entries[i], 1);
        }
    }
    
    /**
     * If given matrix <i>A</i>, this method does this: {@code A = PA}
     * (where <i>P</i> is this permutation).
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the size of this permutation does not match
     * the number of rows of the argument
     */
    public void preApply(final Matrix matrix)
    {
        require("P*A", numberOfRows(entries.length, matrix));
        permutate(matrix, false);
    }
    
    /**
     * If given matrix <i>A</i>, this method does this: {@code A = AP}
     * (where <i>P</i> is this permutation).
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the size of this permutation does not match
     * the number of columns of the argument
     */
    public void apply(final Matrix matrix)
    {
        require("A*P", numberOfColumns(entries.length, matrix));
        permutate(matrix, true);
    }
    
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Permutation)
        {
            final var permutation = (Permutation)obj;
            return Arrays.equals(entries, permutation.entries);
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return Arrays.hashCode(entries);
    }

    private void permutate(final Matrix matrix, final boolean swapColumns)
    {
        final var swapped = new int[entries.length];
        for (int i = 0; i < entries.length; i++)
        {
            final int nonzeroRow = entries[i];
            if (swapped[i] == 0)
            {
                swapped[i] = nonzeroRow;
            }
            if (swapped[i] != i)
            {
                swap(matrix, i, nonzeroRow, swapColumns);
                swapped[nonzeroRow] = nonzeroRow;
            }
        }
    }

    private static void swap(
            final Matrix matrix,
            final int index1,
            final int index2,
            final boolean swapColumns)
    {
        if (swapColumns)
        {
            matrix.swapColumns(index1, index2);
        }
        else
        {
            matrix.swapRows(index1, index2);
        }
    }
    
    private static void checkIndices(
            final int index1, final int index2, final int length)
    {
        checkIndex(index1, length);
        checkIndex(index2, length);
    }

}
