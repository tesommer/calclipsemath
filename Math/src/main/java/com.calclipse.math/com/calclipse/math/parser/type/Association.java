package com.calclipse.math.parser.type;

import java.util.Map;

/**
 * A type representing an association between a key and a value.
 * 
 * @author Tone Sommerland
 */
public interface Association extends Map.Entry<Object, Object>
{
    /**
     * A convenience method that can be used to implement
     * {@link #equals(Object)}.
     */
    public static boolean equals(
            final Association association, final Object obj)
    {
        if (obj instanceof Association)
        {
            final var other = (Association)obj;
            return     keysEqual(association, other)
                    && valuesEqual(association, other);
        }
        return false;
    }
    
    /**
     * A convenience method that can be used to implement
     * {@link #hashCode()}.
     */
    public static int hashCode(final Association association)
    {
        return
                (association.getKey() == null ?
                        0 : association.getKey().hashCode())
                ^
                (association.getValue() == null ?
                        0 : association.getValue().hashCode());
    }
    
    private static boolean keysEqual(
            final Association a1, final Association a2)
    {
        return a1.getKey() == null ?
                a2.getKey() == null : a1.getKey().equals(a2.getKey());
    }
    
    private static boolean valuesEqual(
            final Association a1, final Association a2)
    {
        return a1.getValue() == null ?
                a2.getValue() == null : a1.getValue().equals(a2.getValue());
    }
}
