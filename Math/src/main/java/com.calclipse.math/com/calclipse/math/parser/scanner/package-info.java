/**
 * Expression scanning.
 *
 * @see com.calclipse.math.parser.MathParserConfig#scanner
 */
package com.calclipse.math.parser.scanner;
