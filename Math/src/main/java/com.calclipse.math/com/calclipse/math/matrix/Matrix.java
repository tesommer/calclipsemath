package com.calclipse.math.matrix;

import static com.calclipse.math.matrix.size.Constraints.matchingColumns;
import static com.calclipse.math.matrix.size.Constraints.matchingDimensions;
import static com.calclipse.math.matrix.size.Constraints.matchingRows;
import static com.calclipse.math.matrix.size.Constraints.numberOfColumns;
import static com.calclipse.math.matrix.size.Constraints.numberOfRows;
import static com.calclipse.math.matrix.size.Constraints.require;
import static com.calclipse.math.matrix.size.Constraints.rowsAndColumns;
import static java.util.Objects.checkFromToIndex;
import static java.util.Objects.checkIndex;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.matrix.function.RowColumnConsumer;
import com.calclipse.math.matrix.function.RowColumnFunction;
import com.calclipse.math.matrix.function.RowColumnValueConsumer;
import com.calclipse.math.matrix.function.RowColumnValueFunction;
import com.calclipse.math.matrix.internal.UnmodifiableMatrix;

/**
 * A double-valued matrix.
 * A matrix is a two-dimensional array of quantities
 * arranged in rows and columns.
 * A matrix element is addressed by its row and column index,
 * respectively.
 * The internal element representation is subclass dependent.
 * This matrix class uses zero-based row and column indices.
 * This class permits any of its two dimensions two be zero or positive,
 * but not negative.
 * <b>Note:</b>
 * Any method that mutates this object may throw
 * {@code UnsupportedOperationException}.
 * (See {@link #unmodifiable()}.)
 * 
 * @author Tone Sommerland
 */
public abstract class Matrix
{
    private double delta;
    private MatrixStringifier stringifier;
    
      //////////////////
     // Constructors //
    //////////////////
    
    /**
     * Creates a matrix with the specified delta and stringifier.
     * @see #setDelta(double)
     * @see #setStringifier(MatrixStringifier)
     */
    protected Matrix(final double delta, final MatrixStringifier stringifier)
    {
        this.delta = delta;
        this.stringifier = requireNonNull(stringifier, "stringifier");
    }
    
    /**
     * Creates a matrix with a default delta and stringifier.
     * The default delta is {@code 1.0E-10}.
     */
    protected Matrix()
    {
        this(1.0E-10, MatrixStringifier.standard());
    }
    
      ///////////
     // Views //
    ///////////
    
    /**
     * Returns an unmodifiable view of this matrix.
     * The returned matrix throws
     * {@code UnsupportedOperationException}
     * on modification attempts.
     * Other method invoked on the unmodifiable matrix
     * are forwarded to this instance.
     */
    public final Matrix unmodifiable()
    {
        return new UnmodifiableMatrix(this);
    }
    
      //////////////////////////////////////////////
     // Abstract methods and public counterparts //
    //////////////////////////////////////////////

    /**
     * Returns the number of entry rows.
     * The number of rows may be zero or positive.
     */
    protected abstract int rowCount();
    
    /**
     * Returns the number of entry columns.
     * The number of columns may be zero or positive.
     */
    protected abstract int columnCount();
    
    /**
     * Returns the entry at the specified row and column.
     * The row and column passed to this method
     * are within the bounds of this matrix.
     * @param row zero-based row index
     * @param column zero-based column index
     */
    protected abstract double entry(int row, int column);
    
    /**
     * Sets the entry at the specified row and column.
     * The row and column passed to this method
     * are within the bounds of this matrix.
     * @param row zero-based row index
     * @param column zero-based column index
     * @param entry the new value
     * @throws UnsupportedOperationException if not supported
     */
    protected abstract void setEntry(int row, int column, double entry);
    
    /**
     * Sets the number of entry rows and columns.
     * If {@code preserveEntries} is {@code true},
     * then entries within the bounds of the new dimensions should persist.
     * Otherwise they should be zeroed out.
     * New entries resulting from an expansion should contain zeros.
     * The dimensions passed to this method are zero or positive.
     * @throws UnsupportedOperationException if not supported
     */
    protected abstract void setDimensions(
            int rows, int columns, boolean preserveEntries);
    
    /**
     * Duplicates this matrix.
     */
    protected abstract Matrix duplicate();
    
    /**
     * Creates a new instance filled with zeros.
     * The dimensions passed to this method are zero or positive.
     */
    protected abstract Matrix newInstance(int rows, int columns);

    /**
     * Number of rows.
     */
    public final int rows()
    {
        return rowCount();
    }
    
    /**
     * Number of columns.
     */
    public final int columns()
    {
        return columnCount();
    }
    
    /**
     * Returns the element at the specified row and column.
     * @param row zero-based row index
     * @param column zero-based column index
     * @throws IndexOutOfBoundsException if the row and/or column are invalid
     */
    public final double get(final int row, final int column)
    {
        return entry(checkRowIndex(row), checkColumnIndex(column));
    }
    
    /**
     * Sets the element at the specified row and column.
     * @param row zero-based row index
     * @param column zero-based column index
     * @param entry the new value
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if the row and/or column are invalid
     */
    public final void set(
            final int row, final int column, final double entry) 
    {
        setEntry(checkRowIndex(row), checkColumnIndex(column), entry);
    }
    
    /**
     * Resizes this matrix.
     * If {@code preserveEntries} is {@code true},
     * old elements still within the bounds of the new dimensions will persist.
     * Otherwise the they will be zeroed out,
     * and that will (most likely) be faster.
     * New elements resulting from an expansion will contain zeros.
     * @param rows the new number of rows
     * @param columns the new number of columns
     * @param preserveEntries whether or not to preserve old entries
     * @throws UnsupportedOperationException if not supported
     * @throws IllegalArgumentException
     * if {@code rows} and/or {@code columns} are negative
     */
    public final void setSize(
            final int rows, final int columns, final boolean preserveEntries)
    {
        setDimensions(
                requireZeroPlus(rows, "rows"),
                requireZeroPlus(columns, "columns"),
                preserveEntries);
    }
    
    /**
     * Copies this matrix.
     */
    public final Matrix copy()
    {
        return retainAttributes(duplicate());
    }
    
    /**
     * Creates a new matrix filled with zeros.
     * @param rows number of rows to create
     * @param columns number of columns to create
     * @throws IllegalArgumentException
     * {@code rows} and/or {@code columns} are negative
     */
    public final  Matrix create(final int rows, final int columns)
    {
        return retainAttributes(newInstance(
                requireZeroPlus(rows, "rows"),
                requireZeroPlus(columns, "columns")));
    }
    
      ////////////////
     // Properties //
    ////////////////
    
    /**
     * The delta value.
     * @see #setDelta(double)
     */
    public final double delta()
    {
        return delta;
    }
    
    /**
     * Some operations may result in elements that should be zero,
     * but instead have a very small value due to round off errors.
     * The delta value is used as a threshold that determines when an
     * element is sufficiently close to zero to be annihilated.
     * A negative delta turns this feature off.
     * The attribute set by this method will be propagated
     * to instances derived from this.
     * @throws UnsupportedOperationException if not supported
     */
    public final void setDelta(final double delta)
    {
        this.delta = setDeltaHatch(delta);
    }
    
    /**
     * {@link #setDelta(double)}
     * passes its parameter through this method before assigning the property.
     * @implSpec The default implementation returns the new value as is
     * @throws UnsupportedOperationException if not supported
     */
    protected double setDeltaHatch(final double newValue)
    {
        return newValue;
    }
    
    /**
     * The object responsible for the string representation of this matrix.
     */
    public final MatrixStringifier stringifier()
    {
        return stringifier;
    }

    /**
     * Sets the string representations of this matrix.
     * The attribute set by this method will be propagated
     * to instances derived from this.
     * @throws UnsupportedOperationException if not supported
     */
    public final void setStringifier(final MatrixStringifier stringifier)
    {
        this.stringifier = setStringifierHatch(
                requireNonNull(stringifier, "stringifier"));
    }
    
    /**
     * {@link #setStringifier(MatrixStringifier)}
     * passes its parameter through this method before assigning the property.
     * The parameter given to this method is non-null.
     * @implSpec The default implementation returns the new value as is
     * @throws UnsupportedOperationException if not supported
     */
    protected MatrixStringifier setStringifierHatch(
            final MatrixStringifier newValue)
    {
        return newValue;
    }
    
      ///////////////////////////////
     // Methods accepting lambdas //
    ///////////////////////////////
    
    /**
     * Applies the given function to elements of this matrix.
     * @throws UnsupportedOperationException if not supported
     */
    public final void applyRowColumnFunction(final RowColumnFunction function)
    {
        requireNonNull(function, "function");
        for (int row = 0; row < rowCount(); row++)
        {
            for (int column = 0; column < columnCount(); column++)
            {
                setEntry(row, column, function.apply(row, column));
            }
        }
    }
    
    /**
     * Applies the given function to elements of this matrix.
     * @throws UnsupportedOperationException if not supported
     */
    public final void applyRowColumnValueFunction(
            final RowColumnValueFunction function)
    {
        requireNonNull(function, "function");
        for (int row = 0; row < rowCount(); row++)
        {
            for (int column = 0; column < columnCount(); column++)
            {
                final double value = entry(row, column);
                setEntry(row, column, function.apply(row, column, value));
            }
        }
    }
    
    /**
     * Performs the given operation for each element of this matrix.
     */
    public final void acceptRowColumnConsumer(
            final RowColumnConsumer consumer)
    {
        requireNonNull(consumer, "consumer");
        for (int row = 0; row < rowCount(); row++)
        {
            for (int column = 0; column < columnCount(); column++)
            {
                consumer.accept(row, column);
            }
        }
    }
    
    /**
     * Performs the given operation for each element of this matrix.
     */
    public final void acceptRowColumnValueConsumer(
            final RowColumnValueConsumer consumer)
    {
        requireNonNull(consumer, "consumer");
        for (int row = 0; row < rowCount(); row++)
        {
            for (int column = 0; column < columnCount(); column++)
            {
                final double value = entry(row, column);
                consumer.accept(row, column, value);
            }
        }
    }
    
      ////////////////////////////
     // Entry-cleaning methods //
    ////////////////////////////
    
    /**
     * Cleans the specified number using the delta value.
     * @see #setDelta(double)
     */
    public final double clean(final double d)
    {
        return Math.abs(d) <= delta ? 0 : d;
    }
    
    /**
     * Eliminates the specified element if it is close enough to zero.
     * @param row the row index
     * @param column the column index
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if the row and/or column are invalid
     * @see #setDelta(double)
     */
    public final void clean(final int row, final int column)
    {
        checkRowIndex(row);
        checkColumnIndex(column);
        if (Math.abs(entry(row, column)) <= delta)
        {
            setEntry(row, column, 0);
        }
    }

    /**
     * Cleans this matrix by eliminating elements close enough to zero.
     * @throws UnsupportedOperationException if not supported
     * @see #setDelta(double)
     */
    public final void clean()
    {
        acceptRowColumnConsumer(this::clean);
    }
    
      ////////////////////////////////////////////
     // Entry-access and -manipulation methods //
    ////////////////////////////////////////////
    
    /**
     * Returns a partition of this matrix.
     * The partition starts at (fromRow, fromColumn),
     * and extends to the element at (toRow - 1, toColumn - 1), inclusive.
     * @param fromRow start row
     * @param fromColumn start column
     * @param toRow end row (exclusive)
     * @param toColumn end column (exclusive)
     * @throws IndexOutOfBoundsException if the partition is invalid
     */
    public final Matrix partition(
            final int fromRow,
            final int fromColumn,
            final int toRow,
            final int toColumn)
    {
        checkPartition(fromRow, fromColumn, toRow, toColumn);
        return retainAttributes(
                partitionHatch(fromRow, fromColumn, toRow, toColumn));
    }
    
    /**
     * {@link #partition(int, int, int, int)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     */
    protected Matrix partitionHatch(
            final int fromRow,
            final int fromColumn,
            final int toRow,
            final int toColumn)
    {
        final Matrix partition
            = newInstance(toRow - fromRow, toColumn - fromColumn);
        partition.applyRowColumnFunction((row, column)
                -> entry(row + fromRow, column + fromColumn));
        return partition;
    }
    
    /**
     * Sets a partition of this matrix.
     * The element at the specified row and column
     * will be set to the first element of the partition.
     * Should any element of the partition
     * extend outside the boundaries of this matrix,
     * the universe will collapse in on itself,
     * and we'll all be in serious trouble.
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if the partition is invalid
     */
    public final void setPartition(
            final int row, final int column, final Matrix partition)
    {
        checkPartition(
                row,
                column,
                row + partition.rowCount(),
                column + partition.columnCount());
        setPartitionHatch(row, column, partition);
    }
    
    /**
     * {@link #setPartition(int, int, Matrix)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @throws UnsupportedOperationException if not supported
     */
    protected void setPartitionHatch(
            final int row, final int column, final Matrix partition)
    {
        partition.acceptRowColumnValueConsumer((pRow, pColumn, pValue)
                -> setEntry(row + pRow, column + pColumn, pValue));
    }
    
    /**
     * Returns a row of this matrix.
     * @param row the index of the row to return
     * @throws IndexOutOfBoundsException if the index is invalid
     */
    public final Matrix row(final int row)
    {
        checkRowIndex(row);
        return retainAttributes(rowHatch(row));
    }
    
    /**
     * {@link #row(int)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @implSpec
     * The default implementation calls
     * {@link #partitionHatch(int, int, int, int)}.
     */
    protected Matrix rowHatch(final int row)
    {
        return partitionHatch(row, 0, row + 1, columnCount());
    }
    
    /**
     * Sets a row of this matrix.
     * @param row the row index
     * @param entries the row entries
     * @throws IndexOutOfBoundsException if the index is invalid
     * @throws com.calclipse.math.matrix.size.SizeViolation if {@code entries}
     * does not have one row or as many columns as {@code this}
     * @throws UnsupportedOperationException if not supported
     */
    public final void setRow(final int row, final Matrix entries)
    {
        checkRowIndex(row);
        require(
                "setRow",
                numberOfRows(1, entries),
                matchingColumns(this, entries));
        setRowHatch(row, entries);
    }
    
    /**
     * {@link #setRow(int, Matrix)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @implSpec
     * The default implementation calls
     * {@link #setPartitionHatch(int, int, Matrix)}.
     * @throws UnsupportedOperationException if not supported
     */
    protected void setRowHatch(final int row, final Matrix entries)
    {
        setPartitionHatch(row, 0, entries);
    }

    /**
     * Returns a column of this matrix.
     * @param column the index of the column to return
     * @throws IndexOutOfBoundsException if the index is invalid
     */
    public final Matrix column(final int column)
    {
        checkColumnIndex(column);
        return retainAttributes(columnHatch(column));
    }
    
    /**
     * {@link #column(int)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @implSpec
     * The default implementation calls
     * {@link #partitionHatch(int, int, int, int)}.
     */
    protected Matrix columnHatch(final int column)
    {
        return partitionHatch(0, column, rowCount(), column + 1);
    }

    /**
     * Sets a column of this matrix.
     * @param column the column index
     * @param entries the column entries
     * @throws IndexOutOfBoundsException if the index is invalid
     * @throws com.calclipse.math.matrix.size.SizeViolation if {@code entries}
     * does not have one column or as many rows as {@code this}
     * @throws UnsupportedOperationException if not supported
     */
    public final void setColumn(final int column, final Matrix entries)
    {
        checkColumnIndex(column);
        require(
                "setColumn",
                numberOfColumns(1, entries),
                matchingRows(this, entries));
        setColumnHatch(column, entries);
    }
    
    /**
     * {@link #setColumn(int, Matrix)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @implSpec
     * The default implementation calls
     * {@link #setPartitionHatch(int, int, Matrix)}.
     * @throws UnsupportedOperationException if not supported
     */
    protected void setColumnHatch(final int column, final Matrix entries)
    {
        setPartitionHatch(0, column, entries);
    }
    
    /**
     * Swaps two rows.
     * @param row1 index of the first row
     * @param row2 index of the second row
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if any of the indices is invalid
     */
    public final void swapRows(final int row1, final int row2)
    {
        checkRowIndex(row1);
        checkRowIndex(row2);
        swapRowsHatch(row1, row2);
    }
    
    /**
     * {@link #swapRows(int, int)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @throws UnsupportedOperationException if not supported
     */
    protected void swapRowsHatch(final int row1, final int row2)
    {
        for (int column = 0; column < columnCount(); column++)
        {
            final double d = entry(row1, column);
            setEntry(row1, column, entry(row2, column));
            setEntry(row2, column, d);
        }
    }
    
    /**
     * Swaps two columns.
     * @param column1 index of the first column
     * @param column2 index of the second column
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if any of the indices is invalid
     */
    public final void swapColumns(final int column1, final int column2)
    {
        checkColumnIndex(column1);
        checkColumnIndex(column2);
        swapColumnsHatch(column1, column2);
    }
    
    /**
     * {@link #swapColumns(int, int)}
     * delegates to this overridable method.
     * Parameters passed to this method have been checked for validity.
     * @throws UnsupportedOperationException if not supported
     */
    protected void swapColumnsHatch(final int column1, final int column2)
    {
        for (int row = 0; row < rowCount(); row++)
        {
            final double d = entry(row, column1);
            setEntry(row, column1, entry(row, column2));
            setEntry(row, column2, d);
        }
    }
    
      //////////////////
     // Math methods //
    //////////////////
    
    /**
     * Scales a row by a factor.
     * @param row index of the row to scale
     * @param scalar the factor
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if the row index is invalid
     */
    public final void scaleRow(final int row, final double scalar)
    {
        checkRowIndex(row);
        for (int column = 0; column < columnCount(); column++)
        {
            setEntry(row, column, entry(row, column) * scalar);
        }
    }
    
    /**
     * Scales a column by a factor.
     * @param column index of the column to scale
     * @param scalar the factor
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if the column index is invalid
     */
    public final void scaleColumn(final int column, final double scalar)
    {
        checkColumnIndex(column);
        for (int row = 0; row < rowCount(); row++)
        {
            setEntry(row, column, entry(row, column) * scalar);
        }
    }
    
    /**
     * Scales this matrix by a factor.
     * @throws UnsupportedOperationException if not supported
     */
    public final void scale(final double scalar)
    {
        applyRowColumnValueFunction((row, column, value) -> value * scalar);
    }
    
    /**
     * Returns the negative of this matrix.
     */
    public final Matrix negated()
    {
        final Matrix negative = retainAttributes(
                newInstance(rowCount(), columnCount()));
        negative.applyRowColumnFunction(
                (row, column) -> -entry(row, column));
        return negative;
    }

    /**
     * Returns the transpose of this matrix.
     */
    public final Matrix transposed()
    {
        final Matrix trans = retainAttributes(
                newInstance(columnCount(), rowCount()));
        trans.applyRowColumnFunction((row, column) -> entry(column, row));
        return trans;
    }
    
    /**
     * Creates an identity matrix with the specified number of rows.
     * @throws IllegalArgumentException if {@code rows} is negative
     */
    public final Matrix identity(final int rows)
    {
        requireZeroPlus(rows, "rows");
        final Matrix id = retainAttributes(newInstance(rows, rows));
        for (int row = 0; row < rows; row++)
        {
            id.setEntry(row, row, 1);
        }
        return id;
    }
    
    /**
     * Matrix addition.
     * @return {@code this + mx}
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if dimensions mismatch
     */
    public final Matrix plus(final Matrix mx)
    {
        require("+", matchingDimensions(this, mx));
        final Matrix result = retainAttributes(
                newInstance(rowCount(), columnCount()));
        result.applyRowColumnFunction((row, column)
                -> clean(entry(row, column) + mx.entry(row, column)));
        return result;
    }

    /**
     * Addition and assignment ({@code +=}).
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if dimensions mismatch
     * @throws UnsupportedOperationException if not supported
     */
    public final void addToThis(final Matrix mx)
    {
        require("+=", matchingDimensions(this, mx));
        applyRowColumnValueFunction((row, column, value)
                -> clean(value + mx.entry(row, column)));
    }

    /**
     * Matrix subtraction.
     * @return {@code this - mx}
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if dimensions mismatch
     */
    public final Matrix minus(final Matrix mx)
    {
        require("-", matchingDimensions(this, mx));
        final Matrix result = retainAttributes(
                newInstance(rowCount(), columnCount()));
        result.applyRowColumnFunction((row, column)
                -> clean(entry(row, column) - mx.entry(row, column)));
        return result;
    }

    /**
     * Subtraction and assignment ({@code -=}).
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if dimensions mismatch
     * @throws UnsupportedOperationException if not supported
     */
    public final void subtractFromThis(final Matrix mx)
    {
        require("-=", matchingDimensions(this, mx));
        applyRowColumnValueFunction((row, column, value)
                -> clean(value - mx.entry(row, column)));
    }

    /**
     * Matrix multiplication.
     * @return {@code this * mx}
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if this matrix does not have the same
     * number of columns as the number of rows of the argument
     */
    public final Matrix times(final Matrix mx)
    {
        require("*", numberOfColumns(mx.rowCount(), this));
        final Matrix result = retainAttributes(
                newInstance(rowCount(), mx.columnCount()));
        result.applyRowColumnFunction((row, column)
                -> rowDotMatrixColumn(mx, row, column));
        return result;
    }

    private double rowDotMatrixColumn(
            final Matrix matrix, final int row, final int column)
    {
        assert row >= 0 && row < rowCount();
        assert column >= 0 && column < matrix.columnCount();
        double d = 0;
        for (int i = 0; i < columnCount(); i++)
        {
            d += entry(row, i) * matrix.entry(i, column);
        }
        return clean(d);
    }
    
    /**
     * Returns this matrix multiplied by a scalar.
     * @return {@code this * scalar}
     */
    public final Matrix times(final double scalar)
    {
        final Matrix result = retainAttributes(
                newInstance(rowCount(), columnCount()));
        result.applyRowColumnFunction((row, column)
                -> entry(row, column) * scalar);
        return result;
    }
    
    /**
     * Returns this matrix divided by a scalar.
     * @return {@code this / scalar}
     */
    public final Matrix dividedBy(final double scalar)
    {
        return times(1 / scalar);
    }
    
    /**
     * Dot product.
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * unless this and the argument are vectors of equal length
     */
    public final double dot(final Matrix vec)
    {
        require(
                "Scalar product",
                numberOfColumns(1, this),
                matchingDimensions(this, vec));
        double result = 0;
        for (int row = 0; row < rowCount(); row++)
        {
            result += entry(row, 0) * vec.entry(row, 0);
        }
        return clean(result);
    }

    /**
     * Vector product.
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * unless this and the argument are vectors of length 3
     */
    public final Matrix cross(final Matrix vec)
    {
        require("Vector product", rowsAndColumns(3, 1, this, vec));
        
        final Matrix result = retainAttributes(newInstance(3, 1));
        
        result.setEntry(0, 0,
                clean(entry(1, 0) * vec.entry(2, 0)
                        - entry(2, 0) * vec.entry(1, 0)));
        result.setEntry(1, 0,
                clean(entry(2, 0) * vec.entry(0, 0)
                        - entry(0, 0) * vec.entry(2, 0)));
        result.setEntry(2, 0,
                clean(entry(0, 0) * vec.entry(1, 0)
                        - entry(1, 0) * vec.entry(0, 0)));
        
        return result;
    }
    
    /**
     * Adds a multiple of one row to another row.
     * @param fromRow the row to multiply
     * @param fromColumn the starting column at {@code fromRow}
     * @param scalar the multiple
     * @param targetRow the target row
     * @throws UnsupportedOperationException if not supported
     * @throws IndexOutOfBoundsException if any of
     * {@code fromRow}, {@code fromColumn} and {@code targetRow} is invalid
     */
    public final void addRowMultiple(
            final int fromRow,
            final int fromColumn,
            final double scalar,
            final int targetRow)
    {
        checkRowIndex(fromRow);
        checkColumnIndex(fromColumn);
        checkRowIndex(targetRow);
        for (int column = fromColumn; column < columnCount(); column++)
        {
            setEntry(targetRow, column, clean(
                    entry(fromRow, column) * scalar
                    + entry(targetRow, column)));
        }
    }
    
      ////////////////////
     // Object methods //
    ////////////////////
    
    @Override
    public final boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj instanceof Matrix)
        {
            return equals((Matrix)obj);
        }
        return false;
    }

    private boolean equals(final Matrix matrix)
    {
        if (rowCount() != matrix.rowCount()
                || columnCount() != matrix.columnCount())
        {
            return false;
        }
        for (int row = 0; row < rowCount(); row++)
        {
            for (int column = 0; column < columnCount(); column++)
            {
                final double entry1 = entry(row, column);
                final double entry2 = matrix.entry(row, column);
                if (Double.compare(entry1, entry2) != 0)
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public final int hashCode()
    {
        int result = 17 * rowCount() ^ 19 * columnCount();
        for (int row = 0; row < rowCount(); row++)
        {
            for (int column = 0; column < columnCount(); column++)
            {
                result
                    = 31 * result
                    + Double.hashCode(entry(row, column)) * (row ^ column);
            }
        }
        return result;
    }
    
    @Override
    public final String toString()
    {
        return stringifier.stringify(this);
    }
    
      /////////////////////////
     // Argument validation //
    /////////////////////////
    
    private int checkRowIndex(final int index)
    {
        return checkIndex(index, rowCount());
    }
    
    private int checkColumnIndex(final int index)
    {
        return checkIndex(index, columnCount());
    }

    private void checkPartition(
            final int fromRow, final int fromColumn,
            final int toRow, final int toColumn)
    {
        checkFromToIndex(fromRow, toRow, rowCount());
        checkFromToIndex(fromColumn, toColumn, columnCount());
    }

    private static int requireZeroPlus(final int arg, final String argName)
    {
        if (arg < 0)
        {
            throw new IllegalArgumentException(argName + " < 0: " + arg);
        }
        return arg;
    }
    
      ///////////
     // Misc. //
    ///////////
    
    private Matrix retainAttributes(final Matrix derived)
    {
        derived.delta = this.delta;
        derived.stringifier = this.stringifier;
        return derived;
    }

}
