package com.calclipse.math.parser.misc;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.ArgMessages;

/**
 * This class contains static utility methods for checking arguments
 * given to mathematical operations.
 * 
 * @author Tone Sommerland
 */
public final class ArgUtil
{
    private ArgUtil()
    {
    }
    
    /**
     * Checks that {@code argCount} is nonzero, and throws an exception if not.
     * @param argCount actual number of arguments
     * @throws ErrorMessage if {@code argCount} is zero
     */
    public static void requireNonzeroArgCount(final int argCount)
            throws ErrorMessage
    {
        if (argCount == 0)
        {
            throw errorMessage(ArgMessages.expectedNonzeroArgCount());
        }
    }
    
    /**
     * Checks that the number of received arguments
     * equals the number of arguments required,
     * and throws an exception if not.
     * @param required the number of arguments required
     * @param argCount the number of arguments received
     * @throws ErrorMessage if {@code argCount != required}
     */
    public static void requireArgCount(final int required, final int argCount)
            throws ErrorMessage
    {
        if (argCount != required)
        {
            throw errorMessage(ArgMessages.expectedArgCount(required));
        }
    }
    
    /**
     * Checks that the number of received arguments
     * is in the range {@code [minRequired, maxRequired]},
     * and throws an exception if not.
     * @param minRequired the minimum number of arguments required
     * @param maxRequired the maximum number of arguments required
     * @param argCount the number of arguments received
     * @throws ErrorMessage if {@code argCount} is outside the required range
     */
    public static void requireMinMaxArgCount(
            final int minRequired,
            final int maxRequired,
            final int argCount) throws ErrorMessage
    {
        if (argCount < minRequired || argCount > maxRequired)
        {
            throw errorMessage(ArgMessages.expectedMinMaxArgCount(
                    minRequired, maxRequired));
        }
    }
    
    /**
     * Checks that the number of received arguments
     * is equal to or greater than the minimum number required,
     * and throws an exception if not.
     * @param minRequired the minimum number of arguments required
     * @param argCount the number of arguments received
     * @throws ErrorMessage if {@code argCount < minRequired}
     */
    public static void requireMinimumArgCount(
            final int minRequired, final int argCount) throws ErrorMessage
    {
        if (argCount < minRequired)
        {
            throw errorMessage(
                    ArgMessages.expectedMinimumArgCount(minRequired));
        }
    }
    
    /**
     * Checks that the number of received arguments
     * is equal to or less than the maximum number required,
     * and throws an exception if not.
     * @param maxRequired the maximum number of arguments required
     * @param argCount the number of arguments received
     * @throws ErrorMessage if {@code argCount > maxRequired}
     */
    public static void requireMaximumArgCount(
            final int maxRequired, final int argCount) throws ErrorMessage
    {
        if (argCount > maxRequired)
        {
            throw errorMessage(
                    ArgMessages.expectedMaximumArgCount(maxRequired));
        }
    }
    
    /**
     * Creates an error message with a message formatted to indicate
     * that it's for a specific argument.
     * @param argNumber one-based argument number
     * @param message the message
     * @param cause the cause (nullable)
     * @throws IllegalArgumentException if {@code argNumber < 1}
     */
    public static ErrorMessage forArgument(
            final int argNumber, final String message, final Throwable cause)
    {
        return errorMessage(ArgMessages.forArgument(argNumber, message))
                .withCause(cause);
    }

    /**
     * Creates an error message with a message formatted to indicate
     * that it's for a specific argument.
     * The cause's message will be used.
     * @param argNumber one-based argument number
     * @param cause the cause
     * @throws IllegalArgumentException if {@code argNumber < 1}
     */
    public static ErrorMessage forArgument(
            final int argNumber, final Throwable cause)
    {
        return forArgument(argNumber, cause.getMessage(), cause);
    }
    
    /**
     * Creates an error message with a message formatted to indicate
     * that it's for a specific argument.
     * @param argNumber one-based argument number
     * @param message the message
     * @throws IllegalArgumentException if {@code argNumber < 1}
     */
    public static ErrorMessage forArgument(
            final int argNumber, final String message)
    {
        return errorMessage(ArgMessages.forArgument(argNumber, message));
    }

}
