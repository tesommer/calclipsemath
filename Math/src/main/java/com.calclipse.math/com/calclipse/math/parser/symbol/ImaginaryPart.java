package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The imaginary part of a complex.
 * Supported types:
 * <ul>
 *  <li>complex</li>
 *  <li>real: returns zero</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class ImaginaryPart implements UnaryOperation
{
    public static final String NAME = "im";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private ImaginaryPart(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new ImaginaryPart(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Complex)
        {
            final var c = (Complex)o;
            return Value.constantOf(typeContext.numberType()
                    .valueOf(c.imaginary()));
            
        }
        else if (o instanceof Number)
        {
            return Value.constantOf(typeContext.numberType().valueOf(0));
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
