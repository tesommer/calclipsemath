package com.calclipse.math;

import static java.util.Objects.requireNonNull;

import java.math.BigInteger;

/**
 * An arbitrary-precision fraction.
 * 
 * @author Tone Sommerland
 */
public final class Fraction extends Number implements Comparable<Fraction>
{
    private static final long serialVersionUID = 1L;
    
    private final BigInteger num;
    private final BigInteger den;
    private final int sign;
    
    /**
     * Creates a new fraction.
     * @throws ArithmeticException if the denominator is zero
     */
    private Fraction(final BigInteger numerator, final BigInteger denominator)
    {
        requireNonNull(numerator, "numerator");
        requireNonNull(denominator, "denominator");
        if (denominator.equals(BigInteger.ZERO))
        {
            throw new ArithmeticException(
                    "Division by zero: " + numerator + '/' + denominator);
        }
        this.num = numerator;
        this.den = denominator;
        this.sign = calculateSignum(numerator, denominator);
    }
    
    private static int calculateSignum(
            final BigInteger numerator, final BigInteger denominator)
    {
        final int numSign = numerator.signum();
        if (numSign == 0)
        {
            return 0;
        }
        else if (numSign == denominator.signum())
        {
            return 1;
        }
        return -1;
    }
    
    /**
     * Returns a fraction instance.
     * @param numerator the numerator
     * @param denominator the denominator
     * @throws ArithmeticException if the denominator is zero
     */
    public static Fraction valueOf(
            final BigInteger numerator, final BigInteger denominator)
    {
        return new Fraction(numerator, denominator);
    }
    
    /**
     * The numerator of this fraction.
     */
    public BigInteger numerator()
    {
        return num;
    }
    
    /**
     * The denominator of this fraction.
     */
    public BigInteger denominator()
    {
        return den;
    }
    
    /**
     * The sign function.
     * @return -1 if this fraction is negative,
     * 1 if it is positive and 0 if it is zero
     */
    public int signum()
    {
        return sign;
    }
    
    /**
     * The absolute value of this fraction.
     */
    public Fraction abs()
    {
        return new Fraction(num.abs(), den.abs());
    }
    
    /**
     * The negative of this fraction.
     * @return a fraction made by negating the numerator of {@code this}
     */
    public Fraction negated()
    {
        return new Fraction(num.negate(), den);
    }
    
    /**
     * Swaps the numerator and denominator.
     * @throws ArithmeticException if the numerator of this fraction is zero
     */
    public Fraction inverse()
    {
        return new Fraction(den, num);
    }
    
    /**
     * Reduces this fraction to its lowest terms.
     * @return an non-reducible fraction with a positive denominator
     */
    public Fraction reduced()
    {
        final BigInteger gcd = num.gcd(den);
        final var lowestTerms = new Fraction(num.divide(gcd), den.divide(gcd));
        if (lowestTerms.sign >= 0)
        {
            return lowestTerms.abs();
        }
        return lowestTerms.abs().negated();
    }
    
    /**
     * Returns {@code this + f}.
     */
    public Fraction plus(final Fraction f)
    {
        final BigInteger n = num.multiply(f.den).add(f.num.multiply(den));
        final BigInteger d = den.multiply(f.den);
        return new Fraction(n, d).reduced();
    }
    
    /**
     * Returns {@code this - f}.
     */
    public Fraction minus(final Fraction f)
    {
        return plus(f.negated());
    }
    
    /**
     * Returns {@code this * f}.
     */
    public Fraction times(final Fraction f)
    {
        final BigInteger n = num.multiply(f.num);
        final BigInteger d = den.multiply(f.den);
        return new Fraction(n, d).reduced();
    }
    
    /**
     * Returns {@code this / f}.
     * @throws ArithmeticException if the numerator of the argument is zero
     */
    public Fraction dividedBy(final Fraction f)
    {
        return times(f.inverse());
    }
    
    /**
     * Raises this fraction to a power.
     * @param exponent the power
     * @return {@code this^exponent}
     * @throws ArithmeticException
     * if the exponent is negative and the numerator is zero
     */
    public Fraction toThePowerOf(final int exponent)
    {
        if (exponent < 0)
        {
            final int minusExponent = -exponent;
            return new Fraction(den.pow(minusExponent), num.pow(minusExponent));
        }
        return new Fraction(num.pow(exponent), den.pow(exponent));
    }

    @Override
    public int intValue()
    {
        return (int)doubleValue();
    }

    @Override
    public long longValue()
    {
        return (long)doubleValue();
    }

    @Override
    public float floatValue()
    {
        return (float)doubleValue();
    }

    @Override
    public double doubleValue()
    {
        final Fraction redu = reduced();
        final BigInteger[] divAndRem = redu.num.divideAndRemainder(redu.den);
        return divAndRem[0].doubleValue()
                + divAndRem[1].doubleValue() / redu.den.doubleValue();
    }

    @Override
    public int compareTo(final Fraction frac)
    {
        if (sign < frac.sign)
        {
            return -1;
        }
        else if (sign > frac.sign)
        {
            return 1;
        }
        else if (sign == 0)
        {
            return 0;
        }
        return compareNonzeroSameSigned(frac);
    }

    private int compareNonzeroSameSigned(final Fraction frac)
    {
        final BigInteger magnitude1 = num.multiply(frac.den).abs();
        final BigInteger magnitude2 = frac.num.multiply(den).abs();
        if (sign > 0)
        {
            return magnitude1.compareTo(magnitude2);
        }
        return magnitude2.compareTo(magnitude1);
    }
    
    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Fraction)
        {
            final var frac = (Fraction)obj;
            return num.multiply(frac.den).equals(frac.num.multiply(den));
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        if (sign == 0)
        {
            return 0;
        }
        final BigInteger gcd = num.gcd(den);
        final BigInteger absReducedNum = num.divide(gcd).abs();
        final BigInteger absReducedDen = den.divide(gcd).abs();
        int result = Integer.hashCode(sign);
        result = 31 * result + absReducedNum.hashCode();
        result = 31 * result + absReducedDen.hashCode();
        return result;
    }
    
    @Override
    public String toString()
    {
        return num.toString() + '/' + den.toString();
    }

}
