package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Inverts a boolean value.
 * Supported types:
 * <ul>
 *  <li>boolean</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Not implements UnaryOperation
{
    public static final String NAME = "not";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Not(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.NEGATION, new Not(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final boolean b = typeContext.booleanType().isTrue(arg.get());
        return Value.constantOf(typeContext.booleanType().valueOf(!b));
    }

}
