package com.calclipse.math.matrix;

import static java.util.Objects.requireNonNull;

import java.util.function.BiFunction;
import java.util.function.DoubleFunction;

import com.calclipse.math.matrix.internal.MatrixStringification;

/**
 * Makes string representations of matrices.
 * 
 * @author Tone Sommerland
 */
public final class MatrixStringifier
{
    private final BiFunction
    <
        ? super Matrix,
        ? super DoubleFunction<String>,
        String
    >
    arrayStringifier;
    
    private final DoubleFunction<String> entryStringifier;

    private MatrixStringifier(
            final BiFunction
            <
                ? super Matrix,
                ? super DoubleFunction<String>,
                String
            >
            arrayStringifier,
            final DoubleFunction<String> entryStringifier)
    {
        this.arrayStringifier = requireNonNull(
                arrayStringifier, "arrayStringifier");
        this.entryStringifier = requireNonNull(
                entryStringifier, "entryStringifier");
    }
    
    /**
     * Returns a matrix stringifier composed of
     * the given array stringifier and entry stringifier.
     * @param arrayStringifier responsible for the layout of
     * the entries in the string
     * @param entryStringifier stringifies individual entries
     */
    public static MatrixStringifier composedOf(
            final BiFunction
            <
                ? super Matrix,
                ? super DoubleFunction<String>,
                String
            >
            arrayStringifier,
            final DoubleFunction<String> entryStringifier)
    {
        return new MatrixStringifier(arrayStringifier, entryStringifier);
    }
    
    /**
     * The default matrix stringifier.
     */
    public static MatrixStringifier standard()
    {
        return new MatrixStringifier(
                MatrixStringification::stringify,
                Double::toString);
    }
    
    /**
     * The array stringifier of this matrix stringifier.
     */
    public BiFunction<? super Matrix, ? super DoubleFunction<String>, String>
    arrayStringifier()
    {
        return arrayStringifier;
    }

    /**
     * The entry stringifier of this matrix stringifier.
     */
    public DoubleFunction<String> entryStringifier()
    {
        return entryStringifier;
    }

    /**
     * Returns a matrix stringifier similar to this,
     * but with the given array stringifier.
     */
    public MatrixStringifier withArrayStringifier(
            final BiFunction
            <
                ? super Matrix,
                ? super DoubleFunction<String>,
                String
            >
            arrayStringifier)
    {
        return new MatrixStringifier(arrayStringifier, this.entryStringifier);
    }
    
    /**
     * Returns a matrix stringifier similar to this,
     * but with the given entry stringifier.
     */
    public final MatrixStringifier withEntryStringifier(
            final DoubleFunction<String> entryStringifier)
    {
        return new MatrixStringifier(this.arrayStringifier, entryStringifier);
    }
    
    /**
     * Creates a string representation of a matrix.
     */
    public String stringify(final Matrix matrix)
    {
        return arrayStringifier.apply(matrix, entryStringifier);
    }
    
}
