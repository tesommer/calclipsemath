package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Fraction;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The denominator of a fraction.
 * Supported types:
 * <ul>
 *  <li>fraction</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Denominator implements UnaryOperation
{
    public static final String NAME = "den";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Denominator(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Denominator(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Fraction)
        {
            final var f = (Fraction)o;
            return Value.constantOf(typeContext.numberType()
                    .valueOf(f.denominator()));
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
