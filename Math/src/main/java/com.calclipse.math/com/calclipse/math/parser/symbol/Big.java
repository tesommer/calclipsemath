package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import com.calclipse.math.Fraction;
import com.calclipse.math.Real;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.ParseMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Makes an
 * {@link com.calclipse.math.Real arbitrary-precision real number} (bignum).
 * Only the first argument is mandatory.
 * The second argument is converted to a string;
 * it should contain the rounding-mode name (e.g. {@code HALF_EVEN}).
 * The third argument is the precision of the math context.
 * The fourth argument is the fracturability.
 * Supported types:
 * <ul>
 * <li>fraction[, string[, integer, [integer]]]</li>
 * <li>real[, string[, integer, [integer]]]:
 * gives an existing real new math context and fracturability</li>
 * <li>string[, string[, integer, [integer]]]:
 * the first string is used to construct a big decimal</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Big implements VariableArityOperation
{
    public static final String NAME = "big";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private Big(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Big(typeContext)).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(1, 4, args.length);
        final Object o1 = args[0].get();
        final Real real;
        if (o1 instanceof Fraction)
        {
            real = makeReal((Fraction)o1, args);
        }
        else if (o1 instanceof Real)
        {
            real = makeReal((Real)o1, args);
        }
        else
        {
            real = makeReal(o1.toString(), args);
        }
        return Value.constantOf(real);
    }

    private MathContext mathContext(final Value[] args)
            throws ErrorMessage
    {
        if (args.length < 2)
        {
            return typeContext.numberType().mathContext();
        }
        final RoundingMode rounding;
        try
        {
            rounding = RoundingMode.valueOf(args[1].get().toString());
        }
        catch (final IllegalArgumentException ex)
        {
            throw ArgUtil.forArgument(2, ArgMessages.invalidArgument(), ex);
        }
        final int precision;
        if (args.length < 3)
        {
            precision
                = typeContext.numberType().mathContext().getPrecision();
        }
        else
        {
            precision = TypeUtil.argToInt(3, args[2].get());
            if (precision < 0)
            {
                throw ArgUtil.forArgument(3, EvalMessages.domainError());
            }
        }
        return new MathContext(precision, rounding);
    }

    private int fracturability(final Value[] args) throws ErrorMessage
    {
        if (args.length < 4)
        {
            return typeContext.numberType().fracturability();
        }
        return TypeUtil.argToInt(4, args[3].get());
    }
    
    private Real makeReal(final Fraction fraction, final Value[] args)
            throws ErrorMessage
    {
        return Real.fractured(
                fraction, mathContext(args), fracturability(args));
    }
    
    private Real makeReal(final Real real, final Value[] args)
            throws ErrorMessage
    {
        if (real.isFractured())
        {
            return makeReal(real.fraction(), args);
        }
        return Real.unfractured(
                real.bigDecimal(),
                mathContext(args),
                fracturability(args));
    }
    
    private Real makeReal(final String value, final Value[] args)
            throws ErrorMessage
    {
        try
        {
            return Real.unfractured(
                    new BigDecimal(value),
                    mathContext(args),
                    fracturability(args));
        }
        catch (final NumberFormatException ex)
        {
            throw ArgUtil.forArgument(
                    1, ParseMessages.invalidNumber(value), ex);
        }
        catch (final ArithmeticException ex)
        {
            throw ArgUtil.forArgument(1, ex);
        }
    }

}
