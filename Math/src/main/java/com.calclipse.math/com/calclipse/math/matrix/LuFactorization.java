package com.calclipse.math.matrix;

import static com.calclipse.math.matrix.size.Constraints.matchingRows;
import static com.calclipse.math.matrix.size.Constraints.require;
import static java.util.Objects.requireNonNull;

import java.util.Optional;

/**
 * The LU decomposition of a matrix
 * consists of a lower triangular factor L,
 * an upper triangular factor U
 * and a permutation matrix P.
 * The decomposition of A yields A = PLU.
 * 
 * @author Tone Sommerland
 */
public final class LuFactorization
{
    private final Matrix l;
    private final Matrix u;
    private final Permutation p;
    private final Matrix augmentation;
    
    /**
     * Creates an LU factorization.
     * @param mx the matrix to decompose
     * @param aug augmentation (nullable)
     * @param pivoting whether or not partial pivoting should be applied
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the matrix to decompose and the augmentation
     * have unequal number of rows
     */
    private LuFactorization(
            final Matrix mx,
            final Matrix aug,
            final boolean pivoting)
    {
        if (aug != null)
        {
            require("Augmentation rows mismatch.", matchingRows(mx, aug));
        }
        this.l = mx.create(mx.rows(), mx.rows());
        this.u = mx.copy();
        this.augmentation = aug == null ? null : aug.copy();
        final var pBuilder = new Permutation.Builder(mx.rows());
        factorize(pBuilder, pivoting);
        this.p = pBuilder.build();
    }
    
    /**
     * Returns an LU factorization of a matrix.
     * @param mx the matrix to decompose
     * @param pivoting whether or not partial pivoting should be applied
     */
    public static LuFactorization factorize(
            final Matrix mx, final boolean pivoting)
    {
        return new LuFactorization(mx, null, pivoting);
    }
    
    /**
     * Returns an LU factorization of an augmented matrix.
     * @param mx the matrix to decompose
     * @param aug the augmentation
     * @param pivoting whether or not partial pivoting should be applied
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the matrix to decompose and the augmentation
     * have unequal number of rows
     */
    public static LuFactorization factorizeAugmented(
            final Matrix mx, final Matrix aug, final boolean pivoting)
    {
        return new LuFactorization(mx, requireNonNull(aug, "aug"), pivoting);
    }

    /**
     * The lower trapezoidal L factor.
     * The returned matrix is unmodifiable.
     */
    public Matrix l()
    {
        return l.unmodifiable();
    }

    /**
     * The upper triangular U factor.
     * The returned matrix is unmodifiable.
     */
    public Matrix u()
    {
        return u.unmodifiable();
    }

    /**
     * The permutation.
     */
    public Permutation p()
    {
        return p;
    }

    /**
     * The augmentation after row operations.
     * The returned matrix is unmodifiable.
     */
    public Optional<Matrix> augmentation()
    {
        return augmentation == null ?
                Optional.empty() : Optional.of(augmentation.unmodifiable());
    }

    private void factorize(
            final Permutation.Builder p, final boolean pivoting)
    {
        for (int row = 0, col = 0; row < u.rows() && col < u.columns();)
        {
            ensurePivotIfPossible(p, row, col, pivoting);
            if (u.get(row, col) != 0)
            {
                processPivot(row, col);
                row++;
            }
            col++;
        }
    }
    
    private void ensurePivotIfPossible(
            final Permutation.Builder p,
            final int row,
            final int col,
            final boolean pivoting)
    {
        if (pivoting)
        {
            doForcedPivoting(p, row, col);
        }
        else if (u.get(row, col) == 0)
        {
            doEmergencyPivoting(p, row, col);
        }
    }

    private void doForcedPivoting(
            final Permutation.Builder p, final int row, final int col)
    {
        int swapRow = row;
        for (int i = row + 1; i < u.rows(); i++)
        {
            if (Math.abs(u.get(i, col)) > Math.abs(u.get(swapRow, col)))
            {
                swapRow = i;
            }
        }
        if (swapRow != row)
        {
            doSwap(p, swapRow, row);
        }
    }

    private void doEmergencyPivoting(
            final Permutation.Builder p, final int row, final int col)
    {
        for (int i = row + 1; i < u.rows(); i++)
        {
            if (u.get(i, col) != 0)
            {
                doSwap(p, i, row);
                break;
            }
        }
    }
    
    private void doSwap(
            final Permutation.Builder p, final int row1, final int row2)
    {
        l.swapRows(row1, row2);
        u.swapRows(row1, row2);
        p.swap(row1, row2);
        if (augmentation != null)
        {
            augmentation.swapRows(row1, row2);
        }
    }

    private void processPivot(final int row, final int col)
    {
        l.set(row, row, 1);
        for (int i = row + 1; i < u.rows(); i++)
        {
            if (u.get(i, col) != 0)
            {
                final double x = -u.get(i, col) / u.get(row, col);
                l.set(i, row, -x);
                u.addRowMultiple(row, col, x, i);
                if (augmentation != null && augmentation.columns() > 0)
                {
                    augmentation.addRowMultiple(row, 0, x, i);
                }
            }
        }
    }

}
