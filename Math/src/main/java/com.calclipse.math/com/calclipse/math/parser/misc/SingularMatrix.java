package com.calclipse.math.parser.misc;

/**
 * Thrown when trying to invert a singular matrix.
 * 
 * @author Tone Sommerland
 */
public class SingularMatrix extends Exception
{
    private static final long serialVersionUID = 1L;

    public SingularMatrix(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public SingularMatrix(final String message)
    {
        super(message);
    }

    public SingularMatrix(final Throwable cause)
    {
        super(cause);
    }

}
