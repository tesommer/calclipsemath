package com.calclipse.math.matrix.size;

/**
 * Indicates a size mismatch
 * (such as when trying to add matrices of different sizes),
 * that a matrix operation is undefined for the dimensions at hand
 * (such as calculating the determinant of a non-square matrix),
 * or something similar.
 * 
 * @author Tone Sommerland
 */
public final class SizeViolation extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    
    private final Size[] violations;
    
    SizeViolation(final String message, final Size[] violations)
    {
        super(message);
        assert violations.length > 0;
        this.violations = violations.clone();
    }

    /**
     * Returns the sizes involved in this exception being thrown.
     */
    public Size[] detail()
    {
        return violations.clone();
    }

}
