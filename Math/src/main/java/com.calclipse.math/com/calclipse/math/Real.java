package com.calclipse.math;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.NoSuchElementException;

/**
 * An immutable arbitrary-precision real number.
 * An instance contains either a
 * {@link com.calclipse.math.Fraction}
 * or a {@code java.math.BigDecimal}.
 * If containing a fraction, the fraction is reduced.
 * If containing a big decimal, trailing zeros have been stripped.
 * A real instance containing a fraction is referred to as <i>fractured</i>.
 * One containing a big decimal is referred to as <i>unfractured</i>.
 * All real instances contain a {@code java.math.MathContext}.
 * In addition, a real contains a property named <i>fracturability</i>.
 * This is an integer that determines how easy it is to
 * fracture an unfractured real.
 * Fracturing a real means
 * turning it into a real with a fraction rather than a big decimal.
 * -1 means it's always fracturable.
 * -2 or less means it's never fracturable.
 * 0 or more means it's fracturable if the big decimal's scale
 * is less than or equal to the fracturability.
 * That is, the real is fracturable if the fracturability
 * is at least as high as the number of decimals.
 * Note that fracturing e.g. {@code 0.33333}
 * would result in a real containing the fraction
 * {@code 33333/100000}, not {@code 1/3}.
 * A real containing a fraction is always fracturable,
 * regardless of the fracturability property.
 * The fracturability comes into play in binary operations,
 * such as {@link #plus(Real)}.
 * If the two operands (i.e. {@code this} and the augend)
 * are both either fractured or unfractured,
 * the result is also fractured or unfractured, whichever it is.
 * If one is fractured, and the other is both unfractured and fracturable,
 * then the unfractured argument is fractured so the result becomes fractured.
 * The result of a binary operation will receive the math context with the
 * highest precision,
 * or {@code this}'s math context if the precisions are equal.
 * The result will get {@code this}'s fracturability.
 * As far as comparison is concerned,
 * math contexts and fracturabilities are ignored.
 * {@link #equals(Object)}, {@link #hashCode()} and {@link #compareTo(Real)}
 * are consistent with one another.
 * A fractured real with a value of 1/2
 * is equal to an unfractured real with a value of 0.5.
 * But a fractured real with infinite decimals
 * is unequal to any unfractured real.
 * 
 * @author Tone Sommerland
 */
public final class Real extends Number implements Comparable<Real>
{
    private static final long serialVersionUID = 1L;
    
    private final Fraction fraction;
    private final BigDecimal bigDecimal;
    private final MathContext mathContext;
    private final int fracturability;
    
    private Real(
            final Fraction fraction,
            final BigDecimal bigDecimal,
            final MathContext mathContext,
            final int fracturability)
    {
        if (fraction == null)
        {
            this.fraction = null;
            this.bigDecimal = bigDecimal.stripTrailingZeros();
        }
        else
        {
            this.fraction = fraction.reduced();
            this.bigDecimal = null;
        }
        this.mathContext = requireNonNull(mathContext, "mathContext");
        this.fracturability = fracturability;
    }

    /**
     * Returns a fractured real (one containing a fraction).
     * @param fraction the fraction
     * @param mathContext the math context
     * @param fracturability the fracturability
     */
    public static Real fractured(
            final Fraction fraction,
            final MathContext mathContext,
            final int fracturability)
    {
        return new Real(fraction, null, mathContext, fracturability);
    }
    
    /**
     * Returns an unfractured real (one containing a big decimal).
     * @param bigDecimal the big decimal
     * @param mathContext the math context
     * @param fracturability the fracturability
     */
    public static Real unfractured(
            final BigDecimal bigDecimal,
            final MathContext mathContext,
            final int fracturability)
    {
        return new Real(null, bigDecimal, mathContext, fracturability);
    }
    
    /**
     * Whether or not this real is fractured.
     * @return {@code true} if this real contains a fraction,
     * {@code false} if it contains a big decimal
     */
    public boolean isFractured()
    {
        return fraction != null;
    }
    
    /**
     * Returns the fraction of this real.
     * This is present if {@link #isFractured()} returns {@code true}.
     * @throws NoSuchElementException if this real is unfractured
     */
    public Fraction fraction()
    {
        if (fraction == null)
        {
            throw new NoSuchElementException("fraction");
        }
        return fraction;
    }
    
    /**
     * Returns the big decimal of this real.
     * This is present if {@link #isFractured()} returns {@code false}.
     * @throws NoSuchElementException if this real is fractured
     */
    public BigDecimal bigDecimal()
    {
        if (bigDecimal == null)
        {
            throw new NoSuchElementException("bigDecimal");
        }
        return bigDecimal;
    }
    
    /**
     * Returns the math context of this real.
     */
    public MathContext mathContext()
    {
        return mathContext;
    }
    
    /**
     * Returns the fracturability of this real.
     */
    public int fracturability()
    {
        return fracturability;
    }
    
    /**
     * Returns {@code true} if this real is fracturable.
     * If this real is fracturable,
     * {@link #fracture()} can be called.
     * This method returns {@code true} if this real already
     * {@link #isFractured() is fractured}.
     * Otherwise it depends on the fracturability property
     * whether this real is fracturable or not.
     */
    public boolean isFracturable()
    {
        if (fraction != null)
        {
            return true;
        }
        if (fracturability < -1)
        {
            return false;
        }
        if (fracturability == -1)
        {
            return true;
        }
        return fracturability >= bigDecimal.scale();
    }
    
    /**
     * Fractures this real (turns it into a real with a fraction).
     * If this real already {@link #isFractured() is fractured},
     * this method returns {@code this}.
     * @return a real with a fraction
     * @throws ArithmeticException if this real is not
     * {@link #isFracturable() fracturable}
     */
    public Real fracture()
    {
        if (!isFracturable())
        {
            throw new ArithmeticException("Not fracturable");
        }
        if (fraction != null)
        {
            return this;
        }
        return fractured(
                toFraction(bigDecimal),
                mathContext,
                fracturability);
    }
    
    /**
     * Unfractures this real (turns it into a real with a big decimal).
     * If this real already is unfractured
     * (i.e. {@link #isFractured()} returns {@code false}),
     * this method returns {@code this}.
     * @return a real with a big decimal
     * @throws ArithmeticException
     * see {@code java.math.BigDecimal.divide(BigDecimal, MathContext)}
     */
    public Real unfracture()
    {
        if (bigDecimal != null)
        {
            return this;
        }
        return unfractured(
                toBigDecimal(fraction, mathContext),
                mathContext,
                fracturability);
    }
    
    /**
     * The sign of this real.
     * @return -1 if it's negative, 1 if it's positive and 0 if it's zero
     */
    public int signum()
    {
        return fraction == null ? bigDecimal.signum() : fraction.signum();
    }
    
    /**
     * The negative of this real.
     */
    public Real negated()
    {
        return fraction == null
                ? unfractured(bigDecimal.negate(), mathContext, fracturability)
                : fractured(fraction.negated(), mathContext, fracturability);
    }
    
    /**
     * The absolute value of this real.
     */
    public Real abs()
    {
        return fraction == null
                ? unfractured(bigDecimal.abs(), mathContext, fracturability)
                : fractured(fraction.abs(), mathContext, fracturability);
    }
    
    /**
     * Raises this real to a power.
     * @param exponent the exponent
     * @return {@code this^exponent}
     * @throws ArithmeticException if the exponent is negative,
     * this real is fractured and the numerator is zero,
     * otherwise, if {@code BigDecimal.pow(int, MathContext)} does
     */
    public Real toThePowerOf(final int exponent)
    {
        return fraction == null
                ? unfractured(
                        bigDecimal.pow(exponent, mathContext),
                        mathContext,
                        fracturability)
                : fractured(
                        fraction.toThePowerOf(exponent),
                        mathContext,
                        fracturability);
    }

    /**
     * Returns {@code this + augend}.
     * @throws ArithmeticException
     * potentially if having to unfracture an argument,
     * otherwise if {@code BigDecimal.add(BigDecimal, MathContext} does
     */
    public Real plus(final Real augend)
    {
        final var params = new BinaryParams(this, augend);
        return params.evaluate(
                (left, right)
                    -> left.plus(right),
                (left, right, mathContext)
                    -> left.add(right, mathContext));
    }

    /**
     * Returns {@code this - subtrahend}.
     * @throws ArithmeticException
     * potentially if having to unfracture an argument,
     * otherwise if {@code BigDecimal.subtract(BigDecimal, MathContext} does
     */
    public Real minus(final Real subtrahend)
    {
        final var params = new BinaryParams(this, subtrahend);
        return params.evaluate(
                (left, right)
                    -> left.minus(right),
                (left, right, mathContext)
                    -> left.subtract(right, mathContext));
    }
    
    /**
     * Returns {@code this * multiplicand}.
     * @throws ArithmeticException
     * potentially if having to unfracture an argument,
     * otherwise if {@code BigDecimal.multiply(BigDecimal, MathContext} does
     */
    public Real times(final Real multiplicand)
    {
        final var params = new BinaryParams(this, multiplicand);
        return params.evaluate(
                (left, right)
                    -> left.times(right),
                (left, right, mathContext)
                    -> left.multiply(right, mathContext));
    }
    
    /**
     * Returns {@code this / divisor}.
     * @throws ArithmeticException
     * on division by zero,
     * potentially if having to unfracture an argument,
     * otherwise if {@code BigDecimal.divide(BigDecimal, MathContext} does
     */
    public Real dividedBy(final Real divisor)
    {
        final var params = new BinaryParams(this, divisor);
        return params.evaluate(
                (left, right)
                    -> left.dividedBy(right),
                (left, right, mathContext)
                    -> left.divide(right, mathContext));
    }

    @Override
    public int intValue()
    {
        return fraction == null
                ? bigDecimal.intValue() : fraction.intValue();
    }

    @Override
    public long longValue()
    {
        return fraction == null
                ? bigDecimal.longValue() : fraction.longValue();
    }

    @Override
    public float floatValue()
    {
        return fraction == null
                ? bigDecimal.floatValue() : fraction.floatValue();
    }

    @Override
    public double doubleValue()
    {
        return fraction == null
                ? bigDecimal.doubleValue() : fraction.doubleValue();
    }
    
    @Override
    public int compareTo(final Real real)
    {
        return normalize().compareTo(real.normalize());
    }

    @Override
    public int hashCode()
    {
        return normalize().hashCode();
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof Real)
        {
            return normalize().equals(((Real)obj).normalize());
        }
        return false;
    }
    
    @Override
    public String toString()
    {
        if (bigDecimal != null)
        {
            return bigDecimal.toString();
        }
        try
        {
            return toBigDecimal(fraction, mathContext).toString();
        }
        catch (final ArithmeticException ex)
        {
            return toBigDecimal(fraction, MathContext.DECIMAL64).toString();
        }
    }

    private Fraction normalize()
    {
        return fraction == null ? toFraction(bigDecimal) : fraction;
    }

    /**
     * Transforms a big decimal to fraction
     * using the unscaled value as the numerator.
     */
    private static Fraction toFraction(final BigDecimal bigDecimal)
    {
        final BigDecimal stripped = bigDecimal.stripTrailingZeros();
        if (stripped.scale() > 0)
        {
            return Fraction.valueOf(
                    stripped.unscaledValue(),
                    BigInteger.TEN.pow(stripped.scale()));
        }
        else if (stripped.scale() < 0)
        {
            return Fraction.valueOf(
                    stripped.unscaledValue().multiply(
                            BigInteger.TEN.pow(-stripped.scale())),
                    BigInteger.ONE);
        }
        else
        {
            return Fraction.valueOf(stripped.unscaledValue(), BigInteger.ONE);
        }
    }
    
    /**
     * Transforms a fraction to a big decimal
     * by dividing the numerator by the denominator.
     * @throws ArithmeticException
     * see {@code java.math.BigDecimal.divide(BigDecimal, MathContext)
     */
    private static BigDecimal toBigDecimal(
            final Fraction fraction,
            final MathContext mathContext)
    {
        return new BigDecimal(fraction.numerator()).divide(
                new BigDecimal(fraction.denominator()), mathContext);
    }
    
    /**
     * Parameters of a binary operation on reals.
     */
    private static final class BinaryParams
    {
        private final Fraction leftFraction;
        private final Fraction rightFraction;
        private final BigDecimal leftBigDecimal;
        private final BigDecimal rightBigDecimal;
        private final MathContext mathContext;
        private final int fracturability;
        
        /**
         * Creates an object containing parameters for a binary operation.
         * @param left the left operand
         * @param right the right operand
         * @throws ArithmeticException
         * potentially if having to convert a fraction to a big decimal
         * (see {@link Real#toBigDecimal(Fraction, MathContext)})
         */
        public BinaryParams(final Real left, final Real right)
        {
            if (left.fraction == null && right.fraction == null)
            {
                leftFraction = null;
                rightFraction = null;
                leftBigDecimal = left.bigDecimal;
                rightBigDecimal = right.bigDecimal;
            }
            else if (left.bigDecimal == null && right.bigDecimal == null)
            {
                leftFraction = left.fraction;
                rightFraction = right.fraction;
                leftBigDecimal = null;
                rightBigDecimal = null;
            }
            else if (left.fraction == null)
            {
                if (left.isFracturable())
                {
                    leftFraction = toFraction(left.bigDecimal);
                    rightFraction = right.fraction;
                    leftBigDecimal = null;
                    rightBigDecimal = null;
                }
                else
                {
                    leftFraction = null;
                    rightFraction = null;
                    leftBigDecimal = left.bigDecimal;
                    rightBigDecimal
                        = toBigDecimal(right.fraction, right.mathContext);
                }
            }
            else
            {
                if (right.isFracturable())
                {
                    leftFraction = left.fraction;
                    rightFraction = toFraction(right.bigDecimal);
                    leftBigDecimal = null;
                    rightBigDecimal = null;
                }
                else
                {
                    leftFraction = null;
                    rightFraction = null;
                    leftBigDecimal
                        = toBigDecimal(left.fraction, left.mathContext);
                    rightBigDecimal = right.bigDecimal;
                }
            }
            mathContext = selectMathContext(left, right);
            fracturability = left.fracturability;
        }
        
        /**
         * Performs a binary operation with these parameters.
         * @param fracOp an operation that takes fractions
         * @param bigDecOp an operation that takes big decimals
         * @throws ArithmeticException if the operation does
         */
        public Real evaluate(
                final BinaryFracOp fracOp, final BinaryBigDecOp bigDecOp)
        {
            if (leftFraction == null)
            {
                final BigDecimal result = bigDecOp.evaluate(
                        leftBigDecimal, rightBigDecimal, mathContext);
                return unfractured(result, mathContext, fracturability);
            }
            final Fraction result = fracOp.evaluate(
                    leftFraction, rightFraction);
            return fractured(result, mathContext, fracturability);
        }
        
        /**
         * Selects the math context to use in a binary operation.
         */
        private static MathContext selectMathContext(
                final Real r1, final Real r2)
        {
            if (r2.mathContext.getPrecision() > r1.mathContext.getPrecision())
            {
                return r2.mathContext;
            }
            return r1.mathContext;
        }
    }
    
    /**
     * A binary operation on fractions.
     */
    @FunctionalInterface
    private static interface BinaryFracOp
    {
        /**
         * Evaluates this operation.
         * @throws ArithmeticException if the operation does
         */
        public abstract Fraction evaluate(Fraction left, Fraction right);
    }
    
    /**
     * A binary operation on big decimals.
     */
    @FunctionalInterface
    private static interface BinaryBigDecOp
    {
        /**
         * Evaluates this operation.
         * @throws ArithmeticException if the operation does
         */
        public abstract BigDecimal evaluate(
                BigDecimal left,
                BigDecimal right,
                MathContext mathContext);
    }

}
