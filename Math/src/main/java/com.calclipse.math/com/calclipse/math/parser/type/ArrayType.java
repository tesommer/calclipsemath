package com.calclipse.math.parser.type;

import java.util.Collection;
import java.util.List;

/**
 * Represents the math parser's array type.
 * 
 * @author Tone Sommerland
 */
public interface ArrayType
{
    /**
     * Creates an array containing the given elements.
     */
    public abstract Array createArray(Collection<?> elements);
    
    /**
     * Returns an array containing the given elements.
     * @implSpec
     * Calls {@link #createArray(Collection)}.
     */
    public default Array arrayOf(final Object... elements)
    {
        return createArray(List.of(elements));
    }

}
