package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The size of an object.
 * Supported types:
 * <ul>
 *  <li>string: the length</li>
 *  <li>array: the length</li>
 *  <li>matrix: an array containing the number of rows and columns</li>
 *  <li>Sizer</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Size implements UnaryOperation
{
    public static final String NAME = "size";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Size(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Size(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        return typeContext.sizer().sizeOf(arg);
    }

}
