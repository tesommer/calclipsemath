package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.MatrixUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The determinant of a matrix.
 * Supported types:
 * <ul>
 *  <li>matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Determinant implements UnaryOperation
{
    public static final String NAME = "det";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Determinant(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Determinant(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            try
            {
                return Value.constantOf(typeContext.numberType()
                        .valueOf(MatrixUtil.determinant(mx)));
            }
            catch (final SizeViolation ex)
            {
                throw errorMessage(EvalMessages.invalidSize()).withCause(ex);
            }
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
