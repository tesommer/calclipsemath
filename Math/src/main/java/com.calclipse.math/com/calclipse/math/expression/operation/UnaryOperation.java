package com.calclipse.math.expression.operation;

import static java.util.Objects.requireNonNull;

import java.util.function.Supplier;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;

/**
 * A mathematical operation on a single value.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface UnaryOperation
{
    /**
     * Performs the evaluation.
     * @param arg the argument
     * @return the result of the evaluation
     * @throws ErrorMessage if the evaluation resulted in an error
     */
    public abstract Value evaluate(Value arg) throws ErrorMessage;
    
    /**
     * Returns this operation as variable arity.
     * The returned operation expects one argument.
     * @param wrongNumberOfArguments
     * to throw when the returned operation receives
     * the wrong number of arguments.
     */
    public default VariableArityOperation asVariableArity(
            final Supplier<? extends ErrorMessage> wrongNumberOfArguments)
    {
        return args ->
        {
            if (args.length != 1)
            {
                throw wrongNumberOfArguments.get();
            }
            return UnaryOperation.this.evaluate(args[0]);
        };
    }
    
    /**
     * Overrides the given operation with this operation.
     * @param operation the operation to override
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default UnaryOperation override(final UnaryOperation operation)
    {
        requireNonNull(operation, "operation");
        return arg -> operation.overriddenEvaluate(arg, this);
    }
    
    /**
     * Allows this operation to be overridden by the given operation.
     * @param override the operation that overrides this one
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default UnaryOperation acceptOverride(
            final UnaryOperation override)
    {
        requireNonNull(override, "override");
        return arg -> overriddenEvaluate(arg, override);
    }
    
    private Value overriddenEvaluate(
            final Value arg,
            final UnaryOperation override) throws ErrorMessage
    {
        final Value result = override.evaluate(arg);
        if (result == Values.YIELD)
        {
            return evaluate(arg);
        }
        return result;
    }

}
