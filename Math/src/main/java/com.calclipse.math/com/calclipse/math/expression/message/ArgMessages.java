package com.calclipse.math.expression.message;

import static java.util.Objects.requireNonNull;

import java.text.MessageFormat;

/**
 * Argument-related user-level error messages.
 * Each message has a static accessor and a corresponding mutator in
 * {@link Builder}.
 * Any parameters accepted by a message's accessor are used as format elements.
 * This means that the number of format elements of each message
 * can be determined by looking at the message's accessor.
 * (See {@code java.text.MessageFormat} for information on the format pattern.)
 * 
 * This class has the following messages:
 * 
 * <ul>
 * 
 * <li>{@code invalidArgument}:
 * An operation was given an invalid argument.
 * {@link com.calclipse.math.expression.message.EvalMessages}
 * contains more specific messages that may apply.</li>
 * 
 * <li>{@code unexpectedArgCount}:
 * An operation was given the wrong number of arguments.
 * This message isn't very specific and should be used as a last resort.</li>
 * 
 * <li>{@code expectedNonzeroArgCount}:
 * An operation that expects arguments were invoked without arguments.</li>
 * 
 * <li>{@code expectedArgCount}:
 * An operation was given the wrong number of arguments.
 * This message specifies how many were expected.</li>
 * 
 * <li>{@code expectedMinMaxArgCount}:
 * An operation was given the wrong number of arguments.
 * This message specifies the minimum and maximum number of arguments
 * required.</li>
 * 
 * <li>{@code expectedMinimumArgCount}:
 * An operation was given the wrong number of arguments.
 * This message specifies the minimum number of arguments required.</li>
 * 
 * <li>{@code expectedMaximumArgCount}:
 * An operation was given the wrong number of arguments.
 * This message specifies the maximum number of arguments required.</li>
 * 
 * <li>{@code forArgument}:
 * Used for formatting an error message to further specify an argument.
 * The argument is specified using an argument number,
 * where the first argument is number one.</li>
 * 
 * </ul>
 * 
 * This class is thread safe.
 * 
 * @author Tone Sommerland
 */
public final class ArgMessages
{
    private static final class Internals
    {
        private String
        invalidArgument = "Invalid argument";
        
        private String
        unexpectedArgCount = "Wrong number of arguments.";
        
        private String
        expectedNonzeroArgCount = "At least one argument expected.";
        
        private String
        expectedArgCount = "{0} arguments expected.";
        
        private String
        expectedMinMaxArgCount = "Expected between {0} and {1} arguments.";
        
        private String
        expectedMinimumArgCount = "Expected at least {0} arguments.";
        
        private String
        expectedMaximumArgCount = "Expected max {0} arguments.";
        
        private String
        forArgument = "{0} (Argument: {1})";

        private Internals()
        {
        }
    }

    /**
     * Builds instances of {@link ArgMessages}.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final Internals internals = new Internals();

        /**
         * Creates a new builder
         * with a default set of messages as a starting point.
         */
        public Builder()
        {
        }

        /**
         * Sets the {@code invalidArgument} message.
         * @param invalidArgument a message with zero format arguments
         * @return {@code this}
         */
        public Builder setInvalidArgument(final String invalidArgument)
        {
            internals.invalidArgument = requireNonNull(
                    invalidArgument, "invalidArgument");
            return this;
        }

        /**
         * Sets the {@code unexpectedArgCount} message.
         * @param unexpectedArgCount a message with zero format arguments
         * @return {@code this}
         */
        public Builder setUnexpectedArgCount(final String unexpectedArgCount)
        {
            internals.unexpectedArgCount = requireNonNull(
                    unexpectedArgCount, "unexpectedArgCount");
            return this;
        }

        /**
         * Sets the {@code expectedNonzeroArgCount} message.
         * @param expectedNonzeroArgCount a message with zero format arguments
         * @return {@code this}
         */
        public Builder setExpectedNonzeroArgCount(
                final String expectedNonzeroArgCount)
        {
            internals.expectedNonzeroArgCount = requireNonNull(
                    expectedNonzeroArgCount, "expectedNonzeroArgCount");
            return this;
        }

        /**
         * Sets the {@code expectedArgCount} message.
         * @param expectedArgCount a message with one format argument
         * @return {@code this}
         */
        public Builder setExpectedArgCount(final String expectedArgCount)
        {
            internals.expectedArgCount = requireNonNull(
                    expectedArgCount, "expectedArgCount");
            return this;
        }

        /**
         * Sets the {@code expectedMinMaxArgCount} message.
         * @param expectedMinMaxArgCount a message with two format arguments
         * @return {@code this}
         */
        public Builder setExpectedMinMaxArgCount(
                final String expectedMinMaxArgCount)
        {
            internals.expectedMinMaxArgCount = requireNonNull(
                    expectedMinMaxArgCount, "expectedMinMaxArgCount");
            return this;
        }

        /**
         * Sets the {@code expectedMinimumArgCount} message.
         * @param expectedMinimumArgCount a message with one format argument
         * @return {@code this}
         */
        public Builder setExpectedMinimumArgCount(
                final String expectedMinimumArgCount)
        {
            internals.expectedMinimumArgCount = requireNonNull(
                    expectedMinimumArgCount, "expectedMinimumArgCount");
            return this;
        }

        /**
         * Sets the {@code expectedMaximumArgCount} message.
         * @param expectedMaximumArgCount a message with one format argument
         * @return {@code this}
         */
        public Builder setExpectedMaximumArgCount(
                final String expectedMaximumArgCount)
        {
            internals.expectedMaximumArgCount = requireNonNull(
                    expectedMaximumArgCount, "expectedMaximumArgCount");
            return this;
        }

        /**
         * Sets the {@code forArgument} message.
         * @param forArgument a message with two format arguments
         * @return {@code this}
         */
        public Builder setForArgument(final String forArgument)
        {
            internals.forArgument = requireNonNull(forArgument, "forArgument");
            return this;
        }
        
        /**
         * Builds the argument messages.
         */
        public ArgMessages build()
        {
            return new ArgMessages(internals);
        }
    }
    
    private static volatile ArgMessages
    instance = new ArgMessages(new Internals());
    
    private final String invalidArgument;
    private final String unexpectedArgCount;
    private final String expectedNonzeroArgCount;
    private final String expectedArgCount;
    private final String expectedMinMaxArgCount;
    private final String expectedMinimumArgCount;
    private final String expectedMaximumArgCount;
    private final String forArgument;

    private ArgMessages(final Internals internals)
    {
        assert internals.invalidArgument         != null;
        assert internals.unexpectedArgCount      != null;
        assert internals.expectedNonzeroArgCount != null;
        assert internals.expectedArgCount        != null;
        assert internals.expectedMinMaxArgCount  != null;
        assert internals.expectedMinimumArgCount != null;
        assert internals.expectedMaximumArgCount != null;
        assert internals.forArgument             != null;
        this.invalidArgument         = internals.invalidArgument;
        this.unexpectedArgCount      = internals.unexpectedArgCount;
        this.expectedNonzeroArgCount = internals.expectedNonzeroArgCount;
        this.expectedArgCount        = internals.expectedArgCount;
        this.expectedMinMaxArgCount  = internals.expectedMinMaxArgCount;
        this.expectedMinimumArgCount = internals.expectedMinimumArgCount;
        this.expectedMaximumArgCount = internals.expectedMaximumArgCount;
        this.forArgument             = internals.forArgument;
    }
    
    /**
     * Sets the main instance.
     */
    public static void set(final ArgMessages instance)
    {
        ArgMessages.instance = requireNonNull(instance, "instance");
    }

    /**
     * Returns the {@code invalidArgument} message.
     */
    public static String invalidArgument()
    {
        return instance.invalidArgument;
    }

    /**
     * Returns the {@code unexpectedArgCount} message.
     */
    public static String unexpectedArgCount()
    {
        return instance.unexpectedArgCount;
    }

    /**
     * Returns the {@code expectedNonzeroArgCount} message.
     */
    public static String expectedNonzeroArgCount()
    {
        return instance.expectedNonzeroArgCount;
    }

    /**
     * Returns a formatted {@code expectedArgCount} message.
     * @param argCount expected number of arguments
     */
    public static String expectedArgCount(final Object argCount)
    {
        return MessageFormat.format(
                instance.expectedArgCount,
                requireNonNull(argCount, "argCount"));
    }
    
    /**
     * Returns a formatted {@code expectedMinMaxArgCount} message.
     * @param minArgCount minimum number of arguments expected
     * @param maxArgCount maximum number of arguments expected
     */
    public static String expectedMinMaxArgCount(
            final Object minArgCount, final Object maxArgCount)
    {
        return MessageFormat.format(
                instance.expectedMinMaxArgCount,
                requireNonNull(minArgCount, "minArgCount"),
                requireNonNull(maxArgCount, "maxArgCount"));
    }

    /**
     * Returns a formatted {@code expectedMinimumArgCount} message.
     * @param minArgCount minimum number of arguments expected
     */
    public static String expectedMinimumArgCount(final Object minArgCount)
    {
        return MessageFormat.format(
                instance.expectedMinimumArgCount,
                requireNonNull(minArgCount, "minArgCount"));
    }

    /**
     * Returns a formatted {@code expectedMaximumArgCount} message.
     * @param maxArgCount maximum number of arguments expected
     */
    public static String expectedMaximumArgCount(final Object maxArgCount)
    {
        return MessageFormat.format(
                instance.expectedMaximumArgCount,
                requireNonNull(maxArgCount, "maxArgCount"));
    }
    
    /**
     * Returns a formatted {@code forArgument} message.
     * @param argNumber one-based argument number
     * @param message the message to enrich with an argument number
     * @throws IllegalArgumentException if {@code argNumber < 1}
     */
    public static String forArgument(
            final int argNumber, final Object message)
    {
        return MessageFormat.format(
                instance.forArgument,
                requireNonNull(message, "message"),
                requireOnePlus(argNumber, "argNumber < 1: " + argNumber));
    }
    
    private static int requireOnePlus(final int arg, final String message)
    {
        if (arg < 1)
        {
            throw new IllegalArgumentException(message);
        }
        return arg;
    }

    @Override
    public String toString()
    {
        return ArgMessages.class.getSimpleName() + ": " + invalidArgument;
    }

}
