package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

/**
 * A token in an arithmetic expression.
 * 
 * @see com.calclipse.math.expression.Tokens
 * 
 * @author Tone Sommerland
 */
public class Token
{
    final TokenType type;
    final Id id;
    final String name;

    Token(final TokenType type, final Id id, final String name)
    {
        this.type = requireNonNull(type, "type");
        this.id = requireNonNull(id, "id");
        this.name = requireNonEmpty(name, "Empty name");
    }
    
    private static String requireNonEmpty(
            final String str, final String message)
    {
        if (str.isEmpty())
        {
            throw new IllegalArgumentException(message);
        }
        return str;
    }
    
    /**
     * Returns a token similar to this, but with the given ID.
     * @param newId the new ID
     */
    public Token withId(final Id newId)
    {
        return new Token(type, newId, name);
    }
    
    /**
     * Returns a token similar to this, but with the given name.
     * @param newName the new name.
     * @throws IllegalArgumentException if the name is empty
     */
    public Token withName(final String newName)
    {
        return new Token(type, id, newName);
    }

    /**
     * The type of this token.
     */
    public final TokenType type()
    {
        return type;
    }

    /**
     * The name of this token.
     */
    public final String name()
    {
        return name;
    }
    
    /**
     * Checks if this token identifies as another token.
     * This happens to be the case if they have the same
     * {@link com.calclipse.math.expression.Id ID}.
     * @param tokenId the ID of the other token
     */
    public final boolean identifiesAs(final Id tokenId)
    {
        return identifiesAs(id, tokenId);
    }
    
    /**
     * Checks if this token identifies as another token.
     * This happens to be the case if they have the same
     * {@link com.calclipse.math.expression.Id ID}.
     * @param token the other token
     */
    public final boolean identifiesAs(final Token token)
    {
        return identifiesAs(id, token.id);
    }
    
    /**
     * Checks if this token matches another token.
     * Two tokens match if their names are equal.
     * @param tokenName the name of the other token
     */
    public final boolean matches(final String tokenName)
    {
        return matches(name, tokenName);
    }
    
    /**
     * Checks if this token matches another token.
     * Two tokens match if their names are equal.
     * @param token the other token
     */
    public final boolean matches(final Token token)
    {
        return matches(name, token.name);
    }
    
    private static boolean identifiesAs(final Id id1, final Id id2)
    {
        return id1 == id2;
    }
    
    private static boolean matches(final String name1, final String name2)
    {
        return name1.equals(name2);
    }

    @Override
    public String toString()
    {
        return Token.class.getSimpleName()
                + '(' + type + ", " + id + ", " + name + ')';
    }
    
}
