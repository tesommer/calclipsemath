/**
 * Classes for enforcing matrix size constraints.
 */
package com.calclipse.math.matrix.size;
