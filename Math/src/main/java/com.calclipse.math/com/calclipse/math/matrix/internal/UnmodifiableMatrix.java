package com.calclipse.math.matrix.internal;

import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.MatrixStringifier;

/**
 * An unmodifiable view of a matrix.
 * 
 * @author Tone Sommerland
 */
public final class UnmodifiableMatrix extends Matrix
{
    private final Matrix matrix;

    public UnmodifiableMatrix(final Matrix matrix)
    {
        super(matrix.delta(), matrix.stringifier());
        this.matrix = matrix;
    }

    @Override
    protected int rowCount()
    {
        return matrix.rows();
    }

    @Override
    protected int columnCount()
    {
        return matrix.columns();
    }

    @Override
    protected double entry(final int row, final int column)
    {
        return matrix.get(row, column);
    }

    @Override
    protected void setEntry(
            final int row, final int column, final double entry)
    {
        throw new UnsupportedOperationException("setEntry");
    }

    @Override
    protected void setDimensions(
            final int rows, final int columns, final boolean preserveEntries)
    {
        throw new UnsupportedOperationException("setDimensions");
    }

    @Override
    protected Matrix duplicate()
    {
        return matrix.copy();
    }

    @Override
    protected Matrix newInstance(final int rows, final int columns)
    {
        return matrix.create(rows, columns);
    }

    @Override
    protected double setDeltaHatch(final double newValue)
    {
        throw new UnsupportedOperationException("setDelta");
    }

    @Override
    protected MatrixStringifier setStringifierHatch(
            final MatrixStringifier newValue)
    {
        throw new UnsupportedOperationException("setStringifier");
    }

    @Override
    protected Matrix partitionHatch(
            final int fromRow,
            final int fromColumn,
            final int toRow,
            final int toColumn)
    {
        return matrix.partition(fromRow, fromColumn, toRow, toColumn);
    }

    @Override
    protected void setPartitionHatch(
            final int row, final int column, final Matrix partition)
    {
        throw new UnsupportedOperationException("setPartition");
    }

    @Override
    protected Matrix rowHatch(final int row)
    {
        return matrix.row(row);
    }

    @Override
    protected void setRowHatch(final int row, final Matrix entries)
    {
        throw new UnsupportedOperationException("setRow");
    }

    @Override
    protected Matrix columnHatch(final int column)
    {
        return matrix.column(column);
    }

    @Override
    protected void setColumnHatch(final int column, final Matrix entries)
    {
        throw new UnsupportedOperationException("setColumn");
    }

    @Override
    protected void swapRowsHatch(final int row1, final int row2)
    {
        throw new UnsupportedOperationException("swapRows");
    }

    @Override
    protected void swapColumnsHatch(final int column1, final int column2)
    {
        throw new UnsupportedOperationException("swapColumns");
    }

}
