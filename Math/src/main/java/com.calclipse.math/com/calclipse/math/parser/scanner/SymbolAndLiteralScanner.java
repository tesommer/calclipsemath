package com.calclipse.math.parser.scanner;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.ArrayDeque;
import java.util.Queue;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Scanner;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.message.ParseMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Extracts symbols and literals from expressions.
 * String literals are delimited by either single or double quotes
 * (the same delimiter must be used on both ends).
 * White space in numbers is ignored.
 * Period (.) is used as decimal point.
 * Supports C-style backslash-star star-backslash comments.
 * 
 * @author Tone Sommerland
 */
public class SymbolAndLiteralScanner implements Scanner
{
    /**
     * Default start of comment.
     */
    private static final String DEFAULT_SOC = "/*";
    
    /**
     * Default end of comment.
     */
    private static final String DEFAULT_EOC = "*/";
    
    private final Queue<Fragment> buffer = new ArrayDeque<>();

    private final SymbolTable symbolTable;
    private final TypeContext typeContext;
    
    private String input;
    private int start;
    private int position;
    
    /**
     * Creates a new symbol and literal scanner.
     * @param symbolTable the symbol dictionary
     * @param typeContext the context used when extracting literals
     */
    public SymbolAndLiteralScanner(
            final SymbolTable symbolTable, final TypeContext typeContext)
    {
        this.symbolTable = requireNonNull(symbolTable, "symbolTable");
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }

    @Override
    public final void reset(final String expression)
    {
        input = expression;
        start = 0;
        position = 0;
        buffer.clear();
    }

    @Override
    public final boolean hasNext() throws ErrorMessage
    {
        return buffer.isEmpty() ? updateBuffer() : true;
    }

    @Override
    public final Fragment next()
    {
        return buffer.remove();
    }
    
    /**
     * Checks if a comment starts at the specified index.
     * The default implementation
     * considers backslash-star to be the start of a comment.
     * @param str the expression being scanned
     * @param index the index to check for comment
     */
    protected boolean isStartOfComment(final String str, final int index)
    {
        return str.startsWith(DEFAULT_SOC, index);
    }
    
    /**
     * Returns the index immediately following the end of a comment,
     * or negative if the comment is unterminated.
     * Star-backslash ends a comment by default.
     * @param str the expression being scanned
     * @param index the index of the start of the comment (including delimiter)
     * @see #isStartOfComment(String, int)
     */
    protected int indexAfterComment(final String str, final int index)
    {
        final int eoc = str.indexOf(DEFAULT_EOC, index + DEFAULT_SOC.length());
        return eoc < 0 ? -1 : eoc + DEFAULT_EOC.length();
    }
    
    /**
     * Adds the next token to the buffer.
     * @return false if there are no more tokens.
     */
    private boolean updateBuffer() throws ErrorMessage
    {
        for (; position < input.length(); position++)
        {
            if (checkComment())
            {
                return flushComment();
            }
            
            Token token = symbolAt(position);
            if (token != null)
            {
                return flush(Fragment.of(token, position));
                
            }
            else if (!Character.isWhitespace(input.charAt(position)))
            {
                token = stringAt(position);
                if (token == null)
                {
                    token = numberAt(position);
                }
                if (token != null)
                {
                    return flush(Fragment.of(token, position));
                }
            }
        }
        
        return flush(null);
    }
    
    /**
     * Flushes a fragment to the buffer and updates start and position.
     * Any undefined symbol between start and position will also be flushed.
     * @param frag (nullable)
     * @return true if anything was added to the buffer.
     */
    private boolean flush(final Fragment frag)
    {
        boolean flushed = frag != null;
        
        final String undef = input.substring(start, position).trim();
        if (!undef.isEmpty())
        {
            final Token token = Tokens.undefined(undef);
            final int index = input.indexOf(undef, start);
            buffer.add(Fragment.of(token, index));
            flushed = true;
        }
        
        if (frag != null)
        {
            buffer.add(frag);
            position += frag.token().name().length();
        }
        
        start = position;
        return flushed;
    }
    
    private boolean checkComment()
    {
        return isStartOfComment(input, position);
    }
    
    private boolean flushComment() throws ErrorMessage
    {
        final int indexAfterComment = indexAfterComment(input, position);
        
        if (indexAfterComment < 0)
        {
            final Token token = Tokens.plain(input.substring(position));
            final Fragment frag = Fragment.of(token, position);
            throw errorMessage(ParseMessages.undelimitedComment())
                .attach(frag);
        }
        
        final boolean flushed = flush(null);
        start = indexAfterComment;
        position = start;
        return flushed ? true : updateBuffer();
    }
    
    private Token numberAt(final int index) throws ErrorMessage
    {
        char c = input.charAt(index);
        
        if (Character.isDigit(c) || c == '.')
        {
            final var numBuf = new StringBuilder();
            numBuf.append(c);
            
            int i;
            for (i = index + 1; i < input.length(); i++)
            {
                if (symbolAt(i) != null)
                {
                    break;
                }
                c = input.charAt(i);
                if (Character.isDigit(c) || c == '.')
                {
                    numBuf.append(c);
                }
                else if (!Character.isWhitespace(c))
                {
                    break;
                }
            }
            
            final String name = input.substring(index, i).trim();
            final Object value;
            try
            {
                value = typeContext.numberType().parse(numBuf.toString());
            }
            catch (final ErrorMessage ex)
            {
                final Token token = Tokens.plain(name);
                final Fragment frag = Fragment.of(token, index);
                throw ex.attach(frag);
            }
            
            return Operand.of(name, Value.constantOf(value));
        }
        
        return null;
    }
    
    private Token stringAt(final int index) throws ErrorMessage
    {
        final char c = input.charAt(index);
        
        if (c == '\'' || c == '"')
        {
            final int nextDelim = input.indexOf(c, index + 1);
            if (nextDelim < 0)
            {
                final Token token = Tokens.plain(input.substring(index));
                final Fragment frag = Fragment.of(token, index);
                throw errorMessage(ParseMessages.undelimitedString())
                    .attach(frag);
            }
            final String name = input.substring(index, nextDelim + 1);
            final String value = input.substring(index + 1, nextDelim);
            return Operand.of(name, Value.constantOf(value));
        }
        
        return null;
    }
    
    private Token symbolAt(final int index)
    {
        final char c = input.charAt(index);
        return symbolTable.lookUp(c).stream()
                .filter(symbol -> input.startsWith(symbol.name(), index))
                .findFirst()
                .orElse(null);
    }

}
