package com.calclipse.math.matrix;

import static java.util.Objects.checkIndex;

import java.util.ArrayList;
import java.util.List;

/**
 * A QR decomposition of a matrix with Householder reflections.
 * Q is orthogonal.
 * R is upper triangular (i.e. square with zeroes below its main diagonal)
 * if the factorized matrix is square.
 * 
 * @author Tone Sommerland
 */
public final class QrFactorization
{
    private final Matrix r;
    private final List<Matrix> reflectors;
    private final int[] pivotRows;
    
    private QrFactorization(final Matrix matrix)
    {
        r = matrix.copy();
        reflectors = new ArrayList<>(r.columns());
        
        final var pivRows = new ArrayList<Integer>(r.columns());
        
        for (int row = 0, col = 0; row < r.rows() && col < r.columns();)
        {
            if (nonzeroExistsBelow(r, row, col))
            {
                final Matrix u = reflector(r, row, col);
                reflectors.add(u);
                pivRows.add(row);
                
                double d;
                for (int i = col; i < r.columns(); i++)
                {
                    d = 0;
                    for (int j = row; j < u.rows(); j++)
                    {
                        d += r.get(j, i) * u.get(j, 0);
                    }
                    d *= 2;
                    for (int j = row; j < r.rows(); j++)
                    {
                        r.set(j, i, r.get(j, i) - d * u.get(j, 0));
                    }
                }
                
                row++;
                
            }
            else if (r.get(row, col) != 0)
            {
                row++;
            }
            
            col++;
        }
        
        r.clean();
        
        pivotRows = pivRows.stream().mapToInt(i -> i).toArray();
    }
    
    /**
     * Returns a QR factorization of the given matrix.
     * @param matrix the matrix to decompose
     */
    public static QrFactorization factorize(final Matrix matrix)
    {
        return new QrFactorization(matrix);
    }
    
    private static boolean nonzeroExistsBelow(
            final Matrix matrix, final int row, final int column)
    {
        for (int i = row + 1; i < matrix.rows(); i++)
        {
            if (matrix.get(i, column) != 0)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * The R factor.
     * The returned matrix is unmodifiable.
     */
    public Matrix r()
    {
        return r.unmodifiable();
    }
    
    /**
     * The rows in R containing pivot positions.
     * These are the rows for which a reflector has been created
     * to annihilate elements below the pivot.
     * The pivot row at index {@code i}
     * corresponds to the elementary reflector at the same index.
     * @see #reflectors()
     */
    public int[] pivotRows()
    {
        return pivotRows.clone();
    }

    /**
     * The unit vectors characteristic of the Householder matrices
     * used in the decomposition (in the order they were applied).
     * The returned matrices are unmodifiable.
     */
    public Matrix[] reflectors()
    {
        return reflectors.stream()
                .map(Matrix::unmodifiable)
                .toArray(Matrix[]::new);
    }
    
    /**
     * Calculates the orthogonal Q factor.
     */
    public Matrix computeQ()
    {
        // Q = H1*H2*...*Hn
        
        if (reflectors.isEmpty())
        {
            return r.identity(r.rows());
        }

        final Matrix q = r.create(r.rows(), r.rows());
        Matrix u = reflectors.get(0);
        
        // Initializes Q to H1:
        for (int i = 0; i < u.rows(); i++)
        {
            if (i < pivotRows[0])
            {
                q.set(i, i, 1);
            }
            else
            {
                double d = u.get(i, 0);
                q.set(i, i, -2 * d * d + 1);
                d *= -2;
                for (int j = i + 1; j < q.rows(); j++)
                {
                    final double d2 = u.get(j, 0) * d;
                    q.set(i, j, d2);
                    q.set(j, i, d2);
                }
            }
        }
        
        // Computes Q*H2*...*Hn
        for (int i = 1; i < reflectors.size(); i++)
        {
            u = reflectors.get(i);
            for (int j = 0; j < q.rows(); j++)
            {
                double d = 0;
                for (int k = pivotRows[i]; k < u.rows(); k++)
                {
                    d += q.get(j, k) * u.get(k, 0);
                }
                d *= 2;
                for (int k = pivotRows[i]; k < q.columns(); k++)
                {
                    q.set(j, k, q.get(j, k) - d * u.get(k, 0));
                }
            }
        }
        
        q.clean();
        return q;
    }
    
    /**
     * Finds a Householder matrix H that can be used to
     * eliminate the elements of a matrix A below the specified position,
     * by applying the transformation HA.
     * @return the unit vector characteristic of H.
     */
    public static Matrix reflector(
            final Matrix matrix, final int row, final int column)
    {
        checkIndex(row, matrix.rows());
        checkIndex(column, matrix.columns());
        
        final Matrix u = matrix.create(matrix.rows(), 1);
        double s = 0;
        
        for (int i = row; i < matrix.rows(); i++)
        {
            final double d = matrix.get(i, column);
            u.set(i, 0, d);
            s += d * d;
        }
        
        if (u.clean(s) == 0)
        {
            return u;
        }
        
        s = Math.sqrt(s);
        
        if (u.get(row, 0) < 0)
        {
            u.set(row, 0, u.get(row, 0) - s);
        }
        else
        {
            u.set(row, 0, u.get(row, 0) + s);
        }
        
        s = 1 / Math.sqrt(2 * s * (s + Math.abs(matrix.get(row, column))));
        u.scaleColumn(0, s);
        
        return u;
    }

}
