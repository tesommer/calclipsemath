package com.calclipse.math.matrix;

import static com.calclipse.math.matrix.size.Constraints.numberOfColumns;
import static com.calclipse.math.matrix.size.Constraints.require;

/**
 * A matrix suitable for a dense element representation.
 * 
 * @author Tone Sommerland
 */
public final class DenseMatrix extends Matrix
{
    private int rowCount;
    private int columnCount;
    private double[][] entries;

    private DenseMatrix(
            final int rowCount,
            final int columnCount,
            final double... entries)
    {
        this.rowCount = requireZeroPlus(rowCount, "rowCount");
        this.columnCount = requireZeroPlus(columnCount, "columnCount");
        this.entries = copyEntries(rowCount, columnCount, entries);
    }
    
    private DenseMatrix(final DenseMatrix matrix)
    {
        super(matrix.delta(), matrix.stringifier());
        this.rowCount = matrix.rowCount;
        this.columnCount = matrix.columnCount;
        this.entries = new double[this.rowCount][];
        for (int i = 0; i < this.rowCount; i++)
        {
            this.entries[i] = matrix.entries[i].clone();
        }
    }
    
    /**
     * Returns a dense matrix.
     * The matrix is populated row by row, left to right,
     * using the given {@code entries} array.
     * {@code entries} need not be {@code rowCount * columnCount} long.
     * Zero is used for unassigned positions, and excess entries are ignored.
     * @param rowCount number of rows
     * @param columnCount number of columns
     * @param entries the matrix entries starting with row zero, column zero
     * @throws IllegalArgumentException if either dimension is less than zero
     */
    public static DenseMatrix valueOf(
            final int rowCount,
            final int columnCount,
            final double... entries)
    {
        return new DenseMatrix(rowCount, columnCount, entries);
    }

    private static int requireZeroPlus(final int arg, final String argName)
    {
        if (arg < 0)
        {
            throw new IllegalArgumentException(argName + " < 0: " + arg);
        }
        return arg;
    }
    
    private static double[][] copyEntries(
            final int rowCount, final int columnCount, final double[] source)
    {
        final var destination = new double[rowCount][columnCount];
        final int entryCount = Math.min(rowCount * columnCount, source.length);
        for (int i = 0; i < entryCount; i++)
        {
            final int row = i / columnCount;
            final int col = i % columnCount;
            destination[row][col] = source[i];
        }
        return destination;
    }

    @Override
    protected int rowCount()
    {
        return rowCount;
    }

    @Override
    protected int columnCount()
    {
        return columnCount;
    }

    @Override
    protected double entry(final int row, final int column)
    {
        return entries[row][column];
    }

    @Override
    protected void setEntry(
            final int row, final int column, final double entry)
    {
        entries[row][column] = entry;
    }

    @Override
    protected void setDimensions(
            final int rows, final int columns, final boolean preserveEntries)
    {
        if (preserveEntries)
        {
            if (rows != rowCount || columns != columnCount)
            {
                final double[][] oldEntries = entries;
                entries = new double[rows][columns];
                final int minRows = Math.min(rows, rowCount);
                final int minCols = Math.min(columns, columnCount);
                for (int i = 0; i < minRows; i++)
                {
                    System.arraycopy(oldEntries[i], 0, entries[i], 0, minCols);
                }
            }
        }
        else
        {
            entries = new double[rows][columns];
        }
        rowCount = rows;
        columnCount = columns;
    }

    @Override
    protected Matrix duplicate()
    {
        return new DenseMatrix(this);
    }

    @Override
    protected Matrix newInstance(final int rows, final int columns)
    {
        return new DenseMatrix(rows, columns);
    }

    @Override
    protected Matrix rowHatch(final int row)
    {
        final var rowEntries = new DenseMatrix(1, columnCount);
        rowEntries.entries[0] = entries[row].clone();
        return rowEntries;
    }

    @Override
    protected void swapRowsHatch(final int row1, final int row2)
    {
        final double[] temp = entries[row1];
        entries[row1] = entries[row2];
        entries[row2] = temp;
    }

    /**
     * Transforms a dense matrix.
     * This method accesses the elements of the argument directly,
     * and is thus more efficient than, say,
     * {@code mx = this.times(mx)}.
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the number of columns of this matrix
     * is unequal to the number of rows of the argument.
     */
    public void transform(final DenseMatrix matrix)
    {
        require("transform", numberOfColumns(matrix.rowCount, this));
        final double[][] oldEntries = matrix.entries;
        matrix.rowCount = rowCount;
        matrix.entries = new double[rowCount][matrix.columnCount];
        for (int i = 0; i < matrix.rowCount; i++)
        {
            for (int j = 0; j < matrix.columnCount; j++)
            {
                double d = 0;
                for (int k = 0; k < columnCount; k++)
                {
                    d += entries[i][k] * oldEntries[k][j];
                }
                matrix.entries[i][j] = clean(d);
            }
        }
    }

}
