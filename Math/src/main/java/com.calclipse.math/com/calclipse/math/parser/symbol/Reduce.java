package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.Fraction;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * Reduces a fraction to its lowest terms.
 * Supported types:
 * <ul>
 *  <li>fraction</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Reduce implements UnaryOperation
{
    public static final String NAME = "reduce";
    
    private static final Id ID = Id.unique();
    
    private static final UnaryOperation INSTANCE = new Reduce();

    private Reduce()
    {
    }
    
    public static UnaryOperator operator()
    {
        return UnaryOperator.prefixed(NAME, OperatorPriorities.MEDIUM, INSTANCE)
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Fraction)
        {
            final var f = (Fraction)o;
            return Value.constantOf(f.reduced());
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
