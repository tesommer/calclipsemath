package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Factorial.
 * Supported types:
 * <ul>
 *  <li>integer</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Factorial implements UnaryOperation
{
    public static final String NAME = "!";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Factorial(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.postfixed(
                NAME, OperatorPriorities.HIGH, new Factorial(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final long n = TypeUtil.toLong(arg.get());
        try
        {
            return Value.constantOf(typeContext.numberType()
                    .valueOf(MathUtil.factorial(n)));
        }
        catch (final ArithmeticException ex)
        {
            throw errorMessage(EvalMessages.domainError()).withCause(ex);
        }
    }

}
