package com.calclipse.math.parser.step;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.TokenType;
import com.calclipse.math.expression.message.ParseMessages;

/**
 * This module checks the infix expression for errors.
 * 
 * @author Tone Sommerland
 */
public enum Check implements ParseStep
{
    INSTANCE;
    
    private static final String KEY_PREV_TYPE = "prevType";
    private static final String KEY_PAREN_COUNT = "parenCount";

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        final Fragment input = stream.input();
        final TokenType type = input.token().type();
        final var prevType = (TokenType)stream.map().get(KEY_PREV_TYPE);

        updateParenCount(stream, input, type);

        if (type.isUndefined())
        {
            throw errorMessage(ParseMessages.undefinedSymbol(
                    input.token().name())).attach(input);

        }
        else if (type.isPlain())
        {
            throwUnexpected(input);

        }
        else if (prevType == null || isLeftOrBinary(prevType))
        {
            if (isRightOrBinary(type))
            {
                throwUnexpected(input);
            }

        // isRightOrValue(prevType) == true
        }
        else if (isLeftOrValue(type))
        {
            throwUnexpected(input);
        }

        stream.map().put(KEY_PREV_TYPE, type);
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
        if (parenCount(stream) > 0)
        {
            throw errorMessage(ParseMessages.expectedRParen())
                .attach(stream.previousInput());
        }
        final var prevType = (TokenType)stream.map().get(KEY_PREV_TYPE);
        if (prevType != null && !isRightOrValue(prevType))
        {
            throw errorMessage(ParseMessages.unexpectedEnd())
                .attach(stream.previousInput());
        }
    }
    
    private static void throwUnexpected(final Fragment frag)
            throws ErrorMessage
    {
        throw errorMessage(ParseMessages.unexpected(
                frag.token().name())).attach(frag);
    }
    
    private static int parenCount(final TokenStream stream)
    {
        final var parenCount = (Integer)stream.map().get(KEY_PAREN_COUNT);
        return parenCount == null ? 0 : parenCount;
    }
    
    private static void updateParenCount(
            final TokenStream stream,
            final Fragment input,
            final TokenType type) throws ErrorMessage
    {
        if (type.isLeftParenthesis())
        {
            incrementParenCount(stream);
        }
        else if (type.isRightParenthesis())
        {
            decrementParenCount(stream, input);
        }
        
    }

    private static void incrementParenCount(final TokenStream stream)
    {
        stream.map().put(KEY_PAREN_COUNT, parenCount(stream) + 1);
    }

    private static void decrementParenCount(
            final TokenStream stream,
            final Fragment input) throws ErrorMessage
    {
        final int parenCount = parenCount(stream);
        if (parenCount <= 0)
        {
            throw errorMessage(ParseMessages.unexpectedRParen())
                .attach(input);
        }
        stream.map().put(KEY_PAREN_COUNT, parenCount - 1);
    }
    
    private static boolean isLeftOrBinary(final TokenType type)
    {
        return
                type.isLeftParenthesis()
                || type.isLeftUnary()
                || type.isBinary();
    }
    
    private static boolean isRightOrBinary(final TokenType type)
    {
        return
                type.isRightParenthesis()
                || type.isRightUnary()
                || type.isBinary();
    }
    
    private static boolean isLeftOrValue(final TokenType type)
    {
        return
                type.isLeftParenthesis()
                || type.isLeftUnary()
                || type.isOperand()
                || type.isFunction();
    }
    
    private static boolean isRightOrValue(final TokenType type)
    {
        return
                type.isRightParenthesis()
                || type.isRightUnary()
                || type.isOperand()
                || type.isFunction();
    }

}
