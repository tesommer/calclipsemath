package com.calclipse.math.parser.symbol;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * Assignment operation.
 * Returns the left argument after assignment.
 * 
 * @author Tone Sommerland
 */
public final class Assign implements BinaryOperation
{
    public static final String NAME = ":=";
    
    private static final Id ID = Id.unique();
    
    private static final BinaryOperation INSTANCE = new Assign();

    private Assign()
    {
    }
    
    public static BinaryOperator operator()
    {
        return BinaryOperator.of(NAME, OperatorPriorities.ASSIGNMENT, INSTANCE)
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        left.set(right.get());
        return left;
    }

}
