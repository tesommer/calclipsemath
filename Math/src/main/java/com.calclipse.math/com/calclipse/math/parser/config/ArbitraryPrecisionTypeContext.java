package com.calclipse.math.parser.config;

import static com.calclipse.math.expression.Errors.errorMessage;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.calclipse.math.Fraction;
import com.calclipse.math.Real;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.ParseMessages;
import com.calclipse.math.parser.type.NumberType;
import com.calclipse.math.parser.type.TypeContext;

/**
 * This is the type context of the
 * {@link com.calclipse.math.parser.config.ArbitraryPrecisionConfig}.
 * This type context creates {@code Real} instances
 * when parsing number literals.
 */
public class ArbitraryPrecisionTypeContext extends StandardTypeContext
{
    static final TypeContext
    INSTANCE = new ArbitraryPrecisionTypeContext();

    protected ArbitraryPrecisionTypeContext()
    {
    }
    
    @Override
    public NumberType numberType()
    {
        return new ArbitraryPrecisionNumberType();
    }

    private static final class ArbitraryPrecisionNumberType
        extends StandardTypeContext.StandardNumberType
    {
        private ArbitraryPrecisionNumberType()
        {
        }

        @Override
        public Number parse(final String literal) throws ErrorMessage
        {
            try
            {
                return parseReal(literal);
            }
            catch (final NumberFormatException ex)
            {
                throw errorMessage(ParseMessages.invalidNumber(literal))
                    .withCause(ex);
            }
        }
        
        /**
         * Parses a number literal and returns a {@code Real}.
         * @throws ArithmeticException if the literal is malformed.
         */
        private Real parseReal(final String literal)
        {
            try
            {
                final var num = new BigInteger(literal);
                return Real.fractured(
                        Fraction.valueOf(num, BigInteger.ONE),
                        mathContext(),
                        fracturability());
            }
            catch (final NumberFormatException ex)
            {
                return Real.unfractured(
                        new BigDecimal(literal),
                        mathContext(),
                        fracturability());
            }
        }
        
    }
    
}
