package com.calclipse.math.expression;

/**
 * A lexical analyzer used by a
 * {@link com.calclipse.math.expression.Parser}.
 * 
 * @author Tone Sommerland
 */
public interface Scanner
{
    /**
     * Resets the scanner.
     * @param expression the expression to scan
     */
    public abstract void reset(String expression) throws ErrorMessage;
    
    /**
     * Called prior to {@link #next()}
     * to see if there are more tokens to parse.
     * @return {@code true} if there are more tokens
     */
    public abstract boolean hasNext() throws ErrorMessage;
    
    /**
     * Returns the next token to parse.
     */
    public abstract Fragment next() throws ErrorMessage;
    
}
