/**
 * Symbols.
 * All symbols in this package are included in the
 * {@link com.calclipse.math.parser.config.StandardConfig}
 * except {@link com.calclipse.math.parser.symbol.Big}
 * and {@link com.calclipse.math.parser.symbol.Small}.
 * They are included in the
 * {@link com.calclipse.math.parser.config.ArbitraryPrecisionConfig}.
 */
package com.calclipse.math.parser.symbol;
