package com.calclipse.math.parser.misc;

/**
 * Thrown when trying to solve an inconsistent system of equations.
 * 
 * @author Tone Sommerland
 */
public class InconsistentSystem extends Exception
{
    private static final long serialVersionUID = 1L;

    public InconsistentSystem(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public InconsistentSystem(final String message)
    {
        super(message);
    }

    public InconsistentSystem(final Throwable cause)
    {
        super(cause);
    }

}
