package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Converts a number from radians to degrees.
 * Supported types:
 * <ul>
 *  <li>real</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Radians implements UnaryOperation
{
    public static final String NAME = "radians";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Radians(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.postfixed(
                NAME, OperatorPriorities.MEDIUM, new Radians(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final double d = TypeUtil.toDouble(arg.get());
        return Value.constantOf(typeContext.numberType()
                .valueOf(Math.toDegrees(d)));
    }

}
