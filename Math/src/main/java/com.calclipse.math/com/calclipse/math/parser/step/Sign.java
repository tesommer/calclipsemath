package com.calclipse.math.parser.step;

import static java.util.Objects.requireNonNull;

import java.util.function.Predicate;
import java.util.function.Supplier;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.TokenType;

/**
 * Support for unary minus and plus (negative and positive).
 * 
 * @author Tone Sommerland
 */
public final class Sign implements ParseStep
{
    private static final int STATE_1 = TokenStream.DEFAULT_STATE;
    private static final int STATE_2 = STATE_1 + 1;
    
    private static final String NEGATION_KEY = "negation";

    private final Supplier<? extends Token> negation;
    private final Predicate<? super Token> isAddition;
    private final Predicate<? super Token> isSubtraction;
    
    public Sign(
            final Supplier<? extends Token> negation,
            final Predicate<? super Token> isAddition,
            final Predicate<? super Token> isSubtraction)
    {
        this.negation = requireNonNull(negation, "negation");
        this.isAddition = requireNonNull(isAddition, "isAddition");
        this.isSubtraction = requireNonNull(isSubtraction, "isSubtraction");
    }
    
    private Token theNegation(final TokenStream stream)
    {
        return (Token)stream.map().computeIfAbsent(
                NEGATION_KEY, key -> negation.get());
    }

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        final Token unaryMinus = theNegation(stream);
        final Fragment input = stream.input();
        final TokenType type = input.token().type();
        
        if (type.isBinary() || type.isLeftUnary() || type.isLeftParenthesis())
        {
            final int state = stream.state();
            
            if (state == STATE_1)
            {
                if (isSubtraction.test(input.token()))
                {
                    stream.replaceInput(Fragment.of(
                            unaryMinus.withName(input.token().name()),
                            input.position()));
                }
                else if (isAddition.test(input.token()))
                {
                    stream.skip();
                }
            }
            else
            {
                stream.setState(STATE_1);
            }
            
        }
        else
        {
            stream.setState(STATE_2);
        }
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
    }

}
