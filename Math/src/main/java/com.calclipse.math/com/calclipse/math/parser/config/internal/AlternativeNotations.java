package com.calclipse.math.parser.config.internal;

import static com.calclipse.math.expression.Errors.errorMessage;
import static com.calclipse.math.parser.SymbolDependencies.binaryReference;
import static com.calclipse.math.parser.SymbolDependencies.unaryReference;

import java.util.function.Supplier;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.symbol.Abs;
import com.calclipse.math.parser.symbol.Cbrt;
import com.calclipse.math.parser.symbol.Greater;
import com.calclipse.math.parser.symbol.GreaterEquals;
import com.calclipse.math.parser.symbol.Less;
import com.calclipse.math.parser.symbol.LessEquals;
import com.calclipse.math.parser.symbol.Sqrt;
import com.calclipse.math.parser.symbol.ToThePowerOf;
import com.calclipse.math.parser.symbol.Unequals;
import com.calclipse.math.parser.type.TypeContext;

public final class AlternativeNotations
{
    private static final Supplier<ErrorMessage>
    WRONG_NUMBER_OF_ARGS = () -> errorMessage(EvalMessages.syntaxError());
    
    private static final String
    VBAR = "|";
    
    private static final String
    SUPER_2 = String.valueOf(Character.toChars(0x00B2));
    
    private static final String
    SUPER_3 = String.valueOf(Character.toChars(0x00B3));
    
    private static final String
    RADICAL_2 = String.valueOf(Character.toChars(0x221A));
    
    private static final String
    RADICAL_3 = String.valueOf(Character.toChars(0x221B));
    
    private static final String
    INEQUATION = String.valueOf(Character.toChars(0x2260));
    
    private static final String
    ORDER_LESS = String.valueOf(Character.toChars(0x227A));
    
    private static final String
    ORDER_GREATER = String.valueOf(Character.toChars(0x227B));
    
    private static final String
    ORDER_LESS_EQUAL = String.valueOf(Character.toChars(0x2264));
    
    private static final String
    ORDER_GREATER_EQUAL = String.valueOf(Character.toChars(0x2265));
    
    private static final String
    PI = String.valueOf(Character.toChars(0x03C0));

    private AlternativeNotations()
    {
    }
    
    public static void add(
            final SymbolTable symbolTable, final TypeContext typeContext)
    {
        addForAbs(symbolTable, typeContext);
        addForSquare(symbolTable, typeContext);
        addForCube(symbolTable, typeContext);
        addForSqrt(symbolTable);
        addForCbrt(symbolTable);
        addForUnequals(symbolTable);
        addForLess(symbolTable);
        addForGreater(symbolTable);
        addForLessEquals(symbolTable);
        addForGreaterEquals(symbolTable);
        addForPi(symbolTable);
    }
    
    private static void addForAbs(
            final SymbolTable symbolTable, final TypeContext typeContext)
    {
        final var symbol = (UnaryOperator)symbolTable.get(Abs.NAME);
        assert symbol != null;
        final Token alternative = Abs.function(
                Delimitation.byTokensWithoutOpener(
                        Tokens.plain(VBAR),
                        ParensAndComma.COMMA,
                        Token::matches),
                typeContext)
            .withName(VBAR)
            .withOperation(unaryReference(symbol, symbolTable::get)
                    .asVariableArity(WRONG_NUMBER_OF_ARGS));
        symbolTable.add(alternative);
    }
    
    private static void addForSquare(
            final SymbolTable symbolTable, final TypeContext typeContext)
    {
        final var symbol = (BinaryOperator)symbolTable.get(ToThePowerOf.NAME);
        assert symbol != null;
        final Token alternative = UnaryOperator.postfixed(
                SUPER_2,
                symbol.priority(),
                new RaisedTo(
                        binaryReference(symbol, symbolTable::get),
                        typeContext,
                        2));
        symbolTable.add(alternative);
    }
    
    private static void addForCube(
            final SymbolTable symbolTable, final TypeContext typeContext)
    {
        final var symbol = (BinaryOperator)symbolTable.get(ToThePowerOf.NAME);
        assert symbol != null;
        final Token alternative = UnaryOperator.postfixed(
                SUPER_3,
                symbol.priority(),
                new RaisedTo(
                        binaryReference(symbol, symbolTable::get),
                        typeContext,
                        3));
        symbolTable.add(alternative);
    }
    
    private static void addForSqrt(final SymbolTable symbolTable)
    {
        final var symbol = (UnaryOperator)symbolTable.get(Sqrt.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(RADICAL_2)
                .withOperation(unaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForCbrt(final SymbolTable symbolTable)
    {
        final var symbol = (UnaryOperator)symbolTable.get(Cbrt.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(RADICAL_3)
                .withOperation(unaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForUnequals(final SymbolTable symbolTable)
    {
        final var symbol = (BinaryOperator)symbolTable.get(Unequals.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(INEQUATION)
                .withOperation(binaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForLess(final SymbolTable symbolTable)
    {
        final var symbol = (BinaryOperator)symbolTable.get(Less.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(ORDER_LESS)
                .withOperation(binaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForGreater(final SymbolTable symbolTable)
    {
        final var symbol = (BinaryOperator)symbolTable.get(Greater.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(ORDER_GREATER)
                .withOperation(binaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForLessEquals(final SymbolTable symbolTable)
    {
        final var symbol = (BinaryOperator)symbolTable.get(LessEquals.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(ORDER_LESS_EQUAL)
                .withOperation(binaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForGreaterEquals(final SymbolTable symbolTable)
    {
        final var symbol = (BinaryOperator)symbolTable.get(GreaterEquals.NAME);
        assert symbol != null;
        final Token alternative = symbol
                .withName(ORDER_GREATER_EQUAL)
                .withOperation(binaryReference(symbol, symbolTable::get));
        symbolTable.add(alternative);
    }
    
    private static void addForPi(final SymbolTable symbolTable)
    {
        final var symbol = (Operand)symbolTable.get("pi");
        assert symbol != null;
        final Token alternative = symbol.withName(PI);
        symbolTable.add(alternative);
    }
    
    private static final class RaisedTo implements UnaryOperation
    {
        private final BinaryOperation toThePowerOf;
        private final TypeContext typeContext;
        private final int exponent;

        private RaisedTo(
                final BinaryOperation toThePowerOf,
                final TypeContext typeContext,
                final int exponent)
        {
            assert toThePowerOf != null;
            assert typeContext != null;
            this.toThePowerOf = toThePowerOf;
            this.typeContext = typeContext;
            this.exponent = exponent;
        }

        @Override
        public Value evaluate(final Value arg) throws ErrorMessage
        {
            return toThePowerOf.evaluate(arg, Value.constantOf(
                    typeContext.numberType().valueOf(exponent)));
        }
    }

}
