package com.calclipse.math.parser.step;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.message.ParseMessages;

/**
 * Creates a function tree.
 * 
 * @author Tone Sommerland
 */
public enum Arguments implements ParseStep
{
    INSTANCE;
    
    /*
     * This parse step has three states:
     * * The default/initial state:
     *   If the input is a function,
     *   it starts an argument and goes to state 1 or 2.
     * * State 1: The opener of a parameter list is expected.
     *   If the input is the opener, it goes to state 2.
     * * State 2: Now processing a parameter list.
     */
    
    private static final int STATE_1 = TokenStream.DEFAULT_STATE + 1;
    private static final int STATE_2 = STATE_1 + 1;
    
    private static final String KEY_PAREN_COUNT = "parenCount";
    private static final String KEY_EMPTY_ARG = "emptyArg";

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        final int state = stream.state();
        if (state == TokenStream.DEFAULT_STATE)
        {
            processDefaultState(stream);
        }
        else if (state == STATE_1)
        {
            processState1(stream);
        }
        else
        {
            processState2(stream);
        }
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
        if (stream.state() != TokenStream.DEFAULT_STATE)
        {
            throw errorMessage(ParseMessages.unexpectedEnd())
                .attach(stream.previousInput());            
        }
    }
    
    private static void enterState1(final TokenStream stream)
    {
        stream.setState(STATE_1);
    }
    
    private static void enterState2(final TokenStream stream)
    {
        stream.setState(STATE_2);
        stream.map().put(KEY_PAREN_COUNT, 0);
        stream.map().put(KEY_EMPTY_ARG, true);
    }
    
    private static void processFunction(final TokenStream stream)
    {
        stream.startArgument();
        if (stream.hasArgumentListOpener())
        {
            enterState1(stream);
        }
        else
        {
            enterState2(stream);
        }
    }
    
    private static void processDefaultState(final TokenStream stream)
    {
        if (stream.input().token().type().isFunction())
        {
            processFunction(stream);
        }
    }
    
    private static void processState1(final TokenStream stream)
        throws ErrorMessage
    {
        final Fragment input = stream.input();
        if (stream.readsArgumentListOpener(input.token()))
        {
            stream.skip();
            enterState2(stream);
        }
        else
        {
            final String opener = stream.node().delimiters().opener();
            throw errorMessage(ParseMessages.expected(opener)).attach(input);
        }
    }
    
    private static void processState2(final TokenStream stream)
        throws ErrorMessage
    {
        final Fragment input = stream.input();
        final int parenCount = (Integer)stream.map().get(KEY_PAREN_COUNT);
        final boolean emptyArg = (Boolean)stream.map().get(KEY_EMPTY_ARG);
        
        if (parenCount <= 0
                && stream.readsArgumentSeparator(input.token()))
        {
            processState2Separator(stream, input, emptyArg);
            
        }
        else if (parenCount <= 0
                && stream.readsArgumentListCloser(input.token()))
        {
            processState2Closer(stream, input, emptyArg);
            
        }
        else
        {
            processState2Argument(stream, input, emptyArg, parenCount);
        }
    }
    
    private static void processState2Separator(
            final TokenStream stream,
            final Fragment input,
            final boolean emptyArg) throws ErrorMessage
    {
        if (emptyArg)
        {
            final String message
                = ParseMessages.unexpected(input.token().name());
            throw errorMessage(message).attach(input);
        }
        stream.setState(TokenStream.DEFAULT_STATE);
        stream.end();
        stream.endArgument();
        stream.startArgument();
        enterState2(stream);
    }
    
    private static void processState2Closer(
            final TokenStream stream,
            final Fragment input,
            final boolean emptyArg) throws ErrorMessage
    {
        if (emptyArg)
        {
            if (stream.node().arguments().isEmpty())
            {
                stream.endArgument();
            }
            else
            {
                final String message
                    = ParseMessages.unexpected(input.token().name());
                throw errorMessage(message).attach(input);
            }
        }
        else
        {
            stream.setState(TokenStream.DEFAULT_STATE);
            stream.end();
            stream.endArgument();
        }
    }

    private static void processState2Argument(
            final TokenStream stream,
            final Fragment input,
            final boolean emptyArg,
            final int parenCount)
    {
        if (emptyArg)
        {
            stream.map().put(KEY_EMPTY_ARG, false);
        }
        if (input.token().type().isLeftParenthesis())
        {
            stream.map().put(KEY_PAREN_COUNT, parenCount + 1);
        }
        else if (input.token().type().isRightParenthesis())
        {
            stream.map().put(KEY_PAREN_COUNT, parenCount - 1);
        }
        else if (input.token().type().isFunction())
        {
            processFunction(stream);
        }
    }

}
