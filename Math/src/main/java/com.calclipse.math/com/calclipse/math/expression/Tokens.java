package com.calclipse.math.expression;

/**
 * Factory methods for creating token instances.
 * 
 * @author Tone Sommerland
 */
public final class Tokens
{
    private Tokens()
    {
    }

    /**
     * Returns a left-parenthesis token.
     * @param name the token name, e.g. {@code "("}
     * @throws IllegalArgumentException if the name is empty
     */
    public static Token leftParenthesis(final String name)
    {
        return new Token(TokenType.LPAREN, Id.unique(), name);
    }

    /**
     * Returns a right-parenthesis token.
     * @param name the token name, e.g. {@code ")"}
     * @throws IllegalArgumentException if the name is empty
     */
    public static Token rightParenthesis(final String name)
    {
        return new Token(TokenType.RPAREN, Id.unique(), name);
    }

    /**
     * Returns a plain token.
     * @param name the token name, e.g. {@code ","}
     * @throws IllegalArgumentException if the name is empty
     */
    public static Token plain(final String name)
    {
        return new Token(TokenType.PLAIN, Id.unique(), name);
    }

    /**
     * Returns an undefined token.
     * @param name the token name
     * @throws IllegalArgumentException if the name is empty
     */
    public static Token undefined(final String name)
    {
        return new Token(TokenType.UNDEFINED, Id.unique(), name);
    }

}
