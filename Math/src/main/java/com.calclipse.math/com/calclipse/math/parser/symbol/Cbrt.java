package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Cube root.
 * Supported types:
 * <ul>
 *  <li>complex</li>
 *  <li>real</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Cbrt implements UnaryOperation
{
    public static final String NAME = "cbrt";

    private static final double ONE_THIRD = 1 / 3.0;
    
    private static final Id ID = Id.unique();

    private final TypeContext typeContext;
    
    private Cbrt(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Cbrt(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Complex)
        {
            final var c = (Complex)o;
            return Value.constantOf(MathUtil.pow(c, ONE_THIRD));
            
        }
        else if (o instanceof Number)
        {
            final double d = ((Number)o).doubleValue();
            return Value.constantOf(typeContext.numberType()
                    .valueOf(Math.cbrt(d)));
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
