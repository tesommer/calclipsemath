package com.calclipse.math.expression.recursion.internal;

import java.util.concurrent.atomic.AtomicInteger;

import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.math.expression.recursion.UnboundRecursion;

/**
 * A thread-safe recursion counter.
 * 
 * @author Tone Sommerland
 */
public final class AtomicRecursionCounter implements RecursionGuard
{
    private final AtomicInteger count = new AtomicInteger();
    private final int limit;

    /**
     * Creates a thread-safe recursion counter.
     * @param limit the limit at which an {@link UnboundRecursion} is thrown
     * @throws IllegalArgumentException if the limit is less than one
     */
    public AtomicRecursionCounter(final int limit)
    {
        this.limit = requireOnePlus(limit);
    }
    
    private static int requireOnePlus(final int limit)
    {
        if (limit < 1)
        {
            throw new IllegalArgumentException("limit < 1: limit=" + limit);
        }
        return limit;
    }

    @Override
    public void increment() throws UnboundRecursion
    {
        if (count.incrementAndGet() >= limit)
        {
            throw UnboundRecursion.saying(EvalMessages.indefiniteRecursion());
        }
    }

    @Override
    public void decrement()
    {
        count.decrementAndGet();
    }

}
