package com.calclipse.math.expression.message;

import static java.util.Objects.requireNonNull;

import java.text.MessageFormat;

/**
 * Type-related user-level error messages.
 * Each message has a static accessor and a corresponding mutator in
 * {@link Builder}.
 * Any parameters accepted by a message's accessor are used as format elements.
 * This means that the number of format elements of each message
 * can be determined by looking at the message's accessor.
 * (See {@code java.text.MessageFormat} for information on the format pattern.)
 * 
 * This class has the following messages:
 * 
 * <ul>
 * 
 * <li>{@code invalidType}:
 * An argument given to an operation has the wrong type.</li>
 * 
 * <li>{@code incompatibleTypes}:
 * Two arguments were given to an operation,
 * and one or both are of the wrong type.</li>
 * 
 * </ul>
 * 
 * This class is thread safe.
 * 
 * @author Tone Sommerland
 */
public final class TypeMessages
{
    private static final class Internals
    {
        private String
        invalidType = "Invalid data type: {0}";
        
        private String
        incompatibleTypes = "Invalid or incompatible data type(s): {0} {1}";

        private Internals()
        {
        }
    }
    
    /**
     * Builds instances of {@link TypeMessages}.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final Internals internals = new Internals();
        
        /**
         * Creates a new builder
         * with a default set of messages as a starting point.
         */
        public Builder()
        {
        }

        /**
         * Sets the {@code invalidType} message.
         * @param invalidType a message with one format argument
         * @return {@code this}
         */
        public Builder setInvalidType(final String invalidType)
        {
            internals.invalidType = requireNonNull(invalidType, "invalidType");
            return this;
        }

        /**
         * Sets the {@code incompatibleTypes} message.
         * @param incompatibleTypes a message with two format arguments
         * @return {@code this}
         */
        public Builder setIncompatibleTypes(final String incompatibleTypes)
        {
            internals.incompatibleTypes
                = requireNonNull(incompatibleTypes, "incompatibleTypes");
            return this;
        }
        
        /**
         * Builds the type messages.
         */
        public TypeMessages build()
        {
            return new TypeMessages(internals);
        }
    }
    
    private static volatile TypeMessages
    instance = new TypeMessages(new Internals());
    
    private final String invalidType;
    private final String incompatibleTypes;

    private TypeMessages(final Internals internals)
    {
        assert internals.invalidType       != null;
        assert internals.incompatibleTypes != null;
        this.invalidType       = internals.invalidType;
        this.incompatibleTypes = internals.incompatibleTypes;
    }
    
    /**
     * Sets the main instance.
     */
    public static void set(final TypeMessages instance)
    {
        TypeMessages.instance = requireNonNull(instance, "instance");
    }

    /**
     * Returns a formatted {@code invalidType} message.
     * @param value the invalid value
     */
    public static String invalidType(final Object value)
    {
        return MessageFormat.format(
                instance.invalidType,
                TypeNamer.nameTypeOf(requireNonNull(value, "value")));
    }

    /**
     * Returns a formatted {@code incompatibleTypes} message.
     * @param firstValue the first incompatible value
     * @param secondValue the second incompatible value
     */
    public static String incompatibleTypes(
            final Object firstValue, final Object secondValue)
    {
        return MessageFormat.format(
                instance.incompatibleTypes,
                TypeNamer.nameTypeOf(
                        requireNonNull(firstValue, "firstValue")),
                TypeNamer.nameTypeOf(
                        requireNonNull(secondValue, "secondValue")));
    }

    @Override
    public String toString()
    {
        return TypeMessages.class.getSimpleName() + ": " + invalidType;
    }

}
