package com.calclipse.math.parser.config.internal;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.Sizer;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The default sizer.
 * Supports matrices, arrays, strings and other sizers.
 * 
 * @author Tone Sommerland
 */
public final class DefaultSizer implements Sizer
{
    private final TypeContext typeContext;

    public DefaultSizer(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }

    @Override
    public Value sizeOf(final Value value) throws ErrorMessage
    {
        final Object o = value.get();

        if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            if (value.isConstant())
            {
                final var arr = typeContext.arrayType().arrayOf(
                        typeContext.numberType().valueOf(mx.rows()),
                        typeContext.numberType().valueOf(mx.columns()));
                return Value.constantOf(arr);
            }
            else
            {
                return new MatrixSizeValue(typeContext, mx);
            }

        }
        else if (o instanceof Array)
        {
            final var arr = (Array)o;
            return Value.constantOf(typeContext.numberType()
                    .valueOf(arr.size()));

        }
        else if (o instanceof String)
        {
            final var str = (String)o;
            return Value.constantOf(typeContext.numberType()
                    .valueOf(str.length()));

        }
        else if (o instanceof Sizer)
        {
            return ((Sizer)o).sizeOf(value);
        }

        throw errorMessage(TypeMessages.invalidType(o));
    }

    private static final class MatrixSizeValue implements Value
    {
        private final TypeContext typeContext;
        private final Matrix matrix;

        private MatrixSizeValue(
                final TypeContext typeContext, final Matrix matrix)
        {
            assert typeContext != null;
            assert matrix      != null;
            this.typeContext = typeContext;
            this.matrix = matrix;
        }

        @Override
        public Object get()
        {
            return typeContext.arrayType().arrayOf(
                    typeContext.numberType().valueOf(matrix.rows()),
                    typeContext.numberType().valueOf(matrix.columns()));
        }

        @Override
        public void set(final Object value) throws ErrorMessage
        {
            if (value instanceof Array)
            {
                final var arr = (Array)value;
                if (arr.size() != 2)
                {
                    throw errorMessage(EvalMessages.invalidDimensionCount());
                }
                final int rows = TypeUtil.toInt(arr.get(0));
                final int cols = TypeUtil.toInt(arr.get(1));
                if (rows < 1 || cols < 1)
                {
                    throw errorMessage(EvalMessages.invalidSize());
                }
                matrix.setSize(rows, cols, true);
            }
            else
            {
                throw errorMessage(TypeMessages.invalidType(value));
            }
        }

        @Override
        public boolean isConstant()
        {
            return false;
        }

    }

}
