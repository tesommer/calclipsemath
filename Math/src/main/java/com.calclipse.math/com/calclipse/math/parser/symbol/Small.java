package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.ParseMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;

/**
 * Creates a {@code Long} or a {@code Double}.
 * Supports either one or two arguments.
 * If given two arguments, the second is the radix (an integer).
 * Then the string representation of the first argument will be used
 * to parse a {@code Long}
 * (if the first argument is a number,
 * the string representation of its long value is parsed).
 * If given one argument, a {@code Double} is created.
 * If the argument is a {@code Number},
 * the {@code doubleValue} will be used to create the {@code Double}.
 * Otherwise the string representation will be parsed.
 * 
 * @author Tone Sommerland
 */
public final class Small implements VariableArityOperation
{
    public static final String NAME = "small";
    
    private static final Id ID = Id.unique();
    
    private static final VariableArityOperation INSTANCE = new Small();

    private Small()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(1, 2, args.length);
        final Object o1 = args[0].get();
        try
        {
            final Number result;
            if (args.length == 1)
            {
                result = toDouble(o1);
            }
            else
            {
                final int radix = TypeUtil.argToInt(2, args[1].get());
                if (radix < Character.MIN_RADIX || radix > Character.MAX_RADIX)
                {
                    throw ArgUtil.forArgument(2, EvalMessages.domainError());
                }
                result = toLong(o1, radix);
            }
            return Value.constantOf(result);
        }
        catch (final NumberFormatException ex)
        {
            throw errorMessage(ParseMessages.invalidNumber(o1))
                .withCause(ex);
        }
    }

    /**
     * Returns the value as a double.
     * @throws NumberFormatException if given an invalid number to parse.
     */
    private static Double toDouble(final Object value)
    {
        if (value instanceof Number)
        {
            return ((Number)value).doubleValue();
        }
        return Double.valueOf(value.toString());
    }

    /**
     * Returns the value as a long.
     * @throws NumberFormatException if given an invalid number to parse.
     */
    private static Long toLong(final Object value, final int radix)
    {
        final String str;
        if (value instanceof Number)
        {
            str = "" + ((Number)value).longValue();
        }
        else
        {
            str = value.toString();
        }
        return Long.valueOf(str, radix);
    }

}
