package com.calclipse.math.parser.step;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.function.Supplier;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.TokenType;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * Support for implicit multiplication.
 * 
 * @author Tone Sommerland
 */
public final class ImplicitMultiplication implements ParseStep
{
    private static final String MULTIPLICATION_KEY = "multiplication";
    
    private final Supplier<BinaryOperator> multiplication;
    
    /**
     * Creates a new implicit multiplication step.
     * @param multiplication the multiplication operator
     */
    public ImplicitMultiplication(
            final Supplier<BinaryOperator> multiplication)
    {
        this.multiplication = requireNonNull(multiplication, "multiplication");
    }
    
    private BinaryOperator theMultiplication(final TokenStream stream)
    {
        return (BinaryOperator)stream.map().computeIfAbsent(
                MULTIPLICATION_KEY, key -> multiplication.get());
    }

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        if (stream.buffer().isEmpty())
        {
            stream.buffer().add(stream.input());
            
        }
        else
        {
            final Fragment left = stream.buffer().remove(0);
            final Fragment right = stream.input();
            final TokenType leftType = left.token().type();
            final TokenType rightType = right.token().type();
            
            if (isImplicitMultiplication(leftType, rightType))
            {
                final int priority = implicitMultiplicationPriority(
                        left, right);
                final Fragment fragment = implicitMultiplicationFragment(
                        theMultiplication(stream), priority, right);
                stream.write(fragment);
            }
            
            stream.buffer().add(right);
        }
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
    }
    
    private static boolean isImplicitMultiplication(
            final TokenType leftType, final TokenType rightType)
    {
        if (leftType.isFunction() || leftType.isOperand()
                || leftType.isRightUnary() || leftType.isRightParenthesis())
        {
            return rightType.isFunction() || rightType.isOperand()
                || rightType.isLeftUnary() || rightType.isLeftParenthesis();
        }
        return false;
    }

    private static int implicitMultiplicationPriority(
            final Fragment left, final Fragment right)
    {
        return Math.min(leftPriority(left), rightPriority(right));
    }

    private static int leftPriority(final Fragment left)
    {
        if (left.token().type().isRightUnary())
        {
            return leftOrRightPriority((Operator)left.token());
        }
        return OperatorPriorities.IMPLICIT_MULTIPLICATION;
    }

    private static int rightPriority(final Fragment right)
    {
        if (right.token().type().isLeftUnary())
        {
            return leftOrRightPriority((Operator)right.token());
        }
        return OperatorPriorities.IMPLICIT_MULTIPLICATION;
    }
    
    private static int leftOrRightPriority(final Operator operator)
    {
        return Math.min(
                OperatorPriorities.IMPLICIT_MULTIPLICATION,
                operator.priority());
    }

    private static Fragment implicitMultiplicationFragment(
            final BinaryOperator multiplication,
            final int priority,
            final Fragment right)
    {
        final String name = right.token().name();
        final Token token = wrapMultiplication(
                multiplication, name, priority);
        return Fragment.of(token, right.position());
    }
    
    private static Token wrapMultiplication(
            final BinaryOperator multiplication,
            final String name,
            final int priority)
    {
        final var operation = new OperationWrapper(multiplication.operation());
        return multiplication
                .withOperation(operation)
                .withName(name)
                .withPriority(priority);
    }
    
    private static final class OperationWrapper implements BinaryOperation
    {
        private final BinaryOperation operation;

        private OperationWrapper(final BinaryOperation operation)
        {
            assert operation != null;
            this.operation = operation;
        }

        @Override
        public Value evaluate(final Value left, final Value right)
                throws ErrorMessage
        {
            try
            {
                return operation.evaluate(left, right);
            }
            catch (final ErrorMessage ex)
            {
                final String message = EvalMessages.forImplicitMultiplication(
                        ex.getMessage());
                throw errorMessage(message).withCause(ex);
            }
        }
    }

}
