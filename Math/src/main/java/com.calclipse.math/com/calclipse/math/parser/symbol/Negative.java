package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Negation.
 * Supported types:
 * <ul>
 *  <li>fraction</li>
 *  <li>complex</li>
 *  <li>real</li>
 *  <li>matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Negative implements UnaryOperation
{
    public static final String NAME = "negative";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Negative(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.NEGATION, new Negative(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Fraction)
        {
            final var f = (Fraction)o;
            return Value.constantOf(f.negated());
            
        }
        else if (o instanceof Complex)
        {
            final var c = (Complex)o;
            return Value.constantOf(c.negated());
            
        }
        else if (o instanceof Number)
        {
            final var n = (Number)o;
            return Value.constantOf(typeContext.numberType()
                    .valueOf(-n.doubleValue()));
            
        }
        else if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            return Value.constantOf(mx.negated());
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }

}
