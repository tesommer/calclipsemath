package com.calclipse.math.parser.config;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.stream.Collectors.toMap;

import java.util.Map;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.ParseMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.message.TypeName;
import com.calclipse.math.expression.message.TypeNamer;
import com.calclipse.math.matrix.DenseMatrix;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.config.internal.ArrayListArray;
import com.calclipse.math.parser.config.internal.DefaultIndexer;
import com.calclipse.math.parser.config.internal.DefaultSizer;
import com.calclipse.math.parser.config.internal.EntryStringification;
import com.calclipse.math.parser.config.internal.HashMapAssociativeArray;
import com.calclipse.math.parser.config.internal.StandardAssociation;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.ArrayType;
import com.calclipse.math.parser.type.Association;
import com.calclipse.math.parser.type.AssociativeArray;
import com.calclipse.math.parser.type.AssociativeArrayType;
import com.calclipse.math.parser.type.BooleanType;
import com.calclipse.math.parser.type.Comparer;
import com.calclipse.math.parser.type.Indexer;
import com.calclipse.math.parser.type.MatrixType;
import com.calclipse.math.parser.type.NumberType;
import com.calclipse.math.parser.type.Sizer;
import com.calclipse.math.parser.type.TypeContext;

/**
 * This is the type context of the
 * {@link com.calclipse.math.parser.config.StandardConfig}.
 * 
 * The number type returned by this class
 * mainly uses {@code java.lang.Double}
 * to represent the boolean and real data-types.
 * {@link com.calclipse.math.parser.type.NumberType#valueOf(long)}
 * returns {@code java.lang.Long}.
 * {@link com.calclipse.math.parser.type.NumberType#valueOf(Number)}
 * returns the number as is.
 * 
 * The matrix type returned by this class
 * makes {@link com.calclipse.math.matrix.DenseMatrix} instances.
 * 
 * The comparer returned by this class
 * supports comparing numbers of different classes,
 * such that for instance
 * {@code new Integer(2)} equals {@code new Double(2.0)}.
 * 
 * This class registers names for the
 * {@link com.calclipse.math.parser.type.Array},
 * {@link com.calclipse.math.parser.type.AssociativeArray} and
 * {@link com.calclipse.math.parser.type.Association} types with the
 * {@link com.calclipse.math.expression.message.TypeNamer}
 * within a class initializer.
 * 
 * The associations created by this type context
 * do not accept {@code null} as the key or value.
 * The associative-array type ignores associations containing {@code undef}
 * as the key or value when creating associative arrays.
 * 
 * @author Tone Sommerland
 */
public class StandardTypeContext implements TypeContext
{
    static final TypeContext INSTANCE = new StandardTypeContext();
    
    private static final MatrixType MATRIX_TYPE = StandardMatrixType.INSTANCE;
    private static final ArrayType ARRAY_TYPE = ArrayListArray::new;
    private static final Indexer INDEXER = new DefaultIndexer(INSTANCE);
    private static final Sizer SIZER = new DefaultSizer(INSTANCE);
    
    static
    {
        TypeNamer.registerOnPrimary(TypeName.of(
                Array.class, "array"));
        TypeNamer.registerOnPrimary(TypeName.of(
                Association.class, "association"));
        TypeNamer.registerOnPrimary(TypeName.of(
                AssociativeArray.class, "associative-array"));
    }

    protected StandardTypeContext()
    {
    }
    
    @Override
    public NumberType numberType()
    {
        return StandardNumberType.INSTANCE;
    }

    @Override
    public BooleanType booleanType()
    {
        return StandardBooleanType.INSTANCE;
    }

    @Override
    public MatrixType matrixType()
    {
        return MATRIX_TYPE;
    }

    @Override
    public ArrayType arrayType()
    {
        return ARRAY_TYPE;
    }

    @Override
    public AssociativeArrayType associativeArrayType()
    {
        return StandardAssociativeArrayType.INSTANCE;
    }

    @Override
    public Comparer comparer()
    {
        return StandardComparer.INSTANCE;
    }

    @Override
    public Indexer indexer()
    {
        return INDEXER;
    }

    @Override
    public Sizer sizer()
    {
        return SIZER;
    }
    
    static class StandardNumberType implements NumberType
    {
        private static final NumberType INSTANCE = new StandardNumberType();

        StandardNumberType()
        {
        }

        @Override
        public Number parse(final String literal) throws ErrorMessage
        {
            try
            {
                return Double.valueOf(literal);
            }
            catch (final NumberFormatException ex)
            {
                throw errorMessage(ParseMessages.invalidNumber(literal))
                    .withCause(ex);
            }
        }

        @Override
        public Number valueOf(final double d)
        {
            return Double.valueOf(d);
        }

        @Override
        public Number valueOf(final long l)
        {
            return Long.valueOf(l);
        }

        @Override
        public Number valueOf(final Number n)
        {
            return n;
        }
        
    }
    
    private static enum StandardBooleanType implements BooleanType
    {
        INSTANCE;

        @Override
        public Object valueOf(final boolean b)
        {
            return Double.valueOf(b ? 1 : 0);
        }

        @Override
        public boolean isTrue(final Object boolValue) throws ErrorMessage
        {
            if (boolValue instanceof Number)
            {
                final var numb = (Number)boolValue;
                return numb.doubleValue() != 0;
            }
            throw errorMessage(TypeMessages.invalidType(boolValue));
        }
        
    }
    
    private static enum StandardMatrixType implements MatrixType
    {
        INSTANCE;

        @Override
        public Matrix createMatrix(final int rows, final int columns) {
            final Matrix matrix = DenseMatrix.valueOf(rows, columns);
            matrix.setStringifier(matrix.stringifier().withEntryStringifier(
                    EntryStringification::stringify));
            return matrix;
        }
    }
    
    private static enum StandardAssociativeArrayType
        implements AssociativeArrayType
    {
        INSTANCE;

        @Override
        public AssociativeArray createAssociativeArray(
                final Map<?, ?> associations)
        {
            return new HashMapAssociativeArray(associations.entrySet().stream()
                    .filter(StandardAssociativeArrayType::doesNotContainUndef)
                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue)));
        }

        @Override
        public Association associate(final Object key, final Object value)
        {
            return new StandardAssociation(key, value);
        }
        
        private static boolean doesNotContainUndef(
                final Map.Entry<?, ?> association)
        {
            return !(
                    Values.UNDEF.get().equals(association.getKey())
                    ||
                    Values.UNDEF.get().equals(association.getValue())
                    );
        }
        
    }
    
    private static enum StandardComparer implements Comparer
    {
        INSTANCE;

        @Override
        @SuppressWarnings({"unchecked"})
        public int compare(final Object value1, final Object value2)
                throws ErrorMessage
        {
            if (areNumbersOfDifferentClasses(value1, value2))
            {
                return compareNumbers((Number)value1, (Number)value2);

            }
            else if (value1 instanceof Comparable)
            {
                final var comparable1 = (Comparable<Object>)value1;
                try
                {
                    return comparable1.compareTo(value2);
                }
                catch (final ClassCastException ex)
                {
                    throw errorMessage(
                            TypeMessages.incompatibleTypes(value1, value2))
                    .withCause(ex);
                }
            }

            throw errorMessage(
                    TypeMessages.incompatibleTypes(value1, value2));
        }

        @Override
        public boolean areEqual(final Object value1, final Object value2)
        {
            if (areNumbersOfDifferentClasses(value1, value2))
            {
                return compareNumbers((Number)value1, (Number)value2) == 0;
            }
            return value1.equals(value2);
        }

        private static boolean areNumbersOfDifferentClasses(
                final Object value1, final Object value2)
        {
            return value1 instanceof Number && value2 instanceof Number
                    && value1.getClass() != value2.getClass();
        }

        private static int compareNumbers(final Number n1, final Number n2)
        {
            return Double.compare(n1.doubleValue(), n2.doubleValue());
        }

    }

}
