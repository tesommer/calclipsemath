package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Number of columns in a matrix.
 * The returned value is assignable unless the input is a constant.
 * Supported types:
 * <ul>
 *  <li>matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Columns implements UnaryOperation
{
    public static final String NAME = "cols";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Columns(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Columns(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();
        
        if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            if (arg.isConstant())
            {
                return Value.constantOf(typeContext.numberType()
                        .valueOf(mx.columns()));
            }
            else
            {
                return new ColsValue(typeContext, mx);
            }
        }
        
        throw errorMessage(TypeMessages.invalidType(o));
    }
    
    private static final class ColsValue implements Value
    {
        private final TypeContext typeContext;
        private final Matrix matrix;
        
        private ColsValue(final TypeContext typeContext, final Matrix matrix)
        {
            assert typeContext != null;
            assert matrix != null;
            this.typeContext = typeContext;
            this.matrix = matrix;
        }

        @Override
        public Object get()
        {
            return typeContext.numberType().valueOf(matrix.columns());
        }

        @Override
        public void set(final Object value) throws ErrorMessage
        {
            final int newCols = TypeUtil.toInt(value);
            if (newCols < 1)
            {
                throw errorMessage(EvalMessages.invalidSize());
            }
            matrix.setSize(matrix.rows(), newCols, true);
        }

        @Override
        public boolean isConstant()
        {
            return false;
        }
        
    }

}
