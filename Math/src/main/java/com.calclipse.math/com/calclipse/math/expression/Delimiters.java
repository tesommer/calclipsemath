package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.util.stream.Stream;

/**
 * Parameter delimiters.
 * This class only contains information,
 * and is not used for parameter processing.
 * An instance contains zero or one opener,
 * at least one separator,
 * and one closer.
 * 
 * @see com.calclipse.math.expression.Function#delimiters()
 * 
 * @author Tone Sommerland
 */
public final class Delimiters
{
    private final String opener;
    private final String closer;
    private final String[] separators;
    
    private Delimiters(
            final String opener,
            final String closer,
            final String firstSeparator,
            final String[] remainingSeparators)
    {
        assert opener != null;
        this.opener = opener;
        this.closer = requireNonEmpty(closer, "Empty closer");
        requireNonNull(firstSeparator, "firstSeparator");
        requireNonNull(remainingSeparators, "remainingSeparators");
        this.separators = Stream.concat(
                Stream.of(firstSeparator),
                Stream.of(remainingSeparators))
                    .map(separator ->
                        requireNonEmpty(separator, "Empty separator"))
                    .toArray(String[]::new);
    }
    
    /**
     * Returns an instance
     * containing the given opener, closer and separators.
     * @param opener the parameter list opener
     * @param closer the parameter list closer
     * @param firstSeparator the first parameter separator
     * @param remainingSeparators any remaining parameter separators
     * @throws IllegalArgumentException if given an empty delimiter
     */
    public static Delimiters withOpener(
            final String opener,
            final String closer,
            final String firstSeparator,
            final String... remainingSeparators)
    {
        return new Delimiters(
                requireNonEmpty(opener, "Empty opener"),
                closer,
                firstSeparator,
                remainingSeparators);
    }
    
    /**
     * Returns an opener-less
     * instance containing the given closer and separators.
     * @param closer the parameter list closer
     * @param firstSeparator the first parameter separator
     * @param remainingSeparators any remaining parameter separators
     * @throws IllegalArgumentException if given an empty delimiter
     */
    public static Delimiters withoutOpener(
            final String closer,
            final String firstSeparator,
            final String... remainingSeparators)
    {
        return new Delimiters(
                "", closer, firstSeparator, remainingSeparators);
    }
    
    private static String requireNonEmpty(
            final String str, final String message)
    {
        if (str.isEmpty())
        {
            throw new IllegalArgumentException(message);
        }
        return str;
    }

    /**
     * The parameter list opener.
     * @return the empty string if this instance does not have an opener
     */
    public String opener()
    {
        return opener;
    }

    /**
     * The parameter list closer.
     */
    public String closer()
    {
        return closer;
    }

    /**
     * The parameter separators.
     * The returned array has at least one element.
     */
    public String[] separators()
    {
        return separators.clone();
    }

    @Override
    public String toString()
    {
        return Delimiters.class.getSimpleName()
                + ": opener="
                + opener == null ? "" : opener
                + ", closer="
                + closer
                + ", separator(s):"
                + Stream.of(separators).collect(joining(" "));
    }

}
