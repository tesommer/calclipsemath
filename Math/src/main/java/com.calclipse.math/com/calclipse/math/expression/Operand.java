package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.operation.Value;

/**
 * An operand in an arithmetic expression.
 * This class delegates
 * {@link #get()},
 * {@link #set(Object)} and
 * {@link #isConstant()} to its internal
 * {@link #value() value}.
 * 
 * @author Tone Sommerland
 */
public final class Operand extends Token implements Value
{
    private final Value value;

    private Operand(final Id id, final String name, final Value value)
    {
        super(TokenType.OPERAND, id, name);
        this.value = requireNonNull(value, "value");
    }
    
    /**
     * Returns an operand with the given name and value.
     * @param name the operand name
     * @param value the operand value
     * @throws IllegalArgumentException if the name is empty
     */
    public static Operand of(final String name, final Value value)
    {
        return new Operand(Id.unique(), name, value);
    }
    
    /**
     * The value of this operand.
     */
    public Value value()
    {
        return value;
    }
    
    @Override
    public Operand withId(final Id newId)
    {
        return new Operand(newId, name, value);
    }
    
    @Override
    public Operand withName(final String newName)
    {
        return new Operand(id, newName, value);
    }
    
    /**
     * Returns an operand similar to this, but with the given value.
     * @param newValue the new value
     */
    public Operand withValue(final Value newValue)
    {
        return new Operand(id, name, newValue);
    }

    @Override
    public Object get()
    {
        return value.get();
    }

    @Override
    public void set(final Object val) throws ErrorMessage
    {
        value.set(val);
    }

    @Override
    public boolean isConstant()
    {
        return value.isConstant();
    }
    
}
