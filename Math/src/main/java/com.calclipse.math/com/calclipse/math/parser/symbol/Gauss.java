package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MatrixUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Reduces a matrix to echelon form.
 * Accepts the matrix to reduce,
 * a boolean flag specifying whether to use partial pivoting,
 * and an augmentation matrix.
 * The two last arguments are optional.
 * Results in an array containing
 * the echelon matrix,
 * and the augmentation after row operations (if supplied).
 * 
 * @author Tone Sommerland
 */
public final class Gauss implements VariableArityOperation
{
    public static final String NAME = "gauss";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Gauss(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(NAME, delimitation, new Gauss(typeContext))
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final var params = new ReductionParameters(typeContext, args);
        final Matrix coeffCopy = params.coefficients().copy();
        final Matrix aug = params.augmentation();
        final Matrix augCopy = aug == null ? null : aug.copy();
        if (augCopy == null)
        {
            MatrixUtil.gauss(coeffCopy, params.isPivoting());
        }
        else
        {
            try
            {
                MatrixUtil.gauss(coeffCopy, augCopy, params.isPivoting());
            }
            catch (final SizeViolation ex)
            {
                throw errorMessage(EvalMessages.mismatchingDimensions())
                    .withCause(ex);
            }
        }
        final var arr = typeContext.arrayType().arrayOf(coeffCopy);
        if (augCopy != null)
        {
            arr.add(augCopy);
        }
        return Value.constantOf(arr);
    }

    /**
     * Retrieves user-supplied parameters for matrix reduction.
     * <ol>
     * <li>the coefficients</li>
     * <li>partial pivoting (optional)</li>
     * <li>augmentation (optional)</li>
     * </ol>
     * 
     * @author Tone Sommerland
     */
    static class ReductionParameters
    {
        private final Matrix coefficients;
        private final Matrix augmentation;
        private final boolean pivoting;
        
        ReductionParameters(
                final TypeContext typeContext,
                final Value[] args) throws ErrorMessage
        {
            ArgUtil.requireMinMaxArgCount(1, 3, args.length);
            this.coefficients = coefficients(args);
            this.augmentation = augmentation(args);
            this.pivoting = isPivoting(typeContext, args);
        }
        
        private static Matrix coefficients(final Value[] args)
                throws ErrorMessage
        {
            final Object o1 = args[0].get();
            if (o1 instanceof Matrix)
            {
                return (Matrix)o1;
            }
            throw ArgUtil.forArgument(1, TypeMessages.invalidType(o1));
        }

        private static boolean isPivoting(
                final TypeContext typeContext,
                final Value[] args) throws ErrorMessage
        {
            if (args.length >= 2)
            {
                return TypeUtil.argToBoolean(2, args[1].get(), typeContext);
            }
            return false;
        }

        private static Matrix augmentation(final Value[] args)
                throws ErrorMessage
        {
            if (args.length == 3)
            {
                final Object o3 = args[2].get();
                if (o3 instanceof Matrix)
                {
                    return (Matrix)o3;
                }
                throw ArgUtil.forArgument(3, TypeMessages.invalidType(o3));
            }
            return null;
        }

        Matrix coefficients()
        {
            return coefficients;
        }

        /**
         * May return {@code null}.
         */
        Matrix augmentation()
        {
            return augmentation;
        }

        boolean isPivoting()
        {
            return pivoting;
        }
        
    }

}
