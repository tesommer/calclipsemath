package com.calclipse.math.expression;

/**
 * A step in the parsing process.
 * Each step may be preceded or succeeded by other steps.
 * 
 * @author Tone Sommerland
 */
public interface ParseStep
{
    /**
     * Called to deliver the next token to be parsed.
     */
    public abstract void read(TokenStream stream) throws ErrorMessage;
    
    /**
     * Called to signal the end of the current expression.
     */
    public abstract void end(TokenStream stream) throws ErrorMessage;
    
}
