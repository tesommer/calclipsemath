package com.calclipse.math.matrix.function;

/**
 * A consumer that accepts a row index and a column index.
 * 
 * @see com.calclipse.math.matrix.Matrix
 * #acceptRowColumnConsumer(RowColumnConsumer)
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface RowColumnConsumer
{
    /**
     * Performs this operation.
     * @param row the row index
     * @param column the column index
     */
    public abstract void accept(int row, int column);

}
