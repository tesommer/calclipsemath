package com.calclipse.math.expression.message;

import static java.util.Objects.requireNonNull;

import java.util.Arrays;
import java.util.stream.Stream;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.matrix.Matrix;

/**
 * Maps operand values into type names.
 * This class is used by type messages
 * as an extra formatting step on values
 * before they are used as format elements.
 * 
 * This class is thread safe.
 * 
 * @author Tone Sommerland
 */
public final class TypeNamer
{
    private static volatile TypeNamer
    primary = TypeNamer.of(
            "[unknown type]",
            TypeName.of(Number.class,   "real"),
            TypeName.of(Fraction.class, "fraction"),
            TypeName.of(Complex.class,  "complex"),
            TypeName.of(Matrix.class,   "matrix"),
            TypeName.of(String.class,   "string"));
    
    private static volatile TypeNamer secondary;

    private final TypeNamer fallbackNamer;
    private final String fallbackName;
    private final TypeName[] names;
    
    private TypeNamer(
            final TypeNamer fallbackNamer,
            final String fallbackName,
            final TypeName[] names)
    {
        if (fallbackNamer == null)
        {
            this.fallbackNamer = null;
            this.fallbackName = requireNonNull(fallbackName, "fallbackName");
        }
        else
        {
            this.fallbackNamer = fallbackNamer;
            this.fallbackName = null;
        }
        this.names = names.clone();
    }
    
    /**
     * Returns an instance of this class.
     * @param fallbackName the name of unrecognized data types
     * @param names type names in order of lowest to highest precedence
     */
    public static TypeNamer of(
            final String fallbackName, final TypeName... names)
    {
        return new TypeNamer(null, fallbackName, names);
    }
    
    /**
     * Registers the given names on the primary namer.
     * The more recent a name is registered, the higher its precedence.
     * @param names type names in order of lowest to highest precedence
     */
    public static void registerOnPrimary(final TypeName... names)
    {
        synchronized (TypeNamer.class)
        {
            primary = primary.plus(names);
        }
    }
    
    /**
     * Sets the secondary namer that will override the primary.
     */
    public static void setSecondary(final TypeNamer namer)
    {
        synchronized (TypeNamer.class)
        {
            secondary = requireNonNull(namer, "namer");
        }
    }
    
    /**
     * Sets the secondary namer that will override the primary.
     * Uses the old secondary as fallback if set, otherwise the primary.
     * @param names type names in order of lowest to highest precedence
     */
    public static void setSecondary(final TypeName... names)
    {
        synchronized (TypeNamer.class)
        {
            final TypeNamer fallback = secondary == null ? primary : secondary;
            secondary = new TypeNamer(fallback, null, names);
        }
    }
    
    /**
     * Resets the secondary namer, thereby yielding control to the primary.
     */
    public static void unsetSecondary()
    {
        synchronized (TypeNamer.class)
        {
            secondary = null;
        }
    }
    
    static String nameTypeOf(final Object value)
    {
        synchronized (TypeNamer.class)
        {
            if (secondary == null)
            {
                return primary.name(value);
            }
            return secondary.name(value);
        }
    }
    
    /**
     * Returns this namer supplemented with the given names.
     * @param moreNames type names in order of lowest to highest precedence
     */
    public TypeNamer plus(final TypeName... moreNames)
    {
        return new TypeNamer(
                fallbackNamer,
                fallbackName,
                Stream.concat(Stream.of(names), Stream.of(moreNames))
                    .toArray(TypeName[]::new));
    }
    
    private String name(final Object value)
    {
        for (int i = names.length - 1; i >= 0; i--)
        {
            if (names[i].isTypeOf(value))
            {
                return names[i].name();
            }
        }
        return fallbackNamer == null
                ? fallbackName : fallbackNamer.name(value);
    }

    @Override
    public String toString()
    {
        return TypeNamer.class.getSimpleName()
                + '{'  + fallbackNamer
                + ": " + fallbackName
                + ": " + Arrays.toString(names) + '}';
    }

}
