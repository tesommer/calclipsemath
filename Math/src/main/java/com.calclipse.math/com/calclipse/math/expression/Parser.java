package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

/**
 * An expression parser.
 * Parsing an expression involves two main procedures:
 * 1) A scanner scans the input string and divides it into tokens.
 * 2) The tokens are handed to a sequence of parse steps,
 * which parse the tokens to build the compiled expression.
 * The evaluator given to this parser
 * should be able to evaluate the parsed expression,
 * whether it be on postfix form, prefix form or something else. 
 * 
 * @author Tone Sommerland
 */
public final class Parser
{
    private final Scanner scanner;
    private final ParseStep[] steps;
    private final Evaluator evaluator;
    
    private Parser(
            final Scanner scanner,
            final ParseStep[] steps,
            final Evaluator evaluator)
    {
        this.scanner = requireNonNull(scanner, "scanner");
        this.steps = steps.clone();
        this.evaluator = requireNonNull(evaluator, "evaluator");
    }
    
    /**
     * Returns an expression parser.
     * @param scanner the lexical analyzer
     * @param steps the parse steps
     * @param evaluator an evaluator for the parsed expression
     */
    public static Parser of(
            final Scanner scanner,
            final ParseStep[] steps,
            final Evaluator evaluator)
    {
        return new Parser(scanner, steps, evaluator);
    }
    
    /**
     * Parses an expression.
     * @throws ErrorMessage if there is an error in the expression
     */
    public Expression parse(final String expression) throws ErrorMessage
    {
        final var stream = new TokenStream(steps, evaluator);
        scanner.reset(expression);
        while (scanner.hasNext())
        {
            stream.read(scanner.next());
        }
        stream.end();
        return stream.output();
    }
    
}
