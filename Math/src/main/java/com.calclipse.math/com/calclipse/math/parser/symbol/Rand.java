package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import java.util.Random;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Random number generator.
 * Hands out real numbers between 0 (inclusive) and 1 (exclusive).
 * May be assigned an integer as the seed.
 * 
 * @author Tone Sommerland
 */
public final class Rand implements Value
{
    public static final String NAME = "rand";
    
    private static final Id ID = Id.unique();
    
    private final Random random = new Random();
    private final TypeContext typeContext;
    
    private Rand(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Operand operand(final TypeContext typeContext)
    {
        return Operand.of(NAME, new Rand(typeContext)).withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Object get()
    {
        return typeContext.numberType().valueOf(random.nextDouble());
    }

    @Override
    public void set(final Object value) throws ErrorMessage
    {
        random.setSeed(TypeUtil.toLong(value));
    }

    @Override
    public boolean isConstant()
    {
        return false;
    }

}
