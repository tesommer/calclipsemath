package com.calclipse.math.parser.step;

import static java.util.stream.Collectors.toList;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Evaluator;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;

/**
 * This post-postfix step translates selected operators to functions.
 * If the operator is binary, the function will get two arguments.
 * If the operator is unary, the function will get one.
 * One intended use of this step is to make operators into control flows.
 * The only requirement of the replacement function
 * is that it contains the calculation logic.
 * The operator, which occurs in expressions, may be a dummy.
 * 
 * @author Tone Sommerland
 */
public final class OperatorToFunction implements ParseStep
{
    /**
     * Replaces an operator with a function.
     * 
     * @author Tone Sommerland
     */
    public interface Replacer
    {
        /**
         * Whether or not the given operator is to be replaced.
         */
        public abstract boolean isTarget(Operator operator);
        
        /**
         * Returns the function that replaces the given operator.
         */
        public abstract Function replace(Operator operator);
    }
    
    private final Replacer[] replacers;

    /**
     * Creates a step that replaces operators with functions.
     * @param replacers replaces a given set of operators
     * with the desired functions
     */
    public OperatorToFunction(final Replacer... replacers)
    {
        this.replacers = replacers.clone();
    }

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        stream.buffer().add(stream.input());
        stream.skip();
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
        final Evaluator evaluator = stream.evaluator();
        final var expression = new Expression.Builder(evaluator);
        stream.buffer().stream().forEach(expression::add);
        for (final Fragment frag
                : Transition.make(expression, replacers, evaluator))
        {
            stream.write(frag);
        }
    }
    
    private static final class Transition
    {
        private final List<Expression.Builder> args = new ArrayList<>(2);
        private final Deque<Integer> stack = new ArrayDeque<>();
        private final Expression.Builder expression;
        private final Replacer[] replacers;
        private final Evaluator evaluator;
        private int index;
        private int argIndex = -1;
        private Fragment replacement;

        private Transition(
                final Expression.Builder expression,
                final Replacer[] replacers,
                final Evaluator evaluator)
        {
            assert expression != null;
            assert replacers != null;
            assert evaluator != null;
            this.expression = expression;
            this.replacers = replacers;
            this.evaluator = evaluator;
            this.index = expression.size() - 1;
            while(step());
        }
        
        private static Expression make(
                final Expression.Builder expression,
                final Replacer[] replacers,
                final Evaluator evaluator)
        {
            new Transition(expression, replacers, evaluator);
            return expression.build();
        }
        
        private boolean step()
        {
            if (index < 0)
            {
                return false;
            }
            if (argIndex < 0)
            {
                stepNonArg();
            }
            else
            {
                stepArg();
            }
            index--;
            return true;
        }

        /**
         * Steps one fragment that's not a replacement argument.
         */
        private void stepNonArg()
        {
            final Fragment frag = expression.get(index);
            final int arity = assignReplacementIfTarget(frag);
            if (arity > 0)
            {
                for (int i = 0; i < arity; i++)
                {
                    args.add(new Expression.Builder(evaluator));
                }
                argIndex = arity - 1;
            }
        }
        
        /**
         * If the given fragment is a target,
         * the arity of the operator is returned.
         * Otherwise returns -1.
         */
        private int assignReplacementIfTarget(final Fragment frag)
        {
            if (!frag.token().type().isOperator())
            {
                return -1;
            }
            final var operator = (Operator)frag.token();
            for (final Replacer replacer : replacers)
            {
                if (replacer.isTarget(operator))
                {
                    final String targetName = frag.token().name();
                    replacement = Fragment.of(
                            replacer.replace(operator).withName(targetName),
                            frag.position());
                    return operator.type().isBinary() ? 2 : 1;
                }
            }
            return -1;
        }

        /**
         * Steps one replacement argument.
         */
        private void stepArg()
        {
            for (
                    Fragment frag = expression.removeAndGet(index--);
                    addToArg(args.get(argIndex), frag);
                    frag = expression.removeAndGet(index--));
            index++;
            stack.clear();
            if (--argIndex < 0)
            {
                finishReplacement();
                args.clear();
            }
        }
        
        /**
         * Returns false if the argument is complete
         * after adding the given fragment.
         */
        private boolean addToArg(
                final Expression.Builder arg, final Fragment frag)
        {
            arg.add(0, frag);
            if (frag.token().type().isBinary())
            {
                stack.push(2);
                return true;
            }
            else if (frag.token().type().isUnary())
            {
                stack.push(1);
                return true;
            }
            else
            {
                return nonOperatorAddedToArg();
            }
        }

        /**
         * Returns false if adding a non-operator to the argument
         * completed the argument.
         */
        private boolean nonOperatorAddedToArg()
        {
            if (stack.isEmpty())
            {
                return false;
            }
            int count = stack.pop() - 1;
            while (!stack.isEmpty() && count <= 0)
            {
                count = stack.pop() - 1;
            }
            if (count > 0)
            {
                stack.push(count);
                return true;
            }
            return false;
        }
        
        private void finishReplacement()
        {
            final Token func = ((Function)replacement.token())
                    .withArguments(args.stream()
                            .map(arg -> make(arg, replacers, evaluator))
                            .collect(toList()));
            expression.set(index, Fragment.of(func, replacement.position()));
        }
    }

}
