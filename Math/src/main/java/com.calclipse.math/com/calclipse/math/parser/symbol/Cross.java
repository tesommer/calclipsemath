package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.ArgUtil;

/**
 * The cross product of two vectors.
 * Supported types:
 * <ul>
 *  <li>matrix, matrix</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Cross implements BinaryOperation
{
    public static final String NAME = "cross";
    
    private static final Id ID = Id.unique();
    
    private static final BinaryOperation INSTANCE = new Cross();

    private Cross()
    {
    }
    
    public static BinaryOperator operator()
    {
        return BinaryOperator.of(NAME, OperatorPriorities.PRODUCT, INSTANCE)
                .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        
        if ((o1 instanceof Matrix) && (o2 instanceof Matrix))
        {
            final var vec1 = (Matrix)o1;
            final var vec2 = (Matrix)o2;
            if (vec1.rows() != 3 || vec1.columns() != 1)
            {
                throw ArgUtil.forArgument(1, EvalMessages.invalidSize());
            }
            if (vec2.rows() != 3 || vec2.columns() != 1)
            {
                throw ArgUtil.forArgument(2, EvalMessages.invalidSize());
            }
            return Value.constantOf(vec1.cross(vec2));
        }
        
        throw errorMessage(TypeMessages.incompatibleTypes(o1, o2));
    }

}
