package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Returns an identity matrix.
 * Supported types:
 * <ul>
 *  <li>integer</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Identity implements UnaryOperation
{
    public static final String NAME = "identity";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private Identity(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Identity(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final int rows = TypeUtil.toInt(arg.get());
        if (rows < 1)
        {
            throw errorMessage(EvalMessages.invalidSize());
        }
        return Value.constantOf(typeContext.matrixType()
                .createMatrix(0, 0).identity(rows));
    }

}
