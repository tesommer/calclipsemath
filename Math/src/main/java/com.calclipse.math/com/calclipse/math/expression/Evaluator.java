package com.calclipse.math.expression;

import com.calclipse.math.expression.internal.PostfixEvaluation;
import com.calclipse.math.expression.operation.Value;

/**
 * Evaluates parsed expressions.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface Evaluator
{
    /**
     * Returns an evaluator for postfix expressions.
     */
    public static Evaluator forPostfix()
    {
        return PostfixEvaluation::evaluate;
    }

    /**
     * Evaluates (executes) an expression.
     * This method should not return {@code null}.
     * @param interceptReturn whether or not to catch a
     * {@link com.calclipse.math.expression.Return}
     * or let it pass
     * @throws ErrorMessage if an evaluation error occurred
     * @throws com.calclipse.math.expression.BadExpression
     * if the expression is invalid or not properly parsed
     */
    public abstract Value evaluate(
            Expression expression,
            boolean interceptReturn) throws ErrorMessage;

}
