package com.calclipse.math.parser.config;

import static com.calclipse.math.expression.Errors.errorMessage;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Optional;

import com.calclipse.math.Fraction;
import com.calclipse.math.Real;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.MathParser;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.symbol.Abs;
import com.calclipse.math.parser.symbol.Big;
import com.calclipse.math.parser.symbol.DividedBy;
import com.calclipse.math.parser.symbol.Frac;
import com.calclipse.math.parser.symbol.FractionPart;
import com.calclipse.math.parser.symbol.IntegerPart;
import com.calclipse.math.parser.symbol.Minus;
import com.calclipse.math.parser.symbol.Modulo;
import com.calclipse.math.parser.symbol.Negative;
import com.calclipse.math.parser.symbol.Plus;
import com.calclipse.math.parser.symbol.Round;
import com.calclipse.math.parser.symbol.Small;
import com.calclipse.math.parser.symbol.Times;
import com.calclipse.math.parser.symbol.ToThePowerOf;
import com.calclipse.math.parser.type.TypeContext;

/**
 * {@link com.calclipse.math.parser.config.StandardConfig Standard config}
 * with added support for
 * {@link com.calclipse.math.Real arbitrary-precision real numbers}.
 * Overrides {@code +}, {@code -}, {@code *}, {@code /} and {@code mod}
 * for either both arguments {@code Real},
 * or one {@code Real} and the other translatable to {@code Real}.
 * Overrides negation, {@code abs}, {@code ipart} and {@code fpart}
 * for {@code Real}.
 * Overrides {@code ^} for {@code Real} base
 * and {@code int}-convertible exponent.
 * Overrides {@code frac} for one fractured {@code Real} argument,
 * and for two {@code Real} arguments without fraction parts
 * (numerator and denominator).
 * Parsing number literals produces {@code Real} instances.
 * Adds {@link com.calclipse.math.parser.symbol.Big}
 * and {@link com.calclipse.math.parser.symbol.Small} to the symbol table.
 * 
 * @author Tone Sommerland
 */
public class ArbitraryPrecisionConfig extends StandardConfig
{
    protected ArbitraryPrecisionConfig()
    {
    }
    
    static MathParser parser()
    {
        return new MathParser(new ArbitraryPrecisionConfig());
    }

    @Override
    protected TypeContext typeContext()
    {
        return Configs.arbitraryPrecisionTypeContext();
    }
    
    @Override
    protected void populateSymbolTable(final SymbolTable symbolTable)
    {
        super.populateSymbolTable(symbolTable);
        overridePlus(symbolTable);
        overrideMinus(symbolTable);
        overrideTimes(symbolTable);
        overrideDividedBy(symbolTable);
        overrideMod(symbolTable);
        overrideToThePowerOf(symbolTable);
        overrideNegative(symbolTable);
        overrideAbs(symbolTable);
        overrideIpart(symbolTable);
        overrideFpart(symbolTable);
        overrideFrac(symbolTable);
        overrideRound(symbolTable);
        symbolTable.add(Big.function(
                ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Small.function(
                ParensAndComma.DELIMITATION));
    }
    
    private static BinaryOperator overrideBinary(
            final SymbolTable symbolTable,
            final String name,
            final BinaryOperation override)
    {
        final var symbol = (BinaryOperator)symbolTable.get(name);
        final BinaryOperator overridden = symbol.withOperation(
                override.override(symbol.operation()));
        symbolTable.remove(name);
        symbolTable.add(overridden);
        return overridden;
    }
    
    private static UnaryOperator overrideUnary(
            final SymbolTable symbolTable,
            final String name,
            final UnaryOperation override)
    {
        final var symbol = (UnaryOperator)symbolTable.get(name);
        final UnaryOperator overridden = symbol.withOperation(
                override.override(symbol.operation()));
        symbolTable.remove(name);
        symbolTable.add(overridden);
        return overridden;
    }
    
    private static Function overrideVariableArity(
            final SymbolTable symbolTable,
            final String name,
            final VariableArityOperation override)
    {
        final var symbol = (Function)symbolTable.get(name);
        final Function overridden = symbol.withOperation(
                override.override(symbol.operation()));
        symbolTable.remove(name);
        symbolTable.add(overridden);
        return overridden;
    }

    private void overridePlus(final SymbolTable symbolTable)
    {
        overrideBinary(symbolTable, Plus.NAME, binaryOverride(
                (left, right) -> left.plus(right)));
    }

    private void overrideMinus(final SymbolTable symbolTable)
    {
        overrideBinary(symbolTable, Minus.NAME, binaryOverride(
                (left, right) -> left.minus(right)));
    }

    private void overrideTimes(final SymbolTable symbolTable)
    {
        overrideBinary(symbolTable, Times.NAME, binaryOverride(
                (left, right) -> left.times(right)));
    }

    private void overrideDividedBy(final SymbolTable symbolTable)
    {
        overrideBinary(symbolTable, DividedBy.NAME, binaryOverride(
                (left, right) -> left.dividedBy(right)));
    }

    private void overrideMod(final SymbolTable symbolTable)
    {
        overrideBinary(symbolTable, Modulo.NAME,
                this::modOverride);
    }

    private static void overrideToThePowerOf(final SymbolTable symbolTable)
    {
        overrideBinary(symbolTable, ToThePowerOf.NAME,
                ArbitraryPrecisionConfig::toThePowerOfOverride);
    }
    
    private static void overrideNegative(final SymbolTable symbolTable)
    {
        overrideUnary(symbolTable, Negative.NAME, unaryOverride(
                arg -> arg.negated()));
    }

    private static void overrideAbs(final SymbolTable symbolTable)
    {
        overrideUnary(symbolTable, Abs.NAME, unaryOverride(
                arg -> arg.abs()));
    }

    private static void overrideIpart(final SymbolTable symbolTable)
    {
        overrideUnary(symbolTable, IntegerPart.NAME, unaryOverride(
                MathUtil::integerPart));
    }

    private static void overrideFpart(final SymbolTable symbolTable)
    {
        overrideUnary(symbolTable, FractionPart.NAME, unaryOverride(
                MathUtil::fractionPart));
    }

    private static void overrideFrac(final SymbolTable symbolTable)
    {
        overrideVariableArity(symbolTable, Frac.NAME,
                ArbitraryPrecisionConfig::fracOverride);
    }

    private static void overrideRound(final SymbolTable symbolTable)
    {
        overrideVariableArity(symbolTable, Round.NAME,
                ArbitraryPrecisionConfig::roundOverride);
    }

    /**
     * Returns an override of a binary operation on two reals.
     */
    private BinaryOperation binaryOverride(final RealBinaryOp op)
    {
        return (left, right) ->
        {
            final Object o1 = left.get();
            final Object o2 = right.get();
            if (o1 instanceof Real || o2 instanceof Real)
            {
                final Real real1 = toReal(o1);
                final Real real2 = toReal(o2);
                if (real1 == null || real2 == null)
                {
                    return Values.YIELD;
                }
                try
                {
                    return Value.constantOf(op.evaluate(real1, real2));
                }
                catch (final ArithmeticException ex)
                {
                    return Values.YIELD;
                }
            }
            return Values.YIELD;
        };
    }
    
    /**
     * Returns an override of a unary operation on a real.
     */
    private static UnaryOperation unaryOverride(final RealUnaryOp op)
    {
        return arg ->
        {
            final Object o1 = arg.get();
            if (o1 instanceof Real)
            {
                return Value.constantOf(op.evaluate((Real)o1));
            }
            return Values.YIELD;
        };
    }
    
    /**
     * Returns a representation of a value as a real, or null.
     */
    private Real toReal(final Object value)
    {
        if (value instanceof Real)
        {
            return (Real)value;
        }
        else if (value instanceof Fraction)
        {
            return toReal((Fraction)value);
        }
        else if (value instanceof BigDecimal)
        {
            return toReal((BigDecimal)value);
        }
        else if (value instanceof BigInteger)
        {
            return toReal(Fraction.valueOf((BigInteger)value, BigInteger.ONE));
        }
        else if (value instanceof Long)
        {
            final BigInteger num = BigInteger.valueOf((Long)value);
            return toReal(Fraction.valueOf(num, BigInteger.ONE));
        }
        else if (value instanceof Number)
        {
            return toReal(new BigDecimal(((Number)value).doubleValue()));
        }
        else
        {
            return null;
        }
    }
    
    private Real toReal(final Fraction fraction)
    {
        return Real.fractured(
                fraction,
                theTypeContext().numberType().mathContext(),
                theTypeContext().numberType().fracturability());
    }
    
    private Real toReal(final BigDecimal bigDecimal)
    {
        return Real.unfractured(
                bigDecimal,
                theTypeContext().numberType().mathContext(),
                theTypeContext().numberType().fracturability());
    }
    
    private Value modOverride(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        if (o1 instanceof Real || o2 instanceof Real)
        {
            final Real real1 = toReal(o1);
            if (real1 == null)
            {
                throw ArgUtil.forArgument(1, TypeMessages.invalidType(o1));
            }
            final Real real2 = toReal(o2);
            if (real2 == null)
            {
                throw ArgUtil.forArgument(2, TypeMessages.invalidType(o2));
            }
            final BigInteger dividend = MathUtil.toBigIntegerExact(real1)
                    .orElseThrow(() -> ArgUtil.forArgument(
                            1, EvalMessages.expectedInteger()));
            final BigInteger divisor = MathUtil.toBigIntegerExact(real2)
                    .orElseThrow(() -> ArgUtil.forArgument(
                            2, EvalMessages.expectedInteger()));
            if (divisor.equals(BigInteger.ZERO))
            {
                throw errorMessage(EvalMessages.divisionByZero());
            }
            final BigInteger remainder = dividend.remainder(divisor);
            return Value.constantOf(
                    toReal(Fraction.valueOf(remainder, BigInteger.ONE)));
        }
        return Values.YIELD;
    }
    
    private static Value toThePowerOfOverride(
            final Value left, final Value right) throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        if (o1 instanceof Real && TypeUtil.isInt(o2))
        {
            final var base = (Real)o1;
            final int exponent = TypeUtil.toInt(o2);
            try
            {
                return Value.constantOf(base.toThePowerOf(exponent));
            }
            catch (final ArithmeticException ex)
            {
                return Values.YIELD;
            }
        }
        return Values.YIELD;
    }
    
    private static Value fracOverride(final Value[] args)
            throws ErrorMessage
    {
        if (args.length == 1)
        {
            return overrideFracForOneArgument(args);
        }
        else if (args.length == 2)
        {
            return overrideFracForTwoArguments(args);
        }
        return Values.YIELD;
    }
    
    private static Value overrideFracForOneArgument(final Value[] args)
            throws ErrorMessage
    {
        final Object o1 = args[0].get();
        if (o1 instanceof Real)
        {
            final var real = (Real)o1;
            if (real.isFractured())
            {
                return Value.constantOf(real.fraction());
            }
        }
        return Values.YIELD;
    }
    
    private static Value overrideFracForTwoArguments(final Value[] args)
            throws ErrorMessage
    {
        final Object o1 = args[0].get();
        final Object o2 = args[1].get();
        if (o1 instanceof Real && o2 instanceof Real)
        {
            final Optional<BigInteger> num = MathUtil.toBigIntegerExact(
                    (Real)o1);
            final Optional<BigInteger> den = MathUtil.toBigIntegerExact(
                    (Real)o2);
            if (num.isPresent() && den.isPresent())
            {
                return Value.constantOf(
                        Fraction.valueOf(num.get(), den.get()));
            }
        }
        return Values.YIELD;
    }
    
    private static Value roundOverride(final Value[] args)
            throws ErrorMessage
    {
        if (args.length == 1 || args.length == 2)
        {
            final Object o1 = args[0].get();
            if (o1 instanceof Real)
            {
                final var real = (Real)o1;
                final int decimals;
                if (args.length == 2)
                {
                    decimals = TypeUtil.argToInt(2, args[1].get());
                }
                else
                {
                    decimals = 0;
                }
                try
                {
                    return Value.constantOf(MathUtil.round(real, decimals));
                }
                catch (final ArithmeticException ex)
                {
                    return Values.YIELD;
                }
            }
        }
        return Values.YIELD;
    }
    
    /**
     * A binary operation on two reals that results in a real.
     */
    @FunctionalInterface
    private static interface RealBinaryOp
    {
        /**
         * Evaluates this operation.
         * @param left the left operand
         * @param right the right operand
         * @return the result
         * @throws ArithmeticException if the operation does.
         */
        public abstract Real evaluate(Real left, Real right);
    }
    
    /**
     * A unary operation on a real that results in a real.
     */
    @FunctionalInterface
    private static interface RealUnaryOp
    {
        /**
         * Evaluates this operation.
         * @param arg the argument
         * @return the result
         */
        public abstract Real evaluate(Real arg);
    }

}
