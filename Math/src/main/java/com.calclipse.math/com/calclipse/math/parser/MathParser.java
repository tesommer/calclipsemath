package com.calclipse.math.parser;

import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.Optional;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Parser;
import com.calclipse.math.expression.Token;

/**
 * A parser for mathematical expressions.
 * 
 * @author Tone Sommerland
 */
public final class MathParser
{
    private final Parser parser;
    private final SymbolTable symbolTable;
    
    /**
     * Creates a math parser with the given configuration.
     */
    public MathParser(final MathParserConfig config)
    {
        this.parser = Parser.of(
                config.scanner(), config.steps(), config.evaluator());
        this.symbolTable = requireNonNull(config.symbolTable());
    }
    
    /**
     * Parses an expression.
     * @throws ErrorMessage if there is an error in the expression
     */
    public Expression parse(final String expression) throws ErrorMessage
    {
        return parser.parse(expression);
    }
    
    /**
     * Adds a symbol to the symbol table.
     * @return {@code false} if the symbol could not be added,
     * for instance due to a name conflict
     */
    public boolean addSymbol(final Token symbol)
    {
        return symbolTable.add(symbol);
    }
    
    /**
     * Removes a symbol from the symbol table.
     * @param name the name of the symbol
     * @return {@code false} if the symbol could not be removed,
     * for instance because it wasn't found
     */
    public boolean removeSymbol(final String name)
    {
        return symbolTable.remove(name);
    }
    
    /**
     * Returns the symbol with the given name.
     * @return empty if not found
     */
    public Optional<Token> symbol(final String name)
    {
        return Optional.ofNullable(symbolTable.get(name));
    }
    
    /**
     * Returns all symbols stored in the symbol table.
     */
    public Collection<Token> symbols()
    {
        return symbolTable.all();
    }
    
    /**
     * Returns the symbols starting with the specified character.
     */
    public Collection<Token> lookUpSymbols(final char firstChar)
    {
        return symbolTable.lookUp(firstChar);
    }
    
    /**
     * Clears the symbol table.
     */
    public void clearSymbols()
    {
        symbolTable.clear();
    }

}
