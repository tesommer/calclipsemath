package com.calclipse.math.parser.misc;

import static com.calclipse.math.matrix.size.Constraints.matchingRows;
import static com.calclipse.math.matrix.size.Constraints.require;
import static com.calclipse.math.matrix.size.Constraints.square;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;

import com.calclipse.math.matrix.Matrix;

/**
 * Static methods for performing matrix calculations.
 * 
 * @author Tone Sommerland
 */
public final class MatrixUtil
{
    private MatrixUtil()
    {
    }

    /**
     * Performs Gaussian elimination on an augmented matrix.
     * @param coeff the coefficient matrix
     * @param aug the augmentation
     * @param pivoting whether or not to use partial pivoting
     * @return the number of row swaps
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the number of rows in the coefficient matrix
     * is unequal to the number of rows in the augmentation.
     */
    public static int gauss(
            final Matrix coeff, final Matrix aug, final boolean pivoting)
    {
        return gaussianElimination(coeff, requireNonNull(aug, "aug"), pivoting);
    }

    /**
     * Performs Gaussian elimination on a matrix.
     * @param mx the matrix to reduce
     * @param pivoting whether or not to use partial pivoting
     * @return the number of row swaps
     */
    public static int gauss(final Matrix mx, final boolean pivoting)
    {
        return gaussianElimination(mx, null, pivoting);
    }

    /**
     * Performs Gauss-Jordan elimination on an augmented matrix.
     * @param coeff the coefficient matrix
     * @param aug the augmentation
     * @param pivoting whether or not to use partial pivoting
     * @return the number of row swaps
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the number of rows in the coefficient matrix
     * is unequal to the number of rows in the augmentation
     */
    public static int gaussJordan(
            final Matrix coeff, final Matrix aug, final boolean pivoting)
    {
        return gaussJordanElimination(
                coeff, requireNonNull(aug, "aug"), pivoting);
    }

    /**
     * Performs Gauss-Jordan elimination on a matrix.
     * @param mx the matrix to reduce
     * @param pivoting whether or not to use partial pivoting
     * @return the number of row swaps
     */
    public static int gaussJordan(
            final Matrix mx, final boolean pivoting)
    {
        return gaussJordanElimination(mx, null, pivoting);
    }
    
    /**
     * Performs Gaussian elimination on a matrix
     * @param coeff coefficient matrix
     * @param aug augmentation (nullable)
     * @param pivoting whether to use partial pivoting
     * @return number of row swaps
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if aug is non-null
     * and coeff and aug have unequal number of rows
     */
    private static int gaussianElimination(
            final Matrix coeff,
            final Matrix aug,
            final boolean pivoting)
    {
        if (aug != null)
        {
            require("Augmentation rows mismatch.", matchingRows(coeff, aug));
        }

        int swaps = 0;
        for (
                int row = 0, col = 0;
                row < coeff.rows() && col < coeff.columns();)
        {
            if (ensurePivotIfPossible(coeff, aug, row, col, pivoting))
            {
                swaps++;
            }
            if (coeff.get(row, col) != 0)
            {
                eliminateBelowPivot(coeff, aug, row, col);
                row++;
            }
            col++;
        }
        
        return swaps;
    }

    private static void eliminateBelowPivot(
            final Matrix coeff,
            final Matrix aug,
            final int row,
            final int col)
    {
        for (int i = row + 1; i < coeff.rows(); i++)
        {
            if (coeff.get(i, col) != 0)
            {
                final double x = -coeff.get(i, col) / coeff.get(row, col);
                coeff.addRowMultiple(row, col, x, i);
                if (aug != null && aug.columns() > 0)
                {
                    aug.addRowMultiple(row, 0, x, i);
                }
            }
        }
    }
    
    private static boolean ensurePivotIfPossible(
            final Matrix coeff,
            final Matrix aug,
            final int row,
            final int col,
            final boolean pivoting)
    {
        if (pivoting)
        {
            return doForcedPivoting(coeff, aug, row, col);
        }
        else if (coeff.get(row, col) == 0)
        {
            return doEmergencyPivoting(coeff, aug, row, col);
        }
        return false;
    }

    private static boolean doForcedPivoting(
            final Matrix coeff,
            final Matrix aug,
            final int row,
            final int col)
    {
        int swapRow = row;
        for (int i = row + 1; i < coeff.rows(); i++)
        {
            if (Math.abs(coeff.get(i, col))
                    > Math.abs(coeff.get(swapRow, col)))
            {
                swapRow = i;
            }
        }
        if (swapRow != row)
        {
            doRowSwap(coeff, aug, row, swapRow);
            return true;
        }
        return false;
    }

    private static boolean doEmergencyPivoting(
            final Matrix coeff,
            final Matrix aug,
            final int row,
            final int col)
    {
        for (int i = row + 1; i < coeff.rows(); i++)
        {
            if (coeff.get(i, col) != 0)
            {
                doRowSwap(coeff, aug, row, i);
                return true;
            }
        }
        return false;
    }

    private static void doRowSwap(
            final Matrix coeff, final Matrix aug, final int row1, int row2)
    {
        coeff.swapRows(row2, row1);
        if (aug != null)
        {
            aug.swapRows(row2, row1);
        }
    }

    /**
     * Performs Gauss-Jordan elimination on a matrix
     * @param coeff coefficient matrix
     * @param aug augmentation (nullable)
     * @param pivoting whether to use partial pivoting
     * @return number of row swaps
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if aug is non-null
     * and coeff and aug have unequal number of rows
     */
    private static int gaussJordanElimination(
            final Matrix coeff,
            final Matrix aug,
            final boolean pivoting)
    {
        if (coeff.columns() <= 0)
        {
            return 0;
        }
        final int swaps = gaussianElimination(coeff, aug, pivoting);
        for (int row = coeff.rows() - 1; row >= 0; row--)
        {
            int col = 0;
            while (col < coeff.columns() - 1 && coeff.get(row, col) == 0)
            {
                col++;
            }
            if (coeff.get(row, col) != 0)
            {
                eliminateAbovePivot(coeff, aug, row, col);
                makeLeadingOne(coeff, aug, row, col);
            }
        }
        return swaps;
    }

    private static void eliminateAbovePivot(
            final Matrix coeff,
            final Matrix aug,
            final int row,
            final int col)
    {
        for (int i = row - 1; i >= 0; i--)
        {
            if (coeff.get(i, col) != 0)
            {
                final double x = -coeff.get(i, col) / coeff.get(row, col);
                coeff.addRowMultiple(row, col, x, i);
                if (aug != null && aug.columns() > 0)
                {
                    aug.addRowMultiple(row, 0, x, i);
                }
            }
        }
    }
    
    /**
     * Creates the leading one in the coefficient matrix.
     * @param row row pivot position
     * @param col column pivot position
     */
    private static void makeLeadingOne(
            final Matrix coeff,
            final Matrix aug,
            final int row,
            final int col)
    {
        if (coeff.get(row, col) != 1)
        {
            final double x = 1 / coeff.get(row, col);
            coeff.scaleRow(row, x);
            if (aug != null)
            {
                aug.scaleRow(row, x);
            }
        }
        
    }
    
    /**
     * Returns the projection of {@code vec}
     * onto the line spanned by the vector {@code onto}.
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * unless the given matrices are vectors of equal length
     */
    public static Matrix project(final Matrix vec, final Matrix onto)
    {
        return onto.times(vec.dot(onto) / onto.dot(onto));
    }
    
    /**
     * Performs the Gram Schmidt process on a matrix
     * (makes it orthogonal).
     */
    public static void gramSchmidt(final Matrix mx)
    {
        for (int i = 0; i < mx.columns(); i++)
        {
            final Matrix vi = mx.column(i);
            final double viDotVi = vi.dot(vi);
            if (viDotVi != 0)
            {
                vi.scale(1 / Math.sqrt(viDotVi));
            }
            mx.setColumn(i, vi);
            for (int j = i + 1; j < mx.columns(); j++)
            {
                final Matrix vj = mx.column(j);
                vj.subtractFromThis(project(vj, vi));
                mx.setColumn(j, vj);
            }
        }
    }
    
    /**
     * The determinant of a matrix.
     * @return the determinant (1 if the matrix is 0-by-0)
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the matrix not square
     */
    public static double determinant(final Matrix mx)
    {
        require("determinant: Invalid size.", square(mx));
        if (mx.rows() == 0)
        {
            return 1;
        }
        final Matrix echelon = mx.copy();
        final int swaps = gaussianElimination(echelon, null, true);
        double det = echelon.get(0, 0);
        for (int i = 1; i < echelon.rows(); i++)
        {
            det *= echelon.get(i, i);
        }
        return swaps % 2 == 0 ? mx.clean(det) : -mx.clean(det);
    }
    
    /**
     * The inverse of a matrix.
     * @return the inverse (a 0-by-0 matrix if the argument is 0-by-0)
     * @throws SingularMatrix if the matrix is singular
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the matrix is not square
     */
    public static Matrix inverse(final Matrix mx) throws SingularMatrix
    {
        require("inverse: Invalid size.", square(mx));
        final Matrix reduced = mx.copy();
        final Matrix id = mx.identity(mx.rows());
        gaussianElimination(reduced, id, true);
        for (int i = 0; i < reduced.rows(); i++)
        {
            if (reduced.get(i, i) == 0)
            {
                throw new SingularMatrix("inverse");
            }
        }
        gaussJordanElimination(reduced, id, true);
        return id;
    }
    
    /**
     * Returns the dividend divided by the divisor.
     * The division is performed by multiplying the dividend
     * with the inverse of the divisor.
     * @throws SingularMatrix if the divisor is singular
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the divisor is empty or not square,
     * or if the dividend's columns and divisor's rows mismatch
     */
    public static Matrix divide(final Matrix dividend, final Matrix divisor)
        throws SingularMatrix
    {
        return dividend.times(inverse(divisor));
    }
    
    /**
     * The Frobenius norm.
     */
    public static double frobeniusNorm(final Matrix mx)
    {
        double norm = 0;
        for (int i = 0; i < mx.columns(); i++)
        {
            for (int j = 0; j < mx.rows(); j++)
            {
                norm += mx.get(j, i) * mx.get(j, i);
            }
        }
        return Math.sqrt(mx.clean(norm));
    }
    
    /**
     * Raises a matrix to a power.
     * @throws SingularMatrix
     * if the exponent is negative and the base is singular
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the base is not square,
     * or if the exponent is negative and the base is empty
     */
    public static Matrix pow(final Matrix base, final int exponent)
        throws SingularMatrix
    {
        require("mx^exp: Invalid size.", square(base));
        
        Matrix mx = base;
        int exp = exponent;
        
        if (exp < 0)
        {
            mx = inverse(mx);
            exp = -exp;
        }
        
        Matrix result = mx.identity(mx.rows());
        boolean resultIsId = true;
        while (exp > 0)
        {
            if (exp % 2 == 0)
            {
                mx = mx.times(mx);
                exp >>= 1;
            }
            else
            {
                result = resultIsId ? mx.copy() : mx.times(result);
                resultIsId = false;
                exp--;
            }
        }
        
        return result;
    }

    /**
     * Solves a set of linear equations.
     * The result will be on parametric vector form.
     * The result will have at least as many columns as
     * the augmentation.
     * Additional columns correspond to free variables.
     * @throws InconsistentSystem if the system is inconsistent
     * @throws com.calclipse.math.matrix.size.SizeViolation
     * if the number of rows in the coefficient matrix
     * is unequal to the number of rows in the augmentation
     */
    public static Matrix solve(
            final Matrix coeff,
            final Matrix aug,
            final boolean pivoting) throws InconsistentSystem
    {
        final Matrix coeffCopy = coeff.copy();
        final Matrix augCopy = aug.copy();
        
        gaussJordanElimination(coeffCopy, augCopy, pivoting);
        
        if (isInconsistent(coeffCopy, augCopy))
        {
            throw new InconsistentSystem("solve");
        }
        
        final Matrix result = findFreeVectors(coeffCopy, augCopy);
        removeZeroVectors(result, augCopy.columns());
        return result;
    }
    
    private static boolean isInconsistent(
            final Matrix coeff, final Matrix aug)
    {
        final Matrix coeffZeroRow = coeff.create(1, coeff.columns());
        final Matrix augZeroRow = aug.create(1, aug.columns());
        for (int i = 0; i < aug.rows(); i++)
        {
            if (!aug.row(i).equals(augZeroRow)
                    && coeff.row(i).equals(coeffZeroRow))
            {
                return true;
            }
        }
        return false;
    }
    
    private static Matrix findFreeVectors(
            final Matrix coeff, final Matrix aug)
    {
        final Matrix result = coeff.create(
                coeff.columns(),
                coeff.columns() + aug.columns());
        
        for (
                int row = 0, col = 0;
                row < coeff.rows() && col < coeff.columns();)
        {
            if (isPivot(coeff, row, col))
            {
                for (int i = 0; i < aug.columns(); i++)
                {
                    result.set(col, i, aug.get(row, i));
                }
                
                for (int i = col + 1; i < coeff.columns(); i++)
                {
                    if (coeff.get(row, i) != 0)
                    {
                        result.set(col, i + aug.columns(), -coeff.get(row, i));
                        result.set(i, i + aug.columns(), 1);
                    }
                    else if (i >= coeff.rows()
                        && isEntryRightOfSquareFree(coeff, row, col))
                    {
                        result.set(i, i + aug.columns(), 1);
                    }
                }
                
                row++;
                
            }
            else
            {
                result.set(col, col + aug.columns(), 1);
            }
            
            col++;
        }
        
        return result;
    }

    /**
     * Checks if a non-pivot element in a pivot row,
     * with a column index greater than the max row index,
     * is a free variable (it may be, even if it is zero).
     */
    private static boolean isEntryRightOfSquareFree(
            final Matrix coeff, final int row, final int col)
    {
        if (row == coeff.rows() - 1)
        {
            return true;
        }
        boolean free = false;
        for (int i = row + 1; i < coeff.rows() && !free; i++)
        {
            int j = 0;
            while (j < coeff.columns() - 1 && j <= i && coeff.get(i, j) == 0)
            {
                j++;
            }
            if (j > col || coeff.get(i, j) == 0)
            {
                free = true;
            }
            else if (j == col)
            {
                break;
            }
        }
        return free;
    }
    
    private static boolean isPivot(
            final Matrix coeff, final int row, final int col)
    {
        return Math.abs(coeff.get(row, col) - 1) <= coeff.delta();
    }
    
    private static void removeZeroVectors(
            final Matrix mx, final int fromCol)
    {
        final var nonZeroVecs = new ArrayList<Matrix>();
        final Matrix zeroCol = mx.create(mx.rows(), 1);
        
        for (int i = fromCol; i < mx.columns(); i++)
        {
            final Matrix col = mx.column(i);
            if (!col.equals(zeroCol))
            {
                nonZeroVecs.add(col);
            }
        }
        
        mx.setSize(zeroCol.rows(), fromCol + nonZeroVecs.size(), true);
        for (int i = 0; i < nonZeroVecs.size(); i++)
        {
            mx.setColumn(fromCol + i, nonZeroVecs.get(i));
        }        
    }

}
