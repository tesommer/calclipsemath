package com.calclipse.math.expression;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.operation.Value;

/**
 * Thrown during evaluation of an expression to return a value.
 * This exception type has suppression disabled and not writable stack trace.
 * 
 * @author Tone Sommerland
 */
public final class Return extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    
    private final Object value;
    
    private Return(final Object value)
    {
        super("Uncaught return", null, false, false);
        this.value = value;
    }
    
    /**
     * Returns a return with the given value.
     * @param value the return value
     */
    public static Return of(final Object value)
    {
        return new Return(requireNonNull(value));
    }
    
    /**
     * Returns a return with a value of
     * {@link Values#UNDEF}.
     */
    public static Return of()
    {
        return new Return(null);
    }
    
    /**
     * The return value.
     * @return {@link Values#UNDEF} if there is no return value
     */
    public Value value()
    {
        return value == null ? Values.UNDEF : Value.constantOf(value);
    }

}
