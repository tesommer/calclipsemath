package com.calclipse.math.parser.type;

/**
 * An abstraction over basic math parser types.
 * Various math operations may depend on this interface
 * in order to create numbers, booleans and matrices etc.,
 * and for object comparison.
 * This interface is intended for avoiding
 * coupling to specific implementations of
 * {@code java.lang.Number} and similar.
 * 
 * @author Tone Sommerland
 */
public interface TypeContext
{
    /**
     * The real number type.
     */
    public abstract NumberType numberType();
    
    /**
     * The boolean type.
     */
    public abstract BooleanType booleanType();
    
    /**
     * The {@link com.calclipse.math.matrix.Matrix} type.
     */
    public abstract MatrixType matrixType();
    
    /**
     * The {@link com.calclipse.math.parser.type.Array} type.
     */
    public abstract ArrayType arrayType();
    
    /**
     * The {@link com.calclipse.math.parser.type.AssociativeArray} type.
     */
    public abstract AssociativeArrayType associativeArrayType();
    
    /**
     * Value comparer.
     */
    public abstract Comparer comparer();
    
    /**
     * Value indexer.
     */
    public abstract Indexer indexer();
    
    /**
     * Value (re-)sizer.
     */
    public abstract Sizer sizer();

}
