package com.calclipse.math.parser;

import com.calclipse.math.expression.Evaluator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Scanner;

/**
 * Math parser configuration.
 * A math parser is configured with a
 * {@link Scanner scanner},
 * a list of
 * {@link ParseStep parse steps},
 * an
 * {@link Evaluator evaluator}
 * that understands the parsed expression,
 * and a
 * {@link SymbolTable symbol table}.
 * 
 * @author Tone Sommerland
 */
public interface MathParserConfig
{
    /**
     * The scanner.
     */
    public abstract Scanner scanner();
    
    /**
     * The parse steps.
     */
    public abstract ParseStep[] steps();
    
    /**
     * The evaluator.
     */
    public abstract Evaluator evaluator();
    
    /**
     * The symbol table.
     */
    public abstract SymbolTable symbolTable();

}
