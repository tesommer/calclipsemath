package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.MatrixUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Absolute value.
 * Supported types:
 * <ul>
 *  <li>complex</li>
 *  <li>real</li>
 *  <li>matrix: the Frobenius norm</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Abs implements UnaryOperation
{
    public static final String NAME = "abs";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private Abs(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static UnaryOperator operator(final TypeContext typeContext)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Abs(typeContext))
                    .withId(ID);
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.of(
                NAME,
                delimitation,
                new Abs(typeContext)
                    .asVariableArity(Abs::wrongNumberOfArguments))
            .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final Object o = arg.get();

        if (o instanceof Complex)
        {
            final var c = (Complex)o;
            return Value.constantOf(c.abs());

        }
        else if (o instanceof Number)
        {
            final var n = (Number)o;
            return Value.constantOf(typeContext.numberType().valueOf(
                    Math.abs(n.doubleValue())));

        }
        else if (o instanceof Matrix)
        {
            final var mx = (Matrix)o;
            return Value.constantOf(typeContext.numberType().valueOf(
                    MatrixUtil.frobeniusNorm(mx)));
        }

        throw errorMessage(TypeMessages.invalidType(o));
    }
    
    private static ErrorMessage wrongNumberOfArguments()
    {
        return errorMessage(ArgMessages.expectedArgCount(1));
    }

}
