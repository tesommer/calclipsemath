package com.calclipse.math.parser.step;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.Indexer;

/**
 * Support for indices.
 * This step is given an
 * {@link com.calclipse.math.parser.type.Indexer}
 * which will handle the indexing during evaluation.
 * This step scans the token stream for occurrences of
 * index list closers, index separators and index list openers.
 * When found, the separators are replaced with
 * a binary operator with a very low priority, called an accumulator.
 * Another binary operator with a very high priority, called a delegator,
 * is inserted before openers.
 * (The purpose of the accumulator is to collect the indices,
 * and the delegator will give them to the indexer).
 * To get an instance of this class, start by calling
 * {@link Indices#openIndexListWith(Predicate)}.
 * 
 * @author Tone Sommerland
 */
public final class Indices implements ParseStep
{
    /**
     * Builds instances of {@link Indices}.
     * There are four mandatory build steps before getting a builder instance.
     * Start by calling
     * {@link Indices#openIndexListWith(Predicate)}.
     */
    public static interface Builder
    {
        /**
         * Sets the accumulator priority.
         * @return {@code this}
         */
        public abstract Builder setAccumulatorPriority(int priority);
        /**
         * Sets the delegator priority.
         * @return {@code this}
         */
        public abstract Builder setDelegatorPriority(int priority);
        /**
         * Builds the {@link Indices} instance.
         */
        public abstract ParseStep build();
    }
    
    /**
     * The {@link Indices} build-step that specifies the indexer.
     * Start by calling
     * {@link Indices#openIndexListWith(Predicate)}.
     * This is the fourth mandatory build step.
     */
    @FunctionalInterface
    public static interface SetIndexer
    {
        /**
         * Sets the indexer
         * that will be handling the indexing during evaluation.
         * @return an {@link Indices} builder
         */
        public abstract Builder delegateIndexingTo(Indexer indexer);
    }
    
    /**
     * The {@link Indices} build-step that specifies the index separator.
     * Start by calling
     * {@link Indices#openIndexListWith(Predicate)}.
     * This is the third mandatory build step.
     */
    @FunctionalInterface
    public static interface SetIndexSeparator
    {
        /**
         * Sets the index separator.
         * @return the next build step
         */
        public abstract SetIndexer separateIndicesWith(
                Predicate<? super Token> separator);
    }
    
    /**
     * The {@link Indices} build-step that specifies the index-list closer.
     * Start by calling
     * {@link Indices#openIndexListWith(Predicate)}.
     * This is the second mandatory build step.
     */
    @FunctionalInterface
    public static interface SetIndexListCloser
    {
        /**
         * Sets the index list closer.
         * @return the next build step
         */
        public abstract SetIndexSeparator closeIndexListWith(
                Predicate<? super Token> closer);
    }
    
    private static final class BuilderImpl implements Builder
    {
        private final Predicate<? super Token> opener;
        private Predicate<? super Token> closer;
        private Predicate<? super Token> separator;
        private Indexer indexer;
        private int accumulatorPriority = -1000;
        private int delegatorPriority = 1000;
        
        private BuilderImpl(final Predicate<? super Token> opener)
        {
            this.opener = requireNonNull(opener, "opener");
        }
        
        private SetIndexSeparator setCloser(
                final Predicate<? super Token> closer)
        {
            this.closer = requireNonNull(closer, "closer");
            return this::setSeparator;
        }
        
        private SetIndexer setSeparator(
                final Predicate<? super Token> separator)
        {
            this.separator = requireNonNull(separator, "separator");
            return this::setIndexer;
        }
        
        private Builder setIndexer(final Indexer indexer)
        {
            this.indexer = requireNonNull(indexer, "indexer");
            return this;
        }

        @Override
        public Builder setAccumulatorPriority(final int accumulatorPriority)
        {
            this.accumulatorPriority = accumulatorPriority;
            return this;
        }

        @Override
        public Builder setDelegatorPriority(final int delegatorPriority)
        {
            this.delegatorPriority = delegatorPriority;
            return this;
        }

        @Override
        public ParseStep build()
        {
            return new Indices(
                    accumulatorPriority,
                    delegatorPriority,
                    opener,
                    closer,
                    separator,
                    indexer);
        }
    }
    
    /**
     * Initiates the procedure for building an {@link Indices}.
     * @param opener the index-list opener
     * @return the next build step
     */
    public static SetIndexListCloser openIndexListWith(
            final Predicate<? super Token> opener)
    {
        final var builder = new BuilderImpl(opener);
        return builder::setCloser;
    }
    
    private static final class IndexList extends ArrayList<Integer>
    {
        private static final long serialVersionUID = 1L;
        
        private IndexList()
        {
        }
    }
    
    private static enum Accumulator implements BinaryOperation
    {
        INSTANCE;

        @Override
        public Value evaluate(final Value left, final Value right)
                throws ErrorMessage
        {
            final Object leftVal = left.get();
            final Object rightVal = right.get();
            final IndexList indices;
            if (leftVal instanceof IndexList)
            {
                indices = (IndexList)leftVal;
            }
            else
            {
                indices = new IndexList();
                indices.add(TypeUtil.toInt(leftVal));
            }
            indices.add(TypeUtil.toInt(rightVal));
            return Value.constantOf(indices);
        }
    }
    
    private static final class Delegator implements BinaryOperation
    {
        private final Indexer indexer;

        private Delegator(final Indexer indexer)
        {
            this.indexer = indexer;
        }

        @Override
        public Value evaluate(final Value left, final Value right)
                throws ErrorMessage
        {
            final Object rightVal = right.get();
            if (rightVal instanceof IndexList)
            {
                final var indices = (IndexList)rightVal;
                return indexer.get(
                        left, indices.stream().mapToInt(i -> i).toArray());
            }
            throw errorMessage(TypeMessages.invalidType(rightVal));
        }
    }

    private final Predicate<? super Token> opener;
    private final Predicate<? super Token> closer;
    private final Predicate<? super Token> separator;
    private final BinaryOperation delegator;
    private final int accumulatorPriority;
    private final int delegatorPriority;

    private Indices(
            final int accumulatorPriority,
            final int delegatorPriority,
            final Predicate<? super Token> opener,
            final Predicate<? super Token> closer,
            final Predicate<? super Token> separator,
            final Indexer indexer)
    {
        assert opener    != null;
        assert closer    != null;
        assert separator != null;
        assert indexer   != null;
        this.opener = opener;
        this.closer = closer;
        this.separator = separator;
        this.delegator = new Delegator(indexer);
        this.accumulatorPriority = accumulatorPriority;
        this.delegatorPriority = delegatorPriority;
    }

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        stream.buffer().add(stream.input());
        stream.skip();
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
        processBuffer(stream.buffer());
        for (final Fragment frag : stream.buffer())
        {
            stream.write(frag);
        }
    }
    
    private void processBuffer(final List<Fragment> buffer)
    {
        final var flags = new ArrayDeque<Boolean>();
        for (int i = buffer.size() - 1; i >= 0; i--)
        {
            final Fragment frag = buffer.get(i);
            if (closer.test(frag.token()))
            {
                flags.add(Boolean.FALSE);
            }
            else if (separator.test(frag.token()) && !flags.isEmpty())
            {
                buffer.set(i, replaceWithAccumulator(frag));
                flags.removeLast();
                flags.add(Boolean.TRUE);
            }
            else if (opener.test(frag.token())
                    && !flags.isEmpty() && flags.removeLast())
            {
                buffer.add(i, replaceWithDelegator(frag));
            }
        }
    }
    
    private Fragment replaceWithAccumulator(final Fragment frag)
    {
        return Fragment.of(
                BinaryOperator.of(
                        frag.token().name(),
                        accumulatorPriority,
                        Accumulator.INSTANCE),
                frag.position());
    }
    
    private Fragment replaceWithDelegator(final Fragment frag)
    {
        return Fragment.of(
                BinaryOperator.of(
                        frag.token().name(),
                        delegatorPriority,
                        delegator),
                frag.position());
    }

}
