package com.calclipse.math.expression.recursion;

import com.calclipse.math.expression.recursion.internal.AtomicRecursionCounter;
import com.calclipse.math.expression.recursion.internal.SimpleRecursionCounter;

/**
 * Protects against indefinite recursion.
 * 
 * @author Tone Sommerland
 */
public interface RecursionGuard
{
    /**
     * Returns a simple, naive recursion counter
     * that increments and decrements an {@code int}.
     * @param limit the limit at which a {@link UnboundRecursion} is thrown
     * @throws IllegalArgumentException if the limit is less than one
     */
    public static RecursionGuard countingTo(final int limit)
    {
        return new SimpleRecursionCounter(limit);
    }
    
    /**
     * Returns a thread-safe, nuclear recursion counter.
     * @param limit the limit at which a {@link UnboundRecursion} is thrown
     * @throws IllegalArgumentException if the limit is less than one
     */
    public static RecursionGuard countingAtomicallyTo(final int limit)
    {
        return new AtomicRecursionCounter(limit);
    }
    
    /**
     * Increments the recursion count.
     * @throws UnboundRecursion
     * if the count has reached the maximum limit.
     */
    public abstract void increment() throws UnboundRecursion;
    
    /**
     * Decrements the recursion count.
     */
    public abstract void decrement();

}
