package com.calclipse.math.expression.operation;

import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Values;

/**
 * Conditional evaluation of a variable number of arguments.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface ConditionalOperation
{
    /**
     * Performs the evaluation.
     * @param args the arguments
     * @return the result of the evaluation
     * @throws ErrorMessage if the evaluation resulted in an error
     */
    public abstract Value evaluate(List<Expression> args) throws ErrorMessage;
    
    /**
     * Overrides the given operation with this operation.
     * @param operation the operation to override
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default ConditionalOperation override(
            final ConditionalOperation operation)
    {
        requireNonNull(operation, "operation");
        return args -> operation.overriddenEvaluate(args, this);
    }
    
    /**
     * Allows this operation to be overridden by the given operation.
     * @param override the operation that overrides this one
     * @return an operation that performs the overridden evaluation
     * @see Values#YIELD
     */
    public default ConditionalOperation acceptOverride(
            final ConditionalOperation override)
    {
        requireNonNull(override, "override");
        return args -> overriddenEvaluate(args, override);
    }
    
    private Value overriddenEvaluate(
            final List<Expression> args,
            final ConditionalOperation override) throws ErrorMessage
    {
        final Value result = override.evaluate(args);
        if (result == Values.YIELD)
        {
            return evaluate(args);
        }
        return result;
    }

}
