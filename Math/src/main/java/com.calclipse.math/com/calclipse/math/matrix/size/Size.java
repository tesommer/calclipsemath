package com.calclipse.math.matrix.size;

import com.calclipse.math.matrix.Matrix;

/**
 * An immutable object encapsulating the size of a matrix.
 * 
 * @author Tone Sommerland
 */
public final class Size
{
    private final int rows;
    private final int columns;
    
    private Size(final int rows, final int columns)
    {
        this.rows = rows;
        this.columns = columns;
    }
    
    /**
     * Returns an instance containing the size of the given matrix.
     */
    public static Size of(final Matrix matrix)
    {
        return new Size(matrix.rows(), matrix.columns());
    }
    
    /**
     * Number of rows.
     */
    public int rows()
    {
        return rows;
    }
    
    /**
     * Number of columns.
     */
    public int columns()
    {
        return columns;
    }

    @Override
    public String toString()
    {
        return String.valueOf(rows) + 'x' + String.valueOf(columns);
    }
}
