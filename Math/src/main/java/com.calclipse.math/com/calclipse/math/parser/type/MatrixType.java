package com.calclipse.math.parser.type;

import com.calclipse.math.matrix.Matrix;

/**
 * Represents the math parser's matrix type.
 * 
 * @author Tone Sommerland
 */
public interface MatrixType
{
    /**
     * Creates a matrix with the specified number of rows and columns.
     */
    public abstract Matrix createMatrix(int rows, int columns);

}
