package com.calclipse.math.parser;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;

/**
 * When you're too busy or tired to make you own parentheses and commas,
 * just use these.
 * 
 * @author Tone Sommerland
 */
public final class ParensAndComma
{
    /**
     * Left parenthesis.
     */
    public static final Token
    LPAREN = Tokens.leftParenthesis("(");
    
    /**
     * Right parenthesis.
     */
    public static final Token
    RPAREN = Tokens.rightParenthesis(")");
    
    /**
     * Comma.
     */
    public static final Token
    COMMA = Tokens.plain(",");
    
    /**
     * A stateless delimitation with
     * {@link #LPAREN} as the opener,
     * {@link #RPAREN} as the closer
     * and
     * {@link #COMMA} as the separator.
     */
    public static final Delimitation
    DELIMITATION = Delimitation.byTokensWithOpener(
            LPAREN, RPAREN, COMMA, Token::identifiesAs);

    private ParensAndComma()
    {
    }

}
