package com.calclipse.math.parser.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * This is a function for creating matrices.
 * The first two arguments are required:
 * number of rows and columns, both integers.
 * Remaining arguments are the line-by-line matrix elements.
 * The number of elements provided must not exceed
 * the number of elements required.
 * 
 * @author Tone Sommerland
 */
public final class BracketAngles implements VariableArityOperation
{
    public static final String NAME = "[<";
    
    public static final Token CLOSER = Tokens.plain(">]");
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;

    private BracketAngles(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(final TypeContext typeContext)
    {
        return Function.of(
                NAME,
                Delimitation.byTokensWithoutOpener(
                        CLOSER, ParensAndComma.COMMA, Token::identifiesAs),
                new BracketAngles(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(2, args.length);
        final int rows = TypeUtil.argToInt(1, args[0].get());
        final int columns = TypeUtil.argToInt(2, args[1].get());
        ArgUtil.requireMaximumArgCount(2 + rows * columns, args.length);
        final Matrix matrix
            = typeContext.matrixType().createMatrix(rows, columns);
        for (int i = 2; i < args.length; i++)
        {
            final double element = TypeUtil.argToDouble(i + 1, args[i].get());
            final int j = i - 2;
            final int row = j / columns;
            final int col = j % columns;
            matrix.set(row, col, element);
        }
        return Value.constantOf(matrix);
    }

}
