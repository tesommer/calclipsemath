package com.calclipse.math.parser.type;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.Value;

/**
 * Defines how data types are accessed using indices.
 * The implementation determines the base of the indices,
 * but one-based indices are conventionally used in mathematics.
 * 
 * @author Tone Sommerland
 */
public interface Indexer
{
    /**
     * Applies indices to a value.
     */
    public abstract Value get(Value value, int... indices) throws ErrorMessage;

}
