package com.calclipse.math.expression.recursion;

import com.calclipse.math.expression.ErrorMessage;

/**
 * Indefinite recursion.
 * 
 * @author Tone Sommerland
 */
public final class UnboundRecursion extends ErrorMessage
{
    private static final long serialVersionUID = 1L;
    
    /**
     * Builds {@link UnboundRecursion} instances.
     * 
     * @author Tone Sommerland
     */
    private static final class Builder extends ErrorMessage.Builder
    {
        private Builder(final String message)
        {
            super(message);
        }

        private Builder(final UnboundRecursion ex)
        {
            super(ex);
        }

        @Override
        protected UnboundRecursion build()
        {
            return new UnboundRecursion(this);
        }
    }
    
    private UnboundRecursion(final Builder builder)
    {
        super(builder);
    }
    
    /**
     * Returns an instance with the given message.
     */
    public static UnboundRecursion saying(final String message)
    {
        return new Builder(message).build();
    }

    @Override
    protected Builder builder()
    {
        return new Builder(this);
    }

}
