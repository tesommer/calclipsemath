package com.calclipse.math.parser.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.misc.MatrixUtil;
import com.calclipse.math.parser.misc.SingularMatrix;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;

/**
 * Exponentiation.
 * Supported types:
 * <ul>
 *  <li>complex|real, complex|real</li>
 *  <li>matrix, integer</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class ToThePowerOf implements BinaryOperation
{
    public static final String NAME = "^";
    
    private static final Id ID = Id.unique();
    
    private final TypeContext typeContext;
    
    private ToThePowerOf(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME,
                OperatorPriorities.EXPONENTIATION,
                new ToThePowerOf(typeContext))
                    .withId(ID);
    }
    
    public static boolean identifiesAs(final Token token)
    {
        return token.identifiesAs(ID);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object o1 = left.get();
        final Object o2 = right.get();
        
        if (o1 instanceof Complex && o2 instanceof Complex)
        {
            return evalComplexComplex(o1, o2);
            
        }
        else if (o1 instanceof Complex && o2 instanceof Number)
        {
            return evalComplexNumber(o1, o2);
            
        }
        else if (o1 instanceof Number && o2 instanceof Complex)
        {
            return evalNumberComplex(o1, o2);
            
        }
        else if (o1 instanceof Number && o2 instanceof Number)
        {
            return evalNumberNumber(typeContext, o1, o2);
            
        }
        else if (o1 instanceof Matrix && o2 instanceof Number)
        {
            return evalMatrixNumber(o1, o2);
        }
        
        throw errorMessage(TypeMessages.incompatibleTypes(o1, o2));
    }

    private static Value evalNumberNumber(
            final TypeContext typeContext, final Object o1, final Object o2)
    {
        final var n1 = (Number)o1;
        final var n2 = (Number)o2;
        final double d1 = n1.doubleValue();
        final double d2 = n2.doubleValue();
        if (d1 < 0 && (long)d2 != d2)
        {
            final Complex c = MathUtil.pow(Complex.valueOf(d1, 0), d2);
            return Value.constantOf(c);
        }
        else
        {
            return Value.constantOf(typeContext.numberType()
                    .valueOf(Math.pow(d1, d2)));
        }
    }

    private static Value evalComplexComplex(final Object o1, final Object o2)
    {
        final var c1 = (Complex)o1;
        final var c2 = (Complex)o2;
        return Value.constantOf(MathUtil.pow(c1, c2));
    }

    private static Value evalComplexNumber(final Object o1, final Object o2)
    {
        final var c1 = (Complex)o1;
        final var n2 = (Number)o2;
        return Value.constantOf(MathUtil.pow(c1, n2.doubleValue()));
    }

    private static Value evalNumberComplex(final Object o1, final Object o2)
    {
        final var n1 = (Number)o1;
        final var c2 = (Complex)o2;
        return Value.constantOf(
                MathUtil.pow(Complex.valueOf(n1.doubleValue(), 0), c2));
    }

    private static Value evalMatrixNumber(final Object o1, final Object o2)
            throws ErrorMessage
    {
        final var mx1 = (Matrix)o1;
        final int i2 = TypeUtil.argToInt(2, o2);
        try
        {
            return Value.constantOf(MatrixUtil.pow(mx1, i2));
        }
        catch (final SizeViolation ex)
        {
            throw ArgUtil.forArgument(1, EvalMessages.invalidSize(), ex);
        }
        catch (final SingularMatrix ex)
        {
            throw ArgUtil.forArgument(1, EvalMessages.singularMatrix(), ex);
        }
    }

}
