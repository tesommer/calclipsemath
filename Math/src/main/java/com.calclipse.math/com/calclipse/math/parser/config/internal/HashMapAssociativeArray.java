package com.calclipse.math.parser.config.internal;

import java.util.HashMap;
import java.util.Map;

import com.calclipse.math.parser.type.AssociativeArray;

/**
 * An associative-array implementation based on {@code HashMap<Object, Object>}.
 * 
 * @author Tone Sommerland
 */
public final class HashMapAssociativeArray
    extends HashMap<Object, Object> implements AssociativeArray
{
    private static final long serialVersionUID = 1L;

    public HashMapAssociativeArray(final Map<?, ?> m)
    {
        super(m);
    }

    @Override
    public String toString()
    {
        return Stringification.stringify(this);
    }

}
