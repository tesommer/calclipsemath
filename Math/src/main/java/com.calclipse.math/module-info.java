/**
 * Contains a math-parser library.
 */
module com.calclipse.math
{
    exports com.calclipse.math;
    exports com.calclipse.math.expression;
    exports com.calclipse.math.expression.message;
    exports com.calclipse.math.expression.operation;
    exports com.calclipse.math.expression.recursion;
    exports com.calclipse.math.matrix;
    exports com.calclipse.math.matrix.function;
    exports com.calclipse.math.matrix.size;
    exports com.calclipse.math.parser;
    exports com.calclipse.math.parser.config;
    exports com.calclipse.math.parser.misc;
    exports com.calclipse.math.parser.scanner;
    exports com.calclipse.math.parser.step;
    exports com.calclipse.math.parser.symbol;
    exports com.calclipse.math.parser.type;
}
