package com.calclipse.mcomp.internal.locator;

import static java.util.Objects.requireNonNull;

import java.util.Map;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

/**
 * Resolves aliases for locations.
 * 
 * @author Tone Sommerland
 */
public final class AliasResolver implements McLocator
{
    private final Map<String, String> aliases;
    private final McContext context;
    
    /**
     * Creates an alias resolver.
     * @param aliases contains the aliases as keys and the locations as values
     */
    public AliasResolver(
            final Map<String, String> aliases, final McContext context)
    {
        this.aliases = Map.copyOf(aliases);
        this.context = requireNonNull(context, "context");
    }

    @Override
    public boolean accepts(final String location, final String referrer)
    {
        return aliases.containsKey(location);
    }

    @Override
    public Mcomp find(final String location, final String referrer)
        throws ErrorMessage
    {
        final String resolved = aliases.get(location);
        return context.mcompAt(resolved, null).orElse(null);
    }

}
