package com.calclipse.mcomp.internal.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.mcomp.McContext;

/**
 * Asks the user for input.
 * The input is read from the standard input stream of the environment.
 * Supported types:
 * <ul>
 *  <li>*: display text, returns the string that was entered</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Sprompt implements UnaryOperation
{
    private static final String NAME = "sprompt";
    
    private final McContext context;

    private Sprompt(final McContext context)
    {
        this.context = requireNonNull(context, "context");
    }
    
    public static UnaryOperator operator(final McContext context)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.VERY_LOW, new Sprompt(context));
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final String input = Prompt.prompt(
                context.environment(), arg.get().toString());
        return Value.constantOf(input);
    }

}
