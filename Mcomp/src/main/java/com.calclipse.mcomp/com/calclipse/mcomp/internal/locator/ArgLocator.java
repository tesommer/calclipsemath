package com.calclipse.mcomp.internal.locator;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

/**
 * This locator makes it possible to pass arguments to scripts or mcomps.
 * The arguments are accessed with the following symbols:
 * <ul>
 * <li>{@code args}:
 * an array containing the arguments as strings</li>
 * <li>{@code args_eval}:
 * an operator that expects an index into args</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class ArgLocator implements McLocator, Mcomp
{
    /**
     * The name of the args symbol: {@code "args"}
     */
    private static final String ARGS_SYMBOL = "args";
    
    /**
     * The name of the args_eval symbol: {@code "args_eval"}
     */
    private static final String ARGS_EVAL_SYMBOL = "args_eval";
    
    private final String location;
    private final String[] args;

    /**
     * Creates a new argument locator.
     * @param location the location that imports the arguments
     * @param args the script/mcomp arguments
     */
    public ArgLocator(final String location, final String... args)
    {
        this.location = requireNonNull(location, "location");
        this.args = args.clone();
    }
    
    /**
     * Attempts to locate the arguments
     * at the specified location in the specified context.
     * @return an empty array if not found
     */
    public static String[] findArgs(
            final String location,
            final McContext context) throws ErrorMessage
    {
        return context.mcompAt(location, null)
                .map(ArgLocator::argsOrEmpty)
                .orElse(new String[0]);
    }

    private static String[] argsOrEmpty(final Mcomp mcomp)
    {
        if (mcomp instanceof ArgLocator)
        {
            return ((ArgLocator)mcomp).args.clone();
        }
        return new String[0];
    }

    @Override
    public boolean accepts(final String loc, final String ref)
    {
        return location.equals(loc);
    }

    @Override
    public Mcomp find(final String loc, final String ref)
            throws ErrorMessage
    {
        return this;
    }
    
    /**
     * Returns the location.
     */
    @Override
    public String name()
    {
        return location;
    }
    
    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        final Token argsSymbol = createArgsSymbol(ARGS_SYMBOL, context);
        final Token evalSymbol = UnaryOperator.prefixed(
                ARGS_EVAL_SYMBOL,
                OperatorPriorities.MEDIUM,
                new ArgEvaluator(context));
        context.exportSymbol(argsSymbol);
        context.exportSymbol(evalSymbol);
    }

    /**
     * Returns the arguments as a symbol that can be exported.
     * @param name the symbol
     * @return a constant containing an
     * {@link com.calclipse.math.parser.type.Array}
     * of strings.
     */
    private final Token createArgsSymbol(
            final String name, final McContext context)
    {
        return Operand.of(name, Value.constantOf(
                context.typeContext().arrayType().arrayOf((Object[])args)));
    }

    /**
     * This operator evaluates located args.
     * It expects an integer: a one-based arg index.
     * 
     * @author Tone Sommerland
     */
    private final class ArgEvaluator implements UnaryOperation
    {
        private final McContext context;

        /**
         * Creates a new evaluator operation.
         */
        private ArgEvaluator(final McContext context)
        {
            this.context = requireNonNull(context, "context");
        }
        
        @Override
        public Value evaluate(final Value arg) throws ErrorMessage
        {
            final int index = TypeUtil.toInt(arg.get()) - 1;
            if (index < 0 || index >= args.length)
            {
                throw errorMessage(EvalMessages.invalidIndex());
            }
            return Value.constantOf(
                    context.parse(args[index]).evaluate(true).get());
        }
        
    }

}
