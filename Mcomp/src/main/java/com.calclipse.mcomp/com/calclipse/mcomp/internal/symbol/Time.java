package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.type.TypeContext;

/**
 * The current time in milliseconds since midnight, January 1, 1970 UTC.
 * 
 * @author Tone Sommerland
 */
public final class Time implements Value
{
    private static final String NAME = "time";
    
    private final TypeContext typeContext;

    private Time(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Operand operand(final TypeContext typeContext)
    {
        return Operand.of(NAME, new Time(typeContext));
    }

    @Override
    public Object get()
    {
        return typeContext.numberType().valueOf(System.currentTimeMillis());
    }

    @Override
    public void set(final Object value) throws ErrorMessage
    {
        throw errorMessage(EvalMessages.illegalAssignment());
    }

    @Override
    public boolean isConstant()
    {
        return false;
    }

}
