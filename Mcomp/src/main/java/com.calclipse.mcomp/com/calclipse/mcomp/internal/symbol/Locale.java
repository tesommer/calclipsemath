package com.calclipse.mcomp.internal.symbol;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;

/**
 * This function creates a {@code java.util.Locale} object.
 * It accepts either 1, 2 or 3 arguments
 * (language, country and variant),
 * which are given to {@code java.util.Locale}'s constructor.
 * 
 * @author Tone Sommerland
 */
public final class Locale implements VariableArityOperation
{
    private static final String NAME = "locale";
    
    private static final VariableArityOperation INSTANCE = new Locale();

    private Locale()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(1, 3, args.length);
        final java.util.Locale locale;
        if (args.length == 1)
        {
            locale = new java.util.Locale(
                    args[0].get().toString());
        }
        else if (args.length == 2)
        {
            locale = new java.util.Locale(
                    args[0].get().toString(),
                    args[1].get().toString());
        }
        else
        {
            locale = new java.util.Locale(
                    args[0].get().toString(),
                    args[1].get().toString(),
                    args[2].get().toString());
        }
        return Value.constantOf(locale);
    }

}
