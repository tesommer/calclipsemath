package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.EvalUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.jump.Break;
import com.calclipse.mcomp.internal.jump.Continue;

/**
 * For loop.
 * This function expects two arguments: a head and a body.
 * The head is a split into three parts:
 * the initialization, condition and update.
 * The split is done at the binary operator with the lowest priority.
 * Typically this will be semicolon.
 * Either the result of the last body execution is returned,
 * or the result of the initialization if the loop never entered the body.
 * 
 * @author Tone Sommerland
 */
public final class For implements ConditionalOperation
{
    private static final String NAME = "for";
    
    private final TypeContext typeContext;

    private For(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.ofConditional(NAME, delimitation, new For(typeContext));
    }

    @Override
    public Value evaluate(final List<Expression> args) throws ErrorMessage
    {
        if (args.size() != 2)
        {
            throw errorMessage(EvalMessages.syntaxError());
        }
        
        final Expression head = args.get(0);
        final Expression body = args.get(1);
        
        final Expression[] headParts = splitHead(head);
        
        final Expression init = headParts[0];
        final Expression condition = headParts[1];
        final Expression update = headParts[2];
        
        Value result = init.evaluate(false);
        try
        {
            while (EvalUtil.evaluateBoolean(condition, false, typeContext))
            {
                try
                {
                    EvalUtil.checkInterrupted(false);
                    result = body.evaluate(false);
                }
                catch (final Continue ex)
                {
                    ex.decrementJumps();
                }
                update.evaluate(false);
            }
        }
        catch (final Break ex)
        {
            ex.decrementJumps();
        }
        
        return result;
    }

    /**
     * Splits the head into three parts:
     * the initializer, the condition and the updater.
     */
    private static Expression[] splitHead(final Expression head)
        throws ErrorMessage
    {
        final var init = new Expression.Builder(head.evaluator());
        final var condition = new Expression.Builder(head.evaluator());
        final var update = new Expression.Builder(head.evaluator());
        
        splitHead(head, init, condition, update);
        
        // Remove trailing semicolons.
        condition.remove(condition.size() - 1);
        update.remove(update.size() - 1);
        
        return new Expression[]
                {init.build(), condition.build(), update.build()};
    }

    private static void splitHead(
            final Expression head,
            final Expression.Builder init,
            final Expression.Builder condition,
            final Expression.Builder update) throws ErrorMessage
    {
        // Example: i := 0; i < 10; i := i + 1
        // Postfix: i 0 := i 10 < ; i i 1 + := ;
        
        Expression.Builder current = init;
        int splitterPriority = Integer.MAX_VALUE;
        int splitterCount = 0;
        int count = 0;
        for (int i = 0; i < head.size(); i++)
        {
            final Fragment frag = head.get(i);
            current.add(frag);
            if (frag.token().type().isOperand()
                    || frag.token().type().isFunction())
            {
                count++;
            }
            else if (frag.token().type().isBinary())
            {
                count--;
                int priority = ((Operator)frag.token()).priority();
                if (priority < splitterPriority)
                {
                    splitterPriority = priority;
                    splitterCount = 1;
                }
                else if (splitterPriority == priority)
                {
                    splitterCount++;
                }
            }
            if (count == 1 && i > 0)
            {
                current = current == init ? condition : update;
            }
        }
        if (splitterCount != 2)
        {
            throw errorMessage(EvalMessages.syntaxError());
        }
    }

}
