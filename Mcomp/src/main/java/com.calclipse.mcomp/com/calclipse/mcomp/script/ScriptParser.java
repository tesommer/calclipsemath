package com.calclipse.mcomp.script;

import java.io.Reader;
import java.util.function.Supplier;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.mcomp.script.internal.DefaultScriptParser;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Tracer;

/**
 * A script parser.
 * 
 * @author Tone Sommerland
 */
public interface ScriptParser
{
    /**
     * Returns the default script parser.
     * @param tracer for tracking script errors that occur during parsing
     */
    public static ScriptParser trackingErrorsWith(final Tracer tracer)
    {
        return new DefaultScriptParser(tracer);
    }
    
    /**
     * Parses a script from the given input.
     * @param input the script
     * @return a parsed script
     * @throws ErrorMessage for a great number of reasons,
     * including syntax error and I/O error
     */
    public abstract Script parse(Reader input) throws ErrorMessage;
    
    /**
     * Specifies the script's location for the script trace.
     * @param location the script location
     */
    public abstract ScriptParser trackingLocation(String location);
    
    /**
     * Specifies a relative place where the script begins.
     * Useful for error tracking
     * when the script is a selection of text in a text editor.
     * @param base
     * relative place from which line/column numbers will be calculated
     */
    public abstract ScriptParser trackingRelativeTo(Place base);
    
    /**
     * Specifies an externally maintained identifier registry.
     */
    public abstract ScriptParser declaringIdentifiersTo(IdRegistry registry);
    
    /**
     * Specifies the supplier of guards
     * from which to receive protection against indefinite recursion.
     */
    public abstract ScriptParser limitingRecursionsWith(
            Supplier<? extends RecursionGuard> recursionGuard);

}
