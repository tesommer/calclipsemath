package com.calclipse.mcomp.trace.internal;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.trace.Trace;

/**
 * A traceable error message.
 * This class stores the execution stack internally as trace objects.
 * 
 * @author Tone Sommerland
 */
public final class TraceableErrorMessage extends ErrorMessage
{
    private static final long serialVersionUID = 1L;
    
    static final class Builder extends ErrorMessage.Builder
    {
        private List<Trace> traceStack;

        Builder(final String message)
        {
            super(message);
        }

        private Builder(final TraceableErrorMessage ex)
        {
            super(ex);
        }
        
        Builder setTraceStack(final Trace trace)
        {
            this.traceStack = List.of(trace);
            return this;
        }
        
        Builder setTraceStack(
                final TraceableErrorMessage previous, final Trace next)
        {
            this.traceStack = Stream.concat(
                    previous.theTraceStack().stream(), Stream.of(next))
                        .collect(toList());
            return this;
        }

        @Override
        protected ErrorMessage build()
        {
            return new TraceableErrorMessage(this);
        }
        
    }
    
    private final transient List<Trace> traceStack;
    
    private TraceableErrorMessage(final Builder builder)
    {
        super(builder);
        this.traceStack = builder.traceStack;
    }

    @Override
    protected Builder builder()
    {
        return new Builder(this);
    }

    private List<Trace> theTraceStack()
    {
        return traceStack == null ? List.of() : traceStack;
    }
    
    /**
     * Returns this exception's trace stack.
     */
    List<Trace> traceStack()
    {
        return theTraceStack();
    }


}
