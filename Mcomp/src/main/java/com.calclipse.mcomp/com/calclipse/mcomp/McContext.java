package com.calclipse.mcomp;

import java.util.Collection;
import java.util.Optional;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Token;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.SystemEnvironment;
import com.calclipse.mcomp.trace.Tracer;

/**
 * Math-Component Context.
 * This interface exposes methods for
 * parsing expressions, locating and executing math components etc.
 * 
 * The context maintains a list of
 * {@link McLocator}s
 * that are queried sequentially when locating {@link Mcomp}s.
 * Additional locators may be added to the context with the
 * {@link #addLocator(McLocator)} method.
 * Adding and removing locators will affect the entire context lineage
 * (in other words, child and parent contexts will be affected by it).
 * 
 * @author Tone Sommerland
 */
public interface McContext
{
    /**
     * The parent context, if it exists.
     */
    public abstract Optional<McContext> parent();
    
    /**
     * Parses a mathematical expression.
     * @param expression the expression to parse
     * @return a parsed expression
     * @throws ErrorMessage if there is an error in the expression
     */
    public abstract Expression parse(String expression) throws ErrorMessage;
    
    /**
     * Adds a symbol to the context's symbol table.
     * @return {@code false} if the symbol was not added
     * (in particular due to a name conflict)
     */
    public abstract boolean addSymbol(Token symbol);
    
    /**
     * Removes the symbol with the given name from the context's symbol table.
     * @return {@code false} if no symbol was removed
     * (in particular because it wasn't in the table)
     */
    public abstract boolean removeSymbol(String name);
    
    /**
     * Returns the symbol with the specified name,
     * or empty if it's not in the table.
     */
    public abstract Optional<Token> symbol(String name);
    
    /**
     * Returns all symbols contained in this context's symbol table.
     */
    public abstract Collection<Token> symbols();
    
    /**
     * Looks up the symbols that begin with the specified character.
     */
    public abstract Collection<Token> lookUpSymbols(char firstChar);
    
    /**
     * Locates a math component.
     * @param location the math component's location
     * @param referrer the location of the invoker (nullable)
     * @return empty if the math components was not found
     * @throws ErrorMessage if an error occurs
     */
    public abstract Optional<Mcomp> mcompAt(String location, String referrer)
            throws ErrorMessage;

    /**
     * Export symbols to make them available to other scripts.
     */
    public abstract void exportSymbol(Token symbol) throws ErrorMessage;
    
    /**
     * Spawns a new context and executes a math component.
     * Any tokens exported by the math component will be added to this context.
     */
    public abstract void importMcomp(Mcomp mcomp) throws ErrorMessage;
    
    /**
     * The symbols imported into this context's symbol table.
     */
    public abstract Collection<Token> importedSymbols();
    
    /**
     * Adds a locator.
     * This method will also affect parent and child contexts.
     */
    public abstract void addLocator(McLocator locator);
    
    /**
     * Removes all locators.
     * @return the removed locators
     */
    public abstract McLocator[] clearLocators();
    
    /**
     * Returns the type context of this math-component context.
     */
    public abstract TypeContext typeContext();
    
    /**
     * Returns this context's tracer.
     * @implSpec
     * Returns {@link Tracer#standard()}.
     */
    public default Tracer tracer()
    {
        return Tracer.standard();
    }

    /**
     * Exported symbols will be prefixed with the name of the
     * imported math component followed by this namespace separator.
     * @implSpec
     * The default namespace separator is {@code ":"} (colon).
     */
    public default String namespaceSeparator()
    {
        return ":";
    }
    
    /**
     * Returns the environment of this context.
     * @implSpec
     * Returns an implementation that delegates to {@code java.lang.System}.
     */
    public default Environment environment()
    {
        return SystemEnvironment.INSTANCE;
    }

}
