package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.EvalUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.jump.Break;
import com.calclipse.mcomp.internal.jump.Continue;

/**
 * Repeat-until control statement.
 * Evaluates the first argument at least once,
 * and then for as long as the second argument is true.
 * Returns the result of the last evaluation of the first argument.
 * Example: {@code reapeat x inc 1 until x > 100 end}
 * 
 * @author Tone Sommerland
 */
public final class Repeat implements ConditionalOperation
{
    private static final String NAME = "repeat";
    
    private final TypeContext typeContext;

    private Repeat(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.ofConditional(
                NAME, delimitation, new Repeat(typeContext));
    }

    @Override
    public Value evaluate(final List<Expression> args) throws ErrorMessage
    {
        if (args.size() != 2)
        {
            throw errorMessage(EvalMessages.syntaxError());
        }
        Value result = Values.UNDEF;
        try
        {
            do
            {
                try
                {
                    EvalUtil.checkInterrupted(false);
                    result = args.get(0).evaluate(false);
                }
                catch (final Continue ex)
                {
                    ex.decrementJumps();
                }
            }
            while(!EvalUtil.evaluateBoolean(args.get(1), false, typeContext));
        }
        catch (final Break ex)
        {
            ex.decrementJumps();
        }
        return result;
    }

}
