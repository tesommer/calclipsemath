package com.calclipse.mcomp.internal.symbol;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.MatrixStringifier;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.EntryFormatter;

/**
 * This operator converts its left argument to a category representation
 * that can be used as a format argument.
 * The right argument is a string specifying the category:
 * <ul>
 * <li>{@code "b"}: Boolean</li>
 * <li>{@code "c"}: Character</li>
 * <li>{@code "i"}: Numeric Integral</li>
 * <li>{@code "f"}: Numeric Floating Point</li>
 * <li>{@code "t"}: Date/Time</li>
 * </ul>
 * For more information, see the Java format-string syntax.
 * 
 * If the left argument is a matrix,
 * the right argument is used as a format string for one argument: an entry.
 * This will change the way the matrix is formatted.
 * The matrix is returned.
 * To specify a locale in addition,
 * use the locale as the right argument on the returned matrix.
 * To revert the format change, use {@code undef} as the right argument.
 * 
 * @see com.calclipse.mcomp.internal.symbol.Printf
 * @see com.calclipse.mcomp.internal.symbol.Sprintf
 * 
 * @author Tone Sommerland
 */
public final class Convertf implements BinaryOperation
{
    private static final String NAME = "convertf";

    /**
     * Right operand value for the boolean category.
     */
    public static final String BOOLEAN = "b";

    /**
     * Right operand value for the character category.
     */
    public static final String CHARACTER = "c";

    /**
     * Right operand value for the numeric integral category.
     */
    public static final String NUMERIC_INTEGRAL = "i";

    /**
     * Right operand value for the numeric floating-point category.
     */
    public static final String NUMERIC_FLOATING_POINT = "f";

    /**
     * Right operand value for the date/time category.
     */
    public static final String DATE_TIME = "t";
    
    private final TypeContext typeContext;

    private Convertf(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static BinaryOperator operator(final TypeContext typeContext)
    {
        return BinaryOperator.of(
                NAME, OperatorPriorities.LOW, new Convertf(typeContext));
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        final Object fArg = left.get();
        if (fArg instanceof Matrix)
        {
            return evaluateMatrixObject((Matrix)fArg, right.get());
        }
        final String convertTo = right.get().toString();
        if (BOOLEAN.equals(convertTo))
        {
            return Value.constantOf(
                    TypeUtil.argToBoolean(1, fArg, typeContext));
        }
        else if (CHARACTER.equals(convertTo))
        {
            return Value.constantOf((char)TypeUtil.argToInt(1, fArg));
        }
        else if (NUMERIC_INTEGRAL.equals(convertTo))
        {
            if (fArg instanceof BigInteger)
            {
                return left;
            }
            return Value.constantOf(TypeUtil.argToLong(1, fArg));
        }
        else if (NUMERIC_FLOATING_POINT.equals(convertTo))
        {
            if (fArg instanceof BigDecimal)
            {
                return left;
            }
            return Value.constantOf(TypeUtil.argToDouble(1, fArg));
        }
        else if (DATE_TIME.equals(convertTo))
        {
            return Value.constantOf(TypeUtil.argToLong(1, fArg));
        }
        return left;
    }
    
    private static Value evaluateMatrixObject(
            final Matrix matrix, final Object right)
    {
        final MatrixStringifier original = matrix.stringifier();
        if (EntryFormatter.isWrappedIn(original))
        {
            if (right instanceof java.util.Locale)
            {
                final var locale = (java.util.Locale)right;
                matrix.setStringifier(
                        EntryFormatter.reWrapWithLocale(original, locale));
                return Value.constantOf(matrix);
            }
            else if (right.equals(Values.UNDEF.get()))
            {
                matrix.setStringifier(EntryFormatter.unwrap(original));
                return Value.constantOf(matrix);
            }
        }
        matrix.setStringifier(
                EntryFormatter.wrap(original, right.toString()));
        return Value.constantOf(matrix);
    }

}
