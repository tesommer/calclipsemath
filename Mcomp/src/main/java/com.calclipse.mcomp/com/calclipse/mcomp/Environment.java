package com.calclipse.mcomp;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * An abstraction of the host environment.
 * Provides access to the standard streams and certain system operations.
 * 
 * @author Tone Sommerland
 */
public interface Environment
{
    /**
     * Standard in.
     */
    public abstract InputStream in();
    
    /**
     * Standard out.
     */
    public abstract PrintStream out();
    
    /**
     * Standard error output-stream.
     */
    public abstract PrintStream err();
    
    /**
     * Point of exit.
     * The implementation may choose not to exit.
     */
    public abstract void exit(int status);

}
