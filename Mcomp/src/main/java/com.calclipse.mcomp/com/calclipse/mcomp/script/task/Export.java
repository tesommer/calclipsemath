package com.calclipse.mcomp.script.task;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Compilable;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.trace.Trace;

/**
 * A task that exports a variable or function.
 * 
 * @author Tone Sommerland
 */
public final class Export extends Task implements Compilable
{
    private final Identifier identifier;
    private final IdRegistry registry;
    private Token symbol;
    
    public Export(
            final Trace scriptTrace,
            final Identifier identifier,
            final IdRegistry registry)
    {
        super(scriptTrace);
        this.identifier = requireNonNull(identifier, "identifier");
        this.registry = requireNonNull(registry, "registry");
    }
    
    @Override
    public boolean isCompiled()
    {
        return symbol != null;
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        try
        {
            compileIfNeeded(context);
            context.exportSymbol(symbol);
        }
        catch (final ErrorMessage ex)
        {
            throw track(ex, context);
        }
    }

    private void compileIfNeeded(final McContext context) throws ErrorMessage
    {
        if (symbol != null)
        {
            return;
        }
        if (registry.isRegistered(identifier.name(), context))
        {
            symbol = context.symbol(identifier.name())
                    .orElseThrow(this::identifierOverwritten);
        }
        else
        {
            throw undeclaredIdentifier();
        }
        if (symbol.type().isOperand())
        {
            final var operand = (Operand)symbol;
            symbol = operand.withValue(Value.constantOf(operand.get()));
        }
    }

    @Override
    public boolean isOneTimeCompilationStep()
    {
        return false;
    }
    
    private ErrorMessage identifierOverwritten()
    {
        return errorMessage(ScriptMessages.identifierOverwritten(
                identifier.name()));
    }
    
    private ErrorMessage undeclaredIdentifier()
    {
        return errorMessage(ScriptMessages.undeclaredIdentifier(
                identifier.name()));
    }
    
    private ErrorMessage track(final Throwable ex, final McContext context)
    {
        return context.tracer().track(
                ex,
                scriptTrace().builder()
                    .setPlace(identifier.place())
                    .build());
    }

}
