package com.calclipse.mcomp.trace.internal;

import static java.util.Objects.requireNonNull;

import java.text.MessageFormat;
import java.util.List;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;
import com.calclipse.mcomp.trace.Tracer;

/**
 * The default tracer.
 * This class formats a message as follows:
 * First it appends a new line.
 * If the trace contains a script name,
 * it applies the name to the {@code scriptNameFormat},
 * and appends that to the message.
 * A similar action is taken if
 * the script location, place or pinpoint is present.
 * 
 * @author Tone Sommerland
 */
public final class DefaultTracer implements Tracer
{
    public static final Tracer INSTANCE = new DefaultTracer();
    
    private static final String EOL = "\n";
    
    private final String messageFormat;
    private final String scriptNameFormat;
    private final String scriptLocationFormat;
    private final String placeFormat;
    private final String causeFormat;
    
    public DefaultTracer(
            final String messageFormat,
            final String scriptNameFormat,
            final String scriptLocationFormat,
            final String placeFormat,
            final String causeFormat)
    {
        this.messageFormat
            = requireNonNull(messageFormat, "messageFormat");
        this.scriptNameFormat
            = requireNonNull(scriptNameFormat, "scriptNameFormat");
        this.scriptLocationFormat
            = requireNonNull(scriptLocationFormat, "scriptLocationFormat");
        this.placeFormat
            = requireNonNull(placeFormat, "placeFormat");
        this.causeFormat
            = requireNonNull(causeFormat, "causeFormat");
    }
    
    private DefaultTracer()
    {
        this(
                "{0}" + EOL,
                EOL + "Name: {0}",
                EOL + "File: {0}",
                EOL + "Place: line {0}, column {1}",
                EOL + "Cause: {0}" + EOL + "{1}");
    }
    
    private String formatMessage(final Trace trace, final String message)
    {
        final var formattedMsg = new StringBuilder();
        append(formattedMsg, messageFormat, message);
        
        trace.scriptName().ifPresent(
                s -> append(formattedMsg, scriptNameFormat, s));
        
        trace.scriptLocation().ifPresent(
                s -> append(formattedMsg, scriptLocationFormat, s));
        
        if (trace.place().isPresent() && trace.pinpoint().isPresent())
        {
            final Place place = trace.place().get();
            final Pinpoint pinpoint = trace.pinpoint().get();
            final var ext = new Extraction(place, pinpoint);
            final Place exactPlace = ext.exactPlace();
            append(
                    formattedMsg,
                    placeFormat,
                    exactPlace.line(),
                    exactPlace.column());
            append(
                    formattedMsg,
                    causeFormat,
                    pinpoint.token(),
                    ext.extract());
            
        }
        else if (trace.place().isPresent())
        {
            final Place place = trace.place().get();
            append(
                    formattedMsg,
                    placeFormat,
                    place.line(),
                    place.column());
        }
        
        return formattedMsg.toString();
    }
    
    private static void append(
            final StringBuilder appendTo,
            final String msgFormat,
            final Object... args)
    {
        appendTo.append(MessageFormat.format(msgFormat, args));
    }

    @Override
    public ErrorMessage track(final String message, final Trace trace)
    {
        return new TraceableErrorMessage.Builder(formatMessage(trace, message))
                .setTraceStack(trace).build();
    }

    @Override
    public ErrorMessage track(final Throwable cause, final Trace trace)
    {
        final var builder = new TraceableErrorMessage.Builder(
                formatMessage(trace, cause.getMessage()));
        if (cause instanceof TraceableErrorMessage)
        {
            builder.setTraceStack((TraceableErrorMessage)cause, trace);
        }
        else
        {
            builder.setTraceStack(trace);
        }
        return builder.build();
    }

    @Override
    public List<Trace> trail(final ErrorMessage ex)
    {
        if (ex instanceof TraceableErrorMessage)
        {
            return ((TraceableErrorMessage)ex).traceStack();
        }
        return List.of();
    }

}
