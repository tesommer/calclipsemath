package com.calclipse.mcomp.script;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;

/**
 * A script that can be executed in a math-component context.
 * 
 * @author Tone Sommerland
 */
public final class Script implements Mcomp
{
    private final String name;
    private final TaskList tasks;
    private final RecursionGuard recursionGuard;
    
    private Script(
            final String name,
            final TaskList tasks,
            final RecursionGuard recursionGuard)
    {
        this.name = requireNonNull(name, "name");
        this.tasks = requireNonNull(tasks, "tasks");
        this.recursionGuard = requireNonNull(recursionGuard, "recursionGuard");
    }
    
    /**
     * Creates a named script that executes the given tasks.
     * Subsequent modifications to the given task list
     * will not affect the script.
     */
    public static Script named(
            final String name,
            final TaskList tasks,
            final RecursionGuard recursionGuard)
    {
        return new Script(name, tasks, recursionGuard);
    }
    
    /**
     * Creates a nameless script that executes the given tasks.
     * Subsequent modifications to the given task list
     * will not affect the script.
     */
    public static Script nameless(
            final TaskList tasks, final RecursionGuard recursionGuard)
    {
        return new Script("", tasks, recursionGuard);
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        try
        {
            recursionGuard.increment();
            tasks.executor().execute(context);
        }
        finally
        {
            recursionGuard.decrement();
        }
    }

    @Override
    public String name()
    {
        return name;
    }

}
