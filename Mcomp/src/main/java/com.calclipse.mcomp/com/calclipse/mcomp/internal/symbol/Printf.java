package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.IllegalFormatException;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.internal.FormatData;

/**
 * Prints a formatted string
 * to the standard output stream of the environment.
 * The format arguments must be converted with the
 * {@link com.calclipse.mcomp.internal.symbol.Convertf} operator.
 * See the Java format-string syntax for more information.
 * 
 * @see com.calclipse.mcomp.internal.FormatData#from(Value...)
 * 
 * @author Tone Sommerland
 */
public final class Printf implements VariableArityOperation
{
    private static final String NAME = "printf";
    
    private final McContext context;

    private Printf(final McContext context)
    {
        this.context = requireNonNull(context, "context");
    }
    
    public static Function function(
            final Delimitation delimitation, final McContext context)
    {
        return Function.of(NAME, delimitation, new Printf(context));
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final FormatData fData = FormatData.from(args);
        try
        {
            fData.locale().ifPresentOrElse(
                    locale -> context.environment().out().format(
                            locale,
                            fData.format(),
                            fData.formatArgs()),
                    ()     -> context.environment().out().format(
                            fData.format(),
                            fData.formatArgs()));
            return Values.UNDEF;
        }
        catch (final IllegalFormatException ex)
        {
            final String message = EvalMessages.messageFor(
                    ArgMessages.invalidArgument(), ex.getMessage());
            throw errorMessage(message).withCause(ex);
        }
    }

}
