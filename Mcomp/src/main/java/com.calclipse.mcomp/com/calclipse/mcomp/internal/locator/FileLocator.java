package com.calclipse.mcomp.internal.locator;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.Script;
import com.calclipse.mcomp.script.ScriptParser;
import com.calclipse.mcomp.trace.Tracer;

/**
 * This locator accepts the path of a script file.
 * It supports relative locations through the use of the referrer parameter.
 * 
 * @author Tone Sommerland
 */
public final class FileLocator implements McLocator
{
    private static final class CacheEntry
    {
        private final Script script;
        private final long lastModified;

        private CacheEntry(final Script script, final long lastModified)
        {
            assert script != null;
            this.script = script;
            this.lastModified = lastModified;
        }
    }

    private final Map<File, CacheEntry> cache = new HashMap<>();
    private final Tracer tracer;

    public FileLocator(final Tracer tracer)
    {
        this.tracer = requireNonNull(tracer, "tracer");
    }

    /**
     * Searches for a file with the specified location as the filename.
     * If referrer is not null, the directory of the referrer
     * will be used as the relative path.
     * @param referrer (nullable)
     * @return null if the file is not found.
     */
    private static File findFile(final String location, final String referrer)
    {
        if (referrer != null && new File(referrer).isFile())
        {
            final File fileWRef = findFileWithReferrer(location, referrer);
            if (fileWRef != null)
            {
                return fileWRef;
            }
        }
        final var file = new File(location);
        return file.isFile() ? file : null;
    }

    private static File findFileWithReferrer(
            final String location, final String referrer)
    {
        final int index = referrer.lastIndexOf(File.separator);
        if (index > -1)
        {
            final var file = new File(referrer.substring(0, index), location);
            if (file.isFile())
            {
                return file;
            }
        }
        return null;
    }

    @Override
    public boolean accepts(final String location, final String referrer)
    {
        return findFile(location, referrer) != null;
    }

    @Override
    public Mcomp find(final String location, final String referrer)
        throws ErrorMessage
    {
        final File file = findFile(location, referrer);
        if (file == null)
        {
            return null;
        }
        final CacheEntry cacheEntry = cache.get(file);
        if (cacheEntry != null
                && cacheEntry.lastModified == file.lastModified())
        {
            return cacheEntry.script;
        }
        return loadAndCacheScript(file);
    }

    private Mcomp loadAndCacheScript(final File file) throws ErrorMessage
    {
        try (
            final var input = new InputStreamReader(new FileInputStream(file));
        )
        {
            final Script newScript = ScriptParser.trackingErrorsWith(tracer)
                    .trackingLocation(file.getAbsolutePath())
                    .parse(input);
            cache.put(file, new CacheEntry(newScript, file.lastModified()));
            return newScript;
        }
        catch (final FileNotFoundException ex)
        {
            return null;
        }
        catch (final IOException ex)
        {
            throw errorMessage(ex.toString()).withCause(ex);
        }
    }

}
