package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import java.util.IllegalFormatException;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.mcomp.internal.FormatData;

public final class Sprintf implements VariableArityOperation
{
    private static final String NAME = "sprintf";
    
    private static final VariableArityOperation INSTANCE = new Sprintf();

    private Sprintf()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        final FormatData fData = FormatData.from(args);
        try
        {
            final String result = fData.locale()
                    .map(locale -> String.format(
                            locale, fData.format(), fData.formatArgs()))
                    .orElse(String.format(fData.format(), fData.formatArgs()));
            return Value.constantOf(result);
        }
        catch (final IllegalFormatException ex)
        {
            final String message = EvalMessages.messageFor(
                    ArgMessages.invalidArgument(), ex.getMessage());
            throw errorMessage(message).withCause(ex);
        }
    }

}
