package com.calclipse.mcomp.internal.jump;

/**
 * Thrown to continue a loop.
 * 
 * @author Tone Sommerland
 */
public final class Continue extends LoopJumper
{
    private static final long serialVersionUID = 1L;
    
    private static final class Builder extends LoopJumper.Builder
    {
        private Builder(final int jumps)
        {
            super(jumps);
        }

        private Builder(final Continue ex)
        {
            super(ex);
        }

        @Override
        protected Continue build()
        {
            return new Continue(this);
        }
    }

    private Continue(final Builder builder)
    {
        super(builder);
    }
    
    /**
     * Returns a continue statement that makes the specified number of jumps.
     * @param jumps the number of nested loops to continue
     * @throws IllegalArgumentException if the number of jumps is less than one
     */
    public static Continue ofJumps(final int jumps)
    {
        return new Builder(jumps).build();
    }

    @Override
    protected Builder builder()
    {
        return new Builder(this);
    }
    
}
