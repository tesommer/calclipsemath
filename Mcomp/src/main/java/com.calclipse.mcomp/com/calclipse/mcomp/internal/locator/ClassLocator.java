package com.calclipse.mcomp.internal.locator;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

/**
 * This locator parses locations of the form {@code class:mypackage.MyMcomp}
 * (fully qualified class names of mcomps).
 * 
 * @author Tone Sommerland
 */
public final class ClassLocator implements McLocator
{
    private final String prefix;
    
    public ClassLocator(final String prefix)
    {
        this.prefix = requireNonNull(prefix, "prefix");
    }

    @Override
    public boolean accepts(final String location, final String referrer)
    {
        return location.startsWith(prefix);
    }

    @Override
    public Mcomp find(final String location, final String referrer)
        throws ErrorMessage
    {
        final String className = location.substring(prefix.length());
        try
        {
            return (Mcomp)Class.forName(className)
                    .getDeclaredConstructor().newInstance();
        }
        catch (
                final ClassCastException
                     |NoSuchMethodException
                     |InvocationTargetException
                     |IllegalAccessException
                     |InstantiationException ex)
        {
            throw errorMessage(ex.toString()).withCause(ex);
        }
        catch (ClassNotFoundException ex)
        {
            return null;
        }
    }

}
