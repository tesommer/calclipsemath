package com.calclipse.mcomp.internal.symbol;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;

/**
 * Returns from a function by throwing
 * {@link com.calclipse.math.expression.Return}.
 * Accepts an optional return value.
 * 
 * @author Tone Sommerland
 */
public final class Return implements VariableArityOperation
{
    private static final String NAME = "return";
    
    private static final VariableArityOperation INSTANCE = new Return();

    private Return()
    {
    }
    
    public static Function function(final Delimitation delimitation)
    {
        return Function.of(NAME, delimitation, INSTANCE);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(0, 1, args.length);
        if (args.length == 1)
        {
            throw com.calclipse.math.expression.Return.of(args[0].get());
        }
        throw com.calclipse.math.expression.Return.of();
    }

}
