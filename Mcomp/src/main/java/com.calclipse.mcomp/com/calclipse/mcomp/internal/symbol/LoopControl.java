package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.function.IntFunction;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.mcomp.internal.jump.Break;
import com.calclipse.mcomp.internal.jump.Continue;
import com.calclipse.mcomp.internal.jump.LoopJumper;

/**
 * A loop-jump statement, such as break or continue.
 * This function accepts an optional integer
 * specifying the number of jumps to make.
 * The default is one.
 * 
 * @author Tone Sommerland
 */
public final class LoopControl implements VariableArityOperation
{
    private static final String BREAK_NAME = "break";
    
    private static final String CONTINUE_NAME = "continue";
    
    private final IntFunction<? extends LoopJumper> jumper;

    private LoopControl(
            final IntFunction<? extends LoopJumper> jumper)
    {
        this.jumper = requireNonNull(jumper, "jumper");
    }
    
    /**
     * Returns a break statement.
     */
    public static Function breakFunction(final Delimitation delimitation)
    {
        return Function.of(
                BREAK_NAME,
                delimitation,
                new LoopControl(Break::ofJumps));
    }
    
    /**
     * Returns a continue statement.
     */
    public static Function continueFunction(final Delimitation delimitation)
    {
        return Function.of(
                CONTINUE_NAME,
                delimitation,
                new LoopControl(Continue::ofJumps));
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireMaximumArgCount(1, args.length);
        if (args.length == 1)
        {
            final int jumps = TypeUtil.toInt(args[0].get());
            if (jumps < 1)
            {
                throw errorMessage(EvalMessages.domainError());
            }
            throw jumper.apply(jumps);
        }
        throw jumper.apply(1);
    }

}
