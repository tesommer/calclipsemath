package com.calclipse.mcomp.internal;

import java.io.InputStream;
import java.io.PrintStream;

import com.calclipse.mcomp.Environment;

/**
 * This implementation delegates to the System class.
 * 
 * @author Tone Sommerland
 */
public enum SystemEnvironment implements Environment
{
    INSTANCE;

    @Override
    public InputStream in()
    {
        return System.in;
    }

    @Override
    public PrintStream out()
    {
        return System.out;
    }

    @Override
    public PrintStream err()
    {
        return System.err;
    }

    @Override
    public void exit(final int status)
    {
        System.exit(status);
    }
    
}
