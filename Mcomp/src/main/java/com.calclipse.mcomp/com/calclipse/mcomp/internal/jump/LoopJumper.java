package com.calclipse.mcomp.internal.jump;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.script.message.ScriptMessages;

/**
 * Thrown to do a loop jump.
 * When a loop-jump statement is encountered inside a loop,
 * the current cycle is terminated.
 * What happens then depends on the type of loop jump.
 * A break statement exits the loop,
 * while a continue statement proceeds with the next iteration.
 * A specified number of jumps is performed.
 * The number of jumps determines the nesting level to jump out of.
 * The number is typically one, but cannot be zero or less.
 * Too many jumps will cause a loop-jump-outside-loop fault.
 * 
 * @see com.calclipse.mcomp.internal.jump.Break
 * @see com.calclipse.mcomp.internal.jump.Continue
 * 
 * @author Tone Sommerland
 */
public abstract class LoopJumper extends ErrorMessage
{
    private static final long serialVersionUID = 1L;
    
    static abstract class Builder extends ErrorMessage.Builder
    {
        private int jumps;

        /**
         * Creates a loop-jumper builder.
         * @param jumps number of jumps
         * @throws IllegalArgumentException
         * if the number of jumps is less than one
         */
        Builder(final int jumps)
        {
            super(ScriptMessages.loopJumpOutsideLoop());
            if (jumps < 1)
            {
                throw new IllegalArgumentException("jumps < 1: " + jumps);
            }
            this.jumps = jumps;
        }

        Builder(final LoopJumper ex)
        {
            super(ex);
            assert ex.jumps >= 1;
            this.jumps = ex.jumps;
        }
        
        private Builder decrementJumps()
        {
             jumps--;
             assert jumps >= 1;
            return this;
        }

        @Override
        protected abstract LoopJumper build();
        
    }
    
    private final int jumps;

    LoopJumper(final Builder builder)
    {
        super(builder);
        assert builder.jumps >= 1;
        this.jumps = builder.jumps;
    }

    @Override
    protected abstract Builder builder();
    
    /**
     * Called inside a loop to decrement the number of jumps.
     * If the number of jumps of this instance is more than one,
     * a new loop jumper is thrown that has one jump less than this one.
     * @implsSpec self-uses {@link #builder()}.
     */
    public final void decrementJumps() throws LoopJumper
    {
        if (jumps == 1)
        {
            return;
        }
        throw builder().decrementJumps().build();
    }
    
}
