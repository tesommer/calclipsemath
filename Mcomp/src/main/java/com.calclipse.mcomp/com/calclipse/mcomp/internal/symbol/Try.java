package com.calclipse.mcomp.internal.symbol;

import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.jump.LoopJumper;

/**
 * Tries to evaluate an expression.
 * Returns {true, value} on success,
 * {false, error message} on failure.
 * 
 * @author Tone Sommerland
 */
public final class Try implements ConditionalOperation
{
    private static final String NAME = "try";
    
    private final TypeContext typeContext;

    private Try(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.ofConditional(NAME, delimitation, new Try(typeContext));
    }

    @Override
    public Value evaluate(final List<Expression> args) throws ErrorMessage
    {
        ArgUtil.requireArgCount(1, args.size());
        try
        {
            return Value.constantOf(typeContext.arrayType().arrayOf(
                    typeContext.booleanType().valueOf(true),
                    args.get(0).evaluate(false).get()));
        }
        catch (final LoopJumper ex)
        {
            throw ex;
        }
        catch (final ErrorMessage ex)
        {
            return Value.constantOf(typeContext.arrayType().arrayOf(
                    typeContext.booleanType().valueOf(false),
                    ex.getMessage()));
        }
    }

}
