package com.calclipse.mcomp.script.task;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

/**
 * A task that imports a math component.
 * 
 * @author Tone Sommerland
 */
public final class Import extends Task
{
    private final String location;
    private final Place place;
    
    public Import(
            final Trace scriptTrace,
            final String location,
            final Place place)
    {
        super(scriptTrace);
        this.location = requireNonNull(location, "location");
        this.place = requireNonNull(place, "place");
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        try
        {
            final String referrer = scriptTrace().scriptLocation()
                    .orElse(null);
            final Mcomp mcomp = context.mcompAt(location, referrer)
                    .orElseThrow(this::locationNotFound);
            context.importMcomp(mcomp);
        }
        catch (final ErrorMessage ex)
        {
            throw track(ex, context);
        }
    }
    
    @Override
    public boolean isOneTimeCompilationStep()
    {
        return false;
    }
    
    private ErrorMessage locationNotFound()
    {
        return errorMessage(ScriptMessages.locationNotFound(location));
    }
    
    private ErrorMessage track(final Throwable ex, final McContext context)
    {
        return context.tracer().track(
                ex, scriptTrace().builder().setPlace(place).build());
    }

}
