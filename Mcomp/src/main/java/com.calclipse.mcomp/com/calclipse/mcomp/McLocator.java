package com.calclipse.mcomp;

import com.calclipse.math.expression.ErrorMessage;

/**
 * A math-component locator.
 * Locators understand location strings on certain formats,
 * and are used by math-component contexts to locate math components.
 * 
 * @see McContext#addLocator(McLocator)
 * @see McContext#mcompAt(String, String)
 * 
 * @author Tone Sommerland
 */
public interface McLocator
{
    /**
     * Whether or not this locator supports the given location format.
     * @param referrer the location of the math component
     * where this call originated (nullable)
     * @return {@code true} if the locator recognizes
     * the format of the specified location
     */
    public abstract boolean accepts(String location, String referrer);
    
    /**
     * Attempts to resolve a location.
     * A return value of {@code null}
     * indicates that the math component was not found.
     * @param referrer the location to the math component
     * where this call originated (nullable)
     */
    public abstract Mcomp find(String location, String referrer)
            throws ErrorMessage;

}
