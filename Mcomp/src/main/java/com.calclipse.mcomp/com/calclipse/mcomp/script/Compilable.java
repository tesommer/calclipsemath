package com.calclipse.mcomp.script;

/**
 * Implemented by tasks that include a compilation step,
 * which is typically needed only the first time the task is executed.
 * This allows certain optimizations to be made
 * when the script is executed more than once.
 * 
 * @author Tone Sommerland
 */
public interface Compilable
{
    /**
     * Whether or not this task is compiled.
     */
    public abstract boolean isCompiled();

}
