package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;

/**
 * Throws an error message.
 * Supported types:
 * <ul>
 *  <li>*: error message</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Raise implements UnaryOperation
{
    private static final String NAME = "raise";
    
    private static final UnaryOperation INSTANCE = new Raise();
    
    private Raise()
    {
    }

    public static UnaryOperator operator()
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, INSTANCE);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        throw errorMessage(arg.get().toString());
    }

}
