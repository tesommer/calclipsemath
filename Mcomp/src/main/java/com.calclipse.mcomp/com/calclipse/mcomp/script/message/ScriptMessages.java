package com.calclipse.mcomp.script.message;

import static java.util.Objects.requireNonNull;

import java.text.MessageFormat;

/**
 * Script-error-messages.
 * Each message has a static accessor and a corresponding mutator in
 * {@link Builder}.
 * Any parameters accepted by a message's accessor are used as format elements.
 * This means that the number of format elements of each message
 * can be determined by looking at the message's accessor.
 * (See {@code java.text.MessageFormat} for information on the format pattern.)
 * 
 * This class has the following messages:
 * 
 * <ul>
 * 
 * <li>{@code invalidIdentifier}:
 * Attempt to declare an identifier with a reserved name.</li>
 * 
 * <li>{@code invalidFunctionVariable}:
 * Attempt to use an invalid identifier as a function variable.
 * Example: {@code f(+)}</li>
 * 
 * <li>{@code invalidArgList}:
 * Attempt to redefine a function with the wrong argument list.</li>
 * 
 * <li>{@code undeclaredIdentifier}:
 * Use of an undeclared identifier
 * in a context in which it should have been declared.</li>
 * 
 * <li>{@code duplicateDeclaration}:
 * Attempt to declare a previously declared identifier.</li>
 * 
 * <li>{@code identifierOverwritten}:
 * Attempt to reassign a previously declared identifier
 * that has been overwritten outside the script.</li>
 * 
 * <li>{@code locationNotFound}:
 * Attempt to import an mcomp failed because the location was wrong.</li>
 * 
 * <li>{@code cannotImportNamespace}:
 * A name conflict prevented a namespace from being imported.
 * Format elements are
 * the namespace and the symbol that caused the conflict.</li>
 * 
 * <li>{@code parseError}:
 * Error while parsing script.</li>
 * 
 * <li>{@code loopJumpOutsideLoop}:
 * A loop jump (like a break or continue statement)
 * was in the wrong place.</li>
 * 
 * </ul>
 * 
 * This class is thread safe.
 * 
 * @author Tone Sommerland
 */
public final class ScriptMessages
{
    private static final class Internals
    {
        private String
        invalidIdentifier = "Invalid identifier: {0}";
        
        private String
        invalidFunctionVariable = "Invalid function variable: {0}";
        
        private String
        invalidArgList = "Invalid argument list: {0}";
        
        private String
        undeclaredIdentifier = "Undeclared identifier: {0}";

        private String
        duplicateDeclaration = "Duplicate declaration: {0}";
        
        private String
        identifierOverwritten = "{0} has been overwritten.";

        private String
        locationNotFound = "Location not found: {0}";

        private String
        cannotImportNamespace
            = "Cannot import namespace: {0}"
            + " Name conflict caused by symbol: {1}";
        
        private String
        parseError = "Error in script: ''{0}'' at line {1}, column {2}.";
        
        private String
        loopJumpOutsideLoop = "Loop jump outside loop.";

        private Internals()
        {
        }
    }
    
    /**
     * Builds instances of {@link ScriptMessages}.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private final Internals internals = new Internals();

        /**
         * Creates a new builder
         * with a default set of messages as a starting point.
         */
        public Builder()
        {
        }

        /**
         * Sets the {@code invalidIdentifier} message.
         * @param invalidIdentifier a message with one format argument
         * @return {@code this}
         */
        public Builder setInvalidIdentifier(final String invalidIdentifier)
        {
            internals.invalidIdentifier = requireNonNull(
                    invalidIdentifier, "invalidIdentifier");
            return this;
        }

        /**
         * Sets the {@code invalidFunctionVariable} message.
         * @param invalidFunctionVariable a message with one format argument
         * @return {@code this}
         */
        public Builder setInvalidFunctionVariable(
                final String invalidFunctionVariable)
        {
            internals.invalidFunctionVariable = requireNonNull(
                    invalidFunctionVariable, "invalidFunctionVariable");
            return this;
        }

        /**
         * Sets the {@code invalidArgList} message.
         * @param invalidArgList a message with one format argument
         * @return {@code this}
         */
        public Builder setInvalidArgList(final String invalidArgList)
        {
            internals.invalidArgList = requireNonNull(
                    invalidArgList, "invalidArgList");
            return this;
        }

        /**
         * Sets the {@code undeclaredIdentifier} message.
         * @param undeclaredIdentifier a message with one format argument
         * @return {@code this}
         */
        public Builder setUndeclaredIdentifier(
                final String undeclaredIdentifier)
        {
            internals.undeclaredIdentifier = requireNonNull(
                    undeclaredIdentifier, "undeclaredIdentifier");
            return this;
        }

        /**
         * Sets the {@code duplicateDeclaration} message.
         * @param duplicateDeclaration a message with one format argument
         * @return {@code this}
         */
        public Builder setDuplicateDeclaration(
                final String duplicateDeclaration)
        {
            internals.duplicateDeclaration = requireNonNull(
                    duplicateDeclaration, "duplicateDeclaration");
            return this;
        }
        
        /**
         * Sets the {@code identifierOverwritten} message.
         * @param identifierOverwritten a message with one format argument
         * @return {@code this}
         */
        public Builder setIdentifierOverwritten(
                final String identifierOverwritten)
        {
            internals.identifierOverwritten = requireNonNull(
                    identifierOverwritten, "identifierOverwritten");
            return this;
        }

        /**
         * Sets the {@code locationNotFound} message.
         * @param locationNotFound a message with one format argument
         * @return {@code this}
         */
        public Builder setLocationNotFound(final String locationNotFound)
        {
            internals.locationNotFound = requireNonNull(
                    locationNotFound, "locationNotFound");
            return this;
        }

        /**
         * Sets the {@code cannotImportNamespace} message.
         * @param cannotImportNamespace a message with two format arguments
         * @return {@code this}
         */
        public Builder setCannotImportNamespace(
                final String cannotImportNamespace)
        {
            internals.cannotImportNamespace = requireNonNull(
                    cannotImportNamespace, "cannotImportNamespace");
            return this;
        }

        /**
         * Sets the {@code parseError} message.
         * @param parseError a message with three format arguments
         * @return {@code this}
         */
        public Builder setParseError(final String parseError)
        {
            internals.parseError = requireNonNull(parseError, "parseError");
            return this;
        }

        /**
         * Sets the {@code loopJumpOutsideLoop} message.
         * @param loopJumpOutsideLoop a message with zero format arguments
         * @return {@code this}
         */
        public Builder setLoopJumpOutsideLoop(
                final String loopJumpOutsideLoop)
        {
            internals.loopJumpOutsideLoop = requireNonNull(
                    loopJumpOutsideLoop, "loopJumpOutsideLoop");
            return this;
        }
        
        /**
         * Builds the parse messages.
         */
        public ScriptMessages build()
        {
            return new ScriptMessages(internals);
        }
    }
    
    private static volatile ScriptMessages
    instance = new ScriptMessages(new Internals());
    
    private final String invalidIdentifier;
    private final String invalidFunctionVariable;
    private final String invalidArgList;
    private final String undeclaredIdentifier;
    private final String duplicateDeclaration;
    private final String identifierOverwritten;
    private final String locationNotFound;
    private final String cannotImportNamespace;
    private final String parseError;
    private final String loopJumpOutsideLoop;
    
    private ScriptMessages(final Internals internals)
    {
        assert internals.invalidIdentifier       != null;
        assert internals.invalidFunctionVariable != null;
        assert internals.invalidArgList          != null;
        assert internals.undeclaredIdentifier    != null;
        assert internals.duplicateDeclaration    != null;
        assert internals.identifierOverwritten   != null;
        assert internals.locationNotFound        != null;
        assert internals.cannotImportNamespace   != null;
        assert internals.parseError              != null;
        assert internals.loopJumpOutsideLoop     != null;
        this.invalidIdentifier       = internals.invalidIdentifier;
        this.invalidFunctionVariable = internals.invalidFunctionVariable;
        this.invalidArgList          = internals.invalidArgList;
        this.undeclaredIdentifier    = internals.undeclaredIdentifier;
        this.duplicateDeclaration    = internals.duplicateDeclaration;
        this.identifierOverwritten   = internals.identifierOverwritten;
        this.locationNotFound        = internals.locationNotFound;
        this.cannotImportNamespace   = internals.cannotImportNamespace;
        this.parseError              = internals.parseError;
        this.loopJumpOutsideLoop     = internals.loopJumpOutsideLoop;
    }
    
    /**
     * Sets the main instance.
     */
    public static void set(final ScriptMessages instance)
    {
        ScriptMessages.instance = requireNonNull(instance, "instance");
    }

    /**
     * Returns a formatted {@code invalidIdentifier} message.
     * @param identifier the invalid identifier
     */
    public static String invalidIdentifier(final Object identifier)
    {
        return MessageFormat.format(
                instance.invalidIdentifier,
                requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns a formatted {@code invalidFunctionVariable} message.
     * @param identifier the variable identifier
     */
    public static String invalidFunctionVariable(final Object identifier)
    {
        return MessageFormat.format(
                instance.invalidFunctionVariable,
                requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns a formatted {@code invalidArgList} message.
     * @param identifier the function identifier
     */
    public static String invalidArgList(final Object identifier)
    {
        return MessageFormat.format(
                instance.invalidArgList,
                requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns a formatted {@code undeclaredIdentifier} message.
     * @param identifier the undeclared identifier
     */
    public static String undeclaredIdentifier(final Object identifier)
    {
        return MessageFormat.format(
                instance.undeclaredIdentifier,
                requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns a formatted {@code duplicateDeclaration} message.
     * @param identifier the duplicate identifier
     */
    public static String duplicateDeclaration(final Object identifier)
    {
        return MessageFormat.format(
                instance.duplicateDeclaration,
                requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns a formatted {@code identifierOverwritten} message.
     * @param identifier the overwritten identifier
     */
    public static String identifierOverwritten(final Object identifier)
    {
        return MessageFormat.format(
                instance.identifierOverwritten,
                requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns a formatted {@code locationNotFound} message.
     * @param location the location
     */
    public static String locationNotFound(final Object location)
    {
        return MessageFormat.format(
                instance.locationNotFound,
                requireNonNull(location, "location"));
    }
    
    /**
     * Returns a formatted {@code cannotImportNamespace} message.
     * @param namespace the namespace
     * @param cause the symbol that caused a name conflict
     */
    public static String cannotImportNamespace(
            final Object namespace, final Object cause)
    {
        return MessageFormat.format(
                instance.cannotImportNamespace,
                requireNonNull(namespace, "namespace"),
                requireNonNull(cause, "cause"));
    }
    
    /**
     * Returns a formatted {@code parseError} message.
     * @param token the token
     * @param line the line number
     * @param column the column number
     */
    public static String parseError(
            final Object token, final Object line, final Object column)
    {
        return MessageFormat.format(
                instance.parseError,
                requireNonNull(token, "token"),
                requireNonNull(line, "line"),
                requireNonNull(column, "column"));
    }
    
    /**
     * Returns a formatted {@code loopJumpOutsideLoop} message.
     */
    public static String loopJumpOutsideLoop()
    {
        return instance.loopJumpOutsideLoop;
    }

    @Override
    public String toString()
    {
        return ScriptMessages.class.getSimpleName() + ": " + invalidIdentifier;
    }

}
