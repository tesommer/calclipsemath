package com.calclipse.mcomp.script.task.internal;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;

/**
 * A variable value.
 * The mutator of this implementation accepts any value (except {@code null}).
 * 
 * @author Tone Sommerland
 */
public final class Variable implements Value
{
    private Object value;

    private Variable(final Object value)
    {
        this.value = requireNonNull(value, "value");
    }
    
    /**
     * Returns a variable containing the given value.
     */
    public static Variable of(final Object value)
    {
        return new Variable(value);
    }
    
    /**
     * Returns a variable containing the value of undef.
     */
    public static Variable of()
    {
        return new Variable(Values.UNDEF.get());
    }

    @Override
    public Object get()
    {
        return value;
    }

    @Override
    public void set(final Object value)
    {
        this.value = requireNonNull(value, "value");
    }

    /**
     * Returns {@code false}.
     */
    @Override
    public boolean isConstant()
    {
        return false;
    }

}
