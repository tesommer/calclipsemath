package com.calclipse.mcomp.internal.symbol;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.EvalUtil;

/**
 * This operator serves as a statement terminator.
 * It has very low priority and only returns its right argument.
 * 
 * @author Tone Sommerland
 */
public final class Semicolon implements BinaryOperation
{
    private static final String NAME = ";";
    
    private static final BinaryOperation INSTANCE = new Semicolon();

    private Semicolon()
    {
    }
    
    public static BinaryOperator operator()
    {
        return BinaryOperator.of(NAME, OperatorPriorities.VERY_LOW, INSTANCE);
    }

    @Override
    public Value evaluate(final Value left, final Value right)
            throws ErrorMessage
    {
        EvalUtil.checkInterrupted(false);
        return right;
    }

}
