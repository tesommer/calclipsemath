package com.calclipse.mcomp.internal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.Delimiters;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Token;
import com.calclipse.mcomp.internal.symbol.If;

/**
 * If-test delimitation.
 * 
 * @see com.calclipse.mcomp.internal.symbol.If
 * 
 * @author Tone Sommerland
 */
public final class IfDelimitation extends Delimitation
{
    private final List<String> clauses = new ArrayList<>();
    private final Token thenToken;
    private final Token elseIfToken;
    private final Token elseToken;
    private final Token endToken;
    private final boolean modifiable;
    
    private IfDelimitation(
            final Token thenToken,
            final Token elseIfToken,
            final Token elseToken,
            final Token endToken,
            final boolean modifiable)
    {
        this.thenToken = requireNonNull(thenToken, "thenToken");
        this.elseIfToken = requireNonNull(elseIfToken, "elseIfToken");
        this.elseToken = requireNonNull(elseToken, "elseToken");
        this.endToken = requireNonNull(endToken, "endToken");
        this.modifiable = modifiable;
    }
    
    private IfDelimitation(final IfDelimitation toSpawn)
    {
        this(
                toSpawn.thenToken,
                toSpawn.elseIfToken,
                toSpawn.elseToken,
                toSpawn.endToken,
                true);
    }
    
    /**
     * Returns an if-delimitation instance.
     * @param thenToken the then clause
     * @param elseIfToken the else-if clause
     * @param elseToken the else clause
     * @param endToken the end
     */
    public static IfDelimitation by(
            final Token thenToken,
            final Token elseIfToken,
            final Token elseToken,
            final Token endToken)
    {
        return new IfDelimitation(
                thenToken, elseIfToken, elseToken, endToken, false);
    }
    
    /**
     * Number of then, else and else-if clauses that has been encountered.
     */
    public int countClauses()
    {
        return clauses.size();
    }
    
    /**
     * Whether or not the clause with the specified index is an else-if.
     * @throws IndexOutOfBoundsException if the index is invalid
     */
    public boolean isElseIf(final int clauseIndex)
    {
        return elseIfToken.matches(clauses.get(clauseIndex));
    }
    
    /**
     * Checks the occurrences of the clauses to see if they are valid.
     * For instance, that an else-if is followed by a then.
     */
    public boolean isValid()
    {
        if (clauses.size() < 1)
        {
            return false;
        }
        else if (!thenToken.matches(clauses.get(0)))
        {
            return false;
        }
        else
        {
            int i = 1;
            while (i < clauses.size())
            {
                if (elseToken.matches(clauses.get(i)))
                {
                    if (i != clauses.size() - 1)
                    {
                        return false;
                    }
                }
                else if (elseIfToken.matches(clauses.get(i)))
                {
                    if (i == clauses.size() - 1)
                    {
                        return false;
                    }
                    i++;
                    if (!thenToken.matches(clauses.get(i)))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                i++;
            }
            return true;
        }
    }

    @Override
    protected boolean hasOpener()
    {
        return false;
    }

    @Override
    protected boolean readsOpener(final Token token)
    {
        return false;
    }

    @Override
    protected boolean readsCloser(final Token token)
    {
        return endToken.matches(token);
    }

    @Override
    protected boolean readsSeparator(final Token token)
    {
        if (readsClause(token))
        {
            if (modifiable)
            {
                clauses.add(token.name());
            }
            return true;
        }
        return false;
    }
    
    private boolean readsClause(final Token token)
    {
        return token.matches(thenToken)
                || token.matches(elseToken) || token.matches(elseIfToken);
    }

    @Override
    protected Delimiters delimiters()
    {
        if (clauses.isEmpty())
        {
            return Delimiters.withoutOpener(
                    endToken.name(), thenToken.name());
        }
        return Delimiters.withoutOpener(
                endToken.name(),
                clauses.get(0),
                clauses.subList(1, clauses.size()).stream()
                    .toArray(String[]::new));
    }

    @Override
    protected Delimitation spawn()
    {
        return new IfDelimitation(this);
    }

    @Override
    protected Function postSpawn(
            final Function func, final Delimitation spawnedDelim)
    {
        if (func.isConditional() && (func.conditional() instanceof If))
        {
            final var operation = (If)func.conditional();
            return func.withConditional(
                    operation.withClauses((IfDelimitation)spawnedDelim));
        }
        return func;
    }

}
