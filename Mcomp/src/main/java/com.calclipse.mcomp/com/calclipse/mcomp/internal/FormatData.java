package com.calclipse.mcomp.internal;

import java.util.Locale;
import java.util.Optional;
import java.util.stream.IntStream;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.ArgUtil;

/**
 * Encapsulates parameters for {@code printf}-style functions.
 * 
 * @author Tone Sommerland
 */
public final class FormatData
{
    private final Locale locale;
    private final String format;
    private final Object[] formatArgs;

    /**
     * Creates a new format-data object.
     * @param locale the locale to apply during formatting (nullable)
     * @param format the format string
     * @param formatArgs arguments referenced by format specifiers
     */
    private FormatData(
            final Locale locale,
            final String format,
            final Object... formatArgs)
    {
        assert format != null;
        assert formatArgs != null;
        this.locale = locale;
        this.format = format;
        this.formatArgs = formatArgs;
    }
    
    /**
     * Translates {@code args} to format parameters.
     * If the first argument is a locale,
     * then at least 2 arguments are expected:
     * the locale, the format string and any format arguments.
     * Otherwise, at least 1 argument is expected:
     * the format string and any format arguments.
     * The format string is obtained by
     * stringifying the corresponding value object.
     * The values of the format arguments are expected to be
     * compatible with the format string.
     */
    public static FormatData from(final Value... args)
            throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(1, args.length);
        final Object firstValue = args[0].get();
        if (firstValue instanceof Locale)
        {
            return withLocale(firstValue, args);
        }
        return withoutLocale(firstValue, args);
    }

    private static FormatData withLocale(
            final Object firstValue, final Value[] args) throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(2, args.length);
        final var locale = (Locale)firstValue;
        final String format = args[1].get().toString();
        final Object[] fArgs = IntStream.range(2, args.length)
                .mapToObj(i -> args[i])
                .map(Value::get)
                .toArray();
        return new FormatData(locale, format, fArgs);
    }
    
    private static FormatData withoutLocale(
            final Object firstValue, final Value[] args) throws ErrorMessage
    {
        final Locale locale = null;
        final String format = firstValue.toString();
        final Object[] fArgs = IntStream.range(1, args.length)
                .mapToObj(i -> args[i])
                .map(Value::get)
                .toArray();
        return new FormatData(locale, format, fArgs);
    }

    /**
     * The locale to apply during formatting.
     */
    public Optional<Locale> locale()
    {
        return Optional.ofNullable(locale);
    }

    /**
     * The format string.
     */
    public String format()
    {
        return format;
    }

    /**
     * The format arguments referenced in the format string.
     */
    public Object[] formatArgs()
    {
        return formatArgs.clone();
    }

}
