package com.calclipse.mcomp.script.task;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Token;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

/**
 * A task that allows imported symbols to be referenced
 * without the namespace prefix.
 * 
 * @author Tone Sommerland
 */
public final class ImportNamespace extends Task
{
    private final String namespace;
    private final Place place;
    
    public ImportNamespace(
            final Trace scriptTrace,
            final String namespace,
            final Place place)
    {
        super(scriptTrace);
        this.namespace = requireNonNull(namespace, "namespace");
        this.place = requireNonNull(place, "place");
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        final String prefix = namespace + context.namespaceSeparator();
        for (final Token symbol : context.importedSymbols())
        {
            if (symbol.name().startsWith(prefix))
            {
                final Token unprefixed = symbol.withName(
                        symbol.name().substring(prefix.length()));
                if (!context.addSymbol(unprefixed))
                {
                    throw cannotImportNamespace(unprefixed.name(), context);
                }
            }
        }
    }
    
    @Override
    public boolean isOneTimeCompilationStep()
    {
        return true;
    }
    
    private ErrorMessage cannotImportNamespace(
            final String symbol, final McContext context)
    {
        return context.tracer().track(
                ScriptMessages.cannotImportNamespace(namespace, symbol),
                scriptTrace().builder().setPlace(place).build());
    }

}
