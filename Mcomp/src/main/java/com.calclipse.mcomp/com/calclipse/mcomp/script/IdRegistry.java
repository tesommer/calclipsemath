package com.calclipse.mcomp.script;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.BiPredicate;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.message.ScriptMessages;

/**
 * A registry of declared identifiers.
 * Used by certain tasks to keep track of identifiers declared in scripts.
 * Each registration belongs to a context,
 * which represents a specific execution of a script.
 * When the execution is finished,
 * and the context is no longer in ordinary use,
 * the context and its associated information may be garbage collected
 * at any time.
 * 
 * @author Tone Sommerland
 */
public final class IdRegistry
{
    /* Contexts associated with maps.
    Those maps contain identifiers associated with Registration objects. */
    private final Map<McContext, Map<String, Registration>>
    registry = new WeakHashMap<>();
    
    public IdRegistry()
    {
    }
    
    private Map<String, Registration> registrations(
            final McContext context, final boolean createIfAbsent)
    {
        var regs = registry.get(requireNonNull(context, "context"));
        if (regs != null)
        {
            return regs;
        }
        if (createIfAbsent)
        {
            regs = new HashMap<>();
            registry.put(context, regs);
            return regs;
        }
        return Map.of();
    }
    
    /**
     * Registers the identifier of a variable.
     * @param identifier the identifier of the variable
     * @param context the execution context
     * @param isValid
     * checks if the identifier is still valid in the context
     */
    public void registerVariable(
            final String identifier,
            final McContext context,
            final BiPredicate<? super String, ? super McContext> isValid)
    {
        registrations(context, true).put(
                identifier, new Registration(null, isValid));
    }
    
    /**
     * Registers the identifier of a function.
     * The variables are not registered by this method,
     * just stored for later retrieval.
     * @param identifier the identifier of the function
     * @param variables identifiers of the function's variables
     * @param context the execution context
     * @param isValid
     * checks if the identifier is still valid in the context
     */
    public void registerFunction(
            final String identifier,
            final Identifier[] variables,
            final McContext context,
            final BiPredicate<? super String, ? super McContext> isValid)
    {
        registrations(context, true).put(
                identifier, new Registration(variables.clone(), isValid));
    }
    
    /**
     * Unregisters an identifier.
     * @param identifier the identifier
     * @param context the execution context
     */
    public void unregister(
            final String identifier, final McContext context)
    {
        registrations(context, true)
            .remove(requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Returns the variables of a registered function.
     * @param identifier the identifier of the function
     * @param context the execution context
     * @throws IllegalArgumentException
     * if the identifier is not a registered function
     */
    public Identifier[] functionVariables(
            final String identifier, final McContext context)
    {
        if (!isRegisteredFunction(identifier, context))
        {
            throw new IllegalArgumentException(
                    "Not a registered function: identifier="
                            + identifier + ", context=" + context);
        }
        return registrations(context, false)
                .get(identifier).functionVariables.clone();
    }
    
    /**
     * Checks whether the given identifier is a registered
     * variable or function in the given context.
     */
    public boolean isRegistered(
            final String identifier, final McContext context)
    {
        return registrations(context, false)
                .containsKey(requireNonNull(identifier, "identifier"));
    }
    
    /**
     * Checks whether the given identifier is a registered
     * variable in the given context.
     */
    public boolean isRegisteredVariable(
            final String identifier, final McContext context)
    {
        return isRegistered(identifier, context)
                && registrations(context, false).get(identifier)
                    .functionVariables == null;
    }
    
    /**
     * Checks whether the given identifier is a registered
     * function in the given context.
     */
    public boolean isRegisteredFunction(
            final String identifier, final McContext context)
    {
        return isRegistered(identifier, context)
                && registrations(context, false).get(identifier)
                    .functionVariables != null;
    }
    
    /**
     * Clears the registry.
     */
    public void clear()
    {
        registry.clear();
    }
    
    /**
     * Validates the registrations of the given context against the context.
     * @throws ErrorMessage
     * if any identifier associated with the given context
     * is found to be invalid in the context
     */
    public void validate(final McContext context) throws ErrorMessage
    {
        final var regs = registrations(context, false);
        for (final String identifier : regs.keySet())
        {
            regs.get(identifier).validate(identifier, context);
        }
    }

    @Override
    public String toString()
    {
        return IdRegistry.class.getSimpleName() + ": " + registry;
    }
    
    /**
     * Registration of a variable or function.
     * (functionVariables is null if this is a variable registration.)
     */
    private static final class Registration
    {
        private final Identifier[] functionVariables;
        private final BiPredicate<? super String, ? super McContext> isValid;
        
        private Registration(
                final Identifier[] functionVariables,
                final BiPredicate<? super String, ? super McContext> isValid)
        {
            this.functionVariables = functionVariables;
            this.isValid = requireNonNull(isValid, "isValid");
        }

        private void validate(
                final String identifier,
                final McContext context) throws ErrorMessage
        {
            if (!isValid.test(identifier, context))
            {
                throw errorMessage(
                        ScriptMessages.identifierOverwritten(identifier));
            }
        }

        @Override
        public String toString()
        {
            return Registration.class.getSimpleName()
                    + " of a "
                    + functionVariables == null ? "variable" : "function";
        }
    }

}
