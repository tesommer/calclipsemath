package com.calclipse.mcomp.script;

import static java.util.Objects.requireNonNull;

import com.calclipse.mcomp.trace.Place;

/**
 * An identifier and its place in the script.
 * 
 * @author Tone Sommerland
 */
public final class Identifier
{
    private final String name;
    private final Place place;
    
    /**
     * Creates a new identifier.
     * @param name the name of the identifier
     * @param place the identifiers place in the script
     * @throws IllegalArgumentException if the name is empty
     */
    public Identifier(final String name, final Place place)
    {
        this.name = requireNonEmpty(name, "name");
        this.place = requireNonNull(place, "place");
    }
    
    private static String requireNonEmpty(
            final String arg, final String argName)
    {
        if (arg.isEmpty())
        {
            throw new IllegalArgumentException(argName + " is empty.");
        }
        return arg;
    }
    
    public String name()
    {
        return name;
    }
    
    public Place place()
    {
        return place;
    }

    @Override
    public String toString()
    {
        return Identifier.class.getSimpleName() + ' ' + name + " at " + place;
    }

}
