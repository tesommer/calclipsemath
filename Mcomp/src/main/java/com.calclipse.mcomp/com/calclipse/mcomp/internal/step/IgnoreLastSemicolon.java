package com.calclipse.mcomp.internal.step;

import static java.util.Objects.requireNonNull;

import java.util.function.Predicate;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;

/**
 * Discards the last token if it is a semicolon.
 * This will make the last semicolon optional.
 * 
 * @author Tone Sommerland
 */
public final class IgnoreLastSemicolon implements ParseStep
{
    private final Predicate<? super Token> isSemicolon;
    
    public IgnoreLastSemicolon(final Predicate<? super Token> isSemicolon)
    {
        this.isSemicolon = requireNonNull(isSemicolon, "isSemicolon");
    }

    @Override
    public void read(final TokenStream stream) throws ErrorMessage
    {
        if (!stream.buffer().isEmpty())
        {
            stream.write(stream.buffer().remove(0));
        }
        stream.buffer().add(stream.input());
        stream.skip();
    }

    @Override
    public void end(final TokenStream stream) throws ErrorMessage
    {
        if (!stream.buffer().isEmpty())
        {
            final Fragment last = stream.buffer().get(0);
            if (!isSemicolon.test(last.token()))
            {
                stream.write(last);
            }
        }
    }

}
