package com.calclipse.mcomp.script.internal;

import static java.util.Objects.requireNonNull;

import java.io.Reader;
import java.util.function.Supplier;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Script;
import com.calclipse.mcomp.script.ScriptParser;
import com.calclipse.mcomp.script.internal.parser.Parser;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;
import com.calclipse.mcomp.trace.Tracer;

public final class DefaultScriptParser implements ScriptParser
{
    private final Tracer tracer;
    private final String location;
    private final Place base;
    private final IdRegistry registry;
    private final Supplier<? extends RecursionGuard> recursionGuard;

    private DefaultScriptParser(
            final Tracer tracer,
            final String location,
            final Place base,
            final IdRegistry registry,
            final Supplier<? extends RecursionGuard> recursionGuard)
    {
        this.tracer = requireNonNull(tracer, "tracer");
        this.location = location;
        this.base = base;
        this.registry = registry;
        this.recursionGuard = requireNonNull(recursionGuard, "recursionGuard");
    }

    public DefaultScriptParser(final Tracer tracer)
    {
        this(
                tracer,
                null,
                Place.START,
                new IdRegistry(),
                () -> RecursionGuard.countingTo(21));
    }

    @Override
    public Script parse(final Reader input) throws ErrorMessage
    {
        try
        {
            final var parser = new Parser(input);
            parser.init(location, base, registry);
            return parser.scriptTrace().scriptName()
                    .map(name -> Script.named(
                            name,
                            parser.tasks(),
                            recursionGuard.get()))
                    .orElse(Script.nameless(
                            parser.tasks(),
                            recursionGuard.get()));
        }
        catch (final ErrorMessage ex)
        {
            throw tracer.track(
                    ex,
                    new Trace.Builder().setScriptLocation(location).build());
        }
    }

    @Override
    public ScriptParser trackingLocation(final String location)
    {
        return new DefaultScriptParser(
                this.tracer,
                location,
                this.base,
                this.registry,
                this.recursionGuard);
    }

    @Override
    public ScriptParser trackingRelativeTo(final Place base)
    {
        return new DefaultScriptParser(
                this.tracer,
                this.location,
                base,
                this.registry,
                this.recursionGuard);
    }

    @Override
    public ScriptParser declaringIdentifiersTo(final IdRegistry registry)
    {
        return new DefaultScriptParser(
                this.tracer,
                this.location,
                this.base,
                registry,
                this.recursionGuard);
    }

    @Override
    public ScriptParser limitingRecursionsWith(
            final Supplier<? extends RecursionGuard> recursionGuard)
    {
        return new DefaultScriptParser(
                this.tracer,
                this.location,
                this.base,
                this.registry,
                recursionGuard);
    }

}
