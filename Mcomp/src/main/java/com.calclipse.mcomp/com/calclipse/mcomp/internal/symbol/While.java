package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.EvalUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.jump.Break;
import com.calclipse.mcomp.internal.jump.Continue;

/**
 * While loop.
 * Executes the second argument for as long as the first argument is true.
 * Example {@code while x < 11 do x inc 2 end}
 * Returns the result of the last execution of the second argument,
 * or undef if it was never executed.
 * 
 * @author Tone Sommerland
 */
public final class While implements ConditionalOperation
{
    private static final String NAME = "while";
    
    private final TypeContext typeContext;

    private While(final TypeContext typeContext)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
    }
    
    public static Function function(
            final Delimitation delimitation, final TypeContext typeContext)
    {
        return Function.ofConditional(
                NAME, delimitation, new While(typeContext));
    }

    @Override
    public Value evaluate(final List<Expression> args) throws ErrorMessage
    {
        if (args.size() != 2)
        {
            throw errorMessage(EvalMessages.syntaxError());
        }
        Value result = Values.UNDEF;
        try
        {
            while (EvalUtil.evaluateBoolean(args.get(0), false, typeContext))
            {
                try
                {
                    EvalUtil.checkInterrupted(false);
                    result = args.get(1).evaluate(false);
                }
                catch (final Continue ex)
                {
                    ex.decrementJumps();
                }
            }
        }
        catch (final Break ex)
        {
            ex.decrementJumps();
        }
        return result;
    }

}
