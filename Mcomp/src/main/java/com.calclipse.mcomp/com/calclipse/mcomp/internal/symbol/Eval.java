package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.Arrays;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.math.parser.misc.ArgUtil;
import com.calclipse.mcomp.McContext;

/**
 * Evaluates a named operation or operand in the given context.
 * Does not support conditional operations.
 * Expected arguments is the symbol name and any operation arguments.
 * 
 * @author Tone Sommerland
 */
public final class Eval implements VariableArityOperation
{
    private static final String NAME = "eval";
    
    private final RecursionGuard recursionGuard;
    private final McContext context;

    private Eval(final RecursionGuard recursionGuard, final McContext context)
    {
        this.recursionGuard = requireNonNull(recursionGuard, "recursionGuard");
        this.context = requireNonNull(context, "context");
    }
    
    public static Function function(
            final Delimitation delimitation,
            final RecursionGuard recursionGuard,
            final McContext context)
    {
        return Function.of(
                NAME, delimitation, new Eval(recursionGuard, context));
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        try
        {
            recursionGuard.increment();
            ArgUtil.requireMinimumArgCount(1, args.length);
            final String symbol = args[0].get().toString();
            final Token token = context.symbol(symbol)
                    .orElseThrow(() -> invalidFirstArg(args.length > 1));
            final Value[] operationArgs = Arrays.copyOfRange(
                    args, 1, args.length);
            if (token.type().isOperand())
            {
                return evaluateOperand(token, operationArgs);
            }
            else if (token.type().isOperator() || token.type().isFunction())
            {
                return evaluateOperation(token, operationArgs);
            }
            throw invalidFirstArg(args.length > 1);
        }
        finally
        {
            recursionGuard.decrement();
        }
    }

    private static Value evaluateOperand(
            final Token token,
            final Value[] operationArgs) throws ErrorMessage
    {
        requireArgCount(token.name(), 0, operationArgs.length);
        return ((Operand)token).value();
    }

    private static Value evaluateOperation(
            final Token token,
            final Value[] operationArgs) throws ErrorMessage
    {
        final Operation operation = asOperation(token, operationArgs);
        try
        {
            return operation.evaluate();
        }
        catch (final ErrorMessage ex)
        {
            throw forSymbol(token.name(), ex);
        }
    }
    
    @FunctionalInterface
    private static interface Operation
    {
        public abstract Value evaluate() throws ErrorMessage;
    }
    
    private static Operation asOperation(
            final Token token,
            final Value[] operationArgs) throws ErrorMessage
    {
        if (token.type().isUnary())
        {
            requireArgCount(token.name(), 1, operationArgs.length);
            final var operation = (UnaryOperation)token;
            return () -> operation.evaluate(operationArgs[0]);
        }
        else if (token.type().isBinary())
        {
            requireArgCount(token.name(), 2, operationArgs.length);
            final var operation = (BinaryOperation)token;
            return () -> operation.evaluate(operationArgs[0], operationArgs[1]);
        }
        else if (token.type().isFunction())
        {
            final var func = (Function)token;
            if (!func.isConditional())
            {
                return () -> func.operation().evaluate(operationArgs);
            }
        }
        throw invalidFirstArg(operationArgs.length > 0);
    }
    
    private static void requireArgCount(
            final String symbol,
            final int required,
            final int argCount) throws ErrorMessage
    {
        try
        {
            ArgUtil.requireArgCount(required, argCount);
        }
        catch (final ErrorMessage ex)
        {
            throw forSymbol(symbol, ex);
        }
    }
    
    private static ErrorMessage forSymbol(
            final String symbol, final ErrorMessage ex)
    {
        return errorMessage(
                EvalMessages.messageFor(ex.getMessage(), symbol)).withCause(ex);
    }
    
    private static ErrorMessage invalidFirstArg(final boolean firstOfMany)
    {
        if (firstOfMany)
        {
            return ArgUtil.forArgument(1, ArgMessages.invalidArgument());
        }
        return errorMessage(ArgMessages.invalidArgument());
    }

}
