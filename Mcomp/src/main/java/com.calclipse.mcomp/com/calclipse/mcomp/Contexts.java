package com.calclipse.mcomp;

import java.util.function.Function;

import com.calclipse.math.parser.MathParserConfig;
import com.calclipse.mcomp.internal.Context;
import com.calclipse.mcomp.internal.config.ScriptConfig;

/**
 * An {@link com.calclipse.mcomp.McContext}-related utility class.
 * This class has methods for acquiring a context instance.
 * 
 * @author Tone Sommerland
 */
public final class Contexts
{
    /**
     * A parser configuration for scripts.
     */
    public static final Function<McContext, MathParserConfig>
    SCRIPT_CONFIG = ScriptConfig::new;

    private Contexts()
    {
    }

    /**
     * Returns a context instance with the
     * {@link #SCRIPT_CONFIG script parser-configuration}.
     */
    public static McContext getOne()
    {
        return new Context(SCRIPT_CONFIG, Function.identity());
    }

    /**
     * Returns a context instance.
     * @param config parser configuration
     */
    public static McContext configuredWith(
            final Function<? super McContext, ? extends MathParserConfig>
            config)
    {
        return new Context(config, Function.identity());
    }

    /**
     * Returns a context instance.
     * @param config parser configuration
     * @param mapping applied to the parent context and child contexts
     */
    public static McContext configuredWithAndMappedTo(
            final Function<? super McContext, ? extends MathParserConfig>
            config,
            final Function<? super McContext, ? extends McContext>
            mapping)
    {
        return mapping.apply(new Context(config, mapping));
    }

}
