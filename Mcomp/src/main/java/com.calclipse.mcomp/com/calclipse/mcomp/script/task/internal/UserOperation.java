package com.calclipse.mcomp.script.task.internal;

import static com.calclipse.math.parser.ParensAndComma.COMMA;
import static com.calclipse.math.parser.ParensAndComma.LPAREN;
import static com.calclipse.math.parser.ParensAndComma.RPAREN;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Trace;

/**
 * A user-defined parameterized operation.
 * 
 * @author Tone Sommerland
 */
public final class UserOperation implements VariableArityOperation
{
    private final ParameterizedExpression wrapped;
    private final String definition;
    private final McContext context;
    private final Trace trace;

    private UserOperation(
            final ParameterizedExpression wrapped,
            final String definition,
            final McContext context,
            final Trace trace)
    {
        this.wrapped = requireNonNull(wrapped, "wrapped");
        this.definition = requireNonNull(definition, "definition");
        this.context = requireNonNull(context, "context");
        this.trace = requireNonNull(trace, "trace");
    }
    
    /**
     * Returns a function with a user-operation in it.
     */
    public static Function inFunction(
            final String name,
            final Variable[] variables,
            final String definition,
            final McContext context,
            final Trace trace) throws ErrorMessage
    {
        final Expression expression = context.parse(definition);
        return Function.of(
                name,
                Delimitation.byTokensWithOpener(
                        LPAREN, RPAREN, COMMA, Token::identifiesAs),
                wrap(expression, variables, definition, context, trace));
    }
    
    /**
     * Returns the parameters of the user operation with the given name.
     * @param name the name of the function containing the user operation
     * @param context the context to look in
     * @throws java.util.NoSuchElementException
     * if the user operation didn't exist or the identifier was something else
     */
    public static Variable[] parameters(
            final String name, final McContext context)
    {
        return context.symbol(name)
            .filter(token -> token.type().isFunction())
            .map(Function.class::cast)
            .filter(function -> !function.isConditional())
            .map(Function::operation)
            .filter(UserOperation.class::isInstance)
            .map(UserOperation.class::cast)
            .map(user -> user.wrapped.parameters())
            .orElseThrow();
    }
    
    private static VariableArityOperation wrap(
            final Expression expression,
            final Variable[] variables,
            final String definition,
            final McContext context,
            final Trace trace)
    {
        return new UserOperation(
                new ParameterizedExpression(expression, variables),
                definition,
                context,
                trace);
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        try
        {
            return wrapped.evaluate(args);
        }
        catch (final ErrorMessage ex)
        {
            throw ex.detail()
                .map(fragment -> Pinpoint.at(definition, fragment))
                .map(pinpoint -> trace.builder().setPinpoint(pinpoint).build())
                .map(trace -> context.tracer().track(ex, trace))
                .orElse(ex);
        }
    }

    @Override
    public String toString()
    {
        return UserOperation.class.getSimpleName() + ": " + definition;
    }

}
