package com.calclipse.mcomp.internal.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.mcomp.McContext;

/**
 * Prints the string representation of its argument
 * to the standard output stream of the environment.
 * Supported types:
 * <ul>
 *  <li>*: returns undef</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Print implements UnaryOperation
{
    private static final String NAME = "print";
    
    private final McContext context;

    private Print(final McContext context)
    {
        this.context = requireNonNull(context, "context");
    }
    
    public static UnaryOperator operator(final McContext context)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.VERY_LOW, new Print(context));
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        context.environment().out().println(arg.get());
        return Values.UNDEF;
    }

}
