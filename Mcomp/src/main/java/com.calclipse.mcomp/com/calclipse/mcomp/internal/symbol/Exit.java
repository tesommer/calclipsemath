package com.calclipse.mcomp.internal.symbol;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.TypeUtil;
import com.calclipse.mcomp.McContext;

/**
 * Exits the environment.
 * Supported types:
 * <ul>
 *  <li>integer: exit status code</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Exit implements UnaryOperation
{
    private static final String NAME = "exit";
    
    private final McContext context;

    private Exit(final McContext context)
    {
        this.context = requireNonNull(context, "context");
    }
    
    public static UnaryOperator operator(final McContext context)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, new Exit(context));
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final int status = TypeUtil.toInt(arg.get());
        context.environment().exit(status);
        return Values.UNDEF;
    }

}
