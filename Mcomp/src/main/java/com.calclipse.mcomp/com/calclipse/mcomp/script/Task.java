package com.calclipse.mcomp.script;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.trace.Trace;

/**
 * A script task.
 * 
 * @author Tone Sommerland
 */
public abstract class Task
{
    private final Trace scriptTrace;
    
    /**
     * Creates a new task.
     * @param scriptTrace the trace of the script that this task belongs to
     */
    protected Task(final Trace scriptTrace)
    {
        this.scriptTrace = requireNonNull(scriptTrace, "scriptTrace");
    }
    
    /**
     * The trace of the script that this task belongs to.
     * The place and pinpoint properties of this trace are absent.
     */
    protected final Trace scriptTrace()
    {
        return scriptTrace;
    }

    /**
     * Executes this task.
     */
    public abstract void execute(McContext context) throws ErrorMessage;
    
    /**
     * Some tasks can be skipped if all
     * compilable tasks in the script are compiled.
     * If this is the case, then this method may return {@code true}.
     */
    public abstract boolean isOneTimeCompilationStep();

    @Override
    public String toString()
    {
        return getClass().getSimpleName() + ": scriptTrace=" + scriptTrace;
    }

}
