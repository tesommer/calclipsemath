package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.List;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.misc.EvalUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.internal.IfDelimitation;

/**
 * If test.
 * Example: {@code if x < 0 then x + 1 elsif x < 2 then x + 2 else x + 3 end}
 * Returns {@code undef} if none of the test conditions are fulfilled.
 * 
 * @author Tone Sommerland
 */
public final class If implements ConditionalOperation
{
    private static final String NAME = "if";

    private final TypeContext typeContext;
    private final IfDelimitation clauses;
    private boolean isValid;

    private If(final TypeContext typeContext, final IfDelimitation clauses)
    {
        this.typeContext = requireNonNull(typeContext, "typeContext");
        this.clauses = requireNonNull(clauses, "clauses");
    }
    
    public static Function function(
            final IfDelimitation delimitation, final TypeContext typeContext)
    {
        return Function.ofConditional(
                NAME, delimitation, new If(typeContext, delimitation));
    }
    
    /**
     * Returns this operation, but with the specified clauses.
     */
    public If withClauses(final IfDelimitation clauses)
    {
        return new If(this.typeContext, clauses);
    }

    @Override
    public Value evaluate(final List<Expression> args) throws ErrorMessage
    {
        if (!isValid)
        {
            isValid = clauses.isValid();
            if (!isValid)
            {
                throw errorMessage(EvalMessages.syntaxError());
            }
        }
        if (EvalUtil.evaluateBoolean(args.get(0), false, typeContext))
        {
            return args.get(1).evaluate(false);
        }
        int i = 1;
        while (i < clauses.countClauses())
        {
            if (clauses.isElseIf(i))
            {
                if (EvalUtil.evaluateBoolean(
                        args.get(i + 1), false, typeContext))
                {
                    return args.get(i + 2).evaluate(false);
                }
                i++;
            }
            else
            {
                return args.get(i + 1).evaluate(false);
            }
            i++;
        }
        return Values.UNDEF;
    }

}
