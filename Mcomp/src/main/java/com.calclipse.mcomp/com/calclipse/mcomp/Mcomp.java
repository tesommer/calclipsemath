package com.calclipse.mcomp;

import com.calclipse.math.expression.ErrorMessage;

/**
 * A scriptable math component.
 * 
 * @author Tone Sommerland
 */
@FunctionalInterface
public interface Mcomp
{
    /**
     * Executes this math component in the given context.
     */
    public abstract void execute(McContext context) throws ErrorMessage;
    
    /**
     * The name of this math component
     * will be used as a namespace for exported symbols.
     * @return the empty string if this mcomp doesn't have a name
     * 
     * @see McContext#exportSymbol(com.calclipse.math.expression.Token)
     * 
     * @implSpec
     * Returns the empty string.
     */
    public default String name()
    {
        return "";
    }

}
