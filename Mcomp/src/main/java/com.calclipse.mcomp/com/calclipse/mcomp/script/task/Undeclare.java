package com.calclipse.mcomp.script.task;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.trace.Trace;

/**
 * Removes a previously declared variable or function.
 * 
 * @author Tone Sommerland
 */
public final class Undeclare extends Task
{
    private final Identifier identifier;
    private final IdRegistry registry;
    
    public Undeclare(
            final Trace scriptTrace,
            final Identifier identifier,
            final IdRegistry registry)
    {
        super(scriptTrace);
        this.identifier = requireNonNull(identifier, "identifier");
        this.registry = requireNonNull(registry, "registry");
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        if (registry.isRegistered(identifier.name(), context))
        {
            registry.unregister(identifier.name(), context);
            context.removeSymbol(identifier.name());
        }
        else
        {
            throw undeclaredIdentifier(context);
        }
    }
    
    @Override
    public boolean isOneTimeCompilationStep()
    {
        return true;
    }
    
    private ErrorMessage undeclaredIdentifier(final McContext context)
    {
        return context.tracer().track(
                ScriptMessages.undeclaredIdentifier(identifier.name()),
                scriptTrace().builder()
                    .setPlace(identifier.place())
                    .build());
    }

}
