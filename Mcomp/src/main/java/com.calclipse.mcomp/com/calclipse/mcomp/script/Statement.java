package com.calclipse.mcomp.script;

import static java.util.Objects.requireNonNull;

import com.calclipse.mcomp.trace.Place;

/**
 * An expression and its place in the script.
 * 
 * @author Tone Sommerland
 */
public final class Statement
{
    private final String expression;
    private final Place place;
    
    public Statement(final String expression, final Place place)
    {
        this.expression = requireNonNull(expression, "expression");
        this.place = requireNonNull(place, "place");
    }
    
    public String expression()
    {
        return expression;
    }
    
    public Place place()
    {
        return place;
    }

    @Override
    public String toString()
    {
        return Statement.class.getSimpleName()
                + " at " + place + ':' + expression;
    }

}
