package com.calclipse.mcomp.script.task;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Compilable;
import com.calclipse.mcomp.script.Statement;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Trace;

/**
 * A task that executes a statement.
 * 
 * @author Tone Sommerland
 */
public final class Execute extends Task implements Compilable
{
    private final Statement statement;
    private Expression expression;
    
    public Execute(final Trace scriptTrace, final Statement statement)
    {
        super(scriptTrace);
        this.statement = requireNonNull(statement, "statement");
    }
    
    @Override
    public boolean isCompiled()
    {
        return expression != null;
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        try
        {
            if (expression == null)
            {
                expression = context.parse(statement.expression());
            }
            expression.evaluate(true);
        }
        catch (final ErrorMessage ex)
        {
            throw context.tracer().track(ex, trace(ex));
        }
    }
    
    private Trace trace(final ErrorMessage ex)
    {
        return ex.detail()
                .map(fragment -> scriptTrace().builder()
                        .setPlace(statement.place())
                        .setPinpoint(pinpointAt(fragment))
                        .build())
                .orElse(scriptTrace());
    }
    
    private Pinpoint pinpointAt(final Fragment fragment)
    {
        return Pinpoint.at(statement.expression(), fragment);
    }

    @Override
    public boolean isOneTimeCompilationStep()
    {
        return false;
    }

}
