package com.calclipse.mcomp.script;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;

/**
 * An executable list of tasks.
 * 
 * @author Tone Sommerland
 */
public final class TaskList
{
    private final Collection<Task> tasks = new ArrayList<>();
    private final Collection<Compilable> compilables = new ArrayList<>();
    
    public TaskList()
    {
    }
    
    /**
     * Adds a task to the task list.
     */
    public void add(final Task task)
    {
        tasks.add(requireNonNull(task, "task"));
        if (task instanceof Compilable)
        {
            compilables.add((Compilable)task);
        }
    }
    
    Mcomp executor()
    {
        return this::execute;
    }
    
    /**
     * Executes the task list.
     */
    private void execute(final McContext context) throws ErrorMessage
    {
        requireNonNull(context, "context");
        final boolean executeAll = isNotOptimizable();
        for (final Task task : tasks)
        {
            if (executeAll || !task.isOneTimeCompilationStep())
            {
                task.execute(context);
            }
        }
    }
    
    private boolean isNotOptimizable()
    {
        if (compilables.isEmpty())
        {
            return true;
        }
        return compilables.stream()
                .anyMatch(compilable -> !compilable.isCompiled());
    }

}
