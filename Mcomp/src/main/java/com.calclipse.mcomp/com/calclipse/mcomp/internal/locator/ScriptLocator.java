package com.calclipse.mcomp.internal.locator;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.calclipse.lib.resource.Resource;
import com.calclipse.lib.resource.Resources;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.Script;
import com.calclipse.mcomp.script.ScriptParser;
import com.calclipse.mcomp.trace.Tracer;

/**
 * This locator parses locations with the format {@code script:path/file},
 * where the path is relative to the module path.
 * This locator caches a script the first time it is requested,
 * and then returns the cached script on subsequent requests for that script.
 * 
 * @author Tone Sommerland
 */
public final class ScriptLocator implements McLocator
{
    private final Tracer tracer;
    private final String prefix;
    
    private final Map<String, Script> cache = new HashMap<>();
    
    public ScriptLocator(final Tracer tracer, final String prefix)
    {
        this.tracer = requireNonNull(tracer, "tracer");
        this.prefix = requireNonNull(prefix, "prefix");
    }

    @Override
    public boolean accepts(final String location, final String referrer)
    {
        return location.startsWith(prefix);
    }

    @Override
    public Mcomp find(final String location, final String referrer)
        throws ErrorMessage
    {
        final Script cachedScript = cache.get(location);
        if (cachedScript == null)
        {
            return loadAndCacheScript(location);
        }
        return cachedScript;
    }

    private Script loadAndCacheScript(final String location)
            throws ErrorMessage
    {
        final Script newScript = loadScript(location);
        if (newScript == null)
        {
            return null;
        }
        cache.put(location, newScript);
        return newScript;
    }
    
    private Script loadScript(final String location) throws ErrorMessage
    {
        final Optional<InputStream> first = findFirst(location);
        if (!first.isPresent())
        {
            return null;
        }
        try (final var input = new InputStreamReader(first.get()))
        {
            return ScriptParser.trackingErrorsWith(tracer)
                    .trackingLocation(location)
                    .parse(input);
        }
        catch (final IOException ex)
        {
            throw errorMessage(ex.toString()).withCause(ex);
        }
    }
    
    private Optional<InputStream> findFirst(final String location)
            throws ErrorMessage
    {
        final String resourceName = '/' + location.substring(prefix.length());
        final Resource resource
            = Resources.findOnModuleOrClassPath(resourceName);
        try
        {
            return resource.next();
        }
        catch (final IOException ex)
        {
            throw errorMessage(ex.toString()).withCause(ex);
        }
    }

}
