package com.calclipse.mcomp.internal.jump;

/**
 * Thrown to break a loop.
 * 
 * @author Tone Sommerland
 */
public final class Break extends LoopJumper
{
    private static final long serialVersionUID = 1L;
    
    private static final class Builder extends LoopJumper.Builder
    {
        private Builder(final int jumps)
        {
            super(jumps);
        }

        private Builder(final Break ex)
        {
            super(ex);
        }

        @Override
        protected Break build()
        {
            return new Break(this);
        }
    }

    private Break(final Builder builder)
    {
        super(builder);
    }
    
    /**
     * Returns a break statement that makes the specified number of jumps.
     * @param jumps the number of nested loops to break
     * @throws IllegalArgumentException if the number of jumps is less than one
     */
    public static Break ofJumps(final int jumps)
    {
        return new Builder(jumps).build();
    }

    @Override
    protected Builder builder()
    {
        return new Builder(this);
    }
    
}
