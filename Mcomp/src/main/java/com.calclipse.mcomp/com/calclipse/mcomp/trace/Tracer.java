package com.calclipse.mcomp.trace;

import java.util.List;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.trace.internal.DefaultTracer;

/**
 * Responsible for tracking errors in scripts.
 * In a typical scenario script A executes script B,
 * an error emerges in script B,
 * script B then throws an exception from a tracer,
 * and script A does the same when it catches the exception.
 * The exception thrown by script A will then contain a message
 * that might help the user correct the mistake that caused the error.
 * The tracer may also be able to retrieve a trace stack from the exception.
 * 
 * @author Tone Sommerland
 */
public interface Tracer
{
    /**
     * Returns the default tracer.
     */
    public static Tracer standard()
    {
        return DefaultTracer.INSTANCE;
    }
    
    /**
     * Returns the default tracer with some extra error-message options.
     * @param messageFormat
     * a format string with one parameter: the error message
     * @param scriptNameFormat
     * a format string with one parameter: the script name
     * @param scriptLocationFormat
     * a format string with one parameter: the script location
     * @param placeFormat
     * a format string with two parameters: the line and column numbers
     * @param causeFormat
     * a format string with two parameters:
     * the name of the token in the pinpoint's fragment, and a code extract
     */
    public static Tracer composition(
            final String messageFormat,
            final String scriptNameFormat,
            final String scriptLocationFormat,
            final String placeFormat,
            final String causeFormat)
    {
        return new DefaultTracer(
                messageFormat,
                scriptNameFormat,
                scriptLocationFormat,
                placeFormat,
                causeFormat);
    }
    
    /**
     * Returns an error message that contains the given message,
     * and that can be tracked by this tracer.
     * @param message the error message
     * @param trace the next trace to be added to the trace stack
     */
    public abstract ErrorMessage track(String message, Trace trace);
    
    /**
     * Returns an error message with the given cause
     * that can be tracked by this tracer.
     * @param cause the cause of the exception to return
     * @param trace the next trace to be added to the trace stack
     */
    public abstract ErrorMessage track(Throwable cause, Trace trace);
    
    /**
     * If the trace information is not available,
     * this method may return an empty list.
     * The trace at index zero is closest to where the error first occurred.
     */
    public abstract List<Trace> trail(ErrorMessage ex);

}
