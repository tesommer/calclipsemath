package com.calclipse.mcomp.internal.symbol;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.misc.EvalUtil;
import com.calclipse.math.parser.misc.TypeUtil;

/**
 * Sleeps for a specified number of milliseconds.
 * Supported types:
 * <ul>
 *  <li>integer: millis to sleep</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Sleep implements UnaryOperation
{
    private static final String NAME = "sleep";
    
    private static final UnaryOperation INSTANCE = new Sleep();

    private Sleep()
    {
    }
    
    public static UnaryOperator operator()
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.MEDIUM, INSTANCE);
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final long millis = TypeUtil.toLong(arg.get());
        if (millis  < 0)
        {
            throw errorMessage(EvalMessages.domainError());
        }
        try
        {
            Thread.sleep(millis);
        }
        catch (final InterruptedException ex)
        {
            throw EvalUtil.interrupt(ex);
        }
        return Values.UNDEF;
    }

}
