package com.calclipse.mcomp.script.task.internal;

import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.misc.ArgUtil;

/**
 * A generic operation created from an expression
 * containing zero or more free variables.
 * When evaluating this operation,
 * the parameters are temporarily assigned the arguments of the evaluation.
 * Afterwards the parameters are restored to their old values.
 * 
 * @author Tone Sommerland
 */
public final class ParameterizedExpression implements VariableArityOperation
{
    private final Expression expression;
    private final Variable[] parameters;
    
    /**
     * Creates a new parameterized expression.
     * @param expression an expression containing zero or more parameters
     * @param parameters the parameters
     */
    ParameterizedExpression(
            final Expression expression, final Variable[] parameters)
    {
        this.expression = requireNonNull(expression, "expression");
        this.parameters = parameters.clone();
    }

    Variable[] parameters()
    {
        return parameters.clone();
    }

    @Override
    public Value evaluate(final Value... args) throws ErrorMessage
    {
        ArgUtil.requireArgCount(parameters.length, args.length);
        final Object[] oldParamVals = copyAndAssignParameters(args);
        try
        {
            return Value.constantOf(expression.evaluate(true).get());
        }
        finally
        {
            restoreParameters(oldParamVals);
        }
    }

    private Object[] copyAndAssignParameters(final Value... args)
    {
        final var oldParamVals = new Object[parameters.length];
        for (int i = 0; i < oldParamVals.length; i++)
        {
            oldParamVals[i] = parameters[i].get();
            parameters[i].set(args[i].get());
        }
        return oldParamVals;
    }

    private void restoreParameters(final Object[] oldParamVals)
    {
        for (int i = 0; i < oldParamVals.length; i++)
        {
            parameters[i].set(oldParamVals[i]);
        }
    }

    @Override
    public String toString()
    {
        return ParameterizedExpression.class.getSimpleName()
                + " of " + parameters.length + " parameters: " + expression;
    }

}
