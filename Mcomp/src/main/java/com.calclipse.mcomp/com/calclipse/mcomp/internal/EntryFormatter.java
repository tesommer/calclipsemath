package com.calclipse.mcomp.internal;

import static java.util.Objects.requireNonNull;

import java.util.IllegalFormatException;
import java.util.Locale;
import java.util.function.DoubleFunction;

import com.calclipse.math.matrix.MatrixStringifier;

/**
 * A matrix-entry stringifier that applies a format to the elements.
 * 
 * @author Tone Sommerland
 */
public final class EntryFormatter implements DoubleFunction<String>
{
    private static final String FALLBACK_ENTRY_STRING = "err";

    private final MatrixStringifier wrapped;
    private final String format;
    private final Locale locale;
    
    private EntryFormatter(
            final MatrixStringifier wrapped,
            final String format,
            final Locale locale)
    {
        this.wrapped = requireNonNull(wrapped, "wrapped");
        this.format = requireNonNull(format, "format");
        this.locale = locale;
    }
    
    /**
     * Wraps a matrix stringifer with an entry formatter.
     * {@link #unwrap(MatrixStringifier)}
     * returns the stringifier given to this method
     * when given the stringifier returned by this method.
     * @param stringifier
     * the stringifier wrapped by the returned instance's entry stringifier
     * @param format the entry format
     */
    public static MatrixStringifier wrap(
            final MatrixStringifier stringifier, final String format)
    {
        return MatrixStringifier.composedOf(
                stringifier.arrayStringifier(),
                new EntryFormatter(stringifier, format, null));
    }
    
    /**
     * Whether or not the given stringifier
     * {@link #wrap(MatrixStringifier, String) wraps}
     * an instance of this class.
     */
    public static boolean isWrappedIn(final MatrixStringifier stringifier)
    {
        return stringifier.entryStringifier()
                instanceof EntryFormatter;
    }
    
    /**
     * Unwraps a previously
     * {@link #wrap(MatrixStringifier, String) wrapped}
     * stringifier.
     * @throws IllegalArgumentException
     * if {@code wrapper} does not wrap an instance of this class
     */
    public static MatrixStringifier unwrap(final MatrixStringifier wrapper)
    {
        return requireWrapper(wrapper).wrapped;
    }
    
    /**
     * Re-wraps an already
     * {@link #wrap(MatrixStringifier, String) wrapped}
     * stringifier with the given locale.
     * @param locale the locale
     * @throws IllegalArgumentException
     * if {@code wrapper} does not wrap an instance of this class
     */
    public static MatrixStringifier reWrapWithLocale(
            final MatrixStringifier wrapper, final Locale locale)
    {
        final EntryFormatter wrapping = requireWrapper(wrapper);
        return MatrixStringifier.composedOf(
                wrapping.wrapped.arrayStringifier(),
                new EntryFormatter(
                        wrapping.wrapped,
                        wrapping.format,
                        requireNonNull(locale, "locale")));
    }
    
    private static EntryFormatter requireWrapper(
            final MatrixStringifier stringifier)
    {
        if (!isWrappedIn(stringifier))
        {
            throw new IllegalArgumentException(
                    "Does not wrap "
                    + EntryFormatter.class.getSimpleName()
                    + ":"
                    + stringifier);
        }
        return (EntryFormatter)stringifier.entryStringifier();
    }

    @Override
    public String apply(final double value)
    {
        try
        {
            if (locale == null)
            {
                return String.format(format, value);
            }
            return String.format(locale, format, value);
        }
        catch (final IllegalFormatException ex)
        {
            return FALLBACK_ENTRY_STRING;
        }
    }

}
