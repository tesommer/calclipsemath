package com.calclipse.mcomp.internal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Token;
import com.calclipse.math.parser.MathParser;
import com.calclipse.math.parser.MathParserConfig;
import com.calclipse.math.parser.config.Configs;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

public final class Context implements McContext
{
    private final Collection<Token> xports = new ArrayList<>();
    private final Map<String, Token> imports = new HashMap<>();
    private final McContext parent;
    private final Collection<McLocator> locators;
    
    private final Function<? super McContext, ? extends MathParserConfig>
    config;
    
    private final Function<? super McContext, ? extends McContext>
    mapping;
    
    private MathParser parser;
    
    /**
     * The locators collection is stored internally
     * because child contexts share the locators.
     */
    private Context(
            final McContext parent,
            final Collection<McLocator> locators,
            final Function<? super McContext, ? extends MathParserConfig>
            config,
            final Function<? super McContext, ? extends McContext>
            mapping)
    {
        assert locators != null;
        assert config   != null;
        assert mapping  != null;
        this.parent   = parent;
        this.locators = locators;
        this.config   = config;
        this.mapping  = mapping;
    }
    
    public Context(
            final Function<? super McContext, ? extends MathParserConfig>
            config,
            final Function<? super McContext, ? extends McContext>
            mapping)
    {
        this(
                null,
                new ArrayList<>(),
                requireNonNull(config,  "config"),
                requireNonNull(mapping, "mapping"));
    }
    
    private MathParser theParser()
    {
        if (parser == null)
        {
            parser = new MathParser(config.apply(this));
        }
        return parser;
    }

    @Override
    public Optional<McContext> parent()
    {
        return Optional.ofNullable(this.parent);
    }

    @Override
    public Expression parse(final String expression) throws ErrorMessage
    {
        return theParser().parse(expression);
    }

    @Override
    public boolean addSymbol(final Token symbol)
    {
        return theParser().addSymbol(symbol);
    }

    @Override
    public boolean removeSymbol(final String name)
    {
        return theParser().removeSymbol(name);
    }

    @Override
    public Optional<Token> symbol(final String name)
    {
        return theParser().symbol(name);
    }

    @Override
    public Collection<Token> symbols()
    {
        return theParser().symbols();
    }

    @Override
    public Collection<Token> lookUpSymbols(final char firstChar)
    {
        return theParser().lookUpSymbols(firstChar);
    }

    @Override
    public Optional<Mcomp> mcompAt(
            final String location, final String referrer) throws ErrorMessage
    {
        for (final McLocator locator : locators)
        {
            if (locator.accepts(location, referrer))
            {
                final Mcomp mcomp = locator.find(location, referrer);
                if (mcomp != null)
                {
                    return Optional.of(mcomp);
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public void exportSymbol(final Token symbol) throws ErrorMessage
    {
        xports.add(symbol);
    }

    @Override
    public void importMcomp(final Mcomp mcomp) throws ErrorMessage
    {
        final var child = new Context(this, locators, config, mapping);
        final McContext mappedChild = mapping.apply(child);
        mcomp.execute(mappedChild);
        child.xports.forEach(
                symbol -> importSymbol(mcomp, mappedChild, symbol));
    }
    
    private void importSymbol(
            final Mcomp exporter,
            final McContext exportedTo,
            final Token symbol)
    {
        final Token imported = symbol.withName(
                exporter.name()
                + exportedTo.namespaceSeparator()
                + symbol.name());
        theParser().removeSymbol(imported.name());
        theParser().addSymbol(imported);
        imports.put(imported.name(), imported);
    }

    @Override
    public Collection<Token> importedSymbols()
    {
        return Collections.unmodifiableCollection(imports.values());
    }

    @Override
    public void addLocator(final McLocator locator)
    {
        locators.add(requireNonNull(locator, "locator"));
    }

    @Override
    public McLocator[] clearLocators()
    {
        final McLocator[] removed = locators.stream().toArray(McLocator[]::new);
        locators.clear();
        return removed;
    }

    @Override
    public TypeContext typeContext()
    {
        return Configs.arbitraryPrecisionTypeContext();
    }

}
