package com.calclipse.mcomp.internal.config;

import static java.util.Objects.requireNonNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.calclipse.math.Real;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.config.ArbitraryPrecisionConfig;
import com.calclipse.math.parser.misc.MathUtil;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.internal.IfDelimitation;
import com.calclipse.mcomp.internal.step.IgnoreLastSemicolon;
import com.calclipse.mcomp.internal.symbol.Convertf;
import com.calclipse.mcomp.internal.symbol.Eval;
import com.calclipse.mcomp.internal.symbol.Exit;
import com.calclipse.mcomp.internal.symbol.For;
import com.calclipse.mcomp.internal.symbol.If;
import com.calclipse.mcomp.internal.symbol.Locale;
import com.calclipse.mcomp.internal.symbol.LoopControl;
import com.calclipse.mcomp.internal.symbol.Print;
import com.calclipse.mcomp.internal.symbol.Printf;
import com.calclipse.mcomp.internal.symbol.Prompt;
import com.calclipse.mcomp.internal.symbol.Raise;
import com.calclipse.mcomp.internal.symbol.Repeat;
import com.calclipse.mcomp.internal.symbol.Return;
import com.calclipse.mcomp.internal.symbol.Semicolon;
import com.calclipse.mcomp.internal.symbol.Sleep;
import com.calclipse.mcomp.internal.symbol.Sprintf;
import com.calclipse.mcomp.internal.symbol.Sprompt;
import com.calclipse.mcomp.internal.symbol.Time;
import com.calclipse.mcomp.internal.symbol.Try;
import com.calclipse.mcomp.internal.symbol.While;

/**
 * This math-parser configuration is based on the
 * {@link com.calclipse.math.parser.config.ArbitraryPrecisionConfig
 * standard config with arbitrary-precision arithmetic}.
 * The config is extended with tokens that allow conditional execution,
 * output to stdout, and more.
 * It also overrides {@code convertf} for
 * {@link com.calclipse.math.Real} format argument.
 * 
 * @author Tone Sommerland
 */
public final class ScriptConfig extends ArbitraryPrecisionConfig{
    
    private static final int RECURSION_LIMIT = 26;
    
    private static final BinaryOperation CONVERTF_OVERRIDE = (left, right) ->
    {
        final Object fArg = left.get();
        if (fArg instanceof Real)
        {
            final var real = (Real)fArg;
            final String convertTo = right.get().toString();
            if (Convertf.NUMERIC_INTEGRAL.equals(convertTo))
            {
                final Optional<BigInteger> bigInt = MathUtil
                        .toBigIntegerExact(real);
                if (bigInt.isPresent())
                {
                    return Value.constantOf(bigInt.get());
                }
            }
            else if (Convertf.NUMERIC_FLOATING_POINT.equals(convertTo))
            {
                if (!real.isFractured())
                {
                    return Value.constantOf(real.bigDecimal());
                }
                try
                {
                    return Value.constantOf(real.unfracture().bigDecimal());
                }
                catch (final ArithmeticException ex)
                {
                    return Values.YIELD;
                }
            }
        }
        return Values.YIELD;
    };

    private static final Token DO      = Tokens.plain("do");
    private static final Token THEN    = Tokens.plain("then");
    private static final Token UNTIL   = Tokens.plain("until");
    private static final Token ELSE    = Tokens.plain("else");
    private static final Token ELSE_IF = Tokens.plain("elsif");
    private static final Token END     = Tokens.plain("end");
    
    private static final Delimitation
    DO_END_DELIMITATION = Delimitation.byTokensWithoutOpener(
            END, DO, Token::identifiesAs);
    
    private static final Delimitation
    UNTIL_END_DELIMITATION = Delimitation.byTokensWithoutOpener(
            END, UNTIL, Token::identifiesAs);
    
    private static final IfDelimitation
    IF_DELIMITATION = IfDelimitation.by(THEN, ELSE_IF, ELSE, END);
    
    private static final Token SEMICOLON = Semicolon.operator();
    
    /**
     * This step is interjected between the sign and check steps.
     */
    private static final ParseStep
    IGNORE_LAST_SEMICOLON = new IgnoreLastSemicolon(SEMICOLON::identifiesAs);
    
    private final McContext context;

    public ScriptConfig(final McContext context)
    {
        this.context = requireNonNull(context, "context");
    }

    @Override
    protected TypeContext typeContext()
    {
        return context.typeContext();
    }

    @Override
    protected void populateSymbolTable(final SymbolTable symbolTable)
    {
        super.populateSymbolTable(symbolTable);
        
        symbolTable.add(DO);
        symbolTable.add(THEN);
        symbolTable.add(UNTIL);
        symbolTable.add(ELSE);
        symbolTable.add(ELSE_IF);
        symbolTable.add(END);
        
        symbolTable.add(SEMICOLON);
        
        symbolTable.add(Return
                .function(ParensAndComma.DELIMITATION));
        symbolTable.add(LoopControl
                .breakFunction(ParensAndComma.DELIMITATION));
        symbolTable.add(LoopControl
                .continueFunction(ParensAndComma.DELIMITATION));
        symbolTable.add(While
                .function(DO_END_DELIMITATION, theTypeContext()));
        symbolTable.add(Repeat
                .function(UNTIL_END_DELIMITATION, theTypeContext()));
        symbolTable.add(For
                .function(DO_END_DELIMITATION, theTypeContext()));
        symbolTable.add(If
                .function(IF_DELIMITATION, theTypeContext()));
        symbolTable.add(Sleep
                .operator());
        symbolTable.add(Try
                .function(ParensAndComma.DELIMITATION, theTypeContext()));
        symbolTable.add(Raise
                .operator());
        symbolTable.add(Eval
                .function(
                        ParensAndComma.DELIMITATION,
                        RecursionGuard.countingTo(RECURSION_LIMIT),
                        context));
        symbolTable.add(Time
                .operand(theTypeContext()));
        symbolTable.add(Locale
                .function(ParensAndComma.DELIMITATION));
        
        symbolTable.add(Print.operator(context));
        symbolTable.add(Prompt.operator(context));
        symbolTable.add(Sprompt.operator(context));
        symbolTable.add(Exit.operator(context));
        final BinaryOperator convertf = Convertf.operator(theTypeContext());
        symbolTable.add(convertf.withOperation(
                CONVERTF_OVERRIDE.override(convertf.operation())));
        symbolTable.add(Printf
                .function(ParensAndComma.DELIMITATION, context));
        symbolTable.add(Sprintf
                .function(ParensAndComma.DELIMITATION));
    }

    @Override
    public ParseStep[] steps()
    {
        final var steps = new ArrayList<ParseStep>(List.of(super.steps()));
        steps.add(4, IGNORE_LAST_SEMICOLON);
        return steps.stream().toArray(ParseStep[]::new);
    }

}
