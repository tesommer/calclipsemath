package com.calclipse.mcomp.trace.internal;

import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Place;

/**
 * Code extraction.
 * 
 * @author Tone Sommerland
 */
public final class Extraction
{
    private final Place place;
    private final String extract;
    
    /**
     * Makes a code extraction.
     * @param base where the code begins
     * @param pinpoint specifies the exact position in the code
     * where the extraction will be made
     */
    Extraction(final Place base, final Pinpoint pinpoint)
    {
        final int[] lineColStart = lineColumnStart(pinpoint);
        final int line = lineColStart[0];
        final int column = lineColStart[1];
        final int start = lineColStart[2];
        final int end = end(pinpoint);
        place = Place.atLineColumn(line, column).relativeTo(base);
        extract = pinpoint.expression().substring(start, end).trim();
    }
    
    /**
     * The exact place in the script this extraction was taken from.
     */
    Place exactPlace()
    {
        return place;
    }
    
    /**
     * The extracted code.
     */
    String extract()
    {
        return extract;
    }
    
    /**
     * Finds the line, column and start index of the extract.
     * The line and column are one-based, the start zero-based.
     */
    private static int[] lineColumnStart(final Pinpoint pinpoint)
    {
        int line = 1;
        int column = 1;
        int start = 0;
        char last = '0';
        final String expression = pinpoint.expression();
        for (int i = 0; i < pinpoint.position(); i++)
        {
            final char c = expression.charAt(i);
            if (c == '\r' || c == '\n')
            {
                if ((last != '\r' && last != '\n') || last == c)
                {
                    start = i;
                    line++;
                    column = 1;
                    last = c;
                }
                else
                {
                    last = '0';
                }
            }
            else
            {
                column++;
                last = c;
            }
        }
        return new int[] {line, column, start};
    }
    
    /**
     * Finds the end index of the extract.
     */
    private static int end(final Pinpoint pinpoint)
    {
        final String expression = pinpoint.expression();
        int end = pinpoint.position() + pinpoint.token().length();
        while (end < expression.length()
                && expression.charAt(end) != '\r'
                && expression.charAt(end) != '\n')
        {
            end++;
        }
        return end;
    }

}
