package com.calclipse.mcomp;

import java.util.Map;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.internal.locator.AliasResolver;
import com.calclipse.mcomp.internal.locator.ArgLocator;
import com.calclipse.mcomp.internal.locator.ClassLocator;
import com.calclipse.mcomp.internal.locator.FileLocator;
import com.calclipse.mcomp.internal.locator.ScriptLocator;

/**
 * An {@link com.calclipse.mcomp.McLocator}-related utility class.
 * This class has methods for populating a context with locators.
 * 
 * @author Tone Sommerland
 */
public final class Locators
{
    private static final String SCRIPT_LOCATION_PREFIX = "script:";
    private static final String CLASS_LOCATION_PREFIX = "class:";

    private Locators()
    {
    }
    
    /**
     * Adds a locator that acts as an alias-to-location translator.
     * @param aliases the aliases associated with their respective locations
     * @param context the context to add the locator to
     */
    public static void addAliasResolver(
            final Map<String, String> aliases, final McContext context)
    {
        context.addLocator(new AliasResolver(aliases, context));
    }
    
    /**
     * Adds a locator for locations on the format {@code script:path/file},
     * where the path is relative to the module path.
     * The locator caches a script the first time it is requested,
     * and then returns the cached script
     * on subsequent requests for that script.
     * @param context the context to add the locator to
     */
    public static void addScriptLocator(final McContext context)
    {
        context.addLocator(new ScriptLocator(
                context.tracer(), SCRIPT_LOCATION_PREFIX));
    }
    
    /**
     * Adds a locator that accepts paths of script files.
     * It supports relative locations through the use of the referrer parameter.
     * @param context the context to add the locator to
     */
    public static void addFileLocator(final McContext context)
    {
        context.addLocator(new FileLocator(context.tracer()));
    }
    
    /**
     * Adds a locator that accepts locations on the format
     * {@code class:class-name},
     * where {@code class-name} is the fully qualified class name of an
     * {@link com.calclipse.mcomp.Mcomp}
     * on the module path.
     * @param context the context to add the locator to
     */
    public static void addClassLocator(final McContext context)
    {
        context.addLocator(new ClassLocator(CLASS_LOCATION_PREFIX));
    }
    
    /**
     * Adds a locator that accepts the given location
     * and gives access to arguments.
     * The arguments are accessed via the following symbols:
     * <ul>
     * <li>{@code args}:
     * an operand with an
     * {@link com.calclipse.math.parser.type.Array}
     * containing the arguments</li>
     * <li>{@code args_eval}:
     * a left-unary operator that accepts a one-based index into {@code args}
     * and evaluates the argument there</li>
     * </ul>
     * The location is used as namespace for the above symbols.
     * @param location the location that imports the arguments
     * (also the name of the imported mcomp)
     * @param context the context to add the locator to
     * @param args the arguments
     */
    public static void addArgLocator(
            final String location,
            final McContext context,
            final String... args)
    {
        context.addLocator(new ArgLocator(location, args));
    }
    
    /**
     * Returns the arguments at the given location.
     * @param location the arguments' location
     * @param context the context to search in
     * @return an empty array if the arguments weren't found at the location
     * @throws ErrorMessage if thrown by the context
     * @see {@link #addArgLocator(String, McContext, String...)}
     */
    public static String[] argsAt(
            final String location,
            final McContext context) throws ErrorMessage
    {
        return ArgLocator.findArgs(location, context);
    }

}
