package com.calclipse.mcomp.trace;

import static java.util.Objects.checkIndex;
import static java.util.Objects.requireNonNull;

import com.calclipse.math.expression.Fragment;

/**
 * Encapsulates information used to pinpoint a position in an expression.
 * 
 * @author Tone Sommerland
 */
public final class Pinpoint
{
    private final String expression;
    private final String token;
    private final int position;
    
    private Pinpoint(
            final String expression, final String token, final int position)
    {
        check(
                requireNonNull(expression, "expression"),
                requireNonNull(token, "token"),
                position);
        this.expression = expression;
        this.token = token;
        this.position = position;
    }
    
    private static void check(
            final String expression, final String token, final int position)
    {
        checkIndex(position, expression.length());
        if (!expression.startsWith(token, position))
        {
            throw new IllegalArgumentException(
                    '\'' + token +"' is not at index " + position
                    + " in '" + expression + '\'');
        }
    }
    
    /**
     * Returns an instance that pinpoints
     * the position of the given token in the given expression.
     * @param expression the expression
     * @param token the token to pinpoint
     * @param position the token's position
     * @throws IllegalArgumentException
     * if {@code token} is not at {@code position} in {@code expression}
     * @throws IndexOutOfBoundsException if {@code position} is out of bounds
     */
    public static Pinpoint at(
            final String expression, final String token, final int position)
    {
        return new Pinpoint(expression, token, position);
    }
    
    /**
     * Returns an instance that pinpoints
     * the given fragment in the given expression.
     * @param expression the expression
     * @param fragment the fragment to pinpoint
     * @throws IllegalArgumentException
     * if {@code fragment} is not at its position in {@code expression}
     * @throws IndexOutOfBoundsException
     * if {@code fragment}'s position is out of bounds
     */
    public static Pinpoint at(
            final String expression, final Fragment fragment)
    {
        return new Pinpoint(
                expression,
                fragment.token().name(),
                fragment.position());
    }
    
    /**
     * The expression containing the position this instance pinpoints.
     */
    public String expression()
    {
        return expression;
    }
    
    /**
     * The token this instance pinpoints.
     */
    public String token()
    {
        return token;
    }
    
    /**
     * The position this instance pinpoints.
     */
    public int position()
    {
        return position;
    }

    @Override
    public String toString()
    {
        return Pinpoint.class.getSimpleName()
                + ": token=" + token + ", position=" + position
                + ", expression=" + expression;
    }

}
