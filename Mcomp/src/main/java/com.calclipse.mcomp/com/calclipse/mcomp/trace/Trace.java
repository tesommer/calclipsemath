package com.calclipse.mcomp.trace;

import java.util.Optional;

/**
 * Identifies (the location of) a script or a location within a script.
 * Used in particular for formatting error messages.
 * A trace may contain the following properties:
 * <ul>
 * <li>{@code scriptName}:
 *     the name used as namespace for exported symbols</li>
 * <li>{@code scriptLocation}:
 *     the location of the script</li>
 * <li>{@code place}:
 *     a place within the script</li>
 * <li>{@code pinpoint}:
 *     a specific token in an expression at a place in the script</li>
 * </ul>
 * All of this class' properties are optional.
 * 
 * @author Tone Sommerland
 */
public final class Trace
{
    /**
     * Builds trace instances.
     * 
     * @author Tone Sommerland
     */
    public static final class Builder
    {
        private String scriptName;
        private String scriptLocation;
        private Place place;
        private Pinpoint pinpoint;
        
        /**
         * Returns a builder with all properties absent.
         */
        public Builder()
        {
        }
        
        private Builder(final Trace trace)
        {
            this.scriptName = trace.scriptName;
            this.scriptLocation = trace.scriptLocation;
            this.place = trace.place;
            this.pinpoint = trace.pinpoint;
        }

        /**
         * Sets the script name.
         * @param scriptName the script name (nullable)
         * @return {@code this}
         */
        public Builder setScriptName(final String scriptName)
        {
            this.scriptName = scriptName;
            return this;
        }

        /**
         * Sets the script location.
         * @param scriptLocation the script location (nullable)
         * @return {@code this}
         */
        public Builder setScriptLocation(final String scriptLocation)
        {
            this.scriptLocation = scriptLocation;
            return this;
        }

        /**
         * Sets the place.
         * @param place the place (nullable)
         * @return {@code this}
         */
        public Builder setPlace(final Place place)
        {
            this.place = place;
            return this;
        }

        /**
         * Sets the pinpoint.
         * @param pinpoint the pinpoint (nullable)
         * @return {@code this}
         */
        public Builder setPinpoint(final Pinpoint pinpoint)
        {
            this.pinpoint = pinpoint;
            return this;
        }
        
        /**
         * Builds the trace object.
         */
        public Trace build()
        {
            return new Trace(scriptName, scriptLocation, place, pinpoint);
        }
    }
    
    private final String scriptName;
    private final String scriptLocation;
    private final Place place;
    private final Pinpoint pinpoint;
    
    private Trace(
            final String scriptName,
            final String scriptLocation,
            final Place place,
            final Pinpoint pinpoint)
    {
        this.scriptName = scriptName;
        this.scriptLocation = scriptLocation;
        this.place = place;
        this.pinpoint = pinpoint;
    }
    
    /**
     * Returns a builder initialized with the values of {@code this}.
     */
    public Builder builder()
    {
        return new Builder(this);
    }
    
    /**
     * The script name.
     */
    public Optional<String> scriptName()
    {
        return Optional.ofNullable(scriptName);
    }
    
    /**
     * The script location.
     */
    public Optional<String> scriptLocation()
    {
        return Optional.ofNullable(scriptLocation);
    }
    
    /**
     * The place.
     */
    public Optional<Place> place()
    {
        return Optional.ofNullable(place);
    }
    
    /**
     * The pinpoint.
     */
    public Optional<Pinpoint> pinpoint()
    {
        return Optional.ofNullable(pinpoint);
    }

    @Override
    public String toString()
    {
        return Trace.class.getSimpleName()
                + ": scriptName=" + scriptName
                + ", scriptLocation=" + scriptLocation
                + ", place=" + place
                + ", pinpoint=" + pinpoint; 
    }

}
