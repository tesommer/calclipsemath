package com.calclipse.mcomp.internal.symbol;

import static java.util.Objects.requireNonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.mcomp.Environment;
import com.calclipse.mcomp.McContext;

/**
 * Asks the user for input.
 * The input is read from the standard input stream of the environment.
 * The input is evaluated as an expression in the current context.
 * Supported types:
 * <ul>
 *  <li>*: display text, returns the result</li>
 * </ul>
 * 
 * @author Tone Sommerland
 */
public final class Prompt implements UnaryOperation
{
    private static final String NAME = "prompt";
    
    private final McContext context;

    private Prompt(final McContext context)
    {
        this.context = requireNonNull(context, "context");
    }
    
    public static UnaryOperator operator(final McContext context)
    {
        return UnaryOperator.prefixed(
                NAME, OperatorPriorities.VERY_LOW, new Prompt(context));
    }

    @Override
    public Value evaluate(final Value arg) throws ErrorMessage
    {
        final String input = prompt(
                context.environment(), arg.get().toString());
        final Expression expression = context.parse(input);
        return expression.evaluate(true);
    }
    
    /**
     * Outputs the given displayText
     * and waits until the user has entered a line of text.
     * @param environment stdin and stdout
     * @param displayText the prompt
     * @return the line that was entered, or the empty string if EOF was read.
     */
    static String prompt(
            final Environment environment, final String displayText)
    {
        environment.out().print(displayText);
        final var reader = new BufferedReader(new InputStreamReader(
                environment.in()));
        try
        {
            final String line = reader.readLine();
            return line == null ? "" : line;
        }
        catch (final IOException ex)
        {
            Logger.getLogger(Prompt.class.getName()).warning(ex.toString());
            return "";
        }
    }

}
