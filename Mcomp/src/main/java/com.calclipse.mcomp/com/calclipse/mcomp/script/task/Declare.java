package com.calclipse.mcomp.script.task;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.util.Collection;
import java.util.stream.Stream;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.script.Statement;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.script.task.internal.UserOperation;
import com.calclipse.mcomp.script.task.internal.Variable;
import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

/**
 * A task that declares or reassigns a variable or a function.
 * A function declaration is obtained by
 * {@link com.calclipse.mcomp.script.task.Declare.Builder
 * #setVariables(Collection) specifying variables}.
 * When reassigning the function, the variables are optional.
 * But if they are specified, they have to be the same as in the declaration.
 * 
 * @author Tone Sommerland
 */
public final class Declare extends Task
{
    /**
     * Builds declare tasks.
     */
    public static final class Builder
    {
        private final Trace        scriptTrace;
        private final Identifier   identifier;
        private final IdRegistry   registry;
        private       Statement    assignment;
        private       Identifier[] variableIdentifiers;
        
        /**
         * Creates a new declare-task builder.
         * @param scriptTrace the script trace
         * @param identifier the identifier to declare
         * @param registry the identifier registry
         */
        public Builder(
                final Trace      scriptTrace,
                final Identifier identifier,
                final IdRegistry registry)
        {
            this.scriptTrace = requireNonNull(scriptTrace, "scriptTrace");
            this.identifier  = requireNonNull(identifier,  "identifier");
            this.registry    = requireNonNull(registry,    "registry");
        }
        
        /**
         * Sets the assignment of the variable or function.
         * In the case of variable,
         * the assignment will be evaluated during execution,
         * and the result given to the variable.
         * In the case of function,
         * the assignment will be parsed
         * and used as the definition of the function.
         * @return {@code this}
         */
        public Builder setAssignment(final Statement assignment)
        {
            this.assignment = requireNonNull(assignment, "assignment");
            return this;
        }
        
        /**
         * Specifies the function variables.
         * @return {@code this}
         */
        public Builder setVariables(final Identifier... variables)
        {
            this.variableIdentifiers = variables.clone();
            return this;
        }
        
        /**
         * Specifies the function variables.
         * @return {@code this}
         */
        public Builder setVariables(final Collection<Identifier> variables)
        {
            this.variableIdentifiers = variables.stream()
                    .toArray(Identifier[]::new);
            return this;
        }
        
        /**
         * Builds the task and returns it.
         */
        public Task build()
        {
            return new Declare(
                    scriptTrace,
                    identifier,
                    registry,
                    assignment,
                    variableIdentifiers);
        }
    }
    
    private final Identifier   identifier;
    private final IdRegistry   registry;
    private final Statement    assignment;
    private final Identifier[] variableIdentifiers;

    private Declare(
            final Trace        scriptTrace,
            final Identifier   identifier,
            final IdRegistry   registry,
            final Statement    assignment,
            final Identifier[] variableIdentifiers)
    {
        super(scriptTrace);
        assert identifier        != null;
        assert registry          != null;
        this.identifier          = identifier;
        this.registry            = registry;
        this.assignment          = assignment;
        this.variableIdentifiers = variableIdentifiers;
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        registry.validate(context);
        if (registry.isRegistered(identifier.name(), context))
        {
            executeSet(context);
        }
        else
        {
            executeCreate(context);
        }
    }

    @Override
    public boolean isOneTimeCompilationStep()
    {
        return true;
    }
    
    private void executeCreate(final McContext context) throws ErrorMessage
    {
        if (variableIdentifiers == null)
        {
            executeCreateVariable(context);
        }
        else
        {
            executeCreateFunction(context);
        }
    }
    
    private void executeSet(final McContext context) throws ErrorMessage
    {
        if (registry.isRegisteredVariable(identifier.name(), context))
        {
            executeSetVariable(context);
        }
        else if (
                registry.isRegisteredFunction(identifier.name(), context))
        {
            executeSetFunction(context);
        }
        else
        {
            throw undeclaredIdentifier(context);
        }
    }
    
    private void executeCreateVariable(final McContext context)
            throws ErrorMessage
    {
        if (!declareVariable(
                identifier.name(), createVariable(context), context))
        {
            throw declarationFailed(context);
        }
    }
    
    private void executeCreateFunction(final McContext context)
            throws ErrorMessage
    {
        if (!declareFunction(createFunction(context), context))
        {
            throw declarationFailed(context);
        }
    }
    
    private void executeSetVariable(final McContext context)
            throws ErrorMessage
    {
        if (variableIdentifiers != null)
        {
            // Attempt to assign parameters to a previously declared variable.
            throw duplicateDeclaration(context);
        }
        if (assignment == null)
        {
            return;
        }
        final Variable preexisting = registeredVariableInContextOrNull(
                identifier, context);
        preexisting.set(evaluateAssignmentOrDefault(context));
    }
    
    private void executeSetFunction(final McContext context)
            throws ErrorMessage
    {
        checkVariableIdentifiersAgainstDeclared(context);
        if (assignment == null)
        {
            return;
        }
        try
        {
            final Variable[] preexisting = UserOperation.parameters(
                    identifier.name(), context);
            final Token symbol = parseAssignmentOrDefault(preexisting, context);
            context.removeSymbol(symbol.name());
            context.addSymbol(symbol);
        }
        catch (final ErrorMessage ex)
        {
            throw assignmentFailed(ex, context);
        }
    }
    
    /**
     * Parses any assignment and creates a variable.
     */
    private Variable createVariable(final McContext context)
            throws ErrorMessage
    {
        return Variable.of(evaluateAssignmentOrDefault(context));
    }
    
    /**
     * Declares a variable, first in the context, then the registry.
     * Returns false if the context rejected it.
     */
    private boolean declareVariable(
            final String name, final Value variable, final McContext context)
    {
        final Token symbol = Operand.of(name, variable);
        if (context.addSymbol(symbol))
        {
            registry.registerVariable(
                    name, context, Declare::isValidVariable);
            return true;
        }
        return false;
    }
    
    /**
     * Parses any assignment and creates a user function.
     * Creates and declares function variables if needed.
     */
    private Function createFunction(final McContext context)
            throws ErrorMessage
    {
        final var variables = new Variable[variableIdentifiers.length];
        for (int i = 0; i < variables.length; i++)
        {
            variables[i] = getOrDeclareFunctionVariable(
                    variableIdentifiers[i], context);
        }
        return parseAssignmentOrDefault(variables, context);
    }
    
    /**
     * Declares a function, first in the context, then the registry.
     * Returns false if the context rejected it.
     */
    private boolean declareFunction(
            final Token symbol, final McContext context)
    {
        if (context.addSymbol(symbol))
        {
            registry.registerFunction(
                    symbol.name(),
                    variableIdentifiers,
                    context,
                    Declare::isValidFunction);
            return true;
        }
        return false;
    }
    
    /**
     * Checks that an identifier is a variable in the registry.
     * If it is, returns it, otherwise returns null.
     */
    private Variable registeredVariableInContextOrNull(
            final Identifier id, final McContext context)
    {
        if (registry.isRegisteredVariable(id.name(), context))
        {
            return (Variable)
                    ((Operand)context.symbol(id.name()).get()).value();
        }
        return null;
    }
    
    /**
     * Finds a suitable function variable
     * or creates one on the fly if it doesn't exist.
     * Throws exception on failure to register a new one.
     */
    private Variable getOrDeclareFunctionVariable(
            final Identifier variableIdentifier,
            final McContext context) throws ErrorMessage
    {
        final Variable preexisting = registeredVariableInContextOrNull(
                variableIdentifier, context);
        if (preexisting == null)
        {
            final Variable onTheFly = Variable.of();
            if (!declareVariable(
                    variableIdentifier.name(), onTheFly, context))
            {
                // the name is not a suitable function variable.
                throw invalidFunctionVariable(variableIdentifier, context);
            }
            return onTheFly;
        }
        return preexisting;
    }
    
    /**
     * If setting a previously declared function,
     * the specified argument list, if present,
     * must be identical to the one in the declaration.
     */
    private void checkVariableIdentifiersAgainstDeclared(
            final McContext context) throws ErrorMessage
    {
        if (variableIdentifiers == null)
        {
            return;
        }
        final Identifier[] declared = registry.functionVariables(
                identifier.name(), context);
        if (variableIdentifiers.length != declared.length)
        {
            throw invalidArgList(context);
        }
        for (int i = 0; i < variableIdentifiers.length; i++)
        {
            if (!variableIdentifiers[i].name()
                    .equals(declared[i].name()))
            {
                throw invalidArgList(context);
            }
        }
    }
    
    /**
     * Evaluates the assignment, if present,
     * otherwise returns the value of undef.
     */
    private Object evaluateAssignmentOrDefault(final McContext context)
            throws ErrorMessage
    {
        if (assignment == null)
        {
            return Values.UNDEF.get();
        }
        try
        {
            return context.parse(assignment.expression())
                    .evaluate(true).get();
        }
        catch (final ErrorMessage ex)
        {
            throw assignmentFailed(ex, context);
        }
    }
    
    /**
     * Parses the assignment, if present,
     * otherwise uses the empty string,
     * and returns a user function.
     */
    private Function parseAssignmentOrDefault(
            final Variable[] variables,
            final McContext context) throws ErrorMessage
    {
        final String definition
            = assignment == null ? "" : assignment.expression();
        final Place place = assignment == null
                ? identifier.place() : assignment.place();
        try
        {
            return UserOperation.inFunction(
                    identifier.name(),
                    variables,
                    definition,
                    context,
                    scriptTrace().builder().setPlace(place).build());
        }
        catch (final ErrorMessage ex)
        {
            throw assignmentFailed(ex, context);
        }
    }
    
    private ErrorMessage assignmentFailed(
            final ErrorMessage ex, final McContext context)
    {
        final Trace trace = ex.detail()
            .map(fragment -> scriptTrace().builder()
                    .setPlace(assignment.place())
                    .setPinpoint(
                            Pinpoint.at(assignment.expression(), fragment))
                    .build())
            .orElse(scriptTrace());
        return context.tracer().track(ex, trace);
    }
    
    private ErrorMessage declarationFailed(final McContext context)
    {
        final String message;
        if (registry.isRegistered(identifier.name(), context))
        {
            message = ScriptMessages.duplicateDeclaration(identifier.name());
        }
        else
        {
            message = ScriptMessages.invalidIdentifier(identifier.name());
        }
        return track(identifier, message, context);
    }
    
    private ErrorMessage undeclaredIdentifier(final McContext context)
    {
        return track(
                identifier,
                ScriptMessages.undeclaredIdentifier(identifier.name()),
                context);
    }
    
    private ErrorMessage duplicateDeclaration(final McContext context)
    {
        return track(
                identifier,
                ScriptMessages.duplicateDeclaration(identifier.name()),
                context);
    }
    
    private ErrorMessage invalidFunctionVariable(
            final Identifier variableIdentifier, final McContext context)
    {
        return track(
                variableIdentifier,
                ScriptMessages
                    .invalidFunctionVariable(variableIdentifier.name()),
                context);
    }
    
    private ErrorMessage invalidArgList(final McContext context)
    {
        return track(
                identifier,
                ScriptMessages.invalidArgList(identifier.name()),
                context);
    }
    
    private ErrorMessage track(
            final Identifier specifics,
            final String message,
            final McContext context)
    {
        return context.tracer().track(
                message,
                scriptTrace().builder()
                    .setPlace(specifics.place())
                    .build());
    }
    
    private static boolean isValidVariable(
            final String identifier, final McContext context)
    {
        return context.symbol(identifier)
                .filter(symbol -> symbol.type().isOperand())
                .map(Operand.class::cast)
                .map(Operand::value)
                .filter(Variable.class::isInstance)
                .isPresent();
    }
    
    private static boolean isValidFunction(
            final String identifier, final McContext context)
    {
        return context.symbol(identifier)
                .filter(symbol -> symbol.type().isFunction())
                .map(Function.class::cast)
                .filter(func -> !func.isConditional())
                .map(Function::operation)
                .filter(UserOperation.class::isInstance)
                .isPresent();
    }

    @Override
    public String toString()
    {
        return Declare.class.getSimpleName()
                + ": scriptTrace=" + scriptTrace()
                + ", identifier=" + identifier
                + ", assignment=" + assignment
                + ", variables=" + variableIdentifiers == null ?
                        null : Stream.of(variableIdentifiers)
                                .map(Object::toString)
                                .collect(joining(", ", "{", "}"));
    }
    
}
