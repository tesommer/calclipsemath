package com.calclipse.mcomp.trace;

/**
 * A place in a script specified with
 * one-based line and column numbers.
 * 
 * @author Tone Sommerland
 */
public final class Place
{
    /**
     * The first column of the first line (line 1, column 1).
     */
    public static final Place START = new Place(1, 1);
    
    private final int line;
    private final int column;
    
    private Place(final int line, final int column)
    {
        this.line = requireOnePlus(line, "line");
        this.column = requireOnePlus(column, "column");
    }
    
    private static int requireOnePlus(final int arg, final String argName)
    {
        if (arg < 1)
        {
            throw new IllegalArgumentException(argName + " < 1: " + arg);
        }
        return arg;
    }
    
    /**
     * Returns the place at the given line and column.
     * @param line one-based line number
     * @param column one-based column number
     * @throws IllegalArgumentException if the line or column are less than one
     */
    public static Place atLineColumn(final int line, final int column)
    {
        return new Place(line, column);
    }
    
    /**
     * The line number.
     */
    public int line()
    {
        return line;
    }
    
    /**
     * The column number.
     */
    public int column()
    {
        return column;
    }
    
    /**
     * Returns a place that uses this as relative to the given base.
     */
    public Place relativeTo(final Place base)
    {
        final int relativeLine = base.line + line - 1;
        final int relativeColumn = line > 1 ? column : base.column + column - 1;
        return new Place(relativeLine, relativeColumn);
    }

    @Override
    public String toString()
    {
        return Place.class.getSimpleName() + '(' + line + ", " + column + ')';
    }

}
