/**
 * Contains a script interpreter.
 */
module com.calclipse.mcomp
{
    requires java.logging;
    requires com.calclipse.lib;
    requires transitive com.calclipse.math;
    exports com.calclipse.mcomp;
    exports com.calclipse.mcomp.script;
    exports com.calclipse.mcomp.script.message;
    exports com.calclipse.mcomp.script.task;
    exports com.calclipse.mcomp.trace;
}
