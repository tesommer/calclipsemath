package test.com.calclipse.math.parser.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;
import com.calclipse.math.parser.misc.InconsistentSystem;
import com.calclipse.math.parser.misc.MatrixUtil;
import com.calclipse.math.parser.misc.SingularMatrix;

import test.TestUtil;
import test.com.calclipse.math.matrix.MockMatrix;

@DisplayName("MatrixUtil")
public final class MatrixUtilTest
{
    private static final double DELTA = 1E-13;
    
    private static final Matrix ID2 = MockMatrix.valueOf(
            2, 2,
            1, 0,
            0, 1);
    
    private static final Matrix ID3 = MockMatrix.valueOf(
            3, 3,
            1, 0, 0,
            0, 1, 0,
            0, 0, 1);

    public MatrixUtilTest()
    {
    }
    
    /*********
     * gauss *
     *********/
    
    @Test
    @DisplayName("gauss: coeff=3x3 aug=3x1")
    public void testGauss_Coeff3x3_Aug3x1()
    {
        final Matrix a1 = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        final Matrix a1WPiv = a1.copy();
        final Matrix b1 = MockMatrix.valueOf(
                3, 1,
                10,
                11,
                12);
        final Matrix b1WPiv = b1.copy();
        MatrixUtil.gauss(a1, b1, false);
        MatrixUtil.gauss(a1WPiv, b1WPiv, true);
        final Matrix expectedA1 = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                0, -3, -6,
                0, 0, 0);
        final Matrix expectedA1WPiv = MockMatrix.valueOf(
                3, 3,
                7, 8, 9,
                0, 6 / 7.0, 12 / 7.0,
                0, 0, 0);
        final Matrix expectedB1 = MockMatrix.valueOf(
                3, 1,
                10,
                -29,
                0);
        final Matrix expectedB1WPiv = MockMatrix.valueOf(
                3, 1,
                12,
                58 / 7.0,
                0);
        TestUtil.assertEquals(
                expectedA1, a1, DELTA, "a1 w/o pivoting");
        TestUtil.assertEquals(
                expectedB1, b1, DELTA, "b1 w/o pivoting");
        TestUtil.assertEquals(
                expectedA1WPiv, a1WPiv, DELTA, "a1 with pivoting");
        TestUtil.assertEquals(
                expectedB1WPiv, b1WPiv, DELTA, "b1 with pivoting");
    }
    
    @Test
    @DisplayName("gauss: coeff=2x5")
    public void testGauss_Coeff2x5()
    {
        final Matrix a1 = MockMatrix.valueOf(
                2, 5,
                2, 2, 2, 2, 2,
                3, 3, 3, 3, 3);
        final Matrix a1WPiv = a1.copy();
        final Matrix a2 = MockMatrix.valueOf(2, 5);
        final Matrix a2WPiv = a2.copy();
        MatrixUtil.gauss(a1, false);
        MatrixUtil.gauss(a1WPiv, true);
        MatrixUtil.gauss(a2, false);
        MatrixUtil.gauss(a2WPiv, true);
        final Matrix expectedA1 = MockMatrix.valueOf(
                2, 5,
                2, 2, 2, 2, 2,
                0, 0, 0, 0, 0);
        final Matrix expectedA1WPiv = MockMatrix.valueOf(
                2, 5,
                3, 3, 3, 3, 3,
                0, 0, 0, 0, 0);
        final Matrix expectedA2WAndWOPiv = MockMatrix.valueOf(2, 5);
        TestUtil.assertEquals(
                expectedA1, a1, DELTA, "a1 w/o pivoting");
        TestUtil.assertEquals(
                expectedA1WPiv, a1WPiv, DELTA, "a1 with pivoting");
        TestUtil.assertEquals(
                expectedA2WAndWOPiv, a2, DELTA, "a2 w/o pivoting");
        TestUtil.assertEquals(
                expectedA2WAndWOPiv, a2WPiv, DELTA, "a2 with pivoting");
    }
    
    @Test
    @DisplayName("gauss: coeff=2x5 aug=3x3")
    public void testGauss_Coeff3X2_Aug3x3()
    {
        final Matrix a1WPiv = MockMatrix.valueOf(
                3, 2,
                1, 2,
                3, 4,
                5, 6);
        final Matrix b1WPiv = MockMatrix.valueOf(
                3, 3,
                10, 11, 12,
                13, 14, 15,
                16, 17, 18);
        MatrixUtil.gauss(a1WPiv, b1WPiv, true);
        final Matrix expectedA1WPiv = MockMatrix.valueOf(
                3, 2,
                5, 6,
                0, 4 / 5.0,
                0, 0);
        final Matrix expectedB1WPiv = MockMatrix.valueOf(
                3, 3,
                16, 17, 18,
                34 / 5.0, 38 / 5.0, 42 / 5.0,
                0, 0, 0);
        TestUtil.assertEquals(
                expectedA1WPiv, a1WPiv, DELTA, "a1 with pivoting");
        TestUtil.assertEquals(
                expectedB1WPiv, b1WPiv, DELTA, "b1 with pivoting");
    }
    
    @Test
    @DisplayName("gauss: coeff=2x2 aug=2x0")
    public void testGauss_Coeff2X2_Aug2x0()
    {
        final Matrix a1WPiv = MockMatrix.valueOf(
                2, 2,
                10, 11,
                12, 13);
        final Matrix b1WPiv = MockMatrix.valueOf(2, 0);
        MatrixUtil.gauss(a1WPiv, b1WPiv, true);
        final Matrix expectedA1WPiv = MockMatrix.valueOf(
                2, 2,
                12, 13,
                0, 1 / 6.0);
        TestUtil.assertEquals(
                expectedA1WPiv, a1WPiv, DELTA, "a1 with pivoting");
        TestUtil.assertZeroColumns(2, b1WPiv);
    }
    
    @Test
    @DisplayName("gauss: invalid aug size")
    public void testGauss_InvalidAugSize()
    {
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.gauss(
                        MockMatrix.valueOf(3, 3),
                        MockMatrix.valueOf(2, 1),
                        false),
                "coeff=3x3 aug=2x1");
    }
    
    /***************
     * gaussJordan *
     ***************/
    
    @Test
    @DisplayName("gaussJordan")
    public void testGaussJordan()
    {
        final Matrix a1 = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                5, 4, 6,
                7, 8, 9);
        final Matrix b1 = MockMatrix.valueOf(
                3, 1,
                17,
                37,
                27);
        MatrixUtil.gaussJordan(a1, b1, false);
        final Matrix expectedA1 = MockMatrix.valueOf(
                3, 3,
                1, 0, 0,
                0, 1, 0,
                0, 0, 1);
        final Matrix expectedB1 = MockMatrix.valueOf(
                3, 1,
                1,
                -14,
                44 / 3.0);
        TestUtil.assertEquals(expectedA1, a1, DELTA, "a1");
        TestUtil.assertEquals(expectedB1, b1, DELTA, "b1");
    }
    
    @Test
    @DisplayName("gaussJordan: coeff=2x2 aug=2x0")
    public void testGaussJordan_Coeff2X2_Aug2x0()
    {
        final Matrix a1WPiv = MockMatrix.valueOf(
                2, 2,
                10, 11,
                12, 13);
        final Matrix b1WPiv = MockMatrix.valueOf(2, 0);
        MatrixUtil.gaussJordan(a1WPiv, b1WPiv, true);
        final Matrix expectedA1WPiv = MockMatrix.valueOf(
                2, 2,
                1, 0,
                0, 1);
        TestUtil.assertEquals(
                expectedA1WPiv, a1WPiv, DELTA, "a1 with pivoting");
        TestUtil.assertZeroColumns(2, b1WPiv);
    }
    
    /***********
     * project *
     ***********/
    
    @Test
    @DisplayName("project")
    public void testProject()
    {
        assertEquals(
                MockMatrix.valueOf(3, 1, 11, 14, 21),
                MatrixUtil.project(
                        MockMatrix.valueOf(3, 1, 11, 14, 21),
                        MockMatrix.valueOf(3, 1, 11, 14, 21)),
                "proj <11, 14, 21> onto <11, 14, 21>");
        assertEquals(
                MockMatrix.valueOf(3, 1, 0, 0, 0),
                MatrixUtil.project(
                        MockMatrix.valueOf(3, 1, 0, 0, 1),
                        MockMatrix.valueOf(3, 1, 0, 1, 0)),
                "proj <0, 0, 1> onto <0, 1, 0>");
        assertEquals(
                MockMatrix.valueOf(3, 1, 40 / 29.0, 60 / 29.0, 80 / 29.0),
                MatrixUtil.project(
                        MockMatrix.valueOf(3, 1, 1, 2, 3),
                        MockMatrix.valueOf(3, 1, 2, 3, 4)),
                "proj <1, 2, 3> onto <2, 3, 4>");
        TestUtil.assertZeroRows(
                1,
                MatrixUtil.project(
                        MockMatrix.valueOf(0, 1),
                        MockMatrix.valueOf(0, 1)));
    }
    
    /***************
     * gramSchmidt *
     ***************/
    
    @Test
    @DisplayName("gramSchmidt")
    public void testGramSchmidt()
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 3, 1, 2, 3, 72, 5, 6, 7, 8, 9);
        MatrixUtil.gramSchmidt(mx);
        assertTrue(TestUtil.isOrthogonal(mx, DELTA), "isOrthogonal");
        assertNotEquals(ID3, mx, "assert not identity");
        final Matrix mx2 = MockMatrix.valueOf(2, 0);
        MatrixUtil.gramSchmidt(mx2);
        TestUtil.assertZeroColumns(2, mx2);
    }
    
    /***************
     * determinant *
     ***************/
    
    @Test
    @DisplayName("determinant")
    public void testDeterminant()
    {
        assertEquals(
                18,
                MatrixUtil.determinant(
                        MockMatrix.valueOf(
                                3, 3, -2, 2, -3, -1, 1, 3, 2, 0, -1)),
                DELTA,
                "assert det 3x3 = 18");
        assertEquals(
                24,
                MatrixUtil.determinant(
                        MockMatrix.valueOf(
                                3, 3, 1, 2, 3, 0, 4, 5, 0, 0, 6)),
                DELTA,
                "assert det 3x3 = 24");
        assertEquals(
                -24,
                MatrixUtil.determinant(
                        MockMatrix.valueOf(
                                3, 3, 0, 4, 5, 1, 2, 3, 0, 0, 6)),
                DELTA,
                "assert det 3x3 = -24");
        assertEquals(
                1,
                MatrixUtil.determinant(
                        MockMatrix.valueOf(
                                0, 0)),
                DELTA,
                "assert det 0x0 = 1");
    }
    
    @Test
    @DisplayName("determinant: invalid size")
    public void testDeterminant_InvalidSize()
    {
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.determinant(MockMatrix.valueOf(0, 1)),
                "det 0x1");
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.determinant(MockMatrix.valueOf(1, 0)),
                "det 1x0");
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.determinant(MockMatrix.valueOf(2, 3)),
                "det 2x3");
    }
    
    /***********
     * inverse *
     ***********/
    
    @Test
    @DisplayName("inverse")
    public void testInverse() throws SingularMatrix
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 3, 1, 2, 3, 72, 5, 6, 7, 8, 9);
        final Matrix expected = MockMatrix.valueOf(
                3, 3,
                -1 / 136.0,   1 / 68.0,   -1 / 136.0,
                -101 / 68.0, -1 / 34.0,   35 / 68.0,
                541 / 408.0,  1 / 68.0, -139 / 408.0);
        TestUtil.assertEquals(
                expected, MatrixUtil.inverse(mx), DELTA, "3x3^-1");
        final Matrix mx2 = MockMatrix.valueOf(0, 0);
        final Matrix expected2 = MockMatrix.valueOf(0, 0);
        final Matrix actual2 = MatrixUtil.inverse(mx2);
        assertEquals(expected2, actual2, "0x0^-1");
        assertNotSame(expected2, actual2, "0x0^⁻1: assert not same");
    }
    
    @Test
    @DisplayName("determinant: invalid size")
    public void testInverse_SingularMatrix()
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        assertThrows(
                SingularMatrix.class,
                () -> MatrixUtil.inverse(mx),
                "3x3^-1: singular");
    }
    
    @Test
    @DisplayName("inverse: invalid size")
    public void testInverse_InvalidSize()
    {
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.inverse(MockMatrix.valueOf(0, 1)),
                "0x1^-1");
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.inverse(MockMatrix.valueOf(1, 0)),
                "1x0^-1");
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.inverse(MockMatrix.valueOf(2, 3)),
                "2x3^-1");
    }
    
    /**********
     * divide *
     **********/

    @Test
    @DisplayName("divide matrix by matrix")
    public void testDivideMatrixByMatrix() throws SingularMatrix
    {
        final Matrix mx = MockMatrix.valueOf(2, 2, 1, 2, 3, 4);
        final Matrix mx2 = MockMatrix.valueOf(2, 2, 5, 6, 7, 8);
        final Matrix expected = MockMatrix.valueOf(2, 2, 3, -2, 2, -1);
        TestUtil.assertEquals(
                expected, MatrixUtil.divide(mx, mx2), DELTA, "2x2 / 2x2");
    }
    
    /*****************
     * frobeniusNorm *
     *****************/
    
    @Test
    @DisplayName("frobeniusNorm")
    public void testFrobeniusNorm()
    {
        assertEquals(
                9.539392014169456,
                MatrixUtil.frobeniusNorm(MockMatrix.valueOf(
                        2, 3, 1, 2, 3, 4, 5, 6)),
                DELTA,
                "|2x3|");
        assertEquals(
                0.0,
                MatrixUtil.frobeniusNorm(MockMatrix.valueOf(
                        0, 3)),
                DELTA,
                "|0x3|");
        assertEquals(
                0.0,
                MatrixUtil.frobeniusNorm(MockMatrix.valueOf(
                        3, 0)),
                DELTA,
                "|3x0|");
        assertEquals(
                0.0,
                MatrixUtil.frobeniusNorm(MockMatrix.valueOf(
                        0, 0)),
                DELTA,
                "|0x0|");
    }
    
    /*******
     * pow *
     *******/
    
    @Test
    @DisplayName("pow")
    public void testPow() throws SingularMatrix
    {
        final Matrix mx = MockMatrix.valueOf(2, 2, 1, 3, 2, 4);
        assertEquals(
                ID2,
                MatrixUtil.pow(mx, 0),
                "2x2^0");
        assertEquals(
                mx,
                MatrixUtil.pow(mx, 1),
                "2x2^1");
        assertEquals(
                MockMatrix.valueOf(2, 2, 7, 15, 10, 22),
                MatrixUtil.pow(mx, 2),
                "2x2^2");
        assertEquals(
                MockMatrix.valueOf(2, 2, 37, 81, 54, 118),
                MatrixUtil.pow(mx, 3),
                "2x2^3");
        assertEquals(
                MockMatrix.valueOf(2, 2, 199, 435, 290, 634),
                MatrixUtil.pow(mx, 4),
                "2x2^4");
        assertEquals(
                MockMatrix.valueOf(2, 2, -2, 3 / 2.0, 1, -.5),
                MatrixUtil.pow(mx, -1),
                "2x2^-1");
        assertEquals(
                MockMatrix.valueOf(
                        2, 2, 11 / 2.0, -15 / 4.0, -5 / 2.0, 7 / 4.0),
                MatrixUtil.pow(mx, -2),
                "2x2^-2");
        assertEquals(
                MockMatrix.valueOf(
                        2, 2, -59 / 4.0, 81 / 8.0, 27 / 4.0, -37 / 8.0),
                MatrixUtil.pow(mx, -3),
                "2x2^-3");
    }
    
    @Test
    @DisplayName("pow: invalid size")
    public void testPow_InvalidSize()
    {
        assertThrows(
                SizeViolation.class,
                () -> MatrixUtil.pow(MockMatrix.valueOf(2, 3), 1),
                "2x3^1");
    }
    
    /*********
     * solve *
     *********/
    
    @Test
    @DisplayName("solve")
    public void testSolve() throws InconsistentSystem
    {
        final Matrix a1 = MockMatrix.valueOf(
                3, 3,
                1, 0, 2,
                0, 1, 3,
                0, 0, 0);
        final Matrix b1 = MockMatrix.valueOf(
                3, 1,
                10,
                20,
                0);
        final Matrix expected1 = MockMatrix.valueOf(
                3, 2,
                10, -2,
                20, -3,
                0, 1);
        assertEquals(
                expected1, MatrixUtil.solve(a1, b1, false), "1: w/o pivoting");
        assertEquals(
                expected1, MatrixUtil.solve(a1, b1, true), "1: with pivoting");
        final Matrix a2 = MockMatrix.valueOf(
                4, 3,
                0, 1, 2,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0);
        final Matrix b2 = MockMatrix.valueOf(
                4, 1,
                0,
                0,
                0,
                0);
        final Matrix expected2 = MockMatrix.valueOf(
                3, 3,
                0, 1, 0,
                0, 0, -2,
                0, 0, 1);
        assertEquals(
                expected2, MatrixUtil.solve(a2, b2, false), "2: w/o pivoting");
        assertEquals(
                expected2, MatrixUtil.solve(a2, b2, true), "2: with pivoting");
        final Matrix a3 = MockMatrix.valueOf(
                2, 3,
                1, 0, 2,
                0, 1, 0);
        final Matrix b3 = MockMatrix.valueOf(
                2, 1,
                0,
                0);
        final Matrix expected3 = MockMatrix.valueOf(
                3, 2,
                0, -2,
                0, 0,
                0, 1);
        assertEquals(
                expected3, MatrixUtil.solve(a3, b3, false), "3: w/o pivoting");
        assertEquals(
                expected3, MatrixUtil.solve(a3, b3, true), "3: with pivoting");
        final Matrix a4 = MockMatrix.valueOf(
                2, 3,
                1, 0, 0,
                0, 1, 0);
        final Matrix b4 = MockMatrix.valueOf(
                2, 1,
                0,
                0);
        final Matrix expected4 = MockMatrix.valueOf(
                3, 2,
                0, 0,
                0, 0,
                0, 1);
        assertEquals(
                expected4, MatrixUtil.solve(a4, b4, false), "4: w/o pivoting");
        assertEquals(
                expected4, MatrixUtil.solve(a4, b4, true), "4: with pivoting");
        final Matrix a5 = MockMatrix.valueOf(
                3, 3,
                1, 0, -4,
                0, 1, 0,
                0, 0, 0);
        final Matrix b5 = MockMatrix.valueOf(
                3, 1,
                -1,
                2,
                0);
        final Matrix expected5 = MockMatrix.valueOf(
                3, 2,
                -1, 4,
                2, 0,
                0, 1);
        assertEquals(
                expected5, MatrixUtil.solve(a5, b5, false), "5: w/o pivoting");
        assertEquals(
                expected5, MatrixUtil.solve(a5, b5, true), "5: with pivoting");
    }
    
    @Test
    @DisplayName("solve: inconsistent system")
    public void testSolve_Inconsistent()
    {
        final Matrix a = MockMatrix.valueOf(
                3, 3,
                1, 0, 2,
                0, 1, 3,
                0, 0, 0);
        final Matrix b = MockMatrix.valueOf(
                3, 1,
                10,
                20,
                30);
        assertThrows(
                InconsistentSystem.class,
                () -> MatrixUtil.solve(a, b, false),
                "inconsistent system");
    }

}
