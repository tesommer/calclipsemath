package test.com.calclipse.math.parser.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.step.Check;

import test.TestUtil;

@DisplayName("Check")
public final class CheckTest
{
    private static final Token
    LPAREN = Tokens.leftParenthesis("(");
    
    private static final Token
    RPAREN = Tokens.rightParenthesis(")");
    
    private static final Token
    COMMA = Tokens.plain(",");
    
    private static final Token
    UNDEFINED = Tokens.undefined("Male/Female/Undefined");
    
    private static final Token
    PI = Operand.of("pi", Values.UNDEF);
    
    private static final Token
    E = Operand.of("e", Values.UNDEF);
    
    private static final Token
    MAX = Function.of(
            "max",
            Delimitation.byTokensWithOpener(
                    LPAREN, RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);
    
    private static final Token
    MULTIPLY = BinaryOperator.of("*", 0, (left, right) -> Values.UNDEF);
    
    private static final Token
    SIN = UnaryOperator.prefixed("sin", 0, arg -> Values.UNDEF);
    
    private static final Token
    FACTORIAL = UnaryOperator.postfixed("!", 0, arg -> Values.UNDEF);

    public CheckTest()
    {
    }
    
    /*************
     * UNDEFINED *
     *************/
    
    @Test
    @DisplayName("Undefined")
    public void testUndefined()
    {
        assertChokesOnLast(Fragment.of(UNDEFINED, 0));
    }
    
    /*********
     * PLAIN *
     *********/
    
    @Test
    @DisplayName("Plain")
    public void testPlain()
    {
        assertChokesOnLast(Fragment.of(COMMA, 0));
    }
    
    /************
     * FUNCTION *
     ************/
    
    @Test
    @DisplayName("Function function")
    public void testFunctionFunction()
    {
        final Fragment frag1 = Fragment.of(MAX,  0);
        final Fragment frag2 = Fragment.of(MAX, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Function operand")
    public void testFunctionOperand()
    {
        final Fragment frag1 = Fragment.of(MAX, 0);
        final Fragment frag2 = Fragment.of(PI, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Function left-unary")
    public void testFunctionLeftUnary()
    {
        final Fragment frag1 = Fragment.of(MAX, 0);
        final Fragment frag2 = Fragment.of(PI, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Function left-parenthesis")
    public void testFunctionLeftParenthesis()
    {
        final Fragment frag1 = Fragment.of(MAX,     0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    /***********
     * OPERAND *
     ***********/
    
    @Test
    @DisplayName("Operand function")
    public void testOperandFunction()
    {
        final Fragment frag1 = Fragment.of(PI,   0);
        final Fragment frag2 = Fragment.of(MAX, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Operand operand")
    public void testOperandOperand()
    {
        final Fragment frag1 = Fragment.of(PI, 0);
        final Fragment frag2 = Fragment.of(E, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Operand left-unary")
    public void testOperandLeftUnary()
    {
        final Fragment frag1 = Fragment.of(PI,   0);
        final Fragment frag2 = Fragment.of(SIN, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Operand left-parenthesis")
    public void testOperandLeftParenthesis()
    {
        final Fragment frag1 = Fragment.of(PI,      0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    /*************
     * BOPERATOR *
     *************/
    
    @Test
    @DisplayName("Binary binary")
    public void testBinaryBinary()
    {
        final Fragment frag1 = Fragment.of(PI,        0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        final Fragment frag3 = Fragment.of(MULTIPLY, 20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Binary right-unary")
    public void testBinaryRightUnary()
    {
        final Fragment frag1 = Fragment.of(PI,         0);
        final Fragment frag2 = Fragment.of(MULTIPLY,  10);
        final Fragment frag3 = Fragment.of(FACTORIAL, 20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Binary right-parenthesis")
    public void testBinaryRightParenthesis()
    {
        final Fragment frag1 = Fragment.of(PI,        0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        final Fragment frag3 = Fragment.of(RPAREN,   20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    /**************
     * LUOPERATOR *
     **************/
    
    @Test
    @DisplayName("Left-unary binary")
    public void testLeftUnaryBinary()
    {
        final Fragment frag1 = Fragment.of(SIN,       0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Left-unary right-unary")
    public void testLeftUnaryRightUnary()
    {
        final Fragment frag1 = Fragment.of(SIN,        0);
        final Fragment frag2 = Fragment.of(FACTORIAL, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Left-unary right-parenthesis")
    public void testLeftUnaryRightParenthesis()
    {
        final Fragment frag1 = Fragment.of(SIN,     0);
        final Fragment frag2 = Fragment.of(RPAREN, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    /**************
     * RUOPERATOR *
     **************/
    
    @Test
    @DisplayName("Right-unary function")
    public void testRightUnaryFunction()
    {
        final Fragment frag1 = Fragment.of(PI,         0);
        final Fragment frag2 = Fragment.of(FACTORIAL, 10);
        final Fragment frag3 = Fragment.of(MAX,       20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Right-unary operand")
    public void testRightUnaryOperand()
    {
        final Fragment frag1 = Fragment.of(PI,         0);
        final Fragment frag2 = Fragment.of(FACTORIAL, 10);
        final Fragment frag3 = Fragment.of(E,         20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Right-unary left-unary")
    public void testRightUnaryLeftUnary()
    {
        final Fragment frag1 = Fragment.of(PI,         0);
        final Fragment frag2 = Fragment.of(FACTORIAL, 10);
        final Fragment frag3 = Fragment.of(SIN,       20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Right-unary left-parenthesis")
    public void testRightUnaryLeftParenthesis()
    {
        final Fragment frag1 = Fragment.of(PI,         0);
        final Fragment frag2 = Fragment.of(FACTORIAL, 10);
        final Fragment frag3 = Fragment.of(LPAREN,    20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    /**********
     * LPAREN *
     **********/
    
    @Test
    @DisplayName("Left-parenthesis binary")
    public void testLeftParenthesisBinary()
    {
        final Fragment frag1 = Fragment.of(LPAREN,    0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Left-parenthesis right-unary")
    public void testLeftParenthesisRightUnary()
    {
        final Fragment frag1 = Fragment.of(LPAREN,     0);
        final Fragment frag2 = Fragment.of(FACTORIAL, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Left-parenthesis right-parenthesis")
    public void testLeftParenthesisRightParenthesis()
    {
        final Fragment frag1 = Fragment.of(LPAREN,  0);
        final Fragment frag2 = Fragment.of(RPAREN, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    /**********
     * RPAREN *
     **********/
    
    @Test
    @DisplayName("Right-parenthesis function")
    public void testRightParenthesisFunction()
    {
        final Fragment frag1 = Fragment.of(LPAREN,  0);
        final Fragment frag2 = Fragment.of(PI,     10);
        final Fragment frag3 = Fragment.of(RPAREN, 20);
        final Fragment frag4 = Fragment.of(MAX,    30);
        assertChokesOnLast(frag1, frag2, frag3, frag4);
    }
    
    @Test
    @DisplayName("Right-parenthesis operand")
    public void testRightParenthesisOperand()
    {
        final Fragment frag1 = Fragment.of(LPAREN,  0);
        final Fragment frag2 = Fragment.of(PI,     10);
        final Fragment frag3 = Fragment.of(RPAREN, 20);
        final Fragment frag4 = Fragment.of(E,      30);
        assertChokesOnLast(frag1, frag2, frag3, frag4);
    }
    
    @Test
    @DisplayName("Right-parenthesis left-unary")
    public void testRightParenthesisLeftUnary()
    {
        final Fragment frag1 = Fragment.of(LPAREN,  0);
        final Fragment frag2 = Fragment.of(PI,     10);
        final Fragment frag3 = Fragment.of(RPAREN, 20);
        final Fragment frag4 = Fragment.of(SIN,    30);
        assertChokesOnLast(frag1, frag2, frag3, frag4);
    }
    
    /***************
     * Paren count *
     ***************/
    
    @Test
    @DisplayName("Too many left parentheses")
    public void testTooManyLeftParentheses()
    {
        final Fragment frag1 = Fragment.of(LPAREN, 0);
        final Fragment frag2 = Fragment.of(PI,    10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Unexpected right parenthesis")
    public void testUnexpectedRightParenthesis()
    {
        final Fragment frag1 = Fragment.of(PI,      0);
        final Fragment frag2 = Fragment.of(RPAREN, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    /***************
     * Start error *
     ***************/
    
    @Test
    @DisplayName("Starting with binary")
    public void testStartingWithBinary()
    {
        assertChokesOnLast(Fragment.of(MULTIPLY, 0));
    }
    
    @Test
    @DisplayName("Starting with right-unary")
    public void testStartingWithRightUnary()
    {
        assertChokesOnLast(Fragment.of(FACTORIAL, 0));
    }
    
    /*************
     * End error *
     *************/
    
    @Test
    @DisplayName("Ending with binary")
    public void testEndingWithBinary()
    {
        final Fragment frag1 = Fragment.of(PI,        0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        assertChokesOnLast(frag1, frag2);
    }
    
    @Test
    @DisplayName("Ending with left-unary")
    public void testEndingWithLeftUnary()
    {
        final Fragment frag1 = Fragment.of(PI,        0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        final Fragment frag3 = Fragment.of(SIN,      20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Ending with left-parenthesis")
    public void testEndingWithLeftParenthesis()
    {
        final Fragment frag1 = Fragment.of(PI,        0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        final Fragment frag3 = Fragment.of(LPAREN,   20);
        assertChokesOnLast(frag1, frag2, frag3);
    }
    
    /*********
     * Misc. *
     *********/
    
    @Test
    @DisplayName("Well-formed infix")
    public void testWellFormedInfix() throws ErrorMessage
    {
        // sin pi ru * ( f )
        final Fragment frag1 = Fragment.of(SIN,        0);
        final Fragment frag2 = Fragment.of(PI,        10);
        final Fragment frag3 = Fragment.of(FACTORIAL, 20);
        final Fragment frag4 = Fragment.of(MULTIPLY,  30);
        final Fragment frag5 = Fragment.of(LPAREN,    40);
        final Fragment frag6 = Fragment.of(MAX,       50);
        final Fragment frag7 = Fragment.of(RPAREN,    60);
        final Expression output = TestUtil.parse(
                Check.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, frag1, frag2, frag3, frag4, frag5, null, frag7);
    }
    
    private static void assertChokesOnLast(final Fragment... input)
    {
        assert input.length > 0;
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(Check.INSTANCE, input),
                "input: " + Arrays.toString(input));
        final Fragment last = input[input.length - 1];
        if (last.token().type().isFunction())
        {
            assertChokedOnFunction(thrown, last);
        }
        else
        {
            assertSame(last, thrown.detail().get(), "detail");
        }
    }

    private static void assertChokedOnFunction(
            final ErrorMessage thrown, final Fragment last)
    {
        final Fragment attachment = thrown.detail().get();
        assertNotSame(last, attachment, "Function: detail");
        assertTrue(
                attachment.token().type().isFunction(),
                "Function: detail type");
        assertEquals(
                last.token().name(),
                attachment.token().name(),
                "Function: detail name");
        assertEquals(
                last.position(),
                attachment.position(),
                "Function: detail position");
    }

}
