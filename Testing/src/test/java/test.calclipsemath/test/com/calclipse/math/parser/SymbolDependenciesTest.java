package test.com.calclipse.math.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.function.Supplier;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.math.parser.SymbolDependencies;
import com.calclipse.math.parser.SymbolTable;

@DisplayName("SymbolDependencies")
public final class SymbolDependenciesTest
{
    public SymbolDependenciesTest()
    {
    }
    
    @Test
    @DisplayName("unaryReference")
    public void testUnaryReference() throws ErrorMessage
    {
        final SymbolTable symbolTable = createSymbolTable();
        final UnaryOperator symbol = UnaryOperator.prefixed(
                "sin", 1, arg -> Value.constantOf(21));
        final UnaryOperator symbol2 = UnaryOperator.prefixed(
                "sin", 1, arg -> Value.constantOf(22));
        symbolTable.add(symbol);
        final UnaryOperation ref = SymbolDependencies
                .unaryReference(symbol, symbolTable::get);
        assertEquals(21, ref.evaluate(Values.UNDEF).get(), "symbol");
        symbolTable.remove(symbol.name());
        symbolTable.add(symbol2);
        assertEquals(22, ref.evaluate(Values.UNDEF).get(), "symbol2");
    }
    
    @Test
    @DisplayName("binaryReference")
    public void testBinaryReference() throws ErrorMessage
    {
        final SymbolTable symbolTable = createSymbolTable();
        final BinaryOperator symbol = BinaryOperator.of(
                "+", 1, (left, right) -> Value.constantOf(21));
        final BinaryOperator symbol2 = BinaryOperator.of(
                "+", 1, (left, right) -> Value.constantOf(22));
        symbolTable.add(symbol);
        final BinaryOperation ref = SymbolDependencies
                .binaryReference(symbol, symbolTable::get);
        assertEquals(
                21, ref.evaluate(Values.UNDEF, Values.UNDEF).get(), "symbol");
        symbolTable.remove(symbol.name());
        symbolTable.add(symbol2);
        assertEquals(
                22, ref.evaluate(Values.UNDEF, Values.UNDEF).get(), "symbol2");
    }
    
    @Test
    @DisplayName("variableArityReference")
    public void testVariableArityReference() throws ErrorMessage
    {
        final SymbolTable symbolTable = createSymbolTable();
        final Function symbol = Function.of(
                "max",
                ParensAndComma.DELIMITATION,
                args -> Value.constantOf(21));
        final Function symbol2 = Function.of(
                "max",
                ParensAndComma.DELIMITATION,
                args -> Value.constantOf(22));
        final Function symbol3 = Function.ofConditional(
                "max",
                ParensAndComma.DELIMITATION,
                args -> Value.constantOf(23));
        symbolTable.add(symbol);
        final VariableArityOperation ref = SymbolDependencies
                .variableArityReference(symbol, symbolTable::get);
        assertEquals(21, ref.evaluate().get(), "symbol");
        symbolTable.remove(symbol.name());
        symbolTable.add(symbol2);
        assertEquals(22, ref.evaluate().get(), "symbol2");
        symbolTable.remove(symbol2.name());
        symbolTable.add(symbol3);
        assertEquals(21, ref.evaluate().get(), "symbol3");
        assertThrows(
                IllegalArgumentException.class,
                () -> SymbolDependencies
                        .variableArityReference(symbol3, symbolTable::get),
                "variableArityReference(symbol3)");
    }
    
    @Test
    @DisplayName("conditionalReference")
    public void testConditionalReference() throws ErrorMessage
    {
        final SymbolTable symbolTable = createSymbolTable();
        final Function symbol = Function.ofConditional(
                "for",
                ParensAndComma.DELIMITATION,
                args -> Value.constantOf(21));
        final Function symbol2 = Function.ofConditional(
                "for",
                ParensAndComma.DELIMITATION,
                args -> Value.constantOf(22));
        final Function symbol3 = Function.of(
                "for",
                ParensAndComma.DELIMITATION,
                args -> Value.constantOf(23));
        symbolTable.add(symbol);
        final ConditionalOperation ref = SymbolDependencies
                .conditionalReference(symbol, symbolTable::get);
        assertEquals(21, ref.evaluate(List.of()).get(), "symbol");
        symbolTable.remove(symbol.name());
        symbolTable.add(symbol2);
        assertEquals(22, ref.evaluate(List.of()).get(), "symbol2");
        symbolTable.remove(symbol2.name());
        symbolTable.add(symbol3);
        assertEquals(21, ref.evaluate(List.of()).get(), "symbol3");
        assertThrows(
                IllegalArgumentException.class,
                () -> SymbolDependencies
                        .conditionalReference(symbol3, symbolTable::get),
                "conditionalReference(symbol3)");
    }
    
    @Test
    @DisplayName("selection")
    public void testSelection()
    {
        final SymbolTable symbolTable = createSymbolTable();
        final Token fallback = Tokens.plain("T");
        final Token a = Tokens.plain("a");
        final Token b = Tokens.plain("b");
        final Token c = Tokens.plain("c");
        symbolTable.add(a);
        symbolTable.add(b);
        symbolTable.add(c);
        final Supplier<Token> selection = SymbolDependencies.selection(
                Token.class, fallback, symbolTable::all, b::identifiesAs);
        assertSame(b, selection.get(), "b");
        symbolTable.remove(b.name());
        assertSame(fallback, selection.get(), "fallback");
    }
    
    private static SymbolTable createSymbolTable()
    {
        return new MockSymbolTable();
    }

}
