package test.com.calclipse.mcomp.script;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.math.expression.recursion.UnboundRecursion;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.Script;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.TaskList;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("Script")
public final class ScriptTest
{
    private static final Trace
    TRACE = new Trace.Builder().setScriptName("Abc").build();

    public ScriptTest()
    {
    }
    
    @Test
    @DisplayName("Unbound recursion")
    public void testUnboundRecursion()
    {
        final var task = new LimitlessRecursionTask();
        final var tasks = new TaskList();
        tasks.add(task);
        final Script script = Script.nameless(
                tasks, RecursionGuard.countingTo(2));
        task.mcomp = script;
        assertThrows(
                UnboundRecursion.class,
                () -> script.execute(MockContext.getOne()),
                "Expected unlimited recursion.");
    }
    
    private static final class LimitlessRecursionTask extends Task
    {
        private Mcomp mcomp;

        private LimitlessRecursionTask()
        {
            super(TRACE);
        }

        @Override
        public void execute(final McContext context) throws ErrorMessage
        {
            if (mcomp != null)
            {
                mcomp.execute(context);
            }
        }

        @Override
        public boolean isOneTimeCompilationStep()
        {
            return false;
        }
        
    }

}
