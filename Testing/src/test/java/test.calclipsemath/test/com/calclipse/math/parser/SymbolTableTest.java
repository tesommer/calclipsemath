package test.com.calclipse.math.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.parser.SymbolTable;

@DisplayName("SymbolTable")
public abstract class SymbolTableTest
{
    protected SymbolTableTest()
    {
    }

    @Nested
    @DisplayName("SymbolTable.longestMatchLookup()")
    public static final class LongestMatchLookupTest extends SymbolTableTest
    {
        public LongestMatchLookupTest()
        {
        }

        @Override
        protected SymbolTable createSymbolTable()
        {
            return SymbolTable.longestMatchLookup();
        }

        @Test
        @DisplayName("lookUp")
        public void testLookUp()
        {
            final SymbolTable symbolTable = createSymbolTable();
            final String name1 = "matrix";
            final String name2 = "vector";
            final String name3 = "max";
            final String name4 = "mini";
            final String name5 = "complex";
            final String name6 = "min";
            final Token token1 = Tokens.plain(name1);
            final Token token2 = Tokens.plain(name2);
            final Token token3 = Tokens.plain(name3);
            final Token token4 = Tokens.plain(name4);
            final Token token5 = Tokens.plain(name5);
            final Token token6 = Tokens.plain(name6);
            assertTrue(
                    symbolTable.lookUp('m').isEmpty(),
                    "lookUp(m).isEmpty()");
            symbolTable.add(token1);
            symbolTable.add(token2);
            symbolTable.add(token3);
            symbolTable.add(token4);
            symbolTable.add(token5);
            symbolTable.add(token6);
            assertTrue(
                    symbolTable.lookUp('x').isEmpty(),
                    "lookUp(x).isEmpty()");
            final var expectedM = List.of(token1, token3, token4, token6);
            final var actualM = List.copyOf(symbolTable.lookUp('m'));
            assertTrue(
                    allInSecondHasMatchInFirst(expectedM, actualM),
                    "lookUp(m): unexpected symbol(s)");
            assertTrue(
                    allInSecondHasMatchInFirst(actualM, expectedM),
                    "lookUp(m): missing symbol(s)");
            assertSame(
                    token1,
                    actualM.get(0),
                    "lookUp(m): symbol at index 0");
            assertSame(
                    token4,
                    actualM.get(1),
                    "lookUp(m): symbol at index 1");
            final var actualV = List.copyOf(symbolTable.lookUp('v'));
            assertEquals(
                    1,
                    actualV.size(),
                    "lookUp(v): size()");
            assertSame(
                    token2,
                    actualV.get(0),
                    "lookUp(v): symbol at index 0");
            final var actualC = List.copyOf(symbolTable.lookUp('c'));
            assertEquals(
                    1,
                    actualC.size(),
                    "lookUp(c): size()");
            assertSame(
                    token5,
                    actualC.get(0),
                    "lookUp(c): symbol at index 0");
        }
    }
    
    /**
     * Creates symbol-table instances to test.
     */
    protected abstract SymbolTable createSymbolTable();
    
    @Test
    @DisplayName("add")
    public final void testAdd()
    {
        final SymbolTable symbolTable = createSymbolTable();
        final String name1 = "token";
        final String name2 = "nekot";
        final Token token1 = Tokens.plain(name1);
        final Token token2 = Tokens.plain(name2);
        final Token token3 = Tokens.plain(name1);
        assertTrue(
                symbolTable.add(token1),
                "Nonexistent symbol");
        assertSymbolTableContains(symbolTable, token1);
        assertTrue(
                symbolTable.add(token2),
                "Name with equal length as existing symbol");
        assertSymbolTableContains(symbolTable, token1, token2);
        assertFalse(
                symbolTable.add(token3),
                "Duplicate symbol");
        assertSymbolTableContains(symbolTable, token1, token2);
        assertFalse(
                symbolTable.add(token1),
                "Reference to existing symbol");
        assertSymbolTableContains(symbolTable, token1, token2);
    }
    
    @Test
    @DisplayName("remove")
    public final void testRemove()
    {
        final SymbolTable symbolTable = createSymbolTable();
        final String name1 = "a";
        final String name2 = "b";
        final String name3 = "ba";
        final String name4 = "bb";
        final String name5 = "bbb";
        final Token token1 = Tokens.plain(name1);
        final Token token2 = Tokens.plain(name2);
        final Token token3 = Tokens.plain(name3);
        final Token token4 = Tokens.plain(name4);
        final Token token5 = Tokens.plain(name5);
        assertFalse(
                symbolTable.remove(name1),
                "table: {}: remove(a)");
        assertFalse(
                symbolTable.remove(""),
                "table: {}: remove()");
        symbolTable.add(token1);
        symbolTable.add(token2);
        symbolTable.add(token3);
        symbolTable.add(token4);
        symbolTable.add(token5);
        assertFalse(
                symbolTable.remove("cc"),
                "table: {a, b, ba, bb, bbb}: remove(cc)");
        assertSymbolTableContains(
                symbolTable, token1, token2, token3, token4, token5);
        assertFalse(
                symbolTable.remove("bc"),
                "table: {a, b, ba, bb, bbb}: remove(bc)");
        assertSymbolTableContains(
                symbolTable, token1, token2, token3, token4, token5);
        assertFalse(
                symbolTable.remove(""),
                "table: {a, b, ba, bb, bbb}: remove()");
        assertSymbolTableContains(
                symbolTable, token1, token2, token3, token4, token5);
        assertTrue(
                symbolTable.remove(name1),
                "table: {a, b, ba, bb, bbb}: remove(a)");
        assertSymbolTableContains(symbolTable, token2, token3, token4, token5);
        assertTrue(
                symbolTable.remove(name2),
                "table: {b, ba, bb, bbb}: remove(b)");
        assertSymbolTableContains(symbolTable, token3, token4, token5);
        assertTrue(
                symbolTable.remove(name3),
                "table: {ba, bb, bbb}: remove(ba)");
        assertSymbolTableContains(symbolTable, token4, token5);
        assertTrue(
                symbolTable.remove(name4),
                "table: {bb, bbb}: remove(bb)");
        assertSymbolTableContains(symbolTable, token5);
        assertTrue(
                symbolTable.remove(name5),
                "table: {bbb}: remove(bbb)");
        assertSymbolTableContains(symbolTable);
    }
    
    @Test
    @DisplayName("get")
    public final void testGet()
    {
        final SymbolTable symbolTable = createSymbolTable();
        final String name1 = "sin";
        final String name2 = "sqrt";
        final String name3 = "acos";
        final String name4 = "asin";
        final String name5 = "atan2";
        final Token token1 = Tokens.plain(name1);
        final Token token2 = Tokens.plain(name2);
        final Token token3 = Tokens.plain(name3);
        final Token token4 = Tokens.plain(name4);
        final Token token5 = Tokens.plain(name5);
        assertNull(
                symbolTable.get(name1),
                "table: {}: get(sin)");
        assertNull(
                symbolTable.get(""),
                "table: {}: get()");
        symbolTable.add(token1);
        symbolTable.add(token2);
        symbolTable.add(token3);
        symbolTable.add(token4);
        symbolTable.add(token5);
        assertNull(
                symbolTable.get("si"),
                "table: {sin, sqrt, acos, asin, atan2}: get(si)");
        assertNull(
                symbolTable.get(""),
                "table: {sin, sqrt, acos, asin, atan2}: get()");
        assertSame(
                token1,
                symbolTable.get(name1),
                "table: {sin, sqrt, acos, asin, atan2}: get(sin)");
        assertSame(
                token2,
                symbolTable.get(name2),
                "table: {sin, sqrt, acos, asin, atan2}: get(sqrt)");
        assertSame(
                token3,
                symbolTable.get(name3),
                "table: {sin, sqrt, acos, asin, atan2}: get(acos)");
        assertSame(
                token4,
                symbolTable.get(name4),
                "table: {sin, sqrt, acos, asin, atan2}: get(asin)");
        assertSame(
                token5,
                symbolTable.get(name5),
                "table: {sin, sqrt, acos, asin, atan2}: get(atan2)");
        assertSymbolTableContains(
                symbolTable, token1, token2, token3, token4, token5);
    }
    
    @Test
    @DisplayName("all")
    public final void testAll()
    {
        final SymbolTable symbolTable = createSymbolTable();
        final String name1 = "matrix";
        final String name2 = "vector";
        final String name3 = "max";
        final String name4 = "min";
        final String name5 = "complex";
        final Token token1 = Tokens.plain(name1);
        final Token token2 = Tokens.plain(name2);
        final Token token3 = Tokens.plain(name3);
        final Token token4 = Tokens.plain(name4);
        final Token token5 = Tokens.plain(name5);
        assertTrue(
                symbolTable.all().isEmpty(),
                "table: {}: all().isEmpty()");
        symbolTable.add(token1);
        symbolTable.add(token2);
        symbolTable.add(token3);
        symbolTable.add(token4);
        symbolTable.add(token5);
        final var expected = List.of(token1, token2, token3, token4, token5);
        final var actual = symbolTable.all();
        assertTrue(
                allInSecondHasMatchInFirst(expected, actual),
                "Unexpected symbol(s)");
        assertTrue(
                allInSecondHasMatchInFirst(actual, expected),
                "Missing symbol(s)");
    }
    
    @Test
    @DisplayName("clear")
    public final void testClear()
    {
        final SymbolTable symbolTable = createSymbolTable();
        symbolTable.clear();
        assertSymbolTableContains(symbolTable);
        final Token token1 = Tokens.plain("e");
        final Token token2 = Tokens.plain("f");
        final Token token3 = Tokens.plain("ee");
        final Token token4 = Tokens.plain("ff");
        symbolTable.add(token1);
        symbolTable.add(token2);
        symbolTable.add(token3);
        symbolTable.add(token4);
        assertSymbolTableContains(symbolTable, token1, token2, token3, token4);
        symbolTable.clear();
        assertSymbolTableContains(symbolTable);
    }
    
    protected static void assertSymbolTableContains(
            final SymbolTable symbolTable, final Token... symbols)
    {
        final Collection<Token> expectedTokens = List.of(symbols);
        final Collection<Token> actualTokens = symbolTable.all();
        assertTrue(
                allInSecondHasMatchInFirst(expectedTokens, actualTokens),
                "Symbol table contains unexpected symbol(s).");
        assertTrue(
                allInSecondHasMatchInFirst(actualTokens, expectedTokens),
                "Symbol table is missing symbol(s).");
    }
    
    protected static boolean allInSecondHasMatchInFirst(
            final Collection<? extends Token> first,
            final Collection<? extends Token> second)
    {
        return second.stream().allMatch(
                hasMatch -> first.stream().anyMatch(
                        token -> token.matches(hasMatch)));
    }

}
