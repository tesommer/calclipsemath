package test.com.calclipse.mcomp.script.task;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static test.TestUtil.assertCompiled;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.script.task.Export;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("Export")
public final class ExportTest
{
    private static final Trace
    TRACE = new Trace.Builder().setScriptName("Teen pig-wolf").build();
    
    private static final Identifier
    ID1 = new Identifier("barfy-on-my-shoes slayer", Place.START);
    
    private static final Identifier
    ID2 = new Identifier("Damn Giles, that tweed maniac!", Place.START);
    
    private final IdRegistry registry = new IdRegistry();

    public ExportTest()
    {
    }
    
    @AfterEach
    public void tearDown()
    {
        registry.clear();
    }
    
    @Test
    @DisplayName("execute")
    public void testExecute() throws ErrorMessage
    {
        final McContext context = MockContext.getOne();
        final Token plain = Tokens.plain(ID1.name());
        final Token operand = Operand.of(ID2.name(), Values.UNDEF);
        final Export exportPlain = taskToTest(ID1);
        final Export exportOperand = taskToTest(ID2);
        assertCompiled(false, exportPlain);
        assertCompiled(false, exportOperand);
        context.importMcomp(cx ->
        {
            registry.registerVariable(ID1.name(), cx, (n, c) -> true);
            registry.registerVariable(ID2.name(), cx, (n, c) -> true);
            cx.addSymbol(plain);
            cx.addSymbol(operand);
            exportPlain.execute(cx);
            exportOperand.execute(cx);
        });
        assertCompiled(true, exportPlain);
        assertCompiled(true, exportOperand);
        final Token importedPlain = context.symbol(
                context.namespaceSeparator() + ID1.name()).get();
        final Token importedOperand = context.symbol(
                context.namespaceSeparator() + ID2.name()).get();
        assertTrue(
                context.importedSymbols().contains(importedPlain),
                "Imports contains plain");
        assertTrue(
                context.importedSymbols().contains(importedOperand),
                "Imports contains operand");
        assertNotSame(
                ((Operand)operand).value(),
                ((Operand)importedOperand).value(),
                "Value of exported operand");
    }
    
    @Test
    @DisplayName("Overwritten identifier")
    public void testOverwrittenIdentifier()
    {
        final McContext context = MockContext.getOne();
        final Export exportPlain = taskToTest(ID1);
        final Mcomp mcomp = cx ->
        {
            registry.registerVariable(ID1.name(), cx, (n, c) -> true);
            exportPlain.execute(cx);
        };
        assertThrows(
                ErrorMessage.class,
                () -> context.importMcomp(mcomp),
                "Overwritten identifier, but no exception.");
    }
    
    private Export taskToTest(final Identifier identifier)
    {
        return new Export(TRACE, identifier, registry);
    }

}
