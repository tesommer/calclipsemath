package test.com.calclipse.mcomp.script;

import static com.calclipse.math.expression.Errors.errorMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.StringReader;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.script.Script;
import com.calclipse.mcomp.script.ScriptParser;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.trace.Trace;
import com.calclipse.mcomp.trace.Tracer;

@DisplayName("ScriptParser")
public final class ScriptParserTest
{
    public ScriptParserTest()
    {
    }
    
    @Nested
    @DisplayName("ScriptParser.getDefault(Tracer)")
    public static final class GetDefaultTest
    {
        private static final ScriptParser
        PARSER = ScriptParser.trackingErrorsWith(MockTracer.INSTANCE)
            .trackingLocation("http://rundontwalk.away");
        
        public GetDefaultTest()
        {
        }
        
        @Test
        @DisplayName("Empty script")
        public void testEmptyScript() throws ErrorMessage
        {
            final Script script = PARSER.parse(new StringReader(""));
            assertEquals("", script.name());
        }
        
        @Test
        @DisplayName("Name only")
        public void testNameOnly() throws ErrorMessage
        {
            final Script script = PARSER.parse(new StringReader(
                    "name hydrogen"));
            assertEquals("hydrogen", script.name());
        }
        
        @Test
        @DisplayName("Undelimited comment in statement")
        public void testUndelimitedCommentInStatement() throws ErrorMessage
        {
            // The parser forgives this.
            final Script script = PARSER.parse(new StringReader(
                    "name kilo\nx := /*2"));
            assertEquals("kilo", script.name());
        }
        
        @Test
        @DisplayName("Undelimited string in statement")
        public void testUndelimitedStringInStatement() throws ErrorMessage
        {
            // The parser forgives this too.
            final Script script = PARSER.parse(new StringReader(
                    "name gf x := '2\n\t\"�"));
            assertEquals("gf", script.name());
        }
        
        @Test
        @DisplayName("Script without name")
        public void testScriptWithoutName() throws ErrorMessage
        {
            final String code
                = "/*name ts*/\n"
                + "import 'xx', 'yy', namespace 'zz'\n"
                + "x := sin y\n"
                + "y := sin x;\n"
                + " cos z\n"
                + "exec print tan x/y\r\n"
                + "export x, y\n"
                + "undeclare x, y\n"
                + "/*\n"
                + " * MAIN SECTION\r"
                + " */\n"
                + "main a b c\n"
                + "\td e f\r"
                + "\tg h\r\n";
            final Script script = PARSER.parse(new StringReader(code));
            assertEquals("", script.name());
        }
        
        @Test
        @DisplayName("Parse error")
        public void testParseError()
        {
            final String code = "x := 2\n�";
            final ErrorMessage thrown = assertThrows(
                    ErrorMessage.class,
                    () -> PARSER.parse(new StringReader(code)),
                    "parse");
            final String expectedMessage = ScriptMessages.parseError("�", 2, 1);
            assertEquals(
                    expectedMessage, thrown.getMessage(), "Thrown message");
        }
        
        @Test
        @DisplayName("Shebang")
        public void testShebang() throws ErrorMessage
        {
            final String code1
                = "#!/bin/calm\n"
                + "name on_1st_line\n";
            final String code2
                = "\r\n"
                + "#!/bin/calm\n"
                + "\n"
                + "name on_2nd_line\n";
            final String code3
                = "#!\r"
                + "\n"
                + "name empty_interpreter_directive\n";
            final String code4
                = "/* 1st line */\n"
                + "#!/bin/calm\n"
                + "name after_comment\n";
            final String code5
                = "name after_name\n"
                + "#!/bin/calm\n"
                + "\n";
            final String code6
                = "#/bin/calm\n"
                + "name missing_exclamation_mark\n";
            final Script script1 = PARSER.parse(new StringReader(code1));
            final Script script2 = PARSER.parse(new StringReader(code2));
            final Script script3 = PARSER.parse(new StringReader(code3));
            final Script script4 = PARSER.parse(new StringReader(code4));
            assertEquals("on_1st_line",                 script1.name());
            assertEquals("on_2nd_line",                 script2.name());
            assertEquals("empty_interpreter_directive", script3.name());
            assertEquals("after_comment",               script4.name());
            assertThrows(
                    ErrorMessage.class,
                    () -> PARSER.parse(new StringReader(code5)),
                    "After name");
            assertThrows(
                    ErrorMessage.class,
                    () -> PARSER.parse(new StringReader(code6)),
                    "Missing exclamation mark");
        }
        
    }
    
    private static enum MockTracer implements Tracer
    {
        INSTANCE;

        @Override
        public ErrorMessage track(final String message, final Trace trace)
        {
            return errorMessage(message);
        }

        @Override
        public ErrorMessage track(final Throwable cause, final Trace trace)
        {
            return errorMessage(cause.getMessage()).withCause(cause);
        }

        @Override
        public List<Trace> trail(final ErrorMessage ex)
        {
            return List.of();
        }
        
    }

}
