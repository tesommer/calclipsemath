package test.com.calclipse.math.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.parser.SymbolTable;

@DisplayName("MockSymbolTable")
public final class MockSymbolTableTest extends SymbolTableTest
{
    public MockSymbolTableTest()
    {
    }

    @Override
    protected SymbolTable createSymbolTable()
    {
        return new MockSymbolTable();
    }
    
    @Test
    @DisplayName("lookUp")
    public void testLookUp()
    {
        final SymbolTable symbolTable = createSymbolTable();
        final Token token1 = Tokens.plain("a");
        final Token token2 = Tokens.plain("abc");
        final Token token3 = Tokens.plain("ba");
        final Token token4 = Tokens.plain("abcde");
        final Token token5 = Tokens.plain("xyz");
        symbolTable.add(token1);
        symbolTable.add(token2);
        symbolTable.add(token3);
        symbolTable.add(token4);
        symbolTable.add(token5);
        final var lookedUp = List.copyOf(symbolTable.lookUp('a'));
        assertEquals(
                3,
                lookedUp.size(),
                "{a, abc, ba, abcde, xyz}: lookUp('a').size()");
        assertSame(
                token1,
                lookedUp.get(0),
                "{a, abc, ba, abcde, xyz}: lookUp('a'): index 0");
        assertSame(
                token2,
                lookedUp.get(1),
                "{a, abc, ba, abcde, xyz}: lookUp('a'): index 1");
        assertSame(
                token4,
                lookedUp.get(2),
                "{a, abc, ba, abcde, xyz}: lookUp('a'): index 2");
    }

}
