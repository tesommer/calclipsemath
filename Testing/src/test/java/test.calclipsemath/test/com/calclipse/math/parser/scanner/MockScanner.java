package test.com.calclipse.math.parser.scanner;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Scanner;

/**
 * A mock scanner that produces a fixed set of tokens.
 * 
 * @author Tone Sommerland
 */
public final class MockScanner implements Scanner
{
    private final Fragment[] fragments;
    private int index;

    public MockScanner(final Fragment... fragments)
    {
        this.fragments = fragments.clone();
    }

    @Override
    public void reset(final String expression) throws ErrorMessage
    {
        index = -1;
    }

    @Override
    public boolean hasNext() throws ErrorMessage
    {
        return ++index < fragments.length;
    }

    @Override
    public Fragment next() throws ErrorMessage
    {
        return fragments[index];
    }

}
