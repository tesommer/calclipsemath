package test.com.calclipse.math.matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.MatrixStringifier;
import com.calclipse.math.matrix.size.SizeViolation;

import test.TestUtil;

@DisplayName("Matrix")
public class MatrixTest
{
    public MatrixTest()
    {
    }
    
    /**
     * Factory for matrix instances to test.
     * @param entries may have any length
     * @implSpec
     * The default implementation returns a
     * {@link test.com.calclipse.math.matrix.MockMatrix}.
     */
    protected Matrix createMatrix(
            final int rowCount,
            final int columnCount,
            final double... entries)
    {
        return MockMatrix.valueOf(rowCount, columnCount, entries);
    }
    
      //////////////////////////////////////////////
     // Abstract methods and public counterparts //
    //////////////////////////////////////////////
    
    @Test
    @DisplayName("Set size larger and preserve")
    public final void testSetSizeLargerAndPreserve()
    {
        final Matrix mx = createMatrix(1, 1, 2);
        mx.setSize(2, 2, true);
        final Matrix expected = createMatrix(2, 2, 2, 0, 0, 0);
        assertEquals(expected, mx, "1x1 -> 2x2: preserve");
    }
    
    @Test
    @DisplayName("Set size equal and preserve")
    public final void testSetSizeEqualAndPreserve()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.setSize(2, 2, true);
        final Matrix expected = createMatrix(2, 2, 1, 2, 3, 4);
        assertEquals(expected, mx, "2x2 -> 2x2: preserve");
    }
    
    @Test
    @DisplayName("Set size smaller and preserve")
    public final void testSetSizeSmallerAndPreserve()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.setSize(2, 1, true);
        final Matrix expected = createMatrix(2, 1, 1, 3);
        assertEquals(expected, mx, "2x2 -> 2x1: preserve");
    }
    
    @Test
    @DisplayName("Set size larger and not preserve")
    public final void testSetSizeLargerAndNotPreserve()
    {
        final Matrix mx = createMatrix(1, 1, 2);
        mx.setSize(2, 2, false);
        final Matrix expected = createMatrix(2, 2, 0, 0, 0, 0);
        assertEquals(expected, mx, "1x1 -> 2x2: !preserve");
    }
    
    @Test
    @DisplayName("Set size equal and not preserve")
    public final void testSetSizeEqualAndNotPreserve()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.setSize(2, 2, false);
        final Matrix expected = createMatrix(2, 2, 0, 0, 0, 0);
        assertEquals(expected, mx, "2x2 -> 2x2: !preserve");
    }
    
    @Test
    @DisplayName("Set size smaller and not preserve")
    public final void testSetSizeSmallerAndNotPreserve()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.setSize(2, 1, false);
        final Matrix expected = createMatrix(2, 1, 0, 0);
        assertEquals(expected, mx, "2x2 -> 2x1: !preserve");
    }
    
      ///////////////////////////////
     // Methods accepting lambdas //
    ///////////////////////////////
    
    @Test
    @DisplayName("applyRowColumnFunction")
    public final void testApplyRowColumnFunction()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.applyRowColumnFunction((row, col) -> row - col);
        final Matrix expected = createMatrix(2, 2, 0, -1, 1, 0);
        assertEquals(expected, mx, "row - col");
    }

    @Test
    @DisplayName("applyRowColumnValueFunction")
    public final void testApplyRowColumnValueFunction()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.applyRowColumnValueFunction((row, col, val)
                -> row + col + 2 * val);
        final Matrix expected = createMatrix(2, 2, 2, 5, 7, 10);
        assertEquals(expected, mx, "row + col + 2 * val");
    }
    
    @Test
    @DisplayName("acceptRowColumnConsumer")
    public final void testAcceptRowColumnConsumer()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix copy = createMatrix(2, 2, 1, 2, 3, 4);
        mx.acceptRowColumnConsumer((row, col) -> copy.set(row, col, 21));
        final Matrix expectedMx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix expectedCopy = createMatrix(2, 2, 21, 21, 21, 21);
        assertEquals(expectedMx, mx, "Original");
        assertEquals(expectedCopy, copy, "21");
    }
    
    @Test
    @DisplayName("acceptRowColumnConsumer that modifies self")
    public final void testAcceptRowColumnConsumerThatModifiesSelf()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.acceptRowColumnConsumer((row, col) -> mx.set(row, col, 22));
        final Matrix expectedMx = createMatrix(2, 2, 22, 22, 22, 22);
        assertEquals(expectedMx, mx, "22");
    }
    
    @Test
    @DisplayName("acceptRowColumnValueConsumer")
    public final void testAcceptRowColumnValueConsumer()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix copy = createMatrix(2, 2, 1, 2, 3, 4);
        mx.acceptRowColumnValueConsumer((row, col, val)
                -> copy.set(row, col, 2 * val));
        final Matrix expectedMx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix expectedCopy = createMatrix(2, 2, 2, 4, 6, 8);
        assertEquals(expectedMx, mx, "Original");
        assertEquals(expectedCopy, copy, "2 * val");
    }
    
    @Test
    @DisplayName("acceptRowColumnValueConsumer that modifies self")
    public final void testAcceptRowColumnValueConsumerThatModifiesSelf()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.acceptRowColumnValueConsumer((row, col, val)
                -> mx.set(row, col, 2 * val));
        final Matrix expectedMx = createMatrix(2, 2, 2, 4, 6, 8);
        assertEquals(expectedMx, mx, "2 * val");
    }
    
      ////////////////////////////
     // Entry-cleaning methods //
    ////////////////////////////

    @Test
    @DisplayName("clean")
    public final void testClean()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final double belowDelta = mx.delta() / 2;
        final double atDelta = mx.delta();
        final double aboveDelta = mx.delta() * 2;
        mx.set(0, 0, belowDelta);
        mx.set(0, 1, atDelta);
        mx.set(1, 0, aboveDelta);
        mx.clean();
        final Matrix expected = createMatrix(2, 2, 0, 0, aboveDelta, 4);
        assertEquals(expected, mx, "Cleaned matrix");
    }
    
      ////////////////////////////////////////////
     // Entry-access and -manipulation methods //
    ////////////////////////////////////////////

    @Test
    @DisplayName("partition")
    public final void testPartition()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix partition = mx.partition(1, 0, 2, 1);
        final Matrix expected = createMatrix(1, 1, 3);
        assertEquals(expected, partition, "getPartition");
    }

    @Test
    @DisplayName("setPartition")
    public final void testSetPartition()
    {
        final Matrix mx = createMatrix(
                3, 4, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        final Matrix partition = createMatrix(2, 2, 60, 70, 100, 110);
        mx.setPartition(1, 1, partition);
        final Matrix expected = createMatrix(
                3, 4, 1, 2, 3, 4, 5, 60, 70, 8, 9, 100, 110, 12);
        assertEquals(expected, mx, "setPartition");
    }

    @Test
    @DisplayName("row")
    public final void testRow()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix row = mx.row(1);
        final Matrix expected = createMatrix(1, 2, 3, 4);
        assertEquals(expected, row, "getRow");
    }

    @Test
    @DisplayName("setRow")
    public final void testSetRow()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix row = createMatrix(1, 2, 42, 53);
        mx.setRow(1, row);
        final Matrix expected = createMatrix(2, 2, 1, 2, 42, 53);
        assertEquals(expected, mx, "setRow");
    }

    @Test
    @DisplayName("column")
    public final void testColumn()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix column = mx.column(1);
        final Matrix expected = createMatrix(2, 1, 2, 4);
        assertEquals(expected, column, "getColumn");
    }

    @Test
    @DisplayName("setColumn")
    public final void testSetColumn()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix column = createMatrix(2, 1, 42, 53);
        mx.setColumn(1, column);
        final Matrix expected = createMatrix(2, 2, 1, 42, 3, 53);
        assertEquals(expected, mx, "setColumn");
    }

    @Test
    @DisplayName("swapRows")
    public final void testSwapRows()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.swapRows(0, 1);
        final Matrix expected = createMatrix(2, 2, 3, 4, 1, 2);
        assertEquals(expected, mx, "swapRows");
    }

    @Test
    @DisplayName("swapColumns")
    public final void testSwapColumns()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.swapColumns(0, 1);
        final Matrix expected = createMatrix(2, 2, 2, 1, 4, 3);
        assertEquals(expected, mx, "swapColumns");
    }
    
      //////////////////
     // Math methods //
    //////////////////

    @Test
    @DisplayName("scaleRow")
    public final void testScaleRow()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.scaleRow(0, 2);
        final Matrix expected = createMatrix(2, 2, 2, 4, 3, 4);
        assertEquals(expected, mx, "scaleRow");
    }

    @Test
    @DisplayName("scaleColumn")
    public final void testScaleColumn()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.scaleColumn(1, 2);
        final Matrix expected = createMatrix(2, 2, 1, 4, 3, 8);
        assertEquals(expected, mx, "scaleColumn");
    }

    @Test
    @DisplayName("scale")
    public final void testScale()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.scale(11);
        final Matrix expected = createMatrix(2, 2, 11, 22, 33, 44);
        assertEquals(expected, mx, "scale");
    }

    @Test
    @DisplayName("negated")
    public final void testNegated()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix negated = mx.negated();
        final Matrix expected = createMatrix(2, 2, -1, -2, -3, -4);
        assertEquals(expected, negated, "negated");
    }

    @Test
    @DisplayName("transposed")
    public final void testTransposed()
    {
        final Matrix mx = createMatrix(2, 3, 1, 2, 3, 4, 5, 6);
        final Matrix trans = mx.transposed();
        final Matrix expected = createMatrix(3, 2, 1, 4, 2, 5, 3, 6);
        assertEquals(expected, trans, "transposed");
    }

    @Test
    @DisplayName("identity")
    public final void testIdentity()
    {
        final Matrix identity = createMatrix(0, 0).identity(2);
        final Matrix expected = createMatrix(2, 2, 1, 0, 0, 1);
        assertEquals(expected, identity, "identity");
    }

    @Test
    @DisplayName("plus")
    public final void testPlus()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix identity = createMatrix(2, 2, 1, 0, 0, 1);
        final Matrix result = mx.plus(mx).plus(identity);
        final Matrix expected = createMatrix(2, 2, 3, 4, 6, 9);
        assertEquals(expected, result, "plus");
    }

    @Test
    @DisplayName("addToThis")
    public final void testAddToThis()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix identity = createMatrix(2, 2, 1, 0, 0, 1);
        mx.addToThis(identity);
        final Matrix expected = createMatrix(2, 2, 2, 2, 3, 5);
        assertEquals(expected, mx, "addToThis");
    }
    
    @Test
    @DisplayName("addToThis: self to self")
    public final void testAddThisToThis()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.addToThis(mx);
        final Matrix expected = createMatrix(2, 2, 2, 4, 6, 8);
        assertEquals(expected, mx, "addToThis: self to self");
    }

    @Test
    @DisplayName("minus")
    public final void testMinus()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix identity = createMatrix(2, 2, 1, 0, 0, 1);
        final Matrix result = mx.minus(mx).minus(identity);
        final Matrix expected = createMatrix(2, 2, -1, 0, 0, -1);
        assertEquals(expected, result, "minus");
    }

    @Test
    @DisplayName("subtractFromThis")
    public final void testSubtractFromThis()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix identity = createMatrix(2, 2, 1, 0, 0, 1);
        mx.subtractFromThis(identity);
        final Matrix expected = createMatrix(2, 2, 0, 2, 3, 3);
        assertEquals(expected, mx, "subtractFromThis");
    }
    
    @Test
    @DisplayName("subtractFromThis: self from self")
    public final void testSubtractThisFromThis()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        mx.subtractFromThis(mx);
        final Matrix expected = createMatrix(2, 2, 0, 0, 0, 0);
        assertEquals(expected, mx, "subtractFromThis: self from self");
    }

    @Test
    @DisplayName("Times matrix")
    public final void testTimesMatrix()
    {
        final Matrix mx = createMatrix(2, 3, 1, 2, 3, 4, 5, 6);
        final Matrix mx2 = createMatrix(3, 2, 7, 8, 9, 10, 11, 12);
        final Matrix expected = createMatrix(2, 2, 58, 64, 139, 154);
        assertEquals(expected, mx.times(mx2), "times(Matrix)");
    }

    @Test
    @DisplayName("Times scalar")
    public final void testTimesScalar()
    {
        final Matrix mx = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix result = mx.times(3.0);
        final Matrix expected = createMatrix(2, 2, 3, 6, 9, 12);
        assertEquals(expected, result, "times(double)");
    }

    @Test
    @DisplayName("Divided by scalar")
    public final void testDividedByScalar()
    {
        final Matrix mx = createMatrix(2, 2, 9, 27, 6, 24);
        final Matrix result = mx.dividedBy(3.0);
        final Matrix expected = createMatrix(2, 2, 3, 9, 2, 8);
        assertEquals(expected, result, "dividedBy(double)");
    }

    @Test
    @DisplayName("dot")
    public final void testDot()
    {
        final Matrix vec1 = createMatrix(3, 1, 1, 2, 3);
        final Matrix vec2 = createMatrix(3, 1, 4, 5, 6);
        final double expected = 32;
        assertEquals(expected, vec1.dot(vec2), 1e-11, "dot");
    }
    
    @Test
    @DisplayName("cross")
    public final void testCross()
    {
        final Matrix vec1 = createMatrix(3, 1, 1, 2, 3);
        final Matrix vec2 = createMatrix(3, 1, 4, 5, 6);
        final Matrix expected = createMatrix(3, 1, -3, 6, -3);
        assertEquals(expected, vec1.cross(vec2), "cross");
    }
    
    @Test
    @DisplayName("addRowMultiple")
    public final void testAddRowMultiple()
    {
        final Matrix mx = createMatrix(3, 3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        mx.addRowMultiple(0, 1, 2, 2);
        final Matrix expected = createMatrix(3, 3, 1, 2, 3, 4, 5, 6, 7, 12, 15);
        assertEquals(expected, mx, "addRowMultiple");
    }
    
    @Test
    @DisplayName("addRowMultiple to same row")
    public final void testAddRowMultipleToSameRow()
    {
        final Matrix mx = createMatrix(3, 3, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        mx.addRowMultiple(1, 1, 2, 1);
        final Matrix expected = createMatrix(3, 3, 1, 2, 3, 4, 15, 18, 7, 8, 9);
        assertEquals(expected, mx, "addRowMultiple");
    }
    
      ////////////////////
     // Object methods //
    ////////////////////
    
    @Test
    @DisplayName("Equals with NaN entry")
    public final void testEqualsWithNaNEntry()
    {
        final Matrix mx = createMatrix(1, 1, Double.NaN);
        final Matrix mx2 = createMatrix(1, 1, Double.NaN);
        assertTrue(mx.equals(mx2), "Assert [NaN] == [NaN]");
    }
    
    @Test
    @DisplayName("hashCode")
    public final void testHashCode()
    {
        final Matrix mx1 = createMatrix(0, 0);
        final Matrix mx2 = createMatrix(2, 2, 1, 2, 3, 4);
        final Matrix mx3 = createMatrix(3, 1, 10, 20, 30);
        final Matrix mx4 = createMatrix(1, 3, 11, 21, 31);
        final Matrix mx5 = createMatrix(0, 0).create(50, 60);
        final Matrix mx6 = createMatrix(0, 0).identity(70);
        final Matrix mx7 = createMatrix(1, 1, 1);
        final Matrix mx8 = createMatrix(0, 2);
        final Matrix mx9 = createMatrix(2, 0);
        final Matrix mx10 = createMatrix(
                3, 4, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89);
        final Matrix mx11 = createMatrix(0, 0).identity(70);
        mx11.swapRows(0, 2);
        final Matrix mx12 = createMatrix(0, 0).identity(3);
        mx12.swapRows(0, 1);
        final Matrix mx13 = createMatrix(0, 0).identity(3);
        mx13.swapRows(0, 2);
        final Matrix mx14 = createMatrix(0, 0).identity(3);
        final Matrix mx15 = createMatrix(2, 3, Double.NaN);
        assertHashCodeConsistency(
                15,
                mx1, mx2, mx3, mx4, mx5, mx6, mx7,
                mx8, mx9, mx10, mx11, mx12, mx13, mx14,
                mx15);
        final Matrix mx_a = createMatrix(1, 3, 1, 2, 3);
        final Matrix mx_b = createMatrix(1, 3, 2, 3, 4);
        final Matrix mx_c = createMatrix(3, 1, 1, 2, 3);
        final Matrix mx_d = createMatrix(3, 1, 2, 3, 4);
        assertHashCodeConsistency(4, mx_a, mx_b, mx_c, mx_d);
    }
    
      ///////////
     // Misc. //
    ///////////
    
    @Test
    @DisplayName("Various operations on zero-sized matrices")
    public final void testVariousOperationsOnZeroSizedMatrices()
    {
        final Matrix mx_0x0 = createMatrix(0, 0);
        final Matrix mx_0x2 = createMatrix(0, 2);
        final Matrix mx_2x0 = createMatrix(2, 0);
        
        mx_0x0.clean();
        mx_0x0.scale(1001);
        mx_0x0.applyRowColumnFunction((r, c) -> 1002);
        mx_0x0.addToThis(mx_0x0);
        mx_0x0.subtractFromThis(mx_0x0);
        mx_0x2.clean();
        mx_0x2.swapColumns(0, 1);
        mx_0x2.scale(1001);
        mx_0x2.applyRowColumnFunction((r, c) -> 1002);
        mx_0x2.addToThis(mx_0x2);
        mx_0x2.subtractFromThis(mx_0x2);
        mx_2x0.clean();
        mx_2x0.swapRows(0, 1);
        mx_2x0.scale(1001);
        mx_2x0.applyRowColumnFunction((r, c) -> 1002);
        mx_2x0.addToThis(mx_2x0);
        mx_2x0.subtractFromThis(mx_2x0);
        TestUtil.assertZeroByZero(mx_0x0);
        TestUtil.assertZeroRows(2, mx_0x2);
        TestUtil.assertZeroColumns(2, mx_2x0);
        
        //partition
        TestUtil.assertZeroByZero(createMatrix(2, 2, 1, 2, 3, 4)
                .partition(1, 1, 1, 1));
        TestUtil.assertZeroByZero(createMatrix(2, 2, 1, 2, 3, 4)
                .partition(2, 2, 2, 2));
        TestUtil.assertZeroRows(1, createMatrix(2, 2, 1, 2, 3, 4)
                .partition(1, 0, 1, 1));
        TestUtil.assertZeroColumns(1, createMatrix(2, 2, 1, 2, 3, 4)
                .partition(0, 1, 1, 1));
        
        // row and column
        TestUtil.assertZeroColumns(1, mx_2x0.row(0));
        TestUtil.assertZeroRows(1, mx_0x2.column(0));
        
        // negate
        TestUtil.assertZeroByZero(mx_0x0.negated());
        TestUtil.assertZeroRows(2, mx_0x2.negated());
        TestUtil.assertZeroColumns(2, mx_2x0.negated());
        
        // transpose
        TestUtil.assertZeroByZero(mx_0x0.transposed());
        TestUtil.assertZeroColumns(2, mx_0x2.transposed());
        TestUtil.assertZeroRows(2, mx_2x0.transposed());
        
        // add
        TestUtil.assertZeroByZero(mx_0x0.plus(mx_0x0));
        TestUtil.assertZeroColumns(2, mx_2x0.plus(mx_2x0));
        TestUtil.assertZeroRows(2, mx_0x2.plus(mx_0x2));
        
        // subtract
        TestUtil.assertZeroByZero(mx_0x0.minus(mx_0x0));
        TestUtil.assertZeroColumns(2, mx_2x0.minus(mx_2x0));
        TestUtil.assertZeroRows(2, mx_0x2.minus(mx_0x2));
        
        // multiply by matrix
        TestUtil.assertZeroByZero(mx_0x0.times(mx_0x0));
        assertEquals(
                createMatrix(2, 2, 0, 0, 0, 0),
                mx_2x0.times(mx_0x2),
                "(2x0) * (0x2)");
        TestUtil.assertZeroByZero(mx_0x2.times(mx_2x0));
        
        // multiply by scalar
        TestUtil.assertZeroByZero(mx_0x0.times(21));
        TestUtil.assertZeroColumns(2, mx_2x0.times(22));
        TestUtil.assertZeroRows(2, mx_0x2.times(23));
        
        // dot product
        assertEquals(
                0,
                createMatrix(0, 1).dot(createMatrix(0, 1)),
                1e-7,
                "(0x1).dot(0x1)");
    }
    
    @Test
    public final void
    modifying_an_unmodifiable_matrix_throws_unsupported_operation
    ()
    {
        final Matrix mx = createMatrix(2, 2, 0, 2, 3, 4).unmodifiable();
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.set(0, 0, 1.2),
                "set");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.setSize(2, 2, true),
                "setSize");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.setDelta(.01),
                "setDelta");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.setStringifier(MatrixStringifier.standard()),
                "setStringifier");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.applyRowColumnFunction(
                        (row, column) -> 1.2),
                "applyRowColumnFunction");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.applyRowColumnValueFunction(
                        (row, column, value) -> 1.2),
                "applyRowColumnValueFunction");
        mx.clean(0, 1);
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.clean(0, 0),
                "clean(row, col)");
        mx.partition(0, 1, 2, 2).unmodifiable().clean();
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.clean(),
                "clean()");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.setPartition(0, 0, createMatrix(1, 1)),
                "setPartition");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.setRow(0, createMatrix(1, 2)),
                "setRow");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.setColumn(0, createMatrix(2, 1)),
                "setColumn");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.swapRows(0, 1),
                "swapRows");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.swapColumns(0, 1),
                "swapColumns");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.scaleRow(0, 1.2),
                "scaleRow");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.scaleColumn(0, 1.2),
                "scaleColumn");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.scale(1.2),
                "scale");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.addToThis(createMatrix(2, 2)),
                "+=");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.subtractFromThis(createMatrix(2, 2)),
                "-=");
        assertThrows(
                UnsupportedOperationException.class,
                () -> mx.addRowMultiple(0, 0, 1.2, 1),
                "addRowMultiple");
    }
    
    @Test
    @DisplayName("Size violations")
    public final void testSizeViolations()
    {
        // setRow
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).setRow(0, createMatrix(2, 2)),
                "setRow1");
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).setRow(0, createMatrix(1, 3)),
                "setRow2");
        // setColumn
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).setColumn(0, createMatrix(2, 2)),
                "setColumn1");
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).setColumn(0, createMatrix(3, 1)),
                "setColumn2");
        // +
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).plus(createMatrix(2, 3)),
                "+");
        // +=
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).addToThis(createMatrix(2, 3)),
                "+=");
        // -
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).minus(createMatrix(2, 3)),
                "-");
        // -=
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 2).subtractFromThis(createMatrix(2, 3)),
                "-=");
        // *
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 3).times(createMatrix(2, 2)),
                "*");
        // dot
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 1).dot(createMatrix(2, 2)),
                "dot1");
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(2, 1).dot(createMatrix(3, 1)),
                "dot2");
        // cross
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(3, 1).cross(createMatrix(3, 2)),
                "cross1");
        assertThrows(
                SizeViolation.class,
                () -> createMatrix(4, 1).cross(createMatrix(4, 1)),
                "cross2");
    }
    
    @Test
    @DisplayName("Index violations")
    public final void testIndexViolations()
    {
        // get
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).get(-1, 0),
                "get1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).get(2, 0),
                "get2");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).get(0, -1),
                "get3");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).get(0, 2),
                "get4");
        // set
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).set(-1, 0, 1.2),
                "set1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).set(2, 0, 3.4),
                "set2");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).set(0, -1, 5.6),
                "set3");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).set(0, 2, 7.8),
                "set4");
        // clean
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).clean(-1, 0),
                "clean1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).clean(2, 0),
                "clean2");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).clean(0, -1),
                "clean3");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).clean(0, 2),
                "clean4");
        // partition
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).partition(-1, 0, 2, 2),
                "getPartition1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).partition(0, 0, 3, 2),
                "getPartition2");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).partition(0, -1, 2, 2),
                "getPartition3");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).partition(0, 0, 2, 3),
                "getPartition4");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).partition(1, 0, 0, 2),
                "getPartition5");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).partition(0, 1, 2, 0),
                "getPartition6");
        // partition
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setPartition(
                        -1, 0, createMatrix(2, 2)),
                "setPartition1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setPartition(
                        1, 0, createMatrix(2, 2)),
                "setPartition2");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setPartition(
                        0, -1, createMatrix(2, 2)),
                "setPartition3");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setPartition(
                        0, 1, createMatrix(2, 2)),
                "setPartition4");
        // row
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).row(-1),
                "getRow1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).row(2),
                "getRow2");
        // setRow
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setRow(-1, createMatrix(1, 2)),
                "setRow1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setRow(2, createMatrix(1, 2)),
                "setRow2");
        // column
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).column(-1),
                "getColumn1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).column(2),
                "getColumn2");
        // setColumn
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setColumn(-1, createMatrix(2, 1)),
                "setColumn1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).setColumn(2, createMatrix(2, 1)),
                "setColumn2");
        // swapRows
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).swapRows(-1, 1),
                "swapRows1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).swapRows(0, 2),
                "swapRows2");
        // swapColumns
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).swapColumns(-1, 1),
                "swapColumns1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).swapColumns(0, 2),
                "swapColumns2");
        // scaleRow
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).scaleRow(-1, 1.2),
                "scaleRow1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).scaleRow(2, 3.4),
                "scaleRow2");
        // scaleColumn
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).scaleColumn(-1, 1.2),
                "scaleColumn1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).scaleColumn(2, 3.4),
                "scaleColumn2");
        // addRowMultiple
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).addRowMultiple(-1, 0, 2.3, 1),
                "addRowMultiple1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).addRowMultiple(2, 0, 2.3, 1),
                "addRowMultiple2");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).addRowMultiple(0, -1, 2.3, 1),
                "addRowMultiple3");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).addRowMultiple(0, 2, 2.3, 1),
                "addRowMultiple4");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).addRowMultiple(0, 0, 2.3, -1),
                "addRowMultiple5");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> createMatrix(2, 2).addRowMultiple(0, 0, 2.3, 2),
                "addRowMultiple6");
    }
    
    @Test
    public final void
    derivatives_retain_attributes_of_original
    ()
    {
        final Matrix original = createMatrix(2, 2, 1, 2, 3, 4);
        final double delta = 0.1234 + original.delta();
        final MatrixStringifier stringifier = MatrixStringifier.composedOf(
                (m, es) -> "<>", d -> "");
        original.setDelta(delta);
        original.setStringifier(stringifier);
        assertHaveSameAttributes(
                original,
                original.unmodifiable(),
                "unmodifiable");
        assertHaveSameAttributes(
                original,
                original.copy(),
                "copy");
        assertHaveSameAttributes(
                original,
                original.create(1, 1),
                "create");
        assertHaveSameAttributes(
                original,
                original.partition(1, 1, 2, 2),
                "partition");
        assertHaveSameAttributes(
                original,
                original.row(0),
                "row");
        assertHaveSameAttributes(
                original,
                original.column(0),
                "column");
        assertHaveSameAttributes(
                original,
                original.negated(),
                "negated");
        assertHaveSameAttributes(
                original,
                original.transposed(),
                "transposed");
        assertHaveSameAttributes(
                original,
                original.identity(3),
                "identity");
        assertHaveSameAttributes(
                original,
                original.plus(original),
                "plus");
        assertHaveSameAttributes(
                original,
                original.minus(original),
                "minus");
        assertHaveSameAttributes(
                original,
                original.times(original),
                "times matrix");
        assertHaveSameAttributes(
                original,
                original.times(1.2),
                "times scalar");
        assertHaveSameAttributes(
                original,
                original.dividedBy(3.4),
                "dividedBy");
        assertHaveSameAttributes(
                original,
                original.create(3, 1).cross(original.create(3, 1)),
                "cross");
    }
    
    @Test
    public final void
    sum_with_unmodifiable_term_is_modifiable_and_retains_original_attributes
    ()
    {
        final Matrix original = createMatrix(2, 2, 10, 11, 12, 13);
        final Matrix sum = original.unmodifiable().plus(original);
        sum.subtractFromThis(original);
        assertEquals(original, sum);
        assertHaveSameAttributes(original, sum, "Attributes");
    }
    
    private static void assertHashCodeConsistency(
            final int uniqueHashCodesRequired, final Matrix... matrices)
    {
        TestUtil.assertHashCodeConsistency(
                matrices,
                uniqueHashCodesRequired,
                mx -> new int[] {mx.copy().hashCode()});
    }
    
    private static void assertHaveSameAttributes(
            final Matrix original,
            final Matrix derivative,
            final String message)
    {
        assertEquals(
                original.delta(),
                derivative.delta(),
                message + ": delta");
        assertSame(
                original.stringifier(),
                derivative.stringifier(),
                message + ": stringifier");
    }

}
