package test.com.calclipse.math.parser.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.parser.misc.ArgUtil;

@DisplayName("ArgUtil")
public final class ArgUtilTest
{
    public ArgUtilTest()
    {
    }
    
    @Test
    @DisplayName("requireNonzeroArgCount")
    public void testRequireNonzeroArgCount() throws ErrorMessage
    {
        ArgUtil.requireNonzeroArgCount(-21);
        ArgUtil.requireNonzeroArgCount(-1);
        ArgUtil.requireNonzeroArgCount(1);
        ArgUtil.requireNonzeroArgCount(2);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> ArgUtil.requireNonzeroArgCount(0),
                "1: (0)");
        assertEquals(
                ArgMessages.expectedNonzeroArgCount(),
                thrown.getMessage(),
                "1: ex.getMessage()");
    }
    
    @Test
    @DisplayName("requireArgCount")
    public void testRequireArgCount() throws ErrorMessage
    {
        ArgUtil.requireArgCount(2, 2);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> ArgUtil.requireArgCount(1, 2),
                "2: (1, 2)");
        assertEquals(
                ArgMessages.expectedArgCount(1),
                thrown.getMessage(),
                "2: ex.getMessage()");
    }
    
    @Test
    @DisplayName("requireMinMaxArgCount")
    public void testRequireMinMaxArgCount() throws ErrorMessage
    {
        ArgUtil.requireMinMaxArgCount(1, 1, 1);
        ArgUtil.requireMinMaxArgCount(1, 2, 1);
        ArgUtil.requireMinMaxArgCount(1, 2, 2);
        ArgUtil.requireMinMaxArgCount(-1, 1, 0);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> ArgUtil.requireMinMaxArgCount(1, 2, 3),
                "3: (1, 2, 3)");
        assertEquals(
                ArgMessages.expectedMinMaxArgCount(1, 2),
                thrown.getMessage(),
                "3: ex.getMessage()");
    }
    
    @Test
    @DisplayName("requireMinimumArgCount")
    public void testRequireMinimumArgCount() throws ErrorMessage
    {
        ArgUtil.requireMinimumArgCount(-1, -1);
        ArgUtil.requireMinimumArgCount(-1, 0);
        ArgUtil.requireMinimumArgCount(0, 0);
        ArgUtil.requireMinimumArgCount(0, 1);
        ArgUtil.requireMinimumArgCount(1, 1);
        ArgUtil.requireMinimumArgCount(1, 2);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> ArgUtil.requireMinimumArgCount(2, 1),
                "4: (2, 1)");
        assertEquals(
                ArgMessages.expectedMinimumArgCount(2),
                thrown.getMessage(),
                "4: ex.getMessage()");
    }
    
    @Test
    @DisplayName("requireMaximumArgCount")
    public void testRequireMaximumArgCount() throws ErrorMessage
    {
        ArgUtil.requireMaximumArgCount(-1, -1);
        ArgUtil.requireMaximumArgCount(0, -1);
        ArgUtil.requireMaximumArgCount(0, 0);
        ArgUtil.requireMaximumArgCount(1, 0);
        ArgUtil.requireMaximumArgCount(1, 1);
        ArgUtil.requireMaximumArgCount(2, 1);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> ArgUtil.requireMaximumArgCount(1, 2),
                "5: (1, 2)");
        assertEquals(
                ArgMessages.expectedMaximumArgCount(1),
                thrown.getMessage(),
                "5: ex.getMessage()");
    }

}
