package test.com.calclipse.mcomp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.Contexts;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

@DisplayName("Contexts")
public final class ContextsTest
{
    private static final String ASSERT_MAPPING_NAME = "Funky S";
    private static final String MAPPING_NAMESPACE_SEPARATOR = "**";
    private static final Token ASSERT_MAPPING_EXPORT = Tokens.plain("t");

    public ContextsTest()
    {
    }
    
    @Test
    @DisplayName("configuredWithAndMappedTo")
    public void testConfiguredWithAndMappedTo() throws ErrorMessage
    {
        final McContext context = Contexts.configuredWithAndMappedTo(
                Contexts.SCRIPT_CONFIG, MyMapping::new);
        new AssertMappingMcomp().execute(context);
    }
    
    private static final class AssertMappingMcomp implements Mcomp
    {
        private AssertMappingMcomp()
        {
        }

        @Override
        public String name()
        {
            return ASSERT_MAPPING_NAME;
        }

        @Override
        public void execute(final McContext context) throws ErrorMessage
        {
            assertEquals(
                    MAPPING_NAMESPACE_SEPARATOR,
                    context.namespaceSeparator(),
                    "Mapping namespace separator");
            if (context.parent().isEmpty())
            {
                context.importMcomp(new AssertMappingMcomp());
                final String exportName
                    = ASSERT_MAPPING_NAME
                    + MAPPING_NAMESPACE_SEPARATOR
                    + ASSERT_MAPPING_EXPORT.name();
                assertTrue(
                        context.symbol(exportName).isPresent(),
                        "Name: assert-mapping export");
                assertSame(
                        ASSERT_MAPPING_EXPORT.type(),
                        context.symbol(exportName).get().type(),
                        "Type: assert-mapping export");
            }
            else
            {
                context.exportSymbol(ASSERT_MAPPING_EXPORT);
            }
        }
        
    }
    
    private static final class MyMapping implements McContext
    {
        private final McContext original;

        private MyMapping(final McContext original)
        {
            assert original != null;
            this.original = original;
        }

        @Override
        public Optional<McContext> parent()
        {
            return original.parent();
        }

        @Override
        public Expression parse(final String expression) throws ErrorMessage
        {
            return original.parse(expression);
        }

        @Override
        public boolean addSymbol(final Token symbol)
        {
            return original.addSymbol(symbol);
        }

        @Override
        public boolean removeSymbol(final String name)
        {
            return original.removeSymbol(name);
        }

        @Override
        public Optional<Token> symbol(final String name)
        {
            return original.symbol(name);
        }

        @Override
        public Collection<Token> symbols()
        {
            return original.symbols();
        }

        @Override
        public Collection<Token> lookUpSymbols(final char firstChar)
        {
            return original.lookUpSymbols(firstChar);
        }

        @Override
        public Optional<Mcomp> mcompAt(
                final String location,
                final String referrer) throws ErrorMessage
        {
            return original.mcompAt(location, referrer);
        }

        @Override
        public void exportSymbol(final Token symbol) throws ErrorMessage
        {
            original.exportSymbol(symbol);
        }

        @Override
        public void importMcomp(final Mcomp mcomp) throws ErrorMessage
        {
            original.importMcomp(mcomp);
        }

        @Override
        public Collection<Token> importedSymbols()
        {
            return original.importedSymbols();
        }

        @Override
        public void addLocator(final McLocator locator)
        {
            original.addLocator(locator);
        }

        @Override
        public McLocator[] clearLocators()
        {
            return original.clearLocators();
        }

        @Override
        public TypeContext typeContext()
        {
            return original.typeContext();
        }

        @Override
        public String namespaceSeparator()
        {
            return MAPPING_NAMESPACE_SEPARATOR;
        }
    }

}
