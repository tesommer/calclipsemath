package test.com.calclipse.math.parser.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.message.ArgMessages;
import com.calclipse.math.expression.message.EvalMessages;
import com.calclipse.math.expression.message.TypeMessages;
import com.calclipse.math.parser.misc.TypeUtil;

import test.com.calclipse.math.matrix.MockMatrix;

@DisplayName("TypeUtil")
public final class TypeUtilTest
{
    public TypeUtilTest()
    {
    }
    
    @Test
    @DisplayName("To double")
    public void testToDouble() throws ErrorMessage
    {
        final double delta = .00000001;
        assertEquals(Math.PI, TypeUtil.toDouble(Math.PI), delta, "pi");
        final Object notNumber = MockMatrix.valueOf(1, 1, 23);
        assertThrowsErrorMessage(
                TypeMessages.invalidType(notNumber),
                notNumber,
                TypeUtil::toDouble,
                TypeUtil::argToDouble);
        assertThrowsIllegalArgumentException(-1, 2.1, TypeUtil::argToDouble);
    }
    
    @Test
    @DisplayName("To long")
    public void testToLong() throws ErrorMessage
    {
        final Object notInteger = BigDecimal.valueOf(2.6);
        assertThrowsErrorMessage(
                EvalMessages.expectedInteger(),
                notInteger,
                TypeUtil::toLong,
                TypeUtil::argToLong);
        final Object notNumber = MockMatrix.valueOf(1, 1, 26);
        assertThrowsErrorMessage(
                TypeMessages.invalidType(notNumber),
                notNumber,
                TypeUtil::toLong,
                TypeUtil::argToLong);
        assertEquals(26L, TypeUtil.toLong(Double.valueOf(26)), "26.0");
        assertEquals(
                Long.MAX_VALUE - 1,
                TypeUtil.toLong(Long.MAX_VALUE - 1),
                "Long.MAX_VALUE - 1");
        assertEquals(
                Long.MAX_VALUE - 1,
                TypeUtil.toLong(BigInteger.valueOf(Long.MAX_VALUE - 1)),
                "BigInteger(Long.MAX_VALUE - 1)");
        assertThrowsIllegalArgumentException(0, 2.1, TypeUtil::argToLong);
    }
    
    @Test
    @DisplayName("isInt")
    public void testIsInt()
    {
        assertTrue(TypeUtil.isInt(Double.valueOf(2)), "2.0");
        assertFalse(TypeUtil.isInt(Double.valueOf(Math.PI)), "pi");
        assertTrue(TypeUtil.isInt(Integer.valueOf(7)), "7");
        assertFalse(TypeUtil.isInt(Long.MAX_VALUE), "Long.MAX_VALUE");
        assertFalse(TypeUtil.isInt(MockMatrix.valueOf(1, 1)), "[[0]]");
    }
    
    @Test
    @DisplayName("To int")
    public void testToInt() throws ErrorMessage
    {
        assertEquals(12, TypeUtil.toInt(Double.valueOf(12)), "12.0");
        final Object notInteger = 2.6;
        assertThrowsErrorMessage(
                EvalMessages.expectedInteger(),
                notInteger,
                TypeUtil::toInt,
                TypeUtil::argToInt);
        final Object notNumber = MockMatrix.valueOf(1, 1, 7);
        assertThrowsErrorMessage(
                TypeMessages.invalidType(notNumber),
                notNumber,
                TypeUtil::toInt,
                TypeUtil::argToInt);
        assertThrowsIllegalArgumentException(-2, 2.1, TypeUtil::argToInt);
    }
    
    private static void assertThrowsErrorMessage(
            final String expectedMessageWithoutArgNumber,
            final Object value,
            final ToSomething<?> toSomething,
            final ArgToSomething<?> argToSomething)
    {
        final ErrorMessage thrown1 = assertThrows(
                ErrorMessage.class,
                () -> argToSomething.apply(2, value),
                "To something with arg number: " + value);
        assertMessage(2, expectedMessageWithoutArgNumber, thrown1);
        final ErrorMessage thrown2 = assertThrows(
                ErrorMessage.class,
                () -> toSomething.apply(value),
                "To something without arg number: " + value);
        assertMessage(expectedMessageWithoutArgNumber, thrown2);
    }
    
    private static void assertMessage(
            final String expected, final Throwable actual)
    {
            assertEquals(
                    expected,
                    actual.getMessage(),
                    "Message without argument number");
    }
    
    private static void assertMessage(
            final int argNumber,
            final String expectedWithoutArgNumber,
            final Throwable actual)
    {
        assertEquals(
                ArgMessages.forArgument(argNumber, expectedWithoutArgNumber),
                actual.getMessage(),
                "Message with argument number");
    }
    
    private static void assertThrowsIllegalArgumentException(
            final int argNumber,
            final Object value,
            final ArgToSomething<?> argToSomething)
    {
        assertThrows(
                IllegalArgumentException.class,
                () -> argToSomething.apply(argNumber, value),
                "argument number: " + argNumber);
    }
    
    @FunctionalInterface
    private static interface ArgToSomething<R>
    {
        public abstract R apply(int argNumber, Object value)
                throws ErrorMessage;
    }
    
    @FunctionalInterface
    private static interface ToSomething<R>
    {
        public abstract R apply(Object value) throws ErrorMessage;
    }

}
