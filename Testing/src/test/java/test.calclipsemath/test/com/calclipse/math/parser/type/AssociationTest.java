package test.com.calclipse.math.parser.type;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.parser.type.Association;

@DisplayName("Association")
public final class AssociationTest
{
    public AssociationTest()
    {
    }
    
    @Test
    @DisplayName("equals(Association, Object)")
    public void testEqualsAssociationObject()
    {
        assertEqual(null, null, null, null);
        assertEqual("a", null, "a", null);
        assertEqual(null, "b", null, "b");
        assertEqual("x", "y", "x", "y");
        assertNotEqual("a", null, null, null);
        assertNotEqual(null, null, "b", null);
        assertNotEqual("x", "y", "x", null);
        assertNotEqual("x", "y", "x", "z");
        assertNotEqual("x", "y", "z", "y");
        assertNotEqual("x", "y", "X", "Y");
    }
    
    @Test
    @DisplayName("hashCode(Association)")
    public void testHashCodeAssociation()
    {
        assertHashCode(0, null, null);
        assertHashCode(0 ^ "a".hashCode(), null, "a");
        assertHashCode(0 ^ "b".hashCode(), "b", null);
        assertHashCode("x".hashCode() ^ "y".hashCode(), "x", "y");
    }
    
    private static void assertEqual(
            final Object key1,
            final Object value1,
            final Object key2,
            final Object value2)
    {
        assertTrue(
                Association.equals(
                        new MockAssociation(key1, value1),
                        new MockAssociation(key2, value2)),
                  "Assert equal: key1=" + key1 + ", value1=" + value1
                  + ", key2=" + key2 + ", value2=" +value2);
    }
    
    private static void assertNotEqual(
            final Object key1,
            final Object value1,
            final Object key2,
            final Object value2)
    {
        assertFalse(
                Association.equals(
                        new MockAssociation(key1, value1),
                        new MockAssociation(key2, value2)),
                  "Assert not equal: key1=" + key1 + ", value1=" + value1
                  + ", key2=" + key2 + ", value2=" +value2);
    }
    
    private static void assertHashCode(
            final int expected, final Object key, final Object value)
    {
        assertEquals(
                expected,
                new MockAssociation(key, value).hashCode(),
                "key=" + key + ", value=" + value);
    }

}
