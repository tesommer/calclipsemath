package test.com.calclipse.math.matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.Permutation;
import com.calclipse.math.matrix.size.SizeViolation;

import test.TestUtil;

@DisplayName("Permutation")
public final class PermutationTest
{
    public PermutationTest()
    {
    }

    @Test
    @DisplayName("get")
    public void testGet()
    {
        final Permutation p = new Permutation.Builder(3).build();
        assertEquals(1, p.get(0, 0), "get(0, 0)");
        assertEquals(0, p.get(1, 0), "get(1, 0)");
        assertEquals(0, p.get(2, 0), "get(2, 0)");
        assertEquals(0, p.get(0, 1), "get(0, 1)");
        assertEquals(1, p.get(1, 1), "get(1, 1)");
        assertEquals(0, p.get(2, 1), "get(2, 1)");
        assertEquals(0, p.get(0, 2), "get(0, 2)");
        assertEquals(0, p.get(1, 2), "get(1, 2)");
        assertEquals(1, p.get(2, 2), "get(2, 2)");
    }

    @Test
    @DisplayName("copyEntriesTo")
    public void testCopyEntriesTo()
    {
        final Permutation p = new Permutation.Builder(3).build();
        final Matrix matrix = MockMatrix.valueOf(
                2, 2,
                1, 2,
                3, 4);
        p.copyEntriesTo(matrix);
        final Matrix expected = MockMatrix.valueOf(
                3, 3,
                1, 0, 0,
                0, 1, 0,
                0, 0, 1);
        assertEquals(expected, matrix, "3x3 to 2x2");
    }
    
    @Test
    @DisplayName("preApply 3x3 after swapping 0 and 2")
    public void testPreApply3x3AfterSwapping0And2()
    {
        final Permutation p = new Permutation.Builder(3).swap(0, 2).build();
        final Matrix matrix = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        p.preApply(matrix);
        final Matrix expected = MockMatrix.valueOf(
                3, 3,
                7, 8, 9,
                4, 5, 6,
                1, 2, 3);
        assertEquals(expected, matrix, "P(3x3).swap(0, 2): P*M");
    }
    
    @Test
    @DisplayName("preApply 4x4 after swapping 1 and 3")
    public void testPreApply4x4AfterSwapping1And3()
    {
        final Permutation p = new Permutation.Builder(4).swap(1, 3).build();
        final Matrix matrix = MockMatrix.valueOf(
                4, 4,
                 1,  2,  3,  4,
                 5,  6,  7,  8,
                 9, 10, 11, 12,
                13, 14, 15, 16);
        p.preApply(matrix);
        final Matrix expected = MockMatrix.valueOf(
                4, 4,
                 1,  2,  3,  4,
                13, 14, 15, 16,
                 9, 10, 11, 12,
                 5,  6,  7,  8);
        assertEquals(expected, matrix, "P(4x4).swap(1, 3): P*M");
    }

    @Test
    @DisplayName("apply 3x3 after swapping 1 and 0")
    public void testApply3x3AfterSwapping1And0()
    {
        final Permutation p = new Permutation.Builder(3).swap(1, 0).build();
        final Matrix matrix = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        p.apply(matrix);
        final Matrix expected = MockMatrix.valueOf(
                3, 3,
                2, 1, 3,
                5, 4, 6,
                8, 7, 9);
        assertEquals(expected, matrix, "P(3x3).swap(1, 0): M*P");
    }
    
    @Test
    @DisplayName("apply 4x4 after swapping 0 and 3")
    public void testApply4x4AfterSwapping0And3()
    {
        final Permutation p = new Permutation.Builder(4).swap(0, 3).build();
        final Matrix matrix = MockMatrix.valueOf(
                4, 4,
                 1,  2,  3,  4,
                 5,  6,  7,  8,
                 9, 10, 11, 12,
                13, 14, 15, 16);
        p.apply(matrix);
        final Matrix expected = MockMatrix.valueOf(
                4, 4,
                 4,  2,  3,  1,
                 8,  6,  7,  5,
                12, 10, 11,  9,
                16, 14, 15, 13);
        assertEquals(expected, matrix, "P(4x4).swap(0, 3): M*P");
    }
    
    @Test
    @DisplayName("equals")
    public void testEquals()
    {
        final Permutation p_sz0_1 = new Permutation.Builder(0).build();
        final Permutation p_sz0_2 = new Permutation.Builder(0).build();
        final Permutation p_sz1_1 = new Permutation.Builder(1).build();
        final Permutation p_sz1_2 = new Permutation.Builder(1).build();
        final Permutation p_sz2_1 = new Permutation.Builder(2).build();
        final Permutation p_sz2_2 = new Permutation.Builder(2).build();
        final Permutation p_sz3_1 = new Permutation.Builder(3).build();
        final Permutation p_sz3_2 = new Permutation.Builder(3).build();
        assertEquals(p_sz0_1, p_sz0_2, "0x0 == 0x0");
        assertEquals(p_sz1_1, p_sz1_2, "1x1 == 1x1");
        assertEquals(p_sz2_1, p_sz2_2, "2x2 == 2x2");
        assertEquals(p_sz3_1, p_sz3_2, "3x3 == 3x3");
        assertEquals(p_sz0_1, p_sz0_1, "0x0 == 0x0: same");
        assertEquals(p_sz1_1, p_sz1_1, "1x1 == 1x1: same");
        assertEquals(p_sz3_1, p_sz3_1, "3x3 == 3x3: same");
        assertNotEquals(p_sz0_1, p_sz2_1, "0x0 == 2x2");
        assertNotEquals(p_sz2_1, p_sz3_1, "2x2 == 3x3");
        assertNotEquals(
                new Permutation.Builder(p_sz2_1).swap(0, 1).build(),
                p_sz2_2,
                "2x2.swap(0, 1) == 2x2");
        assertEquals(
                new Permutation.Builder(p_sz2_1).swap(0, 1).build(),
                new Permutation.Builder(p_sz2_2).swap(0, 1).build(),
                "2x2.swap(0, 1) == 2x2.swap(0, 1)");
    }
    
    @Test
    @DisplayName("hashCode")
    public void testHashCode()
    {
        final Permutation p1 = new Permutation.Builder(0).build();
        final Permutation p2 = new Permutation.Builder(1).build();
        final Permutation p3 = new Permutation.Builder(2).build();
        final Permutation p4 = new Permutation.Builder(2).swap(0, 1).build();
        final Permutation p5 = new Permutation.Builder(3).build();
        final Permutation p6 = new Permutation.Builder(3).swap(0, 1).build();
        final Permutation p7 = new Permutation.Builder(3).swap(0, 2).build();
        final Permutation p8 = new Permutation.Builder(3).swap(1, 2).build();
        final Permutation p9 = new Permutation.Builder(4).swap(0, 2).build();
        final Permutation p10 = new Permutation.Builder(4).swap(1, 3).build();
        final Permutation p11 = new Permutation.Builder(100).build();
        assertHashCodeConsistency(
                11,
                p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11);
    }
    
    @Test
    @DisplayName("Zero-sized permutation")
    public void testZeroSizedPermutation()
    {
        final Permutation p = new Permutation.Builder(0).build();
        assertEquals(0, p.rows(), "P.rows()");
        final Matrix mx = MockMatrix.valueOf(2, 2, 1, 2, 3, 4);
        p.copyEntriesTo(mx);
        TestUtil.assertZeroByZero(mx);
        p.preApply(mx);
        TestUtil.assertZeroByZero(mx);
        p.apply(mx);
        TestUtil.assertZeroByZero(mx);
        final Matrix mx_2x0 = MockMatrix.valueOf(2, 0);
        p.apply(mx_2x0);
        TestUtil.assertZeroColumns(2, mx_2x0);
    }
    
    @Test
    @DisplayName("Size violations")
    public void testSizeViolations()
    {
        final Permutation p = new Permutation.Builder(3).build();
        final Matrix a = MockMatrix.valueOf(4, 3);
        final Matrix b = MockMatrix.valueOf(3, 4);
        assertThrows(SizeViolation.class, () -> p.preApply(a), "P*A");
        assertThrows(SizeViolation.class, () -> p.apply(b), "B*P");
    }
    
    @Test
    @DisplayName("Index violations")
    public void testIndexViolations()
    {
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> new Permutation.Builder(2).swap(1, 2),
                "2x2.swap(1, 2)");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> new Permutation.Builder(2).swap(1, -1),
                "2x2.swap(1, -1");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> new Permutation.Builder(2).build().get(1, 2),
                "2x2.get(1, 2)");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> new Permutation.Builder(2).build().get(1, -1),
                "2x2.get(1, -1");
    }
    
    @Test
    @DisplayName("Argument violations")
    public void testArgumentViolations()
    {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Permutation.Builder(-1),
                "Negative size");
    }
    
    private static void assertHashCodeConsistency(
            final int uniqueHashCodesRequired,
            final Permutation... permutations)
    {
        TestUtil.assertHashCodeConsistency(
                permutations,
                uniqueHashCodesRequired,
                p -> new int[] {new Permutation.Builder(p).build().hashCode()});
    }

}
