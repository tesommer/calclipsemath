package test.com.calclipse.math;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.Complex;

import test.TestUtil;

@DisplayName("Complex")
public final class ComplexTest
{
    private static final double DELTA = 1E-13;

    public ComplexTest()
    {
    }
    
    @Test
    @DisplayName("Complex")
    public void testComplex()
    {
        final Complex complex1 = Complex.valueOf(0, 0);
        final Complex complex2 = Complex.valueOf(-0, 0);
        final Complex complex3 = Complex.valueOf(0, -0);
        final Complex complex4 = Complex.valueOf(-0, -0);
        final Complex complex5 = Complex.valueOf(0, 1);
        final Complex complex6 = Complex.valueOf(1, 0);
        final Complex complex7 = Complex.valueOf(-0, 1);
        final Complex complex8 = Complex.valueOf(1, -0);
        final Complex complex9 = Complex.valueOf(1, -1);
        final Complex complex10 = Complex.valueOf(-1, 1);
        final Complex complex11 = Complex.valueOf(-1, -1);
        final Complex complex12 = Complex.valueOf(2, -3);
        final Complex complex13 = Complex.valueOf(-5, 7);
        final Complex complex14 = Complex.valueOf(-11, -13);
        final Complex complex15 = Complex.valueOf(0, -17);
        final Complex complex16 = Complex.valueOf(-19, -0);
        final Complex complex17 = Complex.valueOf(1, 1);
        final Complex complex18 = Complex.valueOf(
                Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
        final Complex complex19 = Complex.valueOf(
                Double.POSITIVE_INFINITY, 0);
        final Complex complex20 = Complex.valueOf(Double.NaN, 1);
        final Complex complex21 = Complex.valueOf(1, Double.NaN);
        final Complex complex22 = Complex.valueOf(
                Double.NaN, Double.POSITIVE_INFINITY);
        assertOperatingWithinEstablishedParameters(
                14,
                complex1,
                complex2,
                complex3,
                complex4,
                complex5,
                complex6,
                complex7,
                complex8,
                complex9,
                complex10,
                complex11,
                complex12,
                complex13,
                complex14,
                complex15,
                complex16,
                complex17,
                complex18,
                complex19,
                complex20,
                complex21,
                complex22);
    }
    
    @Test
    @DisplayName("equals with NaN")
    public void testEqualsWithNaN()
    {
        final Complex complex1 = Complex.valueOf(Double.NaN, 0);
        final Complex complex2 = Complex.valueOf(0, Double.NaN);
        final Complex complex3 = Complex.valueOf(Double.NaN, Double.NaN);
        assertTrue(
                complex1.equals(Complex.valueOf(
                        complex1.real(), complex1.imaginary())),
                "Assert true (NaN, 0) equals (NaN, 0)");
        assertTrue(
                complex2.equals(Complex.valueOf(
                        complex2.real(), complex2.imaginary())),
                "Assert true (0, NaN) equals (0, NaN)");
        assertTrue(
                complex3.equals(Complex.valueOf(
                        complex3.real(), complex3.imaginary())),
                "Assert true (NaN, NaN) equals (NaN, NaN)");
        assertFalse(
                complex1.equals(complex2),
                "Assert false (NaN, 0) equals (0, NaN)");
    }
    
    private static void assertOperatingWithinEstablishedParameters(
            final int uniqueHashCodesRequired,
            final Complex... complexes)
    {
        assertAbsConjugateNegate(complexes);
        assertAddSubtractMultiplyDivide(complexes);
        assertEqualsHashCode(uniqueHashCodesRequired, complexes);
    }
    
    private static void assertAbsConjugateNegate(
            final Complex... complexes)
    {
        for (final Complex complex : complexes)
        {
            assertAbs(complex);
            assertConjugate(complex);
            assertNegate(complex);
        }
    }
    
    private static void assertAddSubtractMultiplyDivide(
            final Complex... complexes)
    {
        final var scalars = new double[complexes.length * 2];
        for (int i = 0; i < scalars.length - 1; i += 2)
        {
            scalars[i] = complexes[i / 2].real();
            scalars[i + 1] = complexes[i / 2].imaginary();
        }
        for (final Complex c1 : complexes)
        {
            for (final double d2 : scalars)
            {
                assertAdd(c1, d2);
                assertSubtract(c1, d2);
                assertMultiply(c1, d2);
                assertDivide(c1, d2);
            }
            for (final Complex c2 : complexes)
            {
                assertAdd(c1, c2);
                assertSubtract(c1, c2);
                assertMultiply(c1, c2);
                assertDivide(c1, c2);
            }
        }
    }
    
    private static void assertEqualsHashCode(
            final int uniqueHashCodesRequired, final Complex... complexes)
    {
        for (final Complex c1 : complexes)
        {
            for (final Complex c2 : complexes)
            {
                assertSameHashCodeIfEqual(c1, c2);
            }
        }
        assertHashCodeConsistency(uniqueHashCodesRequired, complexes);
    }
    
    private static void assertAbs(final Complex complex)
    {
        final double re = complex.real();
        final double im = complex.imaginary();
        final double expected = Math.sqrt(re * re + im * im);
        final double actual = complex.abs();
        Assertions.assertEquals(expected, actual, DELTA, complex + ".abs()");
    }
    
    private static void assertConjugate(final Complex complex)
    {
        final double re = complex.real();
        final double im = complex.imaginary();
        final Complex expected = Complex.valueOf(re, -im);
        final Complex actual = complex.conjugate();
        assertEquals(expected, actual, complex + ".conjugate()");
    }
    
    private static void assertNegate(final Complex complex)
    {
        final double re = complex.real();
        final double im = complex.imaginary();
        final Complex expected = Complex.valueOf(-re, -im);
        final Complex actual = complex.negated();
        assertEquals(expected, actual, complex + ".negated()");
    }
    
    private static void assertAdd(final Complex c1, final Complex c2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() + c2.real(),
                c1.imaginary() + c2.imaginary());
        final Complex actual = c1.plus(c2);
        assertEquals(
                expected,
                actual,
                c1 + " + " + c2);
    }
    
    private static void assertAdd(final Complex c1, final double d2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() + d2,
                c1.imaginary());
        final Complex actual = c1.plus(d2);
        assertEquals(
                expected,
                actual,
                c1 + " + " + d2);
    }
    
    private static void assertSubtract(final Complex c1, final Complex c2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() - c2.real(),
                c1.imaginary() - c2.imaginary());
        final Complex actual = c1.minus(c2);
        assertEquals(
                expected,
                actual,
                c1 + " - " + c2);
    }
    
    private static void assertSubtract(final Complex c1, final double d2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() - d2,
                c1.imaginary());
        final Complex actual = c1.minus(d2);
        assertEquals(
                expected,
                actual,
                c1 + " - " + d2);
    }
    
    private static void assertMultiply(final Complex c1, final Complex c2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() * c2.real() - c1.imaginary() * c2.imaginary(),
                c1.real() * c2.imaginary() + c1.imaginary() * c2.real());
        final Complex actual = c1.times(c2);
        assertEquals(
                expected,
                actual,
                c1 + " * " + c2);
    }
    
    private static void assertMultiply(final Complex c1, final double d2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() * d2,
                c1.imaginary() * d2);
        final Complex actual = c1.times(d2);
        assertEquals(
                expected,
                actual,
                c1 + " * " + d2);
    }
    
    private static void assertDivide(final Complex c1, final Complex c2)
    {
        final double divisor
            = Math.pow(c2.real(), 2) + Math.pow(c2.imaginary(), 2);
        final Complex expected = Complex.valueOf(
                (c1.real() * c2.real() + c1.imaginary() * c2.imaginary())
                / divisor,
                (c1.imaginary() * c2.real() - c1.real() * c2.imaginary())
                / divisor);
        final Complex actual = c1.dividedBy(c2);
        assertEquals(
                expected,
                actual,
                c1 + " / " + c2);
    }
    
    private static void assertDivide(final Complex c1, final double d2)
    {
        final Complex expected = Complex.valueOf(
                c1.real() / d2,
                c1.imaginary() / d2);
        final Complex actual = c1.dividedBy(d2);
        assertEquals(
                expected,
                actual,
                c1 + " / " + d2);
    }
    
    private static void assertEquals(
            final Complex expected,
            final Complex actual,
            final String message)
    {
        Assertions.assertEquals(
                expected.real(),
                actual.real(),
                DELTA,
                message + ": [real part]");
        Assertions.assertEquals(
                expected.imaginary(),
                actual.imaginary(),
                DELTA,
                message + ": [imaginary part]");
    }
    
    private static void assertSameHashCodeIfEqual(
            final Complex c1, final Complex c2)
    {
        if (c1.equals(c2))
        {
            Assertions.assertEquals(c1.hashCode(), c2.hashCode(), "hashCode");
        }
    }
    
    private static void assertHashCodeConsistency(
            final int uniqueHashCodesRequired, final Complex... complexes)
    {
        TestUtil.assertHashCodeConsistency(
                complexes,
                uniqueHashCodesRequired,
                c -> new int[]
                        {
                        Complex.valueOf(c.real(), c.imaginary()).hashCode(),
                        });
    }

}
