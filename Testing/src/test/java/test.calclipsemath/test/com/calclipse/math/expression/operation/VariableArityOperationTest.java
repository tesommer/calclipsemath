package test.com.calclipse.math.expression.operation;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;

@DisplayName("VariableArityOperation")
public final class VariableArityOperationTest
{
    private static final Value ARG_DEFAULT = valueOf(21);
    private static final Value ARG_OVERRIDE = valueOf(3);
    private static final Value RESULT_DEFAULT = valueOf(1);
    private static final Value RESULT_OVERRIDE = valueOf(2);
    
    private static final VariableArityOperation
    OPERATION = args -> RESULT_DEFAULT;
    
    private static final VariableArityOperation
    OVERRIDE = args ->
    {
        if (args[0] == ARG_OVERRIDE)
        {
            return RESULT_OVERRIDE;
        }
        return Values.YIELD;
    };

    public VariableArityOperationTest()
    {
    }
    
    @Test
    @DisplayName("override")
    public void testOverride() throws ErrorMessage
    {
        assertOverridden(OVERRIDE.override(OPERATION));
    }
    
    @Test
    @DisplayName("acceptOverride")
    public void testAcceptOverride() throws ErrorMessage
    {
        assertOverridden(OPERATION.acceptOverride(OVERRIDE));
    }
    
    private static void assertOverridden(
            final VariableArityOperation overridden) throws ErrorMessage
    {
        assertSame(
                RESULT_DEFAULT, overridden.evaluate(ARG_DEFAULT), "default");
        assertSame(
                RESULT_OVERRIDE, overridden.evaluate(ARG_OVERRIDE), "override");
    }
    
    private static Value valueOf(final Object value)
    {
        return Value.constantOf(value);
    }

}
