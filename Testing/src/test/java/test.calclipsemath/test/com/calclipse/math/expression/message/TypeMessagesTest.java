package test.com.calclipse.math.expression.message;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.Fraction;
import com.calclipse.math.expression.message.TypeMessages;

@DisplayName("DenseMatrix")
public final class TypeMessagesTest
{
    private static final String REAL_TYPE_NAME = "real";
    private static final String FRACTION_TYPE_NAME = "fraction";

    public TypeMessagesTest()
    {
    }
    
    @Test
    @DisplayName("TypeNamePrecedence")
    public void testTypeNamePrecedence()
    {
        assertTrue(
                TypeMessages.invalidType(
                        Double.valueOf(21)).contains(REAL_TYPE_NAME),
                "Number == real");
        assertTrue(
                TypeMessages.invalidType(
                        Fraction.valueOf(BigInteger.TWO, BigInteger.ONE))
                            .contains(FRACTION_TYPE_NAME),
                "Fraction == fraction");
    }

}
