package test.com.calclipse.math.parser.misc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.Real;
import com.calclipse.math.parser.misc.MathUtil;

import test.TestUtil;

@DisplayName("MathUtil")
public final class MathUtilTest
{
    private static final double DELTA = 1E-8;
    
    public MathUtilTest()
    {
    }
    
    @Test
    @DisplayName("factorial")
    public void testFactorial()
    {
        assertEquals(bigInt("1"), MathUtil.factorial(0), "0!");
        assertEquals(bigInt("1"), MathUtil.factorial(1), "1!");
        assertEquals(bigInt("2"), MathUtil.factorial(2), "2!");
        assertEquals(bigInt("6"), MathUtil.factorial(3), "3!");
        assertEquals(bigInt("24"), MathUtil.factorial(4), "4!");
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.factorial(-1),
                "-1!");
    }
    
    @Test
    @DisplayName("choose")
    public void testChoose()
    {
        assertEquals(bigInt("1"), MathUtil.choose(0, 0), "choose(0, 0)");
        assertEquals(bigInt("0"), MathUtil.choose(2, -1), "choose(2, -1)");
        assertEquals(bigInt("0"), MathUtil.choose(2, 3), "choose(2, 3)");
        assertEquals(bigInt("1"), MathUtil.choose(2, 0), "choose(2, 0)");
        assertEquals(bigInt("15"), MathUtil.choose(6, 2), "choose(6, 2)");
        assertEquals(bigInt("20"), MathUtil.choose(6, 3), "choose(6, 3)");
        assertEquals(bigInt("15"), MathUtil.choose(6, 4), "choose(6, 4)");
        assertEquals(bigInt("35"), MathUtil.choose(7, 3), "choose(7, 3)");
        assertEquals(bigInt("35"), MathUtil.choose(7, 4), "choose(7, 4)");
        assertEquals(bigInt("56"), MathUtil.choose(8, 3), "choose(8, 3)");
        assertEquals(bigInt("56"), MathUtil.choose(8, 5), "choose(8, 5)");
        assertEquals(
                bigInt("352716"), MathUtil.choose(21, 10), "choose(21, 10)");
        assertEquals(
                bigInt("352716"), MathUtil.choose(21, 11), "choose(21, 11)");
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.choose(-1, 2),
                "Negative n");
    }
    
    @Test
    @DisplayName("lcm")
    public void testLcm()
    {
        assertEquals(
                bigInt("0"),
                MathUtil.lcm(bigInt("0"), bigInt("0")),
                "lcm(0, 0)");
        assertEquals(
                bigInt("0"),
                MathUtil.lcm(bigInt("0"), bigInt("2")),
                "lcm(0, 2)");
        assertEquals(
                bigInt("0"),
                MathUtil.lcm(bigInt("2"), bigInt("0")),
                "lcm(2, 0)");
        assertEquals(
                bigInt("24"),
                MathUtil.lcm(bigInt("12"), bigInt("8")),
                "lcm(12, 8)");
        assertEquals(
                bigInt("-24"),
                MathUtil.lcm(bigInt("-12"), bigInt("8")),
                "lcm(-12, 8)");
        assertEquals(
                bigInt("-42"),
                MathUtil.lcm(bigInt("21"), bigInt("-6")),
                "lcm(21, -6)");
        assertEquals(
                bigInt("-1"),
                MathUtil.lcm(bigInt("1"), bigInt("-1")),
                "lcm(1, -1)");
    }
    
    @Test
    @DisplayName("round(double, int)")
    public void testRoundDoubleInt()
    {
        assertEquals(
                2.0,    MathUtil.round(2, 2),       DELTA, "round(2, 2)");
        assertEquals(
                2.0,    MathUtil.round(2.34, 0),    DELTA, "round(2.34, 0)");
        assertEquals(
                12.34,  MathUtil.round(12.344, 2),  DELTA, "round(12.344, 2)");
        assertEquals(
                12.35,  MathUtil.round(12.345, 2),  DELTA, "round(12.345, 2)");
        assertEquals(
                -12.34, MathUtil.round(-12.344, 2), DELTA, "round(-12.344, 2)");
        assertEquals(
                -12.35, MathUtil.round(-12.345, 2), DELTA, "round(-12.345, 2)");
        assertEquals(
                -0.0,   MathUtil.round(-0.4, 0),    DELTA, "round(-0.4, 0)");
        assertEquals(
                -1.0,   MathUtil.round(-0.5, 0),    DELTA, "round(-0.5, 0)");
        assertEquals(
                0.0,    MathUtil.round(0.4, 0),     DELTA, "round(0.4, 0)");
        assertEquals(
                1.0,    MathUtil.round(0.5, 0),     DELTA, "round(0.5, 0)");
        assertEquals(
                -1.2,   MathUtil.round(-1.23, 1),   DELTA, "round(-1.23, 1)");
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.round(13.3, -5),
                "Negative decimals");
    }
    
    @Test
    @DisplayName("permutations")
    public void testPermutations()
    {
        assertEquals(
                bigInt("1"),
                MathUtil.permutations(0, 0),
                "permutations(0, 0)");
        assertEquals(
                bigInt("0"),
                MathUtil.permutations(2, -1),
                "permutations(2, -1)");
        assertEquals(
                bigInt("0"),
                MathUtil.permutations(2, 3),
                "permutations(2, 3)");
        assertEquals(
                bigInt("1"),
                MathUtil.permutations(2, 0),
                "permutations(2, 0)");
        assertEquals(
                bigInt("3024"),
                MathUtil.permutations(9, 4),
                "permutations(9, 4)");
        assertEquals(
                bigInt("210"),
                MathUtil.permutations(7, 3),
                "permutations(7, 3)");
        assertEquals(
                bigInt("840"),
                MathUtil.permutations(7, 4),
                "permutations(7, 4)");
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.permutations(-1, 2),
                "Negative n");
    }
    
    @Test
    @DisplayName("acos(Complex)")
    public void testAcosComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(1.0302674748861005, -2.575531832575597),
                MathUtil.acos(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "acos (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("acosh(double)")
    public void testAcoshDouble()
    {
        assertEquals(
                1.894559012672978, MathUtil.acosh(3.4), DELTA, "acosh 3.4");
    }
    
    @Test
    @DisplayName("acosh(Complex)")
    public void testAcoshComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(2.575531832575597, 1.0302674748861005),
                MathUtil.acosh(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "acosh (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("arg(Complex)")
    public void testArgComplex()
    {
        final double actual = MathUtil.arg(Complex.valueOf(3.4, 5.6));
        final double expected = 1.025141272267905;
        assertEquals(expected, actual, DELTA, "arg (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("asin(Complex)")
    public void testAsinComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(0.5405288519087937, 2.575531832575597),
                MathUtil.asin(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "asin (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("asinh(double)")
    public void testAsinhDouble()
    {
        assertEquals(
                1.93787927766645006, MathUtil.asinh(3.4), DELTA, "asinh 3.4");
    }
    
    @Test
    @DisplayName("asinh(Complex)")
    public void testAsinhComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(2.5701589142747383, 1.0199317675774622),
                MathUtil.asinh(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "asinh (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("atan(Complex)")
    public void testAtanComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(1.4903898283284172, 0.13038152874271414),
                MathUtil.atan(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "atan (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("atanh(double)")
    public void testAtanhDouble()
    {
        assertEquals(
                0.354092528962243, MathUtil.atanh(.34), DELTA, "atanh .34");
    }
    
    @Test
    @DisplayName("atanh(Complex)")
    public void testAtanhComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(0.25876450927159533, 0.5497321120584342),
                MathUtil.atanh(Complex.valueOf(.34, .56)),
                DELTA,
                "atanh (.34, .56)");
    }
    
    @Test
    @DisplayName("cos(Complex)")
    public void testCosComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(1.0944823387009368, -0.19666791585360893),
                MathUtil.cos(Complex.valueOf(.34, .56)),
                DELTA,
                "cos (.34, .56)");
    }
    
    @Test
    @DisplayName("cosh(Complex)")
    public void testCoshComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(0.8967000353196396, 0.1841030986841954),
                MathUtil.cosh(Complex.valueOf(.34, .56)),
                DELTA,
                "cosh (.34, .56)");
    }
    
    @Test
    @DisplayName("exp(Complex)")
    public void testExpComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(1.1903490268109893, 0.7462887689095815),
                MathUtil.exp(Complex.valueOf(.34, .56)),
                DELTA,
                "exp (.34, .56)");
    }
    
    @Test
    @DisplayName("ln(Complex)")
    public void testLnComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(4.182254051875295, 1.025141272267905),
                MathUtil.ln(Complex.valueOf(34, 56)),
                DELTA,
                "ln (34, 56)");
    }
    
    @Test
    @DisplayName("pow(Complex, double)")
    public void testPowComplexDouble()
    {
        TestUtil.assertEquals(
                Complex.valueOf(-330039.5510029812, 2306598.7836952056),
                MathUtil.pow(Complex.valueOf(3.4, 5.6), 7.8),
                DELTA,
                "pow((3.4, 5.6), 7.8)");
    }
    
    @Test
    @DisplayName("pow(Complex, Complex)")
    public void testPowComplexComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(206.87842469483564, -6.550257793840156),
                MathUtil.pow(
                        Complex.valueOf(3.4, 5.6), Complex.valueOf(7.8, 9.1)),
                DELTA,
                "pow((3.4, 5.6), (7.8, 9.1))");
        TestUtil.assertEquals(
                Complex.valueOf(0, 0),
                MathUtil.pow(Complex.valueOf(0, 0), Complex.valueOf(7.8, 9.1)),
                DELTA,
                "pow((0, 0), (7.8, 9.1))");
    }
    
    @Test
    @DisplayName("sin(Complex)")
    public void testSinComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(-34.553003563502564, -130.72209341870186),
                MathUtil.sin(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "sin (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("sinh(Complex)")
    public void testSinhComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(11.606625203796467, -9.468202062856422),
                MathUtil.sinh(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "sinh (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("sqrt(Complex)")
    public void testSqrtComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(2.2306205125103284, 1.2552560977074916),
                MathUtil.sqrt(Complex.valueOf(3.4, 5.6)),
                DELTA,
                "sqrt (3.4, 5.6)");
    }
    
    @Test
    @DisplayName("tan(Complex)")
    public void testTanComplex()
    {
        final Complex c = Complex.valueOf(.34, .56);
        final Complex actual = MathUtil.tan(c);
        final Complex expected = MathUtil.sin(c).dividedBy(MathUtil.cos(c));
        TestUtil.assertEquals(expected, actual, DELTA, "tan (.34, .56)");
    }
    
    @Test
    @DisplayName("tanh(Complex)")
    public void testTanhComplex()
    {
        TestUtil.assertEquals(
                Complex.valueOf(0.4377452839485766, 0.5370752626817837),
                MathUtil.tanh(Complex.valueOf(.34, .56)),
                DELTA,
                "tanh (.34, .56)");
    }
    
    @Test
    @DisplayName("toBigIntegerExact(Real)")
    public void testToBigIntegerExactReal()
    {
        final MathContext mc = MathContext.DECIMAL32;
        final Fraction one_half = Fraction.valueOf(bigInt("1"), bigInt("2"));
        assertFalse(
                MathUtil.toBigIntegerExact(Real.fractured(one_half, mc, -1))
                    .isPresent(),
                    "assertFalse: fractured 1/2 isPresent");
        final var one_fourth = new BigDecimal(".25");
        assertFalse(
                MathUtil.toBigIntegerExact(Real.unfractured(one_fourth, mc, -1))
                    .isPresent(),
                    "assertFalse: unfractured .25 isPresent");
        final var one = new BigDecimal("1.0");
        assertEquals(
                BigInteger.ONE,
                MathUtil.toBigIntegerExact(Real.unfractured(one, mc, -1)).get(),
                "unfractured 1");
        final Fraction two = Fraction.valueOf(bigInt("4"), bigInt("2"));
        assertEquals(
                new BigInteger("2"),
                MathUtil.toBigIntegerExact(Real.fractured(two, mc, -2)).get(),
                "fractured 4/2");
    }
    
    @Test
    @DisplayName("integerPart of fractured Real")
    public void testIntegerPartOfFracturedReal()
    {
        final MathContext mc = MathContext.UNLIMITED;
        // 1/2
        final Real iPart1 = MathUtil.integerPart(
                Real.fractured(
                        Fraction.valueOf(BigInteger.ONE, bigInt("2")), mc, -1));
        TestUtil.assertReal(
                Fraction.valueOf(BigInteger.ZERO, BigInteger.ONE), mc, -1,
                iPart1);
        // 22/7
        final Real iPart2 = MathUtil.integerPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("22"), bigInt("7")), mc, 2));
        TestUtil.assertReal(
                Fraction.valueOf(bigInt("3"), BigInteger.ONE), mc, 2,
                iPart2);
        // 22/-7
        final Real iPart3 = MathUtil.integerPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("22"), bigInt("-7")), mc, 2));
        TestUtil.assertReal(
                Fraction.valueOf(bigInt("-3"), BigInteger.ONE), mc, 2,
                iPart3);
        // 6/2
        final Real iPart4 = MathUtil.integerPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("6"), bigInt("2")), mc, -5));
        TestUtil.assertReal(
                Fraction.valueOf(bigInt("3"), BigInteger.ONE), mc, -5,
                iPart4);
    }
    
    @Test
    @DisplayName("integerPart of unfractured Real")
    public void testIntegerPartOfUnfracturedReal()
    {
        final var mc = new MathContext(32, RoundingMode.UNNECESSARY);
        // -2.333333333333333
        final Real iPart1 = MathUtil.integerPart(
                Real.unfractured(new BigDecimal("-2.333333333333333"), mc, -2));
        TestUtil.assertReal(
                new BigDecimal("-2"), mc, -2,
                iPart1);
    }
    
    @Test
    @DisplayName("fractionPart of fractured Real")
    public void testFractionPartOfFracturedReal()
    {
        final var mc = new MathContext(32, RoundingMode.UNNECESSARY);
        // 1/2
        final Real fPart1 = MathUtil.fractionPart(
                Real.fractured(
                        Fraction.valueOf(BigInteger.ONE, bigInt("2")), mc, -1));
        TestUtil.assertReal(
                Fraction.valueOf(BigInteger.ONE, bigInt("2")), mc, -1,
                fPart1);
        // 23/7
        final Real fPart2 = MathUtil.fractionPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("23"), bigInt("7")), mc, 2));
        TestUtil.assertReal(
                Fraction.valueOf(bigInt("2"), bigInt("7")), mc, 2,
                fPart2);
        // 23/-7
        final Real fPart3 = MathUtil.fractionPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("23"), bigInt("-7")), mc, 2));
        TestUtil.assertReal(
                Fraction.valueOf(bigInt("-2"), bigInt("7")), mc, 2,
                fPart3);
        // 6/2
        final Real fPart4 = MathUtil.fractionPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("6"), bigInt("2")), mc, -5));
        TestUtil.assertReal(
                Fraction.valueOf(BigInteger.ZERO, BigInteger.ONE), mc, -5,
                fPart4);
        // 6/-2
        final Real fPart5 = MathUtil.fractionPart(
                Real.fractured(
                        Fraction.valueOf(bigInt("6"), bigInt("-2")), mc, -7));
        TestUtil.assertReal(
                Fraction.valueOf(BigInteger.ZERO, BigInteger.ONE), mc, -7,
                fPart5);
    }
    
    @Test
    @DisplayName("fractionPart of unfractured Real")
    public void testFractionPartOfUnfracturedReal()
    {
        final var mc = new MathContext(32, RoundingMode.UNNECESSARY);
        // -2.333333333333333
        final Real fPart1 = MathUtil.fractionPart(
                Real.unfractured(new BigDecimal("-2.333333333333333"), mc, -1));
        TestUtil.assertReal(
                new BigDecimal("-0.333333333333333"), mc, -1,
                fPart1);
    }
    
    @Test
    @DisplayName("round(Real, int)")
    public void testRoundRealInt()
    {
        // 191/250 = .764
        final Real real1 = Real.fractured(
                Fraction.valueOf(bigInt("191"), bigInt("250")),
                new MathContext(11, RoundingMode.HALF_UP),
                -2);
        TestUtil.assertReal(
                new BigDecimal("0.76"),
                real1.mathContext(),
                real1.fracturability(),
                MathUtil.round(real1, 2));
        // 153/200 = .765
        final Real real2 = Real.fractured(
                Fraction.valueOf(bigInt("153"), bigInt("200")),
                new MathContext(11, RoundingMode.HALF_UP),
                -2);
        TestUtil.assertReal(
                new BigDecimal("0.77"),
                real2.mathContext(),
                real2.fracturability(),
                MathUtil.round(real2, 2));
        final Real real3 = Real.fractured(
                Fraction.valueOf(bigInt("2"), bigInt("3")),
                new MathContext(7, RoundingMode.HALF_UP),
                -2);
        TestUtil.assertReal(
                new BigDecimal("0.6666667"),
                real3.mathContext(),
                real3.fracturability(),
                MathUtil.round(real3, 11));
        final Real real4 = Real.unfractured(
                new BigDecimal("-1.777888888888888888"),
                new MathContext(21, RoundingMode.FLOOR),
                -1);
        TestUtil.assertReal(
                new BigDecimal("-1.7778889"),
                real4.mathContext(),
                real4.fracturability(),
                MathUtil.round(real4, 7));
        final Real real5 = Real.unfractured(
                new BigDecimal("-1.99999999999"),
                new MathContext(3, RoundingMode.CEILING),
                -1);
        TestUtil.assertReal(
                new BigDecimal("-1"),
                real5.mathContext(),
                real5.fracturability(),
                MathUtil.round(real5, 0));
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.round(
                        Real.unfractured(
                                new BigDecimal("1.234"),
                                MathContext.DECIMAL32,
                                -1),
                        -2),
                "Negative decimals");
    }
    
    @Test
    @DisplayName("toFraction")
    public void testToFraction()
    {
        final double delta = 1E-5;
        final int iterations = 20;
        assertFraction(
                "0", "1",    MathUtil.toFraction(0,        iterations, delta)
                .get());
        assertFraction(
                "1", "1",    MathUtil.toFraction(1,        iterations, delta)
                .get());
        assertFraction(
                "22", "1",   MathUtil.toFraction(22,       iterations, delta)
                .get());
        assertFraction(
                "17", "5",   MathUtil.toFraction(3.4,      iterations, delta)
                .get());
        assertFraction(
                "1", "7",    MathUtil.toFraction(1 / 7.0,  iterations, delta)
                .get());
        assertFraction(
                "2", "5",    MathUtil.toFraction(2 / 5.0,  iterations, delta)
                .get());
        assertFraction(
                "1", "49",   MathUtil.toFraction(1 / 49.0, iterations, delta)
                .get());
        assertFraction(
                "-2", "1",   MathUtil.toFraction(-2,       iterations, delta)
                .get());
        assertFraction(
                "-1", "7",   MathUtil.toFraction(1 / -7.0, iterations, delta)
                .get());
        assertFraction(
                "-501", "5", MathUtil.toFraction(-100.2,   iterations, delta)
                .get());
    }
    
    @Test
    @DisplayName("toFraction: NaN and infinity")
    public void testToFraction_NaNAndInfinity()
    {
        final double delta = 1E-5;
        final int iterations = 20;
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.toFraction(
                        Double.NaN, iterations, delta),
                "NaN");
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.toFraction(
                        Double.POSITIVE_INFINITY, iterations, delta),
                "Positive infinity");
        assertThrows(
                ArithmeticException.class,
                () -> MathUtil.toFraction(
                        Double.NEGATIVE_INFINITY, iterations, delta),
                "Negative infinity");
    }
    
    @Test
    @DisplayName("toFraction: Irrational")
    public void testToFraction_Irrational()
    {
        final double delta = 1E-5;
        final int iterations = 20;
        assertFalse(
                MathUtil.toFraction(Math.PI, iterations, delta).isPresent(),
                "toFraction(pi).isPresent()");
        assertFalse(
                MathUtil.toFraction(-Math.PI, iterations, delta).isPresent(),
                "toFraction(-pi).isPresent()");
    }
    
    private static BigInteger bigInt(final String value)
    {
        return new BigInteger(value);
    }
    
    /**
     * Asserts that the actual fraction has
     * the expected numerator and denominator.
     */
    private static void assertFraction(
            final String expectedNumerator,
            final String expectedDenominator,
            final Fraction actual)
    {
        assertEquals(
                bigInt(expectedNumerator),
                actual.numerator(),
                "Numerator");
        assertEquals(
                bigInt(expectedDenominator),
                actual.denominator(),
                "Denominator");
    }

}
