package test.com.calclipse.math.parser.type;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.parser.type.Comparer;

/**
 * Compares numbers.
 * The numbers may be of different types, e.g. integer and double.
 * {@link #areEqual(Object, Object)} also supports arbitrary objects.
 * 
 * @author Tone Sommerland
 */
public enum MockComparer implements Comparer
{
    INSTANCE;

    @Override
    public int compare(final Object value1, final Object value2)
            throws ErrorMessage
    {
        if (value1 instanceof Number && value2 instanceof Number)
        {
            final var n1 = (Number)value1;
            final var n2 = (Number)value2;
            return Double.valueOf(n1.doubleValue())
                .compareTo(Double.valueOf(n2.doubleValue()));
        }
        throw errorMessage("Not number");
    }

    @Override
    public boolean areEqual(final Object value1, final Object value2)
    {
        try
        {
            return compare(value1, value2) == 0;
        }
        catch (final ErrorMessage ex)
        {
            return value1.equals(value2);
        }
    }

}
