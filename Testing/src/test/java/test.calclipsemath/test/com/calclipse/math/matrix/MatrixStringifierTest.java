package test.com.calclipse.math.matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.MatrixStringifier;

@DisplayName("MatrixStringifier")
public final class MatrixStringifierTest
{
    public MatrixStringifierTest()
    {
    }

    @Nested
    @DisplayName("MatrixStringifier.getDefault()")
    public static final class GetDefaultTest
    {
        private static final MatrixStringifier
        INSTANCE = MatrixStringifier.standard();

        public GetDefaultTest()
        {
        }
        
        @Test
        @DisplayName("entryStringifier")
        public void testEntryStringifier()
        {
            assertEquals(
                    "0.0",
                    INSTANCE.entryStringifier().apply(0),
                    "0");
            assertEquals(
                    "21.0",
                    INSTANCE.entryStringifier().apply(21),
                    "21");
            assertEquals(
                    "-22.0",
                    INSTANCE.entryStringifier().apply(-22),
                    "-22");
            assertEquals(
                    "0.0",
                    INSTANCE.entryStringifier().apply(-0),
                    "-0");
            assertEquals(
                    "0.5",
                    INSTANCE.entryStringifier().apply(.5),
                    ".5");
            assertEquals(
                    String.valueOf(Math.PI),
                    INSTANCE.entryStringifier().apply(Math.PI),
                    "pi");
            assertEquals(
                    "1000.2",
                    INSTANCE.entryStringifier().apply(1000.2),
                    "1000.2");
        }

        @Test
        @DisplayName("Stringify empty matrix")
        public void testStringifyEmptyMatrix()
        {
            assertEquals(
                    "[[]]",
                    stringify(MockMatrix.valueOf(0, 0)),
                    "0x0");
            assertEquals(
                    "[[]]",
                    stringify(MockMatrix.valueOf(2, 0)),
                    "2x0");
            assertEquals(
                    "[[]]",
                    stringify(MockMatrix.valueOf(0, 2)),
                    "0x2");
        }
        
        @Test
        @DisplayName("Stringify single-element matrix")
        public void testStringifySingleElementMatrix()
        {
            final Matrix mx = MockMatrix.valueOf(1, 1, 0);
            assertEquals(
                    "[[0.0]]",
                    stringify(mx),
                    "1x1");
        }
        
        @Test
        @DisplayName("Stringify single-row matrix")
        public void testStringifySingleRowMatrix()
        {
            final Matrix mx = MockMatrix.valueOf(1, 3, 21, -22, 23);
            assertEquals(
                    "[[21.0 -22.0 23.0]]",
                    stringify(mx),
                    "1x3");
        }
        
        @Test
        @DisplayName("Stringify single-column matrix")
        public void testStringifySingleColumnMatrix()
        {
            final Matrix mx = MockMatrix.valueOf(3, 1, 21, -22, 23);
            final String expected
                    = "[[21.0 ]\n"
                    + " [-22.0]\n"
                    + " [23.0 ]]";
            assertEquals(
                    expected,
                    stringify(mx),
                    "3x1");
        }
        
        @Test
        @DisplayName("Stringify 2-by-2 matrix containing rational element")
        public void testStringify2By2MatrixContainingRationalElement()
        {
            final Matrix mx = MockMatrix.valueOf(2, 2, 123, 4, 1000.2, 43);
            final String expected
                    = "[[123.0  4.0 ]\n"
                    + " [1000.2 43.0]]";
            assertEquals(
                    expected,
                    stringify(mx),
                    "2x2");
        }
        
        private static String stringify(final Matrix matrix)
        {
            return INSTANCE.stringify(matrix);
        }
        
    }

}
