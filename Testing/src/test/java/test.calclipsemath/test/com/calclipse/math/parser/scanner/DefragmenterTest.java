package test.com.calclipse.math.parser.scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Scanner;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.scanner.Defragmenter;

@DisplayName("Defragmenter")
public final class DefragmenterTest
{
    private DefragmenterTest()
    {
    }
    
    @Test
    @DisplayName("No fragments from source")
    public void testNoFragsFromSource() throws ErrorMessage
    {
        final var defrag = new Defragmenter(new MockScanner());
        defrag.reset("");
        assertScannerOutput(defrag);
    }
    
    @Test
    @DisplayName("No undefs")
    public void testNoUndefs() throws ErrorMessage
    {
        // aaabbbccc ddd
        // 0123456789012
        final var frag1 = Fragment.of(Operand.of("aaa", Values.UNDEF),  0);
        final var frag2 = Fragment.of(Operand.of("bbb", Values.UNDEF),  3);
        final var frag3 = Fragment.of(Operand.of("ccc", Values.UNDEF),  6);
        final var frag4 = Fragment.of(Operand.of("ddd", Values.UNDEF), 10);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4));
        defrag.reset("");
        assertScannerOutput(defrag, frag1, frag2, frag3, frag4);
    }
    
    @Test
    @DisplayName("Undefs containing whitespace")
    public void testUndefsContainingWS() throws ErrorMessage
    {
        // ab cdefg hi
        // 01234567890
        final var frag1 = Fragment.of(Tokens.undefined("a"),     0);
        final var frag2 = Fragment.of(Tokens.undefined("b c"),   1);
        final var frag3 = Fragment.of(Tokens.undefined("de"),    4);
        final var frag4 = Fragment.of(Tokens.undefined("fg hi"), 6);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4));
        defrag.reset("");
        final var out1 = Fragment.of(Tokens.undefined("ab"),    0);
        final var out2 = Fragment.of(Tokens.undefined("cdefg"), 3);
        final var out3 = Fragment.of(Tokens.undefined("hi"),    9);
        assertScannerOutput(defrag, out1, out2, out3);
    }
    
    @Test
    @DisplayName("Undefs with leading whitespace")
    public void testUndefsWithLeadingWS() throws ErrorMessage
    {
        //  A B C DE  FG HI
        // 0123456789012345
        final var frag1 = Fragment.of(Tokens.undefined(" A"),       0);
        final var frag2 = Fragment.of(Tokens.undefined(" B C"),     2);
        final var frag3 = Fragment.of(Tokens.undefined(" DE"),      6);
        final var frag4 = Fragment.of(Tokens.undefined(" \tFG HI"), 9);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4));
        defrag.reset("");
        final var out1 = Fragment.of(Tokens.undefined("A"),   1);
        final var out2 = Fragment.of(Tokens.undefined("B"),   3);
        final var out3 = Fragment.of(Tokens.undefined("C"),   5);
        final var out4 = Fragment.of(Tokens.undefined("DE"),  7);
        final var out5 = Fragment.of(Tokens.undefined("FG"), 11);
        final var out6 = Fragment.of(Tokens.undefined("HI"), 14);
        assertScannerOutput(defrag, out1, out2, out3, out4, out5, out6);
    }
    
    @Test
    @DisplayName("Undefs with trailing whitespace")
    public void testUndefsWithTrailingWS() throws ErrorMessage
    {
        // z y x wv ut sr  
        // 0123456789012345
        final var frag1 = Fragment.of(Tokens.undefined("z "),       0);
        final var frag2 = Fragment.of(Tokens.undefined("y x "),     2);
        final var frag3 = Fragment.of(Tokens.undefined("wv "),      6);
        final var frag4 = Fragment.of(Tokens.undefined("ut sr\t "), 9);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4));
        defrag.reset("");
        final var out1 = Fragment.of(Tokens.undefined("z"),   0);
        final var out2 = Fragment.of(Tokens.undefined("y"),   2);
        final var out3 = Fragment.of(Tokens.undefined("x"),   4);
        final var out4 = Fragment.of(Tokens.undefined("wv"),  6);
        final var out5 = Fragment.of(Tokens.undefined("ut"),  9);
        final var out6 = Fragment.of(Tokens.undefined("sr"), 12);
        assertScannerOutput(defrag, out1, out2, out3, out4, out5, out6);
    }
    
    @Test
    @DisplayName("Undefs with leading and trailing whitespace")
    public void testUndefsWithLeadingAndTrailingWS() throws ErrorMessage
    {
        //  1  2 3  45  67 89 
        // 0123456789012345679
        final var frag1 = Fragment.of(Tokens.undefined(" 1 "),      0);
        final var frag2 = Fragment.of(Tokens.undefined(" 2 3 "),    3);
        final var frag3 = Fragment.of(Tokens.undefined(" 45 "),     8);
        final var frag4 = Fragment.of(Tokens.undefined(" 67 89 "), 12);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4));
        defrag.reset("");
        final var out1 = Fragment.of(Tokens.undefined("1"),   1);
        final var out2 = Fragment.of(Tokens.undefined("2"),   4);
        final var out3 = Fragment.of(Tokens.undefined("3"),   6);
        final var out4 = Fragment.of(Tokens.undefined("45"),  9);
        final var out5 = Fragment.of(Tokens.undefined("67"), 13);
        final var out6 = Fragment.of(Tokens.undefined("89"), 16);
        assertScannerOutput(defrag, out1, out2, out3, out4, out5, out6);
    }
    
    @Test
    @DisplayName("Undefs consisting of whitespace")
    public void testUndefsConsistingOfWS() throws ErrorMessage
    {
        //  aB    cD   eF      
        // 01234567890123456789
        final var frag1 = Fragment.of(Tokens.undefined(" aB "),   0);
        final var frag2 = Fragment.of(Tokens.undefined(" \t"),    4);
        final var frag3 = Fragment.of(Tokens.undefined(" cD "),   6);
        final var frag4 = Fragment.of(Tokens.undefined("\t "),   10);
        final var frag5 = Fragment.of(Tokens.undefined("eF"),    12);
        final var frag6 = Fragment.of(Tokens.undefined(" \t "),  14);
        final var frag7 = Fragment.of(Tokens.undefined("\t \t"), 17);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7));
        defrag.reset("");
        final var out1 = Fragment.of(Tokens.undefined("aB"),  1);
        final var out2 = frag2;
        final var out3 = Fragment.of(Tokens.undefined("cD"),  7);
        final var out4 = frag4;
        final var out5 = Fragment.of(Tokens.undefined("eF"), 12);
        final var out6 = frag6;
        final var out7 = frag7;
        assertScannerOutput(defrag, out1, out2, out3, out4, out5, out6, out7);
    }
    
    @Test
    @DisplayName("Combining adjacent undefs")
    public void testCombiningAdjacentUndefs() throws ErrorMessage
    {
        // c-dba+e*z y x
        // 0123456789012
        final var frag1 = Fragment.of(Operand.of("c-d", Values.UNDEF), 0);
        final var frag2 = Fragment.of(Operand.of("b", Values.UNDEF),   3);
        final var frag3 = Fragment.of(Tokens.undefined("a"),          4);
        final var frag4 = Fragment.of(Tokens.undefined("+"),          5);
        final var frag5 = Fragment.of(Operand.of("e", Values.UNDEF),   6);
        final var frag6 = Fragment.of(Operand.of("*", Values.UNDEF),   7);
        final var frag7 = Fragment.of(Tokens.undefined("z"),          8);
        final var frag8 = Fragment.of(Tokens.undefined("y"),         10);
        final var frag9 = Fragment.of(Tokens.undefined("x"),         12);
        final var defrag = new Defragmenter(new MockScanner(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag8, frag9));
        defrag.reset("");
        final var out1 = frag1;
        final var out2 = Fragment.of(Tokens.undefined("ba+e"), 3);
        final var out3 = frag6;
        final var out4 = frag7;
        final var out5 = frag8;
        final var out6 = frag9;
        assertScannerOutput(defrag, out1, out2, out3, out4, out5, out6);
    }
    
    @Test
    @DisplayName("Single undef consisting of whitespace")
    public void testSingleUndefConsistingOfWS() throws ErrorMessage
    {
        final var frag1 = Fragment.of(Tokens.undefined(" \t \t "), 2);
        final var defrag = new Defragmenter(new MockScanner(frag1));
        defrag.reset("");
        assertScannerOutput(defrag, frag1);
    }
    
    private static void assertScannerOutput(
            final Scanner scanner,
            final Fragment... expectedOutput) throws ErrorMessage
    {
        final var actualOutput = new ArrayList<Fragment>();
        while (scanner.hasNext())
        {
            actualOutput.add(scanner.next());
        }
        
        assertEquals(
                expectedOutput.length,
                actualOutput.size(),
                "Number of fragments returned from scanner");
        
        for (int i = 0; i < expectedOutput.length; i++)
        {
            final Fragment expectedFrag = expectedOutput[i];
            final Fragment actualFrag = actualOutput.get(i);
            assertEquals(
                    expectedFrag.token().name(),
                    actualFrag.token().name(),
                    "Name of token at index: " + i);
            assertEquals(
                    expectedFrag.position(),
                    actualFrag.position(),
                    "Position of fragment at index: " + i);
            assertSame(
                    expectedFrag.token().type(),
                    actualFrag.token().type(),
                    "Type of token at index: " + i);
        }
    }

}
