package test.com.calclipse.math.parser.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenType;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.step.Indices;

import test.TestUtil;

@DisplayName("Indices")
public final class IndicesTest
{
    private static final Token LPAREN = Tokens.leftParenthesis("(");
    private static final Token RPAREN = Tokens.rightParenthesis(")");
    private static final Token COMMA = Tokens.plain(",");
    private static final Token A = Tokens.plain("a");
    private static final Token B = Tokens.plain("b");
    private static final Token C = Tokens.plain("c");
    private static final Token D = Tokens.plain("d");
    
    private static final Object ACCUMULATOR = new Object();
    private static final Object DELEGATOR = new Object();
    
    private static final ParseStep
    STEP = Indices
        .openIndexListWith(LPAREN::identifiesAs)
        .closeIndexListWith(RPAREN::identifiesAs)
        .separateIndicesWith(COMMA::identifiesAs)
        .delegateIndexingTo((value, indices) -> Values.UNDEF)
        .build();

    public IndicesTest()
    {
    }
    
    @Test
    @DisplayName("Expression without indices")
    public void testExpressionWithoutIndices() throws ErrorMessage
    {
        final var frag1 = Fragment.of(A,  0);
        final var frag2 = Fragment.of(B, 10);
        final var frag3 = Fragment.of(C, 20);
        final var inOut = new InOut(frag1, frag2, frag3);
        assertOut(inOut, frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Simple expression with indices")
    public void testSimpleExpressionWithIndices() throws ErrorMessage
    {
        // Input: a ( b , c ) d
        // Expected a del ( b acc c ) d
        final var frag1 = Fragment.of(A,       0);
        final var frag2 = Fragment.of(LPAREN, 10);
        final var frag3 = Fragment.of(B,      20);
        final var frag4 = Fragment.of(COMMA,  30);
        final var frag5 = Fragment.of(C,      40);
        final var frag6 = Fragment.of(RPAREN, 50);
        final var frag7 = Fragment.of(D,      60);
        final var inOut = new InOut(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);
        assertOut(
                inOut,
                frag1,
                DELEGATOR,
                frag2,
                frag3,
                ACCUMULATOR,
                frag5,
                frag6,
                frag7);
    }
    
    @Test
    @DisplayName("Nested indices")
    public void testNestedIndices() throws ErrorMessage
    {
        // Input: a ( b ( c , d ) , b ( c , d ) )
        // Expected: a del ( b del ( c acc d ) acc b del ( c acc d ) )
        final var frag1 = Fragment.of(A,         0);
        final var frag2 = Fragment.of(LPAREN,   10);
        final var frag3 = Fragment.of(B,        20);
        final var frag4 = Fragment.of(LPAREN,   30);
        final var frag5 = Fragment.of(C,        40);
        final var frag6 = Fragment.of(COMMA,    50);
        final var frag7 = Fragment.of(D,        60);
        final var frag8 = Fragment.of(RPAREN,   70);
        final var frag9 = Fragment.of(COMMA,    80);
        final var frag10 = Fragment.of(B,       90);
        final var frag11 = Fragment.of(LPAREN, 100);
        final var frag12 = Fragment.of(C,      110);
        final var frag13 = Fragment.of(COMMA,  120);
        final var frag14 = Fragment.of(D,      130);
        final var frag15 = Fragment.of(RPAREN, 140);
        final var frag16 = Fragment.of(RPAREN, 150);
        // Index:    0 1   2 3 4   5 6 7   8 9 10  11 12  13 14 15  16 17 18
        // Expected: a del ( b del ( c acc d ) acc b  del (  c  acc d  )  )
        // Fragment: 1     2 3     4 5     7 8     10     11 12     14 15 16
        final var inOut = new InOut(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag8, frag9,
                frag10, frag11, frag12, frag13, frag14, frag15, frag16);
        assertOut(
                inOut,
                frag1,
                DELEGATOR,
                frag2,
                frag3,
                DELEGATOR,
                frag4,
                frag5,
                ACCUMULATOR,
                frag7,
                frag8,
                ACCUMULATOR,
                frag10,
                DELEGATOR,
                frag11,
                frag12,
                ACCUMULATOR,
                frag14,
                frag15,
                frag16);
    }
    
    @Test
    @DisplayName("Combination of indices and ordinary parentheses")
    public void testCombinationOfIndicesAndOrdinaryParentheses()
            throws ErrorMessage
    {
        // Input:
        // d ( b ) a ( ( c ) , ( d ) , b ( a ( c , d ) ) )
        
        // Expected:
        // d ( b ) a del ( ( c ) acc ( d ) acc b ( a del ( c acc d ) ) )
        
        final var frag1 = Fragment.of(D,         0);
        final var frag2 = Fragment.of(LPAREN,   10);
        final var frag3 = Fragment.of(B,        20);
        final var frag4 = Fragment.of(RPAREN,   30);
        final var frag5 = Fragment.of(A,        40);
        final var frag6 = Fragment.of(LPAREN,   50);
        final var frag7 = Fragment.of(LPAREN,   60);
        final var frag8 = Fragment.of(C,        70);
        final var frag9 = Fragment.of(RPAREN,   80);
        final var frag10 = Fragment.of(COMMA,   90);
        final var frag11 = Fragment.of(LPAREN, 100);
        final var frag12 = Fragment.of(D,      110);
        final var frag13 = Fragment.of(RPAREN, 120);
        final var frag14 = Fragment.of(COMMA,  130);
        final var frag15 = Fragment.of(B,      140);
        final var frag16 = Fragment.of(LPAREN, 150);
        final var frag17 = Fragment.of(A,      160);
        final var frag18 = Fragment.of(LPAREN, 170);
        final var frag19 = Fragment.of(C,      180);
        final var frag20 = Fragment.of(COMMA,  190);
        final var frag21 = Fragment.of(D,      200);
        final var frag22 = Fragment.of(RPAREN, 210);
        final var frag23 = Fragment.of(RPAREN, 220);
        final var frag24 = Fragment.of(RPAREN, 230);
        final var inOut = new InOut(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag8, frag9,
                frag10, frag11, frag12, frag13, frag14, frag15, frag16, frag17,
                frag18, frag19, frag20, frag21, frag22, frag23, frag24);
        assertOut(
                inOut,
                frag1,
                frag2,
                frag3,
                frag4,
                frag5,
                DELEGATOR,
                frag6,
                frag7,
                frag8,
                frag9,
                ACCUMULATOR,
                frag11,
                frag12,
                frag13,
                ACCUMULATOR,
                frag15,
                frag16,
                frag17,
                DELEGATOR,
                frag18,
                frag19,
                ACCUMULATOR,
                frag21,
                frag22,
                frag23,
                frag24);
    }
    
    @Test
    @DisplayName("Missing index-list opener")
    public void testMissingIndexListOpener() throws ErrorMessage
    {
        // Input: a , b )
        // Expected: a acc b )
        final var frag1 = Fragment.of(A,       0);
        final var frag2 = Fragment.of(COMMA,  10);
        final var frag3 = Fragment.of(B,      20);
        final var frag4 = Fragment.of(RPAREN, 30);
        final var inOut = new InOut(frag1, frag2, frag3, frag4);
        assertOut(inOut, frag1, ACCUMULATOR, frag3, frag4);
    }
    
    @Test
    @DisplayName("Empty index")
    public void testEmptyIndex() throws ErrorMessage
    {
        // Input: a ( , b ) a ( c , ) a ( , )
        // Expected: a del ( acc b ) a del ( c acc ) a del ( acc )
        final var frag1 = Fragment.of(A,         0);
        final var frag2 = Fragment.of(LPAREN,   10);
        final var frag3 = Fragment.of(COMMA,    20);
        final var frag4 = Fragment.of(B,        30);
        final var frag5 = Fragment.of(RPAREN,   40);
        final var frag6 = Fragment.of(A,        50);
        final var frag7 = Fragment.of(LPAREN,   60);
        final var frag8 = Fragment.of(C,        70);
        final var frag9 = Fragment.of(COMMA,    80);
        final var frag10 = Fragment.of(RPAREN,  90);
        final var frag11 = Fragment.of(A,      100);
        final var frag12 = Fragment.of(LPAREN, 110);
        final var frag13 = Fragment.of(COMMA,  120);
        final var frag14 = Fragment.of(RPAREN, 130);
        final var inOut = new InOut(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag8,
                frag9, frag10, frag11, frag12, frag13, frag14);
        assertOut(
                inOut,
                frag1,
                DELEGATOR,
                frag2,
                ACCUMULATOR,
                frag4,
                frag5,
                frag6,
                DELEGATOR,
                frag7,
                frag8,
                ACCUMULATOR,
                frag10,
                frag11,
                DELEGATOR,
                frag12,
                ACCUMULATOR,
                frag14);
    }
    
    @Test
    @DisplayName("Unclosed index list")
    public void testUnclosedIndexList() throws ErrorMessage
    {
        // Input: ( a , ( b , c )
        // Expected: ( a , del ( b acc c )
        final var frag1 = Fragment.of(LPAREN,  0);
        final var frag2 = Fragment.of(A,      10);
        final var frag3 = Fragment.of(COMMA,  20);
        final var frag4 = Fragment.of(LPAREN, 30);
        final var frag5 = Fragment.of(B,      40);
        final var frag6 = Fragment.of(COMMA,  50);
        final var frag7 = Fragment.of(C,      60);
        final var frag8 = Fragment.of(RPAREN, 70);
        final var inOut = new InOut(
                frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag8);
        assertOut(
                inOut,
                frag1,
                frag2,
                frag3,
                DELEGATOR,
                frag4,
                frag5,
                ACCUMULATOR,
                frag7,
                frag8);
    }
    
    /**
     * Asserts input produced the expected output.
     * An object in the expected array must be either a fragment,
     * the accumulator or the delegator.
     */
    private static void assertOut(
            final InOut actual, final Object... expected)
    {
        assertEquals(
                expected.length,
                actual.out.size(),
                "Output size");
        for (
                int inIndex = 0, outIndex = 0;
                outIndex < expected.length;
                outIndex++)
        {
            if (expected[outIndex] == ACCUMULATOR)
            {
                assertAccumulator(actual, inIndex, outIndex);
            }
            else if (expected[outIndex] == DELEGATOR)
            {
                assertDelegator(actual, inIndex--, outIndex);
            }
            else
            {
                assertOutIsIn(expected, actual, inIndex, outIndex);
            }
            inIndex++;
        }
    }

    private static void assertOutIsIn(
            final Object[] expected,
            final InOut actual,
            final int inIndex,
            final int outIndex)
    {
        assertSame(
                expected[outIndex],
                actual.out.get(outIndex),
                "output[" + outIndex + "]");
        assertSame(
                expected[outIndex],
                actual.in[inIndex],
                "input[" + inIndex + "]");
    }

    private static void assertAccumulator(
            final InOut actual, final int inIndex, final int outIndex)
    {
        final String message = "Accumulator expected: output[" + outIndex + "]";
        assertSame(
                TokenType.BOPERATOR,
                actual.out.get(outIndex).token().type(),
                message + ": type");
        assertEquals(
                COMMA.name(),
                actual.out.get(outIndex).token().name(),
                message + ": name");
        assertEquals(
                actual.in[inIndex].position(),
                actual.out.get(outIndex).position(),
                message + ": position");
        assertNotSame(
                actual.in[inIndex],
                actual.out.get(outIndex),
                message + ": input[" + inIndex + "]");
    }

    private static void assertDelegator(
            final InOut actual, final int inIndex, final int outIndex)
    {
        final String message = "Delegator expected: output[" + outIndex + "]";
        assertSame(
                TokenType.BOPERATOR,
                actual.out.get(outIndex).token().type(),
                message + ": type");
        assertEquals(
                LPAREN.name(),
                actual.out.get(outIndex).token().name(),
                message + ": name");
        assertEquals(
                actual.in[inIndex].position(),
                actual.out.get(outIndex).position(),
                message + ": position");
    }
    
    /**
     * Input and output.
     */
    private static final class InOut
    {
        private final Fragment[] in;
        private final Expression out;
        
        private InOut(final Fragment... in) throws ErrorMessage
        {
            this.in = in.clone();
            this.out = TestUtil.parse(STEP, in);
        }
    }

}
