package test.com.calclipse.math.matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.DenseMatrix;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.size.SizeViolation;

@DisplayName("DenseMatrix")
public final class DenseMatrixTest extends MatrixTest
{
    public DenseMatrixTest()
    {
    }

    @Override
    protected DenseMatrix createMatrix(
            final int rowCount,
            final int columnCount,
            final double... entries)
    {
        return DenseMatrix.valueOf(rowCount, columnCount, entries);
    }
    
    @Test
    @DisplayName("Constructor")
    public void testConstructor()
    {
        final Matrix mx_0x0 = DenseMatrix.valueOf(0, 0);
        final Matrix mx_1x0 = DenseMatrix.valueOf(1, 0);
        final Matrix mx_2x3 = DenseMatrix.valueOf(2, 3, 1, 2, 3, 4, 5, 6);
        final Matrix expected_0x0 = createMatrix(0, 0);
        final Matrix expected_1x0 = createMatrix(1, 0);
        final Matrix expected_2x3 = createMatrix(2, 3, 1, 2, 3, 4, 5, 6);
        assertEquals(mx_0x0, expected_0x0, "0x0");
        assertEquals(mx_1x0, expected_1x0, "1x0");
        assertEquals(mx_2x3, expected_2x3, "2x3");
    }
    
    @Test
    @DisplayName("Constructor with less supplied entries than size")
    public void testConstructorWithLessSuppliedEntriesThanSize()
    {
        final Matrix mx = DenseMatrix.valueOf(2, 2, 1);
        final Matrix expected = DenseMatrix.valueOf(2, 2, 1, 0, 0, 0);
        assertEquals(mx, expected, "2x2: 1");
    }
    
    @Test
    @DisplayName("Constructor with more supplied entries than size")
    public void testConstructorWithMoreSuppliedEntriesThanSize()
    {
        final Matrix mx = DenseMatrix.valueOf(2, 2, 1, 2, 3, 4, 5, 6, 7);
        final Matrix expected = DenseMatrix.valueOf(2, 2, 1, 2, 3, 4);
        assertEquals(mx, expected, "2x2: 1, 2, 3, 4, 5, 6, 7");
    }

    @Test
    @DisplayName("transform")
    public void testTransform()
    {
        final DenseMatrix mx = createMatrix(2, 3, 1, 2, 3, 4, 5, 6);
        final DenseMatrix mx2 = createMatrix(3, 2, 7, 8, 9, 10, 11, 12);
        mx.transform(mx2);
        final Matrix expected = createMatrix(2, 2, 58, 64, 139, 154);
        assertEquals(expected, mx2, "transform");

    }
    
    @Test
    @DisplayName("transform: Size violation")
    public void testTransformSizeViolation()
    {
        final DenseMatrix mx = createMatrix(3, 2);
        final DenseMatrix mx2 = createMatrix(3, 2);
        assertThrows(
                SizeViolation.class,
                () -> mx.transform(mx2),
                "3x2.transform(3x2)");
    }
    
    @Test
    @DisplayName("equals: Instance of same class")
    public void testEqualsInstanceOfSameClass()
    {
        final Matrix dense1 = DenseMatrix.valueOf(1, 1, 2);
        final Matrix dense2 = DenseMatrix.valueOf(1, 1, 2);
        final Matrix dense3 = DenseMatrix.valueOf(1, 1, 3);
        assertEquals(dense1, dense2, "Equal entries");
        assertNotEquals(dense1, dense3, "Differing entries");
    }
    
    @Test
    @DisplayName("equals: Instance of different class")
    public void testEqualsInstanceOfDifferentClass()
    {
        final Matrix dense1 = DenseMatrix.valueOf(1, 1, 2);
        final Matrix mock1 = MockMatrix.valueOf(1, 1, 2);
        final Matrix mock2 = MockMatrix.valueOf(1, 1, 3);
        assertEquals(dense1, mock1, "Equal entries");
        assertNotEquals(dense1, mock2, "Differing entries");
    }

}
