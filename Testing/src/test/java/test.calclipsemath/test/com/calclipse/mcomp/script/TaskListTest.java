package test.com.calclipse.mcomp.script;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.recursion.RecursionGuard;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Script;
import com.calclipse.mcomp.script.TaskList;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("TaskList")
public final class TaskListTest
{
    private final MockTask alwaysTask;
    private final MockTask oneStepTask;
    private final MockCompilableTask compilableTask;
    private TaskList taskList;
    private McContext context;
    
    public TaskListTest()
    {
        final Trace trace = new Trace.Builder().setScriptName("name").build();
        alwaysTask = new MockTask(trace, false);
        oneStepTask = new MockTask(trace, true);
        compilableTask = new MockCompilableTask(trace, false);
    }

    @BeforeEach
    public void setUp()
    {
        taskList = new TaskList();
        context = MockContext.getOne();
    }
    
    @AfterEach
    public void tearDown()
    {
        alwaysTask.resetExecutionCount();
        oneStepTask.resetExecutionCount();
        compilableTask.resetExecutionCount();
    }

    @Test
    @DisplayName("Execute with compilable task")
    public void testExecuteWithCompilableTask() throws ErrorMessage
    {
        taskList.add(alwaysTask);
        taskList.add(oneStepTask);
        taskList.add(compilableTask);
        execute(taskList, context);
        assertEquals(
                1,
                alwaysTask.executionCount(),
                "1st execution: exec count: always task");
        assertEquals(
                1,
                oneStepTask.executionCount(),
                "1st execution: exec count: one-step task");
        assertEquals(
                1,
                compilableTask.executionCount(),
                "1st execution: exec count: compilable task");
        assertTrue(
                compilableTask.isCompiled(),
                "compilableTask.isCompiled()");
        execute(taskList, context);
        assertEquals(
                2,
                alwaysTask.executionCount(),
                "2nd execution: exec count: always task");
        assertEquals(
                1,
                oneStepTask.executionCount(),
                "2nd execution: exec count: one-step task");
        assertEquals(
                2,
                compilableTask.executionCount(),
                "2nd execution: exec count: compilable task");
    }
    
    @Test
    @DisplayName("Execute without compilable task")
    public void testExecuteWithoutCompilableTask() throws ErrorMessage
    {
        taskList.add(alwaysTask);
        taskList.add(oneStepTask);
        execute(taskList, context);
        assertEquals(
                1,
                alwaysTask.executionCount(),
                "1st execution: exec count: always task: w/o comp");
        assertEquals(
                1,
                oneStepTask.executionCount(),
                "1st execution: exec count: one-step task: w/o comp");
        execute(taskList, context);
        assertEquals(
                2,
                alwaysTask.executionCount(),
                "2nd execution: exec count: always task: w/o comp");
        assertEquals(
                2,
                oneStepTask.executionCount(),
                "2nd execution: exec count: one-step task: w/o comp");
    }
    
    private static void execute(final TaskList tasks, final McContext context)
            throws ErrorMessage
    {
        Script.nameless(tasks, RecursionGuard.countingTo(2)).execute(context);
    }

}
