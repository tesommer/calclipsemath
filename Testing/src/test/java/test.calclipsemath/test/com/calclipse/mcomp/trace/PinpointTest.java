package test.com.calclipse.mcomp.trace;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.mcomp.trace.Pinpoint;

@DisplayName("Pinpoint")
public final class PinpointTest
{
    public PinpointTest()
    {
    }
    
    @Test
    @DisplayName("Illegal construction arguments")
    public void testIllegalConstructionArguments()
    {
        final String expression = "a+b*c/d^e";
        assertThrows(
                IllegalArgumentException.class,
                () -> Pinpoint.at(expression, "b", 1),
                "Invalid position");
        assertThrows(
                IllegalArgumentException.class,
                () -> Pinpoint.at(expression, "x", 1),
                "Nonexistent token");
        assertThrows(
                IndexOutOfBoundsException.class,
                () -> Pinpoint.at(expression, "b", 121),
                "Position out of bounds");
    }

}
