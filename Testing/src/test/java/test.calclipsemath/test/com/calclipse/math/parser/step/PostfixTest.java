package test.com.calclipse.math.parser.step;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.step.Postfix;

import test.TestUtil;

@DisplayName("Postfix")
public final class PostfixTest
{
    private static final Token
    PLUS = BinaryOperator.of("+", 1, (left, right) -> Values.UNDEF);
    
    private static final Token
    MINUS = BinaryOperator.of("-", 1, (left, right) -> Values.UNDEF);
    
    private static final Token
    TRANSPOSED = UnaryOperator.postfixed("^T", 4, arg -> Values.UNDEF);
    
    private static final Token
    SIN = UnaryOperator.prefixed("sin", 5, arg -> Values.UNDEF);
    
    private static final Token
    TIMES = BinaryOperator.of("*", 10, (left, right) -> Values.UNDEF);
    
    private static final Token
    LN = UnaryOperator.prefixed("ln", 20, arg -> Values.UNDEF);
    
    private static final Token
    FACTORIAL = UnaryOperator.postfixed("!", 30, arg -> Values.UNDEF);
    
    private static final Token LPAREN = Tokens.leftParenthesis("(");
    private static final Token RPAREN = Tokens.rightParenthesis(")");
    private static final Token A = Operand.of("a", Values.UNDEF);
    private static final Token B = Operand.of("b", Values.UNDEF);
    private static final Token C = Operand.of("c", Values.UNDEF);
    private static final Token D = Operand.of("d", Values.UNDEF);
    
    public PostfixTest()
    {
    }
    
    @Test
    @DisplayName("Binary operators")
    public void testBinaryOperators() throws ErrorMessage
    {
        // a + b * c - d => a b c * + d -
        final Fragment frag1 = Fragment.of(A,      0);
        final Fragment frag2 = Fragment.of(PLUS,  10);
        final Fragment frag3 = Fragment.of(B,     20);
        final Fragment frag4 = Fragment.of(TIMES, 30);
        final Fragment frag5 = Fragment.of(C,     40);
        final Fragment frag6 = Fragment.of(MINUS, 50);
        final Fragment frag7 = Fragment.of(D,     60);
        final Expression output = TestUtil.parse(
                Postfix.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);
        TestUtil.assertContainsSameFragments(
                output, frag1, frag3, frag5, frag4, frag2, frag7, frag6);
    }
    
    @Test
    @DisplayName("Parentheses overriding priorities")
    public void testParenthesesOverridingPriorities() throws ErrorMessage
    {
        // a * ( b + c ) => a b c + *
        final Fragment frag1 = Fragment.of(A,       0);
        final Fragment frag2 = Fragment.of(TIMES,  10);
        final Fragment frag3 = Fragment.of(LPAREN, 20);
        final Fragment frag4 = Fragment.of(B,      30);
        final Fragment frag5 = Fragment.of(PLUS,   40);
        final Fragment frag6 = Fragment.of(C,      50);
        final Fragment frag7 = Fragment.of(RPAREN, 60);
        final Expression output = TestUtil.parse(
                Postfix.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);
        TestUtil.assertContainsSameFragments(
                output, frag1, frag4, frag6, frag5, frag2);
    }
    
    @Test
    @DisplayName("Binary and unary operators")
    public void testBinaryAndUnaryOperators() throws ErrorMessage
    {
        /*  +   1
            -   1
            ^T  4
            sin 5
            *   10
            ln  20
            !   30  */
        final Fragment frag1 = Fragment.of(LN,          0);
        final Fragment frag2 = Fragment.of(SIN,        10);
        final Fragment frag3 = Fragment.of(A,          20);
        final Fragment frag4 = Fragment.of(TIMES,      30);
        final Fragment frag5 = Fragment.of(B,          40);
        final Fragment frag6 = Fragment.of(TRANSPOSED, 50);
        final Fragment frag7 = Fragment.of(FACTORIAL,  60);
        // ln sin a * b ^T ! => a b * sin ln ^T !
        final Expression output1 = TestUtil.parse(
                Postfix.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);
        TestUtil.assertContainsSameFragments(
                output1, frag3, frag5, frag4, frag2, frag1, frag6, frag7);
        // sin ln a * b ! ^T => a ln b ! * sin ^T
        final Expression output2 = TestUtil.parse(
                Postfix.INSTANCE,
                frag2, frag1, frag3, frag4, frag5, frag7, frag6);
        TestUtil.assertContainsSameFragments(
                output2, frag3, frag1, frag5, frag7, frag4, frag2, frag6);
    }

}
