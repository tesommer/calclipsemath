package test.com.calclipse.mcomp.script.task;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Tokens;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.task.Import;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("Import")
public final class ImportTest
{
    private static final Trace
    TRACE = new Trace.Builder().setScriptName("CrazyTrace").build();
    
    private static final String MCOMP_NAME = "Miaow";
    private static final String EXPORTED_SYMBOL_NAME = "Bokonon";
    private static final String LOCATION = "San Lorenzo";
    private static final String INVALID = "No, no, no, said Chicken-licken.";

    public ImportTest()
    {
    }
    
    @Test
    @DisplayName("execute")
    public void testExecute() throws ErrorMessage
    {
        final McContext context = MockContext.getOne();
        context.addLocator(MockLocator.INSTANCE);
        taskToTest(LOCATION).execute(context);
        assertTrue(
                context.symbol(
                        MCOMP_NAME
                        + context.namespaceSeparator()
                        + EXPORTED_SYMBOL_NAME)
                            .isPresent(),
                "Presence of imported symbol");
        assertThrows(
                ErrorMessage.class,
                () -> taskToTest(INVALID).execute(context),
                "Invalid location");
    }
    
    private static Task taskToTest(final String location)
    {
        return new Import(TRACE, location, Place.START);
    }
    
    private static final class MockMcomp implements Mcomp
    {
        private static final Mcomp INSTANCE = new MockMcomp();

        private MockMcomp()
        {
        }

        @Override
        public void execute(final McContext context) throws ErrorMessage
        {
            context.exportSymbol(Tokens.plain(EXPORTED_SYMBOL_NAME));
        }

        @Override
        public String name()
        {
            return MCOMP_NAME;
        }
        
    }
    
    private static enum MockLocator implements McLocator
    {
        INSTANCE;

        @Override
        public boolean accepts(final String location, final String referrer)
        {
            return LOCATION.equals(location);
        }

        @Override
        public Mcomp find(
                final String location,
                final String referrer) throws ErrorMessage
        {
            return MockMcomp.INSTANCE;
        }
        
    }

}
