package test.com.calclipse.math.parser.type;

import com.calclipse.math.parser.type.Association;

/**
 * An immutable mock association
 * that permits {@code null} as the key and/or value.
 * 
 * @author Tone Sommerland
 */
public final class MockAssociation implements Association
{
    private final Object key;
    private final Object value;

    public MockAssociation(final Object key, final Object value)
    {
        this.key = key;
        this.value = value;
    }

    @Override
    public Object getKey()
    {
        return key;
    }

    @Override
    public Object getValue()
    {
        return value;
    }

    /**
     * Unsupported operation.
     * @throws UnsupportedOperationException when called
     */
    @Override
    public Object setValue(final Object value)
    {
        throw new UnsupportedOperationException("setValue(" + value + ')');
    }

    @Override
    public boolean equals(final Object obj)
    {
        return Association.equals(this, obj);
    }

    @Override
    public int hashCode()
    {
        return Association.hashCode(this);
    }

    @Override
    public String toString()
    {
        return MockAssociation.class.getSimpleName()
                + '(' + key + '=' + value + ')';
    }

}
