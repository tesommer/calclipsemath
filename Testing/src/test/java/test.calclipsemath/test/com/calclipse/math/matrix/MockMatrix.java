package test.com.calclipse.math.matrix;

import com.calclipse.math.matrix.Matrix;

import test.TestUtil;

/**
 * A (fully functional) mock matrix.
 * 
 * @see test.TestUtil#populateMatrix(Matrix, double...)
 * 
 * @author Tone Sommerland
 */
public final class MockMatrix extends Matrix
{
    private int nRows;
    private int nCols;
    private double[][] entries;

    private MockMatrix(final int rows, final int columns)
    {
        this.nRows = rows;
        this.nCols = columns;
        this.entries = new double[rows][columns];
    }
    
    private MockMatrix(final MockMatrix matrix)
    {
        this.nRows = matrix.nRows;
        this.nCols = matrix.nCols;
        this.entries = new double[this.nRows][];
        for (int i = 0; i < this.nRows; i++)
        {
            this.entries[i] = matrix.entries[i].clone();
        }
    }
    
    public static Matrix valueOf(
            final int rows, final int columns, final double... entries)
    {
        final var matrix = new MockMatrix(rows, columns);
        TestUtil.populateMatrix(matrix, entries);
        return matrix;
    }

    @Override
    protected int rowCount()
    {
        return nRows;
    }

    @Override
    protected int columnCount()
    {
        return nCols;
    }

    @Override
    protected double entry(final int row, final int column)
    {
        return entries[row][column];
    }

    @Override
    protected void setEntry(
            final int row, final int column, final double entry)
    {
        entries[row][column] = entry;
        
    }

    @Override
    protected void setDimensions(
            final int rows, final int columns, final boolean preserveEntries)
    {
        final double[][] oldEntries = entries;
        entries = new double[rows][columns];
        if (preserveEntries)
        {
            final int preRows = Math.min(nRows, rows);
            final int preCols = Math.min(nCols, columns);
            for (int i = 0; i < preRows; i++)
            {
                for (int j = 0; j < preCols; j++)
                {
                    entries[i][j] = oldEntries[i][j];
                }
            }
        }
        nRows = rows;
        nCols = columns;
    }

    @Override
    protected Matrix duplicate()
    {
        return new MockMatrix(this);
    }

    @Override
    protected Matrix newInstance(final int rows, final int columns)
    {
        return new MockMatrix(rows, columns);
    }

}
