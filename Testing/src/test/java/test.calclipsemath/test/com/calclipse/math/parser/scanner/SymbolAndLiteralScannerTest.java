package test.com.calclipse.math.parser.scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Scanner;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenType;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.scanner.SymbolAndLiteralScanner;
import com.calclipse.math.parser.type.TypeContext;

import test.com.calclipse.math.parser.MockSymbolTable;
import test.com.calclipse.math.parser.type.MockTypeContext;

@DisplayName("SymbolAndLiteralScanner")
public final class SymbolAndLiteralScannerTest
{
    private final SymbolTable
    symbolTable = new MockSymbolTable();
    
    private final TypeContext
    typeContext = MockTypeContext.INSTANCE;
    
    private final Scanner
    scanner = new SymbolAndLiteralScanner(symbolTable, typeContext);

    public SymbolAndLiteralScannerTest()
    {
    }
    
    @AfterEach
    public void setUp()
    {
        symbolTable.clear();
    }
    
    @Test
    @DisplayName("Number literal with ipart, fpart and space")
    public void testNumberLiteralWithIpartFpartAndSpace() throws ErrorMessage
    {
        final String literal = " 2.34 56 ";
        final String valueString = "2.3456";
        scanner.reset(literal);
        final ScannerOutput output1 = ScannerOutput.operand(
                literal.trim(),
                1,
                typeContext.numberType().parse(valueString));
        assertScannerOutput(scanner, output1);
    }
    
    @Test
    @DisplayName("Number literal with only fpart")
    public void testNumberLiteralWithOnlyFpart() throws ErrorMessage
    {
        final String literal = ".2";
        final String valueString = literal;
        scanner.reset(literal);
        final ScannerOutput output1 = ScannerOutput.operand(
                literal.trim(),
                0,
                typeContext.numberType().parse(valueString));
        assertScannerOutput(scanner, output1);
    }
    
    @Test
    @DisplayName("Number literal with only ipart")
    public void testNumberLiteralWithOnlyIpart() throws ErrorMessage
    {
        final String literal = "2";
        final String valueString = literal;
        scanner.reset(literal);
        final ScannerOutput output1 = ScannerOutput.operand(
                literal.trim(),
                0,
                typeContext.numberType().parse(valueString));
        assertScannerOutput(scanner, output1);
    }
    
    @Test
    @DisplayName("String literal")
    public void testStringLiteral() throws ErrorMessage
    {
        scanner.reset(" ' a\"b ' \" c'd \" ");
        final ScannerOutput output1 = ScannerOutput.operand(
                "' a\"b '",
                1,
                " a\"b ");
        final ScannerOutput output2 = ScannerOutput.operand(
                "\" c'd \"",
                9,
                " c'd ");
        assertScannerOutput(scanner, output1, output2);
    }
    
    @Test
    @DisplayName("Space-separated symbols")
    public void testSpaceSeparatedSymbols() throws ErrorMessage
    {
        final Token tokenS = Tokens.plain("s");
        final Token tokenInt = Tokens.plain("int");
        final Token tokenSin = Tokens.plain("sin");
        final Token tokenAtan2 = Tokens.plain("atan2");
        final Token token3p4 = Tokens.plain("3.4");
        symbolTable.add(tokenS);
        symbolTable.add(tokenInt);
        symbolTable.add(tokenSin);
        symbolTable.add(tokenAtan2);
        symbolTable.add(token3p4);
        scanner.reset(" s int ");
        final ScannerOutput output1 = ScannerOutput.symbol(
                tokenS,
                1);
        final ScannerOutput output2 = ScannerOutput.symbol(
                tokenInt,
                3);
        assertScannerOutput(scanner, output1, output2);
    }
    
    @Test
    @DisplayName("Adjacent symbols, number and undef")
    public void testAdjacentSymbolsNumberAndUndef() throws ErrorMessage
    {
        final Token tokenS = Tokens.plain("s");
        final Token tokenInt = Tokens.plain("int");
        final Token tokenSin = Tokens.plain("sin");
        final Token tokenAtan2 = Tokens.plain("atan2");
        final Token token3p4 = Tokens.plain("3.4");
        symbolTable.add(tokenAtan2);
        symbolTable.add(tokenInt);
        symbolTable.add(tokenSin);
        symbolTable.add(token3p4);
        symbolTable.add(tokenS);
        // sin t atan2 5 3.4
        scanner.reset(" sintatan253.4");
        final ScannerOutput output1 = ScannerOutput.symbol(
                tokenSin,
                1);
        final ScannerOutput output2 = ScannerOutput.undef(
                "t",
                4);
        final ScannerOutput output3 = ScannerOutput.symbol(
                tokenAtan2,
                5);
        final ScannerOutput output4 = ScannerOutput.operand(
                "5",
                10,
                typeContext.numberType().parse("5"));
        final ScannerOutput output5 = ScannerOutput.symbol(
                token3p4,
                11);
        assertScannerOutput(
                scanner, output1, output2, output3, output4, output5);
    }
    
    @Test
    @DisplayName("Comment only")
    public void testCommentOnly() throws ErrorMessage
    {
        scanner.reset(" /* 1.2 */ ");
        assertScannerOutput(scanner);
    }
    
    @Test
    @DisplayName("Adjacent comments and numbers")
    public void testAdjacentCommentsAndNumbers() throws ErrorMessage
    {
        scanner.reset("/* abc */4/*'TEST'*/5");
        final ScannerOutput output1 = ScannerOutput.operand(
                "4",
                9,
                typeContext.numberType().parse("4"));
        final ScannerOutput output2 = ScannerOutput.operand(
                "5",
                20,
                typeContext.numberType().parse("5"));
        assertScannerOutput(scanner, output1, output2);
    }
    
    @Test
    @DisplayName("Invalid number caused by numeric symbol")
    public void testInvalidNumberCausedByNumericSymbol() throws ErrorMessage
    {
        symbolTable.add(Tokens.plain("2"));
        final String input = "  .2";
        scanner.reset(input);
        final ErrorMessage ex = assertThrows(
                ErrorMessage.class, () -> scan(scanner), input);
        assertEquals(
                2,
                ex.detail().get().position(),
                "1: detail.getPosition()");
    }
    
    @Test
    @DisplayName("Undelimited comment")
    public void testUndelimitedComment() throws ErrorMessage
    {
        final String input = "\r\n\t/* abc de \" ' /* '\r\n ";
        scanner.reset(input);
        final ErrorMessage ex = assertThrows(
                ErrorMessage.class, () -> scan(scanner), input);
        assertEquals(
                3,
                ex.detail().get().position(),
                "2: detail.getPosition()");
    }
    
    @Test
    @DisplayName("Undelimited string")
    public void testUndelimitedString() throws ErrorMessage
    {
        final String input = "''\"\"\"/* abc de  ' /* '\r\n ";
        scanner.reset(input);
        final ErrorMessage ex = assertThrows(
                ErrorMessage.class, () -> scan(scanner), input);
        assertEquals(
                4,
                ex.detail().get().position(),
                "3: detail.getPosition()");
    }
    
    @Test
    @DisplayName("Adjacent number and space-prefixed symbol")
    public void testAdjacentNumberAndSpacePrefixedSymbol()
            throws ErrorMessage
    {
        final Token token = Tokens.plain(" abs");
        symbolTable.add(token);
        scanner.reset("21 abs22  23  abs");
        final ScannerOutput output1 = ScannerOutput.operand(
                "21",
                0,
                typeContext.numberType().parse("21"));
        final ScannerOutput output2 = ScannerOutput.symbol(
                token,
                2);
        final ScannerOutput output3 = ScannerOutput.operand(
                "22  23",
                6,
                typeContext.numberType().parse("2223"));
        final ScannerOutput output4 = ScannerOutput.symbol(
                token,
                13);
        assertScannerOutput(scanner, output1, output2, output3, output4);
    }
    
    private static List<Fragment> scan(final Scanner scanner)
            throws ErrorMessage
    {
        final var fragments = new ArrayList<Fragment>();
        while (scanner.hasNext())
        {
            fragments.add(scanner.next());
        }
        return fragments;
    }
    
    private static void assertScannerOutput(
            final Scanner scanner,
            final ScannerOutput... expectedOutput) throws ErrorMessage
    {
        final var actualOutput = scan(scanner);
        assertEquals(
                expectedOutput.length,
                actualOutput.size(),
                "Number of fragments returned from scanner");
        for (int i = 0; i < expectedOutput.length; i++)
        {
            final ScannerOutput expected = expectedOutput[i];
            final Fragment actual = actualOutput.get(i);
            assertScannerOutput(expected, actual, i);
        }
    }
    
    private static void assertScannerOutput(
            final ScannerOutput expected,
            final Fragment actual,
            final int index)
    {
        assertEquals(
                expected.name,
                actual.token().name(),
                "Name of token at index: " + index);
        assertSame(
                expected.type,
                actual.token().type(),
                "Type of token at index: " + index);
        assertEquals(
                expected.position,
                actual.position(),
                "Position of fragment at index: " + index);
        if  (expected.value != null)
        {
            final var operand = (Operand)actual.token();
            assertEquals(
                    expected.value,
                    operand.get(),
                    "Value of operand at index: " + index);
        }
        if (expected.token != null)
        {
            assertSame(
                    expected.token,
                    actual.token(),
                    "Token at index: " + index);
        }
    }
    
    private static final class ScannerOutput
    {
        private final String name;
        private final int position;
        private final Object value;
        private final TokenType type;
        private final Token token;
        
        private ScannerOutput(
                final String name,
                final TokenType type,
                final int position,
                final Object value,
                final Token token)
        {
            assert name != null;
            assert type != null;
            this.name = name;
            this.position = position;
            this.value = value;
            this.type = type;
            this.token = token;
        }
        
        private static ScannerOutput operand(
                final String name, final int position, final Object value)
        {
            return new ScannerOutput(
                    name, TokenType.OPERAND, position, value, null);
        }
        
        private static ScannerOutput undef(
                final String name, final int position)
        {
            return new ScannerOutput(
                    name, TokenType.UNDEFINED, position, null, null);
        }

        private static ScannerOutput symbol(
                final Token token, final int position)
        {
            return new ScannerOutput(
                    token.name(), token.type(), position, null, token);
        }
        
    }

}
