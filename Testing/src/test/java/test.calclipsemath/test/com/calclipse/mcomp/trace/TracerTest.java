package test.com.calclipse.mcomp.trace;

import static java.text.MessageFormat.format;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;
import com.calclipse.mcomp.trace.Tracer;

@DisplayName("Tracer")
public final class TracerTest
{
    public TracerTest()
    {
    }
    
    @Nested
    @DisplayName("Tracer.getDefault()")
    public static final class GetDefaultTest
    {
        private static final String MESSAGE_FORMAT = "M{0}";
        private static final String SCRIPT_NAME_FORMAT = "N{0}";
        private static final String SCRIPT_LOCATION_FORMAT = "L{0}";
        private static final String PLACE_FORMAT = "P{0}_{1}";
        private static final String CAUSE_FORMAT = "C{0}_{1}";
        
        private static final String MESSAGE = "message";
        private static final String SCRIPT_NAME = "name";
        private static final String SCRIPT_LOCATION = "location";
        
        private static final Tracer
        TRACER = Tracer.composition(
                MESSAGE_FORMAT,
                SCRIPT_NAME_FORMAT,
                SCRIPT_LOCATION_FORMAT,
                PLACE_FORMAT,
                CAUSE_FORMAT);

        public GetDefaultTest()
        {
        }
        
        @Test
        @DisplayName("Extract from single-line expression")
        public void testExtractFromSingleLineExpression()
        {
            final String expression = "a+b / c-F";
            final Token token = Tokens.plain("/");
            final Fragment fragment = Fragment.of(token, 4);
            final Place base = Place.atLineColumn(2, 7);
            final Pinpoint pinpoint = Pinpoint.at(expression, fragment);
            final Trace trace = extract(base, pinpoint);
            assertExtraction(2, 11, token.name(), "a+b / c-F", trace);
        }
        
        @Test
        @DisplayName("Extract from multiline expression")
        public void testExtractFromMultilineExpression()
        {
            final String expression = "a+b+c\n\nD * E\r\nf-h-g";
            final Token token = Tokens.plain("*");
            final Fragment fragment = Fragment.of(token, 9);
            final Place base = Place.atLineColumn(7, 11);
            final Pinpoint pinpoint = Pinpoint.at(expression, fragment);
            final Trace trace = extract(base, pinpoint);
            assertExtraction(9, 3, token.name(), "D * E", trace);
        }
        
        private static Trace extract(
                final Place place, final Pinpoint pinpoint)
        {
            return new Trace.Builder()
                    .setScriptName(SCRIPT_NAME)
                    .setScriptLocation(SCRIPT_LOCATION)
                    .setPlace(place)
                    .setPinpoint(pinpoint)
                    .build();
        }
        
        private static void assertExtraction(
                final int expectedLine,
                final int expectedColumn,
                final String expectedToken,
                final String expectedExtract,
                final Trace trace)
        {
            final String expected
                    = format(MESSAGE_FORMAT, MESSAGE)
                    + format(SCRIPT_NAME_FORMAT, SCRIPT_NAME)
                    + format(SCRIPT_LOCATION_FORMAT, SCRIPT_LOCATION)
                    + format(PLACE_FORMAT, expectedLine, expectedColumn)
                    + format(CAUSE_FORMAT, expectedToken, expectedExtract);
            final String actual = TRACER.track(MESSAGE, trace).getMessage();
            assertEquals(expected, actual, "extraction");
        }
        
    }

}
