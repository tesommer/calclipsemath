package test.com.calclipse.math.matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.LuFactorization;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.Permutation;
import com.calclipse.math.matrix.size.SizeViolation;

import test.TestUtil;

@DisplayName("LuFactorization")
public final class LuFactorizationTest
{
    private static final double DELTA = 1.0E-13;

    public LuFactorizationTest()
    {
    }
    
    @Test
    @DisplayName("Factorize non-augmented matrix")
    public void testFactorizeNonAugmentedMatrix()
    {
        final Matrix mx = MockMatrix.valueOf(
                2, 2,
                1, 2,
                3, 4);
        assertLu(mx, false);
        assertLu(mx, true);
        final Matrix mx2 = MockMatrix.valueOf(
                2, 2,
                0, 1,
                1, 0);
        assertLu(mx2, false);
        assertLu(mx2, true);
        final Matrix mx3 = MockMatrix.valueOf(
                3, 4,
                1,  2,  3,  4,
                5,  6,  7,  8,
                9, 10, 11, 12);
        assertLu(mx3, false);
        assertLu(mx3, true);
        final Matrix mx4 = MockMatrix.valueOf(
                4, 3,
                 1,  2,  3,
                 4,  5,  6,
                 7,  8,  9,
                10, 11, 12);
        assertLu(mx4, false);
        assertLu(mx4, true);
        final Matrix mx5 = MockMatrix.valueOf(
                3, 3,
                0, 0, 0,
                0, 0, 0,
                0, 0, 0);
        assertLu(mx5, false);
        assertLu(mx5, true);
    }
    
    @Test
    @DisplayName("Factorize augmented matrix")
    public void testFactorizeAugmentedMatrix()
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        final Matrix aug = MockMatrix.valueOf(
                3, 1,
                100,
                101,
                102);
        final Matrix actualAugWOPiv = assertLu(mx, false, aug);
        final Matrix expectedAugWOPiv = MockMatrix.valueOf(
                3, 1,
                100,
                -299,
                0);
        TestUtil.assertEquals(
                expectedAugWOPiv, actualAugWOPiv, DELTA,
                "Aug after decomposition w/o pivoting\nExpected:\n"
                        + expectedAugWOPiv
                        + "\nActual:\n"
                        + actualAugWOPiv);
        final Matrix actualAugWPiv = assertLu(mx, true, aug);
        final Matrix expectedAugWPiv = MockMatrix.valueOf(
                3, 1,
                102,
                598.0 / 7,
                0);
        TestUtil.assertEquals(
                expectedAugWPiv, actualAugWPiv, DELTA,
                "Aug after decomposition with pivoting\nExpected:\n"
                        + expectedAugWPiv
                        + "\nActual:\n"
                        + actualAugWPiv);
    }
    
    @Test
    @DisplayName("Factorize augmented matrix with first pivot zero")
    public void testFactorizeAugmentedMatrixWithFirstPivotZero()
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 3,
                0, 2, 3,
                4, 5, 6,
                7, 8, 9);
        final Matrix aug = MockMatrix.valueOf(
                3, 1,
                100,
                101,
                102);
        final Matrix actualAugWOPiv = assertLu(mx, false, aug);
        final Matrix expectedAugWOPiv = MockMatrix.valueOf(
                3, 1,
                101,
                100,
                -149.0/4);
        TestUtil.assertEquals(
                expectedAugWOPiv, actualAugWOPiv, DELTA,
                "Aug, w/o pivoting, zero at first pivot\nExpected:\n"
                        + expectedAugWOPiv
                        + "\nActual:\n"
                        + actualAugWOPiv);
        final Matrix actualAugWPiv = assertLu(mx, true, aug);
        final Matrix expectedAugWPiv = MockMatrix.valueOf(
                3, 1,
                102,
                100,
                149.0/7);
        TestUtil.assertEquals(
                expectedAugWPiv, actualAugWPiv, DELTA,
                "Aug, with pivoting, zero at first pivot\nExpected:\n"
                        + expectedAugWPiv
                        + "\nActual:\n"
                        + actualAugWPiv);
    }
    
    @Test
    @DisplayName("Factorize augmented column")
    public void testFactorizeAugmentedColumn()
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 1,
                1,
                2,
                3);
        final Matrix aug = MockMatrix.valueOf(
                3, 1,
                100,
                101,
                102);
        final Matrix actualAugWOPiv = assertLu(mx, false, aug);
        final Matrix expectedAugWOPiv = MockMatrix.valueOf(
                3, 1,
                100,
                -99,
                -198);
        TestUtil.assertEquals(
                expectedAugWOPiv, actualAugWOPiv, DELTA,
                "Aug after decomposition of column w/o pivoting\nExpected:\n"
                        + expectedAugWOPiv
                        + "\nActual:\n"
                        + actualAugWOPiv);
        final Matrix actualAugWPiv = assertLu(mx, true, aug);
        final Matrix expectedAugWPiv = MockMatrix.valueOf(
                3, 1,
                102,
                33,
                66);
        TestUtil.assertEquals(
                expectedAugWPiv, actualAugWPiv, DELTA,
                "Aug after decomposition of column with pivoting\nExpected:\n"
                        + expectedAugWPiv
                        + "\nActual:\n"
                        + actualAugWPiv);
    }
    
    @Test
    @DisplayName("Factorize and fail if argument is modified")
    public void testFactorizeAndFailIfArgumentIsModified()
    {
        final Matrix mx = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        final Matrix aug = MockMatrix.valueOf(
                3, 1,
                100,
                101,
                102);
        assertLu(mx, false);
        assertLu(mx, true);
        assertLu(mx, false, aug);
        assertLu(mx, true, aug);
        final Matrix expectedMx = MockMatrix.valueOf(
                3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        final Matrix expectedAug = MockMatrix.valueOf(
                3, 1,
                100,
                101,
                102);
        assertEquals(
                expectedMx,
                mx,
                "Modification of coefficient matrix");
        assertEquals(
                expectedAug,
                aug,
                "Modification of augmentation");
    }

    @Test
    @DisplayName("Factorize 0x0 with 0x0")
    public void testFactorize0x0With0x0()
    {
        final Matrix mx_0x0 = MockMatrix.valueOf(0, 0);
        final Matrix aug_0x0 = MockMatrix.valueOf(0, 0);
        factorizeAndAssertZeroByZero(mx_0x0, aug_0x0, true, true, true, true);
    }
    
    @Test
    @DisplayName("Factorize 0x0 with 0x2")
    public void testFactorize0x0With0x2()
    {
        final Matrix mx_0x0 = MockMatrix.valueOf(0, 0);
        final Matrix aug_0x2 = MockMatrix.valueOf(0, 2);
        final LuFactorization lu_0x0_0x2 = factorizeAndAssertZeroByZero(
                mx_0x0, aug_0x2, true, true, true, false);
        TestUtil.assertZeroRows(2, lu_0x0_0x2.augmentation().get());
    }
    
    @Test
    @DisplayName("Factorize 2x0 with 2x0")
    public void testFactorize2x0With2x0()
    {
        final Matrix mx_2x0 = MockMatrix.valueOf(2, 0);
        final Matrix aug_2x0 = MockMatrix.valueOf(2, 0);
        final LuFactorization lu_2x0_2x0 = factorizeAndAssertZeroByZero(
                mx_2x0, aug_2x0, false, false, false, false);
        assertEquals(
                MockMatrix.valueOf(2, 2, 0, 0, 0, 0),
                lu_2x0_2x0.l(),
                "lu_2x0_2x0.getL()");
        TestUtil.assertZeroColumns(2, lu_2x0_2x0.u());
        assertEquals(
                MockMatrix.valueOf(2, 2, 1, 0, 0, 1),
                p(lu_2x0_2x0),
                "lu_2x0_2x0.getP()");
        TestUtil.assertZeroColumns(2, lu_2x0_2x0.augmentation().get());
    }
    
    @Test
    @DisplayName("Factorize 2x0 with 2x2")
    public void testFactorize2x0With2x2()
    {
        final Matrix mx_2x0 = MockMatrix.valueOf(2, 0);
        final Matrix aug_2x2 = MockMatrix.valueOf(2, 2, 1, 2, 3, 4);
        final LuFactorization lu_2x0_2x2 = factorizeAndAssertZeroByZero(
                mx_2x0, aug_2x2, false, false, false, false);
        assertEquals(
                MockMatrix.valueOf(2, 2, 0, 0, 0, 0),
                lu_2x0_2x2.l(),
                "lu_2x0_2x2.getL()");
        TestUtil.assertZeroColumns(2, lu_2x0_2x2.u());
        assertEquals(
                MockMatrix.valueOf(2, 2, 1, 0, 0, 1),
                p(lu_2x0_2x2),
                "lu_2x0_2x2.getP()");
        assertEquals(
                MockMatrix.valueOf(2, 2, 1, 2, 3, 4),
                lu_2x0_2x2.augmentation().get(),
                "lu_2x0_2x2.getAugmentation()");
    }
    
    @Test
    @DisplayName("Factorize 0x2 with 0x0")
    public void testFactorize0x2With0x0()
    {
        final Matrix mx_0x2 = MockMatrix.valueOf(0, 2);
        final Matrix aug_0x0 = MockMatrix.valueOf(0, 0);
        final LuFactorization lu_0x2_0x0 = factorizeAndAssertZeroByZero(
                mx_0x2, aug_0x0, true, false, true, true);
        TestUtil.assertZeroRows(2, lu_0x2_0x0.u());
    }
    
    @Test
    @DisplayName("Factorize 0x2 with 0x2")
    public void testFactorize0x2With0x2()
    {
        final Matrix mx_0x2 = MockMatrix.valueOf(0, 2);
        final Matrix aug_0x2 = MockMatrix.valueOf(0, 2);
        final LuFactorization lu_0x2_0x2 = factorizeAndAssertZeroByZero(
                mx_0x2, aug_0x2, true, false, true, false);
        TestUtil.assertZeroRows(2, lu_0x2_0x2.u());
        TestUtil.assertZeroRows(2, lu_0x2_0x2.augmentation().get());
    }
    
    @Test
    @DisplayName("Factorize 2x2 with 2x0")
    public void testFactorize2x2With2x0()
    {
        final Matrix mx_2x2 = MockMatrix.valueOf(2, 2, 1, 2, 3, 4);
        final Matrix aug_2x0 = MockMatrix.valueOf(2, 0);
        TestUtil.assertZeroColumns(2, assertLu(mx_2x2, true, aug_2x0));
    }
    
    @Test
    @DisplayName("Size violation")
    public void testSizeViolation()
    {
        final Matrix mx_2x2 = MockMatrix.valueOf(2, 2);
        final Matrix aug_3x1 = MockMatrix.valueOf(3, 1);
        assertThrows(
                SizeViolation.class,
                () -> LuFactorization.factorizeAugmented(mx_2x2, aug_3x1, true),
                "(2x2, 3x1)");
    }
    
    /**
     * Creates an LU factorization of a matrix,
     * then asserts that PLU yields the original,
     * that L is lower triangular with diagonal ones or zeroes,
     * and that U is on echelon form.
     * @param mx the matrix to factorize
     * @param pivoting partial pivoting or not
     * @param aug augmentation (nullable)
     * @return the augmentaion after decomposition or null.
     */
    private static Matrix assertLu(
            final Matrix mx, final boolean pivoting, final Matrix aug)
    {
        final LuFactorization luFac = aug == null
                ? LuFactorization.factorize(mx, pivoting)
                : LuFactorization.factorizeAugmented(mx, aug, pivoting);
        final Matrix l = luFac.l();
        final Matrix u = luFac.u();
        final Permutation p = luFac.p();
        final Matrix plu = l.times(u);
        p.preApply(plu);
        TestUtil.assertEquals(
                mx, plu, DELTA,
                "mx approx equals plu\nmx:\n" + mx + "\nplu:\n" + plu);
        assertTrue(
                TestUtil.isLowerTriangular(l, DELTA),
                "isLowerTriangular(l)\n" + l);
        assertTrue(
                hasDiagonalOnesOrZeroes(l),
                "hasDiagonalOnesOrZeroes(l)\n" + l);
        assertTrue(
                TestUtil.isEchelon(u, DELTA),
                "isEchelon(u)\n" + u);
        return luFac.augmentation().orElse(null);
    }
    
    /**
     * Same as {@link #assertLu(Matrix, boolean, Matrix)},
     * but without an augmentation.
     */
    private static void assertLu(final Matrix mx, final boolean pivoting)
    {
        assertLu(mx, pivoting, null);
    }
    
    private static boolean hasDiagonalOnesOrZeroes(final Matrix mx)
    {
        for (int i = 0; i < mx.rows(); i++)
        {
            if (mx.get(i, i) != 1 && mx.get(i, i) != 0)
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Factorizes an augmented matrix,
     * with pivoting,
     * and asserts that selected components are zero by zero in size.
     * @param mx the coefficient matrix
     * @param aug the augmentation
     * @param assertZeroByZeroL true to assert L is zero by zero
     * @param assertZeroByZeroU true to assert U is zero by zero
     * @param assertZeroByZeroP true to assert P is zero by zero
     * @param assertZeroByZeroAug
     * true to assert the augmentation is zero by zero
     * @return the LU factorization
     */
    private static LuFactorization factorizeAndAssertZeroByZero(
            final Matrix mx,
            final Matrix aug,
            final boolean assertZeroByZeroL,
            final boolean assertZeroByZeroU,
            final boolean assertZeroByZeroP,
            final boolean assertZeroByZeroAug)
    {
        final LuFactorization luFac
            = LuFactorization.factorizeAugmented(mx, aug, true);
        if (assertZeroByZeroL)
        {
            TestUtil.assertZeroByZero(luFac.l());
        }
        if (assertZeroByZeroU)
        {
            TestUtil.assertZeroByZero(luFac.u());
        }
        if (assertZeroByZeroP)
        {
            TestUtil.assertZeroByZero(p(luFac));
        }
        if (assertZeroByZeroAug)
        {
            TestUtil.assertZeroByZero(luFac.augmentation().get());
        }
        return luFac;
    }
    
    private static Matrix p(final LuFactorization lu)
    {
        final Matrix p = MockMatrix.valueOf(1, 1);
        lu.p().copyEntriesTo(p);
        return p;
    }

}
