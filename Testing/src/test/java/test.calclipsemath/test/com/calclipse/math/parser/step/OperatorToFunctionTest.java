package test.com.calclipse.math.parser.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.step.OperatorToFunction;

import test.TestUtil;

@DisplayName("OperatorToFunction")
public final class OperatorToFunctionTest
{
    private static final Token LPAREN = Tokens.leftParenthesis("(");
    private static final Token RPAREN = Tokens.rightParenthesis(")");
    private static final Token COMMA = Tokens.plain(",");
    
    private static final Token A = Operand.of("A", Value.constantOf(11));
    private static final Token B = Operand.of("B", Value.constantOf(12));
    private static final Token C = Operand.of("C", Value.constantOf(13));
    private static final Token D = Operand.of("D", Value.constantOf(14));
    
    private static final Operator
    AND_OPERATOR = BinaryOperator.of("&&", 0, (left, right) -> Values.UNDEF);
    
    private static final Function
    AND_FUNCTION = Function.of(
            "andfunc",
            Delimitation.byTokensWithOpener(
                    LPAREN, RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);
    
    private static final Operator
    UAND_OPERATOR = UnaryOperator.prefixed("&", 3, arg -> Values.UNDEF);
    
    private static final Function
    UAND_FUNCTION = Function.of(
            "uandfunc",
            Delimitation.byTokensWithOpener(
                    LPAREN, RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);
    
    private static final Token
    PLUS = BinaryOperator.of("+", 1, (left, right) -> Values.UNDEF);
    
    private static final Token
    MINUS = BinaryOperator.of("-", 1, (left, right) -> Values.UNDEF);
    
    private static final Token
    TIMES = BinaryOperator.of("*", 2, (left, right) -> Values.UNDEF);
    
    private static final Token
    DIVIDED_BY = BinaryOperator.of("/", 2, (left, right) -> Values.UNDEF);
    
    private static final Token
    SQRT = UnaryOperator.prefixed("sqrt", 3, arg -> Values.UNDEF);
    
    private static final ParseStep
    STEP = new OperatorToFunction(MockReplacer.INSTANCE);

    private OperatorToFunctionTest()
    {
    }
    
    @Test
    @DisplayName("Empty expression")
    public void testEmptyExpression() throws ErrorMessage
    {
        assertTrue(
                TestUtil.parse(STEP).isEmpty(),
                "Assert output is empty");
    }
    
    @Test
    @DisplayName("Empty expression")
    public void testNoReplacements() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A,    10);
        final Fragment frag2 = Fragment.of(B,    20);
        final Fragment frag3 = Fragment.of(PLUS, 30);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3);
        TestUtil.assertContainsSameFragments(
                output, frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Simple one-level expression with one replacement")
    public void testSimpleOneLevelExpressionWithOneReplacement()
            throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A,            10);
        final Fragment frag2 = Fragment.of(B,            20);
        final Fragment frag3 = Fragment.of(AND_OPERATOR, 30);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, (Fragment)null);
        assertBinaryReplacement(frag3, output, 0);
        assertReplacementArg(output, 0, 0, frag1);
        assertReplacementArg(output, 0, 1, frag2);
    }
    
    @Test
    @DisplayName("Complex one-level expression with one replacement")
    public void testComplexOneLevelExpressionWithOneReplacement()
            throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A,            10);
        final Fragment frag2 = Fragment.of(A,            20);
        final Fragment frag3 = Fragment.of(B,            30);
        final Fragment frag4 = Fragment.of(PLUS,         40);
        final Fragment frag5 = Fragment.of(C,            50);
        final Fragment frag6 = Fragment.of(D,            60);
        final Fragment frag7 = Fragment.of(SQRT,         70);
        final Fragment frag8 = Fragment.of(MINUS,        80);
        final Fragment frag9 = Fragment.of(AND_OPERATOR, 90);
        final Fragment frag10 = Fragment.of(TIMES,      100);
        final Fragment frag11 = Fragment.of(D,          110);
        final Fragment frag12 = Fragment.of(DIVIDED_BY, 120);
        final Expression output = TestUtil.parse(
                STEP,
                frag1, frag2, frag3, frag4, frag5, frag6,
                frag7, frag8, frag9, frag10, frag11, frag12);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, frag1, null, frag10, frag11, frag12);
        assertBinaryReplacement(frag9, output, 1);
        assertReplacementArg(output, 1, 0, frag2, frag3, frag4);
        assertReplacementArg(output, 1, 1, frag5, frag6, frag7, frag8);
    }
    
    @Test
    @DisplayName("Complex multi-level expression with multiple replacements")
    public void testComplexMultiLevelExpressionWithMultipleReplacements()
            throws ErrorMessage
    {
        // Input:  A SQRT B && B C - D && &&
        // Output: &&
        //          &&,        &&
        //           A SQRT, B  B C -, D
        
        final Fragment frag1 = Fragment.of(A,              10);
        final Fragment frag2 = Fragment.of(SQRT,           20);
        final Fragment frag3 = Fragment.of(B,              30);
        final Fragment frag4 = Fragment.of(AND_OPERATOR,   40);
        final Fragment frag5 = Fragment.of(B,              50);
        final Fragment frag6 = Fragment.of(C,              60);
        final Fragment frag7 = Fragment.of(MINUS,          70);
        final Fragment frag8 = Fragment.of(D,              80);
        final Fragment frag9 = Fragment.of(AND_OPERATOR,   90);
        final Fragment frag10 = Fragment.of(AND_OPERATOR, 100);
        final Expression output = TestUtil.parse(
                STEP,
                frag1, frag2, frag3, frag4, frag5,
                frag6, frag7, frag8, frag9, frag10);
        
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, (Fragment)null);
        assertBinaryReplacement(frag10, output, 0);
        assertReplacementArgButSkipNulls(output, 0, 0, (Fragment)null);
        assertReplacementArgButSkipNulls(output, 0, 1, (Fragment)null);
        
        final Expression arg1 = arg(output, 0, 0);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                arg1, (Fragment)null);
        assertBinaryReplacement(frag4, arg1, 0);
        assertReplacementArg(arg1, 0, 0, frag1, frag2);
        assertReplacementArg(arg1, 0, 1, frag3);
        
        final Expression arg2 = arg(output, 0, 1);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                arg2, (Fragment)null);
        assertBinaryReplacement(frag9, arg2, 0);
        assertReplacementArg(arg2, 0, 0, frag5, frag6, frag7);
        assertReplacementArg(arg2, 0, 1, frag8);
    }
    
    @Test
    @DisplayName("One-level expression with multiple replacements")
    public void testOneLevelExpressionWithMultipleReplacements()
            throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A,            10);
        final Fragment frag2 = Fragment.of(B,            20);
        final Fragment frag3 = Fragment.of(AND_OPERATOR, 30);
        final Fragment frag4 = Fragment.of(C,            40);
        final Fragment frag5 = Fragment.of(D,            50);
        final Fragment frag6 = Fragment.of(AND_OPERATOR, 60);
        final Fragment frag7 = Fragment.of(PLUS,         70);
        final Expression output = TestUtil.parse(
                STEP,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, null, null, frag7);
        assertBinaryReplacement(frag3, output, 0);
        assertReplacementArg(output, 0, 0, frag1);
        assertReplacementArg(output, 0, 1, frag2);
        assertBinaryReplacement(frag6, output, 1);
        assertReplacementArg(output, 1, 0, frag4);
        assertReplacementArg(output, 1, 1, frag5);
    }
    
    @Test
    @DisplayName("Binary and unary replacement")
    public void testBinaryAndUnaryReplacement() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A,             10);
        final Fragment frag2 = Fragment.of(B,             20);
        final Fragment frag3 = Fragment.of(AND_OPERATOR,  30);
        final Fragment frag4 = Fragment.of(C,             40);
        final Fragment frag5 = Fragment.of(UAND_OPERATOR, 50);
        final Fragment frag6 = Fragment.of(PLUS,          60);
        final Expression output = TestUtil.parse(
                STEP,
                frag1, frag2, frag3, frag4, frag5, frag6);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, null, null, frag6);
        assertBinaryReplacement(frag3, output, 0);
        assertReplacementArg(output, 0, 0, frag1);
        assertReplacementArg(output, 0, 1, frag2);
        assertUnaryReplacement(frag5, output, 1);
        assertReplacementArg(output, 1, 0, frag4);
    }
    
    private static void assertBinaryReplacement(
            final Fragment original,
            final Expression output,
            final int replacementIndex)
    {
        final Fragment replacement = output.get(replacementIndex);
        assertEquals(
                original.position(),
                replacement.position(),
                "Replacement position");
        assertTrue(
                replacement.token().type().isFunction(),
                "Replacement type");
        assertEquals(
                AND_OPERATOR.name(),
                replacement.token().name(),
                "Replacement name");
        assertEquals(
                2,
                ((Function)replacement.token()).arguments().size(),
                "Arg count");
    }
    
    private static void assertUnaryReplacement(
            final Fragment original,
            final Expression output,
            final int replacementIndex)
    {
        final Fragment replacement = output.get(replacementIndex);
        assertEquals(
                original.position(),
                replacement.position(),
                "Replacement position");
        assertTrue(
                replacement.token().type().isFunction(),
                "Replacement type");
        assertEquals(
                UAND_OPERATOR.name(),
                replacement.token().name(),
                "Replacement name");
        assertEquals(
                1,
                ((Function)replacement.token()).arguments().size(),
                "Arg count");
    }
    
    private static void assertReplacementArg(
            final Expression output,
            final int replacementIndex,
            final int argIndex,
            final Fragment... expected)
    {
        final var func = (Function)output.get(replacementIndex).token();
        TestUtil.assertContainsSameFragments(
                func.arguments().get(argIndex), expected);
    }
    
    private static void assertReplacementArgButSkipNulls(
            final Expression output,
            final int replacementIndex,
            final int argIndex,
            final Fragment... expected)
    {
        final var func = (Function)output.get(replacementIndex).token();
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                func.arguments().get(argIndex), expected);
    }
    
    private static Expression arg(
            final Expression expression,
            final int funcIndex,
            final int argIndex)
    {
        return ((Function)expression.get(funcIndex).token())
                .arguments().get(argIndex);
    }
    
    private static enum MockReplacer
        implements OperatorToFunction.Replacer
    {
        INSTANCE;

        @Override
        public boolean isTarget(final Operator operator)
        {
            return operator == AND_OPERATOR || operator == UAND_OPERATOR;
        }

        @Override
        public Function replace(final Operator operator)
        {
            return operator == AND_OPERATOR ?
                    AND_FUNCTION : UAND_FUNCTION;
        }
        
    }

}
