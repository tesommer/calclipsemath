package test.com.calclipse.math.expression;

import static com.calclipse.math.expression.Errors.errorMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;

import test.TestUtil;

@DisplayName("TokenStream")
public final class TokenStreamTest
{
    private static final Token LPAREN = Tokens.leftParenthesis("(");
    private static final Token RPAREN = Tokens.rightParenthesis(")");
    private static final Token COMMA = Tokens.plain(",");
    private static final Token A = Tokens.plain("a");
    private static final Token B = Tokens.plain("b");
    private static final Token C = Tokens.plain("c");
    
    private static final Token
    FUNC =  Function.of(
            "func",
            Delimitation.byTokensWithOpener(
                    LPAREN, RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);

    public TokenStreamTest()
    {
    }
    
    @Test
    @DisplayName("Initial step data")
    public void testInitialStepData() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A, 0);
        TestUtil.parse(AssertInitialDataStep.INSTANCE, frag1);
    }
    
    @Test
    @DisplayName("Simple one-level expression")
    public void testSimpleOneLevelExpression() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A,  0);
        final Fragment frag2 = Fragment.of(B, 10);
        final Fragment frag3 = Fragment.of(C, 20);
        final Expression output = TestUtil.parse(
                new ParseStep[0], frag1, frag2, frag3);
        TestUtil.assertContainsSameFragments(output, frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Multi-node expression")
    public void testMultiNodeExpression() throws ErrorMessage
    {
        // Input:  A FUNC ( A FUNC ( B C ) FUNC ( ) C )
        // Output: A FUNC
        //              A FUNC   FUNC C
        //                   B C
        
        final Fragment frag1 = Fragment.of(A,         0);
        final Fragment frag2 = Fragment.of(FUNC,     10);
        final Fragment frag3 = Fragment.of(LPAREN,   20);
        final Fragment frag4 = Fragment.of(A,        30);
        final Fragment frag5 = Fragment.of(FUNC,     40);
        final Fragment frag6 = Fragment.of(LPAREN,   50);
        final Fragment frag7 = Fragment.of(B,        60);
        final Fragment frag8 = Fragment.of(C,        70);
        final Fragment frag9 = Fragment.of(RPAREN,   80);
        final Fragment frag10 = Fragment.of(FUNC,    90);
        final Fragment frag11 = Fragment.of(LPAREN, 100);
        final Fragment frag12 = Fragment.of(RPAREN, 110);
        final Fragment frag13 = Fragment.of(C,      120);
        final Fragment frag14 = Fragment.of(RPAREN, 130);
        
        final Expression output = TestUtil.parse(
                NodeStep.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7,
                frag8, frag9, frag10, frag11, frag12, frag13, frag14);
        
        TestUtil.assertContainsSameFragmentsButSkipNulls(output, frag1, null);
        assertOutput(output, A, FUNC);
        assertArgCount(1, output.get(1));
        final Expression arg1 = arg(output.get(1), 0);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                arg1, frag4, null, null, frag13);
        assertOutput(arg1, A, FUNC, FUNC, C);
        assertArgCount(1, arg1.get(1));
        final Expression arg2 = arg(arg1.get(1), 0);
        TestUtil.assertContainsSameFragments(arg2, frag7, frag8);
        assertOutput(arg2, B, C);
        assertArgCount(0, arg1.get(2));
    }
    
    @Test
    @DisplayName("Multi-node expression with writing step")
    public void testMultiNodeExpressionWithWritingStep() throws ErrorMessage
    {
        // Input:  B FUNC ( B FUNC ( B A ) )
        // Output: FUNC        B
        //            FUNC   B
        //               A B
        
        final Fragment frag1 = Fragment.of(B,        0);
        final Fragment frag2 = Fragment.of(FUNC,    10);
        final Fragment frag3 = Fragment.of(LPAREN,  20);
        final Fragment frag4 = Fragment.of(B,       30);
        final Fragment frag5 = Fragment.of(FUNC,    40);
        final Fragment frag6 = Fragment.of(LPAREN,  50);
        final Fragment frag7 = Fragment.of(B,       60);
        final Fragment frag8 = Fragment.of(A,       70);
        final Fragment frag9 = Fragment.of(RPAREN,  80);
        final Fragment frag10 = Fragment.of(RPAREN, 90);
        
        final Expression output = TestUtil.parse(
                new ParseStep[] {NodeStep.INSTANCE, BsAtEndStep.INSTANCE},
                frag1, frag2, frag3, frag4, frag5,
                frag6, frag7, frag8, frag9, frag10);
        
        TestUtil.assertContainsSameFragmentsButSkipNulls(output, null, frag1);
        assertOutput(output, FUNC, B);
        assertArgCount(1, output.get(0));
        final Expression arg1 = arg(output.get(0), 0);
        TestUtil.assertContainsSameFragmentsButSkipNulls(arg1, null, frag4);
        assertOutput(arg1, FUNC, B);
        assertArgCount(1, arg1.get(0));
        final Expression arg2 = arg(arg1.get(0), 0);
        TestUtil.assertContainsSameFragments(arg2, frag8, frag7);
        assertOutput(arg2, A, B);
    }
    
    @Test
    @DisplayName("Multiple arguments")
    public void testMultipleArguments() throws ErrorMessage
    {
        // Input:  FUNC ( A , B )
        // Output: FUNC
        //            A, B
        
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(COMMA,  30);
        final Fragment frag5 = Fragment.of(B,      40);
        final Fragment frag6 = Fragment.of(RPAREN, 50);
        
        final Expression output = TestUtil.parse(
                NodeStep.INSTANCE, frag1, frag2, frag3, frag4, frag5, frag6);

        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, (Fragment)null);
        assertOutput(output, FUNC);
        assertArgCount(2, output.get(0));
        final Expression arg1 = arg(output.get(0), 0);
        TestUtil.assertContainsSameFragments(arg1, frag3);
        assertOutput(arg1, A);
        final Expression arg2 = arg(output.get(0), 1);
        TestUtil.assertContainsSameFragments(arg2, frag5);
        assertOutput(arg2, B);
    }
    
    @Test
    @DisplayName("Multiple empty arguments")
    public void testMultipleEmptyArguments() throws ErrorMessage
    {
        // Input:  FUNC ( , A , , )
        // Output: FUNC
        
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(COMMA,  20);
        final Fragment frag4 = Fragment.of(A,      30);
        final Fragment frag5 = Fragment.of(COMMA,  40);
        final Fragment frag6 = Fragment.of(COMMA,  50);
        final Fragment frag7 = Fragment.of(RPAREN, 60);
        
        final Expression output = TestUtil.parse(
                NodeStep.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7);

        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, (Fragment)null);
        assertOutput(output, FUNC);
        assertArgCount(1, output.get(0));
        final Expression arg1 = arg(output.get(0), 0);
        TestUtil.assertContainsSameFragments(arg1, frag4);
        assertOutput(arg1, A);
    }
    
    @Test
    @DisplayName("Missing parameter-list closer")
    public void testMissingParamListCloser() throws ErrorMessage
    {
        // Input:  FUNC ( FUNC ( B A )
        // Output: ErrorMessage with ) attached.
        
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(FUNC,   20);
        final Fragment frag4 = Fragment.of(LPAREN, 30);
        final Fragment frag5 = Fragment.of(B,      40);
        final Fragment frag6 = Fragment.of(A,      50);
        final Fragment frag7 = Fragment.of(RPAREN, 60);
        
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () ->
                {
                    TestUtil.parse(
                            new ParseStep[]
                            {
                                    NodeStep.INSTANCE,
                                    BsAtEndStep.INSTANCE,
                            },
                            frag1, frag2, frag3, frag4, frag5, frag6, frag7);
                },
                "Expected exception: input: FUNC ( FUNC ( B A )");
        assertSame(
                frag7, thrown.detail().get(), "Attached previous fragment");
    }
    
    @Test
    @DisplayName("Illegal input access")
    public void testIllegalInputAccess() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A, 0);
        assertThrows(
                NoSuchElementException.class,
                () -> TestUtil.parse(
                        new CallStep(2, TokenStream::input), frag1),
                "Expected exception: input called in end");
    }
    
    @Test
    @DisplayName("Illegal previous-input access")
    public void testIllegalPreviousInputAccess() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A, 0);
        assertThrows(
                NoSuchElementException.class,
                () -> TestUtil.parse(
                        new CallStep(1, TokenStream::previousInput), frag1),
                "Expected exception: previousInput called in first read");
    }
    
    @Test
    @DisplayName("Illegal start of argument")
    public void testIllegalStartOfArgument() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A, 0);
        assertThrows(
                IllegalStateException.class,
                () -> TestUtil.parse(
                        new CallStep(1, TokenStream::startArgument), frag1),
                "Expected exception: startArgument called on non-function");
    }
    
    @Test
    @DisplayName("Illegal end of argument")
    public void testIllegalEndOfArgument() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A, 0);
        assertThrows(
                IllegalStateException.class,
                () -> TestUtil.parse(
                        new CallStep(1, TokenStream::endArgument), frag1),
                "Expected exception: endArgument called before startArgument");
    }
    
    @Test
    @DisplayName("Illegal node access")
    public void testIllegalNodeAccess() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(A, 0);
        assertThrows(
                IllegalStateException.class,
                () -> TestUtil.parse(
                        new CallStep(1, TokenStream::node), frag1),
                "Expected exception: node called before startArgument");
    }
    
    private static void assertOutput(
            final Expression actual, final Token... expected)
    {
        assertEquals(expected.length, actual.size(), "Output length");
        for (int i = 0; i < expected.length; i++)
        {
            final Token expectedToken = expected[i];
            final Token actualToken = actual.get(i).token();
            if (expectedToken.type().isFunction())
            {
                assertNotSame(
                        expectedToken,
                        actualToken,
                        "Function at index " + i
                        + ": It should have been another instance.");
                assertEquals(
                        expectedToken.name(),
                        actualToken.name(),
                        "Function at index " + i + ": name");
            }
            else
            {
                assertSame(expectedToken, actualToken, "Token at index " + i);
            }
        }
    }
    
    private static void assertArgCount(
            final int expectedArgCount, final Fragment func)
    {
        assertEquals(
                expectedArgCount,
                ((Function)func.token()).arguments().size(),
                "Arg count");
    }
    
    private static Expression arg(final Fragment func, final int index)
    {
        return ((Function)func.token()).arguments().get(index);
    }
    
    /**
     * Asserts that the buffer and map are empty,
     * and that the state is the default state.
     */
    private static enum AssertInitialDataStep implements ParseStep
    {
        INSTANCE;

        @Override
        public void read(final TokenStream stream) throws ErrorMessage
        {
            assertTrue(stream.buffer().isEmpty(), "buffer.isEmpty()");
            assertTrue(stream.map().isEmpty(), "map.isEmpty()");
            assertEquals(
                    TokenStream.DEFAULT_STATE,
                    stream.state(),
                    "Initial state");
        }

        @Override
        public void end(final TokenStream stream) throws ErrorMessage
        {
        }
        
    }

    /**
     * Creates a function tree.
     */
    private static enum NodeStep implements ParseStep
    {
        INSTANCE;
        
        private static final int STATE_1 = TokenStream.DEFAULT_STATE + 1;
        private static final int STATE_2 = STATE_1 + 1;
        
        @Override
        public void read(final TokenStream stream) throws ErrorMessage
        {
            final Fragment input = stream.input();
            if (input.token().type().isFunction())
            {
                stream.startArgument();
                stream.setState(STATE_1);
            }
            else if (stream.state() == STATE_1)
            {
                if (input.token().type().isLeftParenthesis())
                {
                    stream.skip();
                }
                stream.setState(STATE_2);
            }
            else if (stream.state() == STATE_2
                    && (
                               input.token().type().isRightParenthesis()
                            || input.token() == COMMA)
                    )
            {
                stream.setState(TokenStream.DEFAULT_STATE);
                stream.end();
                stream.endArgument();
                if (input.token() == COMMA)
                {
                    stream.startArgument();
                    stream.setState(STATE_2);
                }
            }
        }
        
        @Override
        public void end(final TokenStream stream) throws ErrorMessage
        {
            final int state = stream.state();
            if (state != TokenStream.DEFAULT_STATE)
            {
                throw errorMessage("end: state == " + state)
                    .attach(stream.previousInput());
            }
        }
    }
    
    /**
     * Skips Bs and writes them at the end.
     */
    private static enum BsAtEndStep implements ParseStep
    {
        INSTANCE;
        
        @Override
        public void read(final TokenStream stream) throws ErrorMessage
        {
            final Fragment input = stream.input();
            if (input.token().identifiesAs(B))
            {
                stream.buffer().add(input);
                stream.skip();
            }
        }
        
        @Override
        public void end(final TokenStream stream) throws ErrorMessage
        {
            final List<Fragment> buffer = stream.buffer();
            while (!buffer.isEmpty())
            {
                stream.write(buffer.remove(0));
            }
        }
    }
    
    /**
     * Calls a method on the stream inside read(stream) or end(stream).
     * When this happens is determined by onCount.
     * Each call to read and each call to end increases the count.
     * On the first call to read/end, the count is one.
     */
    private static final class CallStep implements ParseStep
    {
        private final int onCount;
        private final Consumer<? super TokenStream> call;
        private int count;

        private CallStep(
                final int onCount, final Consumer<? super TokenStream> call)
        {
            assert onCount >= 1;
            assert call != null;
            this.onCount = onCount;
            this.call = call;
        }

        @Override
        public void read(final TokenStream stream) throws ErrorMessage
        {
            if (++count == onCount)
            {
                call.accept(stream);
            }
        }

        @Override
        public void end(final TokenStream stream) throws ErrorMessage
        {
            if (++count == onCount)
            {
                call.accept(stream);
            }
        }
        
    }

}
