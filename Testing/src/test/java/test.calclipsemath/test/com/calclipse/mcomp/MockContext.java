package test.com.calclipse.mcomp;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.SymbolTable;
import com.calclipse.math.parser.type.TypeContext;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

import test.com.calclipse.math.parser.MockSymbolTable;
import test.com.calclipse.math.parser.type.MockTypeContext;

/**
 * A mock math-component-context.
 * This class uses
 * a {@link test.com.calclipse.math.parser.MockSymbolTable},
 * a {@link test.com.calclipse.math.parser.type.MockTypeContext}
 * and a {@link MockParser}.
 * 
 * @author Tone Sommerland
 */
public final class MockContext implements McContext
{
    /**
     * A mock parser.
     * @author Tone Sommthing
     */
    @FunctionalInterface
    public static interface MockParser
    {
        public abstract Expression parse(String expression)
                throws ErrorMessage;
    }

    private final SymbolTable symbolTable = new MockSymbolTable();
    private final Collection<Token> xports = new ArrayList<>();
    private final Map<String, Token> imports = new HashMap<>();
    private final MockContext parent;
    private final Collection<McLocator> locators;
    private final MockParser parser;

    private MockContext(
            final MockContext parent,
            final Collection<McLocator> locators,
            final MockParser parser)
    {
        assert locators != null;
        assert parser != null;
        this.parent = parent;
        this.locators = locators;
        this.parser = parser;
    }

    public MockContext(final MockParser parser)
    {
        this(null, new ArrayList<>(), requireNonNull(parser, "parser"));
    }
    
    /**
     * Returns an instance with a parser
     * that produces expressions resulting in undef.
     */
    public static McContext getOne()
    {
        return new MockContext(input
                -> new Expression.Builder((expression, interceptReturn)
                        -> Values.UNDEF).build());
    }

    @Override
    public Optional<McContext> parent()
    {
        return Optional.ofNullable(parent);
    }

    @Override
    public Expression parse(final String expression) throws ErrorMessage
    {
        return parser.parse(expression);
    }

    @Override
    public boolean addSymbol(final Token symbol)
    {
        return symbolTable.add(symbol);
    }

    @Override
    public boolean removeSymbol(final String name)
    {
        return symbolTable.remove(name);
    }

    @Override
    public Optional<Token> symbol(final String name)
    {
        return Optional.ofNullable(symbolTable.get(name));
    }

    @Override
    public Collection<Token> symbols()
    {
        return symbolTable.all();
    }

    @Override
    public Collection<Token> lookUpSymbols(final char firstChar)
    {
        return symbolTable.lookUp(firstChar);
    }

    @Override
    public Optional<Mcomp> mcompAt(
            final String location, final String referrer) throws ErrorMessage
    {
        for (final McLocator locator : locators)
        {
            if (locator.accepts(location, referrer))
            {
                final Mcomp mcomp = locator.find(location, referrer);
                if (mcomp != null)
                {
                    return Optional.of(mcomp);
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public void exportSymbol(final Token symbol) throws ErrorMessage
    {
        xports.add(symbol);
    }

    @Override
    public void importMcomp(final Mcomp mcomp) throws ErrorMessage
    {
        final var context = new MockContext(this, locators, parser);
        mcomp.execute(context);
        context.xports.stream()
            .map(token -> token.withName(mcomp.name()
                    + namespaceSeparator() + token.name()))
            .forEach(renamedToken ->
            {
                symbolTable.remove(renamedToken.name());
                symbolTable.add(renamedToken);
                imports.put(renamedToken.name(), renamedToken);
            });
    }

    @Override
    public Collection<Token> importedSymbols()
    {
        return List.copyOf(imports.values());
    }

    @Override
    public void addLocator(final McLocator locator)
    {
        locators.add(locator);
    }

    @Override
    public McLocator[] clearLocators()
    {
        final McLocator[] removed = locators.stream().toArray(McLocator[]::new);
        locators.clear();
        return removed;
    }

    @Override
    public TypeContext typeContext()
    {
        return MockTypeContext.INSTANCE;
    }

}
