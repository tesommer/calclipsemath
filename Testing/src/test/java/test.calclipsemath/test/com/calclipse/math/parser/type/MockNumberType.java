package test.com.calclipse.math.parser.type;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.parser.type.NumberType;

/**
 * This mock number-type uses {@code java.lang.Double}.
 * 
 * @author Tone Sommerland
 */
public enum MockNumberType implements NumberType
{
    INSTANCE;

    @Override
    public Number parse(final String literal) throws ErrorMessage
    {
        try
        {
            return Double.valueOf(literal);
        }
        catch (final NumberFormatException ex)
        {
            throw errorMessage("Bad literal").withCause(ex);
        }
    }

    @Override
    public Number valueOf(final double d)
    {
        return Double.valueOf(d);
    }

    @Override
    public Number valueOf(final long l)
    {
        return Double.valueOf(l);
    }

}
