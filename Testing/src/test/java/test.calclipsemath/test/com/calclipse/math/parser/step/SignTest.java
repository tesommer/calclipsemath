package test.com.calclipse.math.parser.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.step.Sign;

import test.TestUtil;

@DisplayName("Sign")
public final class SignTest
{
    private static final Token
    PLUS = BinaryOperator.of("+", 0, (left, right) -> Values.UNDEF);
    
    private static final Token
    MINUS = BinaryOperator.of("-", 0, (left, right) -> Values.UNDEF);
    
    private static final Token
    NEGATIVE = UnaryOperator.prefixed("~", 0, arg -> Values.UNDEF);
    
    private static final Token LPAREN = Tokens.leftParenthesis("(");
    private static final Token RPAREN = Tokens.rightParenthesis(")");
    private static final Token A = Operand.of("A", Values.UNDEF);
    private static final Token B = Operand.of("B", Values.UNDEF);
    
    private static final ParseStep
    STEP = new Sign(() -> NEGATIVE, PLUS::identifiesAs, MINUS::identifiesAs);

    public SignTest()
    {
    }
    
    @Test
    @DisplayName("Unary and binary minus")
    public void testUnaryAndBinaryMinus() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(MINUS,  0);
        final Fragment frag2 = Fragment.of(A,     10);
        final Fragment frag3 = Fragment.of(MINUS, 20);
        final Fragment frag4 = Fragment.of(B,     30);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3, frag4);
        TestUtil.assertContainsSameTokensButSkipNulls(
                output, null, A, MINUS, B);
        assertReplacement(frag1, output.get(0));
    }
    
    @Test
    @DisplayName("Unary and binary plus")
    public void testUnaryAndBinaryPlus() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(PLUS,  0);
        final Fragment frag2 = Fragment.of(A,    10);
        final Fragment frag3 = Fragment.of(PLUS, 20);
        final Fragment frag4 = Fragment.of(B,    30);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3, frag4);
        TestUtil.assertContainsSameTokens(output, A, PLUS, B);
    }
    
    @Test
    @DisplayName("Double and single unary and binary plus and minus")
    public void testDoubleAndSingleUnaryAndBinaryPlusAndMinus()
            throws ErrorMessage
    {
        // + - - + + A + - B - + A + + B - - A
        // => ~ ~ A + ~ B - A + B - ~ A (~ = negative)
        
        final Fragment frag1 = Fragment.of(PLUS,     0);
        final Fragment frag2 = Fragment.of(MINUS,   10);
        final Fragment frag3 = Fragment.of(MINUS,   20);
        final Fragment frag4 = Fragment.of(PLUS,    30);
        final Fragment frag5 = Fragment.of(PLUS,    40);
        final Fragment frag6 = Fragment.of(A,       50);
        final Fragment frag7 = Fragment.of(PLUS,    60);
        final Fragment frag8 = Fragment.of(MINUS,   70);
        final Fragment frag9 = Fragment.of(B,       80);
        final Fragment frag10 = Fragment.of(MINUS,  90);
        final Fragment frag11 = Fragment.of(PLUS,  100);
        final Fragment frag12 = Fragment.of(A,     110);
        final Fragment frag13 = Fragment.of(PLUS,  120);
        final Fragment frag14 = Fragment.of(PLUS,  130);
        final Fragment frag15 = Fragment.of(B,     140);
        final Fragment frag16 = Fragment.of(MINUS, 150);
        final Fragment frag17 = Fragment.of(MINUS, 160);
        final Fragment frag18 = Fragment.of(A,     170);
        
        final Expression output = TestUtil.parse(
                STEP,
                frag1, frag2, frag3, frag4, frag5, frag6,
                frag7, frag8, frag9, frag10, frag11, frag12,
                frag13, frag14, frag15, frag16, frag17, frag18);
        
        TestUtil.assertContainsSameTokensButSkipNulls(
                output,
                null,
                null,
                A,
                PLUS,
                null,
                B,
                MINUS,
                A,
                PLUS,
                B,
                MINUS,
                null,
                A);
        assertReplacement(frag2, output.get(0));
        assertReplacement(frag3, output.get(1));
        assertReplacement(frag8, output.get(4));
        assertReplacement(frag17, output.get(11));
    }
    
    /*
     * Added 2008-02-13
     */
    
    @Test
    @DisplayName("Left parenthesis followed by minus")
    public void testLParenFollowedByMinus() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(LPAREN,  0);
        final Fragment frag2 = Fragment.of(MINUS,  10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(MINUS,  30);
        final Fragment frag5 = Fragment.of(B,      40);
        final Fragment frag6 = Fragment.of(RPAREN, 50);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3, frag4, frag5, frag6);
        TestUtil.assertContainsSameTokensButSkipNulls(
                output, LPAREN, null, A, MINUS, B, RPAREN);
        assertReplacement(frag2, output.get(1));
    }
    
    @Test
    @DisplayName("Left parenthesis followed by plus")
    public void testLParenFollowedByPlus() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(LPAREN,  0);
        final Fragment frag2 = Fragment.of(PLUS,   10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(MINUS,  30);
        final Fragment frag5 = Fragment.of(B,      40);
        final Fragment frag6 = Fragment.of(RPAREN, 50);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3, frag4, frag5, frag6);
        TestUtil.assertContainsSameTokens(
                output, LPAREN, A, MINUS, B, RPAREN);
    }
    
    private static void assertReplacement(
            final Fragment original, final Fragment replacement)
    {
        assertEquals(
                original.position(),
                replacement.position(),
                "Replacement position");
        assertEquals(
                original.token().name(),
                replacement.token().name(),
                "Replacement name");
        assertTrue(
                replacement.token().identifiesAs(NEGATIVE),
                "Assert replacement is negation");
    }

}
