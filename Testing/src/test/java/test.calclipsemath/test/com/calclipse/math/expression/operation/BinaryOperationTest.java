package test.com.calclipse.math.expression.operation;

import static com.calclipse.math.expression.Errors.errorMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;

@DisplayName("BinaryOperation")
public final class BinaryOperationTest
{
    private static final Value LEFT_DEFAULT = valueOf(21);
    private static final Value LEFT_OVERRIDE = valueOf(3);
    private static final Value RIGHT = valueOf(4);
    private static final Value RESULT_DEFAULT = valueOf(1);
    private static final Value RESULT_OVERRIDE = valueOf(2);
    
    private static final BinaryOperation
    OPERATION = (left, right) -> RESULT_DEFAULT;
    
    private static final BinaryOperation
    OVERRIDE = (left, right) ->
    {
        if (left == LEFT_OVERRIDE)
        {
            return RESULT_OVERRIDE;
        }
        return Values.YIELD;
    };

    public BinaryOperationTest()
    {
    }
    
    @Test
    @DisplayName("asVariableArity")
    public void testAsVariableArity() throws ErrorMessage
    {
        final ErrorMessage wrongNumberOfArguments = errorMessage(
                "Sorry, did I break your nasal bones?");
        final BinaryOperation binary = (left, right) -> valueOf(7);
        final VariableArityOperation variableArity = binary.asVariableArity(
                () -> wrongNumberOfArguments);
        assertEquals(7, variableArity.evaluate(valueOf(11), valueOf(13)).get());
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> variableArity.evaluate(valueOf(11)),
                "Wrong number of arguments");
        assertSame(wrongNumberOfArguments, thrown, "thrown");
    }
    
    @Test
    @DisplayName("override")
    public void testOverride() throws ErrorMessage
    {
        assertOverridden(OVERRIDE.override(OPERATION));
    }
    
    @Test
    @DisplayName("acceptOverride")
    public void testAcceptOverride() throws ErrorMessage
    {
        assertOverridden(OPERATION.acceptOverride(OVERRIDE));
    }
    
    private static void assertOverridden(final BinaryOperation overridden)
            throws ErrorMessage
    {
        assertSame(
                RESULT_DEFAULT,
                overridden.evaluate(LEFT_DEFAULT, RIGHT),
                "default");
        assertSame(
                RESULT_OVERRIDE,
                overridden.evaluate(LEFT_OVERRIDE, RIGHT),
                "override");
    }
    
    private static Value valueOf(final Object value)
    {
        return Value.constantOf(value);
    }

}
