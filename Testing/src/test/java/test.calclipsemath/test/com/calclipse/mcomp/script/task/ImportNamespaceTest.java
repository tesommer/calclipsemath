package test.com.calclipse.mcomp.script.task;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Tokens;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.task.ImportNamespace;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("ImportNamespace")
public final class ImportNamespaceTest
{
    private static final Trace
    TRACE = new Trace.Builder().setScriptName("Amazing trace").build();
    
    private static final String NAMESPACE = "diagnosis";
    private static final String SYMBOL = "shrimp-in-hair syndrome";

    public ImportNamespaceTest()
    {
    }
    
    @Test
    @DisplayName("execute")
    public void testExecute() throws ErrorMessage
    {
        final McContext context = MockContext.getOne();
        context.importMcomp(MockMcomp.INSTANCE);
        final Task task = taskToTest(NAMESPACE);
        task.execute(context);
        assertTrue(
                context.symbol(SYMBOL).isPresent(),
                "Presence of symbol without namespace prefix");
        assertThrows(
                ErrorMessage.class,
                () -> task.execute(context),
                "Importing namespace twice should cause name conflict");
    }
    
    private static Task taskToTest(final String namespace)
    {
        return new ImportNamespace(TRACE, namespace, Place.START);
    }
    
    private static final class MockMcomp implements Mcomp
    {
        private static final Mcomp INSTANCE = new MockMcomp();

        private MockMcomp()
        {
        }

        @Override
        public void execute(final McContext context) throws ErrorMessage
        {
            context.exportSymbol(Tokens.plain(SYMBOL));
        }

        @Override
        public String name()
        {
            return NAMESPACE;
        }
    }

}
