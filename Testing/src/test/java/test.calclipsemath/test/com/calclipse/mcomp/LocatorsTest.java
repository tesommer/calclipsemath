package test.com.calclipse.mcomp;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.Locators;
import com.calclipse.mcomp.McLocator;
import com.calclipse.mcomp.Mcomp;

@DisplayName("Locators")
public final class LocatorsTest
{
    public LocatorsTest()
    {
    }
    
    @Test
    @DisplayName("Arg location")
    public void testArgLocation() throws ErrorMessage
    {
        final var context = MockContext.getOne();
        final String mockLocation = "mock";
        context.addLocator(new MockLocator(mockLocation));
        final var args = new String[] {"arg1", "arg2"};
        Locators.addArgLocator("argv", context, args);
        Locators.addArgLocator("zero", context);
        assertArrayEquals(
                new String[0],
                Locators.argsAt("argt", context),
                "argt");
        assertArrayEquals(
                new String[0],
                Locators.argsAt(mockLocation, context),
                "mock");
        assertArrayEquals(
                args,
                Locators.argsAt("argv", context),
                "argv");
        assertArrayEquals(
                new String[0],
                Locators.argsAt("zero", context),
                "zero");
    }
    
    private static final class MockLocator implements McLocator
    {
        private final String myLocation;

        private MockLocator(final String myLocation)
        {
            assert myLocation != null;
            this.myLocation = myLocation;
        }

        @Override
        public boolean accepts(final String location, final String referrer)
        {
            return location.equals(myLocation);
        }

        @Override
        public Mcomp find(final String location, final String referrer)
                throws ErrorMessage
        {
            return context -> {};
        }
        
    }

}
