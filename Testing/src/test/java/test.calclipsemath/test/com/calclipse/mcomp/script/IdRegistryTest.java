package test.com.calclipse.mcomp.script;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.function.BiPredicate;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.trace.Place;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("IdRegistry")
public final class IdRegistryTest
{
    private static final BiPredicate<String, McContext>
    VALID = (identifier, context) -> true;
    
    private static final String NAME1 = "id1";
    private static final String NAME2 = "id2";
    private static final Identifier ID1 = new Identifier(NAME1, Place.START);
    private static final Identifier ID2 = new Identifier(NAME2, Place.START);
    
    private final IdRegistry registry = new IdRegistry();
    private McContext context1;
    private McContext context2;
    
    public IdRegistryTest()
    {
    }
    
    @BeforeEach
    public void setUp()
    {
        context1 = MockContext.getOne();
        context2 = MockContext.getOne();
    }
    
    @AfterEach
    public void tearDown()
    {
        registry.clear();
    }
    
    @Test
    @DisplayName("Registration and unregistration")
    public void testRegistrationAndUnregistration()
    {
        registry.unregister(NAME1, context1);
        assertFalse(registry.isRegistered(        NAME1, context1), "01");
        assertFalse(registry.isRegisteredVariable(NAME1, context1), "02");
        assertFalse(registry.isRegisteredFunction(NAME1, context1), "03");
        registry.registerVariable(NAME1, context1, VALID);
        registry.registerFunction(NAME2, new Identifier[0], context2, VALID);
        assertTrue( registry.isRegistered(        NAME1, context1), "04");
        assertTrue( registry.isRegisteredVariable(NAME1, context1), "05");
        assertFalse(registry.isRegisteredFunction(NAME1, context1), "06");
        assertFalse(registry.isRegistered(        NAME1, context2), "07");
        assertFalse(registry.isRegisteredVariable(NAME1, context2), "08");
        assertFalse(registry.isRegisteredFunction(NAME1, context2), "09");
        assertFalse(registry.isRegistered(        NAME2, context1), "10");
        assertFalse(registry.isRegisteredVariable(NAME2, context1), "11");
        assertFalse(registry.isRegisteredFunction(NAME2, context1), "12");
        assertTrue( registry.isRegistered(        NAME2, context2), "13");
        assertFalse(registry.isRegisteredVariable(NAME2, context2), "14");
        assertTrue( registry.isRegisteredFunction(NAME2, context2), "15");
        registry.unregister(NAME2, context2);
        assertTrue( registry.isRegisteredVariable(NAME1, context1), "16");
        assertFalse(registry.isRegistered(        NAME2, context2), "17");
        assertFalse(registry.isRegisteredFunction(NAME1, context2), "18");
        registry.unregister(NAME2, context2);
        assertTrue( registry.isRegisteredVariable(NAME1, context1), "19");
        registry.registerFunction(NAME1, new Identifier[0], context1, VALID);
        assertFalse(registry.isRegisteredVariable(NAME1, context1), "20");
        assertTrue( registry.isRegisteredFunction(NAME1, context1), "21");
    }
    
    @Test
    @DisplayName("Validate variable")
    public void testValidateVariable() throws ErrorMessage
    {
        registry.validate(context1);
        registry.registerVariable(NAME1, context1, VALID.negate());
        registry.validate(context2);
        assertThrows(
                ErrorMessage.class,
                () -> registry.validate(context1));
    }
    
    @Test
    @DisplayName("Validate function")
    public void testValidateFunction() throws ErrorMessage
    {
        registry.validate(context1);
        registry.registerFunction(
                NAME1, new Identifier[0], context1, VALID.negate());
        registry.validate(context2);
        assertThrows(
                ErrorMessage.class,
                () -> registry.validate(context1));
    }
    
    @Test
    @DisplayName("Validate function variable")
    public void testValidateFunctionVariable() throws ErrorMessage
    {
        registry.validate(context1);
        registry.registerVariable(NAME1, context1, VALID.negate());
        registry.registerFunction(
                NAME2, new Identifier[] {ID1}, context1, VALID);
        assertThrows(
                ErrorMessage.class,
                () -> registry.validate(context1),
                "1");
        registry.unregister(NAME1, context1);
        registry.validate(context1);
        registry.registerFunction(
                NAME1, new Identifier[0], context1, VALID.negate());
        assertThrows(
                ErrorMessage.class,
                () -> registry.validate(context1),
                "2");
    }
    
    @Test
    @DisplayName("Validate functions with circular variables")
    public void testValidateFunctionsWithCircularVariables()
            throws ErrorMessage
    {
        final var variables1 = new Identifier[] {ID1};
        final var variables2 = new Identifier[] {ID2};
        registry.registerFunction(NAME2, variables1, context1, VALID);
        registry.registerFunction(NAME1, variables2, context1, VALID);
        registry.validate(context1);
        registry.registerFunction(NAME2, variables1, context1, VALID.negate());
        assertThrows(
                ErrorMessage.class,
                () -> registry.validate(context1));
    }
    
    @Test
    @DisplayName("functionVariables")
    public void testFunctionVariables()
    {
        final var variables = new Identifier[] {ID1};
        registry.registerFunction(NAME2, variables, context1, VALID);
        assertArrayEquals(
                variables,
                registry.functionVariables(NAME2, context1),
                "1");
        assertThrows(
                IllegalArgumentException.class,
                () -> registry.functionVariables(NAME1, context1),
                "2");
        registry.registerVariable(NAME1, context1, VALID);
        assertThrows(
                IllegalArgumentException.class,
                () -> registry.functionVariables(NAME1, context1),
                "3");
    }

}
