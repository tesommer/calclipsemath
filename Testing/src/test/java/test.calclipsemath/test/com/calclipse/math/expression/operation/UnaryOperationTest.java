package test.com.calclipse.math.expression.operation;

import static com.calclipse.math.expression.Errors.errorMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;

@DisplayName("UnaryOperation")
public final class UnaryOperationTest
{
    private static final Value ARG_DEFAULT = valueOf(21);
    private static final Value ARG_OVERRIDE = valueOf(3);
    private static final Value RESULT_DEFAULT = valueOf(1);
    private static final Value RESULT_OVERRIDE = valueOf(2);
    
    private static final UnaryOperation OPERATION = arg -> RESULT_DEFAULT;
    
    private static final UnaryOperation OVERRIDE = arg ->
    {
        if (arg == ARG_OVERRIDE)
        {
            return RESULT_OVERRIDE;
        }
        return Values.YIELD;
    };

    public UnaryOperationTest()
    {
    }
    
    @Test
    @DisplayName("asVariableArity")
    public void testAsVariableArity() throws ErrorMessage
    {
        final ErrorMessage wrongNumberOfArguments = errorMessage(
                "Sorry, did I step on your moment?");
        final UnaryOperation unary = arg -> valueOf(7);
        final VariableArityOperation variableArity = unary.asVariableArity(
                () -> wrongNumberOfArguments);
        assertEquals(7, variableArity.evaluate(valueOf(11)).get());
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> variableArity.evaluate(valueOf(11), valueOf(13)),
                "Wrong number of arguments");
        assertSame(wrongNumberOfArguments, thrown, "thrown");
    }
    
    @Test
    @DisplayName("override")
    public void testOverride() throws ErrorMessage
    {
        assertOverridden(OVERRIDE.override(OPERATION));
    }
    
    @Test
    @DisplayName("acceptOverride")
    public void testAcceptOverride() throws ErrorMessage
    {
        assertOverridden(OPERATION.acceptOverride(OVERRIDE));
    }
    
    private static void assertOverridden(final UnaryOperation overridden)
            throws ErrorMessage
    {
        assertSame(
                RESULT_DEFAULT, overridden.evaluate(ARG_DEFAULT), "default");
        assertSame(
                RESULT_OVERRIDE, overridden.evaluate(ARG_OVERRIDE), "override");
    }
    
    private static Value valueOf(final Object value)
    {
        return Value.constantOf(value);
    }

}
