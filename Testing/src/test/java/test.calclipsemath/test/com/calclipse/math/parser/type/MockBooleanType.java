package test.com.calclipse.math.parser.type;

import static com.calclipse.math.expression.Errors.errorMessage;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.parser.type.BooleanType;

/**
 * Uses {@code java.lang.Double} to represent Booleans.
 * 
 * @author Tone Sommerland
 */
public enum MockBooleanType implements BooleanType
{
    INSTANCE;

    @Override
    public Object valueOf(final boolean b)
    {
        return Double.valueOf(b ? 1 : 0);
    }

    @Override
    public boolean isTrue(final Object boolValue) throws ErrorMessage
    {
        if (boolValue instanceof Number)
        {
            return ((Number)boolValue).doubleValue() != 0;
        }
        throw errorMessage("Not a number");
    }

}
