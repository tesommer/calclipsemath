package test.com.calclipse.math.expression.operation;

import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Evaluator;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.Value;

@DisplayName("ConditionalOperation")
public final class ConditionalOperationTest
{
    private static final Evaluator
    EVALUATOR = (expression, interceptReturn) -> Values.UNDEF;
    
    private static final Expression
    ARG_DEFAULT = new Expression.Builder(EVALUATOR).build();
    
    private static final Expression
    ARG_OVERRIDE = new Expression.Builder(EVALUATOR).build();
    
    private static final Value
    RESULT_DEFAULT = valueOf(1);
    
    private static final Value
    RESULT_OVERRIDE = valueOf(2);
    
    private static final ConditionalOperation
    OPERATION = args -> RESULT_DEFAULT;
    
    private static final ConditionalOperation
    OVERRIDE = args ->
    {
        if (args.get(0) == ARG_OVERRIDE)
        {
            return RESULT_OVERRIDE;
        }
        return Values.YIELD;
    };

    public ConditionalOperationTest()
    {
    }
    
    @Test
    @DisplayName("override")
    public void testOverride() throws ErrorMessage
    {
        assertOverridden(OVERRIDE.override(OPERATION));
    }
    
    @Test
    @DisplayName("acceptOverride")
    public void testAcceptOverride() throws ErrorMessage
    {
        assertOverridden(OPERATION.acceptOverride(OVERRIDE));
    }
    
    private static void assertOverridden(
            final ConditionalOperation overridden) throws ErrorMessage
    {
        assertSame(
                RESULT_DEFAULT,
                overridden.evaluate(List.of(ARG_DEFAULT)),
                "default");
        assertSame(
                RESULT_OVERRIDE,
                overridden.evaluate(List.of(ARG_OVERRIDE)),
                "override");
    }
    
    private static Value valueOf(final Object value)
    {
        return Value.constantOf(value);
    }

}
