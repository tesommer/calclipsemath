package test.com.calclipse.mcomp.script;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.trace.Trace;

/**
 * A mock task that counts executions.
 * 
 * @author Tone Sommerland
 */
public final class MockTask extends Task
{
    private final boolean oneTimeCompilationStep;
    private int executionCount = 0;

    public MockTask(
            final Trace scriptTrace, final boolean oneTimeCompilationStep)
    {
        super(scriptTrace);
        this.oneTimeCompilationStep = oneTimeCompilationStep;
    }

    /**
     * The number of times this task has been executed.
     */
    public int executionCount()
    {
        return executionCount;
    }
    
    /**
     * Sets the execution count to zero.
     */
    public void resetExecutionCount()
    {
        executionCount = 0;
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        executionCount++;
    }

    @Override
    public boolean isOneTimeCompilationStep()
    {
        return oneTimeCompilationStep;
    }

}
