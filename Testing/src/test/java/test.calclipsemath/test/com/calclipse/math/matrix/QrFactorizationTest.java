package test.com.calclipse.math.matrix;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.QrFactorization;

import test.TestUtil;

@DisplayName("QrFactorization")
public final class QrFactorizationTest
{
    private static final double DELTA = 1.0E-13;

    public QrFactorizationTest()
    {
    }
    
    @Test
    @DisplayName("factorize")
    public void testFactorize()
    {
        final Matrix mx1 = MockMatrix.valueOf(
                2, 2,
                1, 2,
                3, 4);
        final Matrix mx2 = MockMatrix.valueOf(
                2, 2,
                0, 0,
                0, 0);
        final Matrix mx3 = MockMatrix.valueOf(
                2, 2,
                1, 0,
                0, 1);
        final Matrix mx4 = MockMatrix.valueOf(
                2, 3,
                0, 0, 0,
                1, 1, 0);
        final Matrix mx5 = MockMatrix.valueOf(
                2, 3,
                1, 2, 3,
                4, 5, 6);
        final Matrix mx6 = MockMatrix.valueOf(
                3, 2,
                1, 2,
                3, 4,
                5, 6);
        final Matrix mx7 = MockMatrix.valueOf(
                3, 3,
                  0, 21,  23,
                 42, 22, -66,
                -72, 82,  91);
        final Matrix mx8 = MockMatrix.valueOf(
                5, 5,
                1, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 1, 1, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 1, 0);
        assertQr(mx1, true, true, true);
        assertQr(mx2, true, true, false);
        assertQr(mx3, true, true, false);
        assertQr(mx4, false, true, true);
        assertQr(mx5, false, true, true);
        assertQr(mx6, false, true, true);
        assertQr(mx7, true, true, true);
        assertQr(mx8, true, false, true);
    }
    
    @Test
    @DisplayName("reflector")
    public void testReflector()
    {
        final Matrix mx = MockMatrix.valueOf(
                4, 4,
                11, 21, 32, 33,
                42, 19, 66, 76,
                72, 87, 92, 17);
        final Matrix u = QrFactorization.reflector(mx, 0, 0);
        final Matrix h = householder(u);
        final Matrix result = h.times(mx);
        assertEquals(0, result.get(1, 0), DELTA, "result.get(1, 0)");
        assertEquals(0, result.get(2, 0), DELTA, "result.get(2, 0)");
        assertEquals(0, result.get(3, 0), DELTA, "result.get(3, 0)");
    }
    
    @Test
    @DisplayName("Non-incremental pivot rows")
    public void testNonIncrementalPivotRows()
    {
        final Matrix mx = MockMatrix.valueOf(
                5, 5,
                1, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 1, 1, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 1, 0);
        assertArrayEquals(
                new int[] {1, 3},
                QrFactorization.factorize(mx).pivotRows(),
                "Pivot rows after QR factorization of\n" + mx);
    }
    
    private static void assertQr(
            final Matrix mx,
            final boolean assertRIsUpperTriangular,
            final boolean assertRIsEchelon,
            final boolean assertQIsNotIdentity)
    {
        final QrFactorization qrFac = QrFactorization.factorize(mx);
        final Matrix r = qrFac.r();
        final Matrix q = qrFac.computeQ();
        final Matrix qr = q.times(r);
        TestUtil.assertEquals(
                mx,
                qr,
                DELTA,
                "A == QR\nA:\n" + mx + "\nQR:\n" + qr);
        assertTrue(
                TestUtil.isOrthogonal(q, DELTA),
                "isOrthogonal(Q):\n" + q);
        if (assertRIsUpperTriangular)
        {
            assertTrue(
                    TestUtil.isUpperTriangular(r, DELTA),
                    "isUpperTriangular(R):\n" + r);
        }
        if (assertRIsEchelon)
        {
            assertTrue(
                    TestUtil.isEchelon(r, DELTA),
                    "isEchelon(R):\n" + r);
        }
        if (assertQIsNotIdentity)
        {
            assertNotEquals(
                    mx.identity(q.rows()),
                    q,
                    "Assert Q != identity");
        }
    }
    
    private static Matrix householder(final Matrix u)
    {
        return u.identity(u.rows()).minus(u.times(u.transposed()).times(2));
    }

}
