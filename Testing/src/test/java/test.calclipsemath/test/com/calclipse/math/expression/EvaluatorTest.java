package test.com.calclipse.math.expression;

import static com.calclipse.math.expression.Errors.errorMessage;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BadExpression;
import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Evaluator;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Return;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.BinaryOperation;
import com.calclipse.math.expression.operation.ConditionalOperation;
import com.calclipse.math.expression.operation.UnaryOperation;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.expression.operation.VariableArityOperation;

@DisplayName("Evaluator")
public final class EvaluatorTest
{
    private static final Token LPAREN = Tokens.leftParenthesis("((");
    private static final Token RPAREN = Tokens.rightParenthesis("))");
    private static final Token PLAIN = Tokens.plain(",,");
    private static final Token UNDEFINED = Tokens.undefined("��");
    
    private static final Delimitation
    DELIM = Delimitation.byTokensWithOpener(
            LPAREN, RPAREN, PLAIN, Token::identifiesAs);
    
    private static final Token
    BINARY_MINUS = BinaryOperator.of(
            "-", 1, EvaluatorTest::binaryMinus);
    
    private static final Token
    LEFT_UNARY_TIMES_TWO = UnaryOperator.prefixed(
            "2x", 1, EvaluatorTest::leftUnaryTimesTwo);
    
    private static final Token
    RIGHT_UNARY_TIMES_THREE = UnaryOperator.postfixed(
            "x3", 1, EvaluatorTest::rightUnaryTimesThree);
    
    private static final Function
    FUNCTION_SUM = Function.of(
            "sum", DELIM, EvaluatorTest::functionSum);
    
    private static final Function
    CONDITIONAL_PRODUCT = Function.ofConditional(
            "product", DELIM, EvaluatorTest::conditionalProduct);
    
    private static final Fragment
    PRE_ATTACH = Fragment.of(operand(99), 999);
    
    private static final String ERROR_MESSAGE = "Greetings, earthlings!";
    
    private static final Token
    UNARY_RETURN = UnaryOperator.prefixed(
            "return", 1, EvaluatorTest::unaryReturn);

    public EvaluatorTest()
    {
    }
    
    @DisplayName("Evaluator.forPostfix")
    public static final class ForPostfixTest
    {
        private static final Evaluator INSTANCE = Evaluator.forPostfix();

        public ForPostfixTest()
        {
        }
        
        @Test
        @DisplayName("Empty expression")
        public void testEmptyExpression() throws ErrorMessage
        {
            assertSame(
                    Values.UNDEF,
                    INSTANCE.evaluate(
                            new Expression.Builder(INSTANCE).build(), false),
                    "Evaluate: empty expression");
        }
        
        @Test
        @DisplayName("Operand")
        public void testOperand() throws ErrorMessage
        {
            final Token operand = operand(22);
            final Fragment fragment = Fragment.of(operand, 2);
            final Expression expression = expression(fragment);
            assertSame(
                    operand,
                    INSTANCE.evaluate(expression, false),
                    "Evaluate: operand");
        }
        
        @Test
        @DisplayName("Binary operator")
        public void testBinaryOperator() throws ErrorMessage
        {
            final Value result = evaluate(
                    false,
                    operand(68),
                    operand(72),
                    BINARY_MINUS);
            assertEquals(
                    -4,
                    result.get(),
                    "Evaluate: binary operator");
        }
        
        @Test
        @DisplayName("Left unary operator")
        public void testLeftUnaryOperator() throws ErrorMessage
        {
            final Value result = evaluate(
                    false,
                    operand(2),
                    LEFT_UNARY_TIMES_TWO);
            assertEquals(
                    4,
                    result.get(),
                    "Evaluate: left unary operator");
        }
        
        @Test
        @DisplayName("Right unary operator")
        public void testRightUnaryOperator() throws ErrorMessage
        {
            final Value result = evaluate(
                    false,
                    operand(9),
                    RIGHT_UNARY_TIMES_THREE);
            assertEquals(
                    27,
                    result.get(),
                    "Evaluate: right unary operator");
        }
        
        @Test
        @DisplayName("Function")
        public void testFunction() throws ErrorMessage
        {
            final var args = List.of(
                    expression(operand(1)),
                    expression(operand(2)),
                    expression(operand(3)));
            final Value result = evaluate(
                    false,
                    FUNCTION_SUM.withArguments(args));
            assertEquals(
                    6,
                    result.get(),
                    "Evaluate: function");
        }
        
        @Test
        @DisplayName("Conditional function")
        public void testConditionalFunction() throws ErrorMessage
        {
            final var args = List.of(
                    expression(operand(10)),
                    expression(operand(20)),
                    expression(operand(30)));
            final Value result = evaluate(
                    false,
                    CONDITIONAL_PRODUCT.withArguments(args));
            assertEquals(
                    6000,
                    result.get(),
                    "Evaluate: conditional");
        }
        
        @Test
        @DisplayName("Exception from unary operator")
        public void testExceptionFromUnaryOperator()
        {
            final UnaryOperator err1 = UnaryOperator.prefixed(
                    "error1", 1, unaryErrorOperation(false));
            final UnaryOperator err2 = UnaryOperator.prefixed(
                    "error2", 1, unaryErrorOperation(true));
            final Fragment frag1 = Fragment.of(operand(0), 0);
            final Fragment frag2 = Fragment.of(err1, 10);
            final Fragment frag3 = Fragment.of(err2, 20);
            assertAttachedFragment(frag2, expression(frag1, frag2));
            assertAttachedFragment(frag3, expression(frag1, frag3));
        }
        
        @Test
        @DisplayName("Exception from binary operator")
        public void testExceptionFromBinaryOperator()
        {
            final BinaryOperator err1 = BinaryOperator.of(
                    "err1", 1, binaryErrorOperation(false));
            final BinaryOperator err2 = BinaryOperator.of(
                    "err2", 1, binaryErrorOperation(true));
            final Fragment frag1 = Fragment.of(operand(0), 0);
            final Fragment frag2 = Fragment.of(operand(1), 10);
            final Fragment frag3 = Fragment.of(err1, 20);
            final Fragment frag4 = Fragment.of(err2, 30);
            assertAttachedFragment(frag3, expression(frag1, frag2, frag3));
            assertAttachedFragment(frag4, expression(frag1, frag2, frag4));
        }
        
        @Test
        @DisplayName("Exception from function")
        public void testExceptionFromFunction()
        {
            final Function err1 = Function.of(
                    "err1", DELIM, variableArityErrorOperation(false));
            final Function err2 = Function.of(
                    "err2", DELIM, variableArityErrorOperation(true));
            final Fragment frag1 = Fragment.of(err1, 0);
            final Fragment frag2 = Fragment.of(err2, 10);
            assertAttachedFragment(frag1, expression(frag1));
            assertAttachedFragment(frag2, expression(frag2));
        }
        
        @Test
        @DisplayName("Exception from function argument")
        public void testExceptionFromFunctionArgument()
        {
            final Token err1 = UnaryOperator.prefixed(
                    "err1", 1, unaryErrorOperation(false));
            final Token err2 = UnaryOperator.prefixed(
                    "err2", 1, unaryErrorOperation(true));
            final Fragment frag1 = Fragment.of(operand(1), 0);
            final Fragment frag2 = Fragment.of(err1, 10);
            final Fragment frag3 = Fragment.of(err2, 20);
            assertAttachedFragment(
                    frag2,
                    expression(
                            Fragment.of(FUNCTION_SUM.withArguments(List.of(
                                    expression(frag1, frag2))), 30)));
            assertAttachedFragment(
                    frag3,
                    expression(
                            Fragment.of(FUNCTION_SUM.withArguments(List.of(
                                    expression(frag1, frag3))), 40)));
        }
        
        @Test
        @DisplayName("Exception from conditional function")
        public void testExceptionFromConditionalFunction()
        {
            final Function err1 = Function.ofConditional(
                    "err1", DELIM, conditionalErrorOperation(false));
            final Function err2 = Function.ofConditional(
                    "err2", DELIM, conditionalErrorOperation(true));
            final Fragment frag1 = Fragment.of(err1, 0);
            final Fragment frag2 = Fragment.of(err2, 10);
            assertAttachedFragment(frag1, expression(frag1));
            assertAttachedFragment(PRE_ATTACH, expression(frag2));
        }
        
        @Test
        @DisplayName("Non-intercepted return")
        public void testNonInterceptedReturnException()
        {
            final var returnValue = new Object();
            final Operand arg = operand(returnValue);
            final Return thrown = assertThrows(
                    Return.class,
                    () -> evaluate(false, arg, UNARY_RETURN),
                    "No return thrown.");
            assertNotSame(
                    arg,
                    thrown.value(),
                    "Value of non-intercepted return");
            assertSame(
                    returnValue,
                    thrown.value().get(),
                    "Value-object of non-intercepted return");
        }
        
        @Test
        @DisplayName("Intercepted return")
        public void testInterceptedReturnException() throws ErrorMessage
        {
            final var returnValue = new Object();
            final Operand arg = operand(returnValue);
            final Value result = evaluate(true, arg, UNARY_RETURN);
            assertNotSame(
                    arg,
                    result,
                    "Result of intercepted return");
            assertSame(
                    returnValue,
                    result.get(),
                    "Value-object of intercepted return");
        }
        
        @Test
        @DisplayName("Bad expression")
        public void testBadExpression()
        {
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, LPAREN),
                    "LPAREN");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, RPAREN),
                    "RPAREN");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, PLAIN),
                    "PLAIN");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, UNDEFINED),
                    "UNDEFINED");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, operand(2), operand(3)),
                    "OPERAND OPERAND");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, FUNCTION_SUM, FUNCTION_SUM),
                    "FUNCTION FUNCTION");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, operand(2), BINARY_MINUS),
                    "OPERAND BOPERATOR");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, BINARY_MINUS),
                    "BOPERATOR");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, LEFT_UNARY_TIMES_TWO),
                    "LUOPERATOR");
            assertThrows(
                    BadExpression.class,
                    () -> evaluate(true, RIGHT_UNARY_TIMES_THREE),
                    "RUOPERATOR");
        }
        
        private static Value evaluate(
                final boolean interceptReturn,
                final Token... tokens) throws ErrorMessage
        {
            return INSTANCE.evaluate(expression(tokens), interceptReturn);
        }
        
        private static Expression expression(final Fragment... frags)
        {
            final var express = new Expression.Builder(INSTANCE);
            Stream.of(frags).forEach(express::add);
            return express.build();
        }
        
        private static Expression expression(final Token... tokens)
        {
            int position = 0;
            final var expression = new Expression.Builder(INSTANCE);
            for (final Token token : tokens)
            {
                expression.add(Fragment.of(token, position));
                position += 10;
            }
            return expression.build();
        }
        
        private void assertAttachedFragment(
                final Fragment expected, final Expression expression)
        {
            final ErrorMessage thrown = assertThrows(
                    ErrorMessage.class,
                    () -> INSTANCE.evaluate(expression, false),
                    "No error message was thrown from evaluate");
            assertTrue(
                    thrown.detail().isPresent(), "Presence of attachemnt");
            assertSame(expected, thrown.detail().get(), "attachment");
        }
        
    }
    
    private static Operand operand(final Object value)
    {
        return Operand.of(value.toString(), Value.constantOf(value));
    }
    
    private static Value binaryMinus(final Value left, final Value right)
    {
        final var int1 = (Integer)left.get();
        final var int2 = (Integer)right.get();
        return operand(int1 - int2);
    }
    
    private static Value leftUnaryTimesTwo(final Value arg)
    {
        final var int1 = (Integer)arg.get();
        return operand(int1 * 2);
    }
    
    private static Value rightUnaryTimesThree(final Value arg)
    {
        final var int1 = (Integer)arg.get();
        return operand(int1 * 3);
    }
    
    private static Value functionSum(final Value... args)
    {
        int sum = 0;
        for (final Value arg : args)
        {
            sum += (Integer)arg.get();
        }
        return operand(sum);
    }
    
    private static Value conditionalProduct(final List<Expression> args)
            throws ErrorMessage
    {
        int product = 1;
        for (final Expression arg : args)
        {
            product *= (Integer)arg.evaluate(false).get();
        }
        return operand(product);
    }
    
    private static UnaryOperation unaryErrorOperation(
            final boolean preAttach)
    {
        return arg ->
        {
            if (preAttach)
            {
                throw errorMessage(ERROR_MESSAGE).attach(PRE_ATTACH);
            }
            throw errorMessage(ERROR_MESSAGE);
        };
    }
    
    private static BinaryOperation binaryErrorOperation(
            final boolean preAttach)
    {
        return (left, right) ->
        {
            if (preAttach)
            {
                throw errorMessage(ERROR_MESSAGE).attach(PRE_ATTACH);
            }
            throw errorMessage(ERROR_MESSAGE);
        };
    }
    
    private static VariableArityOperation variableArityErrorOperation(
            final boolean preAttach)
    {
        return args ->
        {
            if (preAttach)
            {
                throw errorMessage(ERROR_MESSAGE).attach(PRE_ATTACH);
            }
            throw errorMessage(ERROR_MESSAGE);
        };
    }
    
    private static ConditionalOperation conditionalErrorOperation(
            final boolean preAttach)
    {
        return args ->
        {
            if (preAttach)
            {
                throw errorMessage(ERROR_MESSAGE).attach(PRE_ATTACH);
            }
            throw errorMessage(ERROR_MESSAGE);
        };
    }
    
    private static Value unaryReturn(final Value arg)
    {
        throw Return.of(arg.get());
    }

}
