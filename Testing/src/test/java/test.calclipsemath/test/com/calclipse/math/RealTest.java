package test.com.calclipse.math;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.Fraction;
import com.calclipse.math.Real;

import test.TestUtil;

@DisplayName("Real")
public final class RealTest
{
    private static final MathContext
    MC_1024 = new MathContext(1024, RoundingMode.HALF_UP);
    
    private static final MathContext
    MC_2048 = new MathContext(2048, RoundingMode.HALF_UP);
    
    private static final BigDecimal
    BD_1 = new BigDecimal("1");
    
    private static final BigDecimal
    BD_1P234 = new BigDecimal("1.234");
    
    private static final BigDecimal
    BD_ONE_THIRD
        = new BigDecimal("1").divide(new BigDecimal("3"), MC_1024);

    public RealTest()
    {
    }
    
    @Test
    @DisplayName("That fraction is reduced")
    public void testThatFractionIsReduced()
    {
        final Real real = real(fraction("600", "-300"), -1);
        TestUtil.assertReal(fraction("-2", "1"), MC_1024, -1, real);
    }
    
    @Test
    @DisplayName("That zeros are stripped")
    public void testThatZerosAreStripped()
    {
        final var unstripped = new BigDecimal(
                new BigInteger("4000"), 2, MC_1024);
        final var stripped = new BigDecimal(
                new BigInteger("40"), MC_1024);
        final Real real = real(unstripped, -1);
        TestUtil.assertReal(stripped, MC_1024, -1, real);
    }
    
    @Test
    @DisplayName("isFracturable")
    public void testIsFracturable()
    {
        assertTrue(
                real(fraction("1", "2"), -2).isFracturable(),
                "1/2, fracturability: -2");
        assertFalse(
                real(BD_1P234, -2).isFracturable(),
                "1.234, fracturability: -2");
        assertFalse(
                real(BD_1, -2).isFracturable(),
                "1, fracturability: -2");
        assertTrue(
                real(BD_ONE_THIRD, -1).isFracturable(),
                "1.333..., fracturability: -1");
        assertFalse(
                real(BD_1P234, 0).isFracturable(),
                "1.234, fracturability: 0");
        assertFalse(
                real(BD_1P234, 1).isFracturable(),
                "1.234, fracturability: 1");
        assertFalse(
                real(BD_1P234, 2).isFracturable(),
                "1.234, fracturability: 2");
        assertTrue(
                real(BD_1P234, 3).isFracturable(),
                "1.234, fracturability: 3");
        assertTrue(
                real(BD_1P234, 4).isFracturable(),
                "1.234, fracturability: 4");
    }
    
    @Test
    @DisplayName("Fracture fraction")
    public void testFractureFraction()
    {
        final Real real = real(fraction("1", "3"), 56);
        final Real fractured = real.fracture();
        assertSame(real, fractured, "Fractured fraction");
    }
    
    @Test
    @DisplayName("Fracture big decimal with infinite decimals")
    public void testFractureBigDecWithInfiniteDecimals()
    {
        final int mvPntRight = 2;
        final int fracturability = 9999;
        final BigDecimal bd = BD_ONE_THIRD.movePointRight(mvPntRight);
        final Real real = real(bd, fracturability);
        final Real fractured = real.fracture();
        final var num = new StringBuilder(MC_1024.getPrecision());
        for (int i = 0; i < MC_1024.getPrecision(); i++)
        {
            num.append('3');
        }
        final int denDigitCount = MC_1024.getPrecision() - mvPntRight + 1;
        final var den = new StringBuilder(denDigitCount);
        for (int i = 0; i < denDigitCount; i++)
        {
            den.append('0');
        }
        den.setCharAt(0, '1');
        final Fraction fraction = fraction(num.toString(), den.toString());
        TestUtil.assertReal(fraction, MC_1024, fracturability, fractured);
    }
    
    @Test
    @DisplayName("Fracture big decimal with negative scale")
    public void testFractureBigDecWithNegativeScale()
    {
        final int fracturability = -1;
        final var bd = new BigDecimal(new BigInteger("2"), -3);
        final Real real = real(bd, fracturability);
        final Real fractured = real.fracture();
        final Fraction fraction = fraction("2000", "1");
        TestUtil.assertReal(fraction, MC_1024, fracturability, fractured);
    }
    
    @Test
    @DisplayName("Fracture big decimal with zero scale")
    public void testFractureBigDecWithZeroScale()
    {
        final int fracturability = -1;
        final var bd = new BigDecimal(new BigInteger("21"));
        final Real real = real(bd, fracturability);
        final Real fractured = real.fracture();
        final Fraction fraction = fraction("21", "1");
        TestUtil.assertReal(fraction, MC_1024, fracturability, fractured);
    }
    
    @Test
    @DisplayName("Fracture big decimal with trailing zeros")
    public void testFractureBigDecWithTrailingZeros()
    {
        final int fracturability = -1;
        final var bd = new BigDecimal(new BigInteger("7000"), 1);
        final Real real = real(bd, fracturability);
        final Real fractured = real.fracture();
        final Fraction fraction = fraction("700", "1");
        TestUtil.assertReal(fraction, MC_1024, fracturability, fractured);
    }
    
    @Test
    @DisplayName("Unfracture big decimal")
    public void testUnfractureBigDec()
    {
        final int fracturability = -2;
        final Real real = real(BD_1P234, fracturability);
        final Real unfractured = real.unfracture();
        assertSame(real, unfractured, "Unfractured big decimal");
    }
    
    @Test
    @DisplayName("Unfracture fraction")
    public void testUnfractureFraction()
    {
        final int fracturability = -2;
        final Real real = real(fraction("1", "2"), fracturability);
        final Real unfractured = real.unfracture();
        final var bigDec = new BigDecimal("0.5");
        TestUtil.assertReal(bigDec, MC_1024, fracturability, unfractured);
    }
    
    @Test
    @DisplayName("Unfracture fraction with infinite decimals")
    public void testUnfractureFractionWithInfiniteDecimals()
    {
        final int fracturability = -2;
        final Real real = real(fraction("1", "3"), fracturability);
        final Real unfractured = real.unfracture();
        final BigDecimal bigDec
            = new BigDecimal(1).divide(new BigDecimal(3), MC_1024);
        TestUtil.assertReal(bigDec, MC_1024, fracturability, unfractured);
    }
    
    @Test
    @DisplayName("+")
    public void testPlus()
    {
        final BinOp binOp = (left, right) -> left.plus(right);
        assertBinOp(
                new BigDecimal("2.234"),
                real(BD_1, -1),
                real(BD_1P234, -1),
                binOp);
        assertBinOp(
                fraction("513", "4"),
                real(fraction("1000", "8"), -1),
                real(fraction("26", "8"), -1),
                binOp);
    }
    
    @Test
    @DisplayName("-")
    public void testMinus()
    {
        final BinOp binOp = (left, right) -> left.minus(right);
        assertBinOp(
                new BigDecimal("-0.234"),
                real(BD_1, -1),
                real(BD_1P234, -1),
                binOp);
        assertBinOp(
                fraction("333", "2"),
                real(fraction("1001", "2"), -1),
                real(fraction("1002", "3"), -1),
                binOp);
    }
    
    @Test
    @DisplayName("*")
    public void testTimes()
    {
        final BinOp binOp = (left, right) -> left.times(right);
        assertBinOp(
                new BigDecimal("1.522756"),
                real(BD_1P234, -1),
                real(BD_1P234, -1),
                binOp);
        assertBinOp(
                fraction("167167", "1"),
                real(fraction("1001", "2"), -1),
                real(fraction("1002", "3"), -1),
                binOp);
    }
    
    @Test
    @DisplayName("/")
    public void testDividedBy()
    {
        final BinOp binOp = (left, right) -> left.dividedBy(right);
        assertBinOp(
                new BigDecimal("500").divide(new BigDecimal("617"), MC_1024),
                real(BD_1, -1),
                real(BD_1P234, -1),
                binOp);
        assertBinOp(
                fraction("1001", "668"),
                real(fraction("1001", "2"), -1),
                real(fraction("1002", "3"), -1),
                binOp);
    }
    
    @Test
    @DisplayName("Divide fractured by zero")
    public void testDivideFracturedByZero()
    {
        assertThrows(
                ArithmeticException.class,
                () -> real(fraction("1", "2"), -1).dividedBy(
                        real(fraction("0", "2"), -1)),
                "(1/2) / (0/2)");
    }
    
    @Test
    @DisplayName("Divide unfractured by zero")
    public void testDivideUnfracturedByZero()
    {
        assertThrows(
                ArithmeticException.class,
                () -> real(BD_1, -1).dividedBy(real(BigDecimal.ZERO, -1)),
                "1.0 / 0.0");
    }
    
    @Test
    @DisplayName("toThePowerOf")
    public void testToThePowerOf()
    {
        TestUtil.assertReal(
                fraction("1", "16"),
                MC_1024,
                -1,
                real(fraction("1", "4"), -1).toThePowerOf(2));
        TestUtil.assertReal(
                fraction("16", "1"),
                MC_1024,
                -1,
                real(fraction("1", "4"), -1).toThePowerOf(-2));
        TestUtil.assertReal(
                fraction("1", "1"),
                MC_1024,
                -1,
                real(fraction("1", "4"), -1).toThePowerOf(0));
        TestUtil.assertReal(
                fraction("1", "1"),
                MC_1024,
                -1,
                real(fraction("0", "4"), -1).toThePowerOf(0));
        TestUtil.assertReal(
                new BigDecimal("1.522756"),
                MC_1024,
                -1,
                real(BD_1P234, -1).toThePowerOf(2));
        TestUtil.assertReal(
                new BigDecimal("1").divide(BD_1P234, MC_1024),
                MC_1024,
                -1,
                real(BD_1P234, -1).toThePowerOf(-1));
        TestUtil.assertReal(
                BD_1,
                MC_1024,
                -1,
                real(BD_1P234, -1).toThePowerOf(0));
        TestUtil.assertReal(
                BD_1,
                MC_1024,
                -1,
                real(BigDecimal.ZERO, -1).toThePowerOf(0));
    }
    
    @Test
    @DisplayName("toThePowerOf: divide fractured by zero")
    public void testToThePowerOf_DivideFracturedByZero()
    {
        assertThrows(
                ArithmeticException.class,
                () -> real(fraction("0", "5"), -1).toThePowerOf(-2),
                "(0/5)^-2");
    }
    
    @Test
    @DisplayName("toThePowerOf: divide unfractured by zero")
    public void testToThePowerOf_DivideUnfracturedByZero()
    {
        assertThrows(
                ArithmeticException.class,
                () -> real(BD_ONE_THIRD.subtract(BD_ONE_THIRD, MC_2048), -1)
                        .toThePowerOf(-1),
                "0.0^-1");
    }
    
    @Test
    @DisplayName("equals(")
    public void testEquals()
    {
        assertEquals(
                Real.fractured(fraction("1", "2"), MC_1024, -1),
                Real.fractured(fraction("1", "2"), MC_2048, 2),
                "1/2 = 1/2 unequal contexts and fracturabilities");
        assertEquals(
                real(fraction("1", "2"), -1),
                real(fraction("3", "6"), -1),
                "1/2 = 3/6");
        assertEquals(
                real(fraction("1", "2"), -1),
                real(new BigDecimal(".5"), -1),
                "1/2 = .5");
        assertNotEquals(
                real(fraction("1", "3"), -1),
                real(BD_ONE_THIRD, -1),
                "1/3 != .333...");
        assertEquals(
                real(new BigDecimal("100"), -1),
                real(new BigDecimal(new BigInteger("1000"), 1), -1),
                "100.0 = 100.0 unequal scales");
    }
    
    @Test
    @DisplayName("hashCode")
    public void testHashCode()
    {
        assertEquals(
                real(fraction("1", "2"), -1).hashCode(),
                real(fraction("3", "6"), -1).hashCode(),
                "hashCode: 1/2 = 3/6");
        assertEquals(
                real(fraction("1", "2"), -1).hashCode(),
                real(new BigDecimal(".5"), -1).hashCode(),
                "hashCode: 1/2 = .5");
        assertNotEquals(
                real(fraction("1", "3"), -1).hashCode(),
                real(BD_ONE_THIRD, -1).hashCode(),
                "hashCode: 1/3 != .333...");
        final var bd100 = new BigDecimal("100");
        final var bd100tz = new BigDecimal(new BigInteger("1000"), 1);
        assertEquals(
                real(bd100, -1).hashCode(),
                real(bd100tz, -1).hashCode(),
                "hashCode: 100.0 = 100.0 unequal scales");
    }
    
    @Test
    @DisplayName("compareTo")
    public void testCompareTo()
    {
        final Real r1 = real(
                fraction("1", "3"),                                   -2);
        final Real r2 = real(
                new BigDecimal(1).divide(new BigDecimal(3), MC_1024), -1);
        final Real r3 = real(
                new BigDecimal(1).divide(new BigDecimal(3), MC_2048),  0);
        final Real r4 = real(
                fraction("1", "2"),                                    1);
        final Real r5 = real(
                new BigDecimal(".5", MC_1024),                         2);
        final Real r6 = real(
                new BigDecimal(new BigInteger("50"), 2, MC_2048),      3);
        assertEquals(1, r1.compareTo(r2), "1/3 <=> .333...");
        assertEquals(1, r3.compareTo(r2), ".333333... <=> .333...");
        assertEquals(0, r6.compareTo(r4), ".50 <=> 1/2");
        assertEquals(0, r6.compareTo(r5), ".50 <=> .5");
    }
    
    @Test
    @DisplayName("To primitives")
    public void testToPrimitives()
    {
        final double delta = 1E-13;
        // doubleValue
        assertEquals(
                1.0 / 3,
                real(BD_ONE_THIRD, -1).doubleValue(),
                delta,
                "doubleValue: unfractured one third");
        assertEquals(
                1.0 / 3,
                real(fraction("1", "3"), -1).doubleValue(),
                delta,
                "doubleValue: fractured one third");
        // longValue
        assertEquals(
                Long.MAX_VALUE,
                real(new BigDecimal(Long.MAX_VALUE), -1).longValue(),
                "longValue: unfractured Long.MAX");
        assertEquals(
                3L,
                real(fraction("23", "7"), -1).longValue(),
                "longValue: fractured 23/7");
        // intValue
        assertEquals(
                32,
                real(new BigDecimal("32.1"), -1).intValue(),
                "intValue: unfractured 32.1");
        assertEquals(
                2,
                real(fraction("5", "2"), -1).intValue(),
                "intValue: fractured 5/2");
        // floatValue
        assertEquals(
                (float)(16.0 / 17),
                real(new BigDecimal("0.9411764705882353"), -1).floatValue(),
                (float)delta,
                "floatValue: unfractured 16.0 / 17");
        assertEquals(
                (float)(16.0 / 17),
                real(fraction("16", "17"), -1).floatValue(),
                (float)delta,
                "floatValue: fractured 16/17");
    }
    
    @Test
    @DisplayName("toString")
    public void testToString()
    {
        assertEquals(
                BD_ONE_THIRD.toString(),
                real(fraction("1", "3"), -2).toString(),
                "Fractured 1/3");
        assertEquals(
                "2",
                real(fraction("2", "1"), -2).toString(),
                "Fractured 2/1");
        assertEquals(
                "2",
                real(new BigDecimal(2.0), -2).toString(),
                "Unfractured 2");
        final Real unlimited = Real.fractured(
                fraction("1", "3"), MathContext.UNLIMITED, -2);
        assertEquals(
                "0.3333333333333333",
                unlimited.toString(),
                "Fractured 1/3 with zero precision context");
    }
    
    private static Fraction fraction(final String num, final String den)
    {
        return Fraction.valueOf(new BigInteger(num), new BigInteger(den));
    }
    
    private static Real real(
            final Fraction fraction, final int fracturability)
    {
        return Real.fractured(fraction, MC_1024, fracturability);
    }
    
    private static Real real(
            final BigDecimal bigDecimal, final int fracturability)
    {
        return Real.unfractured(bigDecimal, MC_1024, fracturability);
    }
    
    /**
     * Asserts a binary operation.
     * @param expectedResult either a fraction or a big decimal
     * @param left left operand
     * @param right right operand
     * @param binOp the binary operation
     */
    private static void assertBinOp(
            final Object expectedResult,
            final Real left,
            final Real right,
            final BinOp binOp)
    {
        final Real actualResult = binOp.eval(left, right);
        if (expectedResult instanceof Fraction)
        {
            TestUtil.assertReal(
                    (Fraction)expectedResult,
                    actualResult.mathContext(),
                    actualResult.fracturability(),
                    actualResult);
        }
        else
        {
            TestUtil.assertReal(
                    (BigDecimal)expectedResult,
                    actualResult.mathContext(),
                    actualResult.fracturability(),
                    actualResult);
        }
        assertBinOp_Frac_Frac(binOp);
        assertBinOp_BigDec_BigDec(binOp);
        assertBinOp_Frac_FracturableBD(binOp);
        assertBinOp_Frac_UnfracturableBD(binOp);
        assertBinOp_FracturableBD_Frac(binOp);
        assertBinOp_UnfracturableBD_Frac(binOp);
    }
    
    private static void assertBinOp_Frac_Frac(final BinOp binOp)
    {
        final Real left = Real.fractured(fraction("1", "2"), MC_1024, 11);
        final Real right = Real.fractured(fraction("60", "30"), MC_2048, 12);
        final Real result = binOp.eval(left, right);
        assertFractured(MC_2048, 11, result);
    }
    
    private static void assertBinOp_BigDec_BigDec(final BinOp binOp)
    {
        final Real left = Real.unfractured(BD_1P234, MC_1024, -2);
        final Real right = Real.unfractured(BD_ONE_THIRD, MC_2048, -3);
        final Real result = binOp.eval(left, right);
        assertUnfractured(MC_2048, -2, result);
    }
    
    private static void assertBinOp_Frac_FracturableBD(final BinOp binOp)
    {
        final Real left = Real.fractured(fraction("1", "2"), MC_2048, -32);
        final Real right = Real.unfractured(BD_ONE_THIRD, MC_1024, -1);
        final Real result = binOp.eval(left, right);
        assertFractured(MC_2048, -32, result);
    }
    
    private static void assertBinOp_Frac_UnfracturableBD(final BinOp binOp)
    {
        final Real left = Real.fractured(fraction("2", "1"), MC_1024, 23);
        final Real right = Real.unfractured(BD_ONE_THIRD, MC_1024, 2);
        final Real result = binOp.eval(left, right);
        assertUnfractured(MC_1024, 23, result);
    }
    
    private static void assertBinOp_FracturableBD_Frac(final BinOp binOp)
    {
        final Real left = Real.unfractured(BD_ONE_THIRD, MC_1024, -1);
        final Real right = Real.fractured(fraction("2", "2"), MC_2048, 22);
        final Real result = binOp.eval(left, right);
        assertFractured(MC_2048, -1, result);
    }
    
    private static void assertBinOp_UnfracturableBD_Frac(final BinOp binOp)
    {
        final Real left = Real.unfractured(BD_ONE_THIRD, MC_2048, -2);
        final Real right = Real.fractured(fraction("3", "2"), MC_2048, 23);
        final Real result = binOp.eval(left, right);
        assertUnfractured(MC_2048, -2, result);
    }
    
    private static void assertFractured(
            final MathContext expectedMathContext,
            final int expectedFracturability,
            final Real actual)
    {
        assertTrue(
                actual.isFractured(),
                "Assert is fractured");
        final Fraction fraction = actual.fraction();
        TestUtil.assertReal(
                fraction, expectedMathContext, expectedFracturability, actual);
    }
    
    private static void assertUnfractured(
            final MathContext expectedMathContext,
            final int expectedFracturability,
            final Real actual)
    {
        assertFalse(
                actual.isFractured(),
                "Assert is unfractured");
        final BigDecimal bigDec = actual.bigDecimal();
        TestUtil.assertReal(
                bigDec, expectedMathContext, expectedFracturability, actual);
    }
    
    /**
     * A binary operation on reals.
     */
    @FunctionalInterface
    private static interface BinOp
    {
        public abstract Real eval(Real left, Real right);
    }

}
