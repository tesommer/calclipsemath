package test.com.calclipse.math;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigInteger;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.Fraction;

import test.TestUtil;

@DisplayName("Fraction")
public final class FractionTest
{
    public FractionTest()
    {
    }
    
    @Test
    @DisplayName("signum")
    public void testSignum()
    {
        assertEquals(1,  frac("3", "4").signum(),   "3/4");
        assertEquals(-1, frac("-3", "4").signum(),  "-3/4");
        assertEquals(-1, frac("3", "-4").signum(),  "3/-4");
        assertEquals(1,  frac("-3", "-4").signum(), "-3/-4");
        assertEquals(0,  frac("0", "4").signum(),   "0/4");
        assertEquals(0,  frac("-0", "4").signum(),  "-0/4");
        assertEquals(0,  frac("0", "-4").signum(),  "0/-4");
        assertEquals(0,  frac("-0", "-4").signum(), "-0/-4");
    }
    
    @Test
    @DisplayName("reduced")
    public void testReduced()
    {
        assertEquals(frac("1", "4"),  frac("6", "24").reduced(),  "6/24");
        assertEquals(frac("2", "1"),  frac("32", "16").reduced(), "32/16");
        assertEquals(frac("0", "1"),  frac("0", "1").reduced(),   "0/1");
        assertEquals(frac("0", "1"),  frac("-0", "1").reduced(),  "-0/1");
        assertEquals(frac("-3", "4"), frac("3", "-4").reduced(),  "3/-4");
        assertEquals(frac("3", "4"),  frac("-6", "-8").reduced(), "-6/-8");
    }
    
    @Test
    @DisplayName("plus")
    public void testPlus()
    {
        assertEquals(
                frac("3", "4"),
                frac("16", "32").plus(frac("6", "24")),
                "16/32 + 6/24");
    }
    
    @Test
    @DisplayName("minus")
    public void testMinus()
    {
        assertEquals(
                frac("1", "4"),
                frac("16", "32").minus(frac("6", "24")),
                "16/32 - 6/24");
        assertEquals(
                frac("-1", "4"),
                frac("6", "24").minus(frac("16", "32")),
                "6/24 - 16/32");
    }
    
    @Test
    @DisplayName("times")
    public void testTimes()
    {
        assertEquals(
                frac("1", "8"),
                frac("16", "32").times(frac("6", "24")),
                "16/32 * 6/24");
    }
    
    @Test
    @DisplayName("dividedBy")
    public void testDividedBy()
    {
        assertEquals(
                frac("-2", "1"),
                frac("16", "32").dividedBy(frac("-6", "24")),
                "16/32 / -6/24");
    }
    
    @Test
    @DisplayName("dividedBy zero")
    public void testDivideByZero()
    {
        assertThrows(
                ArithmeticException.class,
                () -> frac("16", "32").dividedBy(frac("-0", "24")),
                "16/32 / -0/24");
    }
    
    @Test
    @DisplayName("toThePowerOf")
    public void testToThePowerOf()
    {
        assertEquals(
                frac("1", "4"),
                frac("11", "22").toThePowerOf(2),
                "11/22 ^2");
        assertEquals(
                frac("1", "1"),
                frac("22", "23").toThePowerOf(0),
                "22/23 ^0");
        assertEquals(
                frac("128", "1"),
                frac("13", "26").toThePowerOf(-7),
                "13/26 ^-7");
    }
    
    @Test
    @DisplayName("toThePowerOf: Negative exponent and zero numerator")
    public void testToThePowerOfWithNegativeExpAndZeroNum()
    {
        assertThrows(
                ArithmeticException.class,
                () -> frac("0", "2").toThePowerOf(-1),
                "0/2 ^-1");
    }
    
    @Test
    @DisplayName("equals")
    public void testEquals()
    {
        assertEquals(
                frac("16", "-32"),
                frac("-1", "2"),
                "16/-32 == -1/2");
        assertEquals(
                frac("16", "-32"),
                frac("1", "-2"),
                "16/-32 == 1/-2");
        assertEquals(
                frac("-1", "2"),
                frac("1", "-2"),
                "-1/2 == 1/-2");
        assertEquals(
                frac("-0", "2"),
                frac("0", "-2"),
                "-0/2 == 0/-2");
        assertEquals(
                frac("0", "-2"),
                frac("-0", "2"),
                "0/-2 == -0/2");
        assertNotEquals(
                frac("3", "-4"),
                frac("3", "4"),
                "3/-4 != 3/4");
        assertNotEquals(
                frac("-6", "24"),
                frac("6", "24"),
                "-6/24 != 6/24");
    }
    
    @Test
    @DisplayName("doubleValue")
    public void testDoubleValue()
    {
        final double delta = 0.000000000000001;
        assertEquals(
                -.5,
                frac("16", "-32").doubleValue(),
                delta,
                "16/-32");
        assertEquals(
                -(5.0 / 3.0),
                frac("-5", "3").doubleValue(),
                delta,
                "-5/3");
        assertEquals(
                -(5.0 / 3.0),
                frac("5", "-3").doubleValue(),
                delta,
                "5/-3");
        assertEquals(
                5.0 / 3.0,
                frac("5", "3").doubleValue(),
                delta,
                "5/3");
    }
    
    @Test
    @DisplayName("doubleValue: Large terms")
    public void testDoubleValueWithLargeTerms()
    {
        final String bigInt1 = "1000000000000";
        final String bigInt2 = "1000000";
        final double delta = 1.0E-76;
        assertEquals(
                1.0,
                frac(bigInt1, bigInt1).doubleValue(),
                delta,
                bigInt1 + "/" + bigInt1);
        assertEquals(
                1000000.0,
                frac(bigInt1, bigInt2).doubleValue(),
                delta,
                bigInt1 + "/" + bigInt2);
        assertEquals(
                1.0 / 1000000.0,
                frac(bigInt2, bigInt1).doubleValue(),
                delta,
                bigInt2 + "/" + bigInt1);
    }

    @Test
    @DisplayName("compareTo")
    public void testCompareTo()
    {
        final Fraction fm0_4 = frac("-0", "4");
        final Fraction fm1_2 = frac("-1", "2");
        final Fraction fm3_4 = frac("-3", "4");
        final Fraction fm21_m28 = frac("-21", "-28");
        final Fraction f0_2 = frac("0", "2");
        final Fraction f1_4 = frac("1", "4");
        final Fraction f1_2 = frac("1", "2");
        final Fraction f1_m2 = frac("1", "-2");
        final Fraction f3_4 = frac("3", "4");
        assertEquals(0,  fm0_4.compareTo(f0_2),    "-0/4 <=> 0/2");
        assertEquals(1,  fm0_4.compareTo(fm1_2),   "-0/4 <=> -1/2");
        assertEquals(-1, fm1_2.compareTo(f1_4),    "-1/2 <=> 1/4");
        assertEquals(-1, fm1_2.compareTo(fm0_4),   "-1/2 <=> -0/4");
        assertEquals(-1, fm3_4.compareTo(f1_m2),   "-3/4 <=> 1/-2");
        assertEquals(-1, fm3_4.compareTo(fm1_2),   "-3/4 <=> -1/2");
        assertEquals(1,  f3_4.compareTo(f1_2),     "3/4 <=> 1/2");
        assertEquals(0,  f3_4.compareTo(fm21_m28), "3/4 <=> -21/-28");
        assertEquals(1,  f1_m2.compareTo(fm3_4),   "1/-2 <=> -3/4");
        assertEquals(1,  f1_4.compareTo(fm1_2),    "1/4 <=> -1/2");
        assertEquals(0,  f0_2.compareTo(fm0_4),    "0/2 <=> -0/4");
        assertEquals(-1, f1_2.compareTo(f3_4),     "1/2 <=> 3/4");
        assertEquals(0,  fm21_m28.compareTo(f3_4), "-21/-28 <=> 3/4");
    }
    
    @Test
    @DisplayName("hashCode")
    public void testHashCode()
    {
        final Fraction fm0_4 = frac("-0", "4");
        final Fraction f0_2 = frac("0", "2");
        final Fraction fm1_2 = frac("-1", "2");
        final Fraction f1_m2 = frac("1", "-2");
        final Fraction f3_4 = frac("3", "4");
        final Fraction fm21_m28 = frac("-21", "-28");
        final Fraction fm3_4 = frac("-3", "4");
        final Fraction f1_4 = frac("1", "4");
        final Fraction f1_2 = frac("1", "2");
        assertHashCodeConsistency(
                6,
                fm0_4, f0_2, fm1_2, f1_m2, f3_4, fm21_m28, fm3_4, f1_4, f1_2);
        assertEquals(
                fm0_4.hashCode(),
                f0_2.hashCode(),
                "Assert equal hash codes: -0/4 and 0/2");
        assertEquals(
                fm1_2.hashCode(),
                f1_m2.hashCode(),
                "Assert equal hash codes: -1/2 and 1/-2");
        assertEquals(
                f3_4.hashCode(),
                fm21_m28.hashCode(),
                "Assert equal hash codes: 3/4 and -21/-28");
    }
    
    private static Fraction frac(final String num, final String den)
    {
        return Fraction.valueOf(new BigInteger(num), new BigInteger(den));
    }
    
    private static void assertHashCodeConsistency(
            final int uniqueHashCodesRequired, final Fraction... fractions)
    {
        TestUtil.assertHashCodeConsistency(
                fractions,
                uniqueHashCodesRequired,
                f ->
                {
                    final var duplicate = Fraction.valueOf(
                            f.numerator(),
                            f.denominator());
                    final var two = new BigInteger("2");
                    final var multipliedBy2_2 = Fraction.valueOf(
                            f.numerator().multiply(two),
                            f.denominator().multiply(two));
                    return new int[]
                    {
                            duplicate.hashCode(),
                            duplicate.reduced().hashCode(),
                            multipliedBy2_2.hashCode(),
                    };
                });
    }

}
