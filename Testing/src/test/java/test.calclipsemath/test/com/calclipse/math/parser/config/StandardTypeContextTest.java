package test.com.calclipse.math.parser.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.IntStream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.calclipse.math.Complex;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.math.matrix.MatrixStringifier;
import com.calclipse.math.parser.config.Configs;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.Association;
import com.calclipse.math.parser.type.AssociativeArray;
import com.calclipse.math.parser.type.AssociativeArrayType;
import com.calclipse.math.parser.type.BooleanType;
import com.calclipse.math.parser.type.Comparer;
import com.calclipse.math.parser.type.Indexer;
import com.calclipse.math.parser.type.NumberType;
import com.calclipse.math.parser.type.TypeContext;

import test.com.calclipse.math.matrix.MockMatrix;

@DisplayName("StandardTypeContext")
public final class StandardTypeContextTest
{
    private static final TypeContext
    TYPE_CONTEXT = Configs.standardTypeContext();

    public StandardTypeContextTest()
    {
    }
    
    /*****************
     * Comparer test *
     *****************/

    @Nested
    @DisplayName("StandardTypeContext.comparer()")
    public static final class ComparerTest
    {
        private static final Comparer COMPARER = TYPE_CONTEXT.comparer();

        public ComparerTest()
        {
        }
        
        @Test
        @DisplayName("Comparing a comparable object with itself")
        public void testComparingAComparableObjectWithItself()
                throws ErrorMessage
        {
            final Object itself = new A(21);
            assertCompareAndAreEqual(COMPARER, 0, true, itself, itself);
        }
        
        @Test
        @DisplayName("Comparing an uncomparable object with itself")
        public void testComparingAnUncomparableObjectWithItself()
        {
            final Object itself = new C();
            assertThrows(
                    ErrorMessage.class,
                    () -> COMPARER.compare(itself, itself));
        }
        
        @Test
        @DisplayName("Comparing comparable objects")
        public void testComparingComparableObjects() throws ErrorMessage
        {
            final Object a2_1 = new A(2);
            final Object a2_2 = new A(2);
            final Object a3 = new A(3);
            final Object d2p3_1 = 2.3;
            final Object d2p3_2 = 2.3;
            final Object d3p4 = 3.4;
            assertCompareAndAreEqual(COMPARER, 0,  true,  a2_1,   a2_2);
            assertCompareAndAreEqual(COMPARER, -1, false, a2_1,   a3);
            assertCompareAndAreEqual(COMPARER, 0,  true,  d2p3_1, d2p3_2);
            assertCompareAndAreEqual(COMPARER, 1,  false, d3p4,   d2p3_2);
        }
        
        @Test
        @DisplayName("Comparing incompatible objects")
        public void testComparingIncompatibleObjects()
        {
            final Object o1 = new A(1);
            final Object o2 = new B(2);
            assertThrows(ErrorMessage.class, () -> COMPARER.compare(o1, o2));
        }
        
        @Test
        @DisplayName("Comparing uncomparable objects")
        public void testComparingUncomparableObjects()
        {
            final Object o1 = new C();
            final Object o2 = new C();
            assertThrows(ErrorMessage.class, () -> COMPARER.compare(o1, o2));
        }
        
        @Test
        @DisplayName("Comparing numbers of different classes")
        public void testComparingNumbersOfDifferentClasses()
                throws ErrorMessage
        {
            final Object n1 = 2;
            final Object n2 = 2.0;
            final Object n3 = 3.0;
            assertCompareAndAreEqual(COMPARER, 0,  true,  n1, n2);
            assertCompareAndAreEqual(COMPARER, -1, false, n1, n3);
        }
        
        @Test
        @DisplayName("Comparing NaN")
        public void testComparingNaN() throws ErrorMessage
        {
            final Object nan1 = Double.valueOf(Double.NaN);
            final Object nan2 = Double.valueOf(Double.NaN);
            final Object nonNaN = 2.0;
            final Object inf = Double.valueOf(Double.POSITIVE_INFINITY);
            assertCompareAndAreEqual(COMPARER, 0, true,  nan1, nan2);
            assertCompareAndAreEqual(COMPARER, 1, false, nan1, nonNaN);
            assertCompareAndAreEqual(COMPARER, 1, false, nan1, inf);
        }
        
        @Test
        @DisplayName("Comparing infinity")
        public void testComparingInfinity() throws ErrorMessage
        {
            final Object inf1 = Double.valueOf(Double.POSITIVE_INFINITY);
            final Object inf2 = Double.valueOf(Double.POSITIVE_INFINITY);
            final Object finite = 2.0;
            final Object negInf1 = Double.valueOf(Double.NEGATIVE_INFINITY);
            final Object negInf2 = Double.valueOf(Double.NEGATIVE_INFINITY);
            assertCompareAndAreEqual(COMPARER, 0,  true,  inf1,    inf2);
            assertCompareAndAreEqual(COMPARER, 1,  false, inf1,    finite);
            assertCompareAndAreEqual(COMPARER, 0,  true,  negInf1, negInf2);
            assertCompareAndAreEqual(COMPARER, -1, false, negInf1, finite);
            assertCompareAndAreEqual(COMPARER, 1,  false, inf1,    negInf2);
        }
        
        private static void assertCompareAndAreEqual(
                final Comparer comparer,
                final int expectedCompareSignum,
                final boolean expectedAreEqual,
                final Object actual1,
                final Object actual2) throws ErrorMessage
        {
            final String compareMsg
                = "compare(" + actual1 + ", " + actual2 + ")";
            if (expectedCompareSignum < 0)
            {
                assertTrue(
                        comparer.compare(actual1, actual2) < 0,
                        compareMsg + ": assert < 0");
            }
            else if (expectedCompareSignum == 0)
            {
                assertTrue(
                        comparer.compare(actual1, actual2) == 0,
                        compareMsg + ": assert == 0");
            }
            else
            {
                assertTrue(
                        comparer.compare(actual1, actual2) > 0,
                        compareMsg + ": assert > 0");
            }
            final String areEqualMsg
                = "areEqual(" + actual1 + ", " + actual2 + ")";
            if (expectedAreEqual)
            {
                assertTrue(
                        comparer.areEqual(actual1, actual2),
                        areEqualMsg + ": assert true");
            }
            else
            {
                assertFalse(
                        comparer.areEqual(actual1, actual2),
                        areEqualMsg + ": assert false");
            }
        }
        
        private static final class A implements Comparable<A>
        {
            private final Integer i;

            private A(final int i)
            {
                this.i = i;
            }

            @Override
            public int compareTo(final A o)
            {
                return i.compareTo(o.i);
            }

            @Override
            public boolean equals(final Object obj)
            {
                if (obj instanceof A)
                {
                    return i.equals(((A)obj).i);
                }
                return false;
            }

            @Override
            public int hashCode()
            {
                return i.hashCode();
            }

            @Override
            public String toString()
            {
                return "A(" + i + ')';
            }
        }
        
        private static final class B implements Comparable<B>
        {
            private final Integer i;

            private B(final int i)
            {
                this.i = i;
            }

            @Override
            public int compareTo(final B o)
            {
                return i.compareTo(o.i);
            }

            @Override
            public boolean equals(final Object obj)
            {
                if (obj instanceof B)
                {
                    return i.equals(((B)obj).i);
                }
                return false;
            }

            @Override
            public int hashCode()
            {
                return i.hashCode();
            }

            @Override
            public String toString()
            {
                return "B(" + i + ')';
            }
        }
        
        private static final class C
        {
            private C()
            {
            }
        }
    }
    
    /*********************
     * Boolean-type test *
     *********************/

    @Nested
    @DisplayName("StandardTypeContext.booleanType()")
    public static final class BooleanTypeTest
    {
        private static final BooleanType
        BOOLEAN_TYPE = TYPE_CONTEXT.booleanType();

        public BooleanTypeTest()
        {
        }
        
        @Test
        @DisplayName("valueOf(boolean)")
        public void testValueOfBoolean()
        {
            final var falseValue = (Double)BOOLEAN_TYPE.valueOf(false);
            final var trueValue = (Double)BOOLEAN_TYPE.valueOf(true);
            assertEquals(Double.valueOf(0.0), falseValue, "False value");
            assertEquals(Double.valueOf(1.0), trueValue, "True value");
        }
        
        @Test
        @DisplayName("isTrue(Object)")
        public void testIsTrueObject() throws ErrorMessage
        {
            assertTrue(BOOLEAN_TYPE.isTrue(0.02), "isTrue(0.02)");
            assertTrue(BOOLEAN_TYPE.isTrue(1),    "isTrue(1)");
            assertFalse(BOOLEAN_TYPE.isTrue(0.0), "isTrue(0.0)");
            assertFalse(BOOLEAN_TYPE.isTrue(0),   "isTrue(0)");
            assertThrows(
                    ErrorMessage.class,
                    () -> BOOLEAN_TYPE.isTrue(Complex.I),
                    "isTrue(Complex)");
        }
    }
    
    /********************
     * Number-type test *
     ********************/

    @Nested
    @DisplayName("StandardTypeContext.numberType()")
    public static final class NumberTypeTest
    {
        private static final NumberType NUMBER_TYPE = TYPE_CONTEXT.numberType();

        public NumberTypeTest()
        {
        }
        
        @Test
        @DisplayName("Parse number literal")
        public void testParseNumberLiteral() throws ErrorMessage
        {
            final var d = (Double)NUMBER_TYPE.parse("1e-2");
            assertEquals(Double.valueOf(1e-2), d, "parse('1e-2')");
        }
        
        @Test
        @DisplayName("Parse invalid number literal")
        public void testParseInvalidNumberLiteral()
        {
            final String literal
                = "Thou shalt not bow down before any graven image!";
            assertThrows(ErrorMessage.class, () -> NUMBER_TYPE.parse(literal));
        }
        
        @Test
        @DisplayName("valueOf(double)")
        public void testValueOfDouble()
        {
            final var d = (Double)NUMBER_TYPE.valueOf(2.3);
            assertEquals(Double.valueOf(2.3), d, "valueOf(2.3)");
        }
        
        @Test
        @DisplayName("valueOf(long)")
        public void testValueOfLong()
        {
            final long exactAsDouble   = 0x4000000000000000L;
            final long inexactAsDouble = 0x4000000000000001L;
            assertEquals(
                    exactAsDouble,
                    NUMBER_TYPE.valueOf(exactAsDouble),
                    "exactAsDouble");
            assertEquals(
                    inexactAsDouble,
                    NUMBER_TYPE.valueOf(inexactAsDouble),
                    "inexactAsDouble");
        }
        
        @Test
        @DisplayName("valueOf(Number)")
        public void testValueOfNumber()
        {
            assertEquals(
                    Double.valueOf(1.2),
                    NUMBER_TYPE.valueOf(Double.valueOf(1.2)),
                    "Double: 1.2");
            assertEquals(
                    Integer.valueOf(3),
                    NUMBER_TYPE.valueOf(Integer.valueOf(3)),
                    "Integer: 3");
            final BigInteger big = BigInteger.valueOf(Integer.MAX_VALUE + 700);
            assertEquals(
                    big,
                    NUMBER_TYPE.valueOf(big),
                    "BigInteger: Integer.MAX_VALUE + 700");
        }
        
    }
    
    /****************
     * Indexer test *
     ****************/

    @Nested
    @DisplayName("StandardTypeContext.indexer()")
    public static final class IndexerTest
    {
        private static final Indexer INDEXER = TYPE_CONTEXT.indexer();

        public IndexerTest()
        {
        }
        
        /*
         * Matrix element
         */
        
        @Test
        @DisplayName("Matrix element: constant")
        public void testMatrixElementConstant() throws ErrorMessage
        {
            final Matrix indexable = MockMatrix.valueOf(
                    2, 3, 0, 0, 0, 0, 0, 5.4);
            final Value result = INDEXER.get(Value.constantOf(indexable), 2, 3);
            assertConstant(result, 5.4);
        }
        
        @Test
        @DisplayName("Matrix element: variable")
        public void testMatrixElementVariable() throws ErrorMessage
        {
            final Matrix indexable = MockMatrix.valueOf(
                    2, 3, 0, 0, 0, 0, 0, 5.4);
            final Value result = INDEXER.get(new Variable(indexable), 2, 3);
            assertValue(result, 5.4);
            result.set(7.2);
            final Matrix expectedModifiedIndexable = MockMatrix.valueOf(
                    2, 3, 0, 0, 0, 0, 0, 7.2);
            assertEquals(
                    expectedModifiedIndexable,
                    indexable,
                    "Indexable after assignment");
            assertValue(result, 7.2);
        }
        
        @Test
        @DisplayName("Matrix element: invalid index")
        public void testMatrixElementInvalidIndex()
        {
            final Matrix indexable = MockMatrix.valueOf(2, 3);
            final Value indexableValue = Value.constantOf(indexable);
            assertThrowsException(indexableValue, 0, 1);
            assertThrowsException(indexableValue, 1, 0);
            assertThrowsException(indexableValue, 1, 4);
            assertThrowsException(indexableValue, 3, 1);
        }
        
        /*
         * Matrix partition
         */
        
        @Test
        @DisplayName("Matrix partition: constant")
        public void testMatrixPartitionConstant() throws ErrorMessage
        {
            final Matrix indexable = MockMatrix.valueOf(4, 5);
            final Matrix partition = indexable.identity(3);
            indexable.setPartition(1, 2, partition);
            final Value result = INDEXER.get(
                    Value.constantOf(indexable), 2, 3, 5, 6);
            assertConstant(result, partition);
        }
        
        @Test
        @DisplayName("Matrix partition: variable")
        public void testMatrixPartitionVariable() throws ErrorMessage
        {
            final Matrix indexable = MockMatrix.valueOf(4, 5);
            final Matrix partition = indexable.identity(3);
            indexable.setPartition(1, 2, partition);
            final Value result = INDEXER.get(
                    new Variable(indexable), 2, 3, 5, 6);
            assertValue(result, partition);
            final Matrix newPartition = partition.times(2);
            result.set(newPartition);
            final Matrix expectedModifiedIndexable = MockMatrix.valueOf(
                    4, 5,
                    0, 0, 0, 0, 0,
                    0, 0, 2, 0, 0,
                    0, 0, 0, 2, 0,
                    0, 0, 0, 0, 2);
            assertEquals(
                    expectedModifiedIndexable,
                    indexable,
                    "Indexable after assignment");
            assertValue(result, newPartition);
        }
        
        @Test
        @DisplayName("Matrix partition: invalid index")
        public void testMatrixPartitionInvalidIndex()
        {
            final Matrix indexable = MockMatrix.valueOf(4, 5);
            final Value indexableValue = Value.constantOf(indexable);
            assertThrowsException(indexableValue, 0, 1, 2, 2);
            assertThrowsException(indexableValue, 1, 0, 2, 2);
            assertThrowsException(indexableValue, 1, 1, 6, 2);
            assertThrowsException(indexableValue, 1, 1, 2, 7);
            assertThrowsException(indexableValue, 2, 2, 1, 3);
            assertThrowsException(indexableValue, 2, 2, 3, 1);
        }
        
        @Test
        @DisplayName("Matrix partition: invalid size")
        public void testMatrixPartitionInvalidSize() throws ErrorMessage
        {
            final Matrix indexable = MockMatrix.valueOf(4, 5);
            final Value partitionValue = INDEXER.get(
                    new Variable(indexable), 2, 3, 5, 6);
            assertThrows(
                    ErrorMessage.class,
                    () -> partitionValue.set(indexable.identity(4)));
        }
        
        @Test
        @DisplayName("Matrix partition: special cases")
        public void testMatrixPartitionSpecialCases() throws ErrorMessage
        {
            final Matrix indexable = MockMatrix.valueOf(4, 5);
            final Value indexableValue = Value.constantOf(indexable);
            // The following indices should be allowed:
            assertConstant(INDEXER.get(
                    indexableValue, 1, 1, 5, 6), indexable);
            assertConstant(INDEXER.get(
                    indexableValue, 1, 1, 1, 1), MockMatrix.valueOf(0, 0));
            assertConstant(INDEXER.get(
                    indexableValue, 4, 5, 5, 6), MockMatrix.valueOf(1, 1));
        }
        
        /*
         * Array element
         */
        
        @Test
        @DisplayName("Array element: constant")
        public void testArrayElementConstant() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value result = INDEXER.get(Value.constantOf(indexable), 2);
            assertConstant(result, 2);
        }
        
        @Test
        @DisplayName("Array element: variable")
        public void testArrayElementVariable() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value result = INDEXER.get(new Variable(indexable), 2);
            assertValue(result, 2);
            result.set(3.76);
            final var expectedModifiedIndexable = arrayOf(1, 3.76, 3);
            assertEquals(
                    expectedModifiedIndexable,
                    indexable,
                    "Indexable after assignment");
            assertValue(result, 3.76);
        }
        
        @Test
        @DisplayName("Array element: invalid index")
        public void testArrayElementInvalidIndex()
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value indexableValue = Value.constantOf(indexable);
            assertThrowsException(indexableValue, 0);
            assertThrowsException(indexableValue, 4);
        }
        
        @Test
        @DisplayName("Array element: special cases")
        public void testArrayElementSpecialCases() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value indexableValue = Value.constantOf(indexable);
            assertConstant(INDEXER.get(indexableValue, 1), 1);
            assertConstant(INDEXER.get(indexableValue, 3), 3);
        }
        
        /*
         * Sub-array
         */
        
        @Test
        @DisplayName("sub-array: constant")
        public void testSubArrayConstant() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value result = INDEXER.get(Value.constantOf(indexable), 2, 4);
            assertConstant(result, arrayOf(2, 3));
        }
        
        @Test
        @DisplayName("Sub-array: variable")
        public void testSubArrayVariable() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value result = INDEXER.get(new Variable(indexable), 2, 4);
            assertValue(result, arrayOf(2, 3));
            final var newSubArray = arrayOf(1.2, 3.4);
            result.set(newSubArray);
            final var expectedModifiedIndexable = arrayOf(1, 1.2, 3.4);
            assertEquals(
                    expectedModifiedIndexable,
                    indexable,
                    "Indexable after assignment");
            assertValue(result, newSubArray);
        }
        
        @Test
        @DisplayName("Sub-array: invalid index")
        public void testSubArrayInvalidIndex()
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value indexableValue = Value.constantOf(indexable);
            assertThrowsException(indexableValue, 0, 3);
            assertThrowsException(indexableValue, 1, 5);
            assertThrowsException(indexableValue, 3, 2);
        }
        
        @Test
        @DisplayName("Sub-array: invalid size")
        public void testSubArrayInvalidSize() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value subArrayValue = INDEXER.get(
                    new Variable(indexable), 2, 4);
            assertThrows(
                    ErrorMessage.class,
                    () -> subArrayValue.set(arrayOf(1.2, 3.4, 5.6)));
        }
        
        @Test
        @DisplayName("Sub-array: special cases")
        public void testSubArraySpecialCases() throws ErrorMessage
        {
            final var indexable = arrayOf(1, 2, 3);
            final Value indexableValue = Value.constantOf(indexable);
            assertConstant(INDEXER.get(indexableValue, 1, 4), indexable);
            assertConstant(INDEXER.get(indexableValue, 3, 3), arrayOf());
            assertConstant(INDEXER.get(indexableValue, 3, 4), arrayOf(3));
        }
        
        /*
         * String char
         */
        
        @Test
        @DisplayName("String char")
        public void testStringChar() throws ErrorMessage
        {
            final Value indexableValue = Value.constantOf("abc");
            assertConstant(INDEXER.get(indexableValue, 2), "b");
            assertThrowsException(indexableValue, 0);
            assertThrowsException(indexableValue, 4);
        }
        
        /*
         * Substring
         */
        
        @Test
        @DisplayName("Substring")
        public void testSubstring() throws ErrorMessage
        {
            final Value indexableValue = Value.constantOf("abc");
            assertConstant(INDEXER.get(indexableValue, 2, 4), "bc");
            assertThrowsException(indexableValue, 0, 2);
            assertThrowsException(indexableValue, 1, 5);
            assertThrowsException(indexableValue, 2, 1);
        }
        
        /*
         * Misc.
         */
        
        @Test
        @DisplayName("Wrong number of indices")
        public void testWrongNumberOfIndices()
        {
            final Value matrixValue = Value.constantOf(
                    MockMatrix.valueOf(2, 2));
            final Value arrayValue = Value.constantOf(arrayOf(1, 2, 3));
            final Value stringValue = Value.constantOf("abc");
            assertThrowsException(matrixValue);
            assertThrowsException(matrixValue, 1);
            assertThrowsException(matrixValue, 1, 1, 2);
            assertThrowsException(matrixValue, 1, 1, 2, 2, 2);
            assertThrowsException(arrayValue);
            assertThrowsException(arrayValue, 1, 1, 2);
            assertThrowsException(stringValue);
            assertThrowsException(stringValue, 1, 1, 2);
        }
        
        @Test
        @DisplayName("Result access after size modification")
        public void testResultAccessAfterSizeModification()
                throws ErrorMessage
        {
            final Matrix mx = MockMatrix.valueOf(2, 2);
            final var arr = arrayOf(1, 2, 3);
            
            final Value mxElement = INDEXER.get(new Variable(mx), 1, 1);
            final Value mxPartition = INDEXER.get(new Variable(mx), 1, 1, 2, 2);
            final Value arrElement = INDEXER.get(new Variable(arr), 1);
            final Value subArray = INDEXER.get(new Variable(arr), 1, 2);
            
            mx.setSize(2, 3, false);
            arr.add(4);
            
            assertThrows(
                    IllegalStateException.class,
                    mxElement::get,
                    "Read of matrix element after size change");
            assertThrows(
                    IllegalStateException.class,
                    () -> mxElement.set(TYPE_CONTEXT.numberType().valueOf(33)),
                    "Write of matrix element after size change");
            assertThrows(
                    IllegalStateException.class,
                    mxPartition::get,
                    "Read of matrix partition after size change");
            assertThrows(
                    IllegalStateException.class,
                    () -> mxPartition.set(MockMatrix.valueOf(1, 1)),
                    "Write of matrix partition after size change");
            assertThrows(
                    IllegalStateException.class,
                    arrElement::get,
                    "Read of array element after size change");
            assertThrows(
                    IllegalStateException.class,
                    () -> arrElement.set(new Object()),
                    "Write of array element after size change");
            assertThrows(
                    IllegalStateException.class,
                    subArray::get,
                    "Read of sub-array after size change");
            assertThrows(
                    IllegalStateException.class,
                    () -> subArray.set(new Object()),
                    "Write of sub-array after size change");
        }
        
        private static Array arrayOf(final Object... elements)
        {
            return TYPE_CONTEXT.arrayType().arrayOf(elements);
        }
        
        /**
         * Asserts that the given value and indices causes an exception.
         */
        private static void assertThrowsException(
                final Value value, final int... indices)
        {
            assertThrows(
                    ErrorMessage.class,
                    () -> INDEXER.get(value, indices),
                      "Indexable:\n" + value.get()
                    + "\nIndices:\n" + Arrays.toString(indices));
        }
        
        /**
         * Asserts that actual is a constant with the expected value.
         */
        private static void assertConstant(
                final Value actual, final Object expectedValue)
        {
            assertTrue(actual.isConstant(), "isConstant");
            assertValue(actual, expectedValue);
        }
        
        /**
         * Asserts that actual has the expected value.
         */
        private static void assertValue(
                final Value actual, final Object expectedValue)
        {
            assertEquals(expectedValue, actual.get(), "value");
        }
        
        private static final class Variable implements Value
        {
            private Object value;

            private Variable(final Object value)
            {
                this.value = value;
            }

            @Override
            public Object get()
            {
                return value;
            }

            @Override
            public void set(final Object value) throws ErrorMessage
            {
                this.value = value;
            }

            @Override
            public boolean isConstant()
            {
                return false;
            }
            
        }
    }
    
    /*********************
     * Array-type test *
     *********************/

    @Nested
    @DisplayName("StandardTypeContext.arrayType()")
    public static final class ArrayTypeTest
    {
        public ArrayTypeTest()
        {
        }
        
        @Test
        @DisplayName("Stringify empty array")
        public void testStringifyEmptyArray()
        {
            assertEquals("{}", arrayOf().toString());
        }
        
        @Test
        @DisplayName("Stringify array of one single-line element")
        public void testStringifyArrayOfOneSingleLineElement()
        {
            assertEquals("{abc}", arrayOf("abc").toString());
        }
        
        @Test
        @DisplayName("Stringify array of two single-line elements")
        public void testStringifyArrayOfTwoSingleLineElements()
        {
            assertEquals("{abc, def}", arrayOf("abc", "def").toString());
        }
        
        @Test
        @DisplayName("Stringify array of one multiline element")
        public void testStringifyArrayOfOneMultilineElement()
        {
            assertEquals("{a}\n b", arrayOf("a\nb").toString());
        }
        
        @Test
        @DisplayName("Stringify array of two multiline elements")
        public void testStringifyArrayOfTwoMultilineElements()
        {
            assertEquals("{a, c}\n b  d", arrayOf("a\nb", "c\nd").toString());
        }
        
        @Test
        @DisplayName("Stringify array of elements with varying number of lines")
        public void testStringifyArrayOfElementsWithVaryingNumberOfLines()
        {
            final String actual = arrayOf(
                    "a\ntu",
                    "bc",
                    "def\ny\nwz",
                    "gh\nx",
                    "i").toString();
            final String expected
                = "{a , bc, def, gh, i}\n tu      y    x\n         wz";
            assertEquals(expected, actual);
        }
        
        @Test
        @DisplayName(
                "Stringify array of multiline elements using both CR and LF")
        public void testStringifyArrayOfMultilineElementsUsingBothCrAndLf()
        {
            final String actual = arrayOf(
                    "A\nTU",
                    "BC",
                    "DEF\rY\r\nWZ",
                    "GH\nX",
                    "I").toString();
            final String expected
                = "{A , BC, DEF, GH, I}\n TU      Y    X\n         WZ";
            assertEquals(expected, actual);
        }
        
        @Test
        @DisplayName("Stringify array containing the empty string")
        public void testStringifyArrayContainingTheEmptyString()
        {
            assertEquals("{}", arrayOf("").toString());
        }
        
        @Test
        @DisplayName("Stringify array of empty and multiline string")
        public void testStringifyArrayOfEmptyAndMultilineString()
        {
            final String actual = arrayOf("", "a\nbc", "").toString();
            final String expected = "{, a , }\n   bc";
            assertEquals(expected, actual);
        }
        
        private static Array arrayOf(final Object... elements)
        {
            return TYPE_CONTEXT.arrayType().arrayOf(elements);
        }
        
    }
    
    /*******************************
     * Associative-array-type test *
     *******************************/

    @Nested
    @DisplayName("StandardTypeContext.associativeArrayType()")
    public static final class AssociativeArrayTypeTest
    {
        public AssociativeArrayTypeTest()
        {
        }
        
        @Test
        @DisplayName("Give associative arrays undefs")
        public void testGiveAssociativeArraysUndefs()
        {
            assertEquals(
                    arrayOf(),
                    arrayOf(undef(), undef()),
                    "undef => undef");
            assertEquals(
                    arrayOf(),
                    arrayOf("a", undef()),
                    "'a' => undef");
            assertEquals(
                    arrayOf(),
                    arrayOf(undef(), "b"),
                    "undef => 'b'");
        }
        
        @Test
        @DisplayName("Stringify empty association")
        public void testStringifyEmptyAssociation()
        {
            assertEquals(" => ", associate("", "").toString());
        }
        
        @Test
        @DisplayName("Stringify single-line association")
        public void testStringifySingleLineAssociation()
        {
            assertEquals("a => b", associate("a", "b").toString());
        }
        
        @Test
        @DisplayName("Stringify multiline association")
        public void testStringifyMultilineAssociation()
        {
            final String actual = associate("a\nbc", "b").toString();
            final String expected = "a  => b\nbc";
            assertEquals(expected, actual);
        }
        
        @Test
        @DisplayName("Stringify empty associative array")
        public void testStringifyEmptyAssociativeArray()
        {
            assertEquals("{}", arrayOf().toString());
        }
        
        @Test
        @DisplayName("Stringify single-line associative array")
        public void testStringifySingleLineAssociativeArray()
        {
            assertEquals("{x => y}", arrayOf("x", "y").toString());
        }
        
        @Test
        @DisplayName("Stringify multiline associative array")
        public void testStringifyMultilineAssociativeArray()
        {
            final String actual = arrayOf("a\nbc", "b").toString();
            final String expected = "{a  => b}\n bc";
            assertEquals(expected, actual);
        }
        
        private static Association associate(
                final Object key, final Object value)
        {
            return TYPE_CONTEXT.associativeArrayType().associate(key, value);
        }
        
        private static AssociativeArray arrayOf(final Object... elements)
        {
            assert elements.length % 2 == 0;
            final AssociativeArrayType type
                = TYPE_CONTEXT.associativeArrayType();
            return  type.associativeArrayOf(
                    IntStream.iterate(0, i -> i < elements.length, i -> i + 2)
                        .mapToObj(i -> associate(elements[i], elements[i + 1]))
                        .toArray(Association[]::new));
        }
        
        private static Object undef()
        {
            return Values.UNDEF.get();
        }
        
    }
    
    /********************
     * Matrix-type test *
     ********************/

    @Nested
    @DisplayName("StandardTypeContext.matrixType()")
    public static final class MatrixTypeTest
    {
        private static final MatrixStringifier
        STRINGIFIER = Configs.standardTypeContext()
            .matrixType().createMatrix(0, 0).stringifier();
        
        public MatrixTypeTest()
        {
        }
        
        @Test
        @DisplayName("entryStringifier")
        public void testEntryStringifier()
        {
            assertEquals(
                    "0",
                    STRINGIFIER.entryStringifier().apply(0),
                    "0");
            assertEquals(
                    "21",
                    STRINGIFIER.entryStringifier().apply(21),
                    "21");
            assertEquals(
                    "-22",
                    STRINGIFIER.entryStringifier().apply(-22),
                    "-22");
            assertEquals(
                    "0",
                    STRINGIFIER.entryStringifier().apply(-0),
                    "-0");
            assertEquals(
                    "1/2",
                    STRINGIFIER.entryStringifier().apply(.5),
                    ".5");
            assertEquals(
                    String.valueOf(Math.PI),
                    STRINGIFIER.entryStringifier().apply(Math.PI),
                    "pi");
            assertEquals(
                    "5001/5",
                    STRINGIFIER.entryStringifier().apply(1000.2),
                    "1000.2");
        }
    }

}
