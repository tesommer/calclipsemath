package test.com.calclipse.mcomp.script.task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.ParensAndComma;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.script.Statement;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.message.ScriptMessages;
import com.calclipse.mcomp.script.task.Declare;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("Declare")
public final class DeclareTest
{
    private static final Trace
    TRACE = new Trace.Builder()
        .setScriptName("name")
        .setScriptLocation("location")
        .build();
    
    private static final Place PLACE = Place.atLineColumn(1, 2);
    
    private static final Identifier ID = new Identifier("ID123", PLACE);
    private static final Identifier ARG1 = new Identifier("ARG123", PLACE);
    private static final Identifier ARG2 = new Identifier("ARG456", PLACE);
    
    private static final Operand
    PI = Operand.of("pi", Value.constantOf(Math.PI));
    
    private static final Operand
    E = Operand.of("e", Value.constantOf(Math.E));
    
    private static final Operand
    UNDEF = Operand.of("undef", Values.UNDEF);
    
    private static final Object DEFAULT_VALUE = UNDEF.get();
    
    private static final MockContext.MockParser PARSER = e ->
    {
        final var result = new Expression.Builder(
                (expression, interceptReturn)
                -> ((Operand)expression.get(0).token()).value());
        if (PI.matches(e))
        {
            result.add(Fragment.of(PI, 0));
        }
        else if (E.matches(e))
        {
            result.add(Fragment.of(E, 0));
        }
        else
        {
            result.add(Fragment.of(UNDEF, 0));
        }
        return result.build();
    };
    
    private final IdRegistry registry = new IdRegistry();
    private McContext context;
    
    public DeclareTest()
    {
    }
    
    @BeforeEach
    public void setUp()
    {
        reset();
    }
    
    private void reset()
    {
        registry.clear();
        context = new MockContext(PARSER);
    }
    
    /******************
     * Variable tests *
     ******************/
    
    @Test
    @DisplayName("Declare variable")
    public void testDeclareVariable() throws ErrorMessage
    {
        assertNotDeclared(ID);
        declaration(ID, null).execute(context);
        assertDeclaredVariable(ID, DEFAULT_VALUE);
        reset();
        assertNotDeclared(ID);
        declaration(ID, "pi").execute(context);
        assertDeclaredVariable(ID, PI.get());
    }
    
    @Test
    @DisplayName("Reassign variable")
    public void testReassignVariable() throws ErrorMessage
    {
        assertNotDeclared(ID);
        final Task declaration = declaration(ID, "pi");
        declaration.execute(context);
        assertDeclaredVariable(ID, PI.get());
        declaration(ID, "e").execute(context);
        assertDeclaredVariable(ID, E.get());
        declaration.execute(context);
        assertDeclaredVariable(ID, PI.get());
    }
    
    @Test
    @DisplayName("Reassign variable without assignment")
    public void testReassignVariableWithoutAssignment()
            throws ErrorMessage
    {
        assertNotDeclared(ID);
        declaration(ID, "pi").execute(context);
        assertDeclaredVariable(ID, PI.get());
        declaration(ID, null).execute(context);
        assertDeclaredVariable(ID, PI.get());
    }
    
    /**
     * The user attempts to declare a function with the same name as a
     * previously declared variable.
     */
    @Test
    @DisplayName("Reassign variable with parameters")
    public void testReassignVariableWithParameters() throws ErrorMessage
    {
        assertNotDeclared(ID);
        declaration(ID, null).execute(context);
        assertDeclaredVariable(ID, DEFAULT_VALUE);
        assertThrows(
                ErrorMessage.class,
                () -> declarationWithVariables(ID, "pi", ARG1)
                        .execute(context));
    }
    
    /******************
     * Function tests *
     ******************/
    
    @Test
    @DisplayName("Declare function with empty parameter list")
    public void testDeclareFunctionWithEmptyParameterList()
            throws ErrorMessage
    {
        assertNotDeclared(ID);
        declarationWithVariables(ID, null).execute(context);
        assertDeclaredFunction(ID, DEFAULT_VALUE);
    }
    
    @Test
    @DisplayName("Declare function with assignment and one parameter")
    public void testDeclareFunctionWithAssignmentAndOneParameter()
            throws ErrorMessage
    {
        assertNotDeclared(ID);
        assertNotDeclared(ARG1);
        declarationWithVariables(ID, "e", ARG1).execute(context);
        assertDeclaredFunction(ID, E.get(), ARG1);
        assertDeclaredVariable(ARG1, DEFAULT_VALUE);
    }
    
    /**
     * Declares a function with two variables,
     * then redefines the function specifying the same two variables.
     */
    @Test
    @DisplayName("Declare and reassign function with two parameters")
    public void testDeclareAndReassignFunctionWithTwoParameters()
            throws ErrorMessage
    {
        assertNotDeclared(ID);
        assertNotDeclared(ARG1);
        assertNotDeclared(ARG2);
        declarationWithVariables(ID, null, ARG1, ARG2).execute(context);
        assertDeclaredVariable(ARG1, DEFAULT_VALUE);
        assertDeclaredVariable(ARG2, DEFAULT_VALUE);
        assertDeclaredFunction(ID, DEFAULT_VALUE, ARG1, ARG2);
        // The function ignores its parameters.
        assertDeclaredFunction(ID, DEFAULT_VALUE, ARG2, ARG1);
        final Token oldFunc = context.symbol(ID.name()).get();
        final Token oldVar1 = context.symbol(ARG1.name()).get();
        final Token oldVar2 = context.symbol(ARG2.name()).get();
        declarationWithVariables(ID, "e", ARG1, ARG2).execute(context);
        final Token newFunc = context.symbol(ID.name()).get();
        final Token newVar1 = context.symbol(ARG1.name()).get();
        final Token newVar2 = context.symbol(ARG2.name()).get();
        assertNotSame(oldFunc, newFunc, "oldFunc: newFunc");
        assertSame(oldVar1, newVar1, "oldVar1: newVar1");
        assertSame(oldVar2, newVar2, "oldVar2: newVar2");
        declarationWithVariables(ID, null, ARG1, ARG2).execute(context);
        final Token newerFunc = context.symbol(ID.name()).get();
        assertSame(newFunc, newerFunc, "newFunc: newerFunc");
        assertDeclaredVariable(ARG1, DEFAULT_VALUE);
        assertDeclaredVariable(ARG2, DEFAULT_VALUE);
        assertDeclaredFunction(ID, E.get(), ARG1, ARG1);
    }
    
    /**
     * Declares a function with variables,
     * then redefines the function without specifying any variables
     * (not even empty parentheses).
     */
    @Test
    @DisplayName("Declare function with parameters and reassign without")
    public void testDeclareFunctionWithParametersAndReassignWithout()
            throws ErrorMessage
    {
        assertNotDeclared(ID);
        assertNotDeclared(ARG1);
        assertNotDeclared(ARG2);
        declarationWithVariables(ID, null, ARG1, ARG2).execute(context);
        assertDeclaredFunction(ID, DEFAULT_VALUE, ARG1, ARG2);
        final Token oldFunc = context.symbol(ID.name()).get();
        final Token oldVar1 = context.symbol(ARG1.name()).get();
        final Token oldVar2 = context.symbol(ARG2.name()).get();
        declaration(ID, "e").execute(context);
        final Token newFunc = context.symbol(ID.name()).get();
        final Token newVar1 = context.symbol(ARG1.name()).get();
        final Token newVar2 = context.symbol(ARG2.name()).get();
        assertNotSame(oldFunc, newFunc, "oldFunc: newFunc");
        assertSame(oldVar1, newVar1, "oldVar1: newVar1");
        assertSame(oldVar2, newVar2, "oldVar2: newVar2");
        assertDeclaredVariable(ARG1, DEFAULT_VALUE);
        assertDeclaredVariable(ARG2, DEFAULT_VALUE);
        assertDeclaredFunction(ID, E.get(), ARG1, ARG2);
    }
    
    /**
     * Whatever the number of variables a function was declared with,
     * when redefining the function, either zero args or the exact
     * same argument list must be specified.
     */
    @Test
    @DisplayName("Reassign function with wrong parameters")
    public void testReassignFunctionWithWrongParameters()
            throws ErrorMessage
    {
        declarationWithVariables(ID, null, ARG1, ARG2).execute(context);
        assertThrows(
                ErrorMessage.class,
                () -> declarationWithVariables(ID, "e").execute(context),
                "Initial: (ARG1, ARG2), reassign: ()");
        reset();
        declarationWithVariables(ID, null, ARG1).execute(context);
        assertThrows(
                ErrorMessage.class,
                () -> declarationWithVariables(ID, "e", ARG1, ARG2)
                        .execute(context),
                "Initial: (ARG1), reassign: (ARG1, ARG2)");
        reset();
        declarationWithVariables(ID, null, ARG1, ARG2).execute(context);
        assertThrows(
                ErrorMessage.class,
                () -> declarationWithVariables(ID, "e", ARG2, ARG1)
                        .execute(context),
                "Initial: (ARG1, ARG2), reassign: (ARG2, ARG1)");
    }
    
    @Test
    @DisplayName("Declare function with assignment, then reassign without")
    public void testDeclareFunctionWithAssignmentThenReassignWithout()
            throws ErrorMessage
    {
        declarationWithVariables(ID, "e", ARG1).execute(context);
        assertDeclaredFunction(ID, E.get(), ARG1);
        declarationWithVariables(ID, null, ARG1).execute(context);
        assertDeclaredFunction(ID, E.get(), ARG1);
        declaration(ID, null).execute(context);
        assertDeclaredFunction(ID, E.get(), ARG1);
    }
    
    /**
     * Declares a function and then redefines it without a statement,
     * but with the wrong argument list.
     * Had the argument list been right, nothing should happen.
     * This test checks that an exception is thrown in this scenario.
     */
    @Test
    @DisplayName("Reassign function without assignment and wrong parameters")
    public void testReassignFunctionWithoutAssignmentAndWrongParameters()
            throws ErrorMessage
    {
        declarationWithVariables(ID, "e", ARG1).execute(context);
        assertThrows(
                ErrorMessage.class,
                () -> declarationWithVariables(ID, null).execute(context));
    }
    
    @Test
    @DisplayName("Declare function with already declared parameters")
    public void testDeclareFunctionWithAlreadyDeclaredParameters()
            throws ErrorMessage
    {
        assertNotDeclared(ARG1);
        assertNotDeclared(ARG2);
        declaration(ARG1, "pi").execute(context);
        declaration(ARG2, "e").execute(context);
        assertDeclaredVariable(ARG1, PI.get());
        assertDeclaredVariable(ARG2, E.get());
        declarationWithVariables(ID, "pi", ARG1, ARG2).execute(context);
        assertDeclaredFunction(ID, PI.get(), ARG2, ARG2);
        assertDeclaredVariable(ARG1, PI.get());
        assertDeclaredVariable(ARG2, E.get());
    }
    
    @Test
    @DisplayName("Using function variable as normal variable")
    public void testUsingFunctionVariableAsNormalVariable()
            throws ErrorMessage
    {
        declaration(ARG1, "pi").execute(context);
        declarationWithVariables(ID, "e", ARG1).execute(context);
        ((Value)context.symbol(ARG1.name()).get()).set(21);
        assertDeclaredVariable(ARG1, 21);
        ((Function)context.symbol(ID.name()).get()).operation()
            .evaluate(Value.constantOf(23));
        assertDeclaredFunction(ID, E.get(), ARG1);
        assertDeclaredVariable(ARG1, 21);
    }
    
    /*********************
     * Overwritten tests *
     *********************/
    
    @Test
    @DisplayName("Declared variable overwritten in context")
    public void testDeclaredVariableOverwrittenInContext()
            throws ErrorMessage
    {
        declaration(ID, "pi").execute(context);
        context.removeSymbol(ID.name());
        context.addSymbol(Operand.of(ID.name(), PI.value()));
        assertDeclaredVariable(ID, PI.get());
        assertThrowsOverwritten(declaration(ID, null), ID);
    }
    
    @Test
    @DisplayName("Declared variable removed from context")
    public void testDeclaredVariableRemovedFromContext()
            throws ErrorMessage
    {
        declaration(ID, "pi").execute(context);
        context.removeSymbol(ID.name());
        assertThrowsOverwritten(declaration(ID, null), ID);
    }
    
    @Test
    @DisplayName("Declared function overwritten in context")
    public void testDeclaredFunctionOverwrittenInContext()
            throws ErrorMessage
    {
        declarationWithVariables(ID, "pi").execute(context);
        context.removeSymbol(ID.name());
        context.addSymbol(Function.of(
                ID.name(), ParensAndComma.DELIMITATION, args -> E.value()));
        assertDeclaredFunction(ID, E.get());
        assertThrowsOverwritten(declaration(ID, null), ID);
        assertThrowsOverwritten(declarationWithVariables(ID, null), ID);
        assertThrowsOverwritten(declarationWithVariables(ID, null, ARG1), ID);
    }
    
    @Test
    @DisplayName("Declared function removed from context")
    public void testDeclaredFunctionRemovedFromContext()
            throws ErrorMessage
    {
        declarationWithVariables(ID, "pi").execute(context);
        context.removeSymbol(ID.name());
        assertThrowsOverwritten(declaration(ID, null), ID);
    }
    
    @Test
    @DisplayName("Declared function variable overwritten in context")
    public void testDeclaredFunctionVariableOverwrittenInContext()
            throws ErrorMessage
    {
        declarationWithVariables(ID, "pi", ARG1).execute(context);
        context.removeSymbol(ARG1.name());
        context.addSymbol(Tokens.plain(ARG1.name()));
        assertThrowsOverwritten(declaration(ID, null), ARG1);
    }
    
    @Test
    @DisplayName("Declared function variable removed from context")
    public void testDeclaredFunctionVariableRemovedFromContext()
            throws ErrorMessage
    {
        declarationWithVariables(ID, "pi", ARG1).execute(context);
        context.removeSymbol(ARG1.name());
        assertThrowsOverwritten(declaration(ID, null), ARG1);
    }
    
    @Test
    @DisplayName("Declare function with parameter overwritten in context")
    public void testDeclareFunctionWithParameterOverwrittenInContext()
            throws ErrorMessage
    {
        declaration(ARG1, null).execute(context);
        context.removeSymbol(ARG1.name());
        context.addSymbol(Tokens.plain(ARG1.name()));
        assertThrowsOverwritten(declarationWithVariables(ID, null, ARG1), ARG1);
    }
    
    @Test
    @DisplayName("Declare function with parameter removed from context")
    public void testDeclareFunctionWithParameterRemovedFromContext()
            throws ErrorMessage
    {
        declaration(ARG1, null).execute(context);
        context.removeSymbol(ARG1.name());
        assertThrowsOverwritten(declarationWithVariables(ID, null, ARG1), ARG1);
    }
    
    /*******************
     * Private helpers *
     *******************/
    
    private void assertNotDeclared(final Identifier identifier)
    {
        assertFalse(
                registry.isRegistered(identifier.name(), context),
                "Assert " + identifier + " is not in registry");
        assertFalse(
                context.symbol(identifier.name()).isPresent(),
                "Assert " + identifier + " is not in context");
    }
    
    private void assertDeclared(final Identifier identifier)
    {
        assertTrue(
                registry.isRegistered(identifier.name(), context),
                "Assert " + identifier + " is in registry");
        assertTrue(
                context.symbol(identifier.name()).isPresent(),
                "Assert " + identifier + " is in context");
    }
    
    private void assertDeclaredVariable(
            final Identifier identifier, final Object expectedValue)
    {
        assertDeclared(identifier);
        assertTrue(
                registry.isRegisteredVariable(identifier.name(), context),
                "Assert " + identifier + " is registered variable");
        final Object actualValue = context.symbol(identifier.name())
                .map(Operand.class::cast)
                .map(Value::get)
                .get();
        assertEquals(expectedValue, actualValue, "Variable value");
    }
    
    private void assertDeclaredFunction(
            final Identifier identifier,
            final Object expectedValue,
            final Identifier... variables) throws ErrorMessage
    {
        assertDeclared(identifier);
        assertTrue(
                registry.isRegisteredFunction(identifier.name(), context),
                "Assert " + identifier + " is registered function");
        final var func = (Function)context.symbol(identifier.name())
                .get();
        final Operand[] args = Stream.of(variables)
            .map(Identifier::name)
            .map(context::symbol)
            .map(Optional::get)
            .map(Operand.class::cast)
            .toArray(Operand[]::new);
        final Object actualValue = func.operation().evaluate(args).get();
        assertEquals(expectedValue, actualValue, "Function value");
    }
    
    private void assertThrowsOverwritten(
            final Task declaration, final Identifier overwritten)
    {
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> declaration.execute(context),
                "Assert overwritten: " + overwritten);
        assertTrue(
                thrown.getMessage().contains(ScriptMessages
                        .identifierOverwritten(overwritten.name())),
                "Identifier: "
                        + overwritten + ", thrown: " + thrown.getMessage());
    }
    
    private Task declaration(
            final Identifier identifier, final String assignment)
    {
        final var builder =  new Declare.Builder(TRACE, identifier, registry);
        if (assignment != null)
        {
            builder.setAssignment(new Statement(assignment, PLACE));
        }
        return builder.build();
    }
    
    private Task declarationWithVariables(
            final Identifier identifier,
            final String assignment,
            final Identifier... variables)
    {
        final var builder =  new Declare.Builder(TRACE, identifier, registry)
                .setVariables(variables);
        if (assignment != null)
        {
            builder.setAssignment(new Statement(assignment, PLACE));
        }
        return builder.build();
    }

}
