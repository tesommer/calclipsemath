package test.com.calclipse.math.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.calclipse.math.expression.Token;
import com.calclipse.math.parser.SymbolTable;

/**
 * A mock symbol table.
 * {@link #lookUp(char)} returns tokens in the order they were added.
 * {@link #add(Token)}
 * returns false if a token with the same name is already in the table.
 * 
 * @author Tone Sommerland
 */
public final class MockSymbolTable implements SymbolTable
{
    private final List<Token> symbols = new ArrayList<>();

    public MockSymbolTable()
    {
    }

    @Override
    public boolean add(final Token token)
    {
        if (symbols.stream().anyMatch(token::matches))
        {
            return false;
        }
        symbols.add(token);
        return true;
    }

    @Override
    public boolean remove(final String name)
    {
        for (int i = 0; i < symbols.size(); i++)
        {
            if (symbols.get(i).matches(name))
            {
                symbols.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public Token get(final String name)
    {
        return symbols.stream()
                .filter(token -> token.matches(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Collection<Token> all()
    {
        return List.copyOf(symbols);
    }

    @Override
    public Collection<Token> lookUp(final char firstChar)
    {
        final String prefix = String.valueOf(firstChar);
        return symbols.stream()
                .filter(token -> token.name().startsWith(prefix))
                .collect(Collectors.toList());
    }

    @Override
    public void clear()
    {
        symbols.clear();
    }

}
