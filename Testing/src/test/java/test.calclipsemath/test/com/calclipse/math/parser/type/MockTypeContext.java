package test.com.calclipse.math.parser.type;

import static com.calclipse.math.expression.Errors.errorMessage;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.type.Array;
import com.calclipse.math.parser.type.ArrayType;
import com.calclipse.math.parser.type.Association;
import com.calclipse.math.parser.type.AssociativeArray;
import com.calclipse.math.parser.type.AssociativeArrayType;
import com.calclipse.math.parser.type.BooleanType;
import com.calclipse.math.parser.type.Comparer;
import com.calclipse.math.parser.type.Indexer;
import com.calclipse.math.parser.type.MatrixType;
import com.calclipse.math.parser.type.NumberType;
import com.calclipse.math.parser.type.Sizer;
import com.calclipse.math.parser.type.TypeContext;

import test.com.calclipse.math.matrix.MockMatrix;

/**
 * Uses
 * {@link test.com.calclipse.math.parser.type.MockNumberType},
 * {@link test.com.calclipse.math.parser.type.MockBooleanType},
 * {@link test.com.calclipse.math.matrix.MockMatrix}
 * {@link test.com.calclipse.math.parser.type.MockAssociation}
 * and
 * {@link test.com.calclipse.math.parser.type.MockComparer}.
 * 
 * Does not permit {@code null} for key and/or value in associations.
 * 
 * @author Tone Sommerland
 */
public enum MockTypeContext implements TypeContext
{
    INSTANCE;
    
    private static final String MOCK_MESSAGE = "This is a mock.";
    
    private static final MatrixType
    MATRIX_TYPE = (rows, columns) -> MockMatrix.valueOf(rows, columns);
    
    private static final ArrayType ARRAY_TYPE = MockArray::new;

    @Override
    public NumberType numberType()
    {
        return MockNumberType.INSTANCE;
    }

    @Override
    public BooleanType booleanType()
    {
        return MockBooleanType.INSTANCE;
    }

    @Override
    public MatrixType matrixType()
    {
        return MATRIX_TYPE;
    }

    @Override
    public ArrayType arrayType()
    {
        return ARRAY_TYPE;
    }

    @Override
    public AssociativeArrayType associativeArrayType()
    {
        return new AssociativeArrayType()
        {
            @Override
            public AssociativeArray createAssociativeArray(
                    final Map<?, ?> associations)
            {
                return new MockAssociativeArray(associations);
            }

            @Override
            public Association associate(final Object key, final Object value)
            {
                return new MockAssociation(
                        requireNonNull(key, "key"),
                        requireNonNull(value, "value"));
            }
        };
    }

    @Override
    public Comparer comparer()
    {
        return MockComparer.INSTANCE;
    }

    @Override
    public Indexer indexer()
    {
        return MockIndexer.INSTANCE;
    }

    @Override
    public Sizer sizer()
    {
        return MockSizer.INSTANCE;
    }
    
    private static enum MockIndexer implements Indexer
    {
        INSTANCE;

        @Override
        public Value get(final Value value, final int... indices)
                throws ErrorMessage
        {
            throw errorMessage(MOCK_MESSAGE);
        }
        
    }
    
    private static enum MockSizer implements Sizer
    {
        INSTANCE;

        @Override
        public Value sizeOf(final Value value) throws ErrorMessage
        {
            throw errorMessage(MOCK_MESSAGE);
        }
        
    }
    
    private static final class MockArray
        extends ArrayList<Object> implements Array
    {
        private static final long serialVersionUID = 1L;

        private MockArray(final Collection<?> c)
        {
            super(c);
        }
        
    }
    
    private static final class MockAssociativeArray
        extends HashMap<Object, Object> implements AssociativeArray
    {
        private static final long serialVersionUID = 1L;

        private MockAssociativeArray(final Map<?, ?> m)
        {
            super(m);
        }
        
    }

}
