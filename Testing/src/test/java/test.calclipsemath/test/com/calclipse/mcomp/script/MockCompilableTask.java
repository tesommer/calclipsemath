package test.com.calclipse.mcomp.script;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Compilable;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.trace.Trace;

/**
 * A compilable mock-task that counts executions.
 * This task is compiled if it has been executed one or more times.
 * 
 * @author Tone Sommerland
 */
public final class MockCompilableTask extends Task implements Compilable
{
    private final boolean oneTimeCompilationStep;
    private int executionCount = 0;

    public MockCompilableTask(
            final Trace scriptTrace, final boolean oneTimeCompilationStep)
    {
        super(scriptTrace);
        this.oneTimeCompilationStep = oneTimeCompilationStep;
    }

    /**
     * The number of times this task has been executed.
     */
    public int executionCount()
    {
        return executionCount;
    }
    
    /**
     * Sets the execution count to zero.
     * This will also reset the compiled property.
     */
    public void resetExecutionCount()
    {
        executionCount = 0;
    }

    @Override
    public boolean isCompiled()
    {
        return executionCount > 0;
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        executionCount++;
    }

    @Override
    public boolean isOneTimeCompilationStep()
    {
        return oneTimeCompilationStep;
    }

}
