package test.com.calclipse.mcomp.script.task;

import static com.calclipse.math.expression.Errors.errorMessage;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static test.TestUtil.assertCompiled;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Evaluator;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Values;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.Statement;
import com.calclipse.mcomp.script.task.Execute;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("Execute")
public final class ExecuteTest
{
    private static final Trace
    TRACE = new Trace.Builder().setScriptName("Dick Tracy").build();
    
    private static final Statement
    STATEMENT = new Statement("I'm on duty.", Place.START);
    
    private static final Evaluator
    THROW_ERROR_MESSAGE = (expression, interceptReturn) ->
    {
        throw errorMessage("Wearing that dress");
    };

    public ExecuteTest()
    {
    }
    
    @Test
    @DisplayName("execute")
    public void testExecute() throws ErrorMessage
    {
        final var sideEffect = new SideEffect();
        final Execute task = taskToTest();
        assertCompiled(false, task);
        task.execute(context(sideEffect));
        assertCompiled(true, task);
        assertSideEffect(true, sideEffect);
        final Execute task2 = taskToTest();
        assertCompiled(false, task2);
        assertThrows(
                ErrorMessage.class,
                () -> task2.execute(context(THROW_ERROR_MESSAGE)),
                "Execution should have caused error message to be thrown.");
        assertCompiled(true, task2);
    }
    
    private static Execute taskToTest()
    {
        return new Execute(TRACE, STATEMENT);
    }
    
    private static McContext context(final Evaluator evaluator)
    {
        return new MockContext(
                expression -> new Expression.Builder(evaluator).build());
    }
    
    private static void assertSideEffect(
            final boolean expected, final SideEffect actual)
    {
        if (expected)
        {
            assertTrue(
                    actual.evaluated,
                    "Missing side effect from evaluation.");
        }
        else
        {
            assertFalse(
                    actual.evaluated,
                    "Unexpected side effect from evaluation.");
        }
    }
    
    private static final class SideEffect implements Evaluator
    {
        private boolean evaluated;

        private SideEffect()
        {
        }

        @Override
        public Value evaluate(
                final Expression expression,
                final boolean interceptReturn) throws ErrorMessage
        {
            evaluated = true;
            return Values.UNDEF;
        }
    }

}
