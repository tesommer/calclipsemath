package test.com.calclipse.math.parser.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.BinaryOperator;
import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.math.parser.step.ImplicitMultiplication;

import test.TestUtil;

@DisplayName("ImplicitMultiplication")
public final class ImplicitMultiplicationTest
{
    private static final Token
    LPAREN = Tokens.leftParenthesis("(");
    
    private static final Token
    RPAREN = Tokens.rightParenthesis(")");
    
    private static final Token
    COMMA = Tokens.plain(",");
    
    private static final Token
    PI = Operand.of("pi", Values.UNDEF);
    
    private static final Token
    E = Operand.of("e", Values.UNDEF);
    
    private static final Token
    MAX = Function.of(
            "max",
            Delimitation.byTokensWithOpener(
                    LPAREN, RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);
    
    private static final BinaryOperator
    MULTIPLY = BinaryOperator.of("*", 1, (left, right) -> Values.UNDEF);
    
    private static final Operator
    SIN = UnaryOperator.prefixed("sin", 2, arg -> Values.UNDEF);
    
    private static final Operator
    FACTORIAL = UnaryOperator.postfixed("!", 3, arg -> Values.UNDEF);
    
    private static final ParseStep
    STEP = new ImplicitMultiplication(() -> MULTIPLY);

    public ImplicitMultiplicationTest()
    {
    }
    
    @Test
    @DisplayName("Explicit multiplication")
    public void testExplicitMultiplication() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(FACTORIAL, 0);
        final Fragment frag2 = Fragment.of(MULTIPLY, 10);
        final Fragment frag3 = Fragment.of(SIN,      20);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3);
        TestUtil.assertContainsSameFragments(output, frag1, frag2, frag3);
    }
    
    @Test
    @DisplayName("Value value")
    public void testValueValue() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(PI,   0);
        final Fragment frag2 = Fragment.of(E,   10);
        final Fragment frag3 = Fragment.of(MAX, 20);
        final Fragment frag4 = Fragment.of(MAX, 30);
        final Fragment frag5 = Fragment.of(E,   40);
        final Expression output = TestUtil.parse(
                STEP, frag1, frag2, frag3, frag4, frag5);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output,
                frag1, null, frag2, null,
                null, null, null, null, frag5);
        assertImplicitMultiplication(
                output.get(1),
                frag2,
                OperatorPriorities.IMPLICIT_MULTIPLICATION);
        assertImplicitMultiplication(
                output.get(3),
                frag3,
                OperatorPriorities.IMPLICIT_MULTIPLICATION);
        assertImplicitMultiplication(
                output.get(5),
                frag4,
                OperatorPriorities.IMPLICIT_MULTIPLICATION);
        assertImplicitMultiplication(
                output.get(7),
                frag5,
                OperatorPriorities.IMPLICIT_MULTIPLICATION);
    }
    
    @Test
    @DisplayName("Right-parenthesis left-parenthesis")
    public void testRightParenthesisLeftParenthesis() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(RPAREN,  0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Expression output = TestUtil.parse(STEP, frag1, frag2);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, frag1, null, frag2);
        assertImplicitMultiplication(
                output.get(1),
                frag2,
                OperatorPriorities.IMPLICIT_MULTIPLICATION);
    }
    
    @Test
    @DisplayName("Right-unary left-parenthesis")
    public void testRightUnaryLeftParenthesis() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(FACTORIAL, 0);
        final Fragment frag2 = Fragment.of(LPAREN,   10);
        final Expression output = TestUtil.parse(STEP, frag1, frag2);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, frag1, null, frag2);
        assertImplicitMultiplication(
                output.get(1),
                frag2,
                FACTORIAL.priority());
    }
    
    @Test
    @DisplayName("Right-parenthesis left-unary")
    public void testRightParenthesisLeftUnary() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(RPAREN, 0);
        final Fragment frag2 = Fragment.of(SIN,   10);
        final Expression output = TestUtil.parse(STEP, frag1, frag2);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, frag1, null, frag2);
        assertImplicitMultiplication(
                output.get(1),
                frag2,
                SIN.priority());
    }
    
    @Test
    @DisplayName("Right-unary left-unary")
    public void testRightUnaryLeftUnary() throws ErrorMessage
    {
        final Fragment frag1 = Fragment.of(FACTORIAL, 0);
        final Fragment frag2 = Fragment.of(SIN,      10);
        final Expression output = TestUtil.parse(STEP, frag1, frag2);
        TestUtil.assertContainsSameFragmentsButSkipNulls(
                output, frag1, null, frag2);
        final int expectedPriority
            = Math.min(FACTORIAL.priority(), SIN.priority());
        assertImplicitMultiplication(
                output.get(1),
                frag2,
                expectedPriority);
    }
    
    /**
     * Asserts correct properties of an implicit multiplication
     * @param implicit the implicit multiplication
     * @param next the fragment after the implicit multiplication
     * @param expectedPriority the expected priority of implicit
     */
    private static void assertImplicitMultiplication(
            final Fragment implicit,
            final Fragment next,
            final int expectedPriority)
    {
        assertEquals(
                next.position(),
                implicit.position(),
                "Position of implicit multiplication");
        assertEquals(
                next.token().name(),
                implicit.token().name(),
                "Name of implicit multiplication");
        assertEquals(
                expectedPriority,
                ((Operator)implicit.token()).priority(),
                "Priority of implicit multiplication");
        assertTrue(
                implicit.token().identifiesAs(MULTIPLY),
                "ID of implicit multiplication");
    }

}
