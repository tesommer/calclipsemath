package test.com.calclipse.math.parser.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.Delimitation;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Id;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenStream;
import com.calclipse.math.expression.Tokens;
import com.calclipse.math.expression.Values;
import com.calclipse.math.parser.step.Arguments;

import test.TestUtil;

@DisplayName("Arguments")
public final class ArgumentsTest
{
    private static final Token LPAREN = Tokens.leftParenthesis("(");
    private static final Token RPAREN = Tokens.rightParenthesis(")");
    private static final Token COMMA = Tokens.plain(",");
    private static final Token A = Tokens.plain("a");
    private static final Token B = Tokens.plain("b");
    private static final Token C = Tokens.plain("c");
    private static final Token D = Tokens.plain("d");
    
    private static final Function
    FUNC = Function.of(
            "f",
            Delimitation.byTokensWithOpener(
                    LPAREN, RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);
    
    private static final Function
    FUNC_WO_OPENER = Function.of(
            "f2",
            Delimitation.byTokensWithoutOpener(
                    RPAREN, COMMA, Token::identifiesAs),
            args -> Values.UNDEF);

    public ArgumentsTest()
    {
    }
    
    @Test
    @DisplayName("Single function with one argument")
    public void testSingleFunctionWithOneArgument() throws ErrorMessage
    {
        // f ( A )
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(RPAREN, 30);
        final Expression output = TestUtil.parse(
                Arguments.INSTANCE,
                frag1, frag2, frag3, frag4);
        assertContains(output, frag1);
        assertEquals(
                1,
                arguments(output, 0).size(),
                "Number of arguments");
        assertContains(argument(output, 0, 0), frag3);
    }
    
    @Test
    @DisplayName("Single function with multiple arguments")
    public void testSingleFunctionWithMultipleArguments() throws ErrorMessage
    {
        // f ( A , B )
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(COMMA,  30);
        final Fragment frag5 = Fragment.of(B,      40);
        final Fragment frag6 = Fragment.of(RPAREN, 50);
        final Expression output = TestUtil.parse(
                Arguments.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6);
        assertContains(output, frag1);
        assertEquals(
                2,
                arguments(output, 0).size(),
                "Number of arguments");
        assertContains(argument(output, 0, 0), frag3);
        assertContains(argument(output, 0, 1), frag5);
    }
    
    @Test
    @DisplayName("Complex, multilevel expression")
    public void testComplexMultilevelExpression() throws ErrorMessage
    {
        // A f ( B f ( C , D ) , f ( ) )
        final Fragment frag1 = Fragment.of(A,         0);
        final Fragment frag2 = Fragment.of(FUNC,     10);
        final Fragment frag3 = Fragment.of(LPAREN,   20);
        final Fragment frag4 = Fragment.of(B,        30);
        final Fragment frag5 = Fragment.of(FUNC,     40);
        final Fragment frag6 = Fragment.of(LPAREN,   50);
        final Fragment frag7 = Fragment.of(C,        60);
        final Fragment frag8 = Fragment.of(COMMA,    70);
        final Fragment frag9 = Fragment.of(D,        80);
        final Fragment frag10 = Fragment.of(RPAREN,  90);
        final Fragment frag11 = Fragment.of(COMMA,  100);
        final Fragment frag12 = Fragment.of(FUNC,   110);
        final Fragment frag13 = Fragment.of(LPAREN, 120);
        final Fragment frag14 = Fragment.of(RPAREN, 130);
        final Fragment frag15 = Fragment.of(RPAREN, 140);
        final Expression output = TestUtil.parse(
                Arguments.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6, frag7, frag8,
                frag9, frag10, frag11, frag12, frag13, frag14, frag15);
        assertContains(output, frag1, frag2);
        final List<Expression> args_f2 = arguments(output, 1);
        assertEquals(2, args_f2.size(), "args_f2.size()");
        final Expression arg_f2_0 = args_f2.get(0);
        assertContains(arg_f2_0, frag4, frag5);
        final Expression arg_f2_1 = args_f2.get(1);
        assertContains(arg_f2_1, frag12);
        final List<Expression> args_f5 = arguments(arg_f2_0, 1);
        assertEquals(2, args_f5.size(), "args_f5.size()");
        final Expression arg_f5_0 = args_f5.get(0);
        assertContains(arg_f5_0, frag7);
        final Expression arg_f5_1 = args_f5.get(1);
        assertContains(arg_f5_1, frag9);
        final List<Expression> args_f12 = arguments(arg_f2_1, 0);
        assertTrue(args_f12.isEmpty(), "args_f12.isEmpty()");
    }
    
    @Test
    @DisplayName("Argument containing comma inside parentheses")
    public void testArgumentContainingCommaInsideParentheses()
            throws ErrorMessage
    {
        // f ( ( A , B ) , C )
        final Fragment frag1 = Fragment.of(FUNC,     0);
        final Fragment frag2 = Fragment.of(LPAREN,  10);
        final Fragment frag3 = Fragment.of(LPAREN,  20);
        final Fragment frag4 = Fragment.of(A,       30);
        final Fragment frag5 = Fragment.of(COMMA,   40);
        final Fragment frag6 = Fragment.of(B,       50);
        final Fragment frag7 = Fragment.of(RPAREN,  60);
        final Fragment frag8 = Fragment.of(COMMA,   70);
        final Fragment frag9 = Fragment.of(C,       80);
        final Fragment frag10 = Fragment.of(RPAREN, 90);
        final Expression output = TestUtil.parse(
                Arguments.INSTANCE,
                frag1, frag2, frag3, frag4, frag5,
                frag6, frag7, frag8, frag9, frag10);
        final List<Expression> args_f1 = arguments(output, 0);
        assertEquals(2, args_f1.size(), "args_f1.size()");
        assertContains(args_f1.get(0), frag3, frag4, frag5, frag6, frag7);
        assertContains(args_f1.get(1), frag9);
    }
    
    @Test
    @DisplayName("Function without opener")
    public void testFunctionWithoutOpener() throws ErrorMessage
    {
        // f2 A , B )
        final Fragment frag1 = Fragment.of(FUNC_WO_OPENER, 0);
        final Fragment frag2 = Fragment.of(A,             10);
        final Fragment frag3 = Fragment.of(COMMA,         20);
        final Fragment frag4 = Fragment.of(B,             30);
        final Fragment frag5 = Fragment.of(RPAREN,        40);
        final Expression output = TestUtil.parse(
                Arguments.INSTANCE,
                frag1, frag2, frag3, frag4, frag5);
        assertContains(output, frag1);
        final List<Expression> args_f1 = arguments(output, 0);
        assertEquals(2, args_f1.size(), "args_f1.size()");
        assertContains(args_f1.get(0), frag2);
        assertContains(args_f1.get(1), frag4);
    }
    
    @Test
    @DisplayName("Expression ending prematurely after function")
    public void testExpressionEndingPrematurelyAfterFunction()
    {
        // f
        final Fragment frag1 = Fragment.of(FUNC, 0);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        Arguments.INSTANCE,
                        frag1),
                "parse: f");
        assertEquals(
                frag1.token().name(),
                thrown.detail().get().token().name(),
                "f: detail");
    }
    
    @Test
    @DisplayName("Expression ending prematurely after opener")
    public void testExpressionEndingPrematurelyAfterOpener()
    {
        // f (
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        Arguments.INSTANCE,
                        frag1, frag2),
                "parse: f (");
        assertSame(frag2, thrown.detail().get(), "f (: detail");
    }
    
    @Test
    @DisplayName("Expression ending prematurely inside argument")
    public void testExpressionEndingPrematurelyInsideArgument()
    {
        // f ( a
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(A,      20);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        Arguments.INSTANCE,
                        frag1, frag2, frag3),
                "parse: f ( a");
        assertSame(frag3, thrown.detail().get(), "f ( a: detail");
    }
    
    @Test
    @DisplayName("Expression ending prematurely after function without opener")
    public void testExpressionEndingPrematurelyAfterFunctionWOOpener()
    {
        // f2
        final Fragment frag1 = Fragment.of(FUNC_WO_OPENER, 0);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        Arguments.INSTANCE,
                        frag1),
                "parse: f2");
        assertEquals(
                frag1.token().name(),
                thrown.detail().get().token().name(),
                "f2: detail");
    }
    
    @Test
    @DisplayName("Unexpected separator after opener")
    public void testUnexpectedSeparatorAfterOpener()
    {
        // f ( ,
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(COMMA,  20);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        ArgumentsWithoutEnd.INSTANCE,
                        frag1, frag2, frag3),
                "parse: f ( ,");
        assertSame(frag3, thrown.detail().get(), "f ( ,: detail");
    }
    
    @Test
    @DisplayName("Unexpected separator after separator")
    public void testUnexpectedSeparatorAfterSeparator()
    {
        // f ( a , ,
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(COMMA,  30);
        final Fragment frag5 = Fragment.of(COMMA,  40);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        ArgumentsWithoutEnd.INSTANCE,
                        frag1, frag2, frag3, frag4, frag5),
                "parse: f ( a , ,");
        assertSame(frag5, thrown.detail().get(), "f ( a , ,: detail");
    }
    
    @Test
    @DisplayName("Unexpected closer after separator")
    public void testUnexpectedCloserAfterSeparator()
    {
        // f ( a , )
        final Fragment frag1 = Fragment.of(FUNC,    0);
        final Fragment frag2 = Fragment.of(LPAREN, 10);
        final Fragment frag3 = Fragment.of(A,      20);
        final Fragment frag4 = Fragment.of(COMMA,  30);
        final Fragment frag5 = Fragment.of(RPAREN, 40);
        final ErrorMessage thrown = assertThrows(
                ErrorMessage.class,
                () -> TestUtil.parse(
                        ArgumentsWithoutEnd.INSTANCE,
                        frag1, frag2, frag3, frag4, frag5),
                "parse: f ( a , )");
        assertSame(frag5, thrown.detail().get(), "f ( a , ): detail");
    }
    
    @Test
    @DisplayName("Function with self-closing arguments")
    public void testFunctionWithSelfClosingArguments() throws ErrorMessage
    {
        final Id id = Id.unique();
        final Token closer = RPAREN.withId(id);
        final Delimitation delim = Delimitation.byTokensWithoutOpener(
                closer, COMMA, Token::identifiesAs);
        final Function func = Function.of(
                "|", delim, args -> Values.UNDEF).withId(id);
        final Fragment frag1 = Fragment.of(func,   0);
        final Fragment frag2 = Fragment.of(A,     10);
        final Fragment frag3 = Fragment.of(COMMA, 20);
        final Fragment frag4 = Fragment.of(B,     30);
        final Fragment frag5 = Fragment.of(func,  40);
        final Fragment frag6 = Fragment.of(C,     50);
        final Expression output = TestUtil.parse(
                Arguments.INSTANCE,
                frag1, frag2, frag3, frag4, frag5, frag6);
        assertContains(output, frag1, frag6);
        assertEquals(
                2,
                arguments(output, 0).size(),
                "Number of arguments");
        assertContains(argument(output, 0, 0), frag2);
        assertContains(argument(output, 0, 1), frag4);
    }
    
    private static void assertContains(
            final Expression actual, final Fragment... expected)
    {
        assertEquals(
                expected.length,
                actual.size(),
                "Number of fragments");
        for (int i = 0; i < expected.length; i++)
        {
            final Fragment expectedFrag = expected[i];
            final Fragment actualFrag = actual.get(i);
            assertEquals(
                    expectedFrag.token().name(),
                    actualFrag.token().name(),
                    "Fragment at index " + i + ": token name");
        }
    }
    
    private static List<Expression> arguments(
            final Expression expression, final int indexOfFunction)
    {
        final var func = (Function)expression.get(indexOfFunction).token();
        return func.arguments();
    }
    
    private static Expression argument(
            final Expression expression,
            final int indexOfFunction,
            final int indexOfArgument)
    {
        return arguments(expression, indexOfFunction).get(indexOfArgument);
    }
    
    private static enum ArgumentsWithoutEnd implements ParseStep
    {
        INSTANCE;

        @Override
        public void read(final TokenStream stream) throws ErrorMessage
        {
            Arguments.INSTANCE.read(stream);
        }

        @Override
        public void end(final TokenStream stream) throws ErrorMessage
        {
        }
        
    }

}
