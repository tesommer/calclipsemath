package test.com.calclipse.mcomp.trace;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.mcomp.trace.Place;

@DisplayName("Place")
public final class PlaceTest
{
    public PlaceTest()
    {
    }
    
    @Test
    @DisplayName("Illegal construction arguments")
    public void testIllegalConstructionArguments()
    {
        assertThrows(IllegalArgumentException.class, () -> at(0, 1), "(0, 1)");
        assertThrows(IllegalArgumentException.class, () -> at(1, 0), "(1, 0)");
        assertThrows(IllegalArgumentException.class, () -> at(0, 0), "(0, 0)");
    }
    
    @Test
    @DisplayName("relativeTo")
    public void testRelativeTo()
    {
        assertPlace(1, 1, at(1, 1).relativeTo(at(1, 1)));
        assertPlace(1, 2, at(1, 1).relativeTo(at(1, 2)));
        assertPlace(2, 1, at(1, 1).relativeTo(at(2, 1)));
        assertPlace(2, 2, at(1, 1).relativeTo(at(2, 2)));

        assertPlace(1, 2, at(1, 2).relativeTo(at(1, 1)));
        assertPlace(1, 3, at(1, 2).relativeTo(at(1, 2)));
        assertPlace(2, 2, at(1, 2).relativeTo(at(2, 1)));
        assertPlace(2, 3, at(1, 2).relativeTo(at(2, 2)));

        assertPlace(2, 1, at(2, 1).relativeTo(at(1, 1)));
        assertPlace(2, 1, at(2, 1).relativeTo(at(1, 2)));
        assertPlace(3, 1, at(2, 1).relativeTo(at(2, 1)));
        assertPlace(3, 1, at(2, 1).relativeTo(at(2, 2)));

        assertPlace(2, 2, at(2, 2).relativeTo(at(1, 1)));
        assertPlace(2, 2, at(2, 2).relativeTo(at(1, 2)));
        assertPlace(3, 2, at(2, 2).relativeTo(at(2, 1)));
        assertPlace(3, 2, at(2, 2).relativeTo(at(2, 2)));
    }
    
    private static Place at(final int line, final int column)
    {
        return Place.atLineColumn(line, column);
    }
    
    private static void assertPlace(
            final int expectedLine,
            final int expectedColumn,
            final Place actual)
    {
        assertEquals(expectedLine, actual.line(), "line");
        assertEquals(expectedColumn, actual.column(), "column");
    }

}
