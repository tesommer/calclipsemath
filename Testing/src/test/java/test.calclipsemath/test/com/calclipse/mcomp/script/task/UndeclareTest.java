package test.com.calclipse.mcomp.script.task;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Tokens;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.IdRegistry;
import com.calclipse.mcomp.script.Identifier;
import com.calclipse.mcomp.script.Task;
import com.calclipse.mcomp.script.task.Undeclare;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

import test.com.calclipse.mcomp.MockContext;

@DisplayName("Undeclare")
public final class UndeclareTest
{
    private static final Trace
    TRACE = new Trace.Builder().setScriptName("Hammeroid attack").build();
    
    private static final Identifier
    ID = new Identifier("I'm not dying, I'm dining.", Place.START);
    
    private final IdRegistry registry = new IdRegistry();

    public UndeclareTest()
    {
    }
    
    @AfterEach
    public void tearDown()
    {
        registry.clear();
    }
    
    @Test
    @DisplayName("execute")
    public void testExecute() throws ErrorMessage
    {
        final McContext context = MockContext.getOne();
        context.addSymbol(Tokens.plain(ID.name()));
        registry.registerVariable(ID.name(), context, (n, c) -> true);
        final Task task = taskToTest(ID);
        task.execute(context);
        assertFalse(
                context.symbol(ID.name()).isPresent(),
                "Symbol still in context");
        assertFalse(
                registry.isRegistered(ID.name(), context),
                "Symbol still in registry");
        assertThrows(
                ErrorMessage.class,
                () -> task.execute(context),
                "Undeclaring symbol twice");
    }
    
    private Task taskToTest(final Identifier identifier)
    {
        return new Undeclare(TRACE, identifier, registry);
    }

}
