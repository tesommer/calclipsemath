package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashSet;
import java.util.function.Function;

import org.junit.jupiter.api.Assertions;

import com.calclipse.math.Complex;
import com.calclipse.math.Fraction;
import com.calclipse.math.Real;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Expression;
import com.calclipse.math.expression.Fragment;
import com.calclipse.math.expression.ParseStep;
import com.calclipse.math.expression.Parser;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.Values;
import com.calclipse.math.matrix.Matrix;
import com.calclipse.mcomp.script.Compilable;

import test.com.calclipse.math.parser.scanner.MockScanner;

public final class TestUtil
{
    private TestUtil()
    {
    }
    
    /***********
     * General *
     ***********/
    
    /**
     * Asserts hash code consistency for a group of objects.
     * @param objects the objects to check
     * @param uniqueHashCodesRequired minimum unique hash codes to pass
     * @param hashCodeGenerator generates hash codes that should be equal
     * to the hash code of each object served
     */
    public static <T> void assertHashCodeConsistency(
            final T[] objects,
            final int uniqueHashCodesRequired,
            final Function<? super T, int[]> hashCodeGenerator)
    {
        final var uniqueHashCodes = new HashSet<Integer>(objects.length);
        for (final T obj : objects)
        {
            final int hashCode = obj.hashCode();
            final int[] otherHashCodes = hashCodeGenerator.apply(obj);
            for (final int otherHashCode : otherHashCodes)
            {
                Assertions.assertEquals(
                        hashCode,
                        otherHashCode,
                        "hashCode consistency");
            }
            uniqueHashCodes.add(hashCode);
        }
        assertTrue(
                uniqueHashCodes.size() >= uniqueHashCodesRequired,
                "Number of unique hash codes: "
                        + uniqueHashCodes.size()
                        + " Required: >= "
                        + uniqueHashCodesRequired);
    }
    
    /********
     * math *
     ********/

    /**
     * Asserts a real.
     * @param expectedFraction expected fraction
     * @param expectedMathContext expected math context
     * @param expectedFracturability expected fracturability
     * @param actual actual real
     */
    public static void assertReal(
            final Fraction expectedFraction,
            final MathContext expectedMathContext,
            final int expectedFracturability,
            final Real actual)
    {
        assertReal(
                expectedFraction,
                null,
                expectedMathContext,
                expectedFracturability,
                actual);
    }

    /**
     * Asserts a real.
     * @param expectedBigDecimal expected big decimal
     * @param expectedMathContext expected math context
     * @param expectedFracturability expected fracturability
     * @param actual actual real
     */
    public static void assertReal(
            final BigDecimal expectedBigDecimal,
            final MathContext expectedMathContext,
            final int expectedFracturability,
            final Real actual)
    {
        assertReal(
                null,
                expectedBigDecimal,
                expectedMathContext,
                expectedFracturability,
                actual);
    }

    /**
     * Asserts a real.
     * Either expected fraction or expected big decimal must be present.
     * @param expectedFraction expected fraction
     * @param expectedBigDecimal expected big decimal
     * @param expectedMathContext expected math context
     * @param expectedFracturability expected fracturability
     * @param actual actual real
     */
    private static void assertReal(
            final Fraction expectedFraction,
            final BigDecimal expectedBigDecimal,
            final MathContext expectedMathContext,
            final int expectedFracturability,
            final Real actual)
    {
        if (expectedFraction == null)
        {
            assertUnfracturedReal(expectedBigDecimal, actual);
        }
        else
        {
            assertFracturedReal(expectedFraction, actual);
        }
        Assertions.assertEquals(
                expectedMathContext,
                actual.mathContext(),
                "mathContext");
        Assertions.assertEquals(
                expectedFracturability,
                actual.fracturability(),
                "fracturability");
    }

    private static void assertUnfracturedReal(
            final BigDecimal expectedBigDecimal, final Real actual)
    {
        assertFalse(
                actual.isFractured(),
                "isFractured");
        final BigDecimal actualBigDecimal = actual.bigDecimal();
        assertTrue(
                expectedBigDecimal.compareTo(actualBigDecimal) == 0,
                "bigDecimal: expected: "
                        + expectedBigDecimal
                        + " actual: "
                        + actualBigDecimal);
    }

    private static void assertFracturedReal(
            final Fraction expectedFraction, final Real actual)
    {
        assertTrue(
                actual.isFractured(),
                "isFractured");
        Assertions.assertEquals(
                expectedFraction,
                actual.fraction(),
                "fraction");
    }
    
    /**
     * Whether a complex approximately equals another.
     */
    public static boolean approxEquals(
            final Complex complex1,
            final Complex complex2,
            final double delta)
    {
        return
                approxEquals(complex1.real(), complex2.real(), delta)
                &&
                approxEquals(complex1.imaginary(), complex2.imaginary(), delta);
    }
    
    /**
     * Asserts that two complexes are equal.
     * @param delta error tolerance
     */
    public static void assertEquals(
            final Complex expected,
            final Complex actual,
            final double delta,
            final String message)
    {
        assertTrue(approxEquals(expected, actual, delta), message);
    }
    
    /**************
     * expression *
     **************/
    
    /**
     * Parses the input fragments using the given parse steps.
     * The output expression results in undef if evaluated.
     */
    public static Expression parse(
            final ParseStep[] steps,
            final Fragment... input) throws ErrorMessage
    {
        final Parser parser = Parser.of(
                new MockScanner(input),
                steps,
                (expression, interceptReturn) -> Values.UNDEF);
        return parser.parse("");
    }
    
    /**
     * Parses the input fragments using a single parse step.
     * The output expression results in undef if evaluated.
     */
    public static Expression parse(
            final ParseStep step,
            final Fragment... input) throws ErrorMessage
    {
        return parse(new ParseStep[] {step}, input);
    }
    
    private static void assertContainsSameFragments(
            final boolean skipNulls,
            final Expression actual,
            final Fragment... expected)
    {
        Assertions.assertEquals(
                expected.length, actual.size(), "Number of fragents");
        for (int i = 0; i < expected.length; i++)
        {
            final Fragment expectedFrag = expected[i];
            final Fragment actualFrag = actual.get(i);
            if (skipNulls && expectedFrag == null)
            {
                continue;
            }
            assertSame(expectedFrag, actualFrag, "Fragment at index " + i);
        }
    }
    
    /**
     * Asserts that the actual expression contains the expected fragments
     * (by reference).
     * @param actual the actual expression
     * @param expected the expected fragments
     */
    public static void assertContainsSameFragments(
            final Expression actual, final Fragment... expected)
    {
        assertContainsSameFragments(false, actual, expected);
    }
    
    /**
     * Asserts that the actual expression contains the expected fragments
     * (by reference).
     * Skips objects in the expected array that are null.
     * @param actual the actual expression
     * @param expected the expected fragments
     */
    public static void assertContainsSameFragmentsButSkipNulls(
            final Expression actual, final Fragment... expected)
    {
        assertContainsSameFragments(true, actual, expected);
    }
    
    private static void assertContainsSameTokens(
            final boolean skipNulls,
            final Expression actual,
            final Token... expected)
    {
        Assertions.assertEquals(
                expected.length, actual.size(), "Number of tokens");
        for (int i = 0; i < expected.length; i++)
        {
            final Token expectedToken = expected[i];
            final Token actualToken = actual.get(i).token();
            if (skipNulls && expectedToken == null)
            {
                continue;
            }
            assertSame(expectedToken, actualToken, "Token at index " + i);
        }
    }
    
    /**
     * Asserts that the actual expression contains the expected tokens
     * (by reference).
     * @param actual the actual expression
     * @param expected the expected tokens
     */
    public static void assertContainsSameTokens(
            final Expression actual, final Token... expected)
    {
        assertContainsSameTokens(false, actual, expected);
    }
    
    /**
     * Asserts that the actual expression contains the expected tokens
     * (by reference).
     * Skips objects in the expected array that are null.
     * @param actual the actual expression
     * @param expected the expected tokens
     */
    public static void assertContainsSameTokensButSkipNulls(
            final Expression actual, final Token... expected)
    {
        assertContainsSameTokens(true, actual, expected);
    }
    
    /**********
     * matrix *
     **********/
    
    /**
     * Populates a matrix with the elements from an array.
     * @param matrix the matrix to populate
     * @param entries the elements, one row after another
     * (any length allowed)
     */
    public static void populateMatrix(
            final Matrix matrix, final double... entries)
    {
        final int entryCount
            = Math.min(entries.length, matrix.rows() * matrix.columns());
        for (int i = 0; i < entryCount; i++)
        {
            final int row = i / matrix.columns();
            final int col = i % matrix.columns();
            matrix.set(row, col, entries[i]);
        }
    }
    
    /**
     * Whether a matrix approximately equals another.
     */
    public static boolean approxEquals(
            final Matrix matrix1, final Matrix matrix2, final double delta)
    {
        if (matrix1.rows() != matrix2.rows()
                || matrix1.columns() != matrix2.columns())
        {
            return false;
        }
        for (int i = 0; i < matrix1.rows(); i++)
        {
            for (int j = 0; j < matrix1.columns(); j++)
            {
                if (!approxEquals(
                        matrix1.get(i, j), matrix2.get(i, j), delta))
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Whether a matrix is lower triangular.
     * The matrix must be square and have zeroes above its main diagonal.
     */
    public static boolean isLowerTriangular(
            final Matrix matrix, final double delta)
    {
        if (matrix.rows() != matrix.columns())
        {
            return false;
        }
        for (int i = 0; i < matrix.rows(); i++)
        {
            for (int j = i - 1; j >= 0; j--)
            {
                if (!approxZero(matrix.get(j, i), delta))
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Whether a matrix is upper triangular.
     * The matrix must be square and have zeroes below its main diagonal.
     */
    public static boolean isUpperTriangular(
            final Matrix matrix, final double delta)
    {
        if (matrix.rows() != matrix.columns())
        {
            return false;
        }
        for (int i = 0; i < matrix.rows(); i++)
        {
            for (int j = i + 1; j < matrix.rows(); j++)
            {
                if (!approxZero(matrix.get(j, i), delta))
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Whether or not a matrix is on echelon form.
     */
    public static boolean isEchelon(final Matrix matrix, final double delta)
    {
        int prevCol = -1;
        for (int row = 0; row < matrix.rows(); row++)
        {
            int col;
            for (col = 0; col < matrix.columns(); col++)
            {
                if (!approxZero(matrix.get(row, col), delta))
                {
                    break;
                }
            }
            if (prevCol > col)
            {
                return false;
            }
            else if (prevCol == col && col != matrix.columns())
            {
                return false;
            }
            prevCol = col;
        }
        return true;
    }
    
    /**
     * Whether or not a matrix has orthonormal columns.
     */
    public static boolean isOrthogonal(
            final Matrix matrix, final double delta)
    {
        if (matrix.rows() != matrix.columns())
        {
            return false;
        }
        for (int i = 0; i < matrix.columns(); i++)
        {
            final double norm = normF(matrix.column(i), delta);
            if (!approxEquals(norm, 1, delta))
            {
                return false;
            }
        }
        final Matrix timesTrans = matrix.times(matrix.transposed());
        return approxEquals(matrix.identity(matrix.rows()), timesTrans, delta);
    }
    
    /**
     * The Frobenius norm of a matrix.
     */
    private static double normF(final Matrix matrix, final double delta)
    {
        double sumOfSquares = 0;
        for (int i = 0; i < matrix.rows(); i++)
        {
            for (int j = 0; j < matrix.columns(); j++)
            {
                sumOfSquares += Math.pow(matrix.get(i, j), 2);
            }
        }
        if (approxZero(sumOfSquares, delta))
        {
            return 0;
        }
        return Math.sqrt(sumOfSquares);
    }
    
    /**
     * Asserts that two matrices are equal.
     * @param delta error tolerance
     */
    public static void assertEquals(
            final Matrix expected,
            final Matrix actual,
            final double delta,
            final String message)
    {
        assertTrue(approxEquals(expected, actual, delta), message);
    }
    
    /**
     * Asserts that a matrix has zero rows and zero columns.
     */
    public static void assertZeroByZero(final Matrix matrix)
    {
        Assertions.assertEquals(
                0, matrix.rows(), "Assert zero by zero: rows");
        Assertions.assertEquals(
                0, matrix.columns(), "Assert zero by zero: columns");
    }
    
    /**
     * Asserts that a matrix has zero rows.
     * @param expectedColumns expected number of columns
     */
    public static void assertZeroRows(
            final int expectedColumns, final Matrix matrix)
    {
        Assertions.assertEquals(0, matrix.rows(), "Assert zero rows: rows");
        Assertions.assertEquals(
                expectedColumns, matrix.columns(), "Assert zero rows: columns");
    }
    
    /**
     * Asserts that a matrix has zero columns.
     * @param expectedRows expected number of rows
     */
    public static void assertZeroColumns(
            final int expectedRows, final Matrix matrix)
    {
        Assertions.assertEquals(
                expectedRows, matrix.rows(), "Assert zero columns: rows");
        Assertions.assertEquals(
                0, matrix.columns(), "Assert zero columns: columns");
    }
    
    /********
     * task *
     ********/
    
    /**
     * Asserts the compiled status of a compilable task.
     * @param expected expected status
     * @param actual the actual task
     */
    public static void assertCompiled(
            final boolean expected, final Compilable actual)
    {
        if (expected)
        {
            assertTrue(
                    actual.isCompiled(),
                    "Expected compiled, but wasn't.");
        }
        else
        {
            assertFalse(
                    actual.isCompiled(),
                    "Expected not compiled, but was.");
        }
    }
    
    /***************************
     * Private utility methods *
     ***************************/
    
    private static boolean approxEquals(
            final double d1, final double d2, final double delta)
    {
        return approxZero(d1 - d2, delta);
    }
    
    private static boolean approxZero(final double d, final double delta)
    {
        return Math.abs(d) <= delta;
    }

}
