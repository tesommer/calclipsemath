/**
 * Contains the entry point to the CalclipseMath script interpreter.
 */
package calclipsemath.main;
