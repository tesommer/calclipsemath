package calclipsemath.main;

import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import com.calclipse.lib.resource.Resources;

/**
 * A command-line program that prints mcomp properties to STDOUT.
 * Command-line arguments are:
 * {@code
 * resource-name property-name[ delimiter=","[ prefix=""[ suffix=""]]]}.
 * Searches for {@code property-name} in files named {@code resource-name}
 * located in directories, jars and zips on the module-path or class-path.
 * The same property may be specified multiple times in the same file
 * by appending a period and an optional suffix to the property name.
 * 
 * @author Tone Sommerland
 */
public final class Props
{
    private static String DEFAULT_DELIMITER = ",";
    private static String DEFAULT_PREFIX = "";
    private static String DEFAULT_SUFFIX = "";
    
    private static final String
    HELP
        = "@props.help@";

    private Props()
    {
    }
    
    public static void main(final String[] args) throws IOException
    {
        if (args.length < 2)
        {
            System.err.println(HELP);
            return;
        }
        final var values = new ArrayList<String>();
        Resources.findOnModuleOrClassPath(slashedIfNotAlready(args[0]))
            .use(in -> addPropertyValues(values, args[1], in));
        System.out.print(
                values.stream()
                    .collect(joining(
                            delimiter(args),
                            prefix(args),
                            suffix(args))));
    }
    
    private static boolean addPropertyValues(
            final Collection<? super String> values,
            final String property,
            final InputStream in) throws IOException
    {
        final var props = new Properties();
        props.load(in);
        props.stringPropertyNames().stream()
            .filter(key -> isProperty(property, key))
            .map(props::getProperty)
            .forEach(values::add);
        return true;
    }
    
    private static String slashedIfNotAlready(final String resourceName)
    {
        if (resourceName.startsWith("/"))
        {
            return resourceName;
        }
        return '/' + resourceName;
    }
    
    private static boolean isProperty(final String property, final String key)
    {
        return key.equals(property) || key.startsWith(property + '.');
    }
    
    private static String delimiter(final String[] args)
    {
        return args.length < 3 ? DEFAULT_DELIMITER : args[2];
    }
    
    private static String prefix(final String[] args)
    {
        return args.length < 4 ? DEFAULT_PREFIX : args[3];
    }
    
    private static String suffix(final String[] args)
    {
        return args.length < 5 ? DEFAULT_SUFFIX : args[4];
    }

}
