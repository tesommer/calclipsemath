package calclipsemath.main.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class AliasesMerger
{
    private final Properties mergeFrom = new Properties();
    private final Map<String, String> mergeTo = new HashMap<>();
    
    public AliasesMerger()
    {
    }
    
    public boolean merge(final InputStream in) throws IOException
    {
        mergeFrom.load(in);
        for (final String key : mergeFrom.stringPropertyNames())
        {
            if (mergeTo.containsKey(key))
            {
                throw new IOException("Duplicate alias: " + key);
            }
            mergeTo.put(key, mergeFrom.getProperty(key));
        }
        mergeFrom.clear();
        return true;
    }
    
    public Map<String, String> aliases()
    {
        return Collections.unmodifiableMap(mergeTo);
    }

}
