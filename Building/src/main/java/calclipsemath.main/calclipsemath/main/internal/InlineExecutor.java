package calclipsemath.main.internal;

import java.io.StringReader;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.script.ScriptParser;
import com.calclipse.mcomp.trace.Pinpoint;
import com.calclipse.mcomp.trace.Place;
import com.calclipse.mcomp.trace.Trace;

/**
 * Inline script and expression executor.
 * 
 * @author Tone Sommerland
 */
public final class InlineExecutor
{
    private static final char ESCAPE_PREFIX = '\\';
    private static final char R = 'r';
    private static final char N = 'n';
    private static final char T = 't';
    private static final char CR = '\r';
    private static final char LF = '\n';
    private static final char TAB = '\t';
    
    private InlineExecutor()
    {
    }
    
    /**
     * Executes the given inline script in the given context.
     * @param script may contain escaped CR, LF and tab
     */
    public static void executeScript(
            final String script, final McContext context) throws ErrorMessage
    {
        ScriptParser.trackingErrorsWith(context.tracer()).parse(
                new StringReader(unescape(script))).execute(context);
    }
    
    /**
     * Executes the given inline expression in the given context.
     * @param expression may contain escaped CR, LF and tab
     */
    public static void executeExpression(
            final String expression,
            final McContext context) throws ErrorMessage
    {
        final String unescaped = unescape(expression);
        try
        {
            context.parse(unescaped).evaluate(true);
        }
        catch(final ErrorMessage ex)
        {
            throw enrich(ex, unescaped, context);
        }
    }
    
    /**
     * Evaluates the given inline expression in the given context,
     * and then prints the result.
     * @param expression may contain escaped CR, LF and tab
     */
    public static void calculateExpression(
            final String expression,
            final McContext context) throws ErrorMessage
    {
        final String unescaped = unescape(expression);
        try
        {
            context.environment().out().println(context.parse(unescaped)
                    .evaluate(true).get().toString());
        }
        catch(final ErrorMessage ex)
        {
            throw enrich(ex, unescaped, context);
        }
    }
    
    private static ErrorMessage enrich(
            final ErrorMessage ex,
            final String expression,
            final McContext context)
    {
        final var trace = new Trace.Builder().setPlace(Place.START);
        ex.detail()
            .map(fragment -> Pinpoint.at(expression, fragment))
            .ifPresent(trace::setPinpoint);
        return context.tracer().track(ex, trace.build());
    }
    
    private static String unescape(final String str)
    {
        final var result = new StringBuilder(str);
        for (int i = 0; i < result.length() - 1; i++)
        {
            final char c = result.charAt(i);
            if (c == ESCAPE_PREFIX)
            {
                final char c2 = result.charAt(i + 1);
                if (c2 == R || c2 == N || c2 == T || c2 == ESCAPE_PREFIX)
                {
                    result.deleteCharAt(i);
                    if (c2 == R)
                    {
                        result.setCharAt(i, CR);
                    }
                    else if (c2 == N)
                    {
                        result.setCharAt(i, LF);
                    }
                    else if (c2 == T)
                    {
                        result.setCharAt(i, TAB);
                    }
                }
            }
        }
        return result.toString();
    }

}
