package calclipsemath.main;

import static com.calclipse.math.expression.Errors.errorMessage;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.logging.Logger;

import com.calclipse.lib.resource.Resources;
import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.mcomp.Contexts;
import com.calclipse.mcomp.Locators;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;
import com.calclipse.mcomp.script.ScriptParser;
import com.calclipse.mcomp.script.message.ScriptMessages;

import calclipsemath.main.internal.AliasesMerger;
import calclipsemath.main.internal.InlineExecutor;

/**
 * The entry point to the script interpreter.
 * 
 * @author Tone Sommerland
 */
public final class Main
{
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    
    private static final String ARGS_LOCATION          = "argv";
    private static final String ALIASES_RESOURCE_NAME  = "/aliases.properties";
    private static final String STDIN_LOCATION         = "-";
    private static final String VERSION_ARG            = "-v";
    private static final String HELP_ARG               = "-h";
    private static final String INLINE_SCRIPT_ARG      = "-s";
    private static final String INLINE_EXPRESSION_ARG  = "-e";
    private static final String INLINE_CALCULATION_ARG = "-c";
    
    private static final String
    VERSION
        = "@productandversion@";
    
    private static final String
    HELP
        = "@main.help@";

    private Main()
    {
    }

    /**
     * The main method.
     */
    public static void main(final String[] args)
    {
        final McContext context = Contexts.getOne();
        addLocators(args, context);
        try
        {
            if (args.length == 0)
            {
                readMcompFromStdin(context).execute(context);
            }
            else
            {
                if (args[0].equals(HELP_ARG))
                {
                    printToStdout(HELP, context);
                }
                else if (args[0].equals(VERSION_ARG))
                {
                    printToStdout(VERSION, context);
                }
                else if (args[0].equals(STDIN_LOCATION))
                {
                    readMcompFromStdin(context).execute(context);
                }
                else if (isInlineSpecifier(args[0]))
                {
                    executeInline(args, context);
                }
                else
                {
                    final Mcomp mcomp = context.mcompAt(args[0], null)
                            .orElseThrow(() -> locationNotFound(args[0]));
                    mcomp.execute(context);
                }
            }
        }
        catch (final ErrorMessage ex)
        {
            printToStderr(ex.getMessage(), context);
            System.exit(1);
        }
    }
    
    private static void addLocators(
            final String[] args, final McContext context)
    {
        try
        {
            final Map<String, String> aliases = loadAliases();
            Locators.addClassLocator(context);
            Locators.addScriptLocator(context);
            Locators.addFileLocator(context);
            Locators.addAliasResolver(aliases, context);
            Locators.addArgLocator(ARGS_LOCATION, context, args);
        }
        catch (final IOException ex)
        {
            LOGGER.warning(ex.toString());
        }
    }
    
    private static Map<String, String> loadAliases() throws IOException
    {
        final var merger = new AliasesMerger();
        Resources.findOnModuleOrClassPath(ALIASES_RESOURCE_NAME)
            .use(merger::merge);
    	return merger.aliases();
    }

    private static Mcomp readMcompFromStdin(final McContext context)
            throws ErrorMessage
    {
        return ScriptParser.trackingErrorsWith(context.tracer())
                .trackingLocation(STDIN_LOCATION)
                .parse(new InputStreamReader(context.environment().in()));
    }
    
    private static ErrorMessage locationNotFound(final String location)
    {
        return errorMessage(ScriptMessages.locationNotFound(location));
    }

    private static void printToStderr(
            final String message, final McContext context)
    {
        context.environment().err().println(message);
    }
    
    private static void printToStdout(
            final String message, final McContext context)
    {
        context.environment().out().println(message);
    }
    
    private static boolean isInlineSpecifier(final String arg)
    {
        return
                   arg.equals(INLINE_SCRIPT_ARG)
                || arg.equals(INLINE_EXPRESSION_ARG)
                || arg.equals(INLINE_CALCULATION_ARG);
    }
    
    private static void executeInline(final String[] args, McContext context)
            throws ErrorMessage
    {
        if (args.length < 2)
        {
            throw errorMessage(HELP);
        }
        else if (args[0].equals(INLINE_SCRIPT_ARG))
        {
            InlineExecutor.executeScript(args[1], context);
        }
        else if (args[0].equals(INLINE_EXPRESSION_ARG))
        {
            InlineExecutor.executeExpression(args[1], context);
        }
        else
        {
            InlineExecutor.calculateExpression(args[1], context);
        }
    }

}
