/**
 * Contains the entry point to the CalclipseMath script interpreter.
 */
module calclipsemath.main
{
    requires java.logging;
    requires com.calclipse.lib;
    requires com.calclipse.mcomp;
    exports calclipsemath.main;
}
