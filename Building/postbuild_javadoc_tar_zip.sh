#!/bin/sh

javadoc -d @dirjavadoc@/com.calclipse.math \
        -sourcepath @dirsrc@/main/java/com.calclipse.math \
        -subpackages com.calclipse.math
javadoc -d @dirjavadoc@/com.calclipse.mcomp \
        -sourcepath @dirsrc@/main/java/com.calclipse.mcomp \
        -subpackages com.calclipse.mcomp \
        --module-path @dirhome@/lib \
        -link ../com.calclipse.math

grep '^#!' -l @dirhome@/bin/* | xargs chmod +x
tar -cvpzf @dirhome@.tar.gz @dirhome@
tar -cvpzf @dirjavadoc@.tar.gz @dirjavadoc@

zip -r @dirhome@.zip @dirhome@
zip -r @dirjavadoc@.zip @dirjavadoc@
