@ECHO OFF
setlocal

SET module_path="%~dp0..\lib;%~dp0..\inventory"
SET props_module="calclipsemath.main/calclipsemath.main.Props"

java --module-path %module_path% --module %props_module% mcomp.properties module
