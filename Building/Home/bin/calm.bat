@ECHO OFF
setlocal

SET module_path="%~dp0..\lib;%~dp0..\inventory"
SET main_module="calclipsemath.main/calclipsemath.main.Main"

FOR /F "tokens=* USEBACKQ" %%F IN (`getmodules`) DO (
   SET modules=%%F
)

IF "%modules%"=="" (
   java --module-path %module_path% --module %main_module% %*
)
IF NOT "%modules%"=="" (
   java --module-path %module_path% --add-modules %modules% --module %main_module% %*
)
