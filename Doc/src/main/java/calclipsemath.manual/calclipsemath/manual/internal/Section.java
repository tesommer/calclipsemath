package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class Section
{
    private final List<SectionElement> elements = new ArrayList<>();
    private final String title;

    Section(final String title)
    {
        this.title = title;
    }
    
    Section()
    {
        this(null);
    }

    public void addElement(final SectionElement element)
    {
        elements.add(requireNonNull(element, "element"));
    }

    public Optional<String> title()
    {
        return Optional.ofNullable(title);
    }

    public List<SectionElement> elements()
    {
        return Collections.unmodifiableList(elements);
    }

}
