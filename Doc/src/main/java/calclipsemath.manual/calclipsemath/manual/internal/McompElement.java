package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class McompElement extends ManualElement
{
    private final List<String> locations = new ArrayList<>();
    private final List<TokenElement> tokens = new ArrayList<>();
    private final String namespace;
    
    McompElement(final String name, final String namespace)
    {
        super(name);
        this.namespace = requireNonNull(namespace, "namespace");
    }
    
    public void addLocation(final String location)
    {
        locations.add(requireNonNull(location, "location"));
    }
    
    public void addToken(final TokenElement token)
    {
        tokens.add(requireNonNull(token, "token"));
    }

    public String namespace()
    {
        return namespace;
    }

    public List<String> locations()
    {
        return Collections.unmodifiableList(locations);
    }

    public List<TokenElement> tokens()
    {
        return Collections.unmodifiableList(tokens);
    }

}
