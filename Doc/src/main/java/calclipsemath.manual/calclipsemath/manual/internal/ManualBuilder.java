package calclipsemath.manual.internal;

import java.util.HashMap;
import java.util.Map;

import com.calclipse.math.expression.Delimiters;
import com.calclipse.math.expression.Function;
import com.calclipse.math.expression.Operator;
import com.calclipse.math.expression.Token;
import com.calclipse.math.expression.TokenType;
import com.calclipse.mcomp.McContext;

public final class ManualBuilder
{
    private ManualBuilder()
    {
    }
    
    public static Manual buildManual(final McContext context)
    {
        final var manual = new Manual();
        final var mcomps = new HashMap<String, McompElement>();
        final String namespaceSeparator = context.namespaceSeparator();
        for (final Token token : context.symbols())
        {
            if (isExported(token, namespaceSeparator))
            {
                mcompElement(mcomps, token, namespaceSeparator)
                    .addToken(tokenElement(token, namespaceSeparator));
            }
            else
            {
                manual.addElement(tokenElement(token, namespaceSeparator));
            }
        }
        mcomps.values().forEach(manual::addElement);
        return manual;
    }
    
    private static TokenElement tokenElement(
            final Token token, final String namespaceSeparator)
    {
        final String fullName = token.name();
        final String name;
        final int nsIndex = fullName.indexOf(namespaceSeparator);
        if (nsIndex < 1)
        {
            name = fullName;
        }
        else
        {
            name = fullName.substring(nsIndex + 1);
        }
        if (token.type().isBinary() || token.type().isUnary())
        {
            return new TokenElement(
                    name,
                    fullName,
                    type(token.type()),
                    ((Operator)token).priority());
        }
        else if (token.type().isFunction())
        {
            final Delimiters delimiters = ((Function)token).delimiters();
            return new TokenElement(
                    name,
                    fullName,
                    type(token.type()),
                    new Delim(delimiters.opener(), delimiters.closer()));
        }
        return new TokenElement(
                name,
                fullName,
                type(token.type()));
    }
    
    private static McompElement mcompElement(
            final Map<String, McompElement> mcomps,
            final Token token,
            final String namespaceSeparator)
    {
        final String namespace = namespace(token, namespaceSeparator);
        return mcomps.computeIfAbsent(
                namespace, ns -> new McompElement(ns, ns));
    }
    
    private static boolean isExported(
            final Token token, final String namespaceSeparator)
    {
        return !namespace(token, namespaceSeparator).trim().isEmpty();
    }
    
    private static String namespace(
            final Token token, final String namespaceSeparator)
    {
        final String name = token.name();
        final int nsIndex = name.indexOf(namespaceSeparator);
        if (nsIndex < 1)
        {
            return "";
        }
        return name.substring(0, nsIndex);
    }
    
    private static TokenElementType type(final TokenType type)
    {
        if (type.isLeftParenthesis())
        {
            return TokenElementType.LPAREN;
        }
        else if (type.isRightParenthesis())
        {
            return TokenElementType.RPAREN;
        }
        else if (type.isFunction())
        {
            return TokenElementType.FUNCTION;
        }
        else if (type.isOperand())
        {
            return TokenElementType.OPERAND;
        }
        else if (type.isBinary())
        {
            return TokenElementType.BOPERATOR;
        }
        else if (type.isLeftUnary())
        {
            return TokenElementType.LUOPERATOR;
        }
        else if (type.isRightUnary())
        {
            return TokenElementType.RUOPERATOR;
        }
        else if (type.isUndefined())
        {
            return TokenElementType.UNDEFINED;
        }
        else
        {
            return TokenElementType.NOOP;
        }
    }

}
