package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

import java.util.Optional;

public final class TokenElement extends ManualElement
{
    private final String fullName;
    private final TokenElementType type;
    private final int priority;
    private final Delim delim;
    
    private TokenElement(
            final String name,
            final String fullName,
            final TokenElementType type,
            final int priority,
            final Delim delim)
    {
        super(name);
        this.fullName = requireNonNull(fullName, "fullName");
        this.type = requireNonNull(type, "type");
        this.priority = priority;
        this.delim = delim;
    }
    
    TokenElement(
            final String name,
            final String fullName,
            final TokenElementType type,
            final Delim delim)
    {
        this(name, fullName, type, -1, requireNonNull(delim, "delim"));
    }
    
    TokenElement(
            final String name,
            final String fullName,
            final TokenElementType type,
            final int priority)
    {
        this(name, fullName, type, priority, null);
    }
    
    TokenElement(
            final String name,
            final String fullName,
            final TokenElementType type)
    {
        this(name, fullName, type, -1, null);
    }

    public String fullName()
    {
        return fullName;
    }

    public TokenElementType type()
    {
        return type;
    }

    public int priority()
    {
        return priority;
    }

    public Optional<Delim> delim()
    {
        return Optional.ofNullable(delim);
    }

}
