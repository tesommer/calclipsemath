package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManualElement
{
    private final List<Section> sections = new ArrayList<>();
    private final String name;
    
    ManualElement(final String name)
    {
        this.name = requireNonNull(name, "name");
    }
    
    public void addSection(final Section section)
    {
        sections.add(requireNonNull(section, "section"));
    }

    public String name()
    {
        return name;
    }

    public List<Section> sections()
    {
        return Collections.unmodifiableList(sections);
    }

}
