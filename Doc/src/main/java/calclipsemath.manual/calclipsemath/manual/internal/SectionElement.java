package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

public class SectionElement
{
    private final String content;

    SectionElement(final String content)
    {
        this.content = requireNonNull(content, "content");
    }

    public String content()
    {
        return content;
    }

}
