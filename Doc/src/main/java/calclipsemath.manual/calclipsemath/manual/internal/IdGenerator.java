package calclipsemath.manual.internal;

public final class IdGenerator
{
    private IdGenerator()
    {
    }
    
    public static String generateId(final String name)
    {
        final var id = new StringBuilder("id");
        for (int i = 0; i < name.length(); i++)
        {
            final char c = name.charAt(i);
            if (Character.isLetterOrDigit(c))
            {
                id.append(c);
            }
            else
            {
                id.append('_');
                id.append((int)c);
            }
        }
        return id.toString();
    }

}
