package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

public final class Delim
{
    private final String opener;
    private final String closer;

    Delim(final String opener, final String closer)
    {
        this.opener = requireNonNull(opener, "opener");
        this.closer = requireNonNull(closer, "closer");
    }

    public String opener()
    {
        return opener;
    }

    public String closer()
    {
        return closer;
    }

}
