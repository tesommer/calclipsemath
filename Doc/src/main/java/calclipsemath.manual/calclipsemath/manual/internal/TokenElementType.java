package calclipsemath.manual.internal;

public enum TokenElementType
{
    LPAREN,
    RPAREN,
    FUNCTION,
    OPERAND,
    BOPERATOR,
    LUOPERATOR,
    RUOPERATOR,
    UNDEFINED,
    NOOP,
}
