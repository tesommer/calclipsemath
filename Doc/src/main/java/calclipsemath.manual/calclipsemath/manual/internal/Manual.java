package calclipsemath.manual.internal;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Manual
{
    private final List<ManualElement> elements = new ArrayList<>();

    Manual()
    {
    }
    
    public void addElement(final ManualElement element)
    {
        elements.add(requireNonNull(element, "element"));
    }

    public List<ManualElement> elements()
    {
        return Collections.unmodifiableList(elements);
    }

}
