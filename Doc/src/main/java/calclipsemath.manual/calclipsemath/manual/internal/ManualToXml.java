package calclipsemath.manual.internal;

public final class ManualToXml
{
    private static final String EOL = System.getProperty("line.separator");
    
    private static final String QUOTE = "\"";
    private static final String QUOTE_GT = "\">";
    private static final String BEGIN_NAME = "        <name>";
    private static final String END_NAME = "</name>";

    private ManualToXml()
    {
    }
    
    public static String toXml(final Manual manual)
    {
        final var xml = new StringBuilder();
        xml.append("<?xml version=\"1.0\"?>");
        xml.append(EOL);
        xml.append("<manual>");
        xml.append(EOL);
        for (final ManualElement element : manual.elements())
        {
            if (element instanceof DataTypeElement)
            {
                append(xml, (DataTypeElement)element);
            }
            else if (element instanceof TokenElement)
            {
                append(xml, (TokenElement)element, 1);
            }
            else if (element instanceof McompElement)
            {
                append(xml, (McompElement)element);
            }
        }
        xml.append("</manual>");
        xml.append(EOL);
        return xml.toString();
    }
    
    private static void append(
            final StringBuilder xml,
            final TokenElement token,
            final int indents)
    {
        final var top = new StringBuilder("<token id=\"");
        top.append(IdGenerator.generateId(token.fullName()));
        top.append(QUOTE);
        if (token.type() == TokenElementType.BOPERATOR
                || token.type() == TokenElementType.LUOPERATOR
                || token.type() == TokenElementType.RUOPERATOR)
        {
            top.append(" priority=\"");
            top.append(token.priority());
            top.append(QUOTE);
        }
        top.append(">");
        top.append(EOL);
        top.append("    <name>");
        top.append(escape(token.name()));
        top.append("</name>");
        top.append(EOL);
        top.append("    <type>");
        top.append(token.type());
        top.append("</type>");
        top.append(EOL);
        token.delim().ifPresent(delim ->
        {
            top.append("    <delimitation>");
            top.append(EOL);
            if (!delim.opener().isEmpty())
            {
                top.append("        <opener>");
                top.append(escape(delim.opener()));
                top.append("</opener>");
                top.append(EOL);
            }
            top.append("        <closer>");
            top.append(escape(delim.closer()));
            top.append("</closer>");
            top.append(EOL);
            top.append("    </delimitation>");
            top.append(EOL);
        });
        final var bottom = new StringBuilder("</token>");
        bottom.append(EOL);
        xml.append(indent(top, indents));
        for (final Section section : token.sections())
        {
            append(xml, section, indents + 2);
        }
        xml.append(indent(bottom, indents));
    }
    
    private static void append(
            final StringBuilder xml, final McompElement mcomp)
    {
        xml.append("    <mcomp id=\"");
        xml.append(IdGenerator.generateId(mcomp.name()));
        xml.append(QUOTE_GT);
        xml.append(EOL);
        xml.append(BEGIN_NAME);
        xml.append(mcomp.name());
        xml.append(END_NAME);
        xml.append(EOL);
        xml.append("        <namespace>");
        xml.append(mcomp.namespace());
        xml.append("</namespace>");
        xml.append(EOL);
        for (final String location : mcomp.locations())
        {
            xml.append("        <location>");
            xml.append(location);
            xml.append("</location>");
            xml.append(EOL);
        }
        for (final Section section : mcomp.sections())
        {
            append(xml, section, 2);
        }
        for (final TokenElement token : mcomp.tokens())
        {
            append(xml, token, 2);
        }
        xml.append("    </mcomp>");
        xml.append(EOL);
    }
    
    private static void append(
            final StringBuilder xml, final DataTypeElement dataType)
    {
        xml.append("    <data-type id=\"");
        xml.append(IdGenerator.generateId(dataType.name()));
        xml.append(QUOTE_GT);
        xml.append(EOL);
        xml.append(BEGIN_NAME);
        xml.append(dataType.name());
        xml.append(END_NAME);
        xml.append(EOL);
        for (final Section section : dataType.sections())
        {
            append(xml, section, 2);
        }
        xml.append("    </data-type>");
        xml.append(EOL);
    }
    
    private static void append(
            final StringBuilder xml, final Section section, final int indents)
    {
        final var top = new StringBuilder("<section>");
        top.append(EOL);
        section.title().ifPresent(title ->
        {
            top.append("    <title>");
            top.append(title);
            top.append("</title>");
            top.append(EOL);
        });
        final var bottom = new StringBuilder("</section>");
        bottom.append(EOL);
        xml.append(indent(top, indents));
        for (final SectionElement element : section.elements())
        {
            if (element instanceof Remark)
            {
                append(xml,(Remark)element, indents + 1);
            }
            else if (element instanceof Code)
            {
                append(xml,(Code)element, indents + 1);
            }
        }
        xml.append(indent(bottom, indents));
    }
    
    private static void append(
            final StringBuilder xml, final Remark remark, final int indents)
    {
        final var element = new StringBuilder("<remark>");
        element.append(EOL);
        element.append(indent(remark.content(), 1));
        element.append(EOL);
        element.append("</remark>");
        element.append(EOL);
        xml.append(indent(element, indents));
    }
    
    private static void append(
            final StringBuilder xml, final Code code, final int indents)
    {
        final var top = new StringBuilder("<code>");
        top.append(EOL);
        final var bottom = new StringBuilder("</code>");
        bottom.append(EOL);
        xml.append(indent(top, indents));
        xml.append(code.content());
        xml.append(EOL);
        xml.append(indent(bottom, indents));
    }
    
    private static String escape(final String text)
    {
        return text
            .replace("&", "&amp;")
            .replace("<", "&lt;")
            .replace(">", "&gt;");
    }
    
    private static String indent(final String text, final int times)
    {
        String indentedText = text;
        for (int i = 0; i < times; i++)
        {
            indentedText = indentedText.replaceAll("(?m)^", "    ");
        }
        return indentedText;
    }
    
    private static String indent(final StringBuilder text, final int times)
    {
        return indent(text.toString(), times);
    }

}
