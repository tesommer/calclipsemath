package calclipsemath.manual;

import com.calclipse.math.expression.ErrorMessage;
import com.calclipse.math.expression.Operand;
import com.calclipse.math.expression.UnaryOperator;
import com.calclipse.math.expression.operation.Value;
import com.calclipse.math.parser.OperatorPriorities;
import com.calclipse.mcomp.McContext;
import com.calclipse.mcomp.Mcomp;

import calclipsemath.manual.internal.IdGenerator;
import calclipsemath.manual.internal.Manual;
import calclipsemath.manual.internal.ManualBuilder;
import calclipsemath.manual.internal.ManualToXml;

/**
 * This mcomp generates a template manual
 * using the context where this mcomp is imported.
 * The name of this mcomp is "manual_template".
 * Exports symbol "xml" which contains the manual as XML.
 * An operator named "id" is also exported.
 * The operator generates an XML ID for a name or symbol.
 * 
 * @author Tone Sommerland
 */
public final class ManualTemplateComp implements Mcomp
{
    public static final String MCOMP_NAME = "manual_template";
    public static final String MAN_SYMBOL = "man";
    public static final String XML_SYMBOL = "xml";
    public static final String ID_SYMBOL = "id";

    public ManualTemplateComp()
    {
    }

    @Override
    public void execute(final McContext context) throws ErrorMessage
    {
        final McContext manualContext = context.parent().orElse(context);
        final Manual manual = ManualBuilder.buildManual(manualContext);
        context.exportSymbol(Operand.of(
                XML_SYMBOL,
                Value.constantOf(ManualToXml.toXml(manual))));
        context.exportSymbol(UnaryOperator.prefixed(
                ID_SYMBOL,
                OperatorPriorities.MEDIUM,
                arg -> Value.constantOf(
                        IdGenerator.generateId(arg.get().toString()))));
    }

    @Override
    public String name()
    {
        return MCOMP_NAME;
    }

}
