/**
 * Contains an {@link com.calclipse.mcomp.Mcomp}
 * that generates a symbol-reference template.
 */
module calclipsemath.manual
{
    requires transitive com.calclipse.mcomp;
    exports calclipsemath.manual;
}
