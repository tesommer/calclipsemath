<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html lang="en">
            <head>
                <meta charset="UTF-8" />
                <title>Manual</title>
                <link rel="stylesheet" href="manual.css" />
                <script>
                    <xsl:text disable-output-escaping="yes">
                        function showOrHideIndex() {
                            var index = document.getElementById("index");
                            var menu = document.getElementById("index_menu");
                            var toggle = document.getElementById("index_toggle");
                            if (menu.style.visibility !== "visible") {
                                menu.style.visibility = "visible";
                                index.style.zIndex = "21";
                                toggle.className = "index_toggle_on";
                            } else {
                                menu.style.visibility = "hidden";
                                index.style.zIndex = "-21";
                                toggle.className = "index_toggle_off";
                            }
                        }
                        function colorComments() {
                            var codeElements = document.getElementsByTagName("code");
                            for (var i = 0; i &lt; codeElements.length; ++i) {
                                var codeElement = codeElements[i];
                                var code = codeElement.innerHTML;
                                codeElement.innerHTML
                                    = code.replace(/\/\*/g, "&lt;span class=\"comment\"&gt;/*")
                                          .replace(/\*\//g, "*/&lt;/span&gt;");
                            }
                        }
                    </xsl:text>
                </script>
            </head>
            <body onLoad="colorComments()">
                <xsl:call-template name="index"/>
                <h1>Manual</h1>
                <xsl:apply-templates select="*/data-type|*/token|*/mcomp"/>
            </body>
        </html>
    </xsl:template>
    
    <!--
        Index
    -->
    
    <xsl:template name="index">
        <nav id="index">
            <button id="index_toggle" type="button" onclick="showOrHideIndex()">Index</button>
            <ul id="index_menu" onclick="showOrHideIndex()">
                <li class="home">
                    <a>
                        <xsl:attribute name="href">
                            <xsl:text>#</xsl:text>
                        </xsl:attribute>
                        Top
                    </a>
                </li>
                <xsl:for-each select="*/data-type|*/token|*/mcomp">
                    <xsl:sort select="name"/>
                    <xsl:choose>
                        <xsl:when test="name(.) = 'data-type'">
                            <xsl:call-template name="indexItem_DataType"/>
                        </xsl:when>
                        <xsl:when test="name(.) = 'mcomp'">
                            <xsl:call-template name="indexItem_Mcomp"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="indexItem_Token"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </ul>
        </nav>
    </xsl:template>
    
    <xsl:template name="indexItem_DataType">
        <li class="data-type">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#</xsl:text><xsl:value-of select="@id"/>
                </xsl:attribute>
                <xsl:value-of select="name"/>
            </a>
        </li>
    </xsl:template>
    
    <xsl:template name="indexItem_Token_Delim">
        <xsl:if test="count(delimitation) = 1 and count(delimitation/opener) = 0 and string-length(delimitation/closer[last()]) = 1">
            <xsl:text disable-output-escaping="yes">&#8230;</xsl:text>
            <xsl:value-of select="delimitation/closer[last()]"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="indexItem_Token">
        <li class="token">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#</xsl:text><xsl:value-of select="@id"/>
                </xsl:attribute>
                <xsl:value-of select="name"/>
                <xsl:call-template name="indexItem_Token_Delim"/>
            </a>
        </li>
    </xsl:template>
    
    <xsl:template name="indexItem_Mcomp">
        <li class="mcomp">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#</xsl:text><xsl:value-of select="@id"/>
                </xsl:attribute>
                <xsl:value-of select="name"/>
            </a>
        </li>
        <xsl:for-each select="token">
            <xsl:sort select="name"/>
            <xsl:call-template name="indexItem_Mcomp_Token"/>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="indexItem_Mcomp_Token">
        <li class="exported_token">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#</xsl:text><xsl:value-of select="@id"/>
                </xsl:attribute>
                <xsl:value-of select="../namespace"/>:<xsl:value-of select="name"/>
                <xsl:call-template name="indexItem_Token_Delim"/>
            </a>
        </li>
    </xsl:template>
    
    <!--
        Manual
    -->
    
    <!-- title -->
    <xsl:template match="title">
        <h3><xsl:value-of select="."/></h3>
    </xsl:template>
    
    <!-- remark -->
    <xsl:template match="remark">
        <p><xsl:apply-templates select="link|text()"/></p>
    </xsl:template>
    
    <!-- code -->
    <xsl:template match="code">
        <pre>
            <code><xsl:apply-templates select="link|text()"/></code>
        </pre>
    </xsl:template>
    
    <!-- link -->
    <xsl:template match="link">
        <xsl:apply-templates select="@ref|@href"/>
    </xsl:template>
    
    <!-- internal link -->
    <xsl:template match="@ref">
        <a>
            <xsl:attribute name="href">
                <xsl:text>#</xsl:text><xsl:value-of select="."/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test=".. = ''">
                    <xsl:variable name="ref" select="."/>
                    <xsl:value-of select="//*[@id=$ref]/name"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=".."/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
    </xsl:template>
    
    <!-- external link -->
    <xsl:template match="@href">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="."/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test=".. = ''">
                    <xsl:value-of select="."/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=".."/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
    </xsl:template>
    
    <!-- section -->
    <xsl:template match="section">
        <xsl:apply-templates select="title|remark|code"/>
    </xsl:template>
    
    <!-- priority -->
    <xsl:template match="@priority">
        <li><b>Priority: </b><xsl:value-of select="."/></li>
    </xsl:template>
    
    <!-- delimitation/opener -->
    <xsl:template match="delimitation/opener">
        <xsl:apply-templates select="link|text()"/>
    </xsl:template>
    
    <!-- delimitation/closer -->
    <xsl:template match="delimitation/closer">
        <xsl:text disable-output-escaping="yes">&#8230;</xsl:text><xsl:apply-templates select="link|text()"/>
    </xsl:template>
    
    <!-- delimitation/separator -->
    <xsl:template match="delimitation/separator">
        <xsl:if test="position() &lt; count(../*)">
            <xsl:text disable-output-escaping="yes">&#8230;</xsl:text>
        </xsl:if>
        <xsl:apply-templates select="link|text()"/>
    </xsl:template>
    
    <!-- namespace -->
    <xsl:template match="namespace">
        <li><b>Namespace: </b><xsl:value-of select="."/></li>
    </xsl:template>
    
    <!-- location -->
    <xsl:template match="location">
        <li><b>Location: </b><xsl:value-of select="."/></li>
    </xsl:template>
    
    <!-- data-type -->
    <xsl:template match="data-type">
        <section class="data-type">
            <h2>
                <a>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@id"/>
                    </xsl:attribute>
                    <xsl:value-of select="name"/>
                </a>
            </h2>
            <xsl:apply-templates select="section"/>
        </section>
    </xsl:template>
    
    <!-- token -->
    <xsl:template match="token">
        <section class="token">
            <h2>
                <a>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@id"/>
                    </xsl:attribute>
                    <xsl:value-of select="name"/>
                </a>
                <xsl:apply-templates select="delimitation/opener"/>
                <xsl:apply-templates select="delimitation/separator"/>
                <xsl:apply-templates select="delimitation/closer"/>
            </h2>
            <ul class="properties">
                <li>
                    <b>Type: </b>
                    <xsl:choose>
                        <xsl:when test="type = 'BOPERATOR'">
                            binary operator
                        </xsl:when>
                        <xsl:when test="type = 'LUOPERATOR'">
                            prefixed unary operator
                        </xsl:when>
                        <xsl:when test="type = 'RUOPERATOR'">
                            postfixed unary operator
                        </xsl:when>
                        <xsl:when test="type = 'OPERAND'">
                            value
                        </xsl:when>
                        <xsl:when test="type = 'FUNCTION'">
                            function
                        </xsl:when>
                        <xsl:when test="type = 'LPAREN'">
                            left parenthesis
                        </xsl:when>
                        <xsl:when test="type = 'RPAREN'">
                            right parenthesis
                        </xsl:when>
                        <xsl:when test="type = 'NOOP'">
                            no-op
                        </xsl:when>
                        <xsl:otherwise>
                            undefined
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
                <xsl:apply-templates select="@priority" />
            </ul>
            <xsl:apply-templates select="section"/>
        </section>
    </xsl:template>
    
    <!-- mcomp -->
    <xsl:template match="mcomp">
        <section class="mcomp">
            <h2>
                <a>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@id"/>
                    </xsl:attribute>
                    <xsl:value-of select="name"/>
                </a>
            </h2>
            <ul class="properties">
                <xsl:apply-templates select="namespace"/>
                <xsl:apply-templates select="location"/>
            </ul>
            <xsl:apply-templates select="section"/>
            <xsl:apply-templates select="token"/>
        </section>
    </xsl:template>
    
</xsl:stylesheet>
