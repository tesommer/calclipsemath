/*****************
 * Show/hide TOC *
 *****************/

/**
 * Shows the TOC.
 */
function showTOC() {
    document.getElementById('toc').style.visibility = "visible";
    document.getElementById('hidetoc').style.visibility = 'visible';
    document.getElementById('showtoc').style.visibility = 'hidden';
    document.getElementById('logo').className = "logo_tocshown";
    document.getElementById('toccontainer').className = "toccontainer_tocshown";
    document.getElementById('contents').className = "contents_tocshown";
    document.getElementById('footer').className = "footer_tocshown";
}

/**
 * Hides the TOC.
 * (Does not collapse menus.)
 */
function hideTOC() {
    document.getElementById('toc').style.visibility = "hidden";
    document.getElementById('hidetoc').style.visibility = 'hidden';
    document.getElementById('showtoc').style.visibility = 'visible';
    document.getElementById('logo').className = "logo_tochidden";
    document.getElementById('toccontainer').className = "toccontainer_tochidden";
    document.getElementById('contents').className = "contents_tochidden";
    document.getElementById('footer').className = "footer_tochidden";
}

/************************
 * Collapse/expand menu *
 ************************/

const COLLAPSE_IMAGE_SRC = "images\/collapse.png"
const COLLAPSE_IMAGE_ALT = "Collapse menu";
const EXPAND_IMAGE_SRC = "images\/expand.png"
const EXPAND_IMAGE_ALT = "Expand menu";

/**
 * Expands a menu.
 * Params are the ID to the ul to expand,
 * and the ID to the collapse/expand img.
 */
function expand(obj, img) {
    obj.style.visibility = "visible";
    obj.style.position = "relative";
    img.src = COLLAPSE_IMAGE_SRC;
    img.alt = COLLAPSE_IMAGE_ALT;
}

/**
 * Collapses a menu.
 * Params are the ID to the ul to collapse,
 * and the ID to the collapse/expand img.
 */
function collapse(obj, img) {
    obj.style.visibility = "hidden";
    obj.style.position = "absolute";
    img.src = EXPAND_IMAGE_SRC;
    img.alt = EXPAND_IMAGE_ALT;
}

/**
 * Expands or collapses menus and submenus.
 * Expected params: sequence of IDs to ul and img elements,
 * e.g. ('ul1', 'img1', 'ul2', 'img2').
 */
function collapseExpandAll() {
    var obj = document.getElementById(arguments[0]);
    var img = document.getElementById(arguments[1]);
    if (img.alt == COLLAPSE_IMAGE_ALT) {
        for (var i = 0; i < arguments.length - 1; i += 2) {
            obj = document.getElementById(arguments[i]);
            img = document.getElementById(arguments[i + 1]);
            collapse(obj, img);
        }
    } else {
        expand(obj, img);
    }
}

/**
 * Collapses menus and submenus.
 * Expected params: sequence of IDs to ul and img elements,
 * e.g. ('ul1', 'img1', 'ul2', 'img2').
 */
function collapseAll() {
    for (var i = 0; i < arguments.length - 1; i += 2) {
        var obj = document.getElementById(arguments[i]);
        var img = document.getElementById(arguments[i + 1]);
        collapse(obj, img);
    }
}
