[CalM -- CalclipseMath](http://calclipse.com)
==================================================

What is CalM?
-------------

* a Java library for parsing and evaluating mathematical expressions
* a script interpreter based on this library

Feature highlights
-----------------

* free (as in free speech)
* highly customizable
* recognizes dozens of math operations out of the box
* data types include complex numbers, strings, matrices and arrays
* supports arbitrary-precision arithmetic
* sample expression: `5.2+ln 2e^sin 2.6pi`

Getting started
---------------

You can
[download the latest version of CalM here](https://bitbucket.org/tesommer/calclipsemath/downloads/).
The downloaded archive contains a folder with the following subdirectories:

* *bin:* executables
* *doc:* documentation
* *inventory:* scripts and mcomps
* *lib:* libraries

The *bin* directory contains launchers for the CalM script interpreter:

* *calm.bat* for Windows
* *calm* for GNU/Linux

Executing CalM with ``-h`` as command-line argument prints a help message to
stdout.

**Note:**  
It's recommended to read the CalM documentation. To do so, open *index.html*
located in the *doc* directory in a Web browser. The documentation contains a
symbol reference with a list of symbols that can be used in expressions.

### The calculator script

The *inventory* directory contains a file name *calc.txt*. It's a calculator
script. You can run the script with this command:

```
calm script:calc.txt
```

Now you can enter mathematical expressions, e.g. ``100 / 200``. The script
declares variables named A to Z that you can store results in, like this:

```
A := 500.2 + 72
```

The variable ``ans`` contains the last answer.

Enter either ``quit()`` or ``exit 0`` to exit the script.

Sample script: binomial distribution
------------------------------------

The following CalM script calculates the probability of *k* successes in a
series of *n* experiments (where an experiment can either succeed or fail) given
the probability *p* for success.

```
exec
    print 'Binomial distribution:';
    print 'The probability of k successes';
    print 'in a series of n experiments';
    print 'where the probability of success is p.';

N := prompt 'n: '
K := prompt 'k: '
P := prompt 'p: '

PX := (N nCr K)(P^K)(1-P)^(N-K)

FRAC_PX := try(frac(PX))

main

printf('Probability: ');

/* If PX is rational, print it as a fraction, unless the denominator is one. */
if 1@FRAC_PX and den 2@FRAC_PX != 1 then
    printf(2@FRAC_PX + ' = ')
end;

print PX
```

If you save the script to, say, *C:\MyScripts\bindistr.txt*, you can run it like
this:

```
calm \MyScripts\bindistr.txt
```

If you save the script in the *inventory* folder, you can run it like this:

```
calm script:bindistr.txt
```

### Short explanation

``exec`` is a script keyword that's followed by an expression to be executed. If
the expression spans multiple lines, the extra lines must be space- or tab
indented. Semicolon is a binary operator used for combining multiple expressions
into one. A semicolon at the end is permitted, but optional.

String literals (i.e. text) are surrounded by either single- or double quotes.

A variable is created by specifying an identifier (e.g. ``N``) followed by an
optional assignment (e.g. ``:= prompt 'n: '``). The ``:=`` keyword, like
``exec``, is followed by an expression: the variable assignment. The ``prompt``
operator waits for the user to input a value.

**Note:**  
``:=`` is both a script keyword and a binary operator that can be used in
expressions.

The variable ``PX`` is assigned the propbability of the binomial distribution
with the values for *n*, *k* and *p* entered by the user.

The variable ``PX_FRAC`` is assigned the result of trying to turn ``PX`` into a
fraction: a two-element array. If ``PX`` is irrational, the first element will
contain ``false``, and the second an error message. Otherwise, the first element
will contain ``true``, and the second the resulting fraction.

``main`` is a script keyword that marks the beginning of the main block.
Everything following the main keyword is interpreted as an expression,
regardless of indentation.

``/*`` begins a comment, and ``*/`` ends it. Comments can occur anywhere in a
script.

A deeper understanding of the above script can be gained by looking up the
symbols
``:=``,
``!=``,
``^``,
``@``,
``and``,
``den``,
``frac``,
``if``,
``nCr``,
``print``,
``printf``,
``prompt`` and
``try``
in the symbol reference.

___

*Copyright (C) 2019 Tone Sommerland*
